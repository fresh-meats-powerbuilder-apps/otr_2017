HA$PBExportHeader$w_appt_exception_filter.srw
forward
global type w_appt_exception_filter from w_base_response_ext
end type
type dw_1 from u_base_dw_ext within w_appt_exception_filter
end type
type dw_division from datawindow within w_appt_exception_filter
end type
type dw_complex from u_base_dw_ext within w_appt_exception_filter
end type
type dw_plants from u_base_dw_ext within w_appt_exception_filter
end type
type dw_carriers from u_base_dw_ext within w_appt_exception_filter
end type
type gb_1 from groupbox within w_appt_exception_filter
end type
end forward

global type w_appt_exception_filter from w_base_response_ext
integer x = 864
integer y = 292
integer width = 997
integer height = 888
string title = "Appointment Exception Filter "
long backcolor = 12632256
dw_1 dw_1
dw_division dw_division
dw_complex dw_complex
dw_plants dw_plants
dw_carriers dw_carriers
gb_1 gb_1
end type
global w_appt_exception_filter w_appt_exception_filter

type variables
Integer		ii_rc
String 		is_division, is_complex, is_plant, is_carrier
DataWindowChild   idwc_division, idwc_plant, idwc_complex, idwc_carrier




end variables

event open;call super::open;String   ls_incoming

Character  lc_start, lc_end

dw_1.InsertRow(0) 

ls_incoming = message.stringparm

if iw_frame.iu_string.nf_amIempty(ls_incoming) Then
	dw_1.SetItem( 1, "from_char", "A")
   dw_1.SetItem( 1, "to_char", "Z")
Else
	lc_start = iw_frame.iu_string.nf_GetToken(ls_incoming, '~t')
   lc_end = iw_frame.iu_string.nf_GetToken(ls_incoming, '~t')
   is_division = iw_frame.iu_string.nf_GetToken(ls_incoming, '~t')
	is_complex = iw_frame.iu_string.nf_GetToken(ls_incoming, '~t')
	is_plant = iw_frame.iu_string.nf_GetToken(ls_incoming, '~t')
	is_carrier = iw_frame.iu_string.nf_GetToken(ls_incoming, '~t')
	
   dw_1.SetItem( 1, "from_char", lc_start)
   dw_1.SetItem( 1, "to_char", lc_end)
	dw_division.setitem(1,"division_code",is_division )
	dw_complex.setitem(1, "complex", is_complex)
	dw_plants.setitem(1, "plant_code", is_plant)
	dw_carriers.setitem(1, "carrier_code", is_carrier)
End if

dw_1.SelectText(1, 1)
dw_1.SetFocus()

end event

on w_appt_exception_filter.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.dw_division=create dw_division
this.dw_complex=create dw_complex
this.dw_plants=create dw_plants
this.dw_carriers=create dw_carriers
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.dw_division
this.Control[iCurrent+3]=this.dw_complex
this.Control[iCurrent+4]=this.dw_plants
this.Control[iCurrent+5]=this.dw_carriers
this.Control[iCurrent+6]=this.gb_1
end on

on w_appt_exception_filter.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.dw_division)
destroy(this.dw_complex)
destroy(this.dw_plants)
destroy(this.dw_carriers)
destroy(this.gb_1)
end on

event ue_base_cancel;call super::ue_base_cancel;String     ls_filter
			  
ls_filter = Space(1)

CloseWithReturn( This, ls_filter)

Return
 

end event

event ue_base_ok;call super::ue_base_ok;Character    lc_start, &
             lc_end


String       ls_exception_filter

If dw_plants.AcceptText() = 1 Then
	is_plant = dw_plants.GetItemString(1 , "plant_code")
Else
	is_plant = Space(3)
End If
SetProfileString( iw_frame.is_UserINI, "OTR", "lastplant", is_plant)

If dw_complex.AcceptText() = 1 Then
	is_complex = dw_complex.GetItemString(1 , "complex")
Else
	is_complex = Space(3)
End If
SetProfileString( iw_frame.is_UserINI, "OTR", "lastcomplex", is_complex)

IF dw_division.accepttext() = 1 Then
	is_division = dw_division.GetItemString(1, "division_code")
Else
	is_division = Space(2)
End If
SetProfileString( iw_frame.is_UserINI, "OTR", "lastdivision", is_division)

If dw_carriers.AcceptText() = 1 Then
	is_carrier = dw_carriers.GetItemString(1 , "carrier_code")
Else
	is_carrier = Space(4)
End If
SetProfileString( iw_frame.is_UserINI, "OTR", "lastcarrier", is_carrier)

If dw_1.AcceptText() = 1 Then
   lc_start = dw_1.object.from_char[1]
   lc_end = dw_1.object.to_char[1]
	If Trim(lc_end) = "" Then
		MessageBox("Information", "End character is required, please enter a letter.")
		dw_1.SetFocus()
		dw_1.SetColumn("to_char")
		Return
	End If
	
   ls_exception_filter = lc_start + '~t' + lc_end + '~t' + is_division + '~t' + is_complex + '~t' + is_plant + '~t' + is_carrier
   CloseWithReturn( This, 	ls_exception_filter )
Else
   MessageBox("Customer Select Characters", "Please enter Customer Select Characters.") 
   Return
End if










end event

type cb_base_help from w_base_response_ext`cb_base_help within w_appt_exception_filter
integer x = 677
integer y = 640
integer taborder = 80
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_appt_exception_filter
integer x = 366
integer y = 640
integer taborder = 70
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_appt_exception_filter
integer x = 55
integer y = 640
integer taborder = 60
end type

type dw_1 from u_base_dw_ext within w_appt_exception_filter
integer x = 91
integer y = 116
integer width = 814
integer height = 88
integer taborder = 10
string dataobject = "d_appt_exception_hdr"
boolean border = false
end type

type dw_division from datawindow within w_appt_exception_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 55
integer y = 432
integer width = 398
integer height = 196
integer taborder = 40
boolean bringtotop = true
string title = "none"
string dataobject = "d_dddw_divisions"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_dwndropdown;Long ll_row

ll_row = idwc_division.Find( "type_code ='" + GetText() + "'", 1, idwc_division.RowCount() )

If (ll_row = 0) Then
	idwc_division.SetTrans(SQLCA)
	idwc_division.Retrieve('DIVCODE')
Else 
	idwc_division.ScrollToRow( ll_row )
	idwc_division.SelectRow( ll_row, True )
End If


end event

event constructor;String 		ls_value

ii_rc = GetChild( 'division_code', idwc_division )
IF ii_rc = -1 THEN 
	MessageBox("DataWindwoChild Error", "Unable to get Child Handle for division_code.")
else
	idwc_division.Reset()
	idwc_division.SetTrans(SQLCA)
	idwc_division.Retrieve('DIVCODE')
	dw_division.insertRow(0)

	ls_value = ProfileString(iw_frame.is_userini, "OTR", "lastdivision", "") 
	dw_division.SetItem( 1, "division_code", ls_value)

End IF


end event

event getfocus;THIS.SelectText (1,99)
end event

event itemchanged;long ll_FoundRow
iw_frame.SetMicroHelp(dwo.name)
IF Trim(data) = "" THEN 
	is_division = space(2)
	Return
End if

ll_FoundRow = idwc_division.Find ( "type_code='"+Trim(data)+"'", 1, idwc_division.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Division Error", data + " is Not a Valid Division Code." )
	SelectText(1,2)
	Return 1
Else
	is_division = data
END IF

SetProfileString( iw_frame.is_UserINI, "OTR", "lastdivision",data)
end event

event itemerror;Return 1
end event

event itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)

end event

type dw_complex from u_base_dw_ext within w_appt_exception_filter
integer x = 645
integer y = 284
integer width = 302
integer height = 148
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_dddw_complex"
boolean border = false
end type

event constructor;call super::constructor;String 		ls_value

ii_rc = GetChild( 'complex', idwc_complex )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for complex.")
idwc_complex.Reset()
idwc_complex.SetTrans(SQLCA)
idwc_complex.Retrieve()

dw_complex.insertrow(0)

ls_value = ProfileString(iw_frame.is_userini, "OTR", "lastcomplex", "") 

dw_complex.SetItem( 1, "complex", ls_value)

end event

event getfocus;call super::getfocus;THIS.SelectText (1,99)
end event

event itemchanged;call super::itemchanged;long ll_FoundRow

iw_frame.SetMicroHelp(dwo.name)

IF Trim(data) = "" THEN
	is_complex = space(3)
	Return
END IF

ll_FoundRow = idwc_complex.Find ( "complex_code='"+data+"'", 1, idwc_complex.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Complex Error", data + " is Not a Valid Complex Code." )
	SelectText(1,3)
	Return 1
ELSE
	is_complex = data
END IF		

SetProfileString( iw_frame.is_UserINI, "OTR", "lastcomplex",data)
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)

end event

type dw_plants from u_base_dw_ext within w_appt_exception_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 55
integer y = 256
integer width = 425
integer height = 164
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_dddw_plants"
boolean border = false
end type

event ue_dwndropdown;Long ll_row

ll_row = idwc_plant.Find( "location_code ='" + GetText() + "'", 1, idwc_plant.RowCount() )

If (ll_row = 0) Then
	idwc_plant.SetTrans( SQLCA )
	idwc_plant.Retrieve()
Else 
	idwc_plant.ScrollToRow( ll_row )
	idwc_plant.SelectRow( ll_row, True )
End If
end event

event constructor;call super::constructor;String 		ls_value

ii_rc = GetChild( 'plant_code', idwc_plant )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for plant_code.")

dw_plants.insertrow(0)

ls_value = ProfileString(iw_frame.is_userini, "OTR", "lastplant", "") 

dw_plants.SetItem( 1, "plant_code", ls_value)

end event

event destructor;call super::destructor;idwc_plant.setfilter("")
idwc_plant.filter()
end event

event itemchanged;call super::itemchanged;long ll_FoundRow

integer	li_rtn

string ls_complex,ls_search

dwitemstatus l_status

is_plant = Trim(data)

SetProfileString( iw_frame.is_UserINI, "OTR", "lastplant",data)
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)




end event

type dw_carriers from u_base_dw_ext within w_appt_exception_filter
integer x = 562
integer y = 436
integer width = 384
integer height = 180
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_dddw_carriers"
boolean border = false
end type

event constructor;call super::constructor;String 		ls_value


ii_rc = GetChild( 'carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for carrier_code.")
idwc_carrier.Reset()
idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()

dw_carriers.insertRow(0)

ls_value = ProfileString(iw_frame.is_userini, "OTR", "lastcarrier", "") 

dw_carriers.SetItem( 1, "carrier_code", ls_value)

end event

event itemchanged;call super::itemchanged;long ll_FoundRow

iw_frame.SetMicroHelp(dwo.name)

IF Trim(data) = "" THEN 
	is_carrier = Space(4)
	Return
End If

ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Carrier Error", data + " is Not a Valid Carrier Code." )
	SelectText(1,4)
	Return 1
Else
	is_carrier = data
END IF		

SetProfileString(iw_frame.is_UserINI, "OTR", "lastcarrier",data)
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)



end event

event getfocus;call super::getfocus;THIS.SelectText (1,99)
end event

type gb_1 from groupbox within w_appt_exception_filter
integer x = 55
integer y = 32
integer width = 891
integer height = 208
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Customer Select Character"
end type

