HA$PBExportHeader$w_appt_review_queue.srw
$PBExportComments$SR1540 - Appointment Process Improvement.
forward
global type w_appt_review_queue from w_base_sheet_ext
end type
type dw_review_queue_hdr from u_base_dw_ext within w_appt_review_queue
end type
type dw_review_queue from u_base_dw_ext within w_appt_review_queue
end type
end forward

global type w_appt_review_queue from w_base_sheet_ext
integer width = 2661
integer height = 1368
string title = "Appointment Review Queue"
boolean resizable = false
long backcolor = 12632256
dw_review_queue_hdr dw_review_queue_hdr
dw_review_queue dw_review_queue
end type
global w_appt_review_queue w_appt_review_queue

type variables
String	is_customer, is_sales_order_id, is_complex, is_plant, is_appt_id
Date		idt_appt_date
int 		ii_commhandle

s_error	istr_error_info
u_otr006 iu_otr006
u_otr002	iu_otr002
u_ws_appointments iu_ws_appointments
end variables

forward prototypes
public function boolean wf_check_for_changes ()
public function integer wf_load_review_queue ()
public function boolean wf_ck_review_applied_deletes ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_check_for_changes ();Call w_base_sheet::closequery
If Message.ReturnValue = 1 Then return false

return True
end function

public function integer wf_load_review_queue ();integer  li_rtn

string   ls_input, ls_output

s_error  lstr_Error_Info

dw_review_queue_hdr.Reset()
dw_review_queue.Reset()

lstr_error_info.se_event_name = "wf_retrieve"
lstr_error_info.se_window_name = "w_review_queue"
lstr_error_info.se_procedure_name = "wf_retrieve"
lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_message = Space(71)

ls_input = is_customer + '~t' + is_sales_order_id + '~t' + is_complex + '~t' + is_plant + '~t' + String(idt_appt_date) + '~t' + is_appt_id + '~r~n'

//li_rtn = iu_otr006.nf_otrt77ar_inq_order_appt(lstr_error_info, ls_input, ls_output)
li_rtn = iu_ws_appointments.uf_otrt77er(lstr_error_info, ls_input, ls_output)
li_rtn = dw_review_queue.ImportString(Trim(ls_output))
dw_review_queue.SetFocus()
dw_review_queue.ResetUpdate()

Return 1



end function

public function boolean wf_ck_review_applied_deletes ();Long			ll_rowcount, ll_count
String		ls_update_ind, ls_status_code
Boolean		lb_error_flag

ll_rowcount = dw_review_queue.RowCount()
ll_count = +1

Do until ll_count > ll_rowcount
	ls_update_ind = dw_review_queue.GetItemString ( ll_count, "update_ind" )
	If ls_update_ind = 'D' Then
		ls_status_code = dw_review_queue.GetItemString( ll_count, "status_code" )
		If ls_status_code = 'R' Then 
			dw_review_queue.SelectRow(ll_count, True)
			lb_error_flag = False
		End If
	End If
	
	ll_count = ll_count +1
Loop

If lb_error_flag = True Then
	Return False
Else
	Return True
End If

end function

public function boolean wf_retrieve ();String	ls_input, ls_output, ls_sales_order_id, ls_customer, ls_complex, ls_plant, ls_appt_id, ls_appt_date, ls_title_input
Integer	li_rtn
Date 		Current, ldt_appt_date
Long		ll_rowcount, ll_count

Current = today()

If Not wf_Check_for_changes() Then return false

If IsNull(dw_review_queue_hdr.GetItemString( 1, "sales_order_id")) = False Then 
	ls_sales_order_id = dw_review_queue_hdr.GetItemString( 1, "sales_order_id")
End If

If IsNull(dw_review_queue_hdr.GetItemString( 1, "customer_code")) = False Then
	ls_customer  = dw_review_queue_hdr.GetItemString( 1, "customer_code")
End If

If IsNull(dw_review_queue_hdr.GetItemString( 1, "complex_code")) = False Then
	ls_complex  = dw_review_queue_hdr.GetItemString( 1, "complex_code")
End If

If IsNull(dw_review_queue_hdr.GetItemString( 1, "plant_code")) = False Then
	ls_plant  = dw_review_queue_hdr.GetItemString( 1, "plant_code")
End If

If IsNull(dw_review_queue_hdr.GetItemDate( 1, "appt_date")) = False Then
	ldt_appt_date  = dw_review_queue_hdr.GetItemDate( 1, "appt_date")
End If

If IsNull(dw_review_queue_hdr.GetItemString( 1, "appt_id")) = False Then
	ls_appt_id  = dw_review_queue_hdr.GetItemString( 1, "appt_id")
End If

OpenWithParm( w_appt_review_queue_filter, This, iw_frame )
IF Message.StringParm = "Cancel" then return True

// Turn the redraw off for the window while we retrieve
This.SetRedraw(FALSE)

// Call RPC for data
ls_input = 	dw_review_queue_hdr.GetItemString( 1, "customer_code") + '~t' + &
				dw_review_queue_hdr.GetItemString( 1, "sales_order_id") + '~t' + &
				dw_review_queue_hdr.GetItemString( 1, "complex_code") + '~t' + &
				dw_review_queue_hdr.GetItemString( 1, "plant_code") + '~t' + &
				String(dw_review_queue_hdr.GetItemDate( 1, "appt_date"), "YYYY-MM-DD") + '~t' + &
				dw_review_queue_hdr.GetItemString( 1, "appt_id") + '~t'

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_app_name = "OTR"
istr_error_info.se_function_name = "nf_otrt77ar_inq_order_appt"
istr_error_info.se_window_name = "w_review_queue"

//li_rtn = iu_otr006.nf_otrt77ar_inq_order_appt(istr_error_info, ls_input, ls_output)
li_rtn = iu_ws_appointments.uf_otrt77er(istr_error_info, ls_input, ls_output)

dw_review_queue.Reset()
dw_review_queue.ImportString(ls_output)

ls_title_input = "Appointments for"

If IsNull(dw_review_queue_hdr.GetItemString( 1, "sales_order_id")) = False Then 
	ls_sales_order_id = dw_review_queue_hdr.GetItemString( 1, "sales_order_id")
	If ls_sales_order_id  >  '       ' Then
		ls_title_input = ls_title_input + ", Sales Order = " + ls_sales_order_id
	End If
End If

If IsNull(dw_review_queue_hdr.GetItemString( 1, "customer_code")) = False Then
	ls_customer  = dw_review_queue_hdr.GetItemString( 1, "customer_code")
	If ls_customer > '       ' Then
		ls_title_input = ls_title_input + ", Customer = " + ls_customer
	End If
End If

If IsNull(dw_review_queue_hdr.GetItemString( 1, "complex_code")) = False Then
	ls_complex  = dw_review_queue_hdr.GetItemString( 1, "complex_code")
	If ls_complex > '        ' Then
		ls_title_input = ls_title_input + ", Complex = " + ls_complex
	End If
End If

If IsNull(dw_review_queue_hdr.GetItemString( 1, "plant_code")) = False Then
	ls_plant  = dw_review_queue_hdr.GetItemString( 1, "plant_code")
	If ls_plant > '   ' Then
		ls_title_input = ls_title_input + ", Plant = " + ls_plant
	End If
End If

If IsNull(dw_review_queue_hdr.GetItemDate( 1, "appt_date")) = False Then
	ls_appt_date  = String(dw_review_queue_hdr.GetItemDate( 1, "appt_date"), "YYYY-MM-DD")
	If ls_appt_date > '0001-01-01' Then
		ls_title_input = ls_title_input + ", Appt Date = " + String(dw_review_queue_hdr.GetItemDate( 1, "appt_date"), "YYYY-MM-DD")
	End If
End If

If IsNull(dw_review_queue_hdr.GetItemString( 1, "appt_id")) = False Then
	ls_appt_id  = dw_review_queue_hdr.GetItemString( 1, "appt_id")
	If ls_appt_id > '               ' Then
		ls_title_input = ls_title_input + ", Appt ID = " + ls_appt_id
	End If
End If
//
This.Title = ls_title_input

dw_review_queue.ResetUpdate()
dw_review_queue.SetRedraw(True)
dw_review_queue.ResetUpdate()

// Turn the redraw off for the window while we retrieve
This.SetRedraw(TRUE)

Return FALSE

end function

public function boolean wf_update ();
/* Modification History

Ticket No   Author	     Date         Description
====================================================================================
459885      KUPPUSAMYC    09/2/2005    While deleting "Sales Order" system try to updating 
													the deleted record "error_ind" column value,changed 
													the logic system should update "error_ind" column 
													value for only UPDATE process
  

wr9051     willemst       09/21/2009   add appt contact & remove approval ind
*/ 


Integer		li_rtn, li_return_code
Long			ll_rowcount, ll_count, ll_dw_rowcount, ll_dw_count
String		ls_header, ls_detail, ls_output, ls_load_key, ls_dw_order_number, ls_ds_order_number, ls_ds_error_ind, ls_stop_code, ls_appt_date, ls_appt_time, ls_appt_contact, ls_eta_date, ls_eta_time, ls_override_ind, ls_notify_carrier, ls_return_msg, ls_debug
Boolean		lb_error_ind, lb_record_found, lb_UpdateInd_errors
DataStore	lds_datastore
String ls_string
lb_UpdateInd_errors = False

li_rtn = dw_review_queue.AcceptText()

If wf_ck_review_applied_deletes() = False Then
	MessageBox("Invalid Selection", String("Unable to delete records in Review or Applied status."))
	Return False
End If

ll_rowcount = dw_review_queue.RowCount()
ll_count = +1

Do until ll_count > ll_rowcount
	
	If (dw_review_queue.GetItemStatus(ll_count, 0, Primary!) = DataModified! and + &
			(dw_review_queue.GetItemString( ll_count, "update_ind") <> 'U' and dw_review_queue.GetItemString( ll_count, "update_ind") <> 'D' and + & 
			  dw_review_queue.GetItemString(ll_count, "update_ind") <> ' ')) Then
		dw_review_queue.SetItem(ll_count, "error_ind", 'Y')
		lb_UpdateInd_errors = True
	Else
		If dw_review_queue.GetItemString( ll_count, "update_ind") = 'U' Then 
			ls_detail += dw_review_queue.GetItemString(ll_count, "load_key") + '~t' + &
							 dw_review_queue.GetItemString(ll_count, "complex_code") + '~t' + &
	 						 dw_review_queue.GetItemString(ll_count, "appointment_id") + '~t' + &
							 dw_review_queue.GetItemString(ll_count, "sales_order_id") + '~t' + &
							 dw_review_queue.GetitemString(ll_count, "customer_name") + '~t' + &
							 String(dw_review_queue.GetItemDate(ll_count, "appt_date"), "YYYY-MM-DD") + '~t' + &
							 String(dw_review_queue.GetItemTime(ll_count, "appt_time"), "HH:MM:SS") + '~t' + &
							 String(dw_review_queue.GetItemDate(ll_count, "update_date"), "YYYY-MM-DD") + '~t' + &
							 String(dw_review_queue.GetItemTime(ll_count, "update_time"), "HH:MM:SS") + '~t' + &
							 dw_review_queue.GetItemString(ll_count, "update_ind") + '~t' + &
							 dw_review_queue.GetItemString(ll_count, "status_code") + '~t' + &
							 dw_review_queue.GetItemString(ll_count, "error_ind") + '~t' + &
 							 dw_review_queue.GetitemString(ll_count, "contact_name") + '~r~n'
		End If
		
		If	dw_review_queue.GetItemString( ll_count, "update_ind") = 'D' Then
			ls_detail += dw_review_queue.GetItemString(ll_count, "load_key") + '~t' + &
							 dw_review_queue.GetItemString(ll_count, "complex_code") + '~t' + &
	 						 dw_review_queue.GetItemString(ll_count, "appointment_id") + '~t' + &
							 dw_review_queue.GetItemString(ll_count, "sales_order_id") + '~t' + &
							 dw_review_queue.GetitemString(ll_count, "customer_name") + '~t' + &
							 String(dw_review_queue.GetItemDate(ll_count, "appt_date"), "YYYY-MM-DD") + '~t' + &
							 String(dw_review_queue.GetItemTime(ll_count, "appt_time"), "HH:MM:SS") + '~t' + &
							 String(dw_review_queue.GetItemDate(ll_count, "update_date"), "YYYY-MM-DD") + '~t' + &
							 String(dw_review_queue.GetItemTime(ll_count, "update_time"), "HH:MM:SS") + '~t' + &
							 dw_review_queue.GetItemString(ll_count, "update_ind") + '~t' + &
							 dw_review_queue.GetItemString(ll_count, "status_code") + '~t' + &
							 dw_review_queue.GetItemString(ll_count, "error_ind") + '~t' + &
 							 dw_review_queue.GetitemString(ll_count, "contact_name") + '~r~n'

		End If
	end if		
	ll_count = ll_count +1
Loop

If lb_UpdateInd_errors = True Then
	Messagebox("Update Issue", "Update Indicator must be set for changes to be saved.")
	Return False
End If

ls_header = dw_review_queue_hdr.GetItemString( 1, "customer_code") + '~t' + &
			  	dw_review_queue_hdr.GetItemString( 1, "sales_order_id") + '~t' + &
			  	dw_review_queue_hdr.GetItemString( 1, "complex_code") + '~t' + &
			  	dw_review_queue_hdr.GetItemString( 1, "plant_code") + '~t' +&
			  	String(dw_review_queue_hdr.GetItemDate( 1, "appt_date"), "YYYY-MM-DD") + '~t' + &
			  	dw_review_queue_hdr.getitemstring( 1, "appt_id") + '~r~n'

This.SetRedraw(False)
SetPointer(HourGlass!)

//li_rtn = iu_otr006.nf_otrt78ar_upd_order_appt( istr_error_info, ls_header, ls_detail, ls_output)
li_rtn = iu_ws_appointments.uf_otrt78er(istr_error_info, ls_header, ls_detail, ls_output)

lds_Datastore = Create datastore
lds_Datastore.DataObject = 'd_review_queue_dtl'
lds_datastore.ImportString(ls_output)

ls_debug = lds_datastore.describe("Datastore.Data")

lds_datastore.SetSort("1 A, 2 A, 5 A")
lds_datastore.Sort()

//  Check error_ind for further processing.
ll_rowcount = lds_datastore.RowCount()
ll_count = +1

Do until ll_count > ll_rowcount
	ls_ds_error_ind = lds_datastore.GetItemString( ll_count, 13)
	
	If	ls_ds_error_ind = 'Y' Then
		
		ll_dw_rowcount = dw_review_queue.RowCount()
		ll_dw_count = +1
		lb_record_found = False
		ls_ds_order_number = lds_datastore.GetItemString( ll_count, 4)
		
		Do Until ll_dw_count >  ll_dw_rowcount or lb_record_found = True
			ls_dw_order_number = dw_review_queue.GetItemString( ll_dw_count, "sales_order_id")
			
			If ls_dw_order_number = ls_ds_order_number Then
				dw_review_queue.SetItem( ll_dw_count, "error_ind", 'Y')
				lb_record_found = True
			End If
			
			ll_dw_count = ll_dw_count +1
		Loop
	End If

	ll_count = ll_count +1
Loop
//ll_rowcount = lds_datastore.RowCount()
//ll_count = +1
//
//Do until ll_count > ll_rowcount
//	If	lds_datastore.GetItemString( ll_count, 13) = 'E' Then
//		lds_datastore.SelectRow(ll_count, True)
//		lb_error_ind = True
//	Else
//		If lds_datastore.GetItemString( ll_count, 13) = 'S' Then
//			ls_load_key = lds_datastore.GetItemString( ll_count, 5)
//			ls_stop_code = lds_datastore.GetItemString( ll_count, 6)
//			ls_appt_date = String(lds_datastore.GetItemDate( ll_count, 7))
//			ls_appt_time = String(lds_datastore.GetItemTime( ll_count, 8))
//			If ls_load_key > ' ' Then
//				li_rtn = iu_otr002.nf_otrt19ar(ls_load_key, ls_stop_code, ls_appt_contact, ls_appt_date, ls_appt_time, ls_eta_date, ls_eta_time, ls_override_ind, ls_notify_carrier, li_return_code, ls_return_msg, istr_error_info, ii_commhandle)
//			End If
//		End If
//	End If
//
//	ll_count = ll_count +1
//Loop


//  Reset everything because everything passed through update. < Clean Up Records >
ll_rowcount = dw_review_queue.RowCount()
ll_count = +1

Do until ll_count > ll_rowcount
	dw_review_queue.SelectRow(ll_count, False)
	If	dw_review_queue.GetItemString( ll_count, "update_ind") = 'U' Then
		dw_review_queue.SetItem( ll_count, "update_ind", ' ')
		// Added for HD0000000459885
		If dw_review_queue.GetItemString ( ll_count, "error_ind" ) > ' ' Then
			dw_review_queue.SetItem( ll_count, "error_ind", ' ' )
		End If
		ll_count = ll_count +1
		// End changes for HD0000000459885

	Else
		If dw_review_queue.GetItemString( ll_count, "update_ind") = 'D' Then
			dw_review_queue.DeleteRow(ll_count)
			ll_rowcount = dw_review_queue.RowCount()
		// Added for HD0000000459885
		Else
			If dw_review_queue.GetItemString ( ll_count, "error_ind" ) > ' ' Then
				dw_review_queue.SetItem( ll_count, "error_ind", ' ' )
			End If
			ll_count = ll_count +1
		// End changes for HD0000000459885
		End If
	End If
	// Commented for HD0000000459885
//	If dw_review_queue.GetItemString ( ll_count, "error_ind" ) > ' ' Then
//		dw_review_queue.SetItem( ll_count, "error_ind", ' ' )
//	End If
//
//	ll_count = ll_count +1
	// End changes for HD0000000459885
Loop

If lb_error_ind = True Then
	MessageBox("Update Issue", String("Order needs to be approved before updating."))
	Return False
End If

dw_review_queue.SetFocus()
dw_review_queue.ResetUpdate()
This.SetRedraw(True)
SetPointer(Arrow!)																		

Return True

end function

on w_appt_review_queue.create
int iCurrent
call super::create
this.dw_review_queue_hdr=create dw_review_queue_hdr
this.dw_review_queue=create dw_review_queue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_review_queue_hdr
this.Control[iCurrent+2]=this.dw_review_queue
end on

on w_appt_review_queue.destroy
call super::destroy
destroy(this.dw_review_queue_hdr)
destroy(this.dw_review_queue)
end on

event open;call super::open;dw_review_queue_hdr.InsertRow(0)
end event

event close;call super::close;//DESTROY iu_otr006
//DESTROY iu_otr002
DESTROY iu_ws_appointments
end event

event ue_postopen;call super::ue_postopen;//iu_otr006 = CREATE u_otr006
//iu_otr002 = CREATE u_otr002
iu_ws_appointments = CREATE u_ws_appointments

IF wf_retrieve() THEN Close( this )

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'sales_order_id'
		dw_review_queue_hdr.SetItem( 1, "sales_order_id", as_value)
	Case 'customer_code'
		dw_review_queue_hdr.SetItem( 1, "customer_code", as_value)
	Case 'complex_code'
		dw_review_queue_hdr.SetItem( 1, "complex_code", as_value)
	Case 'plant_code'
		dw_review_queue_hdr.SetItem( 1, "plant_code", as_value)
	Case 'appt_id'
		dw_review_queue_hdr.SetItem( 1, "appt_id", as_value)
	Case 'appt_date'
		dw_review_queue_hdr.SetItem( 1, "appt_date", Date(as_value))
End Choose
end event

type dw_review_queue_hdr from u_base_dw_ext within w_appt_review_queue
boolean visible = false
integer x = 9
integer y = 1068
integer width = 2185
integer height = 164
integer taborder = 0
string dataobject = "d_review_queue_hdr"
boolean border = false
end type

type dw_review_queue from u_base_dw_ext within w_appt_review_queue
integer x = 14
integer width = 2619
integer height = 1244
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_review_queue_dtl"
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = styleraised!
end type

event clicked;call super::clicked;String          ls_load_key, &
                ls_input_string

w_load_detail    lw_load_detail 

is_ObjectAtPointer = GetObjectAtPointer()
is_ObjectAtPointer = Left( is_ObjectAtPointer, Pos( is_ObjectAtPointer, "~t") - 1 )

If is_ObjectAtPointer  = "load_key" Then
   
   ls_load_key = This.GetItemString(row, "load_key")
	if trim(ls_load_key) <> "" then
	   ls_input_string = ls_load_key
	   OpenSheetWithParm(lw_load_detail, ls_input_string, iw_frame, 0 , Original!)     
	end if
End if

end event

event doubleclicked;String          ls_load_key, &
                ls_input_string

w_load_detail    lw_load_detail 

is_ObjectAtPointer = GetObjectAtPointer()
is_ObjectAtPointer = Left( is_ObjectAtPointer, Pos( is_ObjectAtPointer, "~t") - 1 )

If is_ObjectAtPointer  = "load_key" Then
   
   ls_load_key = This.GetItemString(row, "load_key")
	if trim(ls_load_key) <> "" then
	   ls_input_string = ls_load_key
	   OpenSheetWithParm(lw_load_detail, ls_input_string, iw_frame, 0 , Original!)     
	end if
End if

end event

