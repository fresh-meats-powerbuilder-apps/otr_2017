HA$PBExportHeader$w_appt_review_queue_filter.srw
$PBExportComments$SR1540 - Appointment Process Improvement.
forward
global type w_appt_review_queue_filter from w_base_response_ext
end type
type dw_shipto_cust from u_base_dw_ext within w_appt_review_queue_filter
end type
type dw_plants from u_base_dw_ext within w_appt_review_queue_filter
end type
type dw_complex from u_base_dw_ext within w_appt_review_queue_filter
end type
type dw_appt_id from u_base_dw_ext within w_appt_review_queue_filter
end type
type dw_appt_date from u_base_dw_ext within w_appt_review_queue_filter
end type
type st_1 from statictext within w_appt_review_queue_filter
end type
type dw_sales_order_id from u_base_dw_ext within w_appt_review_queue_filter
end type
end forward

global type w_appt_review_queue_filter from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 1765
integer height = 552
string title = "Appointment Review Queue"
long backcolor = 12632256
boolean clientedge = true
dw_shipto_cust dw_shipto_cust
dw_plants dw_plants
dw_complex dw_complex
dw_appt_id dw_appt_id
dw_appt_date dw_appt_date
st_1 st_1
dw_sales_order_id dw_sales_order_id
end type
global w_appt_review_queue_filter w_appt_review_queue_filter

type variables
integer				ii_rc

String				is_sales_order_id, &
						is_complex, &
						is_shipto_cust, &
						is_cancel, &
						is_plant, &
						is_appt_id

Date					idt_appt_date

DataWindowChild	idwc_plant, &
						idwc_complex, &
						idwc_shipto

w_appt_review_queue		iw_parentwindow
u_validate_customer		iu_validate_customer
end variables

forward prototypes
public subroutine wf_shipto_retrieve ()
end prototypes

public subroutine wf_shipto_retrieve ();
ii_rc = dw_shipto_cust.GetChild( 'shipto_customer', idwc_shipto )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for shipto_customer.")
idwc_shipto.Reset()
idwc_shipto.SetTrans(SQLCA)
idwc_shipto.Retrieve()
end subroutine

event open;call super::open;
iw_parentwindow = Message.PowerObjectParm
iu_validate_customer = CREATE u_validate_customer

end event

event ue_base_ok;call super::ue_base_ok;string		ls_sales_order_id, &
				ls_complex, &
				ls_shipto_cust, &
				ls_plant, &
				ls_appt_id, &
				ls_date

Date			ldt_appt_date

If dw_sales_order_id.AcceptText() = 1 then
	is_sales_order_id = dw_sales_order_id.GetItemString(1 , "sales_order_id")
//	SetProfileString( iw_frame.is_UserINI, "OTR", "lastsalesorderid", is_sales_order_id)
End If

If dw_shipto_cust.AcceptText() = 1 then
	is_shipto_cust = dw_shipto_cust.GetItemString(1 , "shipto_customer")
//	SetProfileString( iw_frame.is_UserINI, "OTR", "lastshiptocustomer", is_shipto_cust)
Else
	Return
End If

If dw_complex.AcceptText() = 1 then
	is_complex = dw_complex.GetItemString(1 , "complex")
//	SetProfileString( iw_frame.is_UserINI, "OTR", "lastcomplex", is_complex)
End If

If dw_plants.AcceptText() = 1 then
	is_plant = dw_plants.GetItemString(1 , "plant_code")
//	SetProfileString( iw_frame.is_UserINI, "OTR", "lastplant", is_plant)
Else
	Return
End If

If dw_appt_date.AcceptText() = 1 then
	idt_appt_date = dw_appt_date.GetItemDate(1 , "work_date")
	ldt_appt_date = idt_appt_date
	ls_date = String(ldt_appt_date,"MM/DD/YYYY") 
//	SetProfileString( iw_frame.is_UserINI, "OTR", "Lastapptdate", ls_date)
Else
	If ldt_appt_date = Date('1/1/1900') Then
		idt_appt_date = Date('01/01/0001')
		ldt_appt_date = Date('01/01/0001')
		ls_date = '01/01/0001'
//		SetProfileString( iw_frame.is_UserINI, "OTR", "Lastapptdate", ls_date)
	End If
End If

If dw_appt_id.AcceptText() = 1 Then
	is_appt_id = dw_appt_id.GetItemString(1, "appt_id")
//	SetProfileString( iw_frame.is_UserINI, "OTR", "lastapptid", is_appt_id )
End If

If is_shipto_cust > "       " Then
	ls_shipto_cust = is_shipto_cust
Else
	ls_shipto_cust = "       "
End If
If IsNull(ls_shipto_cust) Then
	ls_shipto_cust = "       "
End If


If is_sales_order_id > "    " Then
	ls_sales_order_id = is_sales_order_id
Else
	ls_sales_order_id = "    "
End If
If IsNull(ls_sales_order_id) Then
	ls_sales_order_id = "    "
End If


If is_complex > "   " Then
	ls_complex = is_complex
Else
	ls_complex = "   "
End If
If IsNull(ls_complex) Then
	ls_complex = "   "
End If


If is_plant > "   " Then
	ls_plant = is_plant
Else
	ls_plant = "   "
End If
If IsNull(ls_plant) Then
	ls_plant = "   "
End If

If is_appt_id > "               " Then
	ls_appt_id = is_appt_id
Else
	ls_appt_id = "               "
End If
If IsNull(ls_appt_id) Then
	ls_appt_id = "               "
End If

iw_parentwindow.Event ue_set_data('sales_order_id',ls_sales_order_id)
iw_parentwindow.Event ue_set_data('customer_code' ,ls_shipto_cust)
iw_parentwindow.Event ue_set_data('complex_code' ,ls_complex)
iw_parentwindow.Event ue_set_data('plant_code' ,ls_plant)
iw_parentwindow.Event ue_set_data('appt_date' , String(ldt_appt_date, 'MM/DD/YYYY'))
iw_parentwindow.Event ue_set_data('appt_id' ,ls_appt_id)

CloseWithReturn(This, "OK")
Return

end event

on w_appt_review_queue_filter.create
int iCurrent
call super::create
this.dw_shipto_cust=create dw_shipto_cust
this.dw_plants=create dw_plants
this.dw_complex=create dw_complex
this.dw_appt_id=create dw_appt_id
this.dw_appt_date=create dw_appt_date
this.st_1=create st_1
this.dw_sales_order_id=create dw_sales_order_id
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_shipto_cust
this.Control[iCurrent+2]=this.dw_plants
this.Control[iCurrent+3]=this.dw_complex
this.Control[iCurrent+4]=this.dw_appt_id
this.Control[iCurrent+5]=this.dw_appt_date
this.Control[iCurrent+6]=this.st_1
this.Control[iCurrent+7]=this.dw_sales_order_id
end on

on w_appt_review_queue_filter.destroy
call super::destroy
destroy(this.dw_shipto_cust)
destroy(this.dw_plants)
destroy(this.dw_complex)
destroy(this.dw_appt_id)
destroy(this.dw_appt_date)
destroy(this.st_1)
destroy(this.dw_sales_order_id)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_appt_review_queue_filter
integer x = 1454
integer y = 304
integer taborder = 90
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_appt_review_queue_filter
integer x = 1454
integer y = 160
integer taborder = 80
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_appt_review_queue_filter
integer x = 1454
integer y = 12
integer taborder = 70
end type

type dw_shipto_cust from u_base_dw_ext within w_appt_review_queue_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 23
integer y = 24
integer width = 535
integer height = 156
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_dddw_shipto_cust"
boolean border = false
end type

event ue_dwndropdown;wf_shipto_retrieve()
end event

event constructor;call super::constructor;String 		ls_value

dw_shipto_cust.insertrow(0)

//ls_value = ProfileString(iw_frame.is_userini, "OTR", "lastshiptocustomer", "") 
//
//dw_shipto_cust.SetItem( 1, "shipto_customer", ls_value)
//
end event

event getfocus;call super::getfocus;THIS.SelectText (1,99)
end event

event itemchanged;call super::itemchanged;String 	ls_customer_type, ls_customer_id
Long 		ll_nbr, ll_foundrow
Integer 	rtn
Boolean 	lb_rtn

ls_customer_type	= 'S'
ls_customer_id		= data
	
if ls_customer_id <> "" then
	lb_rtn = iu_validate_customer.nf_validate_customer(ls_customer_type,ls_customer_id)
	if lb_rtn = false then
		messagebox("Message",ls_customer_id + " is not a valid customer ")
		this.settext("")
		return 1
	else
		is_shipto_cust = ls_customer_id
	end if
else
	is_shipto_cust = ls_customer_id
end if

//SetProfileString( iw_frame.is_UserINI, "OTR", "lastshiptocustomer",data)
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)
end event

type dw_plants from u_base_dw_ext within w_appt_review_queue_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 599
integer y = 24
integer width = 453
integer height = 140
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_dddw_plants"
boolean border = false
end type

event ue_dwndropdown;Long ll_row

ll_row = idwc_plant.Find( "location_code ='" + GetText() + "'", 1, idwc_plant.RowCount() )

If (ll_row = 0) Then
	idwc_plant.SetTrans( SQLCA )
	idwc_plant.Retrieve()
Else 
	idwc_plant.ScrollToRow( ll_row )
	idwc_plant.SelectRow( ll_row, True )
End If
end event

event constructor;call super::constructor;String 		ls_value

ii_rc = GetChild( 'plant_code', idwc_plant )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for plant_code.")

dw_plants.insertrow(0)

//ls_value = ProfileString(iw_frame.is_userini, "OTR", "lastplant", "") 

//dw_plants.SetItem( 1, "plant_code", ls_value)

end event

event destructor;call super::destructor;idwc_plant.setfilter("")
idwc_plant.filter()

end event

event itemchanged;call super::itemchanged;long ll_FoundRow

integer	li_rtn

string ls_complex,ls_search

dwitemstatus l_status

is_plant = Trim(data)

IF Trim(data) = "" THEN Return

ll_FoundRow = idwc_plant.Find ( "location_code='"+data+"'", 1, idwc_plant.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Plant Error", data + " is Not a Valid Plant Code." )
	this.settext("")
	Return 1
else
	ls_complex = idwc_plant.getitemstring(ll_foundrow,"complex_code")
	dw_complex.selectrow(0,false)
//	ls_search = "complex ='"+ls_complex+"'"
//	li_rtn = dw_complex.find(ls_search,1,dw_complex.rowcount())
//	if li_rtn > 0 then
//		dw_complex.selectrow(li_rtn,true)
//	end if
END IF		

//SetProfileString( iw_frame.is_UserINI, "OTR", "lastplant",data)
end event

event itemerror;call super::itemerror;Return 0
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)




end event

type dw_complex from u_base_dw_ext within w_appt_review_queue_filter
integer x = 1097
integer y = 32
integer width = 315
integer height = 156
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_dddw_complex"
boolean border = false
end type

event constructor;call super::constructor;String 		ls_value

ii_rc = GetChild( 'complex', idwc_complex )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for complex.")
idwc_complex.Reset()
idwc_complex.SetTrans(SQLCA)
idwc_complex.Retrieve()

dw_complex.insertrow(0)

//ls_value = ProfileString(iw_frame.is_userini, "OTR", "lastcomplex", "") 
//
//dw_complex.SetItem( 1, "complex", ls_value)
//
end event

event getfocus;call super::getfocus;THIS.SelectText (1,99)
end event

event itemchanged;call super::itemchanged;long ll_FoundRow

iw_frame.SetMicroHelp(dwo.name)

IF Trim(data) = "" THEN
	is_complex = space(3)
	Return
END IF

ll_FoundRow = idwc_complex.Find ( "complex_code='"+data+"'", 1, idwc_complex.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Complex Error", data + " is Not a Valid Complex Code." )
	SelectText(1,3)
	Return 1
ELSE
	is_complex = data
END IF		

//SetProfileString( iw_frame.is_UserINI, "OTR", "lastcomplex",data)
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)

end event

type dw_appt_id from u_base_dw_ext within w_appt_review_queue_filter
integer x = 23
integer y = 244
integer width = 562
integer height = 164
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_appt_id"
boolean border = false
end type

event constructor;call super::constructor;String 		ls_value

dw_appt_id.insertrow(0)

//ls_value = ProfileString(iw_frame.is_userini, "OTR", "lastapptid", "") 
//
//dw_appt_id.SetItem( 1, "appt_id", ls_value)
//
end event

event getfocus;call super::getfocus;THIS.SelectText (1,99)
end event

event itemchanged;call super::itemchanged;
is_appt_id = data

//SetProfileString( iw_frame.is_UserINI, "OTR", "lastapptid", is_appt_id )


end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)
end event

type dw_appt_date from u_base_dw_ext within w_appt_review_queue_filter
integer x = 1079
integer y = 320
integer width = 338
integer height = 88
integer taborder = 60
boolean bringtotop = true
string dataobject = "d_work_date"
boolean border = false
end type

event constructor;call super::constructor;String	ls_text
date 		current

//current = today()
current = Date('01/01/0001')

dw_appt_date.InsertRow(0)

//ls_text = ProfileString( iw_frame.is_UserINI, "OTR", "Lastapptdate","")

If ls_text = '' Then
	SetItem( 1, "work_date", current )
	dw_appt_date.SetItem(1, "work_date", current )
Else
	dw_appt_date.SetItem( 1, "work_date", Date(ls_text))
End If



end event

event getfocus;call super::getfocus;THIS.SelectText (1,99)
end event

event itemchanged;call super::itemchanged;String	ls_date

idt_appt_date = Date(data)

ls_date = String(idt_appt_date,"MM/DD/YYYY") 

//SetProfileString( iw_frame.is_UserINI, "OTR", "Lastapptdate", ls_date)
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText(1,99)
end event

type st_1 from statictext within w_appt_review_queue_filter
integer x = 1079
integer y = 248
integer width = 338
integer height = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Appt. Date"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_sales_order_id from u_base_dw_ext within w_appt_review_queue_filter
integer x = 695
integer y = 196
integer width = 288
integer height = 212
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_appt_sales_order_id"
boolean border = false
end type

event constructor;call super::constructor;String 		ls_value

dw_sales_order_id.insertrow(0)

//ls_value = ProfileString(iw_frame.is_userini, "OTR", "lastsalesorderid", "") 

//dw_sales_order_id.SetItem( 1, "sales_order_id", ls_value)

end event

event getfocus;call super::getfocus;THIS.SelectText (1,99)
end event

event itemchanged;call super::itemchanged;
is_sales_order_id = data

//SetProfileString( iw_frame.is_UserINI, "OTR", "lastsalesorderid", is_sales_order_id )


end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)
end event

