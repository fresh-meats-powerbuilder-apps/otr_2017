HA$PBExportHeader$w_carr_no_appt.srw
forward
global type w_carr_no_appt from w_base_sheet_ext
end type
type dw_carr_no_appt from u_base_dw_ext within w_carr_no_appt
end type
end forward

global type w_carr_no_appt from w_base_sheet_ext
integer width = 2016
integer height = 1108
string title = "Complexes Without Appointments "
long backcolor = 12632256
dw_carr_no_appt dw_carr_no_appt
end type
global w_carr_no_appt w_carr_no_appt

type variables
u_otr002       iu_otr002
u_ws_appointments  iu_ws_appointments
string    is_closemessage_txt
end variables

forward prototypes
public function boolean wf_retrieve ()
public function integer wf_load_carr_no_appt (string as_appt_info)
end prototypes

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer

String   ls_start_carrier, &
         ls_end_carrier, &
			ls_from_date, &
			ls_to_date, &
			ls_date_code, &
			ls_to_from_filter

ls_start_carrier = dw_carr_no_appt.object.start_carrier[1]     
ls_end_carrier = dw_carr_no_appt.object.end_carrier[1]
ls_from_date = dw_carr_no_appt.object.from_date[1]
ls_to_date = dw_carr_no_appt.object.to_date[1]
ls_date_code = dw_carr_no_appt.object.date_code[1]

ls_to_from_filter = ls_start_carrier + '~t' + ls_end_carrier + '~t' + ls_from_date + '~t' + ls_to_date + '~t' + ls_date_code 
			
OpenWithParm( w_carr_no_appt_filter, ls_to_from_filter, iw_frame )
ls_to_from_filter = Message.StringParm
IF iw_frame.iu_string.nf_amIempty(ls_to_from_filter) then
//	 SetMicroHelp("Ready...")
    return TRUE
end if
// check to see if cancel was pressed
dw_carr_no_appt.Reset()
wf_load_carr_no_appt ( ls_to_from_filter )
dw_carr_no_appt.ResetUpdate()
 
Return FALSE








end function

public function integer wf_load_carr_no_appt (string as_appt_info);date     ld_temp_date 

integer  li_rtn, &
      	li_RPCrtn 

long     ll_row, &
         ll_total_no_rows
	
string   ls_carr_no_appt_string, &
       	ls_temp, &
         ls_req_carrier_start, &
         ls_req_carrier_end, &
         ls_req_delv_from_date, &
         ls_req_delv_to_date, &
         ls_from_date, &
			ls_to_date, &
         ls_year, &
         ls_month, &
         ls_day, &
         ls_date_ind, &
			ls_to_from_filter


s_error                 lstr_Error_Info

SetPointer(HourGlass!)

lstr_error_info.se_event_name = "wf_retrieve"
lstr_error_info.se_window_name = "w_carr_no_appt"
lstr_error_info.se_procedure_name = "wf_retrieve"
lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_message = Space(71)

ls_to_from_filter = Message.StringParm

ls_req_carrier_start    =  iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
ls_req_carrier_end      =  iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
ls_from_date            =  iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
ls_year                 =  Left(ls_from_date, 4)                
ls_month                =  Mid(ls_from_date, 5, 2)
ls_day                  =  Mid(ls_from_date, 7, 2)
ls_req_delv_from_date   =  ls_year + '-' + ls_month + '-' + ls_day
ls_to_date              =  iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
ls_year                 =  Left(ls_to_date, 4)                
ls_month                =  Mid(ls_to_date, 5, 2)
ls_day                  =  Mid(ls_to_date, 7, 2)
ls_req_delv_to_date     =  ls_year + '-' + ls_month + '-' + ls_day
ls_date_ind             =  iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')

ls_carr_no_appt_string = Space(1201)

//li_rtn = iu_otr002.nf_otrt40ar(ls_req_carrier_start, ls_req_carrier_end, &
//       ls_req_delv_from_date, ls_req_delv_to_date, ls_date_ind, &
//       ls_carr_no_appt_string, lstr_error_info, 0)
  li_rtn = iu_ws_appointments.uf_otrt40er(ls_req_carrier_start, ls_req_carrier_end, ls_req_delv_from_date, ls_req_delv_to_date, ls_date_ind,ls_carr_no_appt_string, lstr_error_info)

// Check the return code from the above function call
If li_rtn <> 0 Then
 		dw_carr_no_appt.ResetUpdate()
	   Return -1
ELSE
      iw_frame.SetMicroHelp( "Ready...")
END IF

 
li_rtn = dw_carr_no_appt.ImportString(Trim(ls_carr_no_appt_string))

If li_rtn <= 0 Then 
	iw_frame.SetMicroHelp("No Records Found.")
End If

dw_carr_no_appt.object.start_carrier[1] = ls_req_carrier_start      
dw_carr_no_appt.object.end_carrier[1] = ls_req_carrier_end  
dw_carr_no_appt.object.from_date[1] = ls_from_date
dw_carr_no_appt.object.to_date[1] = ls_to_date
dw_carr_no_appt.object.date_code[1] = ls_date_ind


this.Title = "Complexes Without Appointments - " + ls_req_carrier_start + " through " + ls_req_carrier_end  

Return -1


end function

event ue_postopen;call super::ue_postopen;//iu_otr002  =  CREATE u_otr002
iu_ws_appointments = CREATE u_ws_appointments
IF wf_retrieve() THEN Close( this )
end event

on open;call w_base_sheet_ext::open;dw_carr_no_appt.InsertRow(0)
end on

event close;call super::close;//DESTROY iu_otr002
DESTROY iu_ws_appointments
end event

on w_carr_no_appt.create
int iCurrent
call super::create
this.dw_carr_no_appt=create dw_carr_no_appt
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_carr_no_appt
end on

on w_carr_no_appt.destroy
call super::destroy
destroy(this.dw_carr_no_appt)
end on

type dw_carr_no_appt from u_base_dw_ext within w_carr_no_appt
integer x = 23
integer y = 20
integer width = 1934
integer height = 960
integer taborder = 0
string dataobject = "d_carr_no_appt"
boolean vscrollbar = true
end type

event doubleclicked;call super::doubleclicked;char     lc_from_date[8], &
         lc_to_date[8], &
         lc_date_ind[1]

long     ll_Column
      
string   ls_input_string, &
         ls_carrier_code

w_mass_appt_update  lw_mass_appt_update
  

IF row > 0 and row <= RowCount() and (is_ObjectAtPointer = "carrier_code_1" or is_ObjectAtPointer = "carrier_code_2" or is_ObjectAtPointer = "carrier_code_3" or is_ObjectAtPointer = "carrier_code_4" or is_ObjectAtPointer = "carrier_code_5" or is_ObjectAtPointer = "carrier_code_6")  Then
	ll_column = Integer(dwo.id)  //find out correct id (always 1)
   ls_carrier_code = This.object.data[Row, ll_column]
   lc_from_date = This.object.from_date[1]
   lc_to_date = This.object.to_date[1]
   lc_date_ind = This.object.date_code[1]
   ls_input_string = ls_carrier_code + lc_from_date + lc_to_date + lc_date_ind  + "  "
   OpenSheetWithParm(lw_mass_appt_update, ls_input_string, "w_mass_appt_update", iw_frame, 0, Original!)  
End IF

end event

