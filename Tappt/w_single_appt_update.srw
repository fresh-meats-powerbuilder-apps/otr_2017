HA$PBExportHeader$w_single_appt_update.srw
forward
global type w_single_appt_update from w_base_sheet_ext
end type
type dw_appt_update from u_base_dw_ext within w_single_appt_update
end type
end forward

global type w_single_appt_update from w_base_sheet_ext
integer x = 352
integer y = 308
integer width = 2962
integer height = 1280
string title = "Single Appointment"
long backcolor = 12632256
dw_appt_update dw_appt_update
end type
global w_single_appt_update w_single_appt_update

type variables
u_otr002  	iu_otr002

u_ws_appointments  iu_ws_appointments
   
long    		il_LastRow, &
				il_xpos, &
				il_ypos, &
				il_error_row

integer  	ii_error_column

string  		is_description, &
				is_closemessage_txt

string 		is_load_key


end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function integer wf_changed ()
public function integer wf_load_single_appt ()
public function boolean wf_pre_save ()
end prototypes

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer

 
li_rc = wf_Changed()

CHOOSE CASE li_rc	//begin 1
	CASE  1	//	a valid change occurred ...
		li_answer = MessageBox("Wait",is_closemessage_txt,question!,yesnocancel!)
		CHOOSE CASE li_answer
			CASE 1	//	yes, save changes
				IF wf_Update() THEN	//	the save succeeded
				ELSE	//	the save failed
					li_answer = MessageBox("Save Failed","Retrieve Anyway?",question!,yesno!)
					IF li_answer = 1 THEN	//	retrieve w/o saving changes
					ELSE	//	cancel the close
						Return( false )
					END IF
				END IF	
			CASE 2 	//	retrieve w/o saving changes
			CASE 3	//	cancel the retrieve
				Return(false)
		END CHOOSE
	CASE  -1 		//	a validation error occurred
		li_answer = MessageBox("Data Entry Error","Retrieve w/o Save?",question!,okcancel!)
		CHOOSE CASE li_answer
			CASE 1	//	retrieve w/o saving changes
			CASE 2	//	cancel the retrieve
				Return( false )
		END CHOOSE
	CASE  -2 	//	a serious validation error, window may not close in this state
		MessageBox("A Serious Error Has Occurred","Correct Error Before Retrieving!",exclamation!,ok!)
		Return(False)
	CASE ELSE	//	nothing has changed
END CHOOSE

if iw_frame.iu_string.nf_amIEmpty(is_load_key) then
    OpenWithParm( w_single_appt_update_filter, is_load_key, iw_frame ) 
    is_load_key = Message.StringParm
    IF iw_frame.iu_string.nf_IsEmpty(is_load_key)  Then
        Return True
    End If
end if

// Turn the redraw off for the window while we retrieve
This.SetRedraw(FALSE)

// Populate the datawindows
This.wf_load_single_appt()
is_load_key = "     "

// Turn the redraw off for the window while we retrieve
This.SetRedraw(TRUE)

Return FALSE



end function

public function boolean wf_update ();
Integer  li_return_error_code, &    
         li_rtn
			
long     ll_Row
	
string   ls_return_error_msg, &   
         ls_appt_update_string, &
      	ls_month, &
         ls_day, &
         ls_year, &
         ls_hours, &
         ls_minutes, &
         ls_appt_contact, &
         ls_load_key, &
         ls_stop_code, &
         ls_override_ind, & 
         ls_notify_carrier_ind, &
         ls_appt_date, &
         ls_appt_time, &
         ls_eta_date, &
         ls_eta_time 

date     ld_date
time     lt_time

s_error  lstr_Error_Info

If wf_pre_save() = TRUE Then  

SetPointer(HourGlass!)
dw_appt_update.SetRedraw(False)

ll_row = dw_appt_update.GetNextModified(0, Primary!)

IF ll_row = 0 Then
	SetMicroHelp("No Modified Data.")
	Return True
End IF

do while ll_row <> 0
   ls_load_key = dw_appt_update.object.load_key[ll_Row]           
   ls_stop_code = dw_appt_update.object.stop_code[ll_Row] 
   ls_appt_contact = dw_appt_update.object.appt_contact[ll_Row] 
	ls_appt_contact = ls_appt_contact + space(10 - Len(ls_appt_contact))
   ls_eta_date = String(dw_appt_update.object.appt_date[ll_Row], "yyyymmdd")
	ls_appt_date = ls_eta_date
   lt_time = dw_appt_update.object.appt_time[ll_Row]
	ls_hours = Mid(String(lt_time,"hh:mm"), 1, 2) 
   ls_minutes = Mid(String(lt_time,"hh:mm"), 4, 2)
   ls_appt_time = ls_hours + ls_minutes 
   ls_eta_time = ls_appt_time 
   ls_override_ind = dw_appt_update.object.override_ind[ll_Row] 
   ls_notify_carrier_ind = dw_appt_update.object.notify_carrier_ind[ll_Row] 
   ls_return_error_msg = space(80)
	
   lstr_error_info.se_event_name = "wf_update"
   lstr_error_info.se_window_name = "w_mass_appt_update"
   lstr_error_info.se_procedure_name = "wf_update"
   lstr_error_info.se_user_id = SQLCA.UserID
   lstr_error_info.se_message = Space(71)

//   li_rtn = iu_otr002.nf_otrt19ar(ls_load_key, ls_stop_code, &
// 	 	ls_appt_contact, ls_appt_date, ls_appt_time, ls_eta_date, &
//	   ls_eta_time, ls_override_ind, ls_notify_carrier_ind, &
//      li_return_error_code, ls_return_error_msg, lstr_error_info, 0)
     li_rtn = iu_ws_appointments.uf_otrt19er(ls_load_key, ls_stop_code, &
 	 	ls_appt_contact, ls_appt_date, ls_appt_time, ls_eta_date, &
	   ls_eta_time, ls_override_ind, ls_notify_carrier_ind, lstr_error_info)


// Check the return code from the above function call

   If li_rtn = 0 or li_rtn = 1 Then
      dw_appt_update.DeleteRow(ll_Row) 
      ll_Row --
   Else 
      If li_rtn < 0 Then
         dw_appt_update.SetRow(ll_row)
 	      dw_appt_update.SetRedraw(True)
         Return False 
      Else
         If SQLCA.ii_messagebox_rtn = 3 Then
            dw_appt_update.SetRow(ll_row)
            dw_appt_update.SetRedraw(True)
            Return False
         End If 
      End If  
    End If
 	  
	ll_row = dw_appt_update.GetNextModified(ll_row, PRIMARY!)
loop

End if


dw_appt_update.SetColumn("carrier_code")

SetMicroHelp("Update Processing Complete.")

dw_appt_update.SetRedraw(True)
Return TRUE
end function

public function integer wf_changed ();/*
	purpose:	this function will examine all datawindow controls on the window
				that have update capability against the database.  if anything has changed,
				and the change is valid, a 1 is returned.  if an invalid change has occurred
				then a -1 is returned.  if nothing has changed a 0 is returned.

*/

IF dw_appt_update.ModifiedCount() + &
	dw_appt_update.DeletedCount() > 0 THEN
	is_closemessage_txt = "Save Changes?"
	RETURN 1
END IF

// nothing has been changed
RETURN 0
	
end function

public function integer wf_load_single_appt ();integer  li_rtn

string   ls_appt_update_string, &
      	ls_carrier_code, &
         ls_from_ship_date, &
         ls_to_ship_date, &
         ls_refetch_load_key, &
         ls_refetch_stop_code, &
         ls_refetch_delv_date, &
         ls_date_ind

s_error  lstr_Error_Info

dw_appt_update.Reset()

lstr_error_info.se_event_name = "wf_retrieve"
lstr_error_info.se_window_name = "w_single_appt_update"
lstr_error_info.se_procedure_name = "wf_retrieve"
lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_message = Space(71)

ls_refetch_load_key   = Space(7)
ls_refetch_stop_code  = Space(2)
ls_refetch_delv_date  = Space(10)
ls_date_ind           = Space(1)


Do

   ls_appt_update_string = Space(2926)
 	
//	li_rtn = iu_otr002.nf_otrt18ar(ls_carrier_code, ls_from_ship_date,&
//      		ls_to_ship_date, is_load_key, ls_date_ind, &
//            " " ,ls_appt_update_string, ls_refetch_load_key, &
//            ls_refetch_stop_code, ls_refetch_delv_date, lstr_error_info, 0)

   li_rtn = iu_ws_appointments.uf_otrt18er(ls_carrier_code, ls_from_ship_date, ls_to_ship_date, is_load_key, ls_date_ind," ", ls_appt_update_string, lstr_error_info) 

// Check the return code from the above function call
If li_rtn <> 0 Then
 		dw_appt_update.ResetUpdate()
      Return  -1
ELSE
      iw_frame.SetMicroHelp( "Ready...")
END IF

     li_rtn = dw_appt_update.ImportString(Trim(ls_appt_update_string))

//Loop While not iw_frame.iu_string.nf_IsEmpty(ls_refetch_load_key)
loop while len(Trim(ls_refetch_load_key)) > 0

ls_carrier_code = dw_appt_update.GetItemString(1,"carrier_code") 

this.Title = "Single Appointment for " + is_load_key + ", " + ls_carrier_code  

dw_appt_update.SetFocus()

dw_appt_update.ResetUpdate()

Return 1



end function

public function boolean wf_pre_save ();date		 ld_ApptDate
		  
long      ll_Row

string	 ls_ApptContact

time      lt_ApptTime


dw_appt_update.AcceptText()

ll_Row = dw_appt_update.GetNextModified(0, PRIMARY!)

Do while ll_Row <> 0
  ls_ApptContact = dw_appt_update.GetItemString(ll_Row, "appt_contact")
  If IsNull(ls_ApptContact) or Len(Trim(ls_apptContact)) = 0 then
     MessageBox("Appointment Contact", "Please enter an Appointment Contact") 
     dw_appt_update.SetColumn("appt_contact")     
	  ii_error_column = dw_appt_update.getcolumn()
     il_error_row = ll_Row  
	  dw_appt_update.postevent("ue_post_tab")
     Return FALSE
  End if
  lt_ApptTime = dw_appt_update.GetItemTime(ll_Row, "appt_time")
  If IsTime(String(lt_ApptTime)) = FALSE then
     MessageBox("Appointment Time", "Please enter an Appointment Time")
     dw_appt_update.SetColumn("appt_Time")
	  ii_error_column = dw_appt_update.getcolumn()
     il_error_row = ll_Row
	  dw_appt_update.postevent("ue_post_tab")
     Return FALSE    
  End if
  If String(lt_ApptTime) > '00:00:00' then
     ld_ApptDate = dw_appt_update.GetItemDate(ll_Row, "appt_date")
     If IsDate(String(ld_ApptDate))= FALSE then      
        MessageBox("Appointment Date", "Please enter an Appointment Date")
        dw_appt_update.SetColumn("appt_date")
		  ii_error_column = dw_appt_update.getcolumn()
        il_error_row = ll_Row
		  dw_appt_update.postevent("ue_post_tab")
        Return FALSE     
     End if
  End If
ll_Row = dw_appt_update.GetNextModified(ll_Row, PRIMARY!)
Loop 

Return TRUE
end function

on open;call w_base_sheet_ext::open;string    ls_input_string


dw_appt_update.InsertRow(0)

ls_input_string = message.stringparm
is_load_key = ls_input_string


end on

event ue_postopen;call super::ue_postopen;//iu_otr002  =  CREATE u_otr002
iu_ws_appointments = CREATE u_ws_appointments


IF wf_retrieve() THEN Close( this )


end event

event close;call super::close;//DESTROY iu_otr002

DESTROY iu_ws_appointments 
end event

on w_single_appt_update.create
int iCurrent
call super::create
this.dw_appt_update=create dw_appt_update
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_appt_update
end on

on w_single_appt_update.destroy
call super::destroy
destroy(this.dw_appt_update)
end on

event ue_get_data(string as_value);call super::ue_get_data;CHOOSE CASE as_value
	CASE 'ToolTip'
		Message.StringParm = is_description
	case  "xpos"
		Message.StringParm = string(il_xpos)
	case  "ypos"
		Message.StringParm = string(il_ypos)
	case else
		Message.StringParm = ''	
END CHOOSE

end event

type dw_appt_update from u_base_dw_ext within w_single_appt_update
event ue_post_tab pbm_custom24
integer x = 18
integer y = 8
integer width = 2898
integer height = 1124
integer taborder = 20
string dataobject = "d_appt_update"
boolean vscrollbar = true
boolean border = false
end type

on ue_post_tab;call u_base_dw_ext::ue_post_tab;scrolltorow(il_error_row)
setrow(il_error_row)
setcolumn(ii_error_column)
setfocus()

end on

event itemchanged;call super::itemchanged;date      ld_ApptDate

Long     ll_deviation

string   ls_temp_hour, &
         ls_temp_min, &
         ls_temp_time, &
			ls2_temp_hour, &
			ls2_temp_min, &
			ls2_temp_time

time     lt_ApptTime, &   
         lt_deviation_time, &
         lt_to_hours, &
			lt2_to_hours


      
ld_ApptDate = this.GetItemDate(Row,"appt_date")

If is_ColumnName = "appt_date" Then
	ld_ApptDate = date(data) //dw_appt_update.GetItemDate(Row,"appt_date")
   If IsNull(Data) then      
      MessageBox("Appointment Date", "Please enter an Appointment Date") 
      This.SetColumn("appt_Date")     
   Else
      If This.object.stop_code[Row] = "01" Then
         If ld_ApptDate <> This.object.promised_deliv_date[Row] Then
            This.SetItem(Row,"override_ind","E")             
         End if
      Else
         If ld_ApptDate <> This.object.sched_deliv_date[Row] Then
            This.SetItem(Row,"override_ind","E")             
         End if
      End if
   End if
End if

If is_ColumnName = "appt_time" Then
   lt_ApptTime = Time(This.GetText())

   If This.object.late_load_ind[Row] =  "T"  or  &
      This.object.late_load_ind[Row] =  "W"  or  &   
      This.object.late_load_ind[Row] =  "O"  or  &
      This.object.late_load_ind[Row] =  "L"  			Then
		This.SetItem(Row,"override_ind","Y")  
	ELSE
		
      If This.object.stop_code[Row] = "01" Then
         If ld_ApptDate <> This.object.promised_deliv_date[Row] Then
            This.SetItem(Row,"override_ind","E")                 
         Else      
            If This.object.keyed_ind[Row] = "Y" Then  
               ll_deviation = This.object.appt_deviation_hours[Row] * 3600              
               lt_deviation_time = RelativeTime(This.object.promised_deliv_time[Row], ll_deviation) 
               If lt_ApptTime > lt_deviation_time Then
                    This.SetItem(Row,"override_ind","E")
               End if
            Else  
               If (This.object.recv_from_hours[Row] = "0000") and &
                  (This.object.recv_to_hours[Row] = "0000") and &
						(This.object.recv2_from_hours[Row] = "0000") and &
                  (This.object.recv2_to_hours[Row] = "0000") Then
                   ll_deviation = This.object.appt_deviation_hours[Row] * 3600    
                   lt_deviation_time = RelativeTime(This.object.promised_deliv_time[Row], ll_deviation) 
                   If lt_ApptTime > lt_deviation_time Then
                       This.SetItem(Row,"override_ind","E")
                   End if
               Else
                   ls_temp_time = This.object.recv_to_hours[Row]
                   ls_temp_hour = Left(ls_temp_time,2)
                   ls_temp_min = Mid(ls_temp_time,3,2)
                   ls_temp_time = ls_temp_hour + ":" + ls_temp_min 
                   lt_to_hours = Time(ls_temp_time)
						 
						 ls2_temp_time = This.object.recv2_to_hours[Row]
                   ls2_temp_hour = Left(ls2_temp_time,2)
                   ls2_temp_min = Mid(ls2_temp_time,3,2)
                   ls2_temp_time = ls2_temp_hour + ":" + ls2_temp_min 
                   lt2_to_hours = Time(ls2_temp_time)
						 
                   If lt_ApptTime > lt_to_hours Then
							 If lt2_to_hours = Time('00:00') Then
                         This.SetItem(Row,"override_ind","E")     
							 End if
                   End if
						 
                   If lt_ApptTime > lt_to_hours Then
							 If lt_ApptTime > lt2_to_hours Then
                         This.SetItem(Row,"override_ind","E")     
							 End if
                   End if
						 
                 End if
              End if 
        End if   
     Else
         If ld_ApptDate <> This.object.sched_deliv_date[Row] Then
            This.SetItem(Row,"override_ind","E")                 
         Else
            If (This.object.recv_from_hours[Row] = "0000") and &
               (This.object.recv_to_hours[Row] = "0000") and &
					(This.object.recv2_from_hours[Row] = "0000") and &
               (This.object.recv2_to_hours[Row] = "0000") Then
                ll_deviation = This.object.appt_deviation_hours[Row] * 3600    
                lt_deviation_time = RelativeTime(This.object.sched_deliv_time[Row], ll_deviation) 
                If lt_ApptTime > lt_deviation_time Then
                    This.SetItem(Row,"override_ind","E")
                End if
            Else
                ls_temp_time = This.object.recv_to_hours[Row]
                ls_temp_hour = Left(ls_temp_time,2)
                ls_temp_min = Mid(ls_temp_time,3,2)
                ls_temp_time = ls_temp_hour + ":" + ls_temp_min 
                lt_to_hours = Time(ls_temp_time) 
					 
					 ls2_temp_time = This.object.recv2_to_hours[Row]
                ls2_temp_hour = Left(ls2_temp_time,2)
                ls2_temp_min = Mid(ls2_temp_time,3,2)
                ls2_temp_time = ls2_temp_hour + ":" + ls2_temp_min 
                lt2_to_hours = Time(ls2_temp_time)
						 
                If lt_ApptTime > lt_to_hours Then
						 If lt2_to_hours = Time('00:00') Then
                      This.SetItem(Row,"override_ind","E")
					    End if
                End if 
					 
                If lt_ApptTime > lt_to_hours Then
						 If lt_ApptTime > lt2_to_hours Then
                      This.SetItem(Row,"override_ind","E")     
						 End if
                End if
					 
            End if
           End if
         End if
   End if
End if

 
If This.object.override_ind[Row] = "E" Then
    This.SelectRow(Row,TRUE)
Else
    This.SelectRow(Row,FALSE)
End if

end event

event constructor;call super::constructor;This.is_selection = "0"
end event

event doubleclicked;call super::doubleclicked;char     lc_stop_code[2], &
         lc_load_key[7] 

long		ll_column

string   ls_input_string
			
  
If dwo.name = "stop_code" Then
	ll_column = Integer(dwo.id)
	lc_stop_code = This.object.stop_code[Row]
   lc_load_key = This.object.load_key[row]
   ls_input_string =  lc_load_key + lc_stop_code
   OpenWithParm(w_stops_order_po_resp, ls_input_string)     
End if
 
end event

on ue_postconstructor;call u_base_dw_ext::ue_postconstructor;ib_updateable = TRUE


end on

event ue_mousemove;call super::ue_mousemove;String			ls_name, &
					ls_appt_contact


Choose Case dwo.name
	Case 'appt_contact'
		If il_LastRow = row Then return
	   is_description = This.GetItemString(row, 'appt_contact')
		  

		If IsValid(w_tooltip) Then Close(w_tooltip)
		This.SetFocus()
		il_LastRow = row
		If iw_frame.iu_string.nf_IsEmpty(is_description) Then Return
		OpenWithParm(w_tooltip, iw_parent)
	Case Else
		il_LastRow = 0
		If IsValid(w_tooltip) Then Close(w_tooltip)
End Choose


end event

