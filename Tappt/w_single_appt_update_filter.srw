HA$PBExportHeader$w_single_appt_update_filter.srw
forward
global type w_single_appt_update_filter from w_base_response_ext
end type
type st_1 from statictext within w_single_appt_update_filter
end type
type dw_1 from u_base_dw_ext within w_single_appt_update_filter
end type
end forward

global type w_single_appt_update_filter from w_base_response_ext
int X=915
int Y=345
int Width=956
int Height=589
boolean TitleBar=true
string Title="Single Appointment Filter "
long BackColor=12632256
st_1 st_1
dw_1 dw_1
end type
global w_single_appt_update_filter w_single_appt_update_filter

type variables
integer	ii_rc




end variables

event open;call super::open;String   ls_to_from_filter


ls_to_from_filter = Message.StringParm

dw_1.InsertRow(0) 

IF Not iw_frame.iu_string.nf_IsEmpty(ls_to_from_filter) THEN 
	dw_1.SetItem( 1, "order_number", ls_to_from_filter)
End if
  
dw_1.SelectText(1, 5)




end event

on w_single_appt_update_filter.create
int iCurrent
call w_base_response_ext::create
this.st_1=create st_1
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
this.Control[iCurrent+2]=dw_1
end on

on w_single_appt_update_filter.destroy
call w_base_response_ext::destroy
destroy(this.st_1)
destroy(this.dw_1)
end on

event ue_base_cancel;call super::ue_base_cancel;string ls_cancel

ls_cancel = ' '

CloseWithReturn( This, ls_cancel )

 
end event

event ue_base_ok;call super::ue_base_ok;String    ls_order_number


If dw_1.AcceptText() = 1 Then
   ls_order_number = dw_1.object.order_number[1]
	IF Trim(ls_order_number) = "" Then
			MessageBox("Order Number", "Please enter an Order Number") 
			dw_1.SetFocus()
	Else	
  	 CloseWithReturn( This, ls_order_number)
	End If
End if



end event

type cb_base_help from w_base_response_ext`cb_base_help within w_single_appt_update_filter
int X=627
int Y=305
int TabOrder=40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_single_appt_update_filter
int Y=305
int TabOrder=30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_single_appt_update_filter
int X=37
int Y=305
int TabOrder=20
end type

type st_1 from statictext within w_single_appt_update_filter
int X=69
int Y=121
int Width=458
int Height=73
boolean Enabled=false
string Text="Order Number:"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_1 from u_base_dw_ext within w_single_appt_update_filter
int X=558
int Y=125
int Width=298
int Height=137
int TabOrder=10
string DataObject="d_order_number"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

