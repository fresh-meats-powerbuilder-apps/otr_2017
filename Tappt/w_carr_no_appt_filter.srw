HA$PBExportHeader$w_carr_no_appt_filter.srw
forward
global type w_carr_no_appt_filter from w_base_response_ext
end type
type dw_start_end_complexes from u_base_dw_ext within w_carr_no_appt_filter
end type
type gb_2 from groupbox within w_carr_no_appt_filter
end type
type dw_from_and_to_dates from u_base_dw_ext within w_carr_no_appt_filter
end type
type gb_1 from groupbox within w_carr_no_appt_filter
end type
end forward

global type w_carr_no_appt_filter from w_base_response_ext
integer x = 686
integer y = 200
integer width = 1527
integer height = 844
string title = "Complexes Without Appointments Filter "
long backcolor = 12632256
dw_start_end_complexes dw_start_end_complexes
gb_2 gb_2
dw_from_and_to_dates dw_from_and_to_dates
gb_1 gb_1
end type
global w_carr_no_appt_filter w_carr_no_appt_filter

type variables



end variables

event open;call super::open;long          ll_newrow
    
string        ls_input_string, &
              ls_temp, &
              ls_display_date, &
              ls_year, &
              ls_month, &
              ls_day, &
              ls_work_date, & 
				  ls_to_from_filter, &
				  ls_from_date, &
				  ls_to_date, &
				  ls_start_carrier, &
				  ls_end_carrier, &
				  ls_date_code

ls_to_from_filter = Message.StringParm

dw_start_end_complexes.InsertRow(0)
dw_from_and_to_dates.InsertRow(0) 

IF iw_frame.iu_string.nf_AmIEmpty(ls_to_from_filter) Then
	dw_from_and_to_dates.setitem(1, "date_type", "Delivery")
	dw_from_and_to_dates.SetItem(1, "from_date", RelativeDate(Today(),-3))
	dw_from_and_to_dates.SetItem(1, "to_date", RelativeDate(Today(),+7))
Else
	ls_start_carrier = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
   ls_end_carrier = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
   ls_from_date = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
   ls_to_date = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
	ls_date_code = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
	dw_start_end_complexes.SetItem( 1, "start_complex", ls_start_carrier)
	dw_start_end_complexes.SetItem( 1, "end_complex", ls_end_carrier)
	if ls_date_code = "S" then
      dw_from_and_to_dates.setitem(1, "date_type", "Ship")
   else 
      dw_from_and_to_dates.setitem(1, "date_type", "Delivery")
   end if 
   ls_year  = left(ls_from_date, 4)
   ls_month = mid(ls_from_date, 5, 2)
   ls_day   = mid(ls_from_date, 7, 2)
   ls_display_date = ls_month + "/" + ls_day + "/" + ls_year 
   dw_from_and_to_dates.SetItem( 1, "from_date", Date(ls_display_date))
   ls_year  = left(ls_to_date, 4)
   ls_month = mid(ls_to_date, 5, 2)
   ls_day   = mid(ls_to_date, 7, 2)
   ls_display_date = ls_month + "/" + ls_day + "/" + ls_year 
   dw_from_and_to_dates.SetItem( 1, "to_date", Date(ls_display_date))
End IF
	
dw_start_end_complexes.SetFocus()









end event

on w_carr_no_appt_filter.create
int iCurrent
call super::create
this.dw_start_end_complexes=create dw_start_end_complexes
this.gb_2=create gb_2
this.dw_from_and_to_dates=create dw_from_and_to_dates
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_start_end_complexes
this.Control[iCurrent+2]=this.gb_2
this.Control[iCurrent+3]=this.dw_from_and_to_dates
this.Control[iCurrent+4]=this.gb_1
end on

on w_carr_no_appt_filter.destroy
call super::destroy
destroy(this.dw_start_end_complexes)
destroy(this.gb_2)
destroy(this.dw_from_and_to_dates)
destroy(this.gb_1)
end on

event ue_base_cancel;call super::ue_base_cancel;String ls_cancel

ls_cancel = ' '

CloseWithReturn( This, ls_cancel )
 

end event

event ue_base_ok;call super::ue_base_ok;char      lc_start_carrier[4], &
          lc_end_carrier[4], &
          lc_date_code[1] 	       

date      ld_temp_from_date, &
          ld_temp_to_date
 
string    ls_temp, &
          ls_from_date, &
          ls_to_date, &
          ls_whos_date, &
			 ls_to_from_filter



If dw_start_end_complexes.AcceptText() = 1 and dw_from_and_to_dates.AcceptText() = 1 Then

   lc_start_carrier[] = " "
   lc_end_carrier[] = " "
  
   ls_temp = dw_start_end_complexes.object.start_complex[1]
   If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 then
	   MessageBox("Carrier Code", "Please enter a Start Complex Code")
      dw_start_end_complexes.SetFocus()
	   dw_start_end_complexes.SetColumn("start_complex")
      Return
   End if
   lc_start_carrier = ls_temp

   ls_temp = dw_start_end_complexes.object.end_complex[1]
   If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 then
	   MessageBox("Carrier Code", "Please enter an End Complex Code")
      dw_start_end_complexes.SetFocus()
	   dw_start_end_complexes.SetColumn("end_complex")
      Return
   End if
   lc_end_carrier = ls_temp

   ls_whos_date = dw_from_and_to_dates.object.date_type[1]
   if ls_whos_date = "Delivery" or ls_whos_date = "D" then
      lc_date_code = "D"
   else 
      if ls_whos_date = "Ship"  or ls_whos_date = "S" then
         lc_date_code = "S"
      else
         messagebox("Drop Down Box", "Please choose one of the drop down box choices.")
         return
      end if
   end if
  
   ld_temp_from_date = dw_from_and_to_dates.GetItemDate(1, "from_date")
   If IsNull(ld_temp_from_date) then
	   MessageBox("Delivery From Date", "Please enter a Delivery From Date")
      dw_from_and_to_dates.SetFocus()
      dw_from_and_to_dates.SetColumn("from_date")	  
      Return
   End if
   ls_from_date  = String(ld_temp_from_date, "yyyymmdd")

   ld_temp_to_date = dw_from_and_to_dates.GetItemDate(1, "to_date")
   If IsNull(ld_temp_to_date) then
	   MessageBox("Delivery To Date", "Please enter a Delivery To Date")
	   dw_from_and_to_dates.SetFocus()
      dw_from_and_to_dates.SetColumn("to_date")	  
      Return
   End if
   ls_to_date  = String(ld_temp_to_date, "yyyymmdd")

   If ld_temp_from_date  > ld_temp_to_date  Then
      MessageBox("Delivery To Date", "Delivery To Date can not be less than Delivery From Date")
	   dw_from_and_to_dates.SetFocus()
      dw_from_and_to_dates.SetColumn("to_date")	  
      Return
   End if

	ls_to_from_filter = lc_start_carrier[] + '~t' + lc_end_carrier[] + '~t' + ls_from_date + '~t' + ls_to_date + '~t' + lc_date_code[]
	
   CloseWithReturn( This, ls_to_from_filter )

Else
    MessageBox("Invalid Parameters", "Please enter Inquire Parameters")
    Return
End if







end event

type cb_base_help from w_base_response_ext`cb_base_help within w_carr_no_appt_filter
integer x = 974
integer y = 600
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_carr_no_appt_filter
integer x = 622
integer y = 600
integer taborder = 40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_carr_no_appt_filter
integer x = 261
integer y = 600
integer taborder = 30
end type

type dw_start_end_complexes from u_base_dw_ext within w_carr_no_appt_filter
integer x = 334
integer y = 84
integer width = 786
integer height = 164
integer taborder = 10
string dataobject = "d_start_end_complexes"
boolean border = false
end type

event getfocus;call super::getfocus;SelectText(1,6)
end event

event itemfocuschanged;call super::itemfocuschanged;SelectText(1,6)
end event

event constructor;call super::constructor;Integer li_rc

DataWindowChild	ldwc_complex1, ldwc_complex2

//insertRow(0)

li_rc = This.GetChild( 'start_complex', ldwc_complex1 )
IF li_rc = -1 THEN 
	MessageBox("DataWindwoChild Error", "Unable to get Child Handle for start complex.")
else
	ldwc_complex1.SetTrans(SQLCA)
	ldwc_complex1.Retrieve()
End IF

li_rc = This.GetChild( 'end_complex', ldwc_complex2 )
IF li_rc = -1 THEN 
	MessageBox("DataWindwoChild Error", "Unable to get Child Handle for end complex.")
else
	ldwc_complex2.SetTrans(SQLCA)
	ldwc_complex2.Retrieve()
End IF


end event

type gb_2 from groupbox within w_carr_no_appt_filter
integer x = 306
integer y = 28
integer width = 878
integer height = 236
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Complexes"
end type

type dw_from_and_to_dates from u_base_dw_ext within w_carr_no_appt_filter
integer x = 78
integer y = 336
integer width = 1376
integer height = 184
integer taborder = 20
string dataobject = "d_from_and_to_dates_type"
boolean border = false
end type

event getfocus;call super::getfocus;SelectText(1,99)
end event

event itemfocuschanged;call super::itemfocuschanged;SelectText(1,99)
end event

type gb_1 from groupbox within w_carr_no_appt_filter
integer x = 41
integer y = 288
integer width = 1431
integer height = 260
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
end type

