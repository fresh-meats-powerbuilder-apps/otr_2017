HA$PBExportHeader$w_appt_exception.srw
forward
global type w_appt_exception from w_base_sheet_ext
end type
type dw_appt_exception from u_base_dw_ext within w_appt_exception
end type
end forward

global type w_appt_exception from w_base_sheet_ext
integer width = 3026
integer height = 1552
string title = "Appointment Exceptions"
long backcolor = 12632256
dw_appt_exception dw_appt_exception
end type
global w_appt_exception w_appt_exception

type variables
u_otr002   iu_otr002

u_ws_appointments     iu_ws_appointments

character 	ic_start, ic_end

integer     ii_error_column

long        il_LastRow, &
				il_xpos, &
				il_ypos, &
				il_error_row 

string      is_closemessage_txt, &
				is_division, &
				is_complex, &
				is_description, &
				is_plant, &
				is_carrier




end variables

forward prototypes
public function boolean wf_update ()
public function integer wf_changed ()
public function integer wf_load_appt_exception (string as_inquire_info)
public function boolean wf_retrieve ()
public function boolean wf_pre_save ()
end prototypes

public function boolean wf_update ();integer  li_return_error_code, &    
         li_rtn, &
			li_RPCrtn, &
         li_CurrentRow, &
         li_ctr

long     ll_Row, &
         ll_Modified_Ctr
	
string   ls_return_error_msg, &   
         ls_appt_update_string, &
      	ls_month, &
         ls_day, &
         ls_year, &
         ls_hours, &
         ls_minutes, &
         ls_temp, &
      	ls_appt_contact, &
         ls_load_key, &
         ls_stop_code, &
         ls_override_ind, & 
         ls_notify_carrier_ind, & 
         ls_update_ind, & 
         ls_appt_date, &
         ls_appt_time, &
         ls_eta_date, &
         ls_eta_time 
         

s_error  lstr_Error_Info

dw_appt_exception.AcceptText()

If wf_pre_save() = false Then
	iw_frame.SetMicroHelp( "Problem in Presave")
   Return TRUE
End if
	
	
SetPointer( HourGlass! )

dw_appt_exception.SetRedraw(False)
ll_row = 0
ll_row = dw_appt_exception.GetNextModified(ll_row, Primary!)

Do While ll_row <> 0
   ls_update_ind = dw_appt_exception.object.update_ind[ll_Row]
     
   If ls_update_ind = "H"  then      
      ls_override_ind = "E" 
   End if
   If ls_update_ind = "A"  then      
      ls_override_ind = "Y"  
   End if
   If ls_update_ind = "D"  then      
      ls_override_ind = "D" 
   End if

   ls_load_key = dw_appt_exception.object.load_key[ll_Row]
   ls_stop_code = dw_appt_exception.object.stop_code[ll_Row]
 
   ls_appt_contact = dw_appt_exception.object.appt_contact[ll_Row] + &
       space(10 - Len(string(dw_appt_exception.object.appt_contact[ll_Row]))) 
   ls_month	= String(month(dw_appt_exception.object.appt_date[ll_row]))
   IF len(ls_month) = 1 THEN ls_month = '0' + ls_month
   ls_day	= String(day(dw_appt_exception.object.appt_date[ll_row]))
   IF len(ls_day) = 1 THEN ls_day = '0' + ls_day
   ls_year	= String(Year(dw_appt_exception.object.appt_date[ll_row]))
   ls_appt_date = ls_year + ls_month + ls_day
   ls_eta_date = ls_appt_date
 
   ls_hours = Mid(String(dw_appt_exception.GetItemTime(ll_Row, "appt_time"),"hh:mm"), 1, 2) 
   ls_minutes = Mid(String(dw_appt_exception.GetItemTime(ll_Row, "appt_time"),"hh:mm"), 4, 2)
   ls_appt_time = ls_hours + ls_minutes 
   ls_eta_time = ls_appt_time 
   ls_notify_carrier_ind = dw_appt_exception.object.notify_carrier_ind[ll_Row]

   lstr_error_info.se_event_name = "wf_update"
   lstr_error_info.se_window_name = "w_appt_exception"
   lstr_error_info.se_procedure_name = "wf_update"
   lstr_error_info.se_user_id = SQLCA.UserID
   lstr_error_info.se_message = Space(71)
    
//   li_rtn = iu_otr002.nf_otrt19ar(ls_load_key, ls_stop_code, &
// 	   	ls_appt_contact, ls_appt_date, ls_appt_time, ls_eta_date, &
//		   ls_eta_time, ls_override_ind, ls_notify_carrier_ind, &
//         li_return_error_code, ls_return_error_msg, lstr_error_info, 0)


  li_rtn = iu_ws_appointments.uf_otrt19er(ls_load_key, ls_stop_code, &
 	   	ls_appt_contact, ls_appt_date, ls_appt_time, ls_eta_date, &
		   ls_eta_time, ls_override_ind, ls_notify_carrier_ind, lstr_error_info)

// Check the return code from the above function call

   If li_rtn < 0 Then
 	  	Return False 
   Else
      If li_rtn = 0 or li_rtn = 1 Then
         ls_temp = dw_appt_exception.object.update_ind[ll_Row]
         If ls_temp <> "H"  Then
            dw_appt_exception.DeleteRow(ll_Row) 
            ll_Row --
         End If 
      Else
         If SQLCA.ii_messagebox_rtn = 3 Then
            dw_appt_exception.SetRow(ll_row)
            dw_appt_exception.SetRedraw(True)
            Return False
         End if 
      End if   
    End If

   ll_row = dw_appt_exception.GetNextModified(ll_row, Primary!)
loop

iw_frame.SetMicroHelp( "Update Processing Completed")
dw_appt_exception.ResetUpdate()
dw_appt_exception.SetRedraw(True)

Return TRUE



end function

public function integer wf_changed ();/*
	purpose:	this function will examine all datawindow controls on the window
				that have update capability against the database.  if anything has changed,
				and the change is valid, a 1 is returned.  if an invalid change has occurred
				then a -1 is returned.  if nothing has changed a 0 is returned.

*/

IF dw_appt_exception.ModifiedCount() + &
	dw_appt_exception.DeletedCount() > 0 THEN
	is_closemessage_txt = "Save Changes?"
	RETURN 1
END IF

// nothing has been changed
RETURN 0
	
end function

public function integer wf_load_appt_exception (string as_inquire_info);Long		ll_rowcount, ll_count
Date     ld_temp_date
Integer  li_rtn, li_RPCrtn 
String   ls_appt_except_inq_string, &
      	ls_temp, &
         ls_month, &
         ls_day, &
         ls_year, &
         ls_customer_from_char, & 
         ls_customer_to_char, &
	 		ls_refetch_customer_id, &
         ls_refetch_load_key, &
         ls_refetch_stop_code, &
			ls_load_key
    
s_error  lstr_Error_Info

ls_customer_from_char = ic_start
ls_customer_to_char = ic_end

dw_appt_exception.Reset()

SetPointer( HourGlass! )

lstr_error_info.se_event_name = "wf_retrieve"
lstr_error_info.se_window_name = "w_appt_exception"
lstr_error_info.se_procedure_name = "wf_retrieve"
lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_message = Space(71)

ls_refetch_customer_id    = Space(7) 
ls_refetch_load_key       = Space(7) 
ls_refetch_stop_code      = Space(2) 

Do 
   ls_appt_except_inq_string = Space(3441)

   li_rtn = iu_ws_appointments.uf_otrt23er(ls_customer_from_char, ls_customer_to_char, &
     	 		is_division, ls_appt_except_inq_string, &
             lstr_error_info, is_complex, is_plant, is_carrier)
 	
	// Check the return code from the above function call
   If li_rtn <> 0 Then
 	   dw_appt_exception.ResetUpdate()
      Return -1 
   ELSE
      iw_frame.SetMicroHelp( "Ready...")
   END IF

  	li_rtn = dw_appt_exception.ImportString(Trim(ls_appt_except_inq_string))

Loop While len(trim(ls_Refetch_customer_id)) > 0

ll_rowcount = dw_appt_exception.RowCount()
ll_count = +1

Do until ll_count > ll_rowcount
	ls_load_key = dw_appt_exception.GetItemString(ll_count, "load_key")
	If ls_load_key = '       ' Then
		dw_appt_exception.RowsDiscard( ll_count, ll_count, primary! )
		ll_rowcount = dw_appt_exception.RowCount()
		ll_count = +1
	Else
		ll_count = ll_count +1
	End If
Loop

dw_appt_exception.ResetUpdate()

if is_division > "  " Then
	this.Title = "Appointment Exceptions for " + ic_start + " - " + ic_end + ", Division = " + is_division
Else
	this.Title = "Appointment Exceptions for " + ic_start + " - " + ic_end + ", Division = ALL"
End If

Return 1

end function

public function boolean wf_retrieve ();integer	li_rc, li_answer

String   ls_to_from_filter

li_rc = wf_Changed()

CHOOSE CASE li_rc	//begin 1
	CASE  1	//	a valid change occurred ...
		li_answer = MessageBox("Wait",is_closemessage_txt,question!,yesnocancel!)
		CHOOSE CASE li_answer
			CASE 1	//	yes, save changes
				IF wf_Update() THEN	//	the save succeeded
				ELSE	//	the save failed
					li_answer = MessageBox("Save Failed","Retrieve Anyway?",question!,yesno!)
					IF li_answer = 1 THEN	//	retrieve w/o saving changes
					ELSE	//	cancel the close
						Return( false )
					END IF
				END IF	
			CASE 2 	//	retrieve w/o saving changes
			CASE 3	//	cancel the retrieve
				Return(false)
		END CHOOSE
	CASE  -1 		//	a validation error occurred
		li_answer = MessageBox("Data Entry Error","Retrieve w/o Save?",question!,okcancel!)
		CHOOSE CASE li_answer
			CASE 1	//	retrieve w/o saving changes
			CASE 2	//	cancel the retrieve
				Return( false )
		END CHOOSE
	CASE  -2 	//	a serious validation error, window may not close in this state
		MessageBox("A Serious Error Has Occurred","Correct Error Before Retrieving!",exclamation!,ok!)
		Return(False)
	CASE ELSE	//	nothing has changed
END CHOOSE

// ibdkdld
ls_to_from_filter = ic_start + '~t' + ic_end + '~t' + is_division + '~t' + is_complex + '~t' + is_plant + '~t' + is_carrier

OpenWithParm( w_appt_exception_filter, ls_to_from_filter, iw_frame )
ls_to_from_filter = message.stringparm
IF iw_frame.iu_string.nf_amIempty(ls_to_from_filter) Then
	Return True
Else
	ic_start = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
   ic_end = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
	is_division = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
	is_complex = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
	is_plant = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
	is_carrier = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
End If

// Turn the redraw off for the window while we retrieve
This.SetRedraw(FALSE)

// Populate the datawindows
This.wf_load_appt_exception ( ls_to_from_filter )

// Turn the redraw on after information has been imported into dw
This.SetRedraw(TRUE)

Return FALSE

end function

public function boolean wf_pre_save ();date		 ld_ApptDate
		  
//integer   li_CurrentRow, &
//          li_ModifiedInd

long      ll_Total_No_Rows, &
          ll_Row

string	 ls_ApptContact

time      lt_ApptTime

ll_Row = 0

dw_appt_exception.AcceptText()

ll_Total_No_Rows = dw_appt_exception.RowCount()

ll_Row = dw_appt_exception.GetNextModified(ll_Row, PRIMARY!)


Do while ll_Row <> 0
   ls_ApptContact = dw_appt_exception.GetItemString(ll_Row, "appt_contact")
   If IsNull(ls_ApptContact) or Len(Trim(ls_apptContact)) = 0 then
     	MessageBox("Appointment Contact", "Please enter an Appointment Contact") 
      dw_appt_exception.SetColumn("appt_contact")     
		ii_error_column = dw_appt_exception.getcolumn()
      il_error_row = ll_Row  
		dw_appt_exception.postevent("ue_post_tab")
      Return FALSE
   End if
   lt_ApptTime = dw_appt_exception.GetItemTime(ll_Row, "appt_time")
   If IsTime(String(lt_ApptTime)) = FALSE then
      MessageBox("Appointment Time", "Please enter an Appointment Time")
      dw_appt_exception.SetColumn("appt_Time")
   	ii_error_column = dw_appt_exception.getcolumn()
      il_error_row = ll_Row
	   dw_appt_exception.postevent("ue_post_tab")
      Return FALSE    
   End if
   If String(lt_ApptTime) = '00:00:00' then
      MessageBox("Appointment Time", "Please enter an Appointment Time")
      dw_appt_exception.SetColumn("appt_Time")
		ii_error_column = dw_appt_exception.getcolumn()
      il_error_row = ll_Row
		dw_appt_exception.postevent("ue_post_tab")
      Return FALSE    
   End if
   ld_ApptDate = dw_appt_exception.GetItemDate(ll_Row, "appt_date")
   If IsDate(String(ld_ApptDate))= FALSE then      
     	MessageBox("Appointment Date", "Please enter an Appointment Date")
      dw_appt_exception.SetColumn("appt_date")
		ii_error_column = dw_appt_exception.getcolumn()
      il_error_row = ll_Row
		dw_appt_exception.postevent("ue_post_tab")
      Return FALSE     
   End if
   ll_Row = dw_appt_exception.GetNextModified(ll_Row, PRIMARY!)
Loop 


Return TRUE
end function

event open;call super::open;dw_appt_exception.InsertRow(0) 



end event

event close;call super::close;//DESTROY iu_otr002
DESTROY u_ws_appointments
end event

on w_appt_exception.create
int iCurrent
call super::create
this.dw_appt_exception=create dw_appt_exception
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_appt_exception
end on

on w_appt_exception.destroy
call super::destroy
destroy(this.dw_appt_exception)
end on

event ue_postopen;call super::ue_postopen;//iu_otr002  =  CREATE u_otr002
iu_ws_appointments = CREATE u_ws_appointments
IF wf_retrieve() THEN Close( this )

end event

event ue_get_data(string as_value);call super::ue_get_data;CHOOSE CASE as_value
	CASE 'ToolTip'
		Message.StringParm = is_description
	case  "xpos"
		Message.StringParm = string(il_xpos)
	case  "ypos"
		Message.StringParm = string(il_ypos)
	case else
		Message.StringParm = ''	
END CHOOSE

end event

type dw_appt_exception from u_base_dw_ext within w_appt_exception
event ue_post_tab pbm_custom24
integer y = 4
integer width = 2953
integer height = 1380
integer taborder = 20
string dataobject = "d_appt_exception"
boolean vscrollbar = true
end type

on ue_post_tab;call u_base_dw_ext::ue_post_tab;scrolltorow(il_error_row)
setrow(il_error_row)
setcolumn(ii_error_column)
setfocus()

end on

event doubleclicked;call super::doubleclicked;char     lc_stop_code[2], &
         lc_load_key[7] 

long     ll_Column, &
         ll_Row

string   ls_input_string, &
         ls_clicked_column_name, &
			ls_customer_id


//If dwo.id = "3" Then
IF dwo.name = "stop_code" Then
	ll_column = Integer(dwo.id)
   lc_stop_code = This.object.stop_code[row]
   lc_load_key = This.object.load_key[row]
   ls_input_string =  lc_load_key + lc_stop_code
   OpenWithParm(w_stops_order_po_resp, ls_input_string)     
End if
 

// This if is to fix it from going back to load detail after a calander change
If is_ObjectAtPointer = "load_key" Then
   Is_ObjectAtPointer = "          "
End If


If dwo.name  = "recv_phone" Then
   ls_customer_id = This.object.customer_id[row]
   OpenSheetWithParm(w_receive_hrs, ls_customer_id, iw_frame, 0, Original!)     
End if


If dwo.name  = "recv_from_hours" Then
   ls_customer_id = This.object.customer_id[row]
   OpenSheetWithParm(w_receive_hrs, ls_customer_id, iw_frame, 0, Original!)          
End if

      
If dwo.name  = "recv_to_hours" Then
   ls_customer_id = This.object.customer_id[row]
   OpenSheetWithParm(w_receive_hrs, ls_customer_id, iw_frame, 0, Original!)          
End if


If dwo.name  = "recv2_from_hours" Then
   ls_customer_id = This.object.customer_id[row]
   OpenSheetWithParm(w_receive_hrs, ls_customer_id, iw_frame, 0, Original!)          
End if

      
If dwo.name  = "recv2_to_hours" Then
   ls_customer_id = This.object.customer_id[row]
   OpenSheetWithParm(w_receive_hrs, ls_customer_id, iw_frame, 0, Original!)          
End if

end event

event itemchanged;call super::itemchanged;char		 lc_ApptContact

date		 ld_ApptDate
		  
integer   li_CurrentRow, &
          li_ApptTime 

string	ls_ColumnName, &
         ls_ColumnValue, &
         ls_temp

time     lt_time         



If is_ColumnName = "appt_date" Then
   If IsNull(data) then      
     	MessageBox("Appointment Date", "Please enter an Appointment Date") 
     	This.SetColumn("appt_date")
   End if
   This.SetItem(Row, "notify_carrier_ind", "Y") 
End if


If is_ColumnName = "appt_contact" Then
   If IsNull(data) or Len(Trim(data)) = 0  then      
     	MessageBox("Appointment Contact", "Please enter an Appointment Contact") 
     	This.SetColumn("appt_contact")
   End if
   This.SetItem(Row, "notify_carrier_ind", "Y") 
End if


If is_ColumnName = "appt_time" Then
   If IsNull(data) or Len(Trim(data)) = 0  then      
     	MessageBox("Appointment Time", "Please enter an Appointment Time") 
     	This.SetColumn("appt_time")
   End if
   This.SetItem(Row, "notify_carrier_ind", "Y") 
End if

   
end event

event constructor;call super::constructor;This.is_selection = "0"
end event

event ue_postconstructor;call super::ue_postconstructor;ib_updateable = TRUE

 
end event

event ue_mousemove;call super::ue_mousemove;String			ls_name, &
					ls_appt_contact


Choose Case dwo.name
	Case 'appt_contact'
		If il_LastRow = row Then return
	   is_description = This.GetItemString(row, 'appt_contact')
		  

		If IsValid(w_tooltip) Then Close(w_tooltip)
		This.SetFocus()
		il_LastRow = row
		If iw_frame.iu_string.nf_IsEmpty(is_description) Then Return
		OpenWithParm(w_tooltip, iw_parent)
	Case Else
		il_LastRow = 0
		If IsValid(w_tooltip) Then Close(w_tooltip)
End Choose

end event

