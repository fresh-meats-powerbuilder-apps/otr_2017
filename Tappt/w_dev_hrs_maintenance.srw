HA$PBExportHeader$w_dev_hrs_maintenance.srw
forward
global type w_dev_hrs_maintenance from w_base_sheet_ext
end type
type dw_dev_hrs from u_base_dw_ext within w_dev_hrs_maintenance
end type
type gb_1 from groupbox within w_dev_hrs_maintenance
end type
end forward

global type w_dev_hrs_maintenance from w_base_sheet_ext
integer width = 837
integer height = 568
string title = "APPT Hour Deviation"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
long backcolor = 12632256
dw_dev_hrs dw_dev_hrs
gb_1 gb_1
end type
global w_dev_hrs_maintenance w_dev_hrs_maintenance

type variables
u_otr002   iu_otr002

u_ws_appointments  iu_ws_appointments
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_retrieve ();char     lc_update_control

integer  li_rtn, &
			li_RPCrtn 

string   ls_tdiscntl_inq_upd_string

s_error  lstr_Error_Info

li_rtn = dw_dev_hrs.Reset()

ls_tdiscntl_inq_upd_string = "N" + Space(103)

lstr_error_info.se_event_name = "wf_retrieve"
lstr_error_info.se_window_name = "w_dev_hrs_maintenance"
lstr_error_info.se_procedure_name = "wf_retrieve"
lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_message = Space(71)

//li_rtn = iu_otr002.nf_otrt37ar(ls_tdiscntl_inq_upd_string, &
//	 		lstr_error_info, 0)
  li_rtn = iu_ws_appointments.uf_otrt37er(ls_tdiscntl_inq_upd_string, lstr_Error_Info)
  

// Check the return code from the above function call
If li_rtn <> 0 Then
// 		SQLCA.nf_display_message(li_rtn, lstr_Error_Info, 0)
      dw_dev_hrs.ResetUpdate()
      Return False 
Else
      iw_frame.SetMicroHelp( "Ready...")
END IF

//dw_dev_hrs.SetItem(1, "dev_hours", 0)

li_rtn = dw_dev_hrs.ImportString(Trim(ls_tdiscntl_inq_upd_string))

dw_dev_hrs.SetFocus()

dw_dev_hrs.ResetUpdate()

dw_dev_hrs.SelectText(1,4)

dw_dev_hrs.SetItem(1, "dev_hours", integer(dw_dev_hrs.getItemstring(1, "hour_dev")))
dw_dev_hrs.SelectText(1,2)

Return TRUE


end function

public function boolean wf_update ();integer  li_rtn, &
         li_accept, &
			li_hour

string   ls_tdiscntl_inq_upd_string, ls_temp
   
s_error  lstr_Error_Info

li_accept = dw_dev_hrs.AcceptText()


if li_accept = 1 Then
ls_temp = dw_dev_hrs.GetText()

IF integer(ls_temp) < 0 or integer(ls_temp) > 24 Then
        MessageBox("DataWindow Error","Hours must be between 0 and 24")
		  dw_dev_hrs.SetColumn("dev_hours")
		  dw_dev_hrs.SelectText(1,2)
		  Return False
End if

ls_tdiscntl_inq_upd_string = "Y" + "~t" + ls_temp + Space(98)

lstr_error_info.se_event_name = "wf_update"
lstr_error_info.se_window_name = "w_dev_hrs_maintenance"
lstr_error_info.se_procedure_name = "wf_update"
lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_message = Space(71)


//li_rtn = iu_otr002.nf_otrt37ar(ls_tdiscntl_inq_upd_string, &
//	 	                         lstr_error_info, 0)
  li_rtn = iu_ws_appointments.uf_otrt37er(ls_tdiscntl_inq_upd_string, lstr_Error_Info)

// Check the return code from the above function call

      If li_rtn < 0 Then
// 	   	SQLCA.nf_display_message(li_rtn, lstr_Error_Info, 0)
         Return False 
//      Else 
//       	SQLCA.nf_display_message(li_rtn, lstr_Error_Info, 0)
      End If
End IF

dw_dev_hrs.SetFocus()

dw_dev_hrs.SelectText(1,4)

Return TRUE




end function

event close;call super::close;//DESTROY iu_otr002
DESTROY iu_ws_appointments
end event

event ue_postopen;call super::ue_postopen;wf_retrieve()
//dw_dev_hrs.SetItem(1, "dev_hours", integer(dw_dev_hrs.getItemstring(1, "hour_dev")))
//dw_dev_hrs.SelectText(1,2)
end event

event open;call super::open;//iu_otr002  =  CREATE u_otr002
iu_ws_appointments = CREATE u_ws_appointments 
end event

on w_dev_hrs_maintenance.create
int iCurrent
call super::create
this.dw_dev_hrs=create dw_dev_hrs
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_dev_hrs
this.Control[iCurrent+2]=this.gb_1
end on

on w_dev_hrs_maintenance.destroy
call super::destroy
destroy(this.dw_dev_hrs)
destroy(this.gb_1)
end on

type dw_dev_hrs from u_base_dw_ext within w_dev_hrs_maintenance
integer x = 183
integer y = 152
integer width = 480
integer height = 188
integer taborder = 10
string dataobject = "d_dev_hrs_maintenance"
boolean border = false
end type

event itemchanged;call super::itemchanged;If is_ColumnName = "dev_hours" Then
    If iw_frame.iu_string.nf_isempty(data) then
    	MessageBox("Deviation Hours", "Please enter Deviation Hours")
   	dw_dev_hrs.SetColumn("dev_hours")
      Return  
	 Else
		IF integer(data) < 0 or integer(data) > 24 or len(trim(data)) = 0 Then
		  dw_dev_hrs.SetColumn("dev_hours")
		  dw_dev_hrs.SelectText(1,2)
		  Return 1
      End if
    End If
End if


end event

type gb_1 from groupbox within w_dev_hrs_maintenance
integer x = 96
integer y = 60
integer width = 645
integer height = 348
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Appointment "
end type

