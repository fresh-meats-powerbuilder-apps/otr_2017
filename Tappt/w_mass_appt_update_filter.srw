HA$PBExportHeader$w_mass_appt_update_filter.srw
forward
global type w_mass_appt_update_filter from w_base_response_ext
end type
type dw_from_and_to_dates from u_base_dw_ext within w_mass_appt_update_filter
end type
type dw_division from datawindow within w_mass_appt_update_filter
end type
type dw_complexes from u_base_dw_ext within w_mass_appt_update_filter
end type
type gb_1 from groupbox within w_mass_appt_update_filter
end type
end forward

global type w_mass_appt_update_filter from w_base_response_ext
integer x = 645
integer y = 308
integer width = 1550
integer height = 784
string title = "Mass Appointments Filter "
long backcolor = 12632256
dw_from_and_to_dates dw_from_and_to_dates
dw_division dw_division
dw_complexes dw_complexes
gb_1 gb_1
end type
global w_mass_appt_update_filter w_mass_appt_update_filter

type variables
integer	ii_rc

DataWindowChild   idwc_carrier, idwc_division 

string is_division 
end variables

event open;call super::open;long          ll_newrow
    
string        ls_input_string, &
              ls_temp, &
              ls_display_date, &
              ls_year, &
              ls_month, &
              ls_day, &
              ls_work_date, & 
				  ls_to_from_filter, &
				  ls_from_date, &
				  ls_to_date, &
				  ls_carrier, &
				  ls_date_code//, ls_division

ls_to_from_filter = Message.StringParm

dw_from_and_to_dates.InsertRow(0) 

IF iw_frame.iu_string.nf_AmIEmpty(ls_to_from_filter) Then
	dw_from_and_to_dates.setitem(1, "date_type", "Delivery")
	dw_from_and_to_dates.SetItem(1, "from_date", RelativeDate(Today(),-3))
	dw_from_and_to_dates.SetItem(1, "to_date", RelativeDate(Today(),+7))
	dw_division.SetItem( 1, "division_code", "  ")
Else
	ls_carrier = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
   ls_from_date = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
   ls_to_date = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
	ls_date_code = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
// ibdkdld
	is_division  = iw_frame.iu_string.nf_GetToken(ls_to_from_filter, '~t')
	dw_complexes.SetItem( 1, "complex_code", ls_carrier)
	
	if ls_date_code = "S" then
      dw_from_and_to_dates.setitem(1, "date_type", "Ship")
   else 
      dw_from_and_to_dates.setitem(1, "date_type", "Delivery")
   end if 
   ls_year  = left(ls_from_date, 4)
   ls_month = mid(ls_from_date, 5, 2)
   ls_day   = mid(ls_from_date, 7, 2)
   ls_display_date = ls_month + "/" + ls_day + "/" + ls_year 
   dw_from_and_to_dates.SetItem( 1, "from_date", Date(ls_display_date))
   ls_year  = left(ls_to_date, 4)
   ls_month = mid(ls_to_date, 5, 2)
   ls_day   = mid(ls_to_date, 7, 2)
   ls_display_date = ls_month + "/" + ls_day + "/" + ls_year 
   dw_from_and_to_dates.SetItem( 1, "to_date", Date(ls_display_date))
// ibdkdld
//	messagebox("div", is_division)
	dw_division.SetItem( 1, "division_code", is_division)
End IF
	
dw_complexes.SelectText(1, 4)








end event

on w_mass_appt_update_filter.create
int iCurrent
call super::create
this.dw_from_and_to_dates=create dw_from_and_to_dates
this.dw_division=create dw_division
this.dw_complexes=create dw_complexes
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_from_and_to_dates
this.Control[iCurrent+2]=this.dw_division
this.Control[iCurrent+3]=this.dw_complexes
this.Control[iCurrent+4]=this.gb_1
end on

on w_mass_appt_update_filter.destroy
call super::destroy
destroy(this.dw_from_and_to_dates)
destroy(this.dw_division)
destroy(this.dw_complexes)
destroy(this.gb_1)
end on

event ue_base_cancel;call super::ue_base_cancel;String ls_cancel

ls_cancel = ' '
CloseWithReturn( this, ls_cancel )


end event

event ue_base_ok;call super::ue_base_ok;char      lc_carrier_code[4], &
          lc_date_code[1]
	       
date      ld_work_from_date, &
          ld_work_to_date 

string    ls_from_date, &
          ls_to_date, &
          ls_whos_date, &
			 ls_to_from_filter


// ibdkdld
If dw_complexes.AcceptText() = 1 & 
	and dw_from_and_to_dates.AcceptText() = 1 &
	and dw_division.AcceptText() = 1 then

   lc_carrier_code[]	= " "
      
   lc_carrier_code = dw_complexes.object.complex_code[1]

   ld_work_from_date = dw_from_and_to_dates.GetItemDate(1, "from_date")
   If IsNull(ld_work_from_date) then
	   MessageBox("Delivery From Date", "Please enter a Delivery From Date")
      dw_from_and_to_dates.SetFocus()
      dw_from_and_to_dates.SetColumn("from_date")	  
      Return
   End if
   ls_from_date  = String(ld_work_from_date, "yyyymmdd")
  
   ld_work_to_date = dw_from_and_to_dates.GetItemDate(1, "to_date")
   If IsNull(ld_work_to_date) then
	   MessageBox("Delivery To Date", "Please enter a Delivery To Date")
	   dw_from_and_to_dates.SetFocus()
      dw_from_and_to_dates.SetColumn("to_date")	  
      Return
   End if
   ls_to_date  = String(ld_work_to_date, "yyyymmdd")

   If ld_work_from_date  > ld_work_to_date  Then
      MessageBox("Delivery To Date", "Delivery To Date can not be less than Delivery From Date")
	   dw_from_and_to_dates.SetFocus()
      dw_from_and_to_dates.SetColumn("to_date")	  
      Return
   End if

  ls_whos_date = dw_from_and_to_dates.object.date_type[1]
   if ls_whos_date = "Delivery" or ls_whos_date = "D" then
      lc_date_code = "D"
   else 
      if ls_whos_date = "Ship"  or ls_whos_date = "S" then
         lc_date_code = "S"
      else
         messagebox("Drop Down Box", "Please choose one of the drop down box choices.")
         return
      end if
   end if

	// ibdkdld
	If IsNull(is_division) Then
		is_division = "  "
	End If


//   ist_appt_info.carrier_code  = lc_carrier_code
//   ist_appt_info.from_date     = ls_from_date
//   ist_appt_info.to_date       = ls_to_date
//   ist_appt_info.date_code     = lc_date_code[]
//   ist_appt_info.cancel        = FALSE
  ls_to_from_filter = lc_carrier_code + '~t' +  ls_from_date + '~t' + ls_to_date + '~t' + lc_date_code[] + '~t' + is_division
   CloseWithReturn( This, ls_to_from_filter )

End if



end event

type cb_base_help from w_base_response_ext`cb_base_help within w_mass_appt_update_filter
integer x = 992
integer y = 540
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_mass_appt_update_filter
integer x = 599
integer y = 540
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_mass_appt_update_filter
integer x = 206
integer y = 540
integer taborder = 40
end type

type dw_from_and_to_dates from u_base_dw_ext within w_mass_appt_update_filter
integer x = 87
integer y = 224
integer width = 1362
integer height = 236
integer taborder = 30
string dataobject = "d_from_and_to_dates_type"
boolean border = false
end type

type dw_division from datawindow within w_mass_appt_update_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 571
integer y = 8
integer width = 393
integer height = 188
integer taborder = 20
boolean bringtotop = true
string title = "none"
string dataobject = "d_dddw_divisions"
boolean border = false
boolean livescroll = true
end type

event ue_dwndropdown;Long ll_row

ll_row = idwc_division.Find( "type_code ='" + GetText() + "'", 1, idwc_division.RowCount() )

If (ll_row = 0) Then
	idwc_division.SetTrans(SQLCA)
	idwc_division.Retrieve('DIVCODE')
Else 
	idwc_division.ScrollToRow( ll_row )
	idwc_division.SelectRow( ll_row, True )
End If


end event

event constructor;insertRow(0)
ii_rc = GetChild( 'division_code', idwc_division )
IF ii_rc = -1 THEN 
	MessageBox("DataWindwoChild Error", "Unable to get Child Handle for division_code.")
else
	idwc_division.SetTrans(SQLCA)
	idwc_division.Retrieve('DIVCODE')
End IF
end event

event getfocus;THIS.SelectText (1,99)
end event

event itemchanged;long ll_FoundRow
//iw_frame.SetMicroHelp(dwo.name)
IF Trim(data) = "" THEN 
	is_division = data
	Return
End if

ll_FoundRow = idwc_division.Find ( "type_code='"+data+"'", 1, idwc_division.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Division Error", data + " is Not a Valid Division Code." )
	Return 1
else
	is_division = data
END IF
end event

event itemerror;Return 1
end event

event itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)

end event

type dw_complexes from u_base_dw_ext within w_mass_appt_update_filter
integer x = 87
integer y = 12
integer width = 357
integer height = 172
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_complex"
boolean border = false
end type

event constructor;call super::constructor;InsertRow(0)

ii_rc = GetChild( 'complex_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for complex_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()
end event

event itemchanged;call super::itemchanged;long ll_FoundRow

IF Trim(data) = "" THEN Return

ll_FoundRow = idwc_carrier.Find ( "type_code='"+data+"'", 1, idwc_carrier.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Complex Error", data + " is Not a Valid Complex Code." )
//   dw_carriers.SetFocus()  
//   dw_carriers.SetColumn("carrier_code")
	This.SelectText(1,Len(Data))
   Return 1
END IF		
end event

event itemerror;call super::itemerror;Return 1
end event

type gb_1 from groupbox within w_mass_appt_update_filter
integer x = 23
integer y = 188
integer width = 1467
integer height = 276
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
end type

