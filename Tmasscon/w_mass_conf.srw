HA$PBExportHeader$w_mass_conf.srw
forward
global type w_mass_conf from w_base_sheet_ext
end type
type dw_1 from u_base_dw_ext within w_mass_conf
end type
end forward

global type w_mass_conf from w_base_sheet_ext
integer x = 5
integer y = 72
integer width = 2889
integer height = 1228
string title = "Unconfirmed Deliveries"
long backcolor = 12632256
event ue_set_values pbm_custom01
dw_1 dw_1
end type
global w_mass_conf w_mass_conf

type variables
character   ic_control

datawindowchild   idwc_latecode

long       il_RowCount, &
             il_Row

string	is_closemessage_txt, &
              is_late_reason, &
              is_eta_date, &
              is_eta_time, &
 	is_from_menu_ind, &
	is_start_carrier, &
	is_end_carrier, &
	is_delv_date, &
	is_division

integer	ii_rc

uo_mass_conf	iuo_mass_conf

u_otr004   iu_otr004
u_ws_cust_service iu_ws_cust_service

end variables

forward prototypes
public function boolean wf_update ()
public function integer wf_changed ()
public function boolean wf_presave ()
public function boolean wf_retrieve ()
public function integer wf_mass_conf_inq ()
end prototypes

event ue_set_values;call super::ue_set_values;date    ld_eta_date

long    ll_ClickedRow

string  ls_late_reason, &
        ls_string

time    lt_eta_time


ls_string = Message.StringParm

dw_1.SetFocus() 
ll_ClickedRow = dw_1.GetRow()

ls_late_reason   = Left(ls_string, 8)
ld_eta_date      = Date(Mid(ls_string, 9, 10))
lt_eta_time      = Time(Mid(ls_string, 19, 5))   


IF Trim(ls_late_reason) > "" Then
    dw_1.SetItem(ll_ClickedRow, "late_reason", ls_late_reason)
End if
 
If String(ld_eta_date, "mm/dd/yyyy") > "01/01/1900"  Then
    dw_1.SetItem(ll_ClickedRow, "eta_date", ld_eta_date)
Else
	 SetNull(ld_eta_date)
	 dw_1.SetItem(ll_ClickedRow, "eta_date", ld_eta_date)
End if

//If String(lt_eta_time, "hh:mm") > "00:00" Then
    dw_1.SetItem(ll_ClickedRow, "eta_time", lt_eta_time)   
//End if


end event

public function boolean wf_update ();boolean     lb_return, &
            lb_retry

date       ld_work_date

integer    li_rtn, &
			  li_rc        

long		  ll_Row,& 
           ll_RowCount

string      ls_confirmed, &
            ls_load_key, &
				ls_stop_code, &
            ls_eta_date, &
            ls_eta_time, &
            ls_actual_delv_date, &
            ls_actual_delv_time, &
            ls_late_code, &
            ls_actual_date_upd, &
				ls_update_eta_ind
  
time       lt_work_time

s_error	  lstr_Error_Info

dwItemStatus		status


lstr_error_info.se_event_name = "wf_update"
lstr_error_info.se_window_name = "w_mass_conf"
lstr_error_info.se_procedure_name = "wf_update"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

IF dw_1.AcceptText() = -1 THEN Return(False)

lb_return = wf_presave()
If lb_return = False then return(False)

ll_Row = 0

SetPointer(HourGlass!)

dw_1.SetRedraw(False)

ll_Row = dw_1.GetNextModified(ll_Row, PRIMARY!)

Do while ll_Row <> 0 
      lb_retry             = false
      ls_actual_date_upd   = "N"  
      ls_confirmed         = dw_1.GetItemString( ll_Row, "confirmation_ind" )
		ls_load_key		   	= dw_1.GetItemString( ll_Row, "load_key" )
		ls_stop_code	   	= dw_1.GetItemString( ll_Row, "stop_code" )
		ls_update_eta_ind    = dw_1.GetItemString( ll_Row, "update_eta_ind")
   	ld_work_date  	   	= dw_1.GetItemDate( ll_Row, "eta_date" )
      ls_eta_date    	   = String(ld_work_date, "yyyy-mm-dd")
      lt_work_time         = dw_1.GetItemTime( ll_Row, "eta_time" )
      ls_eta_time    	   = String( lt_work_time, "hhmm" )
	   ld_work_date  	      = dw_1.GetItemDate( ll_Row, "actual_delv_date" )
		ls_actual_delv_date  = String(ld_work_date, "yyyy-mm-dd")
      lt_work_time         = dw_1.GetItemTime( ll_Row, "actual_delv_time" )
		ls_actual_delv_time  = String(lt_work_time, "hhmm")
	
      If Trim(ls_confirmed) = "Y"  Then
          If ls_actual_delv_date < "1900-01-01" Then
              ls_actual_delv_date  = ls_eta_date
              ls_actual_delv_time  = ls_eta_time     
          End if 
      end if

      IF  Trim(ls_actual_delv_date) > ""  Then
          ls_actual_date_upd   = "Y" 
	   Else 
			dw_1.SetItem(ll_row,"confirmation_ind",'N')
			ls_confirmed = 'N'
      End if           
//      add 11/25/1997 to allow no eta update
		If ls_eta_date = "" then 
			  ls_eta_date = "0000000000"
			  ls_eta_time = "0000"
		End if
//      add 11/25/1997 to only update eta when update ind is Y			 
		If ls_update_eta_ind <> 'Y' then 
		   ls_eta_date = space(10)
			ls_eta_time = space(4)
		End if

      ls_late_code		   = dw_1.GetItemString( ll_Row, "late_reason" )

  //    li_rtn = iu_otr004.nf_otrt48ar(ls_load_key, ls_stop_code, &
   //            ls_eta_date, ls_eta_time, ls_actual_delv_date, ls_actual_delv_time, &
    //           ls_late_code, lstr_Error_Info, 0)
					
					li_rtn = iu_ws_cust_service.nf_otrt48er(ls_load_key, ls_stop_code, &
               ls_eta_date, ls_eta_time, ls_actual_delv_date, ls_actual_delv_time, &
               ls_late_code, lstr_Error_Info)

     
      CHOOSE CASE li_rtn
			CASE is < 0 
	         li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
										ls_load_key + " has failed do to an Error.  Do you want to Continue?", &
										Question!, YesNo! )
            dw_1.SetFocus()
            dw_1.SetColumn("Eta_date")
            dw_1.ScrollToRow(ll_Row)

            if ll_row = ll_rowcount then
               lb_retry = true
            end if
         
         CASE 42
            ls_confirmed = "N"
            ls_actual_date_upd  = "N"
				If iu_otr004.ii_messagebox_rtn = 3 Then
                li_rc = 2
            End if

   		CASE ELSE
           li_rc = 0
                  
     	END CHOOSE

		IF li_rc = 2 THEN 			 
         dw_1.SetFocus()
         dw_1.SetColumn("Eta_date")
         dw_1.ScrollToRow(ll_Row)
			dw_1.Setredraw(TRUE)
         iw_frame.SetMicroHelp( "Ready...")
			Return(False)
      ELSE
         If li_rc = 0 Then 
            If Trim(ls_confirmed) = "Y"   or   ls_actual_date_upd  = "Y"  Then
                 dw_1.DeleteRow(ll_Row) 
                 ll_Row --
                 ll_RowCount --   
            Else
						dw_1.SetItem(ll_row,"update_eta_ind","N")	
				End if         
         End if
      End If
		
 ll_Row = dw_1.GetNextModified(ll_Row, PRIMARY!) 
LOOP

dw_1.SetRedraw(True)

Return TRUE

end function

public function integer wf_changed ();/*
	purpose:	this function will examine all datawindow controls on the window
				that have update capability against the database.  if anything has changed,
				and the change is valid, a 1 is returned.  if an invalid change has occurred
				then a -1 is returned.  if nothing has changed a 0 is returned.

*/

int i, totcontrols, rc, counter
datawindow dw[]

totcontrols = UpperBound(this.control)

FOR i = 1 to totcontrols
	IF TypeOf(this.control[i]) = datawindow! THEN
		counter ++
		dw[counter] = this.control[i]
			rc = dw[counter].AcceptText()
			IF rc = -1 THEN 
				RETURN -1
			ELSE
				CONTINUE
			END IF
			counter --
	END IF
NEXT

FOR i = 1 to counter
	IF dw[i].ModifiedCount() > 0 THEN
		is_closemessage_txt = "Save Changes?"
		RETURN 1
	END IF
NEXT

RETURN 0
	
end function

public function boolean wf_presave ();date      ld_work_date

integer   li_late_reason_err_ind

long      ll_Row

string    ls_work_date, &
          ls_work_time, &
			 ls_eta_date, &
			 ls_confirm_ind

time      lt_work_time


ll_Row = 0

dw_1.AcceptText()

ll_Row = dw_1.GetNextModified(ll_Row, PRIMARY!)

Do while ll_Row <> 0
      ld_work_date = dw_1.GetItemDate(ll_Row, "eta_date")
      ls_eta_date = String(ld_work_date,"mm/dd/yyyy")
//      If IsDate(ls_eta_date)= FALSE Then          
//        	MessageBox("E.T.A. Date", "Please enter a valid E.T.A. Date")
//         dw_1.SetColumn("eta_date")
//			Return FALSE     
//      Else
         lt_work_time = dw_1.GetItemTime(ll_Row, "eta_time")
         ls_work_time = String(lt_work_time,"hhmm")
//         If IsNull(ls_work_time) = TRUE or String(ls_work_time) = "" then
//             MessageBox("E.T.A. Time", "Please enter a valid E.T.A. Time")
//             dw_1.SetColumn("eta_Time")
//			    Return FALSE    
//         End if         
//      End if

      ld_work_date = dw_1.GetItemDate(ll_Row, "actual_delv_date")
      ls_work_date = String(ld_work_date,"mm/dd/yyyy")
      lt_work_time = dw_1.GetItemTime(ll_Row, "actual_delv_time")
      ls_work_time = String(lt_work_time,"hhmm")

      If ls_work_date > "01/01/1900" Then
         If IsDate(ls_work_date)= FALSE or ls_work_date <= "01/01/1900" Then          
          	MessageBox("Actual Delivery Date", "Please enter a valid Actual Delivery Date")
            dw_1.SetColumn("actual_delv_date")
	   		Return FALSE     
         Else
            If IsNull(ls_work_time) = TRUE or String(ls_work_time) = "0000" or String(ls_work_time) = "" then
               MessageBox("Actual Delivery Time", "Please enter an Actual Delivery Time")
               dw_1.SetColumn("actual_delv_Time")
			      Return FALSE    
            End if         
         End if
      End if
		
 ls_confirm_ind = dw_1.GetItemString(ll_Row, "confirmation_ind")
 If ls_confirm_ind = "Y" Then
	If ls_work_date > "01/01/1900" or ls_eta_date > "01/01/1900" Then
	Else
		 	MessageBox("Confirmation Clicked", "Please enter an ETA Date or Actual Delivery Date")
            dw_1.SetColumn("actual_delv_date")
	   		Return FALSE     
	End if
End if

      li_late_reason_err_ind = dw_1.GetItemNumber(ll_Row, "late_reason_err_ind")
      If li_late_reason_err_ind = 1 Then
     	  MessageBox("Late Reason Code Error", "Please enter a valid Late Reason Code") 
        dw_1.SetColumn("late_reason")     
        Return FALSE
      End if
ll_Row = dw_1.GetNextModified(ll_Row, PRIMARY!)
Loop 

Return TRUE
end function

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer

 
li_rc = wf_Changed()

CHOOSE CASE li_rc	//begin 1
	CASE  1	//	a valid change occurred ...
		li_answer = MessageBox("Wait",is_closemessage_txt,question!,yesnocancel!)
		CHOOSE CASE li_answer
			CASE 1	//	yes, save changes
				IF wf_Update() THEN	//	the save succeeded
				ELSE	//	the save failed
					li_answer = MessageBox("Save Failed","Retrieve Anyway?",question!,yesno!)
					IF li_answer = 1 THEN	//	retrieve w/o saving changes
					ELSE	//	cancel the close
						Return( false )
					END IF
				END IF	
			CASE 2 	//	retrieve w/o saving changes
			CASE 3	//	cancel the retrieve
				Return(false)
		END CHOOSE
	CASE  -1 		//	a validation error occurred
		li_answer = MessageBox("Data Entry Error","Retrieve w/o Save?",question!,okcancel!)
		CHOOSE CASE li_answer
			CASE 1	//	retrieve w/o saving changes
			CASE 2	//	cancel the retrieve
				Return( false )
		END CHOOSE
	CASE  -2 	//	a serious validation error, window may not close in this state
		MessageBox("A Serious Error Has Occurred","Correct Error Before Retrieving!",exclamation!,ok!)
		Return(False)
	CASE ELSE	//	nothing has changed
END CHOOSE

If is_from_menu_ind = "Y" Then
	OpenWithParm( w_mass_conf_filter, iuo_mass_conf, iw_frame )
	iuo_mass_conf = Message.PowerObjectParm

	If isvalid(iuo_mass_conf) = False then
		Return True
	End If
Else
	iuo_mass_conf.start_carrier_code	= is_start_carrier
	iuo_mass_conf.end_carrier_code	= is_end_carrier
	iuo_mass_conf.delv_date 			= is_delv_date
	//ibdkdld
	iuo_mass_conf.division				= is_division
	iuo_mass_conf.cancel					= 'F'
End If

IF iuo_mass_conf.cancel = 'T'  Then
     Return True
End If

// Turn the redraw off for the window while we retrieve
This.SetRedraw(FALSE)

// Populate the datawindows
This.wf_mass_conf_inq ()

// Turn the redraw off for the window while we retrieve
This.SetRedraw(TRUE)

Return FALSE




end function

public function integer wf_mass_conf_inq ();integer               li_rtn				        

string                ls_mass_conf_inq_string, &
                      ls_work_date, &
                      ls_work_yyyy,  &
                      ls_work_mm, &
                      ls_work_dd, &
                      ls_req_start_carrier_code, &
                      ls_req_end_carrier_code, &
                      ls_req_delv_date, & 
                      ls_refetch_carrier_code, &
                      ls_refetch_load_key, &
                      ls_refetch_stop_code, ls_division

s_error               lstr_Error_Info



dw_1.Reset()
SetPointer( HourGlass! )

ls_req_start_carrier_code    = iuo_mass_conf.start_carrier_code
ls_req_end_carrier_code      = iuo_mass_conf.end_carrier_code
ls_req_delv_date             = iuo_mass_conf.delv_date 
//ibdkdld
ls_division 					  = iuo_mass_conf.division
is_division = ls_division

lstr_error_info.se_event_name = "wf_mass_conf_inq"
lstr_error_info.se_window_name = "w_mass_conf"
lstr_error_info.se_procedure_name = "wf_mass_conf_inq"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ls_mass_conf_inq_string	= Space(4851)
ls_refetch_carrier_code = Space(4)
ls_refetch_load_key     = Space(7)
ls_refetch_stop_code    = Space(2)
// ibdkdld
//li_rtn = iu_otr004.nf_otrt47ar(ls_req_start_carrier_code, &
//         ls_req_end_carrier_code, ls_req_delv_date, ls_division, ls_mass_conf_inq_string, &
//         ls_refetch_carrier_code, ls_refetch_load_key, ls_refetch_stop_code, &
//         lstr_error_info, 0)
			
li_rtn = iu_ws_cust_service.nf_otrt47er(ls_req_start_carrier_code, &
         ls_req_end_carrier_code, ls_req_delv_date, ls_division, ls_mass_conf_inq_string, &
         lstr_Error_Info)			

// Check the return code from the above function call
IF li_rtn <> 0 THEN
    return(-1)
ELSE
    iw_frame.SetMicroHelp( "Ready...")
END IF

li_rtn = dw_1.ImportString(Trim(ls_mass_conf_inq_string))
If li_rtn < 1 Then iw_frame.SetMicroHelp("No Records Found")

DO WHILE Trim(ls_refetch_load_key) > ""

	ls_mass_conf_inq_string	= Space(4851)
 // ibdkdld
 // 	li_rtn = iu_otr004.nf_otrt47ar(ls_req_start_carrier_code, &
 //        ls_req_end_carrier_code, ls_req_delv_date, ls_division,ls_mass_conf_inq_string, &
 //        ls_refetch_carrier_code, ls_refetch_load_key, ls_refetch_stop_code, &
 //        lstr_error_info, 0)
			
	li_rtn = iu_ws_cust_service.nf_otrt47er(ls_req_start_carrier_code, &
         ls_req_end_carrier_code, ls_req_delv_date, ls_division, ls_mass_conf_inq_string, &
         lstr_Error_Info)			

    // Check the return code from the above function call
    IF li_rtn <> 0 THEN
 	     return(-1)
    ELSE
        iw_frame.SetMicroHelp( "Ready...")
    END IF

    li_rtn = dw_1.ImportString(Trim(ls_mass_conf_inq_string))
 
LOOP
 
ls_work_date = ls_req_delv_date
ls_work_yyyy = Left(ls_work_date, 4)
ls_work_mm   = Mid(ls_work_date, 5, 2)
ls_work_dd   = Mid(ls_work_date, 7, 2)
ls_work_date = ls_work_mm + "/" + ls_work_dd + "/" + ls_work_yyyy     
// ibdkdld
if is_division >  "  "  Then 
	This.Title = "Unconfirmed Deliveries for "+ls_req_start_carrier_code + " - " + ls_req_end_carrier_code  + ", " + ls_work_date + ", Division = " + is_division
Else
	This.Title = "Unconfirmed Deliveries for "+ls_req_start_carrier_code + " - " + ls_req_end_carrier_code  + ", " + ls_work_date + ", Division = ALL"
end if
dw_1.Sort()
dw_1.SetFocus()
dw_1.ScrollToRow(1)
dw_1.SetColumn("confirmation_ind")
dw_1.ResetUpdate()
		
Return(1)      


      
end function

event ue_postopen;call super::ue_postopen;iu_otr004  =  CREATE u_otr004
iu_ws_cust_service = CREATE u_ws_cust_service

string	ls_input_string

ls_input_string = Message.StringParm

If IsNull(ls_input_string) or ls_input_string = '' Then
	is_from_menu_ind = "Y"
Else
	is_from_menu_ind	= "N"
	is_start_carrier	= Left(ls_input_string, 4)
	is_end_carrier		= Mid(ls_input_string, 5, 4)
	is_delv_date		= Mid(ls_input_string, 9, 8)
	//ibdkdld
	is_division			= right(ls_input_string, 2)
End If

IF wf_retrieve() THEN Close( this )

end event

on open;call w_base_sheet_ext::open;dw_1.InsertRow(0) 




end on

event close;call super::close;DESTROY iu_otr004
DESTROY iu_ws_cust_service
end event

on w_mass_conf.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_mass_conf.destroy
call super::destroy
destroy(this.dw_1)
end on

type dw_1 from u_base_dw_ext within w_mass_conf
integer x = 5
integer y = 12
integer width = 2839
integer height = 1096
integer taborder = 10
string dataobject = "d_mass_conf"
boolean vscrollbar = true
end type

event doubleclicked;call super::doubleclicked;String          ls_load_key, &
                ls_stop_code, &
                ls_input_string, &
                ls_late_reason, &
                ls_eta_date, &
                ls_eta_time
         

If is_ObjectAtPointer  = "stop_code" Then
   ls_load_key = This.object.load_key[row]
   ls_stop_code = This.object.stop_code[row]
   ls_input_string =  ls_load_key + ls_stop_code
   OpenWithParm(w_stops_order_po_resp, ls_input_string)
End if


If is_ObjectAtPointer = "add_check_call_ind" Then 
     ls_load_key = This.object.load_key[row]
     ls_stop_code = This.object.stop_code[row]
     ls_input_string =  ls_load_key + ls_stop_code + "M"
     OpenWithParm(w_check_call_add_child, ls_input_string) 
     This.SetItem(Row, "add_check_call_ind", "X")  
     This.SelectRow(Row, TRUE)
     This.ScrollToRow(Row)    
End if


end event

event itemchanged;call super::itemchanged;Integer    li_CurrentCol 

long 			ll_FoundRow

String      ls_GetText


li_CurrentCol = This.GetColumn() 

If li_CurrentCol = 11 Then 
   dw_1.SetItem(Row, "late_reason_err_ind", 0) 
   ls_GetText = GetText()
   IF Trim(ls_GetText) = "" THEN Return

       ll_FoundRow = idwc_latecode.Find ( "type_code = '"+ls_GetText+"'", 1, idwc_latecode.RowCount() )
       IF ll_FoundRow < 1 THEN
//          MessageBox ("Late Reason Error", ls_GetText + " is not a valid late reason code.", &
//          StopSign!, OK!)
//          This.SetColumn("late_reason") 
//          dw_1.SetItem(Row, "late_reason_err_ind", 1) 
          dw_1.selecttext(1,len(data))
	       Return 1
       End if 
End If

// Added Choose case to determine if ETA date or time was changed
// added 11-25-97 to control updating eta only when changed
Choose Case dwo.Name
	Case "eta_date"  
     this.SetItem(row, "update_eta_ind", "Y") 
     this.SetItem(row, "verify_eta_ind", "N") 
   Case "eta_time"  
     this.SetItem(row, "update_eta_ind", "Y") 
     this.SetItem(row, "verify_eta_ind", "N") 
	Case "update_eta_ind"  
     this.SetItem(row, "verify_eta_ind", "N") 
End Choose
end event

event constructor;call super::constructor;ii_rc = GetChild( 'late_reason', idwc_latecode )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for late code ")

idwc_latecode.SetTrans(SQLCA)
idwc_latecode.Retrieve('LATECODE')




end event

event clicked;call super::clicked;long ll_FoundRow

This.SelectRow(0, FALSE)

If is_ObjectAtPointer = "late_reason" Then
	 string ls_text
	 ls_text = GetText()
    IF Trim(ls_text) = "" THEN Return
    ll_FoundRow = idwc_latecode.Find ( "late_reason='"+ls_text+"'", 1, idwc_latecode.RowCount() )
End If 


end event

event ue_postconstructor;call super::ue_postconstructor;ib_updateable = TRUE
end event

