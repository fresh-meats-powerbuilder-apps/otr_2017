HA$PBExportHeader$nvuo_eta_problems.sru
forward
global type nvuo_eta_problems from nonvisualobject
end type
end forward

global type nvuo_eta_problems from nonvisualobject autoinstantiate
end type

type variables
string      start_carrier_code, &
               end_carrier_code, &
					delv_date, &
					division, &
               cancel
end variables

on nvuo_eta_problems.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvuo_eta_problems.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

