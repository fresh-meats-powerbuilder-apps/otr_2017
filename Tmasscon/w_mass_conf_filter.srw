HA$PBExportHeader$w_mass_conf_filter.srw
forward
global type w_mass_conf_filter from w_base_response_ext
end type
type dw_carrier from u_base_dw_ext within w_mass_conf_filter
end type
type st_1 from statictext within w_mass_conf_filter
end type
type dw_date from u_base_dw_ext within w_mass_conf_filter
end type
type dw_division from datawindow within w_mass_conf_filter
end type
type gb_1 from groupbox within w_mass_conf_filter
end type
end forward

global type w_mass_conf_filter from w_base_response_ext
integer x = 763
integer y = 328
integer width = 1531
integer height = 648
string title = "Unconfirmed Deliveries Filter"
long backcolor = 12632256
dw_carrier dw_carrier
st_1 st_1
dw_date dw_date
dw_division dw_division
gb_1 gb_1
end type
global w_mass_conf_filter w_mass_conf_filter

type variables
uo_mass_conf	iuo_mass_conf

integer	ii_rc

DataWindowChild   idwc_carrier ,idwc_division

String is_division
end variables

event open;call super::open;String           ls_work_date, &
                 ls_work_yyyy,  &
                 ls_work_mm, &
                 ls_work_dd, &
                 ls_start_carrier, &
                 ls_end_carrier 

long             ll_row


iuo_mass_conf = Message.PowerObjectParm
dw_carrier.Object.Data = iuo_mass_conf

dw_date.InsertRow(0) 

IF iuo_mass_conf.end_carrier_code <> '' THEN 
    If iuo_mass_conf.end_carrier_code  = iuo_mass_conf.start_carrier_code  THEN
        dw_carrier.object.end_carrier_code[1] = "    "
    End if  
END IF


IF iuo_mass_conf.delv_date <> '' THEN 
      ls_work_date = iuo_mass_conf.delv_date
      ls_work_yyyy = Left(ls_work_date, 4)
      ls_work_mm   = Mid(ls_work_date, 5, 2)
      ls_work_dd   = Mid(ls_work_date, 7, 2)
      ll_Row = dw_date.GetRow()
      dw_date.SetColumn("work_date") 
      ls_work_date = ls_work_mm + "/" + ls_work_dd + "/" + ls_work_yyyy     
	   dw_date.SetText(ls_work_date)
  ELSE
      dw_date.object.work_date[1] = Today()
END IF
// ibdkdld
IF iuo_mass_conf.division <> '  ' THEN 
   dw_division.SetItem( 1, "division_code", iuo_mass_conf.division)
	is_division = 	iuo_mass_conf.division
END IF
	
	
dw_carrier.SetFocus()
dw_carrier.SetColumn("start_carrier_code")
dw_carrier.SelectText(1, 4)

end event

on w_mass_conf_filter.create
int iCurrent
call super::create
this.dw_carrier=create dw_carrier
this.st_1=create st_1
this.dw_date=create dw_date
this.dw_division=create dw_division
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_carrier
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_date
this.Control[iCurrent+4]=this.dw_division
this.Control[iCurrent+5]=this.gb_1
end on

on w_mass_conf_filter.destroy
call super::destroy
destroy(this.dw_carrier)
destroy(this.st_1)
destroy(this.dw_date)
destroy(this.dw_division)
destroy(this.gb_1)
end on

event ue_base_cancel;call super::ue_base_cancel;iuo_mass_conf.cancel = 'T'
CloseWithReturn( This, iuo_mass_conf )

end event

event ue_base_ok;call super::ue_base_ok;char                    lc_req_start_carrier_code[4], &
                        lc_req_end_carrier_code[4], &
								lc_req_delv_date[8]
String						ls_division 
		
Date							ld_work_date
		
If dw_carrier.AcceptText() = 1  & 
	and dw_date.AcceptText() = 1 & 
	and dw_division.AcceptText() = 1 then

   lc_req_start_carrier_code[]	= " "
   lc_req_end_carrier_code[]	= " "
     
   lc_req_start_carrier_code	= trim( dw_carrier.object.start_carrier_code[1])
   lc_req_end_carrier_code	= trim( dw_carrier.object.end_carrier_code[1])

   If Trim(lc_req_end_carrier_code) = "" Then
      lc_req_end_carrier_code = lc_req_start_carrier_code
   End if
  
   ld_work_date 	=  dw_date.GetItemDate( 1, "work_date" )
   lc_req_delv_date = String(ld_work_date, "yyyymmdd")
	// ibdkdld
	If IsNull(is_division) Then
		is_division = "  "
	End If

   iuo_mass_conf.start_carrier_code	= lc_req_start_carrier_code
   iuo_mass_conf.end_carrier_code	= lc_req_end_carrier_code
   iuo_mass_conf.delv_date       	= lc_req_delv_date
//ibdkdld
   iuo_mass_conf.division	       	= is_Division
   iuo_mass_conf.cancel             = 'F'
	

   CloseWithReturn( This, iuo_mass_conf )
   Return

End if 

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_mass_conf_filter
integer x = 997
integer y = 408
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_mass_conf_filter
integer x = 603
integer y = 408
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_mass_conf_filter
integer x = 206
integer y = 408
integer taborder = 40
end type

type dw_carrier from u_base_dw_ext within w_mass_conf_filter
integer x = 73
integer y = 108
integer width = 882
integer height = 164
integer taborder = 10
string dataobject = "d_dddw_start_end_carriers"
boolean border = false
end type

event itemchanged;call super::itemchanged;long ll_FoundRow

IF Trim(data) = "" THEN Return

IF dwo.name = "start_carrier_code" Then
   ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
   IF ll_FoundRow < 1 THEN
	  MessageBox( "Carrier Error", data + " is Not a Valid Carrier Code." )
	  dw_carrier.SetItem(1,"start_carrier_code", "")
	  dw_carrier.SetFocus()  
     dw_carrier.SetColumn("start_carrier_code")
     Return 1
   END IF		
End if

IF dwo.name = "end_carrier_code" Then
   ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
   IF ll_FoundRow < 1 THEN
  	  MessageBox( "Carrier Error", data + " is Not a Valid Carrier Code." )
     dw_carrier.SetItem(1,"end_carrier_code", "")
     dw_carrier.SetFocus()  
     dw_carrier.SetColumn("end_carrier_code")
	  Return 1
   END IF		
End if


end event

event constructor;call super::constructor;InsertRow(0)

ii_rc = GetChild( 'start_carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()

ii_rc = GetChild( 'end_carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()

end event

event itemerror;call super::itemerror;Return 1
end event

type st_1 from statictext within w_mass_conf_filter
integer x = 983
integer y = 72
integer width = 261
integer height = 60
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Delivery Date"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_date from u_base_dw_ext within w_mass_conf_filter
integer x = 978
integer y = 128
integer width = 430
integer height = 92
integer taborder = 20
string dataobject = "d_work_date"
boolean border = false
end type

type dw_division from datawindow within w_mass_conf_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 992
integer y = 204
integer width = 393
integer height = 168
integer taborder = 30
boolean bringtotop = true
string title = "none"
string dataobject = "d_dddw_divisions"
boolean border = false
boolean livescroll = true
end type

event ue_dwndropdown;Long ll_row

ll_row = idwc_division.Find( "type_code ='" + GetText() + "'", 1, idwc_division.RowCount() )

If (ll_row = 0) Then
	idwc_division.SetTrans(SQLCA)
	idwc_division.Retrieve('DIVCODE')
Else 
	idwc_division.ScrollToRow( ll_row )
	idwc_division.SelectRow( ll_row, True )
End If

end event

event constructor;insertRow(0)
ii_rc = GetChild( 'division_code', idwc_division )
IF ii_rc = -1 THEN 
	MessageBox("DataWindwoChild Error", "Unable to get Child Handle for division_code.")
else
	idwc_division.SetTrans(SQLCA)
	idwc_division.Retrieve('DIVCODE')
End IF
end event

event itemchanged;long ll_FoundRow
iw_frame.SetMicroHelp(dwo.name)
IF Trim(data) = "" THEN 
	is_division = data
	Return
End if

ll_FoundRow = idwc_division.Find ( "type_code='"+data+"'", 1, idwc_division.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Division Error", data + " is Not a Valid Division Code." )
	Return 1
else
	is_division = data
	
END IF		

end event

event getfocus;THIS.SelectText (1,99)
end event

event itemerror;Return 1
end event

event itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)


end event

type gb_1 from groupbox within w_mass_conf_filter
integer x = 64
integer y = 40
integer width = 910
integer height = 268
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Carriers"
end type

