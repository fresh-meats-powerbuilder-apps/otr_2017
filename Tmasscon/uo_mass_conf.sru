HA$PBExportHeader$uo_mass_conf.sru
forward
global type uo_mass_conf from nonvisualobject
end type
end forward

global type uo_mass_conf from nonvisualobject autoinstantiate
end type

type variables
String	start_carrier_code, &
	end_carrier_code, &
	delv_date, &
	division, &
	cancel
end variables

on uo_mass_conf.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_mass_conf.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

