HA$PBExportHeader$w_unknown_eta_problems.srw
forward
global type w_unknown_eta_problems from w_base_sheet_ext
end type
type dw_1 from u_base_dw_ext within w_unknown_eta_problems
end type
end forward

global type w_unknown_eta_problems from w_base_sheet_ext
integer width = 2999
string title = "Unknown ETA Problems"
long backcolor = 12632256
dw_1 dw_1
end type
global w_unknown_eta_problems w_unknown_eta_problems

type variables
u_otr005		iu_otr005
u_ws_cust_service iu_ws_cust_service

string		is_closemessage_txt
string                        is_problem_type

long       il_RowCount, &
             il_Row

integer	ii_rc

nvuo_eta_problems 	invuo_eta_problems

end variables

forward prototypes
public function boolean wf_retrieve ()
public function integer wf_changed ()
public function integer wf_eta_problems_inq ()
end prototypes

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer

invuo_eta_problems.cancel = 'U'
OpenWithParm( w_unknown_eta_filter, invuo_eta_problems, iw_frame )
invuo_eta_problems = Message.PowerObjectParm

IF isvalid(invuo_eta_problems)= false then
   return False
end if

IF invuo_eta_problems.cancel = 'T'  Then
     Return True
End If

// Turn the redraw off for the window while we retrieve
This.SetRedraw(FALSE)

// Populate the datawindows
This.wf_eta_problems_inq ()

// Turn the redraw off for the window while we retrieve
This.SetRedraw(TRUE)

Return FALSE


end function

public function integer wf_changed ();/*
	purpose:	this function will examine all datawindow controls on the window
				that have update capability against the database.  if anything has changed,
				and the change is valid, a 1 is returned.  if an invalid change has occurred
				then a -1 is returned.  if nothing has changed a 0 is returned.

*/

int i, totcontrols, rc, counter
datawindow dw[]

totcontrols = UpperBound(this.control)

FOR i = 1 to totcontrols
	IF TypeOf(this.control[i]) = datawindow! THEN
		counter ++
		dw[counter] = this.control[i]
			rc = dw[counter].AcceptText()
			IF rc = -1 THEN 
				RETURN -1
			ELSE
				CONTINUE
			END IF
			counter --
	END IF
NEXT

FOR i = 1 to counter
	IF dw[i].ModifiedCount() > 0 THEN
		is_closemessage_txt = "Save Changes?"
		RETURN 1
	END IF
NEXT

RETURN 0
	
end function

public function integer wf_eta_problems_inq ();integer               li_rtn				        

string                ls_problem_string, &
                      ls_work_date, &
                      ls_work_yyyy,  &
                      ls_work_mm, &
                      ls_work_dd, &
                      ls_req_from_carrier, &
                      ls_req_to_carrier, &
                      ls_refetch_problem_type, &
                      ls_refetch_carrier, &
                      ls_refetch_load_key, &
                      ls_refetch_stop_code
                      
s_error               lstr_Error_Info


SetPointer(HourGlass!)
dw_1.Reset()
// Turn the redraw off for the window, while we retrieve
This.SetRedraw(False)

ls_req_from_carrier    = invuo_eta_problems.start_carrier_code
ls_req_to_carrier      = invuo_eta_problems.end_carrier_code

lstr_error_info.se_event_name = "wf_eta_problems_inq"
lstr_error_info.se_window_name = "w_customer_problems"
lstr_error_info.se_procedure_name = "wf_eta_problems_inq"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ls_problem_string	      = Space(20000)
ls_refetch_problem_type = Space(1)
ls_refetch_carrier      = Space(4)
ls_refetch_load_key     = Space(7)
ls_refetch_stop_code    = Space(2)

Do
	
  ls_problem_string = Space(20000)
  
//  li_rtn = iu_otr005.nf_otrt63ar(ls_req_from_carrier, ls_req_to_carrier, &
//	  		ls_refetch_problem_type, ls_refetch_carrier, ls_refetch_load_key, &
//		   ls_refetch_stop_code, ls_problem_string, lstr_error_info, 0)
			
	 li_rtn = iu_ws_cust_service.nf_otrt63er(ls_req_from_carrier, ls_req_to_carrier, &
	  		 ls_problem_string,lstr_Error_Info)		
						
	Choose Case li_rtn
		Case 0
			iw_frame.SetMicroHelp( "Ready...")
//		Case 1
//			iw_frame.SetMicroHelp(lstr_error_info.se_message)
//			Return(-1)
		Case Else
			iw_frame.SetMicroHelp(lstr_error_info.se_message)
			SetPointer(Arrow!)
			dw_1.SetReDraw(True)
			Return(-1)
	End Choose
		
	li_rtn = dw_1.ImportString( Trim( ls_problem_string ) )
	
//Loop While Trim(ls_refetch_problem_type) > ""
 
Loop While ( Len( Trim( ls_refetch_problem_type ) ) > 0 )


 
This.Title = "Unknown ETA Problems for "+ls_req_from_carrier + " - " + ls_req_to_carrier 


dw_1.Sort()
dw_1.SetFocus()
dw_1.ScrollToRow(1)
dw_1.ResetUpdate()
dw_1.SetRedraw(True)
SetPointer(Arrow!)
		
Return(1)      


      
end function

on w_unknown_eta_problems.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_unknown_eta_problems.destroy
call super::destroy
destroy(this.dw_1)
end on

event close;call super::close;DESTROY iu_otr005
DESTROY iu_ws_cust_service
end event

event ue_postopen;call super::ue_postopen;iu_otr005  =  CREATE u_otr005
iu_ws_cust_service = CREATE u_ws_cust_service

IF wf_retrieve() THEN Close( this )










end event

event resize;call super::resize;dw_1.resize(workspacewidth(), workspaceheight())
//dw_1.resize(workspacewidth() - 1, workspaceheight() - 1)
end event

event open;call super::open;dw_1.InsertRow(0)

end event

type dw_1 from u_base_dw_ext within w_unknown_eta_problems
integer y = 12
integer width = 2958
integer height = 1312
string dataobject = "d_cust_problem_review"
boolean vscrollbar = true
end type

