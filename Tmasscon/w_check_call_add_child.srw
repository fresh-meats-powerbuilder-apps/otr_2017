HA$PBExportHeader$w_check_call_add_child.srw
forward
global type w_check_call_add_child from w_base_response
end type
type dw_check_call_add from u_base_dw_ext within w_check_call_add_child
end type
end forward

global type w_check_call_add_child from w_base_response
integer x = 37
integer y = 288
integer width = 2843
integer height = 572
string title = "Check Call Add"
long backcolor = 12632256
dw_check_call_add dw_check_call_add
end type
global w_check_call_add_child w_check_call_add_child

type variables
u_otr003              iu_otr003
u_otr004              iu_otr004
u_ws_cust_service iu_ws_cust_service

Character            ic_load_key[7], &
                          ic_stop_code[2], &
                          ic_calling_program[1]

DataWindowChild idwc_latecode 

Integer                ii_rc

String                  is_late_reason, &
                          is_eta_date, &
                          is_eta_time
end variables

forward prototypes
public function boolean wf_add_check_call ()
public function boolean wf_presave ()
end prototypes

public function boolean wf_add_check_call ();boolean           lb_return                  

Date              ld_work_date

Integer           li_rtn, &
						li_rc
               
Time              lt_work_time        

dwItemStatus		status

s_error				lstr_Error_Info


string  ls_disp_date, &
        ls_disp_time, &
        ls_late_reason, &
        ls_string, &
  		  ls_load_key, &
		  ls_stop_code, &
	     ls_check_call_date, &
		  ls_check_call_time, &
		  ls_check_call_city, &
		  ls_check_call_state, &
		  ls_late_reason_code, &
	     ls_eta_date, &
		  ls_eta_time, &
        ls_no_eta_ind, &
        ls_eta_contact, &
        ls_miles_out,  &
        ls_update_date, &
        ls_update_time


lstr_error_info.se_event_name = "wf_add_check_call"
lstr_error_info.se_window_name = "w_check_call_add_child"
lstr_error_info.se_procedure_name = "wf_add_check_call"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

IF dw_check_call_add.AcceptText() = -1  Then
   Return (False)
End if

lb_return = wf_presave()

If lb_return = FALSE Then
   Return False
Else
   ls_load_key	        = ic_load_key        
   ls_stop_code 	     = dw_check_call_add.object.stop_code[1]
   ld_work_date        = dw_check_call_add.GetItemDate( 1, "cc_date" )
   ls_check_call_date  = String(ld_work_date, "yyyymmdd")
   lt_work_time        = dw_check_call_add.GetItemTime( 1, "cc_time" )
   ls_check_call_time  = String(lt_work_time, "hhmm")    
   ls_check_call_city  = dw_check_call_add.object.city[1]
   ls_check_call_state = dw_check_call_add.object.state[1]
   ls_late_reason_code = dw_check_call_add.object.late_reason[1]
   ls_late_reason      = ls_late_reason_code
   ls_miles_out        = dw_check_call_add.object.miles_out[1]
   ls_no_eta_ind       = dw_check_call_add.object.no_eta_ind[1]
   If ls_no_eta_ind = "X" Then
      ls_eta_date      = "00000000"            
      ls_eta_time      = "0000"
      ls_disp_date     = "00/00/0000"
      ls_disp_time     = "00:00"
   Else
      ld_work_date     = dw_check_call_add.GetItemDate( 1, "eta_date" )
      ls_disp_date     = String(ld_work_date, "mm/dd/yyyy")
      ls_eta_date      = String(ld_work_date, "yyyymmdd") 
      lt_work_time     = dw_check_call_add.GetItemTime( 1, "eta_time" )
      ls_disp_time     = String(lt_work_time, "hh:mm")
      ls_eta_time      = String(lt_work_time, "hhmm")
   End if
   ls_eta_contact      = dw_check_call_add.object.eta_contact[1]
 
   IF ic_calling_program[1] = "M" Then

      ls_late_reason = ls_late_reason + space(8 - Len(ls_late_reason))
      ls_string =  ls_late_reason +  ls_disp_date + ls_disp_time

       
      If Trim(ls_string) > ""  Then
          IF Trim(ls_late_reason) = "" Then  
             ls_late_reason = "        "
          End if
   
          Message.StringParm = ls_string  
          iw_frame.iw_active_sheet.TriggerEvent("ue_set_values")
      End if
  
	 END  IF	


					 
	    li_rtn = iu_ws_cust_service.nf_otrt52er(ls_load_key, ls_stop_code, &
                ls_check_call_date, ls_check_call_time, ls_check_call_city, &
                ls_check_call_state, ls_late_reason_code, ls_miles_out, &
                ls_eta_date, ls_eta_time, ls_eta_contact, lstr_Error_Info)				 
					 
					 

         dw_check_call_add.SetItemStatus ( 1, 0, Primary!, NotModified! )
	   	CHOOSE CASE li_rtn
			CASE is < 0 
				dw_check_call_add.Setredraw(TRUE)
				li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
										"  Load Key -  " + ls_load_key + " has failed do to an Error." , &
										Information!, OK! )
            dw_check_call_add.SetFocus()
            dw_check_call_add.SetColumn("eta_date") 
				Return (False)

         CASE  1 
            li_rc = 0
				
			CASE 2
            dw_check_call_add.SetRedraw( True )
				li_rc = MessageBox( "UNABLE TO CALCULATE MILEAGE",  &
										"Please enter the Check Call miles out and save the record again.", &
										Information!, OK! )
            dw_check_call_add.SetFocus()
            dw_check_call_add.SetColumn("miles_out") 
				Return (False)

			CASE 3
            dw_check_call_add.SetRedraw( True )
				li_rc = MessageBox(  "INVALID ETA DATE",  &
					 "ETA Date Cannot be less than Scheduled Ship Date.", &
							Information!, OK! )
            dw_check_call_add.SetFocus()
            dw_check_call_add.SetColumn("eta_date") 
				Return (False)
				
			CASE  10
            dw_check_call_add.SetRedraw( True )
            dw_check_call_add.SetFocus()
            dw_check_call_add.SetColumn("miles_out") 
          	Return (False)


       	CASE ELSE
				li_rc = 0
		END CHOOSE

        IF li_rc = 1 THEN
           li_rc = 0
        END IF

		  IF li_rc = 2 THEN 			// NO  do not continue processing
			  dw_check_call_add.Setredraw(TRUE)
		     Return(False)
		  END IF
 End if

dw_check_call_add.ResetUpdate()

Return TRUE





end function

public function boolean wf_presave ();date		 ld_cc_date
		  
integer   li_late_reason_err_ind       

long      ll_row

string	 ls_cc_city, &
          ls_cc_state, &
          ls_stop_code

time      lt_cc_time



dw_check_call_add.AcceptText()

ll_Row = 0
ll_Row = dw_check_call_add.GetNextModified(ll_Row, PRIMARY!)

Do While ll_Row <> 0
	ls_stop_code = dw_check_call_add.object.stop_code[ll_row]
   If IsNull(ls_stop_code) Then
    	MessageBox("Stop Code", "Please enter a Stop Code") 
      dw_check_call_add.SetColumn("stop_code")     
	   Return FALSE
   End if
   li_late_reason_err_ind = dw_check_call_add.GetItemNumber(ll_Row, "late_reason_err_ind")
   If li_late_reason_err_ind = 1 Then
      MessageBox("Late Reason Code Error", "Please enter a valid Late Reason Code") 
      dw_check_call_add.SetColumn("late_reason")     
	   Return FALSE
   End if
   ld_cc_date = dw_check_call_add.GetItemDate(ll_Row, "cc_date")
   If IsNull(ld_cc_date) Then
    	MessageBox("Check Call Date", "Please enter a Check Call Date") 
      dw_check_call_add.SetColumn("cc_date")     
    Return FALSE
   End if
   If IsDate(String(ld_cc_date)) = FALSE then      
     	MessageBox("Check Call Date", "Please enter a Check Call Date") 
      dw_check_call_add.SetColumn("cc_date")   
      Return FALSE
   End if
   lt_cc_time = dw_check_call_add.GetItemTime(ll_Row, "cc_time")
   If IsTime(String(lt_cc_time)) = FALSE then
    	MessageBox("Check Call Time", "Please enter a Check Call Time") 
      dw_check_call_add.SetColumn("cc_time")     
    Return FALSE
   End if
   If String(lt_cc_time) = "00:00:00"  then
    	MessageBox("Check Call Time", "Please enter a Check Call Time") 
      dw_check_call_add.SetColumn("cc_time")     
	   Return FALSE
   End if
   ls_cc_city = dw_check_call_add.object.city[ll_row]
   If IsNull(ls_cc_city)  then
      MessageBox("Check Call City", "Please enter a Check Call City") 
      dw_check_call_add.SetColumn("city") 
      Return FALSE    
   End if 
   If ls_cc_city = "                         "  then
      MessageBox("Check Call City", "Please enter a Check Call City") 
      dw_check_call_add.SetColumn("city") 
      Return FALSE    
   End if  
   ls_cc_state = dw_check_call_add.object.state[ll_row]
   If IsNull(ls_cc_state)  then
      MessageBox("Check Call State", "Please enter a Check Call State") 
      dw_check_call_add.SetColumn("state") 
      Return FALSE    
   End if 
   If ls_cc_state = "  "  then
      MessageBox("Check Call State", "Please enter a Check Call State") 
      dw_check_call_add.SetColumn("state") 
      Return FALSE    
   End if  
 ll_Row = dw_check_call_add.GetNextModified(ll_Row, PRIMARY!)

Loop

Return TRUE
end function

on w_check_call_add_child.create
int iCurrent
call super::create
this.dw_check_call_add=create dw_check_call_add
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_check_call_add
end on

on w_check_call_add_child.destroy
call super::destroy
destroy(this.dw_check_call_add)
end on

event close;call super::close;DESTROY iu_otr003
DESTROY iu_otr004
DESTROY iu_ws_cust_service
end event

event open;call super::open;iu_otr003  =  CREATE u_otr003
iu_otr004  =  CREATE u_otr004
iu_ws_cust_service = CREATE u_ws_cust_service
 
string        ls_input_string, &
              ls_temp
              

ls_input_String = Message.StringParm

ls_temp = Left(ls_input_string, 7)	
ic_load_key =  ls_temp

ls_temp =  Mid(ls_input_string, 8, 2)	
ic_stop_code =  ls_temp

ls_temp = Mid(ls_input_string, 10, 1)
ic_calling_program = ls_temp

This.Title = "Check Call Add for Load Key "+ic_load_key[] + " Stop " + ic_stop_code[]  

dw_check_call_add.InsertRow(0) 
dw_check_call_add.SetFocus()

dw_check_call_add.SetItem(1, "stop_code", ic_stop_code)
dw_check_call_add.SetItem(1, "cc_date", Today())

dw_check_call_add.Describe("stop_code.Protect")
dw_check_call_add.Modify("stop_code.Protect=1")

dw_check_call_add.SetColumn("cc_date")
end event

event ue_base_ok;call super::ue_base_ok;boolean        lb_return

Integer        li_rc

long       ll_count


IF dw_check_call_add.AcceptText() = -1 Then  Return


ll_count = dw_check_call_add.ModifiedCount()

IF dw_check_call_add.ModifiedCount( ) > 0 THEN 
	li_rc = MessageBox( "Modified Data" , "Save changes before closing ?", &
           Question! , YesNoCancel! )
ELSE
   Close ( w_check_call_add_child)
   iw_frame.SetMicroHelp ("Ready")
END IF 

IF li_rc = 1 Then
   lb_return = wf_add_check_call()
   If lb_return = TRUE Then
       iw_frame.SetMicroHelp ("Ready")
       Close ( w_check_call_add_child)
   End if
ELSE
  IF li_rc = 2 Then
     Close ( w_check_call_add_child)
  END IF
END IF
end event

event ue_base_cancel;call super::ue_base_cancel;close(w_check_call_add_child)
end event

type cb_base_help from w_base_response`cb_base_help within w_check_call_add_child
integer x = 1618
integer taborder = 40
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_check_call_add_child
integer x = 1271
integer y = 340
integer taborder = 30
end type

type cb_base_ok from w_base_response`cb_base_ok within w_check_call_add_child
integer x = 923
integer y = 340
integer taborder = 20
end type

type dw_check_call_add from u_base_dw_ext within w_check_call_add_child
integer y = 48
integer width = 2830
integer height = 252
integer taborder = 10
string dataobject = "d_load_det_last_check_call"
boolean border = false
end type

event itemchanged;call super::itemchanged;Character  lc_stop_code[2] 

Long       ll_FoundRow 


If dwo.id = '2' Then
   IF Trim(data) = "" THEN 
       Return
   End if
   IF data = "00"  Then
       MessageBox ("Error", "Stop Code entered must be greater than zero.", &
       StopSign!, OK!)
       This.SetColumn("stop_code")
       return          
   END IF
End if



If dwo.id = '7' Then
   IF Trim(data) = "" THEN 
      Return
   End if
   ll_FoundRow = idwc_latecode.Find ( "type_code = '"+data+"'", 1, idwc_latecode.RowCount() )
   dw_check_call_add.SetItem(1, "late_reason_err_ind", 0) 
   IF ll_FoundRow < 1 THEN
      dw_check_call_add.SetItem(1, "late_reason_err_ind", 1) 
      MessageBox ("Late Reason Error", data + " is not a valid late reason code.", &
          StopSign!, OK!)
      This.SetColumn("late_reason") 
	   Return 1
   END IF		
End if
end event

event clicked;call super::clicked;long       ll_FoundRow

If is_ObjectAtPointer = "late_reason" Then
	 string ls_text
	 ls_text = GetText()
	 IF Trim(ls_text) = "" THEN Return
    ll_FoundRow = idwc_latecode.Find ( "late_reason='"+ls_text+"'", 1, idwc_latecode.RowCount() )
End If 

 

end event

event constructor;call super::constructor;ii_rc = GetChild( 'late_reason', idwc_latecode )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for late code ")

idwc_latecode.SetTrans(SQLCA)
idwc_latecode.Retrieve('LATECODE')




end event

event itemerror;call super::itemerror;Return 1 
end event

event ue_postconstructor;call super::ue_postconstructor;ib_updateable = TRUE
end event

