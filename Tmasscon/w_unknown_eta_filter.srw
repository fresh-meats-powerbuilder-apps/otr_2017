HA$PBExportHeader$w_unknown_eta_filter.srw
forward
global type w_unknown_eta_filter from w_base_response_ext
end type
type dw_1 from u_base_dw_ext within w_unknown_eta_filter
end type
type gb_1 from groupbox within w_unknown_eta_filter
end type
end forward

global type w_unknown_eta_filter from w_base_response_ext
integer width = 1193
integer height = 812
string title = "Unknown ETA Problems"
long backcolor = 12632256
dw_1 dw_1
gb_1 gb_1
end type
global w_unknown_eta_filter w_unknown_eta_filter

type variables
nvuo_eta_problems		invuo_eta_problems

integer	ii_rc

DataWindowChild   idwc_carrier 

end variables

on w_unknown_eta_filter.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.gb_1
end on

on w_unknown_eta_filter.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.gb_1)
end on

event open;call super::open;String           ls_start_carrier, &
                 ls_end_carrier 

long             ll_row


invuo_eta_problems = Message.PowerObjectParm

If isvalid(invuo_eta_problems) = true Then
   dw_1.Object.Data = invuo_eta_problems
   IF invuo_eta_problems.end_carrier_code <> '' THEN 
       If invuo_eta_problems.end_carrier_code  = invuo_eta_problems.start_carrier_code  THEN
          dw_1.object.end_carrier_code[1] = "    "
       End if  
   END IF
End If


dw_1.SetFocus()
dw_1.SetColumn("start_carrier_code")
dw_1.SelectText(1, 4)

end event

event ue_base_cancel;call super::ue_base_cancel;invuo_eta_problems.cancel = 'T'
CloseWithReturn( This, invuo_eta_problems )

end event

event ue_base_ok;call super::ue_base_ok;char                    lc_req_start_carrier_code[4], &
                        lc_req_end_carrier_code[4]
		

If dw_1.AcceptText() = 1 then

   lc_req_start_carrier_code[]	= " "
   lc_req_end_carrier_code[]	= " "
     
   lc_req_start_carrier_code	= trim( dw_1.object.start_carrier_code[1])
   lc_req_end_carrier_code	= trim( dw_1.object.end_carrier_code[1])

   If Trim(lc_req_end_carrier_code) = "" Then
      lc_req_end_carrier_code = lc_req_start_carrier_code
   End if
  
   invuo_eta_problems.start_carrier_code	= lc_req_start_carrier_code
   invuo_eta_problems.end_carrier_code	   = lc_req_end_carrier_code
   invuo_eta_problems.cancel              = 'F'

   CloseWithReturn( This, invuo_eta_problems )
   Return

End if 

end event

event closequery;Message.PowerObjectParm = invuo_eta_problems
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_unknown_eta_filter
integer x = 745
integer y = 504
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_unknown_eta_filter
integer x = 425
integer y = 504
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_unknown_eta_filter
integer x = 101
integer y = 500
integer taborder = 20
end type

type dw_1 from u_base_dw_ext within w_unknown_eta_filter
integer x = 91
integer y = 132
integer width = 873
integer height = 232
integer taborder = 10
string dataobject = "d_dddw_start_end_carriers"
boolean border = false
end type

event constructor;call super::constructor;InsertRow(0)

ii_rc = GetChild( 'start_carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()

ii_rc = GetChild( 'end_carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()

end event

event itemchanged;call super::itemchanged;long ll_FoundRow

IF Trim(data) = "" THEN Return

IF dwo.name = "start_carrier_code" Then
   ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
   IF ll_FoundRow < 1 THEN
	  MessageBox( "Carrier Error", data + " is Not a Valid Carrier Code." )
	  dw_1.SetItem(1,"start_carrier_code", "")
	  dw_1.SetFocus()  
     dw_1.SetColumn("start_carrier_code")
     Return 1
   END IF		
End if

IF dwo.name = "end_carrier_code" Then
   ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
   IF ll_FoundRow < 1 THEN
  	  MessageBox( "Carrier Error", data + " is Not a Valid Carrier Code." )
     dw_1.SetItem(1,"end_carrier_code", "")
     dw_1.SetFocus()  
     dw_1.SetColumn("end_carrier_code")
	  Return 1
   END IF		
End if


end event

event itemerror;call super::itemerror;Return 1
end event

type gb_1 from groupbox within w_unknown_eta_filter
integer x = 55
integer y = 68
integer width = 965
integer height = 352
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Carriers"
end type

