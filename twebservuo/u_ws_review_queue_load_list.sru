HA$PBExportHeader$u_ws_review_queue_load_list.sru
forward
global type u_ws_review_queue_load_list from u_webservice
end type
end forward

global type u_ws_review_queue_load_list from u_webservice
end type
global u_ws_review_queue_load_list u_ws_review_queue_load_list

forward prototypes
public function integer nf_otrt12er (string as_req_load_key, ref string ls_stop_detail_inq_string, ref s_error astr_error_info)
public function integer nf_otrt15er (string as_req_load_key, ref string as_ship_info_inq_string, ref s_error astr_error_info)
public function integer nf_otrt34er (string as_req_from_carrier, string as_req_to_carrier, string as_req_complex, string as_division, string as_plant, ref string as_cust_serv_summary_str, ref s_error astr_error_info)
public function integer nf_otrt56er (string as_req_load_key, string as_req_order, ref string as_order_info_string, s_error astr_error_info)
public function integer nf_otrt57er (string as_req_load_key, string as_req_order, string as_ffw_code, ref s_error astr_error_info)
public function integer nf_otrt53er (string as_load_key, string as_stop_code, ref string as_load_info_string, ref string as_order_info_string, ref s_error astr_error_info)
public function integer nf_otrt32er (string as_req_load_key, string as_req_stop_code, string as_comment, ref s_error astr_error_info)
public function integer nf_otrt11er (string as_req_load_key, string as_req_bol_number, string as_req_customer_id, string as_req_customer_po, ref string as_load_detail_inq_string, ref s_error astr_error_info)
public function integer nf_otrt17er (string as_actual_delivery_date, string as_late_reason_code, string as_appt_contact, string as_eta_date, string as_eta_time, string as_actual_delivery_time, string as_load_key, string as_stop_code, string as_carrier_notified_ind, string as_appt_date, string as_appt_time, s_error astr_error_info)
public function integer nf_otrt14er (string as_req_load_key, ref string as_check_call_inq_string, s_error astr_error_info)
public function integer nf_otrt31er (string as_req_load_key, string as_se_app_name, ref string as_stop_info_inq_string, s_error astr_error_info)
public function integer nf_otrt16er (ref string as_load_key, ref string as_stop_code, ref string as_check_call_date, ref string as_check_call_time, ref string as_check_call_city, ref string as_check_call_state, ref string as_late_reason_code, ref string as_miles_out, ref string as_eta_date, ref string as_eta_time, ref string as_eta_contact, string as_update_date, string as_update_time, ref s_error astr_error_info)
public function integer nf_otrt61er (string as_req_order_number, string as_req_bol_number, string as_req_customer_id, string as_req_customer_po, ref string as_load_detail_inq_string, ref string as_check_call_inq_string, ref string as_stop_detail_inq_string, ref s_error astr_error_info)
public function integer nf_otrt76er (string as_req_load_key, ref datawindow ad_dispatch_event, ref s_error astr_error_info)
end prototypes

public function integer nf_otrt12er (string as_req_load_key, ref string ls_stop_detail_inq_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT12ER'
ls_tran_id = 'T12E'


ls_input_string = as_req_load_key

li_rval = uf_rpccall(ls_input_string,ls_stop_detail_inq_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_otrt15er (string as_req_load_key, ref string as_ship_info_inq_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT15ER'
ls_tran_id = 'T15E'


ls_input_string = as_req_load_key

li_rval = uf_rpccall(ls_input_string,as_ship_info_inq_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_otrt34er (string as_req_from_carrier, string as_req_to_carrier, string as_req_complex, string as_division, string as_plant, ref string as_cust_serv_summary_str, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT34ER'
ls_tran_id = 'T34E'


ls_input_string=as_req_from_carrier +'~t' + as_req_to_carrier +'~t' + as_req_complex +'~t' + as_division  +'~t' + as_plant

li_rval = uf_rpccall(ls_input_string,as_cust_serv_summary_str, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_otrt56er (string as_req_load_key, string as_req_order, ref string as_order_info_string, s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT56ER'
ls_tran_id = 'T56E'


ls_input_string = as_req_load_key +'~t' + as_req_order

li_rval = uf_rpccall(ls_input_string, as_order_info_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval


end function

public function integer nf_otrt57er (string as_req_load_key, string as_req_order, string as_ffw_code, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT57ER'
ls_tran_id = 'T57E'


ls_input_string = 	as_req_load_key  +'~t' +  as_req_order  +'~t' + as_ffw_code


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_otrt53er (string as_load_key, string as_stop_code, ref string as_load_info_string, ref string as_order_info_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval
string ls_stringsep

u_String_Functions  lu_string

ls_program_name= 'OTRT53ER'
ls_tran_id = 'T53E'

ls_input_string = as_load_key +'~t' + as_stop_code

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ls_stringsep = Left(ls_OutputString, 1)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_load_info_string += ls_output
			Case 'D'
				as_order_info_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)


end function

public function integer nf_otrt32er (string as_req_load_key, string as_req_stop_code, string as_comment, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT32ER'
ls_tran_id = 'T32E'


ls_input_string = as_req_load_key + '~t' +as_req_stop_code + '~t' + as_comment

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_otrt11er (string as_req_load_key, string as_req_bol_number, string as_req_customer_id, string as_req_customer_po, ref string as_load_detail_inq_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT11ER'
ls_tran_id = 'T11E'


ls_input_string = as_req_load_key +'~t'+as_req_bol_number+'~t'+as_req_customer_id+'~t'+as_req_customer_po

li_rval = uf_rpccall(ls_input_string, as_load_detail_inq_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_otrt17er (string as_actual_delivery_date, string as_late_reason_code, string as_appt_contact, string as_eta_date, string as_eta_time, string as_actual_delivery_time, string as_load_key, string as_stop_code, string as_carrier_notified_ind, string as_appt_date, string as_appt_time, s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT17ER'
ls_tran_id = 'T17E'


ls_input_string = 	as_actual_delivery_date +'~t' + as_late_reason_code+'~t' + as_appt_contact  +'~t' +  as_eta_date+'~t' +  as_eta_time  +'~t' +  &
						 as_actual_delivery_time +'~t' + as_load_key  +'~t' + as_stop_code +'~t' +as_carrier_notified_ind +'~t' + as_appt_date  +'~t' +  as_appt_time  


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_otrt14er (string as_req_load_key, ref string as_check_call_inq_string, s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT14ER'
ls_tran_id = 'T14E'


ls_input_string = as_req_load_key

li_rval = uf_rpccall(ls_input_string,as_check_call_inq_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_otrt31er (string as_req_load_key, string as_se_app_name, ref string as_stop_info_inq_string, s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT31ER'
ls_tran_id = 'T31E'


ls_input_string = as_req_load_key + '~t' + as_se_app_name

li_rval = uf_rpccall(ls_input_string,as_stop_info_inq_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_otrt16er (ref string as_load_key, ref string as_stop_code, ref string as_check_call_date, ref string as_check_call_time, ref string as_check_call_city, ref string as_check_call_state, ref string as_late_reason_code, ref string as_miles_out, ref string as_eta_date, ref string as_eta_time, ref string as_eta_contact, string as_update_date, string as_update_time, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT16ER'
ls_tran_id = 'T16E'


ls_input_string = as_load_key +'~t' +as_stop_code+'~t' +as_check_call_date+'~t'+as_check_call_time+'~t'+as_check_call_city+'~t'+as_check_call_state+'~t'+&
as_late_reason_code+'~t'+as_miles_out+'~t'+as_eta_date+'~t'+as_eta_time+'~t'+as_eta_contact+'~t'+as_update_date+'~t'+as_update_time

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_otrt61er (string as_req_order_number, string as_req_bol_number, string as_req_customer_id, string as_req_customer_po, ref string as_load_detail_inq_string, ref string as_check_call_inq_string, ref string as_stop_detail_inq_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_output_string, ls_stringId, ls_output, ls_stringind
integer li_rval, li_version
string ls_stringsep

u_String_Functions lu_string

ls_program_name = 'OTRT61ER'
ls_tran_id = 'T61E'

ls_input_string = as_req_order_number + '~t' + as_req_bol_number + '~t' + as_req_customer_id + '~t' + as_req_customer_po

li_version = 1

li_rval = uf_rpccall(ls_input_string, ls_output_string, astr_error_info, ls_program_name, ls_tran_id,li_version)

ls_stringsep = Left(ls_output_string, 1)

if li_rval = 0 then
	ls_stringInd = Mid(ls_output_string, 2, 1)
	ls_output_string = Mid(ls_output_string, 3)
	
	Do While Len(Trim(ls_output_string)) >0
		ls_output = lu_string.nf_GetToken(ls_output_string, ls_stringsep)
		
		Choose Case ls_stringind
			Case 'H' 
				as_load_detail_inq_string += ls_output
			Case 'L'
				as_check_call_inq_string += ls_output
			Case 'S'
				as_stop_detail_inq_string += ls_output
		End Choose
		ls_stringInd = Left(ls_output_string, 1)
		ls_output_string = Mid(ls_output_string, 2)
	Loop
end if
return li_rval

		
end function

public function integer nf_otrt76er (string as_req_load_key, ref datawindow ad_dispatch_event, ref s_error astr_error_info);
string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT76ER'
ls_tran_id = 'T76E'


ls_input_string = as_req_load_key

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ad_dispatch_event.ImportString(ls_outputstring)


return li_rval

end function

on u_ws_review_queue_load_list.create
call super::create
end on

on u_ws_review_queue_load_list.destroy
call super::destroy
end on

