HA$PBExportHeader$u_ws_traffic.sru
forward
global type u_ws_traffic from u_webservice
end type
end forward

global type u_ws_traffic from u_webservice
end type
global u_ws_traffic u_ws_traffic

forward prototypes
public function integer nf_otrt29er (string as_req_complex, string as_req_assign_stat, string as_req_carrier_code, ref string as_trailerinfo_string, ref s_error astr_error_info)
public function integer nf_otrt55er (string as_req_complex, string as_req_plant, string as_req_control, string as_req_region, string as_req_division, string as_req_transmode, string as_req_carrier, string as_from_ship_date, string as_to_ship_date, ref string as_load_list_string, ref s_error astr_error_info)
public function integer nf_otrt30er (string as_update_control, string as_carrier_assign_upd_string, ref s_error astr_error_info)
public function integer nf_otrt26er (string as_req_complex, string as_req_control, string as_req_plant, string as_req_region, string as_req_division, string as_req_transmode, string as_req_carrier_code, string as_req_ship_from_date, string as_req_ship_to_date, ref string as_stops_detail_string, ref s_error astr_error_info)
public function integer nf_otrt27er (ref string as_load_key, ref string as_load_detail_hdr_string, ref string as_load_detail_rpt_string, ref s_error astr_error_info)
public function integer nf_otrt28er (string as_load_key, ref string as_load_message_string, ref s_error astr_error_info)
public function boolean nf_otrt64er (string as_req_ffw_code, string as_req_division, ref datawindow ad_business_hours_string, ref datawindow ad_closed_dates_string, ref s_error astr_error_info)
public function boolean nf_otrt65er (string as_ffw_code, string as_ffw_hours_string, ref s_error astr_error_info)
public function boolean nf_otrt66er (string as_ffw_cide, string as_action_ind, string as_ckised_date, ref s_error astr_error_info)
end prototypes

public function integer nf_otrt29er (string as_req_complex, string as_req_assign_stat, string as_req_carrier_code, ref string as_trailerinfo_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT29ER'
ls_tran_id = 'T29E'


ls_input_string = as_req_complex +'~t' + as_req_assign_stat+'~t' + as_req_carrier_code

li_rval = uf_rpccall(ls_input_string,as_trailerinfo_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)


end function

public function integer nf_otrt55er (string as_req_complex, string as_req_plant, string as_req_control, string as_req_region, string as_req_division, string as_req_transmode, string as_req_carrier, string as_from_ship_date, string as_to_ship_date, ref string as_load_list_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval, li_version

u_String_Functions  lu_string
ls_program_name= 'OTRT55ER'
ls_tran_id = 'T55E'

ls_input_string = as_req_complex +'~t' + as_req_plant+'~t' + as_req_control &
+'~t' + as_req_region+'~t' + as_req_division+'~t'&
+ as_req_transmode+'~t' + as_req_carrier+'~t' + &
as_from_ship_date+'~t' + as_to_ship_date+'~t' + as_load_list_string

li_version = 1 

li_rval = uf_rpccall(ls_input_string,as_load_list_string, astr_error_info, ls_program_name, ls_tran_id,li_version)

return li_rval

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

end function

public function integer nf_otrt30er (string as_update_control, string as_carrier_assign_upd_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval


ls_program_name='OTRT30ER'
ls_tran_id='T30E'

ls_input_string=as_update_control+ '~t' + as_carrier_assign_upd_string

li_rval=uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_otrt26er (string as_req_complex, string as_req_control, string as_req_plant, string as_req_region, string as_req_division, string as_req_transmode, string as_req_carrier_code, string as_req_ship_from_date, string as_req_ship_to_date, ref string as_stops_detail_string, ref s_error astr_error_info);string ls_program_name, ls_trans_id, ls_input_string, ls_outputstring
integer li_rval,li_version

u_String_Functions lu_string
ls_program_name= 'OTRT26ER'
ls_trans_id='T26E'

ls_input_string=as_req_complex +'~t'+ as_req_control +'~t'+ as_req_plant +'~t'+ as_req_region &
                       +'~t'+ as_req_division +'~t'+ as_req_transmode +'~t'+ as_req_carrier_code +'~t'+ as_req_ship_from_date &
					+'~t'+ as_req_ship_to_date +'~t'+ as_stops_detail_string		  

li_version = 1

li_rval = uf_rpccall(ls_input_string, as_stops_detail_string, astr_error_info, ls_program_name, ls_trans_id,li_version)

return li_rval
end function

public function integer nf_otrt27er (ref string as_load_key, ref string as_load_detail_hdr_string, ref string as_load_detail_rpt_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval
string ls_stringsep

u_String_Functions lu_string
ls_program_name = 'OTRT27ER'
ls_tran_id='T27E'

ls_input_string = as_load_key

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ls_stringsep = Left(ls_OutputString, 1)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_load_detail_hdr_string += ls_output
			Case 'D'
				as_load_detail_rpt_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval

end function

public function integer nf_otrt28er (string as_load_key, ref string as_load_message_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
int li_rval

u_String_Functions lu_string
ls_program_name='OTRT28ER'
ls_tran_id='T28E'

ls_input_string= as_load_key

li_rval= uf_rpccall(ls_input_string, as_load_message_string, astr_error_info,ls_program_name, ls_tran_id)


return li_rval
end function

public function boolean nf_otrt64er (string as_req_ffw_code, string as_req_division, ref datawindow ad_business_hours_string, ref datawindow ad_closed_dates_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_business_hours_string, ls_closed_dates_string
integer li_rval
string ls_stringsep

ls_business_hours_string = Space(200)
ls_closed_dates_string = Space(1000)

u_String_Functions  lu_string

ls_program_name= 'OTRT64ER'
ls_tran_id = 'T64E'


ls_input_string = as_req_ffw_code +'~t' + as_req_division

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ls_stringsep = Left(ls_OutputString, 1)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				ls_business_hours_string += ls_output
			Case 'D'
				ls_closed_dates_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	

If li_rval = 0 then
		ad_business_hours_string.ImportString(ls_business_hours_string)
		ad_closed_dates_string.ImportString(ls_closed_dates_string)
end if
	
return True

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)


end function

public function boolean nf_otrt65er (string as_ffw_code, string as_ffw_hours_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT65ER'
ls_tran_id = 'T65E'


ls_input_string = as_ffw_code + as_ffw_hours_string

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

// Check the return code from the above function call
IF li_rval <> 0 THEN
	Return False
else
end if
	
Return True

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

end function

public function boolean nf_otrt66er (string as_ffw_cide, string as_action_ind, string as_ckised_date, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT66ER'
ls_tran_id = 'T66E'


ls_input_string = as_ffw_cide +'~t' + as_action_ind +'~t' + as_ckised_date

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

// Check the return code from the above function call
IF li_rval <> 0 THEN
	Return False
else

end if

Return True
end function

on u_ws_traffic.create
call super::create
end on

on u_ws_traffic.destroy
call super::destroy
end on

