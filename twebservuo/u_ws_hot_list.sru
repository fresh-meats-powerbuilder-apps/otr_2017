HA$PBExportHeader$u_ws_hot_list.sru
forward
global type u_ws_hot_list from u_webservice
end type
end forward

global type u_ws_hot_list from u_webservice
end type
global u_ws_hot_list u_ws_hot_list

forward prototypes
public function integer nf_otrt41er (string as_req_load_key, ref string as_cust_serv_msg_inq_string, ref s_error astr_error_info)
public function integer nf_otrt20er (string as_req_load_key, string as_req_from_carrier, string as_req_to_carrier, string as_req_review_status, string as_req_type_code, string as_req_priority, string as_req_ship_date_from, string as_req_ship_date_to, string as_req_delv_date_from, string as_req_delv_date_to, string as_req_complex, string as_req_division, ref string as_hot_list_inq_string, ref s_error astr_error_info)
public function integer nf_otrt21er (string as_load_key, string as_type_code, string as_priority_level, string as_review_status, string as_next_review_date, string as_next_review_time, string as_update_control, s_error astr_error_info)
end prototypes

public function integer nf_otrt41er (string as_req_load_key, ref string as_cust_serv_msg_inq_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT41ER'
ls_tran_id = 'T41E'


ls_input_string = as_req_load_key

li_rval = uf_rpccall(ls_input_string,as_cust_serv_msg_inq_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_otrt20er (string as_req_load_key, string as_req_from_carrier, string as_req_to_carrier, string as_req_review_status, string as_req_type_code, string as_req_priority, string as_req_ship_date_from, string as_req_ship_date_to, string as_req_delv_date_from, string as_req_delv_date_to, string as_req_complex, string as_req_division, ref string as_hot_list_inq_string, ref s_error astr_error_info);integer li_rval
string ls_program_name, ls_tran_id, ls_input_string


u_String_Functions  lu_string

ls_program_name= 'OTRT20ER'
ls_tran_id = 'T20E'


ls_input_string = as_req_load_key+'~t'+as_req_from_carrier+'~t'+as_req_to_carrier+'~t'+as_req_review_status+'~t'+as_req_type_code+'~t'+&
as_req_priority+'~t'+as_req_ship_date_from+'~t'+as_req_ship_date_to+'~t'+as_req_delv_date_from+'~t'+as_req_delv_date_to+'~t'+as_req_complex+'~t'+as_req_division

li_rval = uf_rpccall(ls_input_string,as_hot_list_inq_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_otrt21er (string as_load_key, string as_type_code, string as_priority_level, string as_review_status, string as_next_review_date, string as_next_review_time, string as_update_control, s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string

integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT21ER'
ls_tran_id = 'T21E'

ls_input_string  = as_load_key+'~t'+ as_type_code + '~t'+ as_priority_level +'~t' + as_review_status + '~t' + as_next_review_date + '~t'+&
as_next_review_time+'~t'+as_update_control

 li_rval = uf_rpcupd(ls_input_string,astr_error_info, ls_program_name, ls_tran_id)


return li_rval
end function

on u_ws_hot_list.create
call super::create
end on

on u_ws_hot_list.destroy
call super::destroy
end on

