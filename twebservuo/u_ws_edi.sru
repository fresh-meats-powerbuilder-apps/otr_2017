HA$PBExportHeader$u_ws_edi.sru
forward
global type u_ws_edi from u_webservice
end type
end forward

global type u_ws_edi from u_webservice
end type
global u_ws_edi u_ws_edi

forward prototypes
public function boolean nf_otrt72er (string as_req_carrier, ref datawindow ad_carrier_header_string, ref datawindow ad_carrier_edi_string, ref s_error astr_error_info)
public function integer nf_otrt73er (ref datawindow ad_carrier_string, ref s_error astr_error_info)
end prototypes

public function boolean nf_otrt72er (string as_req_carrier, ref datawindow ad_carrier_header_string, ref datawindow ad_carrier_edi_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind,ls_header,ls_detail
integer li_rval
string ls_stringsep

u_String_Functions  lu_string

ls_program_name= 'OTRT72ER'
ls_tran_id = 'T72E'

ls_input_string = as_req_carrier

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ls_stringsep = Left(ls_OutputString, 1)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				//as_header_string += ls_output
				ls_header += ls_output
			Case 'D'
			//	as_recv_window_inq_string += ls_output
				ls_detail += ls_output
		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
ad_carrier_header_string.ImportString(ls_header)
ad_carrier_edi_string.ImportString(ls_detail)


return true

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)


end function

public function integer nf_otrt73er (ref datawindow ad_carrier_string, ref s_error astr_error_info);
string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT73ER'
ls_tran_id = 'T73E'


ls_input_string =  space(4)

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ad_carrier_string.ImportString(ls_outputstring)


return li_rval


end function

on u_ws_edi.create
call super::create
end on

on u_ws_edi.destroy
call super::destroy
end on

