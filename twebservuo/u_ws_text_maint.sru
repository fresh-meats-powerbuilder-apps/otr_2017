HA$PBExportHeader$u_ws_text_maint.sru
forward
global type u_ws_text_maint from u_webservice
end type
end forward

global type u_ws_text_maint from u_webservice
end type
global u_ws_text_maint u_ws_text_maint

forward prototypes
public function integer uf_otrt38er (string as_req_record_type, ref string as_text_maint_string, ref s_error astr_error_info)
public function integer uf_otrt39er (string as_record_type, string as_record_type_code, string as_type_short_descr, string as_type_description, ref s_error astr_error_info)
end prototypes

public function integer uf_otrt38er (string as_req_record_type, ref string as_text_maint_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id
integer li_rval
u_String_Functions  lu_string


ls_program_name = 'OTRT38ER'
ls_tran_id = 'T38E'

li_rval = uf_rpccall(as_req_record_type, as_text_maint_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_otrt39er (string as_record_type, string as_record_type_code, string as_type_short_descr, string as_type_description, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval
u_String_Functions  lu_string

ls_program_name = 'OTRT39ER'
ls_tran_id = 'T39E'

ls_input_string = as_record_type +'~t' + as_record_type_code + '~t' + as_type_short_descr + '~t' + as_type_description

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)



return li_rval
end function

on u_ws_text_maint.create
call super::create
end on

on u_ws_text_maint.destroy
call super::destroy
end on

