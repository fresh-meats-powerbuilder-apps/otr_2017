HA$PBExportHeader$u_ws_utl.sru
forward
global type u_ws_utl from u_utl_webservice
end type
end forward

global type u_ws_utl from u_utl_webservice
end type
global u_ws_utl u_ws_utl

type variables
//Web Service path
String		is_utlu07er_userid, is_utlu07er_password


end variables

forward prototypes
public function boolean uf_get_utlu07er_parms ()
public function integer nf_utlu07er (s_error astr_error_info, string as_name_password, ref character ac_action_indicator)
public function integer nf_utlu00er (ref s_error astr_error_info, string as_name_type, ref string as_group_id)
end prototypes

public function boolean uf_get_utlu07er_parms ();if pos(is_web_service_address, "cicstst") > 0 then
	is_utlu07er_userid = 'PBWSTST'
	is_utlu07er_password = 'WSTSTVF1'
else
	if pos(is_web_service_address, "cicspar") > 0 then
		is_utlu07er_userid = 'PBWSPAR'
		is_utlu07er_password =  'WSPARVF1'
	else
		if pos(is_web_service_address, "cics00b") > 0 then
			is_utlu07er_userid = 'PBWSPRD'
			is_utlu07er_password =  'WSPRDVF1'
		else
			MessageBox("IBP002.INI file error", "Unable to determine CICS region for web service")
			Return False
		end if
	end if
end if

Return True
end function

public function integer nf_utlu07er (s_error astr_error_info, string as_name_password, ref character ac_action_indicator);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_temp, ls_target_address, ls_OutputString, ls_username, ls_userpassword, ls_Stringind

u_String_Functions		lu_string

gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

utlu01sr_utlu01srservice p_obj

if not uf_get_utlu07er_parms() Then
	Return -1
end if

ict_utl000sr_cics_container.utl000sr_req_password = is_utlu07er_password
ict_utl000sr_cics_container.utl000sr_req_userid =  is_utlu07er_userid
ict_utl000sr_cics_container.utl000sr_req_program = 'UTLU07ER'
ict_utl000sr_cics_container.utl000sr_req_tranid =  'U07E'

ict_utl000sr_program_container_in.utl000sr_rval = 0
ict_utl000sr_program_container_in.utl000sr_message = space(200)
ict_utl000sr_program_container_in.utl000sr_version_number = 0

ipgif_input.utlu01in.value =  as_name_password+'~t'+ac_action_indicator+'~t'

//Set options for connection
ls_temp = as_name_password
ls_username =  lu_string.nf_gettoken(ls_temp, '~t')
ls_userpassword =  lu_string.nf_gettoken(ls_temp, '~r~n')
//ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(ls_username)+"~",Password=~""+string(ls_userpassword)+"~"")
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(is_utlu07er_userid)+"~",Password=~""+string(is_utlu07er_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "utlu01sr_utlu01srservice", is_web_service_address)
if ll_ret <> 0 then
	MessageBox("Error", "Cannot create instance of proxy")
	Return ll_ret
end if

ict_utl000sr_program_container_in.utl000sr_last_record_num = 0
ict_utl000sr_program_container_in.utl000sr_max_record_num = 0
ict_utl000sr_program_container_in.utl000sr_task_num = 0

try
	  ipgif_output = p_obj.utlu01sroperation(ipgif_input)
	  catch (SoapException e)
			//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
			astr_error_info.se_procedure_name = ''
			astr_error_info.se_return_code = ''
			astr_error_info.se_message = e.Text
			uf_display_message_ws(-1, astr_error_info, 0) 
			Return -1
end try
	
ictif_program_container_out = ipgif_output.utlu01pg 
li_rval = ictif_program_container_out.utl000sr_program_container.utl000sr_rval
if li_rval < 0 then
	//check for any RPC errors to display messages
	ls_message = ictif_program_container_out.utl000sr_program_container.utl000sr_message
	
	If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
		astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
		astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
		astr_error_info.se_message = ls_message
	else
		astr_error_info.se_procedure_name = ''
		astr_error_info.se_return_code = ''
		astr_error_info.se_message = ls_message
	end if
	
	uf_display_message_ws(li_rval, astr_error_info, 0) 
	Return -1
else
	ls_message = ictif_program_container_out.utl000sr_program_container.utl000sr_message
	if isnull(ipgif_output.utlu01ot) then
	else	
		ac_action_indicator =  trim(ipgif_output.utlu01ot.value)
	end if
end if

//check for any RPC errors to display messages
ls_message = ictif_program_container_out.utl000sr_program_container.utl000sr_message

If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
	astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_message = ls_message
else
	astr_error_info.se_procedure_name = ''
	astr_error_info.se_return_code = ''
	astr_error_info.se_message = ls_message
end if

uf_display_message_ws(li_rval, astr_error_info, 0)  

Return li_rval

end function

public function integer nf_utlu00er (ref s_error astr_error_info, string as_name_type, ref string as_group_id);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'UTLU00ER'
ls_tran_id = 'U00E'


ls_input_string = 	as_name_type  +'~t' + as_group_id  +'~t' 

li_rval = uf_rpccall(ls_input_string,as_group_id, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)
end function

on u_ws_utl.create
call super::create
end on

on u_ws_utl.destroy
call super::destroy
end on

