HA$PBExportHeader$u_ws_customer.sru
forward
global type u_ws_customer from u_webservice
end type
end forward

global type u_ws_customer from u_webservice
end type
global u_ws_customer u_ws_customer

type variables

end variables

forward prototypes
public function integer uf_cfmc29er (string as_req_customer_id, string as_req_division, ref string as_recv_window_inq_string, ref string as_header_string, ref s_error astr_error_info)
public function integer uf_otrt35er (string as_req_customer_id, string as_recv_type, string as_frt_forwarder, ref s_error astr_error_info)
public function integer uf_cfmc30er (string as_customer_id, string as_division_code, string as_recv_phone, string as_cust_contact, string as_mon_from_hrs, string as_mon_to_hrs, string as_tue_from_hrs, string as_tue_to_hrs, string as_wed_from_hrs, string as_wed_to_hrs, string as_thur_from_hrs, string as_thur_to_hrs, string as_fri_from_hrs, string as_fri_to_hrs, string as_sat_from_hrs, string as_sat_to_hrs, string as_sun_from_hrs, string as_sun_to_hrs, string as_mon_from_hrs2, string as_mon_to_hrs2, string as_tue_from_hrs2, string as_tue_to_hrs2, string as_wed_from_hrs2, string as_wed_to_hrs2, string as_thur_from_hrs2, string as_thur_to_hrs2, string as_fri_from_hrs2, string as_fri_to_hrs2, string as_sat_from_hrs2, string as_sat_to_hrs2, string as_sun_from_hrs2, string as_sun_to_hrs2, ref s_error astr_error_info)
public function integer uf_otrt45er (string as_req_name, ref string as_customer_string, ref s_error astr_error_info)
public function integer uf_otrt46er (string as_req_cust_id, string as_req_from_ship_date, string as_req_to_ship_date, ref string as_cust_order_inq_string, ref s_error astr_error_info)
end prototypes

public function integer uf_cfmc29er (string as_req_customer_id, string as_req_division, ref string as_recv_window_inq_string, ref string as_header_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval
string ls_stringsep

u_String_Functions  lu_string

ls_program_name= 'CFMC29ER'
ls_tran_id = 'C29E'


ls_input_string = as_req_customer_id +'~t' + as_req_division

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ls_stringsep = Left(ls_OutputString, 1)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_header_string += ls_output
			Case 'D'
				as_recv_window_inq_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)


end function

public function integer uf_otrt35er (string as_req_customer_id, string as_recv_type, string as_frt_forwarder, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT35ER'
ls_tran_id = 'T35E'


ls_input_string = 	as_req_customer_id  +'~t' +  as_recv_type  +'~t' + as_frt_forwarder


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)
end function

public function integer uf_cfmc30er (string as_customer_id, string as_division_code, string as_recv_phone, string as_cust_contact, string as_mon_from_hrs, string as_mon_to_hrs, string as_tue_from_hrs, string as_tue_to_hrs, string as_wed_from_hrs, string as_wed_to_hrs, string as_thur_from_hrs, string as_thur_to_hrs, string as_fri_from_hrs, string as_fri_to_hrs, string as_sat_from_hrs, string as_sat_to_hrs, string as_sun_from_hrs, string as_sun_to_hrs, string as_mon_from_hrs2, string as_mon_to_hrs2, string as_tue_from_hrs2, string as_tue_to_hrs2, string as_wed_from_hrs2, string as_wed_to_hrs2, string as_thur_from_hrs2, string as_thur_to_hrs2, string as_fri_from_hrs2, string as_fri_to_hrs2, string as_sat_from_hrs2, string as_sat_to_hrs2, string as_sun_from_hrs2, string as_sun_to_hrs2, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'CFMC30ER'
ls_tran_id = 'C30E'


ls_input_string = 	as_customer_id  +'~t' +  as_division_code  +'~t' +  &
						as_recv_phone +'~t' + as_cust_contact +'~t' + as_mon_from_hrs +'~t' + as_mon_to_hrs +'~t' + as_tue_from_hrs +'~t' + &
						as_tue_to_hrs +'~t' + as_wed_from_hrs +'~t' + as_wed_to_hrs +'~t' + as_thur_from_hrs +'~t' + &  
						as_thur_to_hrs +'~t' + as_fri_from_hrs +'~t' + as_fri_to_hrs +'~t' + as_sat_from_hrs +'~t' + &
						as_sat_to_hrs +'~t' + as_sun_from_hrs +'~t' + as_sun_to_hrs +'~t' + as_mon_from_hrs2 +'~t' + as_mon_to_hrs2 +'~t' + as_tue_from_hrs2 +'~t' + &
						as_tue_to_hrs2 +'~t' + as_wed_from_hrs2 +'~t' + as_wed_to_hrs2 +'~t' + as_thur_from_hrs2 +'~t' + &  
						as_thur_to_hrs2 +'~t' + as_fri_from_hrs2 +'~t' + as_fri_to_hrs2 +'~t' + as_sat_from_hrs2 +'~t' + &
						as_sat_to_hrs2 +'~t' + as_sun_from_hrs2 +'~t' + as_sun_to_hrs2

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)
end function

public function integer uf_otrt45er (string as_req_name, ref string as_customer_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT45ER'
ls_tran_id = 'T45E'


ls_input_string = as_req_name

li_rval = uf_rpccall(ls_input_string,as_customer_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)

end function

public function integer uf_otrt46er (string as_req_cust_id, string as_req_from_ship_date, string as_req_to_ship_date, ref string as_cust_order_inq_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval, li_version


u_String_Functions  lu_string

ls_program_name= 'OTRT46ER'
ls_tran_id = 'T46E'


ls_input_string = as_req_cust_id +'~t' + as_req_from_ship_date+'~t' + as_req_to_ship_date
li_version = 1
li_rval = uf_rpccall(ls_input_string, as_cust_order_inq_string, astr_error_info, ls_program_name, ls_tran_id,li_version)

return li_rval

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

end function

on u_ws_customer.create
call super::create
end on

on u_ws_customer.destroy
call super::destroy
end on

event constructor;call super::constructor;uf_initialize()
end event

