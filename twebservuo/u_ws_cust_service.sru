HA$PBExportHeader$u_ws_cust_service.sru
forward
global type u_ws_cust_service from u_webservice
end type
end forward

global type u_ws_cust_service from u_webservice
end type
global u_ws_cust_service u_ws_cust_service

forward prototypes
public function integer nf_otrt33er (string as_req_load_key, string as_req_stop_code, ref string as_appt_except_inq_string, ref s_error astr_error_info)
public function integer nf_otrt74er (string as_transmode, string as_from_date, string as_to_date, string as_billto_customer, string as_shipto_customer, string as_complex, string as_carrier, string as_sales_location, string as_tsr, string as_svc_region, string as_division, ref datawindow ad_late_deliveries_string, ref s_error astr_error_info)
public function integer nf_otrt47er (string as_req_start_carrier_code, string as_req_end_carrier_code, string as_req_delv_date, string as_division, ref string as_mass_conf_inq_string, ref s_error astr_error_info)
public function integer nf_otrt48er (string as_load_key, string as_stop_code, string as_eta_date, string as_eta_time, string as_actual_delv_date, string as_actual_delv_time, string as_late_code, s_error astr_error_info)
public function integer nf_otrt63er (string as_req_from_carrier, string as_req_to_carrier, ref string as_problem_string, ref s_error astr_error_info)
public function integer nf_otrt52er (string as_load_key, string as_stop_code, string as_check_call_date, string as_check_call_time, string as_check_call_city, string as_check_call_state, string as_late_reason_code, string as_miles_out, string as_eta_date, string as_eta_time, string as_eta_contact, ref s_error astr_error_info)
end prototypes

public function integer nf_otrt33er (string as_req_load_key, string as_req_stop_code, ref string as_appt_except_inq_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string,ls_outputstring

integer li_rval

u_String_Functions lu_string

ls_program_name = 'OTRT33ER'
ls_tran_id = 'T33E'

ls_input_string = as_req_load_key+'~t'+as_req_stop_code

li_rval = uf_rpccall(ls_input_string,as_appt_except_inq_string,astr_error_info,ls_program_name,ls_tran_id)



return li_rval
end function

public function integer nf_otrt74er (string as_transmode, string as_from_date, string as_to_date, string as_billto_customer, string as_shipto_customer, string as_complex, string as_carrier, string as_sales_location, string as_tsr, string as_svc_region, string as_division, ref datawindow ad_late_deliveries_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string,ls_output

integer li_rval
ad_late_deliveries_string.Reset()

u_String_Functions  lu_string

ls_program_name= 'OTRT74ER'
ls_tran_id = 'T74E'

	

ls_input_string  = as_transmode +'~t'+as_from_date+'~t'+as_to_date+'~t'+as_billto_customer+'~t'+as_shipto_customer+'~t'+ as_complex+'~t'+&
as_carrier+'~t'+as_sales_location+'~t'+as_tsr+'~t'+as_svc_region+'~t'+as_division

 li_rval = uf_rpccall(ls_input_string,ls_output, astr_error_info, ls_program_name, ls_tran_id)

ad_late_deliveries_string.ImportString(ls_output)

//If This.nf_display_message(li_rval, astr_error_info, ii_commhandle) Then
//	ad_late_deliveries_string.ImportString(ls_output)
//	end if

return li_rval
end function

public function integer nf_otrt47er (string as_req_start_carrier_code, string as_req_end_carrier_code, string as_req_delv_date, string as_division, ref string as_mass_conf_inq_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval,li_version


u_String_Functions  lu_string

ls_program_name= 'OTRT47ER'
ls_tran_id = 'T47E'

as_division = TRIM(as_division)
if(as_division = "") then
as_division = "  "
end if 

ls_input_string =as_req_start_carrier_code +'~t'+as_req_end_carrier_code+'~t'+as_req_delv_date+'~t'+as_division

li_version = 1

li_rval = uf_rpccall(ls_input_string,as_mass_conf_inq_string, astr_error_info, ls_program_name, ls_tran_id,li_version)

return li_rval
end function

public function integer nf_otrt48er (string as_load_key, string as_stop_code, string as_eta_date, string as_eta_time, string as_actual_delv_date, string as_actual_delv_time, string as_late_code, s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT48ER'
ls_tran_id = 'T48E'


ls_input_string = as_load_key+'~t'+as_stop_code+'~t'+as_eta_date+'~t'+as_eta_time+'~t'+as_actual_delv_date+'~t'+as_actual_delv_time+'~t'+as_late_code


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_otrt63er (string as_req_from_carrier, string as_req_to_carrier, ref string as_problem_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT63ER'
ls_tran_id = 'T63E'


ls_input_string = as_req_from_carrier+'~t'+as_req_to_carrier

li_rval = uf_rpccall(ls_input_string,as_problem_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_otrt52er (string as_load_key, string as_stop_code, string as_check_call_date, string as_check_call_time, string as_check_call_city, string as_check_call_state, string as_late_reason_code, string as_miles_out, string as_eta_date, string as_eta_time, string as_eta_contact, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'OTRT52ER'
ls_tran_id = 'T52E'


ls_input_string = as_load_key +'~t'+ as_stop_code+'~t'+as_check_call_date+'~t'+as_check_call_time+'~t'+ as_check_call_city+'~t'+as_check_call_state+&
'~t'+as_late_reason_code+'~t'+as_miles_out+'~t'+as_eta_date+'~t'+as_eta_time+'~t'+as_eta_contact



li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

on u_ws_cust_service.create
call super::create
end on

on u_ws_cust_service.destroy
call super::destroy
end on

