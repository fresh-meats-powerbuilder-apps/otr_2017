HA$PBExportHeader$w_base_frame_ext.srw
forward
global type w_base_frame_ext from w_netwise_frame
end type
end forward

global type w_base_frame_ext from w_netwise_frame
string title = "Traffic Application "
string menuname = "m_base_menu_ext"
end type
global w_base_frame_ext w_base_frame_ext

type variables
m_base_menu_ext     im_menu

w_base_sheet_ext     iw_active_sheet
end variables

event open;call super::open;
this.im_menu   =   this.menuID
//is_help = "F:\SOFTWARE\PB\EXE32\OTR.HLP"
  
end event

event close;call super::close;Open(w_close_mainframe, This)
Close(w_close_mainframe)

end event

event ue_postopen;call super::ue_postopen;String ls_userid,ls_web_service_address


// Open a child window that displays a message to the user letting them
// know that we are loading some system tables (static data)
Open(w_loadingtables, This)
This.SetMicroHelp("Loading System Tables...")

// Close the loading tables window
Close(w_loadingtables)
This.SetMicroHelp("Ready")

// IBDKDLD 01/02/03
ls_userid = ProfileString( gw_netwise_frame.is_UserIni,"System Settings","UserID","")
If upper(left(ls_userid,4)) = "IBDK" or upper(left(ls_userid,4)) = "QATE" Then
	ls_web_service_address = ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "Web Service URL", "OTRT01SRpath", "") 
	IF pos(ls_web_service_address, "cics00b") > 0 Then
		This.title += "            ****************  WARNING YOU ARE CONNECTED TO PRODUCTION  ****************"
	END IF
ENd IF


end event

on w_base_frame_ext.create
call super::create
if IsValid(this.MenuID) then destroy(this.MenuID)
if this.MenuName = "m_base_menu_ext" then this.MenuID = create m_base_menu_ext
end on

on w_base_frame_ext.destroy
call super::destroy
if IsValid(MenuID) then destroy(MenuID)
end on

event closequery;call super::closequery;//window	lw_activesheet
//
//lw_activesheet = GetActiveSheet ( )
//
//DO WHILE IsValid(lw_activesheet) 
//	Close(lw_activesheet)
//LOOP
//
end event

