HA$PBExportHeader$w_traffic_filter.srw
$PBExportComments$Window
forward
global type w_traffic_filter from w_base_response_ext
end type
type dw_1 from u_base_dw_ext within w_traffic_filter
end type
type st_1 from statictext within w_traffic_filter
end type
end forward

global type w_traffic_filter from w_base_response_ext
int Width=1107
int Height=1105
boolean TitleBar=true
string Title="Transit Exception Filter"
long BackColor=12632256
dw_1 dw_1
st_1 st_1
end type
global w_traffic_filter w_traffic_filter

on w_traffic_filter.create
int iCurrent
call w_base_response_ext::create
this.dw_1=create dw_1
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_1
this.Control[iCurrent+2]=st_1
end on

on w_traffic_filter.destroy
call w_base_response_ext::destroy
destroy(this.dw_1)
destroy(this.st_1)
end on

type cb_base_help from w_base_response_ext`cb_base_help within w_traffic_filter
int X=727
int Y=821
int TabOrder=10
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_traffic_filter
int X=389
int Y=821
int TabOrder=30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_traffic_filter
int X=60
int Y=821
int TabOrder=20
end type

type dw_1 from u_base_dw_ext within w_traffic_filter
int X=55
int Y=137
int Width=951
int Height=645
int TabOrder=2
string DataObject="d_complex_info"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

type st_1 from statictext within w_traffic_filter
int X=69
int Y=45
int Width=247
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Complex"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

