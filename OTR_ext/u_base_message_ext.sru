HA$PBExportHeader$u_base_message_ext.sru
$PBExportComments$Extension layer Message Object - inherited from u_base_message.
forward
global type u_base_message_ext from u_netwise_message
end type
end forward

global type u_base_message_ext from u_netwise_message
end type
global u_base_message_ext u_base_message_ext

type variables

end variables

on u_base_message_ext.create
call message::create
TriggerEvent( this, "constructor" )
end on

on u_base_message_ext.destroy
call message::destroy
TriggerEvent( this, "destructor" )
end on

