HA$PBExportHeader$w_base_login_ext.srw
$PBExportComments$This is the base login window for the framework - inherited from w_base_response.
forward
global type w_base_login_ext from w_netwise_login
end type
end forward

global type w_base_login_ext from w_netwise_login
end type
global w_base_login_ext w_base_login_ext

on w_base_login_ext.create
call super::create
end on

on w_base_login_ext.destroy
call super::destroy
end on

type cb_base_help from w_netwise_login`cb_base_help within w_base_login_ext
end type

type cb_base_cancel from w_netwise_login`cb_base_cancel within w_base_login_ext
end type

type cb_base_ok from w_netwise_login`cb_base_ok within w_base_login_ext
end type

type st_1 from w_netwise_login`st_1 within w_base_login_ext
end type

type st_username from w_netwise_login`st_username within w_base_login_ext
end type

type sle_username from w_netwise_login`sle_username within w_base_login_ext
end type

type st_password from w_netwise_login`st_password within w_base_login_ext
end type

type sle_password from w_netwise_login`sle_password within w_base_login_ext
end type

type st_3 from w_netwise_login`st_3 within w_base_login_ext
end type

type gb_1 from w_netwise_login`gb_1 within w_base_login_ext
end type

type p_1 from w_netwise_login`p_1 within w_base_login_ext
integer x = 224
integer y = 52
integer width = 928
integer height = 396
string picturename = "ibp.bmp"
boolean map3dcolors = true
end type

type dw_login_name from w_netwise_login`dw_login_name within w_base_login_ext
integer x = 224
integer y = 568
end type

