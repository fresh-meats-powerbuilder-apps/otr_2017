HA$PBExportHeader$u_base_transaction_ext.sru
$PBExportComments$Extended transaction object inherited from SQLCA.
forward
global type u_base_transaction_ext from u_netwise_transaction
end type
end forward

global type u_base_transaction_ext from u_netwise_transaction
end type
global u_base_transaction_ext u_base_transaction_ext

on u_base_transaction_ext.create
TriggerEvent( this, "constructor" )
end on

on u_base_transaction_ext.destroy
TriggerEvent( this, "destructor" )
end on

