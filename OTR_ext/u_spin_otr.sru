HA$PBExportHeader$u_spin_otr.sru
forward
global type u_spin_otr from UserObject
end type
type p_down from picture within u_spin_otr
end type
type p_up from picture within u_spin_otr
end type
end forward

global type u_spin_otr from UserObject
int Width=83
int Height=137
long BackColor=12632256
long PictureMaskColor=25166016
long TabTextColor=33554432
long TabBackColor=67108864
event resize pbm_custom01
p_down p_down
p_up p_up
end type
global u_spin_otr u_spin_otr

type variables

end variables

on resize;//p_up.Width = This.Width
//p_up.Height = This.Height / 2
//
//p_down.Width = This.Width
//p_down.Height = This.Height / 2
//
//p_down.y = This.Height / 2
end on

on constructor;//PostEvent("resize")
end on

on u_spin_otr.create
this.p_down=create p_down
this.p_up=create p_up
this.Control[]={ this.p_down,&
this.p_up}
end on

on u_spin_otr.destroy
destroy(this.p_down)
destroy(this.p_up)
end on

type p_down from picture within u_spin_otr
event lbuttondown pbm_lbuttondown
event lbuttonup pbm_lbuttonup
int X=5
int Y=5
int Width=74
int Height=61
string PictureName="spin1.bmp"
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
boolean FocusRectangle=false
end type

on lbuttondown;This.BorderStyle = StyleLowered!

end on

on lbuttonup;This.BorderStyle = StyleRaised!

end on

event clicked;If iw_frame.im_menu.m_Edit.m_Previous.Enabled And &
		iw_frame.im_menu.m_Edit.m_Previous.Visible Then
	iw_frame.im_menu.m_Edit.m_Previous.PostEvent(Clicked!)
End if

end event

type p_up from picture within u_spin_otr
event lbuttondown pbm_lbuttondown
event lbuttonup pbm_lbuttonup
int X=5
int Y=73
int Width=74
int Height=61
string PictureName="spin2.bmp"
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
boolean FocusRectangle=false
end type

on lbuttondown;This.BorderStyle = StyleLowered!

end on

on lbuttonup;This.BorderStyle = StyleRaised!

end on

event clicked;If iw_frame.im_menu.m_Edit.m_Next.Enabled And &
		iw_frame.im_menu.m_Edit.m_Next.Visible Then
	iw_frame.im_menu.m_Edit.m_Next.PostEvent(Clicked!)
End if

end event

