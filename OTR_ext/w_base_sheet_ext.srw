HA$PBExportHeader$w_base_sheet_ext.srw
forward
global type w_base_sheet_ext from w_netwise_sheet
end type
end forward

global type w_base_sheet_ext from w_netwise_sheet
end type
global w_base_sheet_ext w_base_sheet_ext

on activate;call w_netwise_sheet::activate;iw_frame.iw_active_sheet  =  This
end on

on open;call w_netwise_sheet::open;// Always reposition the window to 1, 1
This.x = 1
This.y = 1
end on

on w_base_sheet_ext.create
call w_netwise_sheet::create
end on

on w_base_sheet_ext.destroy
call w_netwise_sheet::destroy
end on

