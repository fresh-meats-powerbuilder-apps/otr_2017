HA$PBExportHeader$u_base_dw_ext.sru
forward
global type u_base_dw_ext from u_netwise_dw
end type
end forward

global type u_base_dw_ext from u_netwise_dw
end type
global u_base_dw_ext u_base_dw_ext

type variables
//st_load_detail      ist_load_detail
end variables

event doubleclicked;call super::doubleclicked;String          ls_load_key, &
                ls_input_string

w_load_detail    lw_load_detail 

is_ObjectAtPointer = GetObjectAtPointer()
is_ObjectAtPointer = Left( is_ObjectAtPointer, Pos( is_ObjectAtPointer, "~t") - 1 )

If is_ObjectAtPointer  = "load_key" Then
   
   ls_load_key = This.GetItemString(row, "load_key")
   ls_input_string = ls_load_key
   OpenSheetWithParm(lw_load_detail, ls_input_string, iw_frame, 0 , Original!)     
End if

end event

on clicked;call u_netwise_dw::clicked;is_ObjectAtPointer = Left( is_ObjectAtPointer, Pos( is_ObjectAtPointer, "~t") - 1 )
end on

event itemfocuschanged;call super::itemfocuschanged;THIS.SelectText (1,99)
end event

