HA$PBExportHeader$u_csr001.sru
forward
global type u_csr001 from u_netwise_transaction
end type
end forward

global type u_csr001 from u_netwise_transaction
end type
global u_csr001 u_csr001

type prototypes
// PowerBuilder Script File: c:\ibp\src\csr001.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Sat Mar 07 11:03:49 1998
// Source Interface File: c:\ibp\src\csr001.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: csrr01sr_cust_ontime_summary
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int csrr01sr_cust_ontime_summary( &
    string req_sales_location, &
    string req_tsr_code, &
    string req_tsr_type, &
    string req_customer_id, &
    string req_customer_type, &
    string req_report_group, &
    string req_from_date, &
    string req_to_date, &
    string req_div_code, &
    ref string refetch_q_id, &
    ref double refetch_q_page, &
    ref string customer_info_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "csr001.dll" alias for "csrr01sr_cust_ontime_summary;Ansi"


//
// Declaration for procedure: csrr02sr_cust_ontime_detail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int csrr02sr_cust_ontime_detail( &
    string req_customer_id, &
    string req_customer_type, &
    string req_from_date, &
    string req_to_date, &
    string req_division, &
    string req_report_group, &
    string req_sales_location, &
    ref string refetch_load_key, &
    ref string refetch_stop_code, &
    ref string refetch_sales_order, &
    ref string order_info_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "csr001.dll" alias for "csrr02sr_cust_ontime_detail;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "csr001.dll"
function int WBcsr001CkCompleted( int CommHnd ) &
    library "csr001.dll"
//
// ***********************************************************

//
// User Structure:  s_error
//
// global type s_error from structure
//     char        se_app_name[8]                
//     char        se_window_name[8]             
//     char        se_function_name[8]           
//     char        se_event_name[32]             
//     char        se_procedure_name[32]         
//     char        se_user_id[8]                 
//     char        se_return_code[8]             
//     char        se_message[71]                
// end type

end prototypes

type variables
int ii_commhandle
int ii_csr001_commhandle

end variables

forward prototypes
public function boolean nf_csrr01sr (string as_req_sales_location, string as_req_tsr_code, string as_req_tsr_type, string as_req_customer_id, string as_req_customer_type, string as_req_report_group, string as_req_from_date, string as_req_to_date, string as_req_div_code, ref datawindow ad_customer_info_string, ref s_error astr_error_info)
public function boolean nf_csrr02sr (string as_req_customer_id, string as_req_customer_type, string as_req_from_date, string as_req_to_date, string as_req_division, string as_req_report_group, string as_req_sales_location, ref datawindow ad_order_info_string, ref s_error astr_error_info)
end prototypes

public function boolean nf_csrr01sr (string as_req_sales_location, string as_req_tsr_code, string as_req_tsr_type, string as_req_customer_id, string as_req_customer_type, string as_req_report_group, string as_req_from_date, string as_req_to_date, string as_req_div_code, ref datawindow ad_customer_info_string, ref s_error astr_error_info);Int		li_rtn

String	ls_refetch_q_id, &
			ls_output

Double   ld_refetch_q_page


ls_refetch_q_id = space(8)
ld_refetch_q_page = 0


ad_customer_info_string.Reset()


gw_netwise_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

Do
	ls_output = Space(20000)
	astr_error_info.se_message = Space(72)
	astr_error_info.se_procedure_name = 'csrr01sr_cust_ontime_summary'


//call an external netwise function to get the required information
li_rtn = csrr01sr_cust_ontime_summary(as_req_sales_location, &
												  as_req_tsr_code, &
												  as_req_tsr_type, &
         									  as_req_customer_id, &
												  as_req_customer_type, &
												  as_req_report_group, &
												  as_req_from_date, &
												  as_req_to_date, &
												  as_req_div_code, &
											  	  ls_refetch_q_id, &
												  ld_refetch_q_page, &
												  ls_output, &
												  astr_error_info, & 
												  ii_commhandle)
			
	If This.nf_display_message(li_rtn, astr_error_info, ii_commhandle) Then
		ad_customer_info_string.ImportString(ls_output)
//	else
//		Return False
	end if

Loop While LenA(Trim(ls_refetch_q_id)) > 0

gw_netwise_frame.SetMicroHelp( "Ready")
// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
END IF

SetPointer(Arrow!)
Return True
end function

public function boolean nf_csrr02sr (string as_req_customer_id, string as_req_customer_type, string as_req_from_date, string as_req_to_date, string as_req_division, string as_req_report_group, string as_req_sales_location, ref datawindow ad_order_info_string, ref s_error astr_error_info);Int		li_rtn

String	ls_refetch_load_key, &
			ls_refetch_stop_code, &
			ls_refetch_sales_order, &
			ls_output

ls_refetch_load_key =    space(7)
ls_refetch_stop_code =   space(2)
ls_refetch_sales_order = space(7)

ad_order_info_string.Reset()

gw_netwise_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

Do
	ls_output = Space(20000)
	astr_error_info.se_message = Space(72)
	astr_error_info.se_procedure_name = 'csrr02sr_cust_ontime_detail'


//call an external netwise function to get the required information
li_rtn = csrr02sr_cust_ontime_detail (as_req_customer_id, &
												  as_req_customer_type, &
												  as_req_from_date, &
												  as_req_to_date, &
												  as_req_division, &	
												  as_req_report_group, &
												  as_req_sales_location, &
											  	  ls_refetch_load_key, &
												  ls_refetch_stop_code, &
												  ls_refetch_sales_order, &
												  ls_output, &
												  astr_error_info, & 
												  ii_commhandle)
			
	If This.nf_display_message(li_rtn, astr_error_info, ii_commhandle) Then
		ad_order_info_string.ImportString(ls_output)
//	else
//		Return False
	end if

Loop While LenA(Trim(ls_refetch_load_key)) > 0

gw_netwise_frame.SetMicroHelp( "Ready")
// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
END IF

SetPointer(Arrow!)

Return True
end function

on u_csr001.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_csr001.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

event constructor;call super::constructor;
//get the commhandle to be used for the windows
ii_commhandle = SQLCA.nf_GetCommHandle("csr001")

If ii_csr001_commhandle = 0 Then
	// an error occured
	Message.ReturnValue = -1
End if


end event

