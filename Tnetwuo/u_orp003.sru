HA$PBExportHeader$u_orp003.sru
$PBExportComments$Acts as communicating object with netwise for "orp003.dll"
forward
global type u_orp003 from u_netwise_transaction
end type
end forward

global type u_orp003 from u_netwise_transaction
end type
global u_orp003 u_orp003

type prototypes
// PowerBuilder Script File: j:\pb\test\src32\orp203.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Sat Mar 22 12:03:33 1997
// Source Interface File: j:\pb\test\src32\orp203.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: orpo32br_GetHeaderInfo
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo32br_GetHeaderInfo( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_info_in, &
    ref string header_info_out, &
    char Action_ind, &
    string weight_overide_string_in, &
    ref string weight_overide_string_out, &
    string weight_cost_string_in, &
    ref string weight_cost_string_out, &
    char addition_in, &
    ref char addition_out, &
    int CommHnd &
) library "orp203.dll" alias for "orpo32br_GetHeaderInfo;Ansi"


//
// Declaration for procedure: orpo33br_get_order_detail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo33br_get_order_detail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Sales_order_id, &
    string detail_info_in, &
    ref string detail_info_out, &
    string weight_str_in, &
    ref string weight_str_out, &
    string res_reductions_in, &
    ref string pa_resolve_out, &
    string action_ind, &
    int CommHnd &
) library "orp203.dll" alias for "orpo33br_get_order_detail;Ansi"


//
// Declaration for procedure: orpo34br_Get_Order_Cust_info
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo34br_Get_Order_Cust_info( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string customer_info_in, &
    ref string customer_info_out, &
    string Weight_string_in, &
    ref string Weight_string_out, &
    int CommHnd &
) library "orp203.dll" alias for "orpo34br_Get_Order_Cust_info;Ansi"


//
// Declaration for procedure: orpo35br_accept_shortage
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo35br_accept_shortage( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string sales_order_id, &
    char ac_autoresolve, &
    string as_shortage_lines, &
    int CommHnd &
) library "orp203.dll" alias for "orpo35br_accept_shortage;Ansi"


//
// Declaration for procedure: orpo36br_get_cust_order
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo36br_get_cust_order( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string_in, &
    string detail_string_in, &
    ref double task_number, &
    ref int Page_number, &
    ref string header_string_out, &
    ref string detail_string_out, &
    ref string so_id_string_out, &
    int CommHnd &
) library "orp203.dll" alias for "orpo36br_get_cust_order;Ansi"


//
// Declaration for procedure: orpo37br_get_Instructions
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo37br_get_Instructions( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Perm_Inst_in, &
    ref string Perm_Inst_out, &
    string Order_Instruction_In, &
    ref string Order_Instruction_Out, &
    int CommHnd &
) library "orp203.dll" alias for "orpo37br_get_Instructions;Ansi"


//
// Declaration for procedure: orpo38br_pa_summary
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo38br_pa_summary( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char process_option, &
    char availability_date[10], &
    string page_descr_in, &
    string plant_data_in, &
    string detail_data_in, &
    ref string plant_data_out, &
    ref string detail_data_out, &
    ref string file_descr_out, &
    int CommHnd &
) library "orp203.dll" alias for "orpo38br_pa_summary;Ansi"


//
// Declaration for procedure: orpo39br_Gen_sales_ord
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo39br_Gen_sales_ord( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char Calling_window_ind, &
    string Header_in, &
    string Detail_in, &
    string Additional_data_in, &
    string res_reductions_in, &
    ref string header_out, &
    ref string detail_out, &
    ref string Additional_data_out, &
    ref string bm_start_time, &
    ref string bm_end_time, &
    ref string bm_task_number, &
    int CommHnd &
) library "orp203.dll" alias for "orpo39br_Gen_sales_ord;Ansi"



//
// Declaration for procedure: orpo40br_valid_product_info
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo40br_valid_product_info( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char sku_product_code[10], &
    ref string valid_product_info, &
    int CommHnd &
) library "orp203.dll" alias for "orpo40br_valid_product_info;Ansi"


//
// Declaration for procedure: orpo41br_Get_Sale_person_default
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo41br_Get_Sale_person_default( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string userid, &
    ref string sales_person_info_string, &
    int CommHnd &
) library "orp203.dll" alias for "orpo41br_Get_Sale_person_default;Ansi"


//
// Declaration for procedure: orpo42br_so_by_cust
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo42br_so_by_cust( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_info_in, &
    ref string header_info_out, &
    string detail_info_in, &
    ref string detail_info_out, &
    ref double task_number, &
    ref int page_number, &
    int CommHnd &
) library "orp203.dll" alias for "orpo42br_so_by_cust;Ansi"


//
// Declaration for procedure: orpo43br_Roll_out
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo43br_Roll_out( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string_in, &
    string Detail_String_in, &
    ref string header_string_out, &
    ref string detail_string_out, &
    int CommHnd &
) library "orp203.dll" alias for "orpo43br_Roll_out;Ansi"


//
// Declaration for procedure: orpo44br_pa_inq_inquire
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo44br_pa_inq_inquire( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_info_in, &
    string main_info_in, &
    char ind_process_option, &
    double task_number_in, &
    double page_number_in, &
    ref string main_info_out, &
    ref string so_info_out, &	
    ref double task_number_out, &
    ref double page_number_out, &
    int CommHnd &  
) library "orp203.dll" alias for "orpo44br_pa_inq_inquire;Ansi"


//
// Declaration for procedure: orpo45br_co_so
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo45br_co_so( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp203.dll" alias for "orpo45br_co_so;Ansi"


//
// Declaration for procedure: orpo46br_res_util
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo46br_res_util( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string detail_info, &
    ref string perc_String, &
    ref double task_number, &
    ref int page_number, &
    int CommHnd &
) library "orp203.dll" alias for "orpo46br_res_util;Ansi"


//
// Declaration for procedure: orpo47br_res_short
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo47br_res_short( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_info_in, &
    ref string header_info_out, &
    ref string detail_info_out, &
    string as_shortage_lines, &
    char ind_rpc, &
    int CommHnd &
) library "orp203.dll" alias for "orpo47br_res_short;Ansi"


//
// Declaration for procedure: orpo48br_update_shortage
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo48br_update_shortage( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_info_in, &
    string detail_info_in, &
    ref string detail_info_out, &
    char ind_rpc, &
    int CommHnd &
) library "orp203.dll" alias for "orpo48br_update_shortage;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "orp203.dll"
function int WBorp203CkCompleted( int CommHnd ) &
    library "orp203.dll"
//
// ***********************************************************


end prototypes

type variables
Private:
int ii_orp003_commhandle
u_OleCom	iu_OleProphet


end variables

forward prototypes
public function integer nf_orpo45ar (ref s_error astr_error_info, string as_input_string, ref string as_output)
public function integer nf_orpo40ar (ref s_error astr_error_info, character ac_sku_product_code[10], ref string as_valid_product_info)
public function integer nf_orpo41ar (ref s_error astr_error_info, string as_userid, ref string as_sales_person_info_string)
public function integer nf_orpo43ar (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string)
public function integer nf_orpo37ar (ref s_error astr_error_info, ref string as_instruction_key, string as_perm_ins_in, ref string as_perm_out)
public function integer nf_orpo36ar (ref string as_header_string, ref s_error astr_error_info, ref string as_detail_string, ref string as_so_id_string, ref double ad_tasknumber, ref integer ai_page_number)
public function integer nf_orpo42ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref double ad_tasknumber, ref integer ai_pagenumber)
public function integer nf_orpo46ar (ref s_error astr_error_info, ref string as_detail_info, ref string as_per_string, ref double ad_task_number, ref integer ai_page_number)
public function integer nf_orpo33ar (ref s_error astr_error_info, string as_order_id, ref string as_detail_info, ref string as_weight_str, ref string as_res_reduce_in, ref string as_pa_resolve_out, string as_action_ind)
public function integer nf_orpo34ar (ref s_error astr_error_info, ref string as_customer_info, ref string as_weight_string)
public function integer nf_orpo39ar (ref s_error astr_error_info, character ac_calling_window_ind, ref string as_rescus_header, ref string as_rescus_detail, ref string as_additional_data, string as_res_info)
public function integer nf_orpo38ar (ref s_error astr_error_info, character ac_process_option, character ac_availability_date[10], string as_page_descr_in, ref string as_plant_data, ref string as_detail_data, ref string as_file_descr_out)
public function integer nf_orpo44ar (ref s_error astr_error_info, ref string as_input_info_in, ref string as_main_info_in, character ac_ind_process_option, double ad_task_number_in, double ad_page_number_in, ref string as_main_info_out, ref string as_so_info_out, ref double ad_task_number_out, ref double ad_page_number_out)
public function integer nf_orpo32ar (ref s_error astr_error_info, ref string as_header_info, character ac_action_ind, ref string as_weight_overide_info, ref string as_weight_cost_info, ref character ac_addition_in)
public function integer nf_orpo35ar (ref s_error astr_error_info, ref string as_sales_id, character ac_auto_resolve, string as_shortage_lines)
public function integer nf_orpo47ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, string as_shortage_lines, character ac_ind)
public function integer nf_orpo48ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, character ac_ind)
public function boolean nf_write_benchmark2 (time at_starttime, time at_endtime, string as_function_name, time at_start_rpc, time at_end_rpc, string as_other_info, string as_task_number)
end prototypes

public function integer nf_orpo45ar (ref s_error astr_error_info, string as_input_string, ref string as_output);Int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, ls_output
			
Double	ld_task_number, &
			ld_last_number, &
			ld_max_number


SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp("Wait...PC communicating with Mainframe")

as_output = ""
ld_task_number = 0
ld_last_number = 0
ld_max_number = 0
ls_output = SPACE(20000)
ls_output = FillA(CharA(0), 20000)
as_input_string = Trim( as_Input_String)

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message)
// /// //// /////
Do
	li_rtn = orpo45br_co_so(ls_app_name, &
									ls_window_name, &
									ls_function_name, &
									ls_event_name, &
									ls_procedure_name, &
									ls_user_id,&
									ls_return_code,&
									ls_message, &
									as_input_string, &
									ls_output, &
									ld_task_number, &
									ld_last_number, &
									ld_max_number, &
									ii_orp003_commhandle) 
	as_output += ls_output
Loop While ld_last_number <> ld_max_number
// /// //// /////
nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message)				 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function integer nf_orpo40ar (ref s_error astr_error_info, character ac_sku_product_code[10], ref string as_valid_product_info);int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



as_valid_product_info = FillA( CharA(0), 474)

gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")
SetPointer(HourGlass!)


nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )


// Call a Netwise external function to get the product information
li_rtn = orpo40br_valid_product_info(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, ac_sku_product_code, &
			as_valid_product_info, ii_orp003_commhandle)


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 



// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)


return li_rtn

end function

public function integer nf_orpo41ar (ref s_error astr_error_info, string as_userid, ref string as_sales_person_info_string);Integer	li_rtn
String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

as_sales_person_info_string = SPACE(12)
as_sales_person_info_string = FillA(CharA(0), 12)

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )
// Call a Netwise external function to get the required information
li_rtn = orpo41br_get_sale_person_default( &
														ls_app_name, &
														ls_window_name, &
														ls_function_name, &
														ls_event_name, &
														ls_procedure_name, &
														ls_user_id, &
														ls_return_code, &
														ls_message, &
														as_userid, &
														as_sales_person_info_string, &
														ii_orp003_commhandle)
///////////////////////////////////////////////////////////////
nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn

end function

public function integer nf_orpo43ar (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string);Int 	li_rtn

String 	ls_detail_string_in,&
			ls_detail_string_out,&
		 	ls_header_string_in,&
		 	ls_header_string_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



gw_NetWise_Frame.SetMicroHelp( "Wait ... Rolling Out Reservation")
SetPointer(HourGlass!)

ls_detail_string_out = Space(16999)
ls_header_string_out = Space(208)

ls_detail_string_out = FillA( CharA(0), 16999)
ls_header_string_out = FillA(CharA(0), 208)


ls_detail_string_in  = Trim( as_detail_string)
ls_header_string_in  = Trim( as_header_string)

astr_error_info.se_procedure_name = 'orpo43br_Roll_out'
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_rtn =  orpo43br_Roll_out( &
    ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message, &
    ls_header_string_in, &
    ls_detail_string_in, &
    ls_header_string_out,&
    ls_detail_string_out,&
    ii_orp003_commhandle) 

as_header_string =  TRIM(ls_header_string_out)
as_detail_string =  TRIM(ls_detail_string_out)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn

end function

public function integer nf_orpo37ar (ref s_error astr_error_info, ref string as_instruction_key, string as_perm_ins_in, ref string as_perm_out);int li_rtn

STRING	ls_instruction_out,&
			ls_MicroHelp, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



ls_MicroHelp = "Wait...PC communicating with Mainframe"

ls_instruction_out = SPACE(216)
as_Perm_out = Space(216)

ls_instruction_out = FillA(CharA(0),216)
as_Perm_out = FillA(CharA(0),216)
as_instruction_key = Trim(as_instruction_key)
as_Perm_ins_in = Trim( as_Perm_ins_in)

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( ls_MicroHelp)


nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

// Call a Netwise external function to get the required information
li_rtn = orpo37br_get_instructions(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, as_instruction_key, ls_instruction_out, &
			as_Perm_ins_in, as_Perm_out, ii_orp003_commhandle)
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

as_instruction_key = ls_instruction_out


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)


Return li_rtn

end function

public function integer nf_orpo36ar (ref string as_header_string, ref s_error astr_error_info, ref string as_detail_string, ref string as_so_id_string, ref double ad_tasknumber, ref integer ai_page_number);int 		li_rtn

String 	ls_header_string_in, &
			ls_header_string_out,&
			ls_detail_string_in, &
			ls_detail_string_out, &
			ls_so_id_string_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

ls_header_string_in = as_header_string
ls_detail_string_in = as_detail_string

ls_header_string_out = Space(208)
ls_so_id_string_out = Space(12871)
ls_detail_string_out = Space(21483)
ls_header_string_out = FillA(CharA(0),208)
ls_so_id_string_out = FillA(CharA(0),12871)
ls_detail_string_out = FillA(CharA(0),21483)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )


// Call a Netwise external function to get the required information
li_rtn  = orpo36br_get_cust_order( ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ls_header_string_in,&
		  		   ls_detail_string_in, &
					ad_Tasknumber, &
					ai_page_number,&
			 		ls_header_string_out, &
					ls_detail_string_out,&
					ls_so_id_string_out, &
					ii_orp003_commhandle)



nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)



as_header_string = Trim(ls_header_string_out )
as_detail_string = Trim(ls_detail_string_out)
as_so_id_string  = Trim(ls_so_id_string_out)

return li_rtn




end function

public function integer nf_orpo42ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref double ad_tasknumber, ref integer ai_pagenumber);INTEGER 	li_Rtn

STRING 	ls_Header_Out, &
			ls_Detail_Out,&
			ls_MicroHelp, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



ls_MicroHelp = "Wait...PC communicating with Mainframe"

ls_Header_Out = SPACE(422)
ls_Detail_Out = SPACE(9701)

ls_Header_Out = FillA(CharA(0), 422)
ls_Detail_Out = FillA(CharA(0), 9701)

as_Header_Info = Trim(as_Header_Info)
as_Detail_Info = Trim( as_Detail_Info)

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( ls_MicroHelp)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_Rtn =  orpo42br_so_by_cust(&
   	ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message, &
    	as_Header_Info, &
		ls_Header_Out, &
    	as_Detail_Info, &
		ls_Detail_Out, &
		ad_tasknumber, &
		ai_pagenumber, &
	   ii_ORP003_CommHandle) 

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

THIS.nf_Display_Message(li_Rtn, astr_Error_Info, ii_ORP003_CommHandle)

as_header_info = ls_Header_Out
as_detail_info = ls_Detail_Out
Return li_Rtn
end function

public function integer nf_orpo46ar (ref s_error astr_error_info, ref string as_detail_info, ref string as_per_string, ref double ad_task_number, ref integer ai_page_number);Boolean  lb_message_val

Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



SetPointer(HourGlass!)

as_per_string	= Space(21)
as_Per_String	= FillA(CharA(0),21)


nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_rtn =  orpo46br_res_util( &
	ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message, &
	as_detail_info, &
	as_per_String,&
	ad_task_number,&
	ai_page_number,&
	ii_orp003_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
lb_message_val =THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)
Return li_rtn
end function

public function integer nf_orpo33ar (ref s_error astr_error_info, string as_order_id, ref string as_detail_info, ref string as_weight_str, ref string as_res_reduce_in, ref string as_pa_resolve_out, string as_action_ind);Char lc_Region
DataStore lds_Tmp 
int li_rtn

STRING	ls_detail_out, &
			ls_weight_out,&
			ls_MicroHelp,&
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,&
			ls_order_id,&
			ls_QueueString,&
			ls_QueueName,&
			ls_Age

Long		ll_RowCount,&
			ll_LoopCount,&
			ll_OleRtn

Decimal	ld_Price,&
			ld_Quantity
			
ls_MicroHelp = "Wait...PC communicating with Mainframe"
gw_NetWise_Frame.SetMicroHelp( ls_MicroHelp)
ls_Detail_Out = SPACE(30000)
ls_Weight_Out = SPACE(30000)
ls_Detail_Out = FillA(CharA(0),30000)
ls_Weight_Out = FillA(CharA(0),30000)
ls_order_id   = LeftA(as_order_id,5)
as_pa_resolve_out = SPACE(30000)
as_pa_resolve_out = FillA(CharA(0),30000)

as_Order_ID = Trim( As_Order_Id)
as_detail_info = Trim( as_detail_info)
as_weight_str = Trim(as_weight_str)
as_res_reduce_in = Trim( as_res_reduce_in)

lc_region = RightA(ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "Netwise Server Info", "ServerSuffix", "t"), 1)
Choose Case lower(lc_region)
	Case 'p'
		ls_QueueName = "ProfitQueue"
	Case 'z'
				ls_QueueName = "ProfitQueueQA"
	Case Else
		ls_QueueName = "ProfitQueueTest"
End Choose

//lds_Tmp = Create DataStore
//lds_Tmp.DataObject = 'd_sales_Detail_Part1'
//ll_RowCount  = lds_Tmp.ImportString(as_Detail_Info)
//ls_QueueString = ''
//For ll_LoopCount = 1 to ll_RowCount
//	   ld_Price = lds_tmp.GetItemDecimal(ll_LoopCount,"sales_price")
//		ld_Quantity = lds_Tmp.GetItemDecimal(ll_LoopCount,"ordered_units") 
//		ls_Age = lds_Tmp.GetItemString(ll_LoopCount,"ordered_age")
//		ls_QueueString += lds_Tmp.GetItemString(ll_LoopCount,"product_code")+"~t"+String(ld_Quantity)+"~t"+String(ld_Price)+"~t"+ls_Age+"~r~n"
//End for
if as_action_ind = 'U' then
	ls_Queuestring = "orpo33b" + "~v" + as_order_id + "~v" + as_weight_str + "~v" + as_res_reduce_in + &
								 "~v" + as_detail_info
	IF LenA(Trim(ls_QueueString)) > 0 Then
		IF Not IsValid(iu_oleprophet) Then 
			iu_oleprophet = Create u_OleCom
			ll_OleRtn = iu_OleProphet.ConnectToNewObject("ProphetQueue.ProphetSender.1")
			IF ll_OleRtn = 0 Then
				iu_OleProphet.Send(ls_QueueName,"Excel Data for Prophet",ls_QueueString)
			Else
				Destroy iu_oleprophet
			End if
		Else
			iu_OleProphet.Send(ls_QueueName,"Excel Data for Prophet",ls_QueueString)
		End if
	End if
End If 

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)

// Call a Netwise external function to get the required information
li_rtn = orpo33br_get_order_detail(	ls_app_name, &
												ls_window_name,&
												ls_function_name,&
												ls_event_name, &
												ls_procedure_name, &
												ls_user_id, &
												ls_return_code, &
												ls_message, &
												ls_order_id,&
												as_detail_info, &
												ls_Detail_Out, &
												as_weight_str, &
												ls_Weight_Out, &
												as_res_reduce_in, &
												as_pa_resolve_out, &
												as_action_ind, &
												ii_orp003_commhandle)
			
			

as_detail_info = 	ls_Detail_Out
as_weight_str =	ls_Weight_Out

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )
					
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)


return li_rtn
end function

public function integer nf_orpo34ar (ref s_error astr_error_info, ref string as_customer_info, ref string as_weight_string);int 		li_rtn
	

String	ls_customer_info_out,&
			ls_Weight_string_out
			
			
String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message
			
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")
SetPointer(HourGlass!)

ls_customer_info_out = Space(30000)
ls_Weight_string_out = Space(30000)

ls_customer_info_out = FillA(CharA(0),30000)
ls_Weight_string_out = FillA(CharA(0),30000)
as_Weight_string		= Trim(as_Weight_string)


nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

// Call a Netwise external function to get the customer information
li_rtn = orpo34br_Get_Order_Cust_info( &
    ls_app_name, &
	 ls_window_name,&
	 ls_function_name,&
	 ls_event_name, &
	 ls_procedure_name, &
	 ls_user_id, &
	 ls_return_code, &
	 ls_message, &
    as_customer_info, &
    ls_customer_info_out, &
    as_Weight_string, &
    ls_Weight_string_out, &
	 ii_orp003_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

as_customer_info 	= Trim(ls_customer_info_out)
as_weight_string	= Trim( ls_Weight_string_out)
return li_rtn

end function

public function integer nf_orpo39ar (ref s_error astr_error_info, character ac_calling_window_ind, ref string as_rescus_header, ref string as_rescus_detail, ref string as_additional_data, string as_res_info);Char			lc_Region
DataStore 	lds_Tmp, lds_temp_data

TIME		lt_startTime, &
			lt_endTime, &
			lt_start_RPC, &
			lt_end_RPC

int 		li_rtn

Long		ll_RowCount,&
			ll_LoopCount,&
			ll_OleRtn, ll_row
			
Decimal	ldec_Price,&
			ldec_quantity
			

String 	ls_rescus_detail_out, &
		 	ls_rescus_Header_out, &
			ls_Additional_data_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id, &
			ls_return_code, &
			ls_message,&
			ls_QueueString,&
			ls_QueueName,&
			ls_Age, &
			ls_other_info, &
			ls_task_number, &
			ls_start_time, &
			ls_end_time

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

ls_rescus_detail_out = Space(60000)
ls_rescus_Header_out = Space(60000)
ls_rescus_detail_out = FillA(CharA(0), 60000)
ls_rescus_Header_out = FillA(CharA(0), 60000)
ls_Additional_data_out = Space(60000)
ls_Additional_data_out = FillA(CharA(0),60000)
ls_start_time = Space(8)
ls_start_time = FillA(CharA(42), 8)
ls_end_time = Space(8)
ls_end_time = FillA(CharA(42), 8)
ls_task_number = Space(10)
ls_task_number = FillA(CharA(42), 10)


lc_region = RightA(ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "Netwise Server Info", "ServerSuffix", "t"), 1)
Choose Case lower(lc_region)
	Case 'p'
		ls_QueueName = "ProfitQueue"
	Case 'z'
				ls_QueueName = "ProfitQueueQA"
	Case Else
		ls_QueueName = "ProfitQueueTest"
End Choose

//lds_Tmp = Create DataStore
//lds_Tmp.DataObject = 'd_customer_order'
//ll_RowCount  = lds_Tmp.ImportString(as_rescus_detail)
//ls_QueueString = ''
//For ll_LoopCount = 1 to ll_RowCount
//	   ldec_Price = lds_tmp.GetItemDecimal(ll_LoopCount,"sales_price")
//		ldec_Quantity = lds_Tmp.GetItemDecimal(ll_LoopCount,"ordered_units") 
//		ls_Age = lds_Tmp.GetItemString(ll_LoopCount,"ordered_age")
//		ls_QueueString += lds_Tmp.GetItemString(ll_LoopCount,"sku_product_code")+"~t"+String(ldec_Quantity)+"~t"+String(ldec_Price)+"~t"+ls_Age+"~r~n"
//End for
ls_QueueString = "orpo39b" + "~v" + string(ac_calling_window_ind) + "~v" + as_additional_data + "~v" &
				+ as_res_info + "~v" + as_rescus_header + "~v" + as_rescus_detail
IF LenA(Trim(ls_QueueString)) > 0 Then
	IF Not IsValid(iu_oleprophet) Then 
		iu_oleprophet = Create u_OleCom
		ll_OleRtn = iu_OleProphet.ConnectToNewObject("ProphetQueue.ProphetSender.1")
		IF ll_OleRtn = 0 Then
			iu_OleProphet.Send(ls_QueueName,"Excel Data for Prophet",ls_QueueString)
		Else
			Destroy iu_oleprophet
		End if
	Else
		iu_OleProphet.Send(ls_QueueName,"Excel Data for Prophet",ls_QueueString)
	End if
End if

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
		
lt_startTime = Now()

// Call a Netwise external function to get the required information
li_rtn = orpo39br_gen_sales_ord(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ac_Calling_window_ind, &
    				as_rescus_header, &
    				as_rescus_detail, &
    				as_additional_Data, &
    				as_res_info, &
    				ls_rescus_header_out, &
    				ls_rescus_detail_out, &
    				ls_Additional_data_out, &
					ls_start_time, &
					ls_end_time, &
					ls_task_number, &
    				ii_orp003_commhandle) 
			
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

lt_endTime = Now()
ls_procedure_name = 'nf_orp39ar'
//lt_start_RPC = Time(ls_start_time)
//lt_end_RPC = Time(ls_end_time)

lt_start_RPC = Time(ls_window_name)
lt_end_RPC = Time(ls_function_name)
lds_temp_data = create Datastore
choose case ac_calling_window_ind
	case 'R'
		lds_temp_data.dataobject = 'd_reservation_long'
	case 'C'
		lds_temp_data.dataobject = 'd_customer_order_rpc'
end choose
ll_row = lds_temp_data.importstring(ls_rescus_detail_out)
ls_other_info = "Number of detail lines = " + string(ll_row)
ls_task_number = ls_app_name

//benchmark call
nf_write_benchmark2(lt_startTime, lt_endtime, ls_procedure_name, &
					lt_start_RPC, lt_end_RPC, ls_other_info, ls_task_number)
			
as_rescus_detail = ls_rescus_detail_out 
as_rescus_header = ls_rescus_Header_out 
as_additional_Data = ls_Additional_data_out

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)
IF li_rtn = 8 Then
	MessageBox("DB2 ERROR","* * * A DeadLock has occurred * * *")
	Return 5
End If
Return li_rtn

end function

public function integer nf_orpo38ar (ref s_error astr_error_info, character ac_process_option, character ac_availability_date[10], string as_page_descr_in, ref string as_plant_data, ref string as_detail_data, ref string as_file_descr_out);INTEGER 	li_rtn, &
			li_other_info

STRING	ls_plants_out, &
			ls_detail_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_start_RPC, &
			ls_end_RPC, &
			ls_other_info, &
			ls_task_number
			
TIME		lt_startTime, &
			lt_endTime, &
			lt_start_RPC, &
			lt_end_RPC

ls_message = SPACE(71)
ls_plants_out = SPACE(75)
ls_detail_out = SPACE(6100)
as_file_descr_out = SPACE(500)

ls_plants_out = FillA( CharA(0), 75)
ls_detail_out = FillA( CharA(0), 6100)
as_file_descr_out = FillA( CharA(0), 500)
ls_message = FillA( CharA(0), 71)

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_startTime = Now()
										
// Call a Netwise external function to get the required information
li_rtn = orpo38br_pa_summary(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ac_process_option, &
					ac_availability_date, &
					as_page_descr_in, &
					as_plant_data, &
					as_detail_data, &
					ls_plants_out, &
					ls_detail_out, &
					as_file_descr_out, &
					ii_orp003_commhandle)

lt_endTime = Now()
ls_procedure_name = 'nf_orp38ar'
lt_start_RPC = Time(ls_window_name)
lt_end_RPC = Time(ls_function_name)
ls_other_info = "Products/Plants: " + ls_event_name
ls_task_number = ls_app_name

//benchmark call
nf_write_benchmark2(lt_startTime, lt_endtime, ls_procedure_name, &
					lt_start_RPC, lt_end_RPC, ls_other_info, ls_task_number)

as_plant_data = TRIM(ls_plants_out)
as_detail_data = TRIM(ls_detail_out)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

return li_rtn
end function

public function integer nf_orpo44ar (ref s_error astr_error_info, ref string as_input_info_in, ref string as_main_info_in, character ac_ind_process_option, double ad_task_number_in, double ad_page_number_in, ref string as_main_info_out, ref string as_so_info_out, ref double ad_task_number_out, ref double ad_page_number_out);Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_detail_info_out

as_so_info_out = SPACE(20000)
as_so_info_out = FillA(CharA(0), 20000) 
as_main_info_out = SPACE(60000)
as_main_info_out = FillA(CharA(0), 60000)

astr_error_info.se_Message = Space(70)
astr_error_info.se_procedure_name = 'orpo44br_pa_inq_inquire'

gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")
SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_rtn =  orpo44br_pa_inq_inquire(ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message, &
		as_input_info_in, &
		as_main_info_in, &
		ac_ind_process_option, &
		ad_task_number_in, &
		ad_page_number_in, &
		as_main_info_out, &
		as_so_info_out, &
		ad_task_number_out, &
		ad_page_number_out, &
		ii_orp003_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function integer nf_orpo32ar (ref s_error astr_error_info, ref string as_header_info, character ac_action_ind, ref string as_weight_overide_info, ref string as_weight_cost_info, ref character ac_addition_in);Int 		li_rtn
String	ls_header_out, &
			ls_weight_overide_out, &
			ls_weight_cost_out

char     lc_addition_out

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message			

ls_header_out 				= SPACE(30000)
ls_weight_overide_out 	= SPACE(30000)
ls_weight_cost_out		= SPACE(30000)	
ls_header_out 				= FillA(CharA(0),30000)
ls_weight_overide_out 	= FillA(CharA(0),30000)
ls_weight_cost_out		= FillA(CharA(0),30000)
lc_addition_out			= SPACE(1)

as_header_info 			= Trim(as_Header_info)
as_weight_overide_info 	= Trim( as_weight_overide_info)
as_weight_cost_info		= Trim(as_weight_cost_info)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)

// Call a Netwise external function to get the customer information
li_rtn = orpo32br_GetHeaderInfo(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_header_info, &
					ls_header_out, &
					ac_action_ind, &
					as_weight_overide_info, &
					ls_weight_overide_out, &
					as_weight_cost_info, &
					ls_weight_cost_out, &
					ac_addition_in, &
					lc_addition_out, &
					ii_orp003_commhandle)

as_weight_overide_info	=	ls_weight_overide_out
as_weight_cost_info		=	ls_weight_cost_out
as_header_info				=	ls_header_out
ac_addition_in				=  lc_addition_out

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)
return li_rtn
end function

public function integer nf_orpo35ar (ref s_error astr_error_info, ref string as_sales_id, character ac_auto_resolve, string as_shortage_lines);Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message			 

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

li_rtn  = orpo35br_accept_shortage(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message ,&
					as_sales_id, &
					ac_Auto_Resolve ,&
					as_shortage_lines, &
					ii_orp003_commhandle ) 

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)
return li_rtn
end function

public function integer nf_orpo47ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, string as_shortage_lines, character ac_ind);Boolean  lb_message_val

Int 		li_rtn

STRING	ls_header_info_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

ls_header_info_out = SPACE(34)
as_detail_info = SPACE(21000)
as_detail_info = FillA(CharA(0),21000)

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp("Retrieving data. Please Wait.")

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_rtn =  orpo47br_res_short( &
    	ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message, &
		as_header_info, &
		ls_header_info_out, &	
    	as_detail_info, &
		as_shortage_lines, &
		ac_ind, &
	   ii_orp003_commhandle)

nf_set_s_error (astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

as_header_info = TRIM(ls_header_info_out)
as_detail_info = TRIM(as_detail_info)

THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function integer nf_orpo48ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, character ac_ind);Boolean  lb_message_val

Int 		li_rtn

STRING 	ls_detail_info_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

ls_detail_info_out = SPACE(21000)
ls_detail_info_Out = FillA(CharA(0),21000)

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp("Recalculating scheduled quantities. Please Wait.")

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_rtn =  orpo48br_Update_Shortage( &
    	ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message, &
		as_header_info, &
    	as_detail_info, &
		ls_detail_info_out, &
		ac_ind, &
	   ii_orp003_commhandle)

as_detail_info = TRIM(ls_detail_info_out)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 
	
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function boolean nf_write_benchmark2 (time at_starttime, time at_endtime, string as_function_name, time at_start_rpc, time at_end_rpc, string as_other_info, string as_task_number);char 					lc_PCIP_Address[100], &
						lc_region

Integer				li_fileNum, &
						li_TimeVal
						
Long					ll_SecondsAfterPC, &
						ll_secondsAfterRPC

String				ls_FileName, &
						ls_ipaddr, &
						ls_pcip_address, &
						ls_path

u_ip_functions		lu_ip_address_functions


li_TimeVal = ProfileInt(gw_netwise_frame.is_WorkingDir + "ibp002.ini",&
								"BenchMark",&
								Message.nf_Get_App_ID(),6)
								
IF li_timeVal = 0 Then Return False								

ll_SecondsAfterPC = SecondsAfter(at_starttime, at_endtime) 
ll_secondsAfterRPC = SecondsAfter(at_start_rpc, at_end_rpc)

IF ll_SecondsAfterPC < li_TimeVal Then Return False 

lc_region = RightA(ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "nw_server", "server" + &
			String(1), ""), 1)
Choose Case lc_region
	Case 'p'
		ls_path = ProfileString(gw_netwise_frame.is_WorkingDir + 'ibp002.ini', "BenchMark", 'PATH', "f:\software\pb\pblog101\")
//		ls_path = "f:\software\pb\pblog101\"
	Case 't'
		ls_path = ProfileString(gw_netwise_frame.is_WorkingDir + 'ibp002.ini', "BenchMark", 'PATH', "j:\test\pb\")
//		ls_path = "j:\pb\test\exe32\"
	Case 'z'
		ls_path = ProfileString(gw_netwise_frame.is_WorkingDir + 'ibp002.ini', "BenchMark", 'PATH', "\\NTWHQ03\NTWHQ03\software\pb\pblog101\")
//		ls_path = "j:\software\pb\qa\pblog101\"
	Case ''
		Return False
End Choose

ls_fileName = ls_path + "BM" + &
					String(Month(Today()), '00') + &
					String(Day(Today()), '00') + &
					".log"

li_FileNum = FileOpen(ls_FileName,  &
	lineMode!, Write!, LockWrite! , Append! )
	
ls_ipaddr = lu_ip_address_functions.nf_get_ip_address()

FileWrite(li_FileNum, Message.nf_Get_App_ID() + &
			"~tUser: " + Trim(SQLCA.Userid) + &
			"~tIP Addr: " + Trim(ls_ipaddr) + &
			"~tTask Number: " + Trim(as_task_number) + &
			"~tProcedure Name: " + Trim(as_function_name) + &
			"~tPC Start: " + Trim(String(At_StartTime,"hh:mm:ss")) + &
			"~tPC End: " + Trim(String(At_EndTime,"hh:mm:ss")) + &
			"~tPC Elapsed: " + Trim(String(ll_SecondsAfterPC)) + &
			"~tRPC Start: " + Trim(String(at_start_rpc,"hh:mm:ss")) + &
			"~tRPC End: " + Trim(String(at_end_rpc,"hh:mm:ss")) + &
			"~tRPC Elapsed: " + Trim(String(ll_secondsAfterRPC)) + &
			"~t" + Trim(as_other_info))

FileClose(li_FileNum)

Return True
end function

event constructor;call super::constructor;STRING	ls_MicroHelp

ls_MicroHelp = gw_NetWise_Frame.wf_GetMicroHelp()

gw_NetWise_Frame.SetMicroHelp( "Opening MainFrame Connections")

ii_orp003_commhandle = SQLCA.nf_GetCommHandle("orp003")

If ii_orp003_commhandle = 0 Then
	Message.ReturnValue = -1
End if

gw_NetWise_Frame.SetMicroHelp(ls_MicroHelp)

end event

on u_orp003.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_orp003.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

