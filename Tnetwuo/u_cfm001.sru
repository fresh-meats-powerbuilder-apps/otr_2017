HA$PBExportHeader$u_cfm001.sru
forward
global type u_cfm001 from u_netwise_transaction
end type
end forward

type wstr_refetch_customer from structure
    character refetch_id[7]
    character refetch_type
end type

global type u_cfm001 from u_netwise_transaction
end type
global u_cfm001 u_cfm001

type prototypes
Private:
// PowerBuilder Script File: j:\dev\workbe~1\src32\cfm201.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Tue Dec 21 15:10:42 1999
// Source Interface File: j:\dev\workbe~1\src32\cfm201.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: cfmc01br_tcfmsubs
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int cfmc01br_tcfmsubs( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    ref string output_string, &
    string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "cfm201.dll" alias for "cfmc01br_tcfmsubs;Ansi"


//
// Declaration for procedure: cfmc05br_tcfmcarr
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int cfmc05br_tcfmcarr( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    ref string output_string, &
    string input_string, &
    int CommHnd &
) library "cfm201.dll" alias for "cfmc05br_tcfmcarr;Ansi"


//
// Declaration for procedure: cfmc09br_tcfmiloc
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int cfmc09br_tcfmiloc( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    ref string output_string, &
    string input_string, &
    int CommHnd &
) library "cfm201.dll" alias for "cfmc09br_tcfmiloc;Ansi"


//
// Declaration for procedure: cfmc25br_product_age_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int cfmc25br_product_age_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "cfm201.dll" alias for "cfmc25br_product_age_inq;Ansi"


//
// Declaration for procedure: cfmc27br_product_age_update
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int cfmc27br_product_age_update( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "cfm201.dll" alias for "cfmc27br_product_age_update;Ansi"


//
// Declaration for procedure: cfmc28br_service_center
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int cfmc28br_service_center( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "cfm201.dll" alias for "cfmc28br_service_center;Ansi"


//
// Declaration for procedure: cfmc31br_skugrp_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int cfmc31br_skugrp_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "cfm201.dll" alias for "cfmc31br_skugrp_inq;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "cfm201.dll"
function int WBcfm201CkCompleted( int CommHnd ) &
    library "cfm201.dll"
//
// ***********************************************************


end prototypes

type variables
Int ii_commhandle
end variables

forward prototypes
public function boolean nf_carrier_preference (ref s_error astr_error_info, string as_header, ref string as_inquire, string as_update)
public function boolean nf_location_preference (ref s_error astr_error_info, string as_header, ref string as_inquire, string as_update)
public function boolean nf_product_substitution (ref s_error astr_error_info, string as_header, ref string as_inquire, string as_update)
public function boolean nf_cfmc25br_product_age_inq (ref s_error astr_error_info, string as_inputstring, ref string as_outputstring)
public function boolean nf_cfmc27br_product_age_update (ref s_error astr_error_info, string as_inputstring, ref string as_outputstring)
public function boolean nf_cfmc28br_service_center_inq (ref s_error astr_error_info, string as_inputstring, ref string as_outputstring)
public function boolean nf_cfmc31br_skugrp_inq (ref s_error astr_error_info, string as_inputstring, ref string as_outputstring)
end prototypes

public function boolean nf_carrier_preference (ref s_error astr_error_info, string as_header, ref string as_inquire, string as_update);Int	li_ret

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

If PosA(RightA(as_header, 3) , 'I') > 0 Then
	as_inquire     = Space(1601)
End if


astr_error_info.se_procedure_name = "cfmc05ar_tcfmcarr"
astr_error_info.se_message = Space(70)


nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_ret = cfmc05br_tcfmcarr(ls_app_name, &
									ls_window_name,&
									ls_function_name,&
									ls_event_name, &
									ls_procedure_name, &
									ls_user_id, &
									ls_return_code, &
									ls_message, &
				   				as_header, &
									as_inquire, &
									as_update, &
									ii_commhandle) 

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )


return nf_display_message(li_ret, astr_error_info, ii_commhandle)
end function

public function boolean nf_location_preference (ref s_error astr_error_info, string as_header, ref string as_inquire, string as_update);Int	li_ret

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

If PosA(RightA(as_header, 3) , 'I') > 0 Then
	as_inquire     = Space(1201)
End if


astr_error_info.se_procedure_name = "cfmc09ar_tcfmiloc"
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_ret = cfmc09br_tcfmiloc(ls_app_name, &
									ls_window_name,&
									ls_function_name,&
									ls_event_name, &
									ls_procedure_name, &
									ls_user_id, &
									ls_return_code, &
									ls_message, &
				   				as_header, &
									as_inquire, &
									as_update, &
									ii_commhandle) 

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

return nf_display_message(li_ret, astr_error_info, ii_commhandle)
end function

public function boolean nf_product_substitution (ref s_error astr_error_info, string as_header, ref string as_inquire, string as_update);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_ret

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_output

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
//ls_output = Space(5601)
//ls_output = Fill(char(0),20000)
If PosA(RightA(as_header, 3) , 'I') > 0 Then
	ls_output     = Space(5601)
End if


astr_error_info.se_procedure_name = "cfmc01ar_tcfmsubs"
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )	
		
do

	li_ret = cfmc01br_tcfmsubs(ls_app_name, &
										ls_window_name,&
										ls_function_name,&
										ls_event_name, &
										ls_procedure_name, &
										ls_user_id, &
										ls_return_code, &
										ls_message, &
										as_header, &
										ls_output, &
										as_update, & 
										ld_task_number, &
										ld_last_record_number, &
										ld_max_record_number, &
										ii_commhandle)
										
	as_inquire += ls_output								
									
Loop while ld_last_record_number <> ld_max_record_number and li_ret > -1
									
nf_set_s_error ( astr_error_info, &
					ls_app_name, &
					ls_window_name, &
					ls_function_name, &
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )									

return nf_display_message(li_ret, astr_error_info, ii_commhandle)
end function

public function boolean nf_cfmc25br_product_age_inq (ref s_error astr_error_info, string as_inputstring, ref string as_outputstring);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_ret

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_output

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
as_outputstring = ""

ls_output = Space(10000)
ls_output = FillA(CharA(0),10000)

astr_error_info.se_procedure_name = "cfmc25br_product_age_inq"
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )	
		
do
	
//	messagebox("Data Input", as_inputstring)

	li_ret = cfmc25br_product_age_inq(ls_app_name, &
												ls_window_name,&
												ls_function_name,&
												ls_event_name, &
												ls_procedure_name, &
												ls_user_id, &
												ls_return_code, &
												ls_message, &
												as_inputstring, &
												ls_output, &
												ld_task_number, &
												ld_last_record_number, &
												ld_max_record_number, &
												ii_commhandle)

//	ls_output = "1" + '~t' +"one" + '~t' + "123" + '~t' + "123" + '~r~n' + &
//					"2" + '~t' +"two" + '~t' + "0" + '~t' + "999" + '~r~n' + &
//					"3" + '~t' +"three" + '~t' + "1" + '~t' + "15" + '~r~n' 
										
	as_outputstring += ls_output
//	messagebox("Data Output", as_outputstring)
									
Loop while ld_last_record_number <> ld_max_record_number and li_ret > -1
									
nf_set_s_error ( astr_error_info, &
					ls_app_name, &
					ls_window_name, &
					ls_function_name, &
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )									

return nf_display_message(li_ret, astr_error_info, ii_commhandle)
end function

public function boolean nf_cfmc27br_product_age_update (ref s_error astr_error_info, string as_inputstring, ref string as_outputstring);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_ret

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_output

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
as_outputstring = ""

ls_output = Space(10000)
ls_output = FillA(CharA(0),10000)

astr_error_info.se_procedure_name = "cfmc27br_product_age_update"
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )	
		
do
	
//	messagebox("Data Input", as_inputstring)

	li_ret = cfmc27br_product_age_update(ls_app_name, &
												ls_window_name,&
												ls_function_name,&
												ls_event_name, &
												ls_procedure_name, &
												ls_user_id, &
												ls_return_code, &
												ls_message, &
												as_inputstring, &
												ls_output, &
												ld_task_number, &
												ld_last_record_number, &
												ld_max_record_number, &
												ii_commhandle)

//	ls_output = "one" + '~t' + "123" + '~t' + "123" + '~r~n' + &
//					"two" + '~t' + "0" + '~t' + "999" + '~r~n' + &
//					"three" + '~t' + "1" + '~t' + "15" + '~r~n' 
										
	as_outputstring += ls_output
//	messagebox("Data Output", as_outputstring)
									
Loop while ld_last_record_number <> ld_max_record_number and li_ret > -1
									
nf_set_s_error ( astr_error_info, &
					ls_app_name, &
					ls_window_name, &
					ls_function_name, &
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )									

return nf_display_message(li_ret, astr_error_info, ii_commhandle)
end function

public function boolean nf_cfmc28br_service_center_inq (ref s_error astr_error_info, string as_inputstring, ref string as_outputstring);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_ret

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_output

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
as_outputstring = ""

ls_output = Space(1000)
ls_output = FillA(CharA(0),1000)

astr_error_info.se_procedure_name = "cfmc28br_service_center"
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )	
		
do
	
//	messagebox("Data Input", as_inputstring)

	li_ret = cfmc28br_service_center(ls_app_name, &
												ls_window_name,&
												ls_function_name,&
												ls_event_name, &
												ls_procedure_name, &
												ls_user_id, &
												ls_return_code, &
												ls_message, &
												as_inputstring, &
												ls_output, &
												ld_task_number, &
												ld_last_record_number, &
												ld_max_record_number, &
												ii_commhandle)

//	ls_output = "00" + '~t' + "605-235-3217" + '~t' + "E-mail Address" + '~t' + "PerishDirectName" + '~t' + &
//					"MeatdirectName" + '~t' + "buyername" + '~t' + "AsstBuyerName" + '~t' + "101" + '~t' + &
//					" " + '~t' + "D" + '~t' + "Y" + '~t' + "Y" + '~t' + "Y" + '~t' + "F" + '~t' + "405" + '~t' + &
//					"01" + '~t' + "W" + '~t' + "Y" + '~t' + "1" + '~t' + "Y" + '~r~n'
										
	as_outputstring += ls_output
//	messagebox("Data Output", as_outputstring)
									
Loop while ld_last_record_number <> ld_max_record_number and li_ret > -1
									
nf_set_s_error ( astr_error_info, &
					ls_app_name, &
					ls_window_name, &
					ls_function_name, &
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )									

return nf_display_message(li_ret, astr_error_info, ii_commhandle)
end function

public function boolean nf_cfmc31br_skugrp_inq (ref s_error astr_error_info, string as_inputstring, ref string as_outputstring);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_ret

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_output

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
as_outputstring = ""

ls_output = Space(1000)
ls_output = FillA(CharA(0),1000)

astr_error_info.se_procedure_name = "cfmc31br_skugrp_inq"
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )	
		
do
	
//	messagebox("Data Input", as_inputstring)

	li_ret = cfmc31br_skugrp_inq(ls_app_name, &
												ls_window_name,&
												ls_function_name,&
												ls_event_name, &
												ls_procedure_name, &
												ls_user_id, &
												ls_return_code, &
												ls_message, &
												as_inputstring, &
												ls_output, &
												ld_task_number, &
												ld_last_record_number, &
												ld_max_record_number, &
												ii_commhandle)

	as_outputstring += ls_output
//	messagebox("Data Output", as_outputstring)
									
Loop while ld_last_record_number <> ld_max_record_number and li_ret > -1
									
nf_set_s_error ( astr_error_info, &
					ls_app_name, &
					ls_window_name, &
					ls_function_name, &
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )									

return nf_display_message(li_ret, astr_error_info, ii_commhandle)
end function

event constructor;call super::constructor;STRING	ls_MicroHelp

ls_MicroHelp = gw_netwise_Frame.wf_GetMicroHelp()

gw_netwise_frame.SetMicroHelp( "Opening MainFrame Connections")

ii_commhandle = SQLCA.nf_GetCommHandle("cfm001")

If ii_commhandle = 0 Then
	Message.ReturnValue = -1
End if

gw_netwise_Frame.SetMicroHelp(ls_MicroHelp)

end event

on u_cfm001.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_cfm001.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

