HA$PBExportHeader$u_orp204.sru
forward
global type u_orp204 from u_netwise_transaction
end type
end forward

global type u_orp204 from u_netwise_transaction
end type
global u_orp204 u_orp204

type prototypes
// PowerBuilder Script File: c:\ibp\src\orp204.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Thu Sep 03 14:03:32 1998
// Source Interface File: C:\ibp\src\ORP204.NTF
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: orpo81br_pa_summary
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo81br_pa_summary( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string process_option, &
    string availability_date, &
    string page_desc_in, &
    string plant_data_in, &
    string detail_data_in, &
    ref string plant_data_out, &
    ref string detail_data_out, &
    ref string file_descr_out, &
    int CommHnd &
) library "orp204.dll" alias for "orpo81br_pa_summary;Ansi"


//
// Declaration for procedure: orpo82br_remove_shortage
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo82br_remove_shortage( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string process_option, &
    string order_number, &
    string detail_data_in, &
    ref string detail_data_out, &
    int CommHnd &
) library "orp204.dll" alias for "orpo82br_remove_shortage;Ansi"


//
// Declaration for procedure: orpo83br_orders_by_tsr
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo83br_orders_by_tsr( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_info, &
    ref string detail_data_out, &
    ref double task_number, &
    ref int page_number, &
    int CommHnd &
) library "orp204.dll" alias for "orpo83br_orders_by_tsr;Ansi"


//
// Declaration for procedure: orpo84br_orders_by_tsr_sku
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo84br_orders_by_tsr_sku( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char process_option, &
    string customer_input, &
    string product_input, &
    string detail_data_in, &
    ref string detail_data, &
    ref double task_number, &
    ref int page_number, &
    int CommHnd &
) library "orp204.dll" alias for "orpo84br_orders_by_tsr_sku;Ansi"


//
// Declaration for procedure: orpo85br_mass_weekly_res
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo85br_mass_weekly_res( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_input, &
    string detail_data_in, &
    ref string header_output, &
    ref string detail_data_out, &
    ref string dates_info_out, &
    int CommHnd &
) library "orp204.dll" alias for "orpo85br_mass_weekly_res;Ansi"


//
// Declaration for procedure: orpo86br_auto_price_single_order
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo86br_auto_price_single_order( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string order_id, &
    int CommHnd &
) library "orp204.dll" alias for "orpo86br_auto_price_single_order;Ansi"


//
// Declaration for procedure: orpo87br_exclude_plants
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo87br_exclude_plants( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char process_option, &
    string order_id, &
    string customer_id, &
    char customer_type, &
    string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string output_string, &
    int CommHnd &
) library "orp204.dll" alias for "orpo87br_exclude_plants;Ansi"
//
// Declaration for procedure: orpo88br_inq_sold_by_product
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo88br_inq_sold_by_product( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp204.dll" alias for "orpo88br_inq_sold_by_product;Ansi"

// Declaration for procedure: orpo88br_inq_sold_by_product
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo89br_tla_inquiry( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp204.dll" alias for "orpo89br_tla_inquiry;Ansi"

// Declaration for procedure: orpo92br_cancelled_prod_rpt
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo92br_cancelled_prod_rpt( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "orp204.dll" alias for "orpo92br_cancelled_prod_rpt;Ansi"

// Declaration for procedure: orpo93br_load_avail_rpt
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo93br_load_avail_rpt( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "orp204.dll" alias for "orpo93br_load_avail_rpt;Ansi"

// Declaration for procedure: orpo94br_late_load_permission
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo94br_late_load_permissions( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref char late_load_notify_ind,  &
    int CommHnd &
) library "orp204.dll" alias for "orpo94br_late_load_permissions;Ansi"
// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "orp204.dll"
function int WBorp204CkCompleted( int CommHnd ) &
    library "orp204.dll"
//
// ***********************************************************


end prototypes

type variables
Private:
int	 ii_orp204_commhandle
u_OleCom	iu_oleprophet

end variables

forward prototypes
public function integer nf_orpo81br_pa_summary (ref s_error astr_error_info, character ac_process_option, character ac_availability_date[10], string as_page_descr_in, ref string as_plant_data, ref string as_detail_data, ref string as_file_descr_out)
public function integer nf_orpo82br_remove_shortage (ref s_error astr_error_info, string as_process_option, string as_order_id, string as_detail_data_in, ref string as_detail_data_out)
public function integer nf_orpo83br_so_by_tsr (ref s_error astr_error_info, string as_header_info, ref string as_detail_info, ref double ad_tasknumber, ref integer ai_pagenumber)
public function integer nf_orpo84br_so_by_sku (ref s_error astr_error_info, character ac_indicator, string as_header_info, string as_product_info, ref string as_detail_info, ref double ad_tasknumber, ref integer ai_pagenumber)
public function integer nf_orpo86br_auto_price_single_order (ref s_error astr_error_info, string as_order_id)
public function integer nf_orpo85br_mass_weekly_res (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref string as_dates_info)
public function integer nf_orpo87br_exclude_plants (ref s_error astr_error_info, character ac_process_option, string as_order_id, string as_customer, character ac_customer_type, string as_input, ref string as_output)
public function integer nf_orpo88br_sold_by_product (ref s_error astr_error_info, string as_input, ref string as_output)
public function integer nf_orpo89br_tla_inquiry (ref s_error astr_error_info, string as_input, ref string as_output)
public function integer nf_orpo92br_cancelled_product_report (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo93br_load_avail_rpt (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo94br_late_load_permission (ref s_error astr_error_info, ref character ac_late_permissions)
public function integer nf_orpo81br_pa_summary (ref s_error astr_error_info, character ac_process_option, character ac_availability_date[10], string as_page_descr_in, ref string as_plant_data, ref string as_detail_data, ref string as_file_descr_out)
public function integer nf_orpo82br_remove_shortage (ref s_error astr_error_info, string as_process_option, string as_order_id, string as_detail_data_in, ref string as_detail_data_out)
public function integer nf_orpo83br_so_by_tsr (ref s_error astr_error_info, string as_header_info, ref string as_detail_info, ref double ad_tasknumber, ref integer ai_pagenumber)
public function integer nf_orpo84br_so_by_sku (ref s_error astr_error_info, character ac_indicator, string as_header_info, string as_product_info, ref string as_detail_info, ref double ad_tasknumber, ref integer ai_pagenumber)
public function integer nf_orpo86br_auto_price_single_order (ref s_error astr_error_info, string as_order_id)
public function integer nf_orpo85br_mass_weekly_res (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref string as_dates_info)
public function integer nf_orpo87br_exclude_plants (ref s_error astr_error_info, character ac_process_option, string as_order_id, string as_customer, character ac_customer_type, string as_input, ref string as_output)
public function integer nf_orpo88br_sold_by_product (ref s_error astr_error_info, string as_input, ref string as_output)
public function integer nf_orpo89br_tla_inquiry (ref s_error astr_error_info, string as_input, ref string as_output)
public function integer nf_orpo92br_cancelled_product_report (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo93br_load_avail_rpt (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo94br_late_load_permission (ref s_error astr_error_info, ref character ac_late_permissions)
end prototypes

public function integer nf_orpo81br_pa_summary (ref s_error astr_error_info, character ac_process_option, character ac_availability_date[10], string as_page_descr_in, ref string as_plant_data, ref string as_detail_data, ref string as_file_descr_out);INTEGER 	li_rtn

STRING	ls_plants_out, &
			ls_detail_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,&
			ls_Avail_date,&
			ls_Process

ls_message = SPACE(71)
ls_plants_out = SPACE(75)
ls_detail_out = SPACE(6100)
as_file_descr_out = SPACE(500)

ls_plants_out = FillA( CharA(0), 75)
ls_detail_out = FillA( CharA(0), 6100)
as_file_descr_out = FillA( CharA(0), 500)
ls_message = FillA( CharA(0), 71)

SetPointer(HourGlass!)
gw_netwise_frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )
								
ls_Avail_date = ac_availability_date
ls_Process = ac_process_option

// Call a Netwise external function to get the required information
li_rtn = orpo81br_pa_summary(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ls_Process, &
					ls_Avail_Date, &
					as_page_descr_in, &
					as_plant_data, &
					as_detail_data, &
					ls_plants_out, &
					ls_detail_out, &
					as_file_descr_out, &
					ii_orp204_commhandle)

as_plant_data = TRIM(ls_plants_out)
as_detail_data = TRIM(ls_detail_out)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp204_commhandle)

return li_rtn
end function

public function integer nf_orpo82br_remove_shortage (ref s_error astr_error_info, string as_process_option, string as_order_id, string as_detail_data_in, ref string as_detail_data_out);INTEGER 	li_rtn

STRING	ls_plants_out, &
			ls_detail_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,&
			ls_order_id,&
			ls_process_option, &
			ls_detail_data_in, &
			ls_detail_data_out

ls_message = SPACE(71)
as_detail_data_out = SPACE(10000)

as_detail_data_out = FillA( CharA(0), 10000)
ls_message = FillA( CharA(0), 71)

SetPointer(HourGlass!)
gw_netwise_frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


// Call a Netwise external function to get the required information
li_rtn = orpo82br_remove_shortage(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_process_option, &
					as_order_id, &
					as_detail_data_in, &
					as_detail_data_out, &
					ii_orp204_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp204_commhandle)

RETURN li_rtn
end function

public function integer nf_orpo83br_so_by_tsr (ref s_error astr_error_info, string as_header_info, ref string as_detail_info, ref double ad_tasknumber, ref integer ai_pagenumber);INTEGER 	li_Rtn

STRING 	ls_Header_Out, &
			ls_Detail_Out,&
			ls_MicroHelp, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



ls_MicroHelp = "Wait...PC communicating with Mainframe"

ls_Detail_Out = SPACE(50000)
ls_Detail_Out = FillA(CharA(0), 50000)

as_Header_Info = Trim(as_Header_Info)

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( ls_MicroHelp)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_Rtn =  orpo83br_orders_by_tsr(&
   	ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message, &
    	as_Header_Info, &
		ls_Detail_Out, &
		ad_tasknumber, &
		ai_pagenumber, &
	   ii_ORP204_CommHandle) 

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

THIS.nf_Display_Message(li_Rtn, astr_Error_Info, ii_ORP204_CommHandle)

as_detail_info = ls_Detail_Out
Return li_Rtn
end function

public function integer nf_orpo84br_so_by_sku (ref s_error astr_error_info, character ac_indicator, string as_header_info, string as_product_info, ref string as_detail_info, ref double ad_tasknumber, ref integer ai_pagenumber);DataStore	lds_tmp

Decimal	ldec_Price,&
			ldec_Quantity

Char	lc_Region


INTEGER 	li_Rtn

Long 	ll_RowCount,&
		ll_LoopCount,&
		ll_OleRtn

STRING 	ls_MicroHelp, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,&
			ls_detail_in, ls_detail_out, &
			ls_other_info, ls_task_number,&
			ls_QueueName,&
			ls_QueueString,&
			ls_Age
			
Time		lt_startTime, lt_endtime, lt_start_RPC, lt_end_RPC

ls_MicroHelp = "Wait...PC communicating with Mainframe"
ls_detail_in = as_detail_info
ls_Detail_Out = SPACE(50000)
ls_Detail_Out = FillA(CharA(0), 50000)

as_Header_Info = Trim(as_Header_Info)
SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( ls_MicroHelp)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
///
lc_region = RightA(ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "Netwise Server Info", "ServerSuffix", "t"), 1)
Choose Case lower(lc_region)
	Case 'p'
		ls_QueueName = "ProfitQueue"
	Case 'z'
		ls_QueueName = "ProfitQueueQA"
	Case Else
		ls_QueueName = "ProfitQueueTest"
End Choose
//lds_Tmp = Create DataStore
//lds_Tmp.DataObject = 'd_sales_tsr_sku_all'
//ll_RowCount  = lds_Tmp.ImportString(as_detail_info)
//ls_QueueString = ''
//For ll_LoopCount = 1 to ll_RowCount
//	   ldec_Price = lds_tmp.GetItemDecimal(ll_LoopCount,"price")
//		ldec_Quantity = lds_Tmp.GetItemDecimal(ll_LoopCount,"ordered_units") 
//		ls_Age = lds_Tmp.GetItemString(ll_LoopCount,"ordered_age")
//		ls_QueueString += lds_Tmp.GetItemString(ll_LoopCount,"product_code")+"~t"+String(ldec_Quantity)+"~t"+String(ldec_Price)+"~t"+ls_Age+"~r~n"		
//End for
if ac_indicator = 'U' then
	ls_QueueString = "orpo84b" + "~v" + as_header_info + "~v" + as_detail_info
	IF LenA(Trim(ls_QueueString)) > 0 Then
		IF Not IsValid(iu_oleprophet) Then 
			iu_oleprophet = Create u_OleCom
			ll_OleRtn = iu_OleProphet.ConnectToNewObject("ProphetQueue.ProphetSender.1")
			IF ll_OleRtn = 0 Then
				iu_OleProphet.Send(ls_QueueName,"Excel Data for Prophet",ls_QueueString)
			Else
				Destroy iu_oleprophet
			End if
		Else
			iu_OleProphet.Send(ls_QueueName,"Excel Data for Prophet",ls_QueueString)
		End if 
	End if
end if




lt_startTime = Now()
///
li_Rtn =  orpo84br_orders_by_tsr_sku(&
   	ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message, &
    	ac_indicator, &
	   as_header_Info, &
		as_product_info, & 
		ls_detail_in, &
		ls_detail_out, &
		ad_tasknumber, &
		ai_pagenumber, &
		ii_ORP204_CommHandle) 
/// /// /// ///
lt_endTime = Now()
//ls_procedure_name = 'nf_orp84br'
//lt_start_RPC = Time(ls_window_name)
//lt_end_RPC = Time(ls_function_name)
lt_start_RPC = lt_startTime
lt_end_RPC = lt_endTime
ls_other_info = ls_event_name
ls_task_number = ls_app_name
//benchmark call
//nf_write_benchmark(lt_startTime, lt_endtime, ls_procedure_name, &
//					lt_start_RPC, lt_end_RPC, ls_other_info, ls_task_number)
/// /// /// ///				
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

THIS.nf_Display_Message(li_Rtn, astr_Error_Info, ii_ORP204_CommHandle)
as_detail_info = ls_detail_out
Return li_Rtn
end function

public function integer nf_orpo86br_auto_price_single_order (ref s_error astr_error_info, string as_order_id);INTEGER 	li_rtn

STRING	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message
			
ls_message = SPACE(71)
ls_message = FillA( CharA(0), 71)

SetPointer(HourGlass!)
gw_netwise_frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


// Call a Netwise external function to get the required information
li_rtn = orpo86br_auto_price_single_order(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_order_id, &
					ii_orp204_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp204_commhandle)

RETURN li_rtn
end function

public function integer nf_orpo85br_mass_weekly_res (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref string as_dates_info);INTEGER 	li_Rtn

STRING 	ls_MicroHelp, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,&
			ls_detail_in, ls_detail_out, &
			ls_header_in, ls_header_out

ls_MicroHelp = "Wait...PC communicating with Mainframe"
ls_detail_in = as_detail_info
ls_Detail_Out = SPACE(40000)
ls_Detail_Out = FillA(CharA(0), 40000)
ls_header_in = as_header_info
ls_header_out = SPACE(100)
ls_header_out = FillA(CharA(0), 100)
as_dates_info = SPACE(100)
as_dates_info = FillA(CharA(0), 100)

as_Header_Info = Trim(as_Header_Info)
SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( ls_MicroHelp)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_Rtn =  orpo85br_mass_weekly_res(&
   	ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message, &
    	ls_header_in, &
		ls_detail_in, &
		ls_header_out, &
		ls_detail_out, &
		as_dates_info, &
	ii_ORP204_CommHandle) 

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

THIS.nf_Display_Message(li_Rtn, astr_Error_Info, ii_ORP204_CommHandle)
as_detail_info = ls_detail_out
as_header_info = ls_header_out
Return li_Rtn
end function

public function integer nf_orpo87br_exclude_plants (ref s_error astr_error_info, character ac_process_option, string as_order_id, string as_customer, character ac_customer_type, string as_input, ref string as_output);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_rtn
String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output

	
SetPointer(HourGlass!)

as_output = ''

astr_error_info.se_procedure_name = "uf_orpo87br_exclude_plants"
astr_error_info.se_message = Space(71)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = FillA(CharA(0),20000)

Do
	li_rtn = orpo87br_exclude_plants(ls_app_name, &
											ls_window_name,&
											ls_function_name,&
											ls_event_name, &
											ls_procedure_name, &
											ls_user_id, &
											ls_return_code, &
											ls_message, &
											ac_process_option, &
											as_order_id, &
											as_customer, &
											ac_customer_type, &
											as_input, &
											ld_task_number, &
											ld_last_record_number, &
											ld_max_record_number, &
											ls_output, &
											ii_orp204_commhandle)

	as_output += ls_output
Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp204_commhandle)

Return li_rtn
// // //

end function

public function integer nf_orpo88br_sold_by_product (ref s_error astr_error_info, string as_input, ref string as_output);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_rtn
String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output

	
SetPointer(HourGlass!)

as_output = ''

astr_error_info.se_procedure_name = "uf_orpo88br_sold_by_product"
astr_error_info.se_message = Space(71)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(10000)
ls_output = FillA(CharA(0),10000)

Do
	li_rtn =  orpo88br_inq_sold_by_product(ls_app_name, &
											ls_window_name,&
											ls_function_name,&
											ls_event_name, &
											ls_procedure_name, &
											ls_user_id, &
											ls_return_code, &
											ls_message, &
											as_input, &
											ls_output, &
											ld_task_number, &
											ld_last_record_number, &
											ld_max_record_number, &
											ii_orp204_commhandle)

	as_output += ls_output
Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp204_commhandle)

Return li_rtn
// // //

end function

public function integer nf_orpo89br_tla_inquiry (ref s_error astr_error_info, string as_input, ref string as_output);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_rtn
String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output

	
SetPointer(HourGlass!)

as_output = ''

astr_error_info.se_procedure_name = "uf_orpo89br_tla_inquiry"
astr_error_info.se_message = Space(71)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = FillA(CharA(0),20000)

Do
	li_rtn =  orpo89br_tla_inquiry(ls_app_name, &
											ls_window_name,&
											ls_function_name,&
											ls_event_name, &
											ls_procedure_name, &
											ls_user_id, &
											ls_return_code, &
											ls_message, &
											as_input, &
											ls_output, &
											ld_task_number, &
											ld_last_record_number, &
											ld_max_record_number, &
											ii_orp204_commhandle)

	as_output += ls_output
Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp204_commhandle)

Return li_rtn
// // //

end function

public function integer nf_orpo92br_cancelled_product_report (ref s_error astr_error_info, string as_input_string);Int			li_rtn
String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output

	
SetPointer(HourGlass!)

astr_error_info.se_procedure_name = "uf_orpo92br_cancelled_product_rpt"
astr_error_info.se_message = Space(71)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_rtn =  orpo92br_cancelled_prod_rpt(ls_app_name, &
											ls_window_name,&
											ls_function_name,&
											ls_event_name, &
											ls_procedure_name, &
											ls_user_id, &
											ls_return_code, &
											ls_message, &
											as_input_string, &
											ii_orp204_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp204_commhandle)

Return li_rtn
// // //

end function

public function integer nf_orpo93br_load_avail_rpt (ref s_error astr_error_info, string as_input_string);Int			li_rtn
String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output

	
SetPointer(HourGlass!)

astr_error_info.se_procedure_name = "uf_orpo93br_load_avail_rpt"
astr_error_info.se_message = Space(71)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_rtn =  orpo93br_load_avail_rpt(ls_app_name, &
											ls_window_name,&
											ls_function_name,&
											ls_event_name, &
											ls_procedure_name, &
											ls_user_id, &
											ls_return_code, &
											ls_message, &
											as_input_string, &
											ii_orp204_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp204_commhandle)

Return li_rtn
// // //

end function

public function integer nf_orpo94br_late_load_permission (ref s_error astr_error_info, ref character ac_late_permissions);Int			li_rtn
String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output

	
SetPointer(HourGlass!)


astr_error_info.se_procedure_name = "uf_orpo94br_late_load_permissions"
astr_error_info.se_message = Space(71)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_rtn = orpo94br_late_load_permissions(ls_app_name, &
											ls_window_name,&
											ls_function_name,&
											ls_event_name, &
											ls_procedure_name, &
											ls_user_id, &
											ls_return_code, &
											ls_message, &
											ac_late_permissions, &
											ii_orp204_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp204_commhandle)

Return li_rtn
// // //

end function

event constructor;call super::constructor;STRING	ls_MicroHelp

ls_MicroHelp = gw_NetWise_Frame.wf_GetMicroHelp()

gw_NetWise_Frame.SetMicroHelp( "Opening MainFrame Connections")

ii_orp204_commhandle = SQLCA.nf_GetCommHandle("orp204")

If ii_orp204_commhandle = 0 Then
	Message.ReturnValue = -1
End if

gw_NetWise_Frame.SetMicroHelp(ls_MicroHelp)

end event

on u_orp204.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_orp204.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

