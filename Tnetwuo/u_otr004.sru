HA$PBExportHeader$u_otr004.sru
forward
global type u_otr004 from u_netwise_transaction
end type
end forward

global type u_otr004 from u_netwise_transaction
end type
global u_otr004 u_otr004

type prototypes
// PowerBuilder Script File: c:\ibp\otr004\otr004.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Fri Jan 03 18:26:04 2003
// Source Interface File: c:\ibp\otr004\otr004.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: hamh01ar_hook_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int hamh01ar_hook_inq( &
    string req_carrier_code, &
    string req_plant_code, &
    ref string hook_inq_string, &
    ref string refetch_plant_code, &
    ref string refetch_hook_type, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr004.dll" alias for "hamh01ar_hook_inq;Ansi"


//
// Declaration for procedure: hamh02ar_hook_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int hamh02ar_hook_upd( &
    string carrier_code, &
    string plant_code, &
    string hook_type, &
    double adj_out, &
    double adj_in, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr004.dll" alias for "hamh02ar_hook_upd;Ansi"


//
// Declaration for procedure: otrt45ar_cust_name_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt45ar_cust_name_inq( &
    string req_name, &
    ref string cust_name_inq_string, &
    ref string refetch_cust_id, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr004.dll" alias for "otrt45ar_cust_name_inq;Ansi"


//
// Declaration for procedure: otrt46ar_cust_order_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt46ar_cust_order_inq( &
    string req_cust_id, &
    string req_from_ship_date, &
    string req_to_ship_date, &
    ref string cust_order_inq_string, &
    ref string refetch_order_number, &
    ref string refetch_ship_date, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr004.dll" alias for "otrt46ar_cust_order_inq;Ansi"


//
// Declaration for procedure: otrt47ar_mass_conf_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt47ar_mass_conf_inq( &
    string req_start_carrier_code, &
    string req_end_carrier_code, &
    string req_delv_date, &
    string req_division, &
    ref string mass_conf_inq_string, &
    ref string refetch_carrier_code, &
    ref string refetch_load_key, &
    ref string refetch_stop_code, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr004.dll" alias for "otrt47ar_mass_conf_inq;Ansi"


//
// Declaration for procedure: otrt48ar_mass_conf_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt48ar_mass_conf_upd( &
    string load_key, &
    string stop_code, &
    string eta_date, &
    string eta_time, &
    string actual_delv_date, &
    string actual_delv_time, &
    string late_code, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr004.dll" alias for "otrt48ar_mass_conf_upd;Ansi"


//
// Declaration for procedure: otrt49ar_load_detail_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt49ar_load_detail_inq( &
    string req_order_number, &
    string req_bol_number, &
    string req_customer_id, &
    string req_customer_po, &
    ref string load_detail_inq_string, &
    ref string check_call_inq_string, &
    ref string stop_detail_inq_string, &
    ref string refetch_stop_code, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr004.dll" alias for "otrt49ar_load_detail_inq;Ansi"


//
// Declaration for procedure: otrt50ar_stop_detail_refetch_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt50ar_stop_detail_refetch_inq( &
    string req_load_key, &
    ref string stop_detail_refetch_string, &
    ref string refetch_stop_code, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr004.dll" alias for "otrt50ar_stop_detail_refetch_inq;Ansi"


//
// Declaration for procedure: otrt51ar_unconfirmed_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt51ar_unconfirmed_inq( &
    string req_from_carrier, &
    string req_to_carrier, &
    string req_from_delv_date, &
    string req_to_delv_date, &
    ref string unconfirmed_inq_string, &
    ref string refetch_load_key, &
    ref string refetch_stop_code, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr004.dll" alias for "otrt51ar_unconfirmed_inq;Ansi"


//
// Declaration for procedure: otrt52ar_check_call_add_stop_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt52ar_check_call_add_stop_upd( &
    string load_key, &
    string stop_code, &
    string check_call_date, &
    string check_call_time, &
    string check_call_city, &
    string check_call_state, &
    string late_code, &
    string miles_out, &
    string eta_date, &
    string eta_time, &
    string eta_contact, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr004.dll" alias for "otrt52ar_check_call_add_stop_upd;Ansi"


//
// Declaration for procedure: otrt53ar_instructions_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt53ar_instructions_inq( &
    string req_load_key, &
    string req_stop_code, &
    ref string load_info_string, &
    ref string order_info_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr004.dll" alias for "otrt53ar_instructions_inq;Ansi"


//
// Declaration for procedure: otrt54ar_memo_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt54ar_memo_inq( &
    string req_order_number, &
    string req_memo_system, &
    string req_memo_id, &
    ref string memo_info_string, &
    ref string refetch_memo_sequence, &
    ref string refetch_memo_line, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr004.dll" alias for "otrt54ar_memo_inq;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "otr004.dll"
function int WBotr004CkCompleted( int CommHnd ) &
    library "otr004.dll"
//
// ***********************************************************

//
// User Structure:  s_error
//
// global type s_error from structure
//     char        se_app_name[8]                
//     char        se_window_name[8]             
//     char        se_function_name[8]           
//     char        se_event_name[32]             
//     char        se_procedure_name[32]         
//     char        se_user_id[8]                 
//     char        se_return_code[8]             
//     char        se_message[71]                
// end type

end prototypes

type variables
int ii_otr004_commhandle

end variables

forward prototypes
public function integer nf_otrt45ar (string as_req_cust_name, ref string as_customer_string, ref string as_refetch_customer_id, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt46ar (string as_req_cust_id, string as_req_from_ship_date, string as_req_to_ship_date, ref string as_cust_order_inq_string, ref string as_refetch_order_number, ref string as_refetch_ship_date, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt48ar (string as_load_key, string as_stop_code, string as_eta_date, string as_eta_time, string as_actual_delv_date, string as_actual_delv_time, string as_late_code, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt49ar (string as_req_order_number, string as_req_bol_number, string as_req_customer_id, string as_req_customer_po, ref string as_load_detail_inq_string, ref string as_check_call_inq_string, ref string as_stop_detail_inq_string, ref string as_refetch_stop_code, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt50ar (string as_req_load_key, ref string as_stop_detail_refetch_string, ref string as_refetch_stop_code, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt51ar (string as_req_from_carrier, string as_req_to_carrier, string as_req_from_delv_date, string as_req_to_delv_date, ref string as_unconfirmed_inq_string, ref string as_refetch_load_key, ref string as_refetch_stop_code, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt52ar (string as_load_key, string as_stop_code, string as_check_call_date, string as_check_call_time, string as_check_call_city, string as_check_call_state, string as_late_reason_code, string as_miles_out, string as_eta_date, string as_eta_time, string as_eta_contact, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt53ar (string as_load_key, string as_stop_code, ref string as_load_info_string, ref string as_order_info_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt54ar (string as_order_number, string as_memo_system, string as_memo_id, ref string as_memo_info_string, ref string as_refetch_memo_sequence, ref string as_refetch_memo_line, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_hamh01ar (string as_req_carrier_code, string as_req_plant_code, ref string as_hook_inq_string, ref string as_refetch_plant_code, ref string as_refetch_hook_type, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_hamh02ar (string as_carrier_code, string as_plant_code, string as_hook_type, double adj_out, double adj_in, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt47ar (string as_req_start_carrier_code, string as_req_end_carrier_code, string as_req_delv_date, string as_division, ref string as_mass_conf_inq_string, ref string as_refetch_carrier_code, ref string as_refetch_load_key, ref string as_refetch_stop_code, ref s_error astr_error_info, integer ai_commhnd)
end prototypes

public function integer nf_otrt45ar (string as_req_cust_name, ref string as_customer_string, ref string as_refetch_customer_id, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information

//li_rtn = otrt45ar_cust_name_inq(as_req_cust_name, as_customer_string, &
//          as_refetch_customer_id, astr_error_info, ii_otr004_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)

return li_rtn


end function

public function integer nf_otrt46ar (string as_req_cust_id, string as_req_from_ship_date, string as_req_to_ship_date, ref string as_cust_order_inq_string, ref string as_refetch_order_number, ref string as_refetch_ship_date, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information

//li_rtn = otrt46ar_cust_order_inq(as_req_cust_id, as_req_from_ship_date, &
//         as_req_to_ship_date, as_cust_order_inq_string, as_refetch_order_number, &
//         as_refetch_ship_date, astr_error_info, ii_otr004_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)

return li_rtn


end function

public function integer nf_otrt48ar (string as_load_key, string as_stop_code, string as_eta_date, string as_eta_time, string as_actual_delv_date, string as_actual_delv_time, string as_late_code, ref s_error astr_error_info, integer ai_commhnd);int li_rtn

gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information
//li_rtn = otrt48ar_mass_conf_upd(as_load_key, as_stop_code, &
//         as_eta_date, as_eta_time, as_actual_delv_date, as_actual_delv_time, &
//         as_late_code, astr_error_info, ii_otr004_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)

return li_rtn
end function

public function integer nf_otrt49ar (string as_req_order_number, string as_req_bol_number, string as_req_customer_id, string as_req_customer_po, ref string as_load_detail_inq_string, ref string as_check_call_inq_string, ref string as_stop_detail_inq_string, ref string as_refetch_stop_code, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt49ar_load_detail_inq(as_req_order_number, as_req_bol_number, &
//         as_req_customer_id, as_req_customer_po, as_load_detail_inq_string, &
//         as_check_call_inq_string, as_stop_detail_inq_string, as_refetch_stop_code, &
//         astr_error_info, ii_otr004_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)

return li_rtn
end function

public function integer nf_otrt50ar (string as_req_load_key, ref string as_stop_detail_refetch_string, ref string as_refetch_stop_code, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")


//call an external netwise function to get the required information
//li_rtn = otrt50ar_stop_detail_refetch_inq(as_req_load_key, as_stop_detail_refetch_string, &
//         as_refetch_stop_code, astr_error_info, ii_otr004_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)

return li_rtn
end function

public function integer nf_otrt51ar (string as_req_from_carrier, string as_req_to_carrier, string as_req_from_delv_date, string as_req_to_delv_date, ref string as_unconfirmed_inq_string, ref string as_refetch_load_key, ref string as_refetch_stop_code, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information

//li_rtn = otrt51ar_unconfirmed_inq(as_req_from_carrier, as_req_to_carrier, &
//         as_req_from_delv_date, as_req_to_delv_date, as_unconfirmed_inq_string, &
//         as_refetch_load_key, as_refetch_stop_code, &
//         astr_error_info, ii_otr004_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)

return li_rtn


end function

public function integer nf_otrt52ar (string as_load_key, string as_stop_code, string as_check_call_date, string as_check_call_time, string as_check_call_city, string as_check_call_state, string as_late_reason_code, string as_miles_out, string as_eta_date, string as_eta_time, string as_eta_contact, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information
//li_rtn = otrt52ar_check_call_add_stop_upd(as_load_key, as_stop_code, &
//         as_check_call_date, as_check_call_time, as_check_call_city, &
//         as_check_call_state, as_late_reason_code, as_miles_out, &
//         as_eta_date, as_eta_time, as_eta_contact, astr_error_info, &
//         ii_otr004_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)

return li_rtn




end function

public function integer nf_otrt53ar (string as_load_key, string as_stop_code, ref string as_load_info_string, ref string as_order_info_string, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt53ar_instructions_inq(as_load_key, as_stop_code, &
//         as_load_info_string, as_order_info_string, &
//         astr_error_info, ii_otr004_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)

return li_rtn




end function

public function integer nf_otrt54ar (string as_order_number, string as_memo_system, string as_memo_id, ref string as_memo_info_string, ref string as_refetch_memo_sequence, ref string as_refetch_memo_line, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")


//call an external netwise function to get the required information
//li_rtn = otrt54ar_memo_inq(as_order_number, as_memo_system, &
//         as_memo_id, as_memo_info_string, as_refetch_memo_sequence, &
//         as_refetch_memo_line, astr_error_info, ii_otr004_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)

return li_rtn




end function

public function integer nf_hamh01ar (string as_req_carrier_code, string as_req_plant_code, ref string as_hook_inq_string, ref string as_refetch_plant_code, ref string as_refetch_hook_type, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information

//li_rtn =  hamh01ar_hook_inq(as_req_carrier_code, as_req_plant_code, &
//         as_hook_inq_string,  as_refetch_plant_code, as_refetch_hook_type, &
//			astr_error_info, ii_otr004_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)

return li_rtn



end function

public function integer nf_hamh02ar (string as_carrier_code, string as_plant_code, string as_hook_type, double adj_out, double adj_in, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information

//li_rtn =  hamh02ar_hook_upd(as_carrier_code, as_plant_code, as_hook_type, &
//         	adj_out, adj_in, &
//            astr_error_info, ii_otr004_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)

return li_rtn




//function int hamh02ar_hook_upd( &
//    char carrier_code[4], &
//    char plant_code[3], &
//    char hook_code[2], &
//    int due_ibp, &
//    int due_carrier, &
//    ref s_error s_error_info, &
//    int CommHnd &
//) library "otr004.dll"
//return 1

end function

public function integer nf_otrt47ar (string as_req_start_carrier_code, string as_req_end_carrier_code, string as_req_delv_date, string as_division, ref string as_mass_conf_inq_string, ref string as_refetch_carrier_code, ref string as_refetch_load_key, ref string as_refetch_stop_code, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information

//li_rtn = otrt47ar_mass_conf_inq(as_req_start_carrier_code, &
//         as_req_end_carrier_code, as_req_delv_date, as_division,as_mass_conf_inq_string, &
//         as_refetch_carrier_code, as_refetch_load_key, as_refetch_stop_code, &
//         astr_error_info, ii_otr004_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr004_commhandle)

return li_rtn
end function

on constructor;call u_netwise_transaction::constructor;
//get the commhandle to be used for the windows
ii_otr004_commhandle = SQLCA.nf_getCommHandle("otr004")

If ii_otr004_commhandle = 0 Then
	// an error occured
	Message.ReturnValue = -1
End if
end on

on u_otr004.create
call super::create
end on

on u_otr004.destroy
call super::destroy
end on

