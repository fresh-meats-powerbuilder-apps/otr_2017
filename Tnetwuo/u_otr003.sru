HA$PBExportHeader$u_otr003.sru
forward
global type u_otr003 from u_netwise_transaction
end type
end forward

global type u_otr003 from u_netwise_transaction
end type
global u_otr003 u_otr003

type prototypes
// PowerBuilder Script File: c:\ibp\otr003\otr003.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Tue Nov 02 13:23:20 2004
// Source Interface File: c:\ibp\otr003\otr003.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: cfmc29br_recv_window_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int cfmc29br_recv_window_inq( &
    string req_customer_id, &
    string req_division, &
    ref string recv_window_inq_string, &
    ref string header_string, &
    ref string refetch_division, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "cfmc29br_recv_window_inq;Ansi"


//
// Declaration for procedure: cfmc30br_recv_window_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int cfmc30br_recv_window_upd( &
    string customer_id, &
    string division_code, &
    string recv_phone, &
    string cust_contact, &
    string mon_from_hrs, &
    string mon_to_hrs, &
    string tues_from_hrs, &
    string tues_to_hrs, &
    string wed_from_hrs, &
    string wed_to_hrs, &
    string thur_from_hrs, &
    string thur_to_hrs, &
    string fri_from_hrs, &
    string fri_to_hrs, &
    string sat_from_hrs, &
    string sat_to_hrs, &
    string sun_from_hrs, &
    string sun_to_hrs, &
    string mon_from_hrs2, &
    string mon_to_hrs2, &
    string tues_from_hrs2, &
    string tues_to_hrs2, &
    string wed_from_hrs2, &
    string wed_to_hrs2, &
    string thur_from_hrs2, &
    string thur_to_hrs2, &
    string fri_from_hrs2, &
    string fri_to_hrs2, &
    string sat_from_hrs2, &
    string sat_to_hrs2, &
    string sun_from_hrs2, &
    string sun_to_hrs2, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "cfmc30br_recv_window_upd;Ansi"


//
// Declaration for procedure: otrt11ar_load_det_hdr_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt11ar_load_det_hdr_inq( &
    string req_load_key, &
    string req_bol_number, &
    string req_customer_id, &
    string req_customer_po, &
    ref string load_detail_inq_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt11ar_load_det_hdr_inq;Ansi"


//
// Declaration for procedure: otrt12ar_load_det_stop_info_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt12ar_load_det_stop_info_inq( &
    string req_load_key, &
    ref string stop_detail_inq_string, &
    ref string refetch_stop_code, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt12ar_load_det_stop_info_inq;Ansi"


//
// Declaration for procedure: otrt13ar_load_det_check_call_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt13ar_load_det_check_call_inq( &
    string req_load_key, &
    ref string check_call_inq_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt13ar_load_det_check_call_inq;Ansi"


//
// Declaration for procedure: otrt14ar_check_call_history_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt14ar_check_call_history_inq( &
    string req_load_key, &
    ref string check_call_inq_string, &
    ref string refetch_stop_code, &
    ref string refetch_check_call_date, &
    ref string refetch_check_call_time, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt14ar_check_call_history_inq;Ansi"


//
// Declaration for procedure: otrt15ar_load_det_ship_info_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt15ar_load_det_ship_info_inq( &
    string req_load_key, &
    ref string ship_info_inq_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt15ar_load_det_ship_info_inq;Ansi"


//
// Declaration for procedure: otrt16ar_load_det_check_call_add
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt16ar_load_det_check_call_add( &
    ref string load_key, &
    ref string stop_code, &
    ref string check_call_date, &
    ref string check_call_time, &
    ref string check_call_city, &
    ref string check_call_state, &
    ref string late_reason_code, &
    ref string miles_out, &
    ref string eta_date, &
    ref string eta_time, &
    ref string eta_contact, &
    ref string update_date, &
    ref string update_time, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt16ar_load_det_check_call_add;Ansi"


//
// Declaration for procedure: otrt17ar_stop_detail_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt17ar_stop_detail_upd( &
    string load_key, &
    string stop_code, &
    string late_reason_code, &
    string appt_date, &
    string appt_time, &
    string appt_contact, &
    string actual_delivery_date, &
    string actual_delivery_time, &
    string eta_date, &
    string eta_time, &
    string carrier_notified_ind, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt17ar_stop_detail_upd;Ansi"


//
// Declaration for procedure: otrt20ar_hot_list_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt20ar_hot_list_inq( &
    string req_load_key, &
    string req_from_carrier, &
    string req_to_carrier, &
    string req_review_status, &
    string req_type_code, &
    string req_priority, &
    string req_ship_date_from, &
    string req_ship_date_to, &
    string req_delv_date_from, &
    string req_delv_date_to, &
    string req_complex, &
    string req_division, &
    ref string hot_list_inq_string, &
    ref s_error s_error_info, &
    ref string refetch_load_key, &
    int CommHnd &
) library "otr003.dll" alias for "otrt20ar_hot_list_inq;Ansi"


//
// Declaration for procedure: otrt21ar_hot_list_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt21ar_hot_list_upd( &
    string load_key, &
    string type_code, &
    string priority_level, &
    string review_status, &
    string next_review_date, &
    string next_review_time, &
    string update_control, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt21ar_hot_list_upd;Ansi"


//
// Declaration for procedure: otrt31ar_stop_info_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt31ar_stop_info_inq( &
    string req_load_key, &
    ref string stop_info_inq_string, &
    ref string refetch_stop_code, &
    ref string refetch_comment_date, &
    ref string refetch_comment_time, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt31ar_stop_info_inq;Ansi"


//
// Declaration for procedure: otrt32ar_stop_comment_add
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt32ar_stop_comment_add( &
    string req_load_key, &
    string req_stop_code, &
    string comment, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt32ar_stop_comment_add;Ansi"


//
// Declaration for procedure: otrt34ar_cust_serv_summary_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt34ar_cust_serv_summary_inq( &
    string req_from_carrier, &
    string req_to_carrier, &
    string req_complex, &
    string req_division, &
    ref string cust_serv_summary_string, &
    ref string refetch_carrier_code, &
    ref string refetch_ship_date, &
    ref string refetch_load_key, &
    ref s_error s_error_info, &
    string plant, &
    int CommHnd &
) library "otr003.dll" alias for "otrt34ar_cust_serv_summary_inq;Ansi"


//
// Declaration for procedure: otrt35ar_cust_recv_type_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt35ar_cust_recv_type_upd( &
    string req_customer_id, &
    ref string recv_type, &
    ref string frt_forwarder, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt35ar_cust_recv_type_upd;Ansi"


//
// Declaration for procedure: otrt36ar_cust_serv_msg_del
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt36ar_cust_serv_msg_del( &
    string load_key, &
    string stop_code, &
    string update_date, &
    string update_time, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt36ar_cust_serv_msg_del;Ansi"


//
// Declaration for procedure: otrt38ar_text_maint_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt38ar_text_maint_inq( &
    string req_record_type, &
    ref string text_maint_string, &
    ref string refetch_record_type, &
    ref string refetch_record_type_code, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt38ar_text_maint_inq;Ansi"


//
// Declaration for procedure: otrt39ar_text_maint_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt39ar_text_maint_upd( &
    string record_type, &
    string record_type_code, &
    string type_short_descr, &
    string type_description, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt39ar_text_maint_upd;Ansi"


//
// Declaration for procedure: otrt41ar_cust_serv_msg_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt41ar_cust_serv_msg_inq( &
    string req_load_key, &
    ref string cust_serv_msg_inq_string, &
    ref s_error s_error_info, &
    ref string refetch_stop_code, &
    ref string refetch_update_date, &
    ref string refetch_update_time, &
    int CommHnd &
) library "otr003.dll" alias for "otrt41ar_cust_serv_msg_inq;Ansi"


//
// Declaration for procedure: otrt42ar_cust_serv_msg_add
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt42ar_cust_serv_msg_add( &
    string load_key, &
    string stop_code, &
    string type_code, &
    string msg, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt42ar_cust_serv_msg_add;Ansi"


//
// Declaration for procedure: otrt43ar_recv_window_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt43ar_recv_window_inq( &
    string req_customer_id, &
    string req_division, &
    ref string recv_window_inq_string, &
    ref string header_string, &
    ref string refetch_division, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt43ar_recv_window_inq;Ansi"


//
// Declaration for procedure: otrt44ar_recv_window_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt44ar_recv_window_upd( &
    string customer_id, &
    string division_code, &
    string recv_phone, &
    string cust_contact, &
    string mon_from_hrs, &
    string mon_to_hrs, &
    string tues_from_hrs, &
    string tues_to_hrs, &
    string wed_from_hrs, &
    string wed_to_hrs, &
    string thur_from_hrs, &
    string thur_to_hrs, &
    string fri_from_hrs, &
    string fri_to_hrs, &
    string sat_from_hrs, &
    string sat_to_hrs, &
    string sun_from_hrs, &
    string sun_to_hrs, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr003.dll" alias for "otrt44ar_recv_window_upd;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "otr003.dll"
function int WBotr003CkCompleted( int CommHnd ) &
    library "otr003.dll"
//
// ***********************************************************

//
// User Structure:  s_error
//
// global type s_error from structure
//     char        se_app_name[8]                
//     char        se_window_name[8]             
//     char        se_function_name[8]           
//     char        se_event_name[32]             
//     char        se_procedure_name[32]         
//     char        se_user_id[8]                 
//     char        se_return_code[6]             
//     char        se_message[71]                
// end type

end prototypes

type variables
int ii_otr003_commhandle

end variables

forward prototypes
public function integer nf_otrt11ar (string as_req_load_key, string as_req_bol_number, string as_req_customer_id, string as_req_customer_po, ref string as_load_detail_inq_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt12ar (string as_req_load_key, ref string as_stop_detail_inq_string, ref string as_refetch_stop_code, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt13ar (string as_req_load_key, ref string as_check_call_inq_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt14ar (string as_req_load_key, ref string as_check_call_inq_string, ref string as_refetch_stop_code, ref string as_refetch_check_call_date, ref string as_refetch_check_call_time, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt15ar (string as_req_load_key, ref string as_ship_info_inq_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt16ar (ref string as_load_key, ref string as_stop_code, ref string as_check_call_date, ref string as_check_call_time, ref string as_check_call_city, ref string as_check_call_state, ref string as_late_reason_code, ref string as_miles_out, ref string as_eta_date, ref string as_eta_time, ref string as_eta_contact, string as_update_date, string as_update_time, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt17ar (string as_load_key, string as_stop_code, string as_late_reason_code, string as_appt_date, string as_appt_time, string as_appt_contact, string as_actual_delivery_date, string as_actual_delivery_time, string as_eta_date, string as_eta_time, string as_carrier_notified_ind, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt31ar (string as_req_load_key, ref string as_stop_info_inq_string, ref string as_refetch_stop_code, ref string as_refetch_comment_date, ref string as_refetch_comment_time, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt32ar (string as_req_load_key, string as_req_stop_code, string as_comment, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt36ar (string as_load_key, string as_stop_code, string as_update_date, string as_update_time, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt38ar (string as_req_record_type, ref string as_text_maint_string, ref string as_refetch_record_type, ref string as_refetch_record_type_code, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt39ar (string as_record_type, string as_record_type_code, string as_type_short_descr, string as_type_description, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt41ar (string as_req_load_key, ref string as_cust_serv_msg_inq_string, ref s_error astr_error_info, ref string as_refetch_stop_code, ref string as_refetch_update_date, ref string as_refetch_update_time, integer ai_commhnd)
public function integer nf_otrt42ar (string as_load_key, string as_stop_code, string as_type_code, string as_message, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt44ar (string as_customer_id, string as_division_code, string as_recv_phone, string as_cust_contact, string as_mon_from_hrs, string as_mon_to_hrs, string as_tue_from_hrs, string as_tue_to_hrs, string as_wed_from_hrs, string as_wed_to_hrs, string as_thur_from_hrs, string as_thur_to_hrs, string as_fri_from_hrs, string as_fri_to_hrs, string as_sat_from_hrs, string as_sat_to_hrs, string as_sun_from_hrs, string as_sun_to_hrs, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt21ar (string as_load_key, string as_type_code, string as_priority_level, string as_review_status, string as_next_review_date, string as_next_review_time, string as_update_control, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt35ar (string as_req_customer_id, ref string as_recv_type, ref string as_frt_forwarder, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt43ar (string as_req_customer_id, string as_req_division, ref string as_recv_window_inq_string, ref string as_header_string, ref string as_refetch_division, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_cfmc30ar (string as_customer_id, string as_division_code, string as_recv_phone, string as_cust_contact, string as_mon_from_hrs, string as_mon_to_hrs, string as_tue_from_hrs, string as_tue_to_hrs, string as_wed_from_hrs, string as_wed_to_hrs, string as_thur_from_hrs, string as_thur_to_hrs, string as_fri_from_hrs, string as_fri_to_hrs, string as_sat_from_hrs, string as_sat_to_hrs, string as_sun_from_hrs, string as_sun_to_hrs, string as_mon_from_hrs2, string as_mon_to_hrs2, string as_tue_from_hrs2, string as_tue_to_hrs2, string as_wed_from_hrs2, string as_wed_to_hrs2, string as_thur_from_hrs2, string as_thur_to_hrs2, string as_fri_from_hrs2, string as_fri_to_hrs2, string as_sat_from_hrs2, string as_sat_to_hrs2, string as_sun_from_hrs2, string as_sun_to_hrs2, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_cfmc29br (string as_req_customer_id, string as_req_division, ref string as_recv_window_inq_string, ref string as_header_string, ref string as_refetch_division, ref s_error astr_error_info, integer ai_command)
public function integer nf_cfmc30br (string as_customer_id, string as_division_code, string as_recv_phone, string as_cust_contact, string as_mon_from_hrs, string as_mon_to_hrs, string as_tue_from_hrs, string as_tue_to_hrs, string as_wed_from_hrs, string as_wed_to_hrs, string as_thur_from_hrs, string as_thur_to_hrs, string as_fri_from_hrs, string as_fri_to_hrs, string as_sat_from_hrs, string as_sat_to_hrs, string as_sun_from_hrs, string as_sun_to_hrs, string as_mon_from_hrs2, string as_mon_to_hrs2, string as_tue_from_hrs2, string as_tue_to_hrs2, string as_wed_from_hrs2, string as_wed_to_hrs2, string as_thur_from_hrs2, string as_thur_to_hrs2, string as_fri_from_hrs2, string as_fri_to_hrs2, string as_sat_from_hrs2, string as_sat_to_hrs2, string as_sun_from_hrs2, string as_sun_to_hrs2, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt20ar (string as_req_load_key, string as_req_from_carrier, string as_req_to_carrier, string as_req_review_status, string as_req_type_code, string as_req_priority, string as_req_ship_date_from, string as_req_ship_date_to, string as_req_delv_date_from, string as_req_delv_date_to, string as_req_complex, string as_req_division, ref string as_hot_list_inq_string, ref s_error astr_error_info, ref string as_refetch_load_key, integer ai_commhnd)
public function integer nf_otrt34ar (string as_req_from_carrier, string as_req_to_carrier, string as_req_complex, string as_division, ref string as_cust_serv_summary_string, ref string as_refetch_carrier_code, ref string as_refetch_ship_date, ref string as_refetch_load_key, ref s_error astr_error_info, string as_plant, integer ai_commhnd)
public function integer wbotr003ckcompleted (string as_req_from_carrier, string as_req_to_carrier, string as_req_complex, string as_division, ref string as_cust_serv_summary_string, ref string as_refetch_carrier_code, ref string as_refetch_ship_date, ref string as_refetch_load_key, ref s_error astr_error_info, string as_plant, integer ai_commhnd)
end prototypes

public function integer nf_otrt11ar (string as_req_load_key, string as_req_bol_number, string as_req_customer_id, string as_req_customer_po, ref string as_load_detail_inq_string, ref s_error astr_error_info, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt11ar_load_det_hdr_inq(as_req_load_key, as_req_bol_number, &
//           as_req_customer_id, as_req_customer_po, as_load_detail_inq_string, &
//           astr_error_info, ii_otr003_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt12ar (string as_req_load_key, ref string as_stop_detail_inq_string, ref string as_refetch_stop_code, ref s_error astr_error_info, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt12ar_load_det_stop_info_inq(as_req_load_key, &
//         as_stop_detail_inq_string, as_refetch_stop_code, &
//         astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt13ar (string as_req_load_key, ref string as_check_call_inq_string, ref s_error astr_error_info, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt13ar_load_det_check_call_inq(as_req_load_key, &
//         as_check_call_inq_string, astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt14ar (string as_req_load_key, ref string as_check_call_inq_string, ref string as_refetch_stop_code, ref string as_refetch_check_call_date, ref string as_refetch_check_call_time, ref s_error astr_error_info, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt14ar_check_call_history_inq(as_req_load_key, as_check_call_inq_string, &
//         as_refetch_stop_code, as_refetch_check_call_date, as_refetch_check_call_time, &
//           astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt15ar (string as_req_load_key, ref string as_ship_info_inq_string, ref s_error astr_error_info, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt15ar_load_det_ship_info_inq(as_req_load_key, &
//         as_ship_info_inq_string, astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt16ar (ref string as_load_key, ref string as_stop_code, ref string as_check_call_date, ref string as_check_call_time, ref string as_check_call_city, ref string as_check_call_state, ref string as_late_reason_code, ref string as_miles_out, ref string as_eta_date, ref string as_eta_time, ref string as_eta_contact, string as_update_date, string as_update_time, ref s_error astr_error_info, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information
//li_rtn = otrt16ar_load_det_check_call_add(as_load_key, as_stop_code, &
//         as_check_call_date, as_check_call_time, as_check_call_city, &
//         as_check_call_state, as_late_reason_code, as_miles_out, &
//         as_eta_date, as_eta_time, as_eta_contact, as_update_date, &
//         as_update_time, astr_error_info, ii_otr003_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn



end function

public function integer nf_otrt17ar (string as_load_key, string as_stop_code, string as_late_reason_code, string as_appt_date, string as_appt_time, string as_appt_contact, string as_actual_delivery_date, string as_actual_delivery_time, string as_eta_date, string as_eta_time, string as_carrier_notified_ind, ref s_error astr_error_info, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")
//call an external netwise function to get the required information
//li_rtn = otrt17ar_stop_detail_upd(as_load_key, as_stop_code, &
//         as_late_reason_code, as_appt_date, as_appt_time, as_appt_contact, &
//         as_actual_delivery_date, as_actual_delivery_time, as_eta_date, &
//         as_eta_time, as_carrier_notified_ind, astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt31ar (string as_req_load_key, ref string as_stop_info_inq_string, ref string as_refetch_stop_code, ref string as_refetch_comment_date, ref string as_refetch_comment_time, ref s_error astr_error_info, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt31ar_stop_info_inq(as_req_load_key, as_stop_info_inq_string, &
//         as_refetch_stop_code, as_refetch_comment_date, as_refetch_comment_time, &
//           astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt32ar (string as_req_load_key, string as_req_stop_code, string as_comment, ref s_error astr_error_info, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information
//li_rtn = otrt32ar_stop_comment_add(as_req_load_key, as_req_stop_code, &
//           as_comment, astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt36ar (string as_load_key, string as_stop_code, string as_update_date, string as_update_time, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information
//li_rtn =otrt36ar_cust_serv_msg_del(as_load_key, as_stop_code, &
//         as_update_date, as_update_time, astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn 
end function

public function integer nf_otrt38ar (string as_req_record_type, ref string as_text_maint_string, ref string as_refetch_record_type, ref string as_refetch_record_type_code, ref s_error astr_error_info, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt38ar_text_maint_inq(as_req_record_type, as_text_maint_string, &
//			as_refetch_record_type, as_refetch_record_type_code, &
//         astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt39ar (string as_record_type, string as_record_type_code, string as_type_short_descr, string as_type_description, ref s_error astr_error_info, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information
//li_rtn = otrt39ar_text_maint_upd(as_record_type, as_record_type_code, &
//    		as_type_short_descr, as_type_description, &
//         astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt41ar (string as_req_load_key, ref string as_cust_serv_msg_inq_string, ref s_error astr_error_info, ref string as_refetch_stop_code, ref string as_refetch_update_date, ref string as_refetch_update_time, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt41ar_cust_serv_msg_inq(as_req_load_key, &
//			as_cust_serv_msg_inq_string, astr_error_info, &
//			as_refetch_stop_code, as_refetch_update_date, &
//			as_refetch_update_time, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt42ar (string as_load_key, string as_stop_code, string as_type_code, string as_message, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information
//li_rtn = otrt42ar_cust_serv_msg_add(as_load_key, &
//         as_stop_code, as_type_code, as_message, &
//			astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt44ar (string as_customer_id, string as_division_code, string as_recv_phone, string as_cust_contact, string as_mon_from_hrs, string as_mon_to_hrs, string as_tue_from_hrs, string as_tue_to_hrs, string as_wed_from_hrs, string as_wed_to_hrs, string as_thur_from_hrs, string as_thur_to_hrs, string as_fri_from_hrs, string as_fri_to_hrs, string as_sat_from_hrs, string as_sat_to_hrs, string as_sun_from_hrs, string as_sun_to_hrs, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information
//li_rtn = otrt44ar_recv_window_upd(as_customer_id, as_division_code, &
//         as_recv_phone, as_cust_contact, as_mon_from_hrs, as_mon_to_hrs,as_tue_from_hrs, &
//         as_tue_to_hrs, as_wed_from_hrs, as_wed_to_hrs,as_thur_from_hrs, &  
//         as_thur_to_hrs, as_fri_from_hrs, as_fri_to_hrs,as_sat_from_hrs, &
//         as_sat_to_hrs, as_sun_from_hrs, as_sun_to_hrs, &
//        astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt21ar (string as_load_key, string as_type_code, string as_priority_level, string as_review_status, string as_next_review_date, string as_next_review_time, string as_update_control, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information
//li_rtn = otrt21ar_hot_list_upd(as_load_key, as_type_code, &
//			as_priority_level, as_review_status, as_next_review_date, &
//			as_next_review_time, as_update_control, astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt35ar (string as_req_customer_id, ref string as_recv_type, ref string as_frt_forwarder, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information
//li_rtn = otrt35ar_cust_recv_type_upd(as_req_customer_id, &
//         as_recv_type, as_frt_forwarder, astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn 
end function

public function integer nf_otrt43ar (string as_req_customer_id, string as_req_division, ref string as_recv_window_inq_string, ref string as_header_string, ref string as_refetch_division, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt43ar_recv_window_inq(as_req_customer_id, &
//         as_req_division, as_recv_window_inq_string,as_header_string, &
//			as_refetch_division, astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_cfmc30ar (string as_customer_id, string as_division_code, string as_recv_phone, string as_cust_contact, string as_mon_from_hrs, string as_mon_to_hrs, string as_tue_from_hrs, string as_tue_to_hrs, string as_wed_from_hrs, string as_wed_to_hrs, string as_thur_from_hrs, string as_thur_to_hrs, string as_fri_from_hrs, string as_fri_to_hrs, string as_sat_from_hrs, string as_sat_to_hrs, string as_sun_from_hrs, string as_sun_to_hrs, string as_mon_from_hrs2, string as_mon_to_hrs2, string as_tue_from_hrs2, string as_tue_to_hrs2, string as_wed_from_hrs2, string as_wed_to_hrs2, string as_thur_from_hrs2, string as_thur_to_hrs2, string as_fri_from_hrs2, string as_fri_to_hrs2, string as_sat_from_hrs2, string as_sat_to_hrs2, string as_sun_from_hrs2, string as_sun_to_hrs2, ref s_error astr_error_info, integer ai_commhnd);int li_rtn

return li_rtn
end function

public function integer nf_cfmc29br (string as_req_customer_id, string as_req_division, ref string as_recv_window_inq_string, ref string as_header_string, ref string as_refetch_division, ref s_error astr_error_info, integer ai_command);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = cfmc29br_recv_window_inq(as_req_customer_id, &
//         as_req_division, as_recv_window_inq_string,as_header_string, &
//			as_refetch_division, astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_cfmc30br (string as_customer_id, string as_division_code, string as_recv_phone, string as_cust_contact, string as_mon_from_hrs, string as_mon_to_hrs, string as_tue_from_hrs, string as_tue_to_hrs, string as_wed_from_hrs, string as_wed_to_hrs, string as_thur_from_hrs, string as_thur_to_hrs, string as_fri_from_hrs, string as_fri_to_hrs, string as_sat_from_hrs, string as_sat_to_hrs, string as_sun_from_hrs, string as_sun_to_hrs, string as_mon_from_hrs2, string as_mon_to_hrs2, string as_tue_from_hrs2, string as_tue_to_hrs2, string as_wed_from_hrs2, string as_wed_to_hrs2, string as_thur_from_hrs2, string as_thur_to_hrs2, string as_fri_from_hrs2, string as_fri_to_hrs2, string as_sat_from_hrs2, string as_sat_to_hrs2, string as_sun_from_hrs2, string as_sun_to_hrs2, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information
//li_rtn = cfmc30br_recv_window_upd(as_customer_id, as_division_code, &
//         as_recv_phone, as_cust_contact, as_mon_from_hrs, as_mon_to_hrs,as_tue_from_hrs, &
//         as_tue_to_hrs, as_wed_from_hrs, as_wed_to_hrs,as_thur_from_hrs, &  
//         as_thur_to_hrs, as_fri_from_hrs, as_fri_to_hrs,as_sat_from_hrs, &
//         as_sat_to_hrs, as_sun_from_hrs, as_sun_to_hrs, as_mon_from_hrs2, as_mon_to_hrs2, as_tue_from_hrs2, &
//         as_tue_to_hrs2, as_wed_from_hrs2, as_wed_to_hrs2, as_thur_from_hrs2, &  
//         as_thur_to_hrs2, as_fri_from_hrs2, as_fri_to_hrs2, as_sat_from_hrs2, &
//         as_sat_to_hrs2, as_sun_from_hrs2, as_sun_to_hrs2, &
//        astr_error_info, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt20ar (string as_req_load_key, string as_req_from_carrier, string as_req_to_carrier, string as_req_review_status, string as_req_type_code, string as_req_priority, string as_req_ship_date_from, string as_req_ship_date_to, string as_req_delv_date_from, string as_req_delv_date_to, string as_req_complex, string as_req_division, ref string as_hot_list_inq_string, ref s_error astr_error_info, ref string as_refetch_load_key, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt20ar_hot_list_inq(as_req_load_key, as_req_from_carrier, &
//         as_req_to_carrier, as_req_review_status, as_req_type_code, &
//			as_req_priority, as_req_ship_date_from, as_req_ship_date_to, &
//			as_req_delv_date_from, as_req_delv_date_to, as_req_complex, &
//			as_req_division, as_hot_list_inq_string, astr_error_info, &
//			as_refetch_load_key, ii_otr003_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer nf_otrt34ar (string as_req_from_carrier, string as_req_to_carrier, string as_req_complex, string as_division, ref string as_cust_serv_summary_string, ref string as_refetch_carrier_code, ref string as_refetch_ship_date, ref string as_refetch_load_key, ref s_error astr_error_info, string as_plant, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt34ar_cust_serv_summary_inq(as_req_from_carrier, as_req_to_carrier, &
//         as_req_complex, as_division, as_cust_serv_summary_string,  as_refetch_carrier_code, &
//         as_refetch_ship_date, as_refetch_load_key, astr_error_info, &
//         as_plant, ii_otr003_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn
end function

public function integer wbotr003ckcompleted (string as_req_from_carrier, string as_req_to_carrier, string as_req_complex, string as_division, ref string as_cust_serv_summary_string, ref string as_refetch_carrier_code, ref string as_refetch_ship_date, ref string as_refetch_load_key, ref s_error astr_error_info, string as_plant, integer ai_commhnd);int li_rtn

gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt34ar_cust_serv_summary_inq(as_req_from_carrier, as_req_to_carrier, &
//         as_req_complex, as_division, as_cust_serv_summary_string,  as_refetch_carrier_code, &
//         as_refetch_ship_date, as_refetch_load_key, astr_error_info, &
//         as_plant, ii_otr003_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rtn

end function

on constructor;call u_netwise_transaction::constructor;
//get the commhandle to be used for the windows
ii_otr003_commhandle = SQLCA.nf_GetCommHandle("otr003")

If ii_otr003_commhandle = 0 Then
	// an error occured
	Message.ReturnValue = -1
End if
end on

on u_otr003.create
call super::create
end on

on u_otr003.destroy
call super::destroy
end on

