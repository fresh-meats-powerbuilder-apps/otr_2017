HA$PBExportHeader$u_otr002.sru
$PBExportComments$Acts as communicating object with netwise for "otr002"
forward
global type u_otr002 from u_netwise_transaction
end type
end forward

global type u_otr002 from u_netwise_transaction
end type
global u_otr002 u_otr002

type prototypes
// PowerBuilder Script File: c:\ibp\otr002\otr002.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Tue Nov 02 13:19:46 2004
// Source Interface File: c:\ibp\otr002\otr002.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: otrt18ar_appt_update_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt18ar_appt_update_inq( &
    string carrier_code, &
    string from_ship_date, &
    string to_ship_date, &
    string load_key, &
    string date_ind, &
    string req_division, &
    ref string appt_update_string, &
    ref string refetch_load_key, &
    ref string refetch_stop_code, &
    ref string refetch_delv_date, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr002.dll" alias for "otrt18ar_appt_update_inq;Ansi"


//
// Declaration for procedure: otrt19ar_appt_update_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt19ar_appt_update_upd( &
    string load_key, &
    string stop_code, &
    string appt_contact, &
    string appt_date, &
    string appt_time, &
    string eta_date, &
    string eta_time, &
    string override_ind, &
    string notify_carrier_ind, &
    ref int return_error_code, &
    ref string return_error_msg, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr002.dll" alias for "otrt19ar_appt_update_upd;Ansi"


//
// Declaration for procedure: otrt22ar_get_carrier_info
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt22ar_get_carrier_info( &
    string carrier_type, &
    string carrier_status, &
    ref string carrier_info_string, &
    ref string refetch_carrier_code, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr002.dll" alias for "otrt22ar_get_carrier_info;Ansi"


//
// Declaration for procedure: otrt23ar_appt_exception_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt23ar_appt_exception_inq( &
    string customer_from_char, &
    string customer_to_char, &
    string req_division, &
    ref string appt_except_inq_string, &
    ref string refetch_customer_id, &
    ref string refetch_load_key, &
    ref string refetch_stop_code, &
    ref s_error s_error_info, &
    string complex, &
    string plant, &
    string carrier, &
    int CommHnd &
) library "otr002.dll" alias for "otrt23ar_appt_exception_inq;Ansi"


//
// Declaration for procedure: otrt24ar_traffic_regions_ext
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt24ar_traffic_regions_ext( &
    ref string traffic_regions_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr002.dll" alias for "otrt24ar_traffic_regions_ext;Ansi"


//
// Declaration for procedure: otrt25ar_carrier_pref_ext
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt25ar_carrier_pref_ext( &
    ref string carrier_pref_string, &
    ref string refetch_customer_id, &
    ref string refetch_customer_type, &
    ref string refetch_carrier_code, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr002.dll" alias for "otrt25ar_carrier_pref_ext;Ansi"


//
// Declaration for procedure: otrt26ar_stops_detail_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt26ar_stops_detail_inq( &
    string req_complex, &
    string req_control, &
    string req_plant, &
    string req_region, &
    string req_division, &
    string req_transmode, &
    string req_carrier_code, &
    string req_ship_from_date, &
    string req_ship_to_date, &
    ref string stops_detail_string, &
    ref string refetch_queue_id, &
    ref int refetch_queue_item, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr002.dll" alias for "otrt26ar_stops_detail_inq;Ansi"


//
// Declaration for procedure: otrt27ar_load_detail_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt27ar_load_detail_inq( &
    ref string req_load_key, &
    ref string load_detail_hdr_string, &
    ref string load_detail_rpt_string, &
    ref string refetch_queue_id, &
    ref int refetch_queue_item, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr002.dll" alias for "otrt27ar_load_detail_inq;Ansi"


//
// Declaration for procedure: otrt28ar_load_messages_ext
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt28ar_load_messages_ext( &
    string load_key, &
    ref string load_messages_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr002.dll" alias for "otrt28ar_load_messages_ext;Ansi"


//
// Declaration for procedure: otrt29ar_trailer_info_ext
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt29ar_trailer_info_ext( &
    string req_complex, &
    string req_assign_status, &
    string req_carrier_code, &
    ref string trailer_info_string, &
    ref string refetch_carrier_code, &
    ref string refetch_trailer_number, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr002.dll" alias for "otrt29ar_trailer_info_ext;Ansi"


//
// Declaration for procedure: otrt30ar_carrier_assign_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt30ar_carrier_assign_upd( &
    string update_control, &
    string carrier_assign_upd_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr002.dll" alias for "otrt30ar_carrier_assign_upd;Ansi"


//
// Declaration for procedure: otrt33ar_stops_detail_pop
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt33ar_stops_detail_pop( &
    string req_load_key, &
    string req_stop_code, &
    ref string po_response_inq_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr002.dll" alias for "otrt33ar_stops_detail_pop;Ansi"


//
// Declaration for procedure: otrt37ar_tdiscntl_inq_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt37ar_tdiscntl_inq_upd( &
    ref string tdiscntl_inq_upd_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr002.dll" alias for "otrt37ar_tdiscntl_inq_upd;Ansi"


//
// Declaration for procedure: otrt40ar_carr_no_appt_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt40ar_carr_no_appt_inq( &
    string req_carrier_start, &
    string req_carrier_end, &
    string req_delv_from_date, &
    string req_delv_to_date, &
    string req_date_ind, &
    ref string carr_no_appt_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr002.dll" alias for "otrt40ar_carr_no_appt_inq;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "otr002.dll"
function int WBotr002CkCompleted( int CommHnd ) &
    library "otr002.dll"
//
// ***********************************************************

//
// User Structure:  s_error
//
// global type s_error from structure
//     char        se_app_name[8]                
//     char        se_window_name[8]             
//     char        se_function_name[8]           
//     char        se_event_name[32]             
//     char        se_procedure_name[32]         
//     char        se_user_id[8]                 
//     char        se_return_code[6]             
//     char        se_message[71]                
// end type

end prototypes

type variables
int ii_otr002_commhandle

end variables

forward prototypes
public function integer nf_otrt37ar (ref string as_tdiscntl_inq_upd_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt19ar (string as_load_key, string as_stop_code, string as_appt_contact, string as_appt_date, string as_appt_time, string as_eta_date, string as_eta_time, string as_override_ind, string as_notify_carrier_ind, ref integer ai_return_error_code, ref string as_return_error_msg, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt22ar (string as_carrier_type, string as_carrier_status, ref string as_carrier_info_string, ref string as_refetch_carrier_code, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt24ar (ref string as_traffic_region, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt25ar (ref string as_carrier_pref_string, ref string as_refetch_customer_id, ref string as_refetch_customer_type, ref string as_refetch_carrier_code, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt26ar (string as_req_complex, string as_req_control, string as_req_plant, string as_req_region, string as_req_division, string as_req_transmode, string as_reg_carrier_code, string as_req_ship_from_date, string as_req_ship_to_date, ref string as_stops_detail_string, ref string as_refetch_queue_id, ref integer ai_refetch_queue_item, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt28ar (string as_load_key, ref string as_load_message_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt29ar (string as_req_complex, string as_req_assign_status, string as_req_carrier_code, ref string as_trailer_info_string, ref string as_refetch_carrier_code, ref string as_refetch_trailer_number, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt30ar (string as_update_control, string as_carrier_assign_upd_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt33ar (string as_req_load_key, string as_req_stop_code, ref string as_appt_except_inq_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt40ar (string as_req_carrier_start, string as_req_carrier_end, string as_req_delv_from_date, string as_req_delv_to_date, string as_req_date_ind, ref string as_carr_no_appt_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt27ar (ref string as_load_key, ref string as_load_detail_hdr_string, ref string as_load_detail_rpt_string, ref string as_refetch_queue_id, ref integer ai_refetch_queue_item, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt18ar (string as_carrier_code, string as_from_ship_date, string as_to_ship_date, string as_load_key, string as_date_ind, string as_division, ref string as_appt_update_string, ref string as_refetch_load_key, ref string as_refetch_stop_code, ref string as_refetch_delv_date, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt23ar (string as_customer_from_char, string as_customer_to_char, string as_division, ref string as_appt_except_inq_string, ref string as_refetch_customer_id, ref string as_refetch_load_key, ref string as_refetch_stop_code, ref s_error astr_error_info, string as_complex, string as_plant, string as_carrier, integer ai_commhnd)
public function integer wbotr002ckcompleted (string as_customer_from_char, string as_customer_to_char, string as_division, ref string as_appt_except_inq_string, ref string as_refetch_customer_id, ref string as_refetch_load_key, ref string as_refetch_stop_code, ref s_error astr_error_info, string as_complex, string as_plant, string as_carrier, integer ai_commhnd)
end prototypes

public function integer nf_otrt37ar (ref string as_tdiscntl_inq_upd_string, ref s_error astr_error_info, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information
//li_rtn = otrt37ar_tdiscntl_inq_upd(as_tdiscntl_inq_upd_string, &
//         astr_error_info, ii_otr002_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)

return li_rtn
end function

public function integer nf_otrt19ar (string as_load_key, string as_stop_code, string as_appt_contact, string as_appt_date, string as_appt_time, string as_eta_date, string as_eta_time, string as_override_ind, string as_notify_carrier_ind, ref integer ai_return_error_code, ref string as_return_error_msg, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

// call an external netwise function to get the required information
//li_rtn = otrt19ar_appt_update_upd(as_load_key, as_stop_code, &
//	 		as_appt_contact, as_appt_date, as_appt_time, as_eta_date, &
//			as_eta_time, as_override_ind, as_notify_carrier_ind, &
//         ai_return_error_code, as_return_error_msg, astr_error_info, ii_otr002_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)
 
return li_rtn
end function

public function integer nf_otrt22ar (string as_carrier_type, string as_carrier_status, ref string as_carrier_info_string, ref string as_refetch_carrier_code, ref s_error astr_error_info, integer ai_commhnd);int li_rtn



gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt22ar_get_carrier_info(as_carrier_type, as_carrier_status, &
//          as_carrier_info_string, as_refetch_carrier_code, astr_error_info, &
//	 		 ii_otr002_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)

return li_rtn
end function

public function integer nf_otrt24ar (ref string as_traffic_region, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

////call an external netwise function to get the required information
//li_rtn = otrt24ar_traffic_regions_ext(as_traffic_region, &
//         astr_error_info, ii_otr002_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)

return li_rtn    
end function

public function integer nf_otrt25ar (ref string as_carrier_pref_string, ref string as_refetch_customer_id, ref string as_refetch_customer_type, ref string as_refetch_carrier_code, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt25ar_carrier_pref_ext(as_carrier_pref_string, &
//         as_refetch_customer_id, as_refetch_customer_type, &
//         as_refetch_carrier_code, &
//         astr_error_info, ii_otr002_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)

return li_rtn    
end function

public function integer nf_otrt26ar (string as_req_complex, string as_req_control, string as_req_plant, string as_req_region, string as_req_division, string as_req_transmode, string as_reg_carrier_code, string as_req_ship_from_date, string as_req_ship_to_date, ref string as_stops_detail_string, ref string as_refetch_queue_id, ref integer ai_refetch_queue_item, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt26ar_stops_detail_inq(as_req_complex, as_req_control, &
//         as_req_plant, as_req_region, as_req_division, as_req_transmode, &
//         as_reg_carrier_code, as_req_ship_from_date, as_req_ship_to_date, &
//         as_stops_detail_string, as_refetch_queue_id, ai_refetch_queue_item, &
//         astr_error_info, ii_otr002_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)

return li_rtn    
end function

public function integer nf_otrt28ar (string as_load_key, ref string as_load_message_string, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt28ar_load_messages_ext(as_load_key, as_load_message_string, &
//         astr_error_info, ii_otr002_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)

return li_rtn    
end function

public function integer nf_otrt29ar (string as_req_complex, string as_req_assign_status, string as_req_carrier_code, ref string as_trailer_info_string, ref string as_refetch_carrier_code, ref string as_refetch_trailer_number, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt29ar_trailer_info_ext(as_req_complex, as_req_assign_status, &
//         as_req_carrier_code, as_trailer_info_string, &
//         as_refetch_carrier_code, as_refetch_trailer_number, &
//         astr_error_info, ii_otr002_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)

return li_rtn
end function

public function integer nf_otrt30ar (string as_update_control, string as_carrier_assign_upd_string, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

//call an external netwise function to get the required information
//li_rtn = otrt30ar_carrier_assign_upd(as_update_control, &
//			as_carrier_assign_upd_string, &
//         astr_error_info, ii_otr002_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)

return li_rtn
end function

public function integer nf_otrt33ar (string as_req_load_key, string as_req_stop_code, ref string as_appt_except_inq_string, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt33ar_stops_detail_pop(as_req_load_key, &
//			as_req_stop_code,as_appt_except_inq_string, &
//         astr_error_info, ii_otr002_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)

return li_rtn
end function

public function integer nf_otrt40ar (string as_req_carrier_start, string as_req_carrier_end, string as_req_delv_from_date, string as_req_delv_to_date, string as_req_date_ind, ref string as_carr_no_appt_string, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn =  otrt40ar_carr_no_appt_inq(as_req_carrier_start, &    
//          as_req_carrier_end, as_req_delv_from_date, as_req_delv_to_date, &
//          as_req_date_ind, as_carr_no_appt_string, astr_error_info, ii_otr002_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)
 
return li_rtn



end function

public function integer nf_otrt27ar (ref string as_load_key, ref string as_load_detail_hdr_string, ref string as_load_detail_rpt_string, ref string as_refetch_queue_id, ref integer ai_refetch_queue_item, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt27ar_load_detail_inq(as_load_key, &
//         as_load_detail_hdr_string, as_load_detail_rpt_string, &
//			as_refetch_queue_id, ai_refetch_queue_item, &
//         astr_error_info, ii_otr002_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)

return li_rtn    
end function

public function integer nf_otrt18ar (string as_carrier_code, string as_from_ship_date, string as_to_ship_date, string as_load_key, string as_date_ind, string as_division, ref string as_appt_update_string, ref string as_refetch_load_key, ref string as_refetch_stop_code, ref string as_refetch_delv_date, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt18ar_appt_update_inq(as_carrier_code, as_from_ship_date,&
//	 		as_to_ship_date,	as_load_key, as_date_ind, as_division, as_appt_update_string, &
//         as_refetch_load_key, as_refetch_stop_code, as_refetch_delv_date, &
//         astr_error_info, ii_otr002_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)

return li_rtn
end function

public function integer nf_otrt23ar (string as_customer_from_char, string as_customer_to_char, string as_division, ref string as_appt_except_inq_string, ref string as_refetch_customer_id, ref string as_refetch_load_key, ref string as_refetch_stop_code, ref s_error astr_error_info, string as_complex, string as_plant, string as_carrier, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt23ar_appt_exception_inq(as_customer_from_char, as_customer_to_char, &
//	 		as_division, as_appt_except_inq_string, as_refetch_customer_id, as_refetch_load_key, &
//         as_refetch_stop_code, astr_error_info, as_complex, as_plant, as_carrier, ii_otr002_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)

return li_rtn          
end function

public function integer wbotr002ckcompleted (string as_customer_from_char, string as_customer_to_char, string as_division, ref string as_appt_except_inq_string, ref string as_refetch_customer_id, ref string as_refetch_load_key, ref string as_refetch_stop_code, ref s_error astr_error_info, string as_complex, string as_plant, string as_carrier, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

//call an external netwise function to get the required information
//li_rtn = otrt23ar_appt_exception_inq(as_customer_from_char, as_customer_to_char, &
//	 		as_division, as_appt_except_inq_string, as_refetch_customer_id, as_refetch_load_key, &
//         as_refetch_stop_code, astr_error_info, as_complex, as_plant, as_carrier, ii_otr002_commhandle)



//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr002_commhandle)

return li_rtn          
end function

on constructor;call u_netwise_transaction::constructor;
//get the commhandle to be used for the windows
ii_otr002_commhandle = SQLCA.nf_GetCommHandle("otr002")

If ii_otr002_commhandle = 0 Then
	// an error occured
	Message.ReturnValue = -1
End if
end on

on u_otr002.create
call super::create
end on

on u_otr002.destroy
call super::destroy
end on

