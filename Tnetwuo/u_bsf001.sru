HA$PBExportHeader$u_bsf001.sru
$PBExportComments$Communication object for Billing Store & Forward
forward
global type u_bsf001 from u_netwise_transaction
end type
end forward

global type u_bsf001 from u_netwise_transaction
end type
global u_bsf001 u_bsf001

type prototypes
// PowerBuilder Script File: j:\pb\test\src32\bsf001.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Tue Aug 05 10:33:28 1997
// Source Interface File: j:\pb\test\src32\bsf001.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: bsfb01ar_order_download
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int bsfb01ar_order_download( &
    string req_complex, &
    ref string refetch_q_id, &
    ref double refetch_q_item, &
    ref string order_header_string, &
    ref string order_instruction_string, &
    ref string order_detail_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "bsf001.dll" alias for "bsfb01ar_order_download;Ansi"
// ***********************************************************
//
// Declaration for procedure: bsfb02ar_apply_to_billing
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int bsfb02ar_apply_to_billing( &
    ref string billing_string, &
    ref string invoice_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "bsf001.dll" alias for "bsfb02ar_apply_to_billing;Ansi"


// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "bsf001.dll"
function int WBbsf001CkCompleted( int CommHnd ) &
    library "bsf001.dll"
//
// ***********************************************************

//
// User Structure:  s_error
//
// global type s_error from structure
//     char        se_app_name[8]                
//     char        se_window_name[8]             
//     char        se_function_name[8]           
//     char        se_event_name[32]             
//     char        se_procedure_name[32]         
//     char        se_user_id[8]                 
//     char        se_return_code[6]             
//     char        se_message[71]                
// end type

end prototypes

type variables
int ii_bsf001_commhandle


end variables

forward prototypes
public function integer nf_bsfb01ar (string as_req_complex, ref string as_refetch_q_id, ref double ad_refetch_q_item, ref string as_order_header_string, ref string as_order_instruction_string, ref string as_order_detail_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_bsfb02ar (ref string as_billing_string, ref string as_invoice_string, ref s_error astr_error_info, integer ai_commhnd)
end prototypes

public function integer nf_bsfb01ar (string as_req_complex, ref string as_refetch_q_id, ref double ad_refetch_q_item, ref string as_order_header_string, ref string as_order_instruction_string, ref string as_order_detail_string, ref s_error astr_error_info, integer ai_commhnd);Int	li_rtn

li_rtn = bsfb01ar_order_download(as_req_complex, &
								as_refetch_q_id, &
								ad_refetch_q_item, &
								as_order_header_string, &
								as_order_instruction_string, &
								as_order_detail_string, &
								astr_error_info, &
								ii_bsf001_commhandle)

return li_rtn

end function

public function integer nf_bsfb02ar (ref string as_billing_string, ref string as_invoice_string, ref s_error astr_error_info, integer ai_commhnd);Int	li_rtn

li_rtn = bsfb02ar_apply_to_billing(as_billing_string, &
										as_invoice_string, &
										astr_error_info, &
										ii_bsf001_commhandle)

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
 

nf_display_message(li_rtn, astr_error_info, ii_bsf001_commhandle)

return li_rtn
end function

on u_bsf001.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_bsf001.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

event constructor;call super::constructor;ii_bsf001_commhandle = SQLCA.nf_GetCommHandle("bsf001")

If ii_bsf001_commhandle <= 0 Then
	Message.ReturnValue = -1
End if


return 1
end event

