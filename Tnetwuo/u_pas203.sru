HA$PBExportHeader$u_pas203.sru
$PBExportComments$Contains pasp20br -- pasp70br
forward
global type u_pas203 from u_netwise_transaction
end type
end forward

global type u_pas203 from u_netwise_transaction
event ue_revisions ( )
end type
global u_pas203 u_pas203

type prototypes
// PowerBuilder Script File: j:\dev\workbe~1\src32\pas203.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Thu Jul 15 13:46:44 1999
// Source Interface File: j:\dev\workbe~1\src32\pas203.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: pasp20br_inq_invt_var
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp20br_inq_invt_var( &
    ref s_error s_error_info, &
    string input_string, &
    ref char Transfer_done, &
    ref double task_number, &
    ref double max_record_number, &
    ref double last_record_number, &
    ref string inventory_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp20br_inq_invt_var;Ansi"


//
// Declaration for procedure: pasp21br_upd_invt_var
// Network connection style : Non-Persistent
// Procedure attributes: authen, async
//
function int pasp21br_upd_invt_var( &
    ref s_error s_error_info, &
    string update_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp21br_upd_invt_var;Ansi"


//
// Declaration for procedure: pasp22br_inq_invt_vs_sales
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp22br_inq_invt_vs_sales( &
    ref s_error s_error_info, &
    string division_string, &
    ref string last_update_string, &
    ref string plant_string, &
    ref string week_sales_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp22br_inq_invt_vs_sales;Ansi"


//
// Declaration for procedure: pasp23br_initiate_auto_conv
// Network connection style : Non-Persistent
// Procedure attributes: authen, async
//
function int pasp23br_initiate_auto_conv( &
    s_error s_error_info, &
    string input_string, &
    ref string message_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp23br_initiate_auto_conv;Ansi"


//
// Declaration for procedure: pasp24br_inq_carry_over
// Network connection style : Non-Persistent
// Procedure attributes: authen, async
//
function int pasp24br_inq_carry_over( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string output_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp24br_inq_carry_over;Ansi"


//
// Declaration for procedure: pasp25br_inq_wt_dist
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp25br_inq_wt_dist( &
    ref s_error s_error_info, &
    string input_string, &
    ref string wgt_dist_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp25br_inq_wt_dist;Ansi"


//
// Declaration for procedure: pasp26br_upd_wt_dist
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp26br_upd_wt_dist( &
    ref s_error s_error_info, &
    string header_string, &
    string detail_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp26br_upd_wt_dist;Ansi"


//
// Declaration for procedure: pasp27br_inq_yield
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp27br_inq_yield( &
    ref s_error s_error_info, &
    string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string header_output_string, &
    ref string yield_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp27br_inq_yield;Ansi"


//
// Declaration for procedure: pasp28br_upd_yield
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp28br_upd_yield( &
    ref s_error s_error_info, &
    string product_code, &
    string yield_string, &
    ref string last_update_timestamp, &
    int CommHnd &
) library "pas203.dll" alias for "pasp28br_upd_yield;Ansi"


//
// Declaration for procedure: pasp29br_inq_locdiv
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp29br_inq_locdiv( &
    ref s_error s_error_info, &
    char division_code[2], &
    ref string loc_div_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp29br_inq_locdiv;Ansi"


//
// Declaration for procedure: pasp30br_upd_locdiv
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp30br_upd_locdiv( &
    ref s_error s_error_info, &
    char division_code[2], &
    string loc_div_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp30br_upd_locdiv;Ansi"


//
// Declaration for procedure: pasp31br_inq_tpaspdav
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp31br_inq_tpaspdav( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    string plant_string, &
    ref string tpaspdav_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp31br_inq_tpaspdav;Ansi"


//
// Declaration for procedure: pasp32br_upd_paspdav
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp32br_upd_paspdav( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string paspdav_string, &
    string header_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp32br_upd_paspdav;Ansi"


//
// Declaration for procedure: pasp33br_inq_plt_const
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp33br_inq_plt_const( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string plant_const_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp33br_inq_plt_const;Ansi"


//
// Declaration for procedure: pasp34br_upd_plt_const
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp34br_upd_plt_const( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    string plant_const_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp34br_upd_plt_const;Ansi"


//
// Declaration for procedure: pasp35br_inq_prod_var
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp35br_inq_prod_var( &
    ref s_error s_error_info, &
    string input_string, &
    ref string task_string, &
    ref string group_index_string, &
    ref string prod_var_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp35br_inq_prod_var;Ansi"


//
// Declaration for procedure: pasp36br_inq_man_prod
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp36br_inq_man_prod( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string end_of_shift, &
    ref string manual_prod_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp36br_inq_man_prod;Ansi"


//
// Declaration for procedure: pasp37br_upd_man_prod
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp37br_upd_man_prod( &
    ref s_error s_error_info, &
    string header_string, &
    string detail_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp37br_upd_man_prod;Ansi"


//
// Declaration for procedure: pasp38br_async_upd_man_prod
// Network connection style : Non-Persistent
// Procedure attributes: authen, async
//
function int pasp38br_async_upd_man_prod( &
    ref s_error s_error_info, &
    string input_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp38br_async_upd_man_prod;Ansi"


//
// Declaration for procedure: pasp39br_inq_salesinv_pos
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp39br_inq_salesinv_pos( &
    ref s_error s_error_info, &
    string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string last_update_timestamp, &
    ref string sales_invt_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp39br_inq_salesinv_pos;Ansi"


//
// Declaration for procedure: pasp40br_inq_daily_summary
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp40br_inq_daily_summary( &
    ref s_error s_error_info, &
    string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string last_update_timestamp, &
    ref string daily_summary_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp40br_inq_daily_summary;Ansi"


//
// Declaration for procedure: pasp43br_inq_mkt_buf
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp43br_inq_mkt_buf( &
    ref s_error s_error_info, &
    string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string output_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp43br_inq_mkt_buf;Ansi"


//
// Declaration for procedure: pasp44br_upd_mkt_buf
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp44br_upd_mkt_buf( &
    ref s_error s_error_info, &
    string division_code, &
    string update_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp44br_upd_mkt_buf;Ansi"


//
// Declaration for procedure: pasp45br_inq_sku_product
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp45br_inq_sku_product( &
    ref s_error s_error_info, &
    string sku_product_code, &
    ref string output_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp45br_inq_sku_product;Ansi"


//
// Declaration for procedure: pasp46br_inq_unsold_inv_ctrl
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp46br_inq_unsold_inv_ctrl( &
    ref s_error s_error_info, &
    string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string output_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp46br_inq_unsold_inv_ctrl;Ansi"


//
// Declaration for procedure: pasp47br_upd_unsold_inv_ctrl
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp47br_upd_unsold_inv_ctrl( &
    ref s_error s_error_info, &
    string division_code, &
    string update_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp47br_upd_unsold_inv_ctrl;Ansi"


//
// Declaration for procedure: pasp49br_inq_freezerinv_pos
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp49br_inq_freezerinv_pos( &
    ref s_error s_error_info, &
    string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string last_update_timestamp, &
    ref string sales_invt_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp49br_inq_freezerinv_pos;Ansi"


//
// Declaration for procedure: pasp50br_auto_tran_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp50br_auto_tran_inq( &
    ref s_error s_error_info, &
    string header_string_in, &
    ref string detail_string_out, &
    ref double task_number, &
    ref double page_number, &
    int CommHnd &
) library "pas203.dll" alias for "pasp50br_auto_tran_inq;Ansi"


//
// Declaration for procedure: pasp51br_pa_negatives
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp51br_pa_negatives( &
    string header_string_in, &
    ref string detail_string_out, &
    ref s_error s_error_info, &
    ref double task_number, &
    ref double page_number, &
    int CommHnd &
) library "pas203.dll" alias for "pasp51br_pa_negatives;Ansi"


//
// Declaration for procedure: pasp52br_inq_product_translation
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp52br_inq_product_translation( &
    ref string output_string, &
    string input_string, &
    ref s_error s_error_info, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas203.dll" alias for "pasp52br_inq_product_translation;Ansi"


//
// Declaration for procedure: pasp53br_upd_product_translation
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp53br_upd_product_translation( &
    string update_string, &
    string header_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "pas203.dll" alias for "pasp53br_upd_product_translation;Ansi"


//
// Declaration for procedure: pasp54br_inq_prod_buffer
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp54br_inq_prod_buffer( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp54br_inq_prod_buffer;Ansi"


//
// Declaration for procedure: pasp55br_upd_prod_buffer
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp55br_upd_prod_buffer( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_header, &
    string input_detail, &
    int CommHnd &
) library "pas203.dll" alias for "pasp55br_upd_prod_buffer;Ansi"


//
// Declaration for procedure: pasp56br_inq_prorate
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp56br_inq_prorate( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas203.dll" alias for "pasp56br_inq_prorate;Ansi"


//
// Declaration for procedure: pasp57br_upd_prorate
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp57br_upd_prorate( &
    ref s_error s_error_info, &
    string input_header, &
    string input_detail, &
    int CommHnd &
) library "pas203.dll" alias for "pasp57br_upd_prorate;Ansi"


//
// Declaration for procedure: pasp58br_cab_product_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp58br_cab_product_inq( &
    ref s_error s_error_info, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp58br_cab_product_inq;Ansi"


//
// Declaration for procedure: pasp59br_inq_plant_shutoff
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp59br_inq_plant_shutoff( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp59br_inq_plant_shutoff;Ansi"


//
// Declaration for procedure: pasp60br_upd_plant_shutoff
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp60br_upd_plant_shutoff( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp60br_upd_plant_shutoff;Ansi"


//
// Declaration for procedure: pasp61br_inq_characteristics
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp61br_inq_characteristics( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_userid, &
    ref string se_return_code, &
    ref string se_message, &
    ref string char_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp61br_inq_characteristics;Ansi"


//
// Declaration for procedure: pasp62br_inq_grnd_beef_prty
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp62br_inq_grnd_beef_prty( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp62br_inq_grnd_beef_prty;Ansi"


//
// Declaration for procedure: pasp63br_inq_location_parm
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp63br_inq_location_parm( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp63br_inq_location_parm;Ansi"


//
// Declaration for procedure: pasp64br_upd_location_parm
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp64br_upd_location_parm( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp64br_upd_location_parm;Ansi"


//
// Declaration for procedure: pasp65br_inq_sold_pct
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp65br_inq_sold_pct( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double max_record_number, &
    ref double last_record_number, &
    int CommHnd &
) library "pas203.dll" alias for "pasp65br_inq_sold_pct;Ansi"


//
// Declaration for procedure: pasp66br_upd_instance_parms
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp66br_upd_instance_parms( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_input_string, &
    string se_input_parameters, &
    int CommHnd &
) library "pas203.dll" alias for "pasp66br_upd_instance_parms;Ansi"


//
// Declaration for procedure: pasp67br_inq_pas_yields_tree
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp67br_inq_pas_yields_tree( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string product_code, &
    string header_string, &
    ref string output_string, &
    ref double task_number, &
    ref double max_record_number, &
    ref double last_record_number, &
    int CommHnd &
) library "pas203.dll" alias for "pasp67br_inq_pas_yields_tree;Ansi"


//
// Declaration for procedure: pasp68br_upd_pas_yields_tree
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp68br_upd_pas_yields_tree( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_header_string, &
    string se_input_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp68br_upd_pas_yields_tree;Ansi"


//
// Declaration for procedure: pasp69br_inq_yield_tree_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp69br_inq_yield_tree_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string input_dates_string, &
    ref string output_string, &
    ref double task_number, &
    ref double max_record_number, &
    ref double last_record_number, &
    int CommHnd &
) library "pas203.dll" alias for "pasp69br_inq_yield_tree_inq;Ansi"


//
// Declaration for procedure: pasp70br_inq_delayed_prd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp70br_inq_delayed_prd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string_in, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas203.dll" alias for "pasp70br_inq_delayed_prd;Ansi"


//
// Declaration for procedure: pasp94br_upd_start_make_to_order
// Network connection style : Non-Persistent
// Procedure attributes: authen, async
//
function int pasp94br_upd_start_make_to_order( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    string header_string, &
    int CommHnd &
) library "pas203.dll" alias for "pasp94br_upd_start_make_to_order;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "pas203.dll"
function int WBpas203CkCompleted( int CommHnd ) &
    library "pas203.dll"
//
// ***********************************************************

//
// User Structure:  s_error
//
// global type s_error from structure
//     char        se_app_name[8]                
//     char        se_window_name[8]             
//     char        se_function_name[8]           
//     char        se_event_name[32]             
//     char        se_procedure_name[32]         
//     char        se_user_id[8]                 
//     char        se_return_code[6]             
//     char        se_message[71]                
// end type

end prototypes

type variables
Int	ii_pas203_commhandle
end variables

forward prototypes
public function boolean nf_inq_wt_dist (ref s_error astr_error_info, string as_input, ref string as_weight_dist)
public function boolean nf_upd_wt_dist (ref s_error astr_error_info, string as_header, string as_weight_dist)
public function boolean nf_inq_locdiv (ref s_error astr_error_info, character ac_division_code[2], ref string as_locdiv)
public function boolean nf_upd_locdiv (ref s_error astr_error_info, character ac_division[2], ref string as_locdiv)
public function boolean nf_inq_plt_const (ref s_error astr_error_info, string as_input, ref string as_plant_constraint)
public function boolean nf_upd_plt_const (ref s_error astr_error_info, string as_header, string as_plant_constraint)
public function boolean nf_upd_yields (ref s_error astr_error_info, string as_product_code, ref string as_yields, ref string as_last_update_timestamp)
public function boolean nf_async_complete (integer ai_commhandle)
public subroutine nf_release_commhandle (integer ai_commhandle)
public function boolean nf_upd_man_prod (ref s_error astr_error_info, string as_header, string as_detail)
public function boolean nf_upd_man_prod_async (ref s_error astr_error_info, string as_input_string, ref integer ai_commhandle)
public function boolean nf_inq_man_prod (ref s_error astr_error_info, string as_input, ref string as_end_of_shift, ref string as_man_prod)
public function boolean nf_inq_daily_summary (ref s_error astr_error_info, string as_input, ref string as_timestamp, ref string as_daily_summary)
public function boolean nf_inq_invt_vs_sales (ref s_error astr_error_info, string as_division, ref string as_last_update, ref string as_plant, ref string as_week_invent)
public function boolean nf_inq_marketing_buffer (ref s_error astr_error_info, string as_input, ref string as_marketing_data)
public function boolean nf_inq_sku_product (ref s_error astr_error_info, string as_product_code, ref string as_sku_product)
public function boolean nf_upd_marketing_buffer (ref s_error astr_error_info, string as_division_code, string as_update)
public function integer nf_get_async_commhandle ()
public function boolean nf_inq_unsold_inv_ctrl (ref s_error astr_error_info, string as_input, ref double ad_task_number, ref double ad_last_record_number, ref double ad_max_record_number, ref string as_output)
public function boolean nf_upd_uns_inv_ctrl (ref s_error astr_error_info, string as_division_code, string as_update)
public function boolean nf_initiate_auto_conv (ref s_error astr_error_info, string as_input, ref string as_message, integer ai_commhnd, boolean ab_firsttime)
public function boolean nf_upd_invent_var (ref s_error astr_error_info, string as_inventory, integer ai_commhnd, boolean ab_first_time)
public function boolean nf_inq_carry_over (ref s_error astr_error_info, string as_input_string, ref double ad_task_number, ref double ad_last_record, ref double ad_max_record, ref string as_output, integer ai_commhandle)
public function boolean nf_inq_invent_var (ref s_error astr_error_info, string as_input, ref character ac_transmission_complete, ref string as_inventory_string)
public function boolean nf_inq_prod_var (ref s_error astr_error_info, string as_input, ref string as_end_shift, ref datawindow adw_prod_var)
public function boolean nf_inq_freezer_inv_position (ref s_error astr_error_info, string as_input_string, ref string as_timestamp, ref datawindow adw_freezer_inv)
public function boolean nf_inq_auto_tran (ref s_error astr_error_info, ref string as_header_string_in, ref datawindow adw_target)
public function boolean nf_inq_pa_negatives (ref string as_header_in, ref s_error astr_error_info, ref datawindow adw_target)
public function boolean nf_inq_sales_inv_position (ref s_error astr_error_info, string as_input, ref string as_timestamp, ref datastore adw_sales_inv)
public function boolean nf_inq_product_translation (ref s_error astr_error_info, string as_input, ref string as_trans_data)
public function boolean nf_upd_product_translation (ref s_error astr_error_info, ref string as_input, ref string as_trans_data)
public function boolean nf_inq_yield (ref s_error astr_error_info, string as_input, ref string as_header_output, readonly datawindow adw_yield)
public function integer nf_pasp57br_upd_prorate (ref s_error astr_error_info, ref string as_input_header, ref string as_input_detail)
public function integer nf_inq_processing_plant_avail (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp59br_inq_plant_shutoff (ref s_error astr_error_info, ref string as_output_string)
public function integer nf_pasp60br_update_plant_shutoff (ref s_error astr_error_info, string as_input_string)
public function integer nf_pasp61br_inq_characteristics (ref s_error astr_error_info, ref string as_char_string)
public function integer nf_pasp62br_inq_grnd_beef_prty (ref s_error astr_error_info, string as_input, ref string as_output)
public function integer nf_upd_prod_buffer (ref s_error astr_error_info, ref string as_input_header, ref string as_input_detail)
public function integer nf_inq_prod_buffer (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp65br_inq_sold_pct (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp64br_upd_location_parm (ref s_error astr_error_info, string as_input_string)
public function integer nf_pasp63br_inq_location_parm (ref s_error astr_error_info, ref string as_output_string)
public function integer nf_pasp66br_upd_instance_parms (ref s_error astr_error_info, readonly string as_input_string, readonly string as_input_parameters)
public function integer nf_upd_paspdav (ref s_error astr_error_info, string as_paspdav, string as_header)
public function integer nf_inq_paspdav (ref s_error astr_error_info, string as_header, string as_plant, ref string as_paspdav)
public function integer nf_pasp67br_inq_pas_yields_tree (ref s_error astr_error_info, readonly string as_product_code, readonly string as_input_header, ref string as_output_string)
public function integer nf_pasp69br_inq_pas_yield_tree_inq (ref s_error astr_error_info, string as_input_string, ref string as_input_dates_string, ref string as_parents_string)
public function integer nf_pasp68br_upd_pas_yields_tree (s_error astr_error_info, string as_header_string, string as_input_string)
public function integer nf_pasp70br_inq_delay_prd_shp_early (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp56br_inq_prorate (ref s_error astr_error_info, ref string as_input_string, ref string as_header_string, ref string as_output_string)
public function boolean nf_pasp94br_upd_str_mk_to_ord (ref s_error astr_error_info, string as_header_string, string as_input_string, ref integer ai_commhandle)
end prototypes

event ue_revisions;/*****************************************************************
**   REVISION NUMBER: rev#01
**   PROJECT NUMBER:  support
**   DATE:				 july 99            
**   PROGRAMMER:      Rich Muckey
**   PURPOSE:         Changed pasp36br to use strings instead of
**                    structures.
******************************************************************
**   REVISION NUMBER: Rev#02
**   PROJECT NUMBER:  Support
**   DATE:            july 1999
**   PROGRAMMER:      David Deal
**   PURPOSE:         Fix Reaccuring problem with Pa View (nf_inq_carry_over)
**   I believe this is a issue with NetWise Or The Hubs
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************/
end event

public function boolean nf_inq_wt_dist (ref s_error astr_error_info, string as_input, ref string as_weight_dist);Int	li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								
as_weight_dist = Space(6301)
astr_error_info.se_procedure_name = "pasp25br_inq_wt_dist"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp25br_inq_wt_dist(astr_error_info, &
									as_input, &
									as_weight_dist, &
									ii_pas203_commhandle)
lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

end function

public function boolean nf_upd_wt_dist (ref s_error astr_error_info, string as_header, string as_weight_dist);Int	li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp26br_upd_wt_dist"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp26br_upd_wt_dist(astr_error_info, &
									as_header, &
									as_weight_dist, &
									ii_pas203_commhandle)
lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

end function

public function boolean nf_inq_locdiv (ref s_error astr_error_info, character ac_division_code[2], ref string as_locdiv);Int	li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								
as_locdiv = Space(8601)
astr_error_info.se_procedure_name = "pasp29br_inq_locdiv"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp29br_inq_locdiv(astr_error_info, &
									ac_division_code, &
									as_locdiv, &
									ii_pas203_commhandle)
lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

end function

public function boolean nf_upd_locdiv (ref s_error astr_error_info, character ac_division[2], ref string as_locdiv);Int	li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp30br_upd_locdiv"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp30br_upd_locdiv(astr_error_info, &
									ac_division, &
									as_locdiv, &
									ii_pas203_commhandle)
lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

end function

public function boolean nf_inq_plt_const (ref s_error astr_error_info, string as_input, ref string as_plant_constraint);Int	li_ret

String						ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message

Time							lt_starttime, &
								lt_endtime
								

as_plant_constraint = Space(10001)
astr_error_info.se_procedure_name = "pasp33br_inq_plt_const"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp33br_inq_plt_const(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_input, &
								as_plant_constraint, &
								ii_pas203_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

end function

public function boolean nf_upd_plt_const (ref s_error astr_error_info, string as_header, string as_plant_constraint);Int	li_ret

String						ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message

Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp34br_upd_plt_const"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp34br_upd_plt_const(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header, &
									as_plant_constraint, &
									ii_pas203_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

end function

public function boolean nf_upd_yields (ref s_error astr_error_info, string as_product_code, ref string as_yields, ref string as_last_update_timestamp);Int	li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp28br_upd_yield"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp28br_upd_yield(astr_error_info, &
									as_product_code, &
									as_yields, &
									as_last_update_timestamp, &
									ii_pas203_commhandle)
lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

end function

public function boolean nf_async_complete (integer ai_commhandle);If WBpas203CkCompleted( ai_commhandle ) = 1 Then return True
Return False
end function

public subroutine nf_release_commhandle (integer ai_commhandle);If IsValid( luo_wkb32 ) Then
	luo_wkb32.WBReleaseCommHandle( ai_commhandle ) 
Else
	luo_wkb16.WBReleaseCommHandle( ai_commhandle ) 
End if	
end subroutine

public function boolean nf_upd_man_prod (ref s_error astr_error_info, string as_header, string as_detail);Int	li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp37br_upd_man_prod"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp37br_upd_man_prod(astr_error_info, &
									as_header, &
									as_detail, &
									ii_pas203_commhandle)
lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

end function

public function boolean nf_upd_man_prod_async (ref s_error astr_error_info, string as_input_string, ref integer ai_commhandle);Boolean	lb_first_time, &
			lb_ret

Int		li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

IF ai_commhandle < 1 Then
	ai_commhandle = This.nf_Get_Async_Commhandle()
	lb_first_time = True
End if

astr_error_info.se_procedure_name = "pasp38br_async_upd_man_prod"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp38br_async_upd_man_prod(astr_error_info, &
												as_input_string, &
												ai_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

// no error the first time
If Not lb_first_time Then
	lb_ret = nf_display_message(li_ret, astr_error_info, ai_commhandle)
	If This.ii_messagebox_rtn = 1 Then return True
	return lb_ret
Else
	return true
End if

end function

public function boolean nf_inq_man_prod (ref s_error astr_error_info, string as_input, ref string as_end_of_shift, ref string as_man_prod);Boolean		lb_first_time

Double		ld_task_number, &
				ld_max_record_number, &
				ld_last_record_number

Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_temp, &
				ls_end_of_shift

Time			lt_starttime, &
				lt_endtime
								


as_man_prod = ""

ld_task_number = 0
ld_max_record_number = 0
ld_last_record_number = 0
lb_first_time = True

astr_error_info.se_procedure_name = "pasp36br_inq_man_prod"
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

ls_end_of_shift = Space(15)

Do 
	ls_temp = Space(17701)
	lt_starttime = Now()

// rev#01 changed to use strings.
	li_ret = pasp36br_inq_man_prod(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input, &
					ld_task_number, &
					ld_max_record_number, &
					ld_last_record_number, &
					ls_end_of_shift, &
					ls_temp, &
					ii_pas203_commhandle)
	lt_endtime = Now()
	
	ls_procedure_name = astr_error_info.se_procedure_name
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

	If Not nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle) Then return false
	If lb_first_time Then
		// end of shift is only filled in on the first call
		as_end_of_shift = ls_end_of_shift
		lb_first_time = False
	End if

	as_man_prod += ls_temp
Loop while ld_max_record_number <> ld_last_record_number

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

return true





end function

public function boolean nf_inq_daily_summary (ref s_error astr_error_info, string as_input, ref string as_timestamp, ref string as_daily_summary);Boolean	lb_firsttime

Double	ld_task_number, &
			ld_max_record_number, &
			ld_last_record_number

Int	li_ret

String	ls_temp, &
			ls_timestamp

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

ls_temp = Space(18901)
as_daily_summary = ""
as_timestamp = ""
ls_timestamp = Space(27)

lb_firsttime = True
ld_task_number = 0
ld_max_record_number = 0
ld_last_record_number = 0

astr_error_info.se_procedure_name = "pasp40br_inq_daily_summary"
astr_error_info.se_message = Space(70)

Do 

	lt_starttime = Now()

	li_ret = pasp40br_inq_daily_summary(astr_error_info, &
										as_input, &
										ld_task_number, &
										ld_max_record_number, &
										ld_last_record_number, &
										ls_timestamp, &
										ls_temp, &
										ii_pas203_commhandle)
	lt_endtime = Now()
	
	ls_procedure_name = astr_error_info.se_procedure_name
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

	If Not nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle) Then return false
	If lb_firsttime Then
		as_timestamp = ls_timestamp
		lb_firsttime = False
	End If

	as_daily_summary += ls_temp
Loop while ld_max_record_number <> ld_last_record_number

return true

end function

public function boolean nf_inq_invt_vs_sales (ref s_error astr_error_info, string as_division, ref string as_last_update, ref string as_plant, ref string as_week_invent);Int	li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

as_last_update = Space(27)
as_plant = Space(14256)
as_week_invent = Space(199)

astr_error_info.se_procedure_name = "pasp22br_inq_invt_vs_sales"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp22br_inq_invt_vs_sales(astr_error_info, &
											as_division, &
											as_last_update, &
											as_plant, &
											as_week_invent, &
											ii_pas203_commhandle)
lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

end function

public function boolean nf_inq_marketing_buffer (ref s_error astr_error_info, string as_input, ref string as_marketing_data);Double	ld_task_number, &
			ld_max_record_number, &
			ld_last_record_number

Int		li_ret

String	ls_temp, &
			ls_timestamp

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								
ls_temp = Space(18601)
as_marketing_data = ""

ld_task_number = 0
ld_max_record_number = 0
ld_last_record_number = 0

astr_error_info.se_procedure_name = "pasp43br_inq_mkt_buf"
astr_error_info.se_message = Space(70)

Do 

	lt_starttime = Now()

	li_ret = pasp43br_inq_mkt_buf(astr_error_info, &
										as_input, &
										ld_task_number, &
										ld_max_record_number, &
										ld_last_record_number, &
										ls_temp, &
										ii_pas203_commhandle)
	lt_endtime = Now()
	
	ls_procedure_name = astr_error_info.se_procedure_name
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

	If Not nf_display_message(li_ret, astr_error_info, &
			ii_pas203_commhandle) Then return false

	as_marketing_data += ls_temp

Loop while ld_max_record_number <> ld_last_record_number

return true

end function

public function boolean nf_inq_sku_product (ref s_error astr_error_info, string as_product_code, ref string as_sku_product);Int	li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								
as_sku_product = Space(21)
astr_error_info.se_procedure_name = "pasp45br_inq_sku_product"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp45br_inq_sku_product(astr_error_info, &
									as_product_code, &
									as_sku_product, &
									ii_pas203_commhandle)
lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)


end function

public function boolean nf_upd_marketing_buffer (ref s_error astr_error_info, string as_division_code, string as_update);Int	li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp44br_upd_mkt_buf"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp44br_upd_mkt_buf(astr_error_info, &
									as_division_code, &
									as_update, &
									ii_pas203_commhandle)
lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

end function

public function integer nf_get_async_commhandle ();Int	li_commhandle

If IsValid(luo_wkb32) Then
	If luo_wkb32.WBGetCommHandle( li_commhandle ) <> 0 Then return -1
	If luo_wkb32.WBSetSecurity( SQLCA.Userid, SQLCA.dbpass, &
					"NETWISE", li_commhandle ) <> 0 Then return -1
	If luo_wkb32.WBSetServerAlias( sqlca.nf_FindServerName( "PAS203" ) , li_commhandle ) <> 0 Then return -1
Else
	If luo_wkb16.WBGetCommHandle( li_commhandle ) <> 0 Then return -1
	If luo_wkb16.WBSetSecurity( sqlca.Userid, sqlca.dbpass, &
					"NETWISE", li_commhandle ) <> 0 Then return -1
	If luo_wkb16.WBSetServerAlias( sqlca.nf_FindServerName( "PAS203" ) , li_commhandle ) <> 0 Then return -1
End if
Return li_commhandle

end function

public function boolean nf_inq_unsold_inv_ctrl (ref s_error astr_error_info, string as_input, ref double ad_task_number, ref double ad_last_record_number, ref double ad_max_record_number, ref string as_output);Int		li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								
as_output = Space(19201)

astr_error_info.se_procedure_name = "pasp46br_inq_unsold_inv_ctrl"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp46br_inq_unsold_inv_ctrl(astr_error_info, &
									as_input, &
									ad_task_number, &
									ad_last_record_number, &
									ad_max_record_number, &
									as_output, &
									ii_pas203_commhandle)
lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

end function

public function boolean nf_upd_uns_inv_ctrl (ref s_error astr_error_info, string as_division_code, string as_update);Int	li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp47br_upd_unsold_inv_ctrl"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp47br_upd_unsold_inv_ctrl(astr_error_info, &
									as_division_code, &
									as_update, &
									ii_pas203_commhandle)
lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

end function

public function boolean nf_initiate_auto_conv (ref s_error astr_error_info, string as_input, ref string as_message, integer ai_commhnd, boolean ab_firsttime);Int	li_ret, &
		li_error_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp23br_initiate_auto_conv"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp23br_initiate_auto_conv(astr_error_info, &
										as_input, &
										as_message, &
										ai_CommHnd )

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

// This is to fix some really weird thing that only happens in a 16-bit EXE
If IsNull(li_ret) then
	li_ret = 0
End If

If ab_FirstTime Then return True

li_error_ret = nf_CheckRpcError(li_ret, astr_error_info, ai_CommHnd)

// return true if these are the same (no error)
return li_ret = li_error_ret
end function

public function boolean nf_upd_invent_var (ref s_error astr_error_info, string as_inventory, integer ai_commhnd, boolean ab_first_time);Int	li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								
astr_error_info.se_procedure_name = "pasp21br_upd_invt_var"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp21br_upd_invt_var(astr_error_info, &
										as_inventory, &
										ai_CommHnd )

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

// The first time the async rpc is called, li_ret will not hold 0
If ab_First_Time Then Return True

Return nf_display_message(li_ret, astr_error_info, ai_CommHnd)

end function

public function boolean nf_inq_carry_over (ref s_error astr_error_info, string as_input_string, ref double ad_task_number, ref double ad_last_record, ref double ad_max_record, ref string as_output, integer ai_commhandle);String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

Int		li_ret
	

as_output = Space(21301)

astr_error_info.se_procedure_name = "pasp24br_inq_carry_over"

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

lt_starttime = Now()

li_ret = pasp24br_inq_carry_over(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ad_task_number, &
					ad_last_record, &
					ad_max_record, &
					as_output, &
					ai_commhandle)

lt_endtime = Now()

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//Added check for neg numbers Rev#02 
if li_ret > 1 Or li_ret < -1 Then li_ret = 0
return nf_display_message(li_ret, astr_error_info, ai_commhandle)

end function

public function boolean nf_inq_invent_var (ref s_error astr_error_info, string as_input, ref character ac_transmission_complete, ref string as_inventory_string);Boolean	lb_FirstTime

Char		lc_TransmissionComplete

Double	ld_task_number, &
			ld_max_record_number, &
			ld_last_record_number

Int	li_ret

String	ls_temp

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

ls_temp = Space(18901)
as_inventory_string = ""

ld_task_number = 0
ld_max_record_number = 0
ld_last_record_number = 0

astr_error_info.se_procedure_name = "pasp20br_inq_invt_var"
astr_error_info.se_message = Space(70)

ac_transmission_complete = ' '
lb_FirstTime = True

Do 

	lt_starttime = Now()

	li_ret = pasp20br_inq_invt_var(astr_error_info, &
										as_input, &
										lc_TransmissionComplete, &
										ld_task_number, &
										ld_max_record_number, &
										ld_last_record_number, &
										ls_temp, &
										ii_pas203_commhandle)
	lt_endtime = Now()
	
	ls_procedure_name = astr_error_info.se_procedure_name
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

	If Not nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle) Then return false

	If lb_firsttime Then
		// Transmission Complete is only filled in on the first call
		ac_transmission_complete = lc_TransmissionComplete
		lb_firsttime = False
	End if
	

	as_inventory_string += ls_temp
Loop while ld_max_record_number <> ld_last_record_number

return true

end function

public function boolean nf_inq_prod_var (ref s_error astr_error_info, string as_input, ref string as_end_shift, ref datawindow adw_prod_var);Int	li_ret, &
		li_tab1, &
		li_tab2

String	ls_task, &
			ls_group_index, &
			ls_Prod_Var

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								
u_String_Functions			lu_string


ls_group_index = Space(8)
ls_prod_var = Space(20000)

astr_error_info.se_procedure_name = "pasp35br_inq_prod_var"
astr_error_info.se_message = Space(70)

ls_task = "0~t0~t0~t                      "

Do

	lt_starttime = Now()

	li_ret = pasp35br_inq_prod_var(astr_error_info, &
											as_input, &
											ls_task, &
											ls_group_index, &
											ls_prod_var, &
											ii_pas203_commhandle)

	lt_endtime = Now()
	
	ls_procedure_name = astr_error_info.se_procedure_name
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

	If Not nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle) Then return False

	adw_prod_var.ImportString(ls_Prod_Var)

	li_tab1 = PosA(ls_task, '~t', 1)
	li_tab2 = PosA(ls_task, '~t', li_tab1 + 1)

Loop while Long(LeftA(ls_task,  li_tab1 - 1)) <> &
				Long(MidA(ls_task, li_tab1 + 1, li_tab2 - li_tab1 - 1))

// get string between third and fourth tab
as_end_shift = MidA(ls_task, lu_string.nf_npos(ls_task, '~t', 1, 3) + 1, lu_string.nf_npos(ls_task, '~t', 1, 4) - &
						lu_string.nf_npos(ls_task, '~t', 1, 3) - 1)

return True
end function

public function boolean nf_inq_freezer_inv_position (ref s_error astr_error_info, string as_input_string, ref string as_timestamp, ref datawindow adw_freezer_inv);Boolean	lb_first_time

Double	ld_task_number, &
			ld_max_record_number, &
			ld_last_record_number

Int	li_ret

String	ls_temp, &
			ls_temp_timestamp

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								
ls_temp_timestamp = Space(26)
ls_temp = Space(11251)

ld_task_number = 0
ld_max_record_number = 0
ld_last_record_number = 0

astr_error_info.se_procedure_name = "pasp49br_inq_freezerinv_pos"
astr_error_info.se_message = Space(70)
lb_first_time = True
adw_freezer_inv.Reset()

Do 

	lt_starttime = Now()

	li_ret = pasp49br_inq_freezerinv_pos(astr_error_info, &
										as_input_string, &
										ld_task_number, &
										ld_last_record_number, &
										ld_max_record_number, &
										ls_temp_timestamp, &
										ls_temp, &
										ii_pas203_commhandle)
	lt_endtime = Now()
	
	ls_procedure_name = astr_error_info.se_procedure_name
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

	If Not nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle) Then return false

	If lb_first_time Then
		as_timestamp = ls_temp_timestamp
		lb_first_time = False
	End if

	adw_freezer_inv.ImportString(ls_temp)
Loop while ld_max_record_number <> ld_last_record_number

return true

end function

public function boolean nf_inq_auto_tran (ref s_error astr_error_info, ref string as_header_string_in, ref datawindow adw_target);int li_rtn
String 	ls_header_string_in, &
			ls_detail_string_out
			
Double	ld_task_number, &
			ld_page_number

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime


SetPointer(HourGlass!)
//iw_frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

ls_header_string_in = as_header_string_in

ls_detail_string_out = Space(15000)
ls_detail_string_out = FillA(CharA(0),15000)

astr_error_info.se_procedure_name = "pasp50br_auto_tran_inq"
astr_error_info.se_message = Space(70)
adw_target.Reset()

Do
	// Call a Netwise external function to get the required information
	lt_starttime = Now()

	li_rtn  = pasp50br_auto_tran_inq( astr_error_info, ls_header_string_in, &
				ls_detail_string_out, ld_task_number, ld_page_number,&
				ii_pas203_commhandle)
				
	lt_endtime = Now()
	
	ls_procedure_name = astr_error_info.se_procedure_name
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

	If Not nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle) Then return false

	adw_target.ImportString(ls_detail_string_out)

Loop While ld_task_number <> 0 And ld_Page_Number <> 0

adw_target.SetRedraw( True)
adw_target.ResetUpdate()

return True



end function

public function boolean nf_inq_pa_negatives (ref string as_header_in, ref s_error astr_error_info, ref datawindow adw_target);int li_rtn
String 	ls_header_string_in, &
			ls_detail_string_out
			
Double	ld_task_number, &
			ld_page_number

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

SetPointer(HourGlass!)
//iw_frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

ls_header_string_in = as_header_in

ls_detail_string_out = Space(15000)
ls_detail_string_out = FillA(CharA(0),15000)

astr_error_info.se_procedure_name = "pasp51br_pa_negatives"
astr_error_info.se_message = Space(70)
adw_target.Reset()

Do
	// Call a Netwise external function to get the required information
	lt_starttime = Now()

	li_rtn  = pasp51br_pa_negatives(ls_header_string_in, &
				ls_detail_string_out,  astr_error_info, ld_task_number, ld_page_number,&
				ii_pas203_commhandle)
				
	lt_endtime = Now()
	
	ls_procedure_name = astr_error_info.se_procedure_name
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

	If Not nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle) Then return false

	adw_target.ImportString(ls_detail_string_out)

Loop While ld_task_number <> 0 And ld_Page_Number <> 0

adw_target.SetRedraw( True)
adw_target.ResetUpdate()

return True

end function

public function boolean nf_inq_sales_inv_position (ref s_error astr_error_info, string as_input, ref string as_timestamp, ref datastore adw_sales_inv);Boolean	lb_first_time

Double	ld_task_number, &
			ld_max_record_number, &
			ld_last_record_number

Int	li_ret

String	ls_temp, &
			ls_temp_timestamp

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								
ls_temp_timestamp = Space(26)
ls_temp = Space(19201)

ld_task_number = 0
ld_max_record_number = 0
ld_last_record_number = 0

astr_error_info.se_procedure_name = "pasp39br_inq_salesinv_pos"
astr_error_info.se_message = Space(70)
lb_first_time = True
adw_sales_inv.Reset()

Do 

	lt_starttime = Now()

	li_ret = pasp39br_inq_salesinv_pos(astr_error_info, &
										as_input, &
										ld_task_number, &
										ld_max_record_number, &
										ld_last_record_number, &
										ls_temp_timestamp, &
										ls_temp, &
										ii_pas203_commhandle)
	lt_endtime = Now()
	
	ls_procedure_name = astr_error_info.se_procedure_name
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

	If Not nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle) Then return false

	If lb_first_time Then
		as_timestamp = ls_temp_timestamp
		lb_first_time = False
	End if

	adw_sales_inv.ImportString(ls_temp)
Loop while ld_max_record_number <> ld_last_record_number

return true

end function

public function boolean nf_inq_product_translation (ref s_error astr_error_info, string as_input, ref string as_trans_data);Double	ld_task_number, &
			ld_max_record_number, &
			ld_last_record_number

Int		li_ret

String	ls_temp, &
			ls_timestamp

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								
ls_temp = Space(14801)
as_trans_data = ""

ld_task_number = 0
ld_max_record_number = 0
ld_last_record_number = 0

astr_error_info.se_procedure_name = "pasp43br_inq_mkt_buf"
astr_error_info.se_message = Space(70)

Do 

	lt_starttime = Now()

	li_ret = pasp52br_inq_product_translation(ls_temp, &
										as_input, &
										astr_error_info, &
										ld_task_number, &
										ld_max_record_number, &
										ld_last_record_number, &
										ii_pas203_commhandle)
	lt_endtime = Now()
	
	ls_procedure_name = astr_error_info.se_procedure_name
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

	If Not nf_display_message(li_ret, astr_error_info, &
			ii_pas203_commhandle) Then return false

	as_trans_data += ls_temp

Loop while ld_max_record_number <> ld_last_record_number

return true

end function

public function boolean nf_upd_product_translation (ref s_error astr_error_info, ref string as_input, ref string as_trans_data);Int	li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp53br_upd_prod_trans"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp53br_upd_product_translation(as_trans_data, &
									as_input, &
									astr_error_info, &
									ii_pas203_commhandle)
lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

end function

public function boolean nf_inq_yield (ref s_error astr_error_info, string as_input, ref string as_header_output, readonly datawindow adw_yield);Boolean	lb_first_time

Double	ld_task_number, &
			ld_max_record_number, &
			ld_last_record_number

Int	li_ret

String	ls_temp, &
			ls_header_temp

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								
ls_temp = Space(10901)
ls_header_temp = Space(30)
adw_yield.Reset()
lb_first_time = True

ld_task_number = 0
ld_max_record_number = 0
ld_last_record_number = 0

astr_error_info.se_procedure_name = "pasp27br_inq_yield"
astr_error_info.se_message = Space(70)

Do 

	lt_starttime = Now()

	li_ret = pasp27br_inq_yield(astr_error_info, &
										as_input, &
										ld_task_number, &
										ld_max_record_number, &
										ld_last_record_number, &
										ls_header_temp, &
										ls_temp, &
										ii_pas203_commhandle)
	lt_endtime = Now()
	
	ls_procedure_name = astr_error_info.se_procedure_name
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

	If Not nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle) Then return false

	If lb_first_time Then 
		as_header_output = ls_header_temp
		lb_first_time = False
	End if

	adw_yield.ImportString(ls_temp)
Loop while ld_max_record_number <> ld_last_record_number

return true

end function

public function integer nf_pasp57br_upd_prorate (ref s_error astr_error_info, ref string as_input_header, ref string as_input_detail);Int	li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								
astr_error_info.se_procedure_name = "pasp57br_upd_prorate"
astr_error_info.se_message = Space(70)

lt_starttime = Now()

li_ret = pasp57br_upd_prorate(astr_error_info, &
									as_input_header, &
									as_input_detail, &
									ii_pas203_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

return li_ret


end function

public function integer nf_inq_processing_plant_avail (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Int		li_ret

String						ls_procedure_name

Time							lt_starttime, &
								lt_endtime
								

as_output_string = Space(60000)
astr_error_info.se_procedure_name = "pasp58br_inq_cab_product_inq"
astr_error_info.se_message = Space(70)

SetPointer(HourGlass!)

lt_starttime = Now()

li_ret = pasp58br_cab_product_inq(astr_error_info, &
									as_input_string, &
									as_output_string, &
									ii_pas203_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_display_message(li_ret, astr_error_info, &
		ii_pas203_commhandle) 

Return li_ret
end function

public function integer nf_pasp59br_inq_plant_shutoff (ref s_error astr_error_info, ref string as_output_string);int li_rtn

STRING	ls_instruction_out,&
			ls_MicroHelp, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

Time							lt_starttime, &
								lt_endtime
								

as_output_string = Space(30000)

as_output_string = FillA(CharA(0),30000)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
lt_starttime = Now()

li_rtn = pasp59br_inq_plant_shutoff(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_output_string, &
					ii_pas203_commhandle)
  
lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle)


Return li_rtn

end function

public function integer nf_pasp60br_update_plant_shutoff (ref s_error astr_error_info, string as_input_string);int li_rtn

STRING	ls_instruction_out,&
			ls_MicroHelp, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

Time							lt_starttime, &
								lt_endtime
								

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
lt_starttime = Now()

li_rtn = pasp60br_upd_plant_shutoff(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ii_pas203_commhandle)
  
lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle)


Return li_rtn

end function

public function integer nf_pasp61br_inq_characteristics (ref s_error astr_error_info, ref string as_char_string);int li_rtn

STRING	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

Time							lt_starttime, &
								lt_endtime
								

as_char_string = Space(30000)

as_char_string = FillA(CharA(0),30000)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
lt_starttime = Now()

li_rtn = pasp61br_inq_characteristics(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_char_string, &
					ii_pas203_commhandle)
  
lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle)


Return li_rtn

end function

public function integer nf_pasp62br_inq_grnd_beef_prty (ref s_error astr_error_info, string as_input, ref string as_output);Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message

Time							lt_starttime, &
								lt_endtime
								
as_output = Space(53001)
astr_error_info.se_procedure_name = "pasp62br_inq_grnd_beef_prty"
astr_error_info.se_message = Space(70)

as_output = FillA(CharA(0),53001)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
lt_starttime = Now()

li_rtn = pasp62br_inq_grnd_beef_prty(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input, &
					as_output, &
					ii_pas203_commhandle)
  
lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle)


Return li_rtn

end function

public function integer nf_upd_prod_buffer (ref s_error astr_error_info, ref string as_input_header, ref string as_input_detail);Int	li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message
	
Time							lt_starttime, &
								lt_endtime
								
astr_error_info.se_procedure_name = "pasp55br_upd_prod_buffer"
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
lt_starttime = Now()

li_ret = pasp55br_upd_prod_buffer(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_header, &
					as_input_detail, &
					ii_pas203_commhandle)
  
lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)

return li_ret
end function

public function integer nf_inq_prod_buffer (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Int		li_ret

String	ls_output, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

Time							lt_starttime, &
								lt_endtime
								
			
as_output_string = Space(19001)

astr_error_info.se_procedure_name = "pasp54br_inq_prod_buf"
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
lt_starttime = Now()

li_ret = pasp54br_inq_prod_buffer(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					as_output_string, &
					ii_pas203_commhandle)
  
lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

return li_ret

end function

public function integer nf_pasp65br_inq_sold_pct (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp65br_inq_sold_pct"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(60000)
ls_output = FillA(CharA(0),60000)

Do
	lt_starttime = Now()

	li_rtn = pasp65br_inq_sold_pct(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_output, &
					ld_task_number, &
					ld_max_record_number, &
					ld_last_record_number, &
					ii_pas203_commhandle)
	as_output_string += ls_output
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle)


Return li_rtn

end function

public function integer nf_pasp64br_upd_location_parm (ref s_error astr_error_info, string as_input_string);Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message
				
Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp64br_upd_location_parm"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information

lt_starttime = Now()

li_rtn = pasp64br_upd_location_parm(ls_app_name, &
				ls_window_name,&
				ls_function_name,&
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id, &
				ls_return_code, &
				ls_message, &
				as_input_string, &
				ii_pas203_commhandle)
  
lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle)


Return li_rtn

end function

public function integer nf_pasp63br_inq_location_parm (ref s_error astr_error_info, ref string as_output_string);Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp63br_inq_location_parm"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
as_output_string = Space(50000)
as_output_string = FillA(CharA(0),50000)

lt_starttime = Now()

li_rtn = pasp63br_inq_location_parm(ls_app_name, &
				ls_window_name,&
				ls_function_name,&
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id, &
				ls_return_code, &
				ls_message, &
				as_output_string, &
				ii_pas203_commhandle)
  
lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle)


Return li_rtn

end function

public function integer nf_pasp66br_upd_instance_parms (ref s_error astr_error_info, readonly string as_input_string, readonly string as_input_parameters);Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message
				
Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp66br_upd_instance_parms"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information

lt_starttime = Now()

li_rtn =  pasp66br_upd_instance_parms(ls_app_name, &
				ls_window_name,&
				ls_function_name,&
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id, &
				ls_return_code, &
				ls_message, &
				as_input_string, &
				as_input_parameters, &
				ii_pas203_commhandle)
  
lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle)


Return li_rtn

end function

public function integer nf_upd_paspdav (ref s_error astr_error_info, string as_paspdav, string as_header);Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message
				
Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp32br_upd_paspdav"
astr_error_info.se_message = Space(70)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information

lt_starttime = Now()

li_ret = pasp32br_upd_paspdav(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_paspdav, &
					as_header, &
					ii_pas203_commhandle)
  
lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)


Return li_ret

end function

public function integer nf_inq_paspdav (ref s_error astr_error_info, string as_header, string as_plant, ref string as_paspdav);Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message
				
Time							lt_starttime, &
								lt_endtime
								

as_paspdav = Space(5001)
astr_error_info.se_procedure_name = "pasp31br_inq_tpaspdav"
astr_error_info.se_message = Space(70)


SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information

lt_starttime = Now()

li_ret = pasp31br_inq_tpaspdav(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_header, &
					as_plant, &
					as_paspdav, &
					ii_pas203_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_ret, astr_error_info, ii_pas203_commhandle)


Return li_ret

end function

public function integer nf_pasp67br_inq_pas_yields_tree (ref s_error astr_error_info, readonly string as_product_code, readonly string as_input_header, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp67br_inq_pas_tree"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(60000)
ls_output = FillA(CharA(0),60000)

Do
	lt_starttime = Now()

	li_rtn = pasp67br_inq_pas_yields_tree(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_product_code, &
					as_input_header, &
					ls_output, &
					ld_task_number, &
					ld_max_record_number, &
					ld_last_record_number, &
					ii_pas203_commhandle)
	as_output_string += ls_output
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle)


Return li_rtn

end function

public function integer nf_pasp69br_inq_pas_yield_tree_inq (ref s_error astr_error_info, string as_input_string, ref string as_input_dates_string, ref string as_parents_string);Boolean		lb_first_time

Int	li_rtn

Double		ld_task_number,&
				ld_last_record_number, &
				ld_max_record_number

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output, &
				ls_input_dates_string
				
Time							lt_starttime, &
								lt_endtime
								

astr_error_info.se_procedure_name = "pasp69br_inq_pas_tree_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ls_input_dates_string = Space(30000)
ls_input_dates_string = FillA(CharA(0),30000)

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(30000)
ls_output = FillA(CharA(0),30000)
lb_first_time = True

Do
	lt_starttime = Now()

	li_rtn = pasp69br_inq_yield_tree_inq(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_input_dates_string, &
					ls_output, &
					ld_task_number, &
					ld_max_record_number, &
					ld_last_record_number, &
					ii_pas203_commhandle)
	
	If lb_first_time Then
		as_input_dates_string = ls_input_dates_string
		lb_first_time = false
	End If
	
	as_parents_string += ls_output
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle)


Return li_rtn

end function

public function integer nf_pasp68br_upd_pas_yields_tree (s_error astr_error_info, string as_header_string, string as_input_string);Int			li_rtn

Long			ll_pos

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_inputstring
				
Time							lt_starttime, &
								lt_endtime
u_String_functions		lu_string
								

astr_error_info.se_procedure_name = "pasp68br_upd_pas_yields_tree"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information

Do 
	ll_pos = lu_string.nf_npos(as_input_string, '~n', 1, 500)
	If ll_pos = 0 Then ll_pos = LenA(as_input_string)
	ls_inputstring = LeftA(as_input_string, ll_pos)
	as_input_string = MidA(as_input_string, ll_pos + 1)
	
	lt_starttime = Now()
	
	li_rtn =  pasp68br_upd_pas_yields_tree(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_header_string, &
					ls_inputstring, &
					ii_pas203_commhandle)
	  
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
Loop while LenA(as_input_string) > 0 and li_rtn >= 0

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle)


Return li_rtn

end function

public function integer nf_pasp70br_inq_delay_prd_shp_early (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp70br_inq_delayed_prd"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(30000)
ls_output = FillA(CharA(0),30000)

Do
	lt_starttime = Now()

	li_rtn = pasp70br_inq_delayed_prd(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas203_commhandle)
	as_output_string += ls_output
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle)


Return li_rtn

end function

public function integer nf_pasp56br_inq_prorate (ref s_error astr_error_info, ref string as_input_string, ref string as_header_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_string_ind, &
				ls_output_string, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
u_string_functions	lu_string
							
							
as_header_string = ''
as_output_string = ''

astr_error_info.se_procedure_name = "pasp56br_inq_prorate"
astr_error_info.se_message = Space(70)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output_string = Space(20000)
ls_output_string = FillA(CharA(0),20000)

Do
	lt_starttime = Now()


	li_rtn = pasp56br_inq_prorate(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_output_string, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas203_commhandle)
	
	ls_string_ind = MidA(ls_output_string, 2, 1)
	ls_output_string = MidA(ls_output_string, 3)
	Do While LenA(Trim(ls_output_string)) > 0
		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		
		Choose Case ls_string_ind
			Case 'H'
				as_header_string += ls_output
			Case 'D'
				as_output_string += ls_output
		End Choose
		ls_string_ind = LeftA(ls_output_string, 1)
		ls_output_string = MidA(ls_output_string, 2)
	Loop 
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number And li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas203_commhandle)


Return li_rtn


end function

public function boolean nf_pasp94br_upd_str_mk_to_ord (ref s_error astr_error_info, string as_header_string, string as_input_string, ref integer ai_commhandle);Boolean	lb_first_time, &
			lb_ret

Int		li_ret

String						ls_procedure_name, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_user_id, &
								ls_return_code, &
								ls_message

Time							lt_starttime, &
								lt_endtime
								

IF ai_commhandle < 1 Then
	ai_commhandle = This.nf_Get_Async_Commhandle()
	lb_first_time = True
End if

astr_error_info.se_procedure_name = "pasp94br_upd_str_mk_to_ord"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information

lt_starttime = Now()

li_ret = pasp94br_upd_start_make_to_order(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					as_header_string, &
					ai_commhandle)
  
lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 


ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

// no error the first time
If Not lb_first_time Then
	lb_ret = nf_display_message(li_ret, astr_error_info, ai_commhandle)
	If This.ii_messagebox_rtn = 1 Then return True
	return lb_ret
Else
	return true
End if

end function

event constructor;call super::constructor;// Get the CommHandle to be used for windows
ii_pas203_commhandle = sqlca.nf_getcommhandle("pas203")
If ii_pas203_commhandle < 0 Then
	// an error occured
	Message.ReturnValue = -1
End if
end event

on u_pas203.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_pas203.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

