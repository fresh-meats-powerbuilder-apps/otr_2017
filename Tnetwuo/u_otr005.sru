HA$PBExportHeader$u_otr005.sru
forward
global type u_otr005 from u_netwise_transaction
end type
end forward

global type u_otr005 from u_netwise_transaction
end type
global u_otr005 u_otr005

type prototypes
// PowerBuilder Script File: c:\ibp\otr005\otr005.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Fri Jan 03 11:07:05 2003
// Source Interface File: c:\ibp\otr005\otr005.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: otrt55ar_load_list_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt55ar_load_list_inq( &
    string req_complex, &
    string req_plant, &
    string req_control, &
    string req_region, &
    string req_division, &
    string req_transmode, &
    string req_carrier, &
    string from_ship_date, &
    string to_ship_date, &
    ref string refetch_queue_id, &
    ref int refetch_queue_item, &
    ref string load_list_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr005.dll" alias for "otrt55ar_load_list_inq;Ansi"


//
// Declaration for procedure: otrt56ar_traffic_inst
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt56ar_traffic_inst( &
    string req_load_key, &
    string req_order, &
    ref string refetch_stop_num, &
    ref string refetch_order, &
    ref string order_info_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr005.dll" alias for "otrt56ar_traffic_inst;Ansi"


//
// Declaration for procedure: otrt57ar_ffw_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt57ar_ffw_upd( &
    string req_load_key, &
    string req_order, &
    string ffw_code, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr005.dll" alias for "otrt57ar_ffw_upd;Ansi"


//
// Declaration for procedure: otrt59ar_transit_except_xtract
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt59ar_transit_except_xtract( &
    string req_complex, &
    string req_plant, &
    string req_division, &
    ref string refetch_load_key, &
    ref string refetch_stop_code, &
    ref string refetch_sales_order, &
    ref string transit_exception_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr005.dll" alias for "otrt59ar_transit_except_xtract;Ansi"


//
// Declaration for procedure: otrt60ar_transit_except_update
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt60ar_transit_except_update( &
    string load_key, &
    string stop_code, &
    string sales_order, &
    string apply_ind, &
    string delivery_date, &
    string delivery_time, &
    string delivery_contact, &
    string est_load_date, &
    string est_load_time, &
    string apply_comment, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr005.dll" alias for "otrt60ar_transit_except_update;Ansi"


//
// Declaration for procedure: otrt61ar_pull_all_stops
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt61ar_pull_all_stops( &
    string req_order_number, &
    string req_bol_number, &
    string req_customer_id, &
    string req_customer_po, &
    ref string load_detail_inq_string, &
    ref string check_call_inq_string, &
    ref string stop_detail_inq_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr005.dll" alias for "otrt61ar_pull_all_stops;Ansi"


//
// Declaration for procedure: otrt62ar_report_initiation
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt62ar_report_initiation( &
    ref string rpt_initiation_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr005.dll" alias for "otrt62ar_report_initiation;Ansi"


//
// Declaration for procedure: otrt63ar_unknown_eta_problems
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt63ar_unknown_eta_problems( &
    string req_from_carrier, &
    string req_to_carrier, &
    ref string refetch_problem_type, &
    ref string refetch_carrier, &
    ref string refetch_load_key, &
    ref string refetch_stop_code, &
    ref string problem_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr005.dll" alias for "otrt63ar_unknown_eta_problems;Ansi"


//
// Declaration for procedure: otrt64ar_ffw_open_close_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt64ar_ffw_open_close_inq( &
    string req_ffw_code, &
    string req_inq_date, &
    ref string refetch_inq_date, &
    ref string business_hours_string, &
    ref string closed_dates_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr005.dll" alias for "otrt64ar_ffw_open_close_inq;Ansi"


//
// Declaration for procedure: otrt65ar_ffw_hours_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt65ar_ffw_hours_upd( &
    string req_ffw_code, &
    string ffw_hours_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr005.dll" alias for "otrt65ar_ffw_hours_upd;Ansi"


//
// Declaration for procedure: otrt66ar_ffw_closed_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt66ar_ffw_closed_upd( &
    string req_ffw_code, &
    string req_action_ind, &
    string req_closed_date, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr005.dll" alias for "otrt66ar_ffw_closed_upd;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "otr005.dll"
function int WBotr005CkCompleted( int CommHnd ) &
    library "otr005.dll"
//
// ***********************************************************

//
// User Structure:  s_error
//
// global type s_error from structure
//     char        se_app_name[8]                
//     char        se_window_name[8]             
//     char        se_function_name[8]           
//     char        se_event_name[32]             
//     char        se_procedure_name[32]         
//     char        se_user_id[8]                 
//     char        se_return_code[8]             
//     char        se_message[71]                
// end type

end prototypes

type variables
int ii_otr005_commhandle

end variables

forward prototypes
public function integer nf_otrt55ar (string as_req_complex, string as_req_plant, string as_req_control, string as_req_region, string as_req_division, string as_req_transmode, string as_req_carrier, string as_from_ship_date, string as_to_ship_date, ref string as_refetch_queue_id, ref integer ai_refetch_queue_item, ref string as_load_list_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt56ar (string as_req_load_key, string as_req_order, ref string as_refetch_stop_num, ref string as_refetch_order, ref string as_order_info_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt57ar (string as_req_load_key, string as_req_order, string as_ffw_code, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt59ar (string as_complex_code, string as_plant_code, string as_division_code, ref string as_load_key, ref string as_stop_code, ref string as_sales_order, ref string as_inquiry_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt61ar (string as_req_order_number, string as_req_bol_number, string as_req_customer_id, string as_req_customer_po, ref string as_load_detail_inq_string, ref string as_check_call_inq_string, ref string as_stop_detail_inq_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt60ar (string as_load_key, string as_stop_code, string as_sales_order, string as_apply_ind, string as_delivery_date, string as_delivery_time, string as_delivery_contact, string as_est_load_date, string as_est_load_time, string as_apply_comment, ref s_error astr_error_info, integer ai_comm_hnd)
public function integer nf_otrt62ar (ref string as_rpt_initiation_string, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_otrt63ar (string as_req_from_carrier, string as_req_to_carrier, ref string as_refetch_problem_type, ref string as_refetch_carrier, ref string as_refetch_load_key, ref string as_refetch_stop_code, ref string as_problem_string, ref s_error astr_error_info, integer ai_commhnd)
public function boolean nf_otrt64ar (string as_req_ffw_code, string as_req_inq_date, ref datawindow ad_business_hours_string, ref datawindow ad_closed_dates_string, ref s_error astr_error_info)
public function boolean nf_otrt66ar (string as_ffw_code, string as_action_ind, string as_closed_date, ref s_error astr_error_info)
public function boolean nf_otrt65ar (string as_ffw_code, string as_ffw_hours_string, ref s_error astr_error_info)
end prototypes

public function integer nf_otrt55ar (string as_req_complex, string as_req_plant, string as_req_control, string as_req_region, string as_req_division, string as_req_transmode, string as_req_carrier, string as_from_ship_date, string as_to_ship_date, ref string as_refetch_queue_id, ref integer ai_refetch_queue_item, ref string as_load_list_string, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")


//call an external netwise function to get the required information
//li_rtn = otrt55ar_load_list_inq(as_req_complex, as_req_plant, &
//         as_req_control, as_req_region, as_req_division, &
//         as_req_transmode, as_req_carrier, as_from_ship_date, &
//         as_to_ship_date, as_refetch_queue_id, ai_refetch_queue_item, &
//         as_load_list_string, astr_error_info, ii_otr005_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr005_commhandle)

return li_rtn




end function

public function integer nf_otrt56ar (string as_req_load_key, string as_req_order, ref string as_refetch_stop_num, ref string as_refetch_order, ref string as_order_info_string, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")


//call an external netwise function to get the required information
//li_rtn = otrt56ar_traffic_inst(as_req_load_key, as_req_order, &
//         as_refetch_stop_num, as_refetch_order, &
//         as_order_info_string, astr_error_info, ii_otr005_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr005_commhandle)

return li_rtn




end function

public function integer nf_otrt57ar (string as_req_load_key, string as_req_order, string as_ffw_code, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")


//call an external netwise function to get the required information
//li_rtn = otrt57ar_ffw_upd(as_req_load_key, as_req_order, &
//         as_ffw_code, &
//         astr_error_info, ii_otr005_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr005_commhandle)

return li_rtn




end function

public function integer nf_otrt59ar (string as_complex_code, string as_plant_code, string as_division_code, ref string as_load_key, ref string as_stop_code, ref string as_sales_order, ref string as_inquiry_string, ref s_error astr_error_info, integer ai_commhnd);Integer li_rtn

gw_netwise_frame.SetMicroHelp( "Wait...Inquiring Database" )

//li_rtn = otrt59ar_transit_except_xtract(as_complex_code, as_plant_code, as_division_code, &
//			as_load_key, as_stop_code, as_sales_order, as_inquiry_string, astr_error_info, ii_otr005_commhandle )
					
// Check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr005_commhandle)

return( li_rtn )
end function

public function integer nf_otrt61ar (string as_req_order_number, string as_req_bol_number, string as_req_customer_id, string as_req_customer_po, ref string as_load_detail_inq_string, ref string as_check_call_inq_string, ref string as_stop_detail_inq_string, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")


//call an external netwise function to get the required information
//li_rtn = otrt61ar_pull_all_stops(as_req_order_number, as_req_bol_number, &
//         as_req_customer_id, as_req_customer_po, as_load_detail_inq_string, &
//         as_check_call_inq_string, as_stop_detail_inq_string, &
//         astr_error_info, ii_otr005_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr005_commhandle)

return li_rtn




end function

public function integer nf_otrt60ar (string as_load_key, string as_stop_code, string as_sales_order, string as_apply_ind, string as_delivery_date, string as_delivery_time, string as_delivery_contact, string as_est_load_date, string as_est_load_time, string as_apply_comment, ref s_error astr_error_info, integer ai_comm_hnd);Integer li_rtn

gw_netwise_frame.SetMicroHelp( "Wait...Updating Database" )

//li_rtn = otrt60ar_transit_except_update( as_load_key, as_stop_code, as_sales_order, &
//	as_apply_ind, as_delivery_date, as_delivery_time, as_delivery_contact, as_est_load_date, &
//	as_est_load_time, as_apply_comment, astr_error_info, ii_otr005_commhandle )
					
// Check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr005_commhandle)

Return( li_rtn )
end function

public function integer nf_otrt62ar (ref string as_rpt_initiation_string, ref s_error astr_error_info, integer ai_commhnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")


//call an external netwise function to get the required information
//li_rtn = otrt62ar_report_initiation(as_rpt_initiation_string, &
//         astr_error_info, ii_otr005_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_otr005_commhandle)

return li_rtn





end function

public function integer nf_otrt63ar (string as_req_from_carrier, string as_req_to_carrier, ref string as_refetch_problem_type, ref string as_refetch_carrier, ref string as_refetch_load_key, ref string as_refetch_stop_code, ref string as_problem_string, ref s_error astr_error_info, integer ai_commhnd);int   li_rtn


gw_netwise_frame.SetMicroHelp("Wait.. Inuqiring Database")


//call an external netwise function to get the required information
//li_rtn = otrt63ar_unknown_eta_problems(as_req_from_carrier, as_req_to_carrier, &
//         as_refetch_problem_type, as_refetch_carrier, as_refetch_load_key, as_refetch_stop_code, &
//			as_problem_string, astr_error_info, ii_otr005_commhandle)
			

//check for any RPC errors, so you can display messages
nf_display_message(li_rtn, astr_error_info, ii_otr005_commhandle)

return li_rtn
end function

public function boolean nf_otrt64ar (string as_req_ffw_code, string as_req_inq_date, ref datawindow ad_business_hours_string, ref datawindow ad_closed_dates_string, ref s_error astr_error_info);Int		li_rtn

String	ls_refetch_inq_date, &
			ls_output_hours_string, &
         ls_output_dates_string


ad_business_hours_string.Reset()
ad_closed_dates_string.Reset()


gw_netwise_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

ls_refetch_inq_date = space(10)
ls_output_hours_string = Space(200)
ls_output_dates_string = Space(1000)
astr_error_info.se_message = Space(72)
astr_error_info.se_procedure_name = 'otrt64ar_ffw_open_close'


//call an external netwise function to get the required information
//li_rtn = otrt64ar_ffw_open_close_inq(as_req_ffw_code, &
//											as_req_inq_date, &
//											ls_refetch_inq_date, &
//         								ls_output_hours_string, &
//											ls_output_dates_string, &
//											astr_error_info, & 
//											ii_otr005_commhandle)
			
	If This.nf_display_message(li_rtn, astr_error_info, ii_otr005_commhandle) Then
		ad_business_hours_string.ImportString(ls_output_hours_string)
		ad_closed_dates_string.ImportString(ls_output_dates_string)
	end if
	
// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
END IF


Do While len(Trim(ls_refetch_inq_date)) > 0

	ls_output_dates_string = Space(1000)
	ls_output_hours_string = Space(200)
	astr_error_info.se_message = Space(72)
	astr_error_info.se_procedure_name = 'otrt64ar_ffw_open_close'


//call an external netwise function to get the required information
	li_rtn = otrt64ar_ffw_open_close_inq(as_req_ffw_code, &
												    as_req_inq_date, &
													 ls_refetch_inq_date, &
         										 ls_output_hours_string, &
													 ls_output_dates_string, &
													 astr_error_info, & 
													 ii_otr005_commhandle)
			
	If This.nf_display_message(li_rtn, astr_error_info, ii_otr005_commhandle) Then
		ad_closed_dates_string.ImportString(ls_output_dates_string)
	end if
	
	
Loop


// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
else
	gw_netwise_frame.SetMicroHelp( "Ready")
end if
	

SetPointer(Arrow!)
Return True
end function

public function boolean nf_otrt66ar (string as_ffw_code, string as_action_ind, string as_closed_date, ref s_error astr_error_info);Int		li_rtn

gw_netwise_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

astr_error_info.se_event_name = "wf_update  nf_otrt66ar"
astr_error_info.se_window_name = "w_ffw_open_close"
astr_error_info.se_user_id = SQLCA.Userid
astr_error_info.se_message = Space(72)
astr_error_info.se_procedure_name = 'otrt66ar_ffw_closed_upd'

//call an external netwise function to get the required information
//li_rtn = otrt66ar_ffw_closed_upd(as_ffw_code, &
//										   as_action_ind, &
//										   as_closed_date, &
//											astr_error_info, &
//										   ii_otr005_commhandle)
			
This.nf_display_message(li_rtn, astr_error_info, ii_otr005_commhandle) 

// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
	SetPointer(Arrow!)
	Return False
else
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
//	gw_netwise_frame.SetMicroHelp( "Ready")
end if

SetPointer(Arrow!)
Return True
end function

public function boolean nf_otrt65ar (string as_ffw_code, string as_ffw_hours_string, ref s_error astr_error_info);Int		li_rtn

gw_netwise_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)
 
astr_error_info.se_event_name = "wf_update  nf_otrt65ar"
astr_error_info.se_window_name = "w_ffw_open_close"
astr_error_info.se_user_id = SQLCA.Userid
astr_error_info.se_message = Space(72)
astr_error_info.se_procedure_name = 'otrt65ar_ffw_hours_upd'

//call an external netwise function to get the required information
//li_rtn = otrt65ar_ffw_hours_upd(as_ffw_code, &
//										  as_ffw_hours_string, &
//										  astr_error_info, & 
//										  ii_otr005_commhandle)
			
This.nf_display_message(li_rtn, astr_error_info, ii_otr005_commhandle)

// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
	SetPointer(Arrow!)
	Return False
else
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
//	gw_netwise_frame.SetMicroHelp( "Ready")
end if
	

SetPointer(Arrow!)
Return True
end function

event constructor;call super::constructor;
//get the commhandle to be used for the windows
ii_otr005_commhandle = SQLCA.nf_GetCommHandle("otr005")

If ii_otr005_commhandle = 0 Then
	// an error occured
	Message.ReturnValue = -1
End if


end event

on u_otr005.create
call super::create
end on

on u_otr005.destroy
call super::destroy
end on

