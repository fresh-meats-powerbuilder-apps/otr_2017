HA$PBExportHeader$u_orp002.sru
forward
global type u_orp002 from u_netwise_transaction
end type
end forward

global type u_orp002 from u_netwise_transaction
end type
global u_orp002 u_orp002

type prototypes
// PowerBuilder Script File: j:\pb\test\src32\orp202.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Wed Jul 09 13:57:39 1997
// Source Interface File: j:\pb\test\src32\orp202.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: orpo68br_schedres
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo68br_schedres( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_info_in, &
    string detail_info_in, &
    ref string header_info_out, &
    ref string detail_info_out, &
    ref double task_number, &
    ref double page_number, &
    int CommHnd &
) library "orp202.dll" alias for "orpo68br_schedres;Ansi"


//
// Declaration for procedure: orpo69br_pricing_review_queue
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo69br_pricing_review_queue( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string_in, &
    string detail_string_in, &
    ref string detail_string_out, &
    ref string header_string_out, &
    int CommHnd &
) library "orp202.dll" alias for "orpo69br_pricing_review_queue;Ansi"


//
// Declaration for procedure: orpo70br_inq_mult_short_desc
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo70br_inq_mult_short_desc( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "orp202.dll" alias for "orpo70br_inq_mult_short_desc;Ansi"


//
// Declaration for procedure: orpo71br_sales_order_variance
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo71br_sales_order_variance( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string_in, &
    ref string detail_string_out, &
    int CommHnd &
) library "orp202.dll" alias for "orpo71br_sales_order_variance;Ansi"


//
// Declaration for procedure: orpo72br_inq_so_exch_rate
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo72br_inq_so_exch_rate( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "orp202.dll" alias for "orpo72br_inq_so_exch_rate;Ansi"


//
// Declaration for procedure: orpo73br_inq_monthly_exch_rates
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo73br_inq_monthly_exch_rates( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "orp202.dll" alias for "orpo73br_inq_monthly_exch_rates;Ansi"


//
// Declaration for procedure: orpo75br_validate_products
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo75br_validate_products( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    string detail_string_in, &
    ref string detail_string_out, &
    int CommHnd &
) library "orp202.dll" alias for "orpo75br_validate_products;Ansi"


//
// Declaration for procedure: orpo77br_Validate_Customer
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo77br_Validate_Customer( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    ref string detail_string_out, &
    ref string Default_String_Out, &
    int CommHnd &
) library "orp202.dll" alias for "orpo77br_Validate_Customer;Ansi"


//
// Declaration for procedure: orpo78br_Get_Order_Confirmation
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo78br_Get_Order_Confirmation( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Primary_Order, &
    ref string List_Of_Stops, &
    ref string Header_Info, &
    ref string Order_Detail, &
    ref string instructions, &
    int CommHnd &
) library "orp202.dll" alias for "orpo78br_Get_Order_Confirmation;Ansi"


//
// Declaration for procedure: orpo79br_Multi_Gen
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo79br_Multi_Gen( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string customer_info, &
    string additional_data, &
    string main_info_in, &
    ref string main_info_out, &
    ref string so_info, &
    ref string multi_return_code, &
    ref string bm_start_time, &
    ref string bm_end_time, &
    ref string bm_task_number, &
    int CommHnd &
) library "orp202.dll" alias for "orpo79br_Multi_Gen;Ansi"

//
// Declaration for procedure: orpo80br_print_fax
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo80br_print_fax( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char process_option_ind, &
    string input_info_in, &
    ref string input_info_out, &
    int CommHnd &
) library "orp202.dll" alias for "orpo80br_print_fax;Ansi"

//
// Declaration for procedure: orpo81br
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo81br_pa_summary( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    String process_option, &
    String availability_date, &
    string page_descr_in, &
    string plant_data_in, &
    string detail_data_in, &
    ref string plant_data_out, &
    ref string detail_data_out, &
    ref string file_descr_out, &
    int CommHnd &
) library "orp202.dll" alias for "orpo81br_pa_summary;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "orp202.dll"
function int WBorp202CkCompleted( int CommHnd ) &
    library "orp202.dll"
//
// ***********************************************************


end prototypes

type variables
Private:
int	 ii_orp002_commhandle
u_OleCom		iu_oleprophet
end variables

forward prototypes
public function boolean nf_orpo70ar_inq_sku_prod (ref s_error astr_error_info, string as_input, ref string as_output)
public function boolean nf_orpo69ar_pricing_review_inq (ref s_error astr_error_info, string as_header_in, string as_detail_in, ref string as_detail_out, ref string as_header_out)
public function boolean nf_orpo71ar_sales_order_variance (ref s_error astr_error_info, string as_header_in, ref string as_detail_out)
public function boolean nf_orpo73ar_inq_monthly_exch_rates (ref s_error astr_error_info, string as_input, ref string as_output)
public function boolean nf_orpo72ar_inq_exch_rate (ref s_error astr_error_info, string as_input, ref string as_output)
public function integer nf_orpo68ar_schedres (ref s_error astr_error_info, ref string as_header_in, ref string as_detail_in, ref double ad_tasknumber, ref double ad_pagenumber)
public function integer nf_orpo75ar_validate_products (ref s_error astr_error_info, string as_header_string, ref string as_detail_string)
public function integer nf_orpo77_validate_customers (ref s_error astr_error_info, string as_header_string, ref string as_detail_string, ref string as_default_string)
public function integer nf_orpo78br_get_order_confimation (string as_primary_order, ref datawindowchild ad_stops_dw, ref datawindowchild ad_header_dw, ref datawindowchild ad_detail_dw, ref datawindowchild ad_instruction_dw, ref s_error astr_error_info)
public function integer nf_orpo79br_multi_gen (ref s_error astr_error_info, string as_customer_info, string as_main_info_in, string as_additional_data, ref string as_main_info_out, ref string as_so_info, ref string multi_return_code)
public function integer nf_orp80br_print_fax ()
public function integer nf_orpo80br_print_fax (ref s_error astr_error_info, character ac_process_option_ind, string as_input_info_in, ref string as_input_info_out)
public function boolean nf_write_benchmark2 (time at_starttime, time at_endtime, string as_function_name, time at_start_rpc, time at_end_rpc, string as_other_info, string as_task_number)
end prototypes

public function boolean nf_orpo70ar_inq_sku_prod (ref s_error astr_error_info, string as_input, ref string as_output);Int			li_ret
String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message

as_output = Space(20000)

astr_error_info.se_procedure_name = "orpo70br_inq_mult_short_desc"
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_ret = orpo70br_inq_mult_short_desc(		ls_app_name, &
										ls_window_name, &
										ls_function_name, &
										ls_event_name, &
										ls_procedure_name, &
										ls_user_id,&
										ls_return_code,&
										ls_message, &
										as_input, &
										as_output, &
										ii_orp002_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
If Not nf_display_message(li_ret, astr_error_info, ii_orp002_commhandle) Then return false

return true

end function

public function boolean nf_orpo69ar_pricing_review_inq (ref s_error astr_error_info, string as_header_in, string as_detail_in, ref string as_detail_out, ref string as_header_out);Int		li_ret

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


as_detail_out = Space(17200)
as_header_out = Space(11)

astr_error_info.se_procedure_name = "orpo69ar_pricing_review_queue"
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_ret = orpo69br_pricing_review_queue(ls_app_name, &
										ls_window_name, &
										ls_function_name, &
										ls_event_name, &
										ls_procedure_name, &
										ls_user_id,&
										ls_return_code,&
										ls_message, &
										as_header_in, &
										as_detail_in, &
										as_detail_out, &
										as_header_out, &
										ii_orp002_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
If Not nf_display_message(li_ret, astr_error_info, &
			ii_orp002_commhandle) Then return false

return true



end function

public function boolean nf_orpo71ar_sales_order_variance (ref s_error astr_error_info, string as_header_in, ref string as_detail_out);Int		li_ret
String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


as_detail_out = Space(13700)

astr_error_info.se_procedure_name = "orpo71br_sales_order_variance"
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_ret = orpo71br_sales_order_variance(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_header_in, &
					as_detail_out, &
					ii_orp002_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
If Not nf_display_message(li_ret, astr_error_info, ii_orp002_commhandle) Then return false


return true
end function

public function boolean nf_orpo73ar_inq_monthly_exch_rates (ref s_error astr_error_info, string as_input, ref string as_output);Int		li_ret

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


astr_error_info.se_procedure_name = "orpo73br_inq_monthly_exch_rates"
astr_error_info.se_message = Space(70)

as_output = Space(466)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_ret = orpo73br_inq_monthly_exch_rates(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input, &
					as_output,&
					ii_orp002_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
If Not nf_display_message(li_ret, astr_error_info, ii_orp002_commhandle) Then return false

return true

end function

public function boolean nf_orpo72ar_inq_exch_rate (ref s_error astr_error_info, string as_input, ref string as_output);Int	li_ret
String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


astr_error_info.se_procedure_name = "orpo72br_inq_so_exch_rate"
astr_error_info.se_message = Space(70)

as_output = Space(6040)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
		
li_ret = orpo72br_inq_so_exch_rate(ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message, &
		as_input, &
		as_output,&
		ii_orp002_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
If Not nf_display_message(li_ret, astr_error_info, ii_orp002_commhandle) Then return false

return true

end function

public function integer nf_orpo68ar_schedres (ref s_error astr_error_info, ref string as_header_in, ref string as_detail_in, ref double ad_tasknumber, ref double ad_pagenumber);Boolean  lb_OK
Int		li_ret
String	ls_detail_out, &
			ls_header_out, &
 			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


ls_detail_out = Space(13166)
ls_header_out = Space(100)

astr_error_info.se_procedure_name = "orpo68ar_schedres"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

li_ret = orpo68br_schedres(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_in, &
								as_detail_in, &
								ls_Header_out, &
								ls_Detail_out, &
								ad_TaskNumber, &
								ad_PageNumber, &
								ii_orp002_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
lb_OK = nf_display_message(li_ret, astr_error_info, &
			ii_orp002_commhandle) 
as_header_in = ls_header_out
as_detail_in = ls_detail_out
return li_ret



end function

public function integer nf_orpo75ar_validate_products (ref s_error astr_error_info, string as_header_string, ref string as_detail_string);Int		li_ret

String	ls_detail_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


astr_error_info.se_procedure_name = "orpo75br_validate_products"
astr_error_info.se_message = Space(70)

ls_detail_out = Space(19951)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_ret = orpo75br_validate_products(ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message, &
		as_header_string, &
		as_detail_string,&
		ls_detail_out, &
		ii_orp002_commhandle)
		
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
as_detail_string	=	Trim(ls_detail_out)										
nf_display_message(li_ret, astr_error_info, ii_orp002_commhandle)

RETURN li_ret
end function

public function integer nf_orpo77_validate_customers (ref s_error astr_error_info, string as_header_string, ref string as_detail_string, ref string as_default_string);Int		li_ret

String	ls_detail_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


astr_error_info.se_procedure_name = "orpo77br_validate_customers"
astr_error_info.se_message = Space(70)

ls_detail_out = Space(60000)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_ret =  orpo77br_Validate_Customer( &
   ls_app_name, &
	ls_window_name,&
	ls_function_name,&
	ls_event_name, &
	ls_procedure_name, &
	ls_user_id, &
	ls_return_code, &
	ls_message, &
   as_header_string, &
   as_detail_string, &
	as_default_String,&
 	ii_orp002_commhandle) 
	 
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
	 
nf_display_message(li_ret, astr_error_info, ii_orp002_commhandle)

RETURN li_ret
end function

public function integer nf_orpo78br_get_order_confimation (string as_primary_order, ref datawindowchild ad_stops_dw, ref datawindowchild ad_header_dw, ref datawindowchild ad_detail_dw, ref datawindowchild ad_instruction_dw, ref s_error astr_error_info);Int		li_ret

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,&
			ls_Stops,&
			ls_Header,&
			ls_Detail,&
			ls_Instruction


astr_error_info.se_procedure_name = "orpo78br_Get_Stops"
astr_error_info.se_message = Space(70)

ls_stops = Space(3399)
ls_stops = FillA(CharA(0),3399)

ls_Header = Space(1000)
ls_Header = FillA(CharA(0),1000)

ls_Detail = Space(12000)
ls_Detail = FillA(CharA(0),12000)

ls_Instruction = Space(2500)
ls_Instruction = FillA(CharA(0),2500)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

li_ret =  orpo78br_Get_Order_Confirmation( &
   ls_app_name, &
	ls_window_name,&
	ls_function_name,&
	ls_event_name, &
	ls_procedure_name, &
	ls_user_id, &
	ls_return_code, &
	ls_message, &
   as_Primary_Order, &
   ls_Stops, &
	ls_Header, &
   ls_Detail, &
   ls_instruction, &
	ii_orp002_commhandle) 
	 
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
if li_ret > -1 Then
	ad_stops_dw.Reset()
	ad_stops_dw.ImportString( ls_Stops)
	
	ad_header_dw.Reset()
	ad_header_dw.ImportString( ls_Header)
	
	ad_Detail_dw.Reset()
	ad_Detail_dw.ImportString( ls_Detail)

	ad_Instruction_dw.Reset()
	ad_Instruction_dw.ImportString( ls_instruction)
END IF
nf_display_message(li_ret, astr_error_info, ii_orp002_commhandle)

RETURN li_ret
end function

public function integer nf_orpo79br_multi_gen (ref s_error astr_error_info, string as_customer_info, string as_main_info_in, string as_additional_data, ref string as_main_info_out, ref string as_so_info, ref string multi_return_code);Char 			lc_Region
DataStore	lds_tmp, lds_temp_datastore
u_orp003		iu_orp003

Decimal	ldec_Price,&
			ldec_Quantity

int 		li_rtn
			

Long		ll_RowCount,&
			ll_LoopCount,&
			ll_OleRtn, ll_row
			

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id, &
			ls_return_code, &
			ls_message,&
			ls_queueString,&
			ls_QueueName,&
			ls_Age, &
			ls_other_info, &
			ls_task_number, &
			ls_start_time, &
			ls_end_time
			
TIME		lt_startTime, &
			lt_endTime, &
			lt_start_RPC, &
			lt_end_RPC
			
			
SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

multi_return_code = Space(1000)
multi_return_code = FillA(CharA(0), 1000)
as_main_info_out = Space(60000)
as_main_info_out = FillA(CharA(0), 60000)
as_so_info = Space(20000)
as_so_info = FillA(CharA(0), 20000)
ls_start_time = Space(8)
ls_start_time = FillA(CharA(42), 8)
ls_end_time = Space(8)
ls_end_time = FillA(CharA(0), 8)
ls_task_number = Space(10)
ls_task_number = FillA(CharA(0), 10)

lc_region = RightA(ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "Netwise Server Info", "ServerSuffix", "t"), 1)
Choose Case lower(lc_region)
	Case 'p'
		ls_QueueName = "ProfitQueue"
	Case 'z'
		ls_QueueName = "ProfitQueueQA"
	Case Else
		ls_QueueName = "ProfitQueueTest"
End Choose




//lds_Tmp = Create DataStore
//lds_Tmp.DataObject = 'd_pa_Inquiry_Main'
//ll_RowCount  = lds_Tmp.ImportString(as_main_info_in)
//ls_QueueString = ''
//For ll_LoopCount = 1 to ll_RowCount
//	   ldec_Price = lds_tmp.GetItemDecimal(ll_LoopCount,"sales_price")
//		ldec_Quantity = lds_Tmp.GetItemDecimal(ll_LoopCount,"ordered_units") 
//		ls_Age = lds_Tmp.GetItemString(ll_LoopCount,"ordered_age")
//		ls_QueueString += lds_Tmp.GetItemString(ll_LoopCount,"sku_product_code")+"~t"+String(ldec_Quantity)+"~t"+String(ldec_Price)+"~t"+ls_Age+"~r~n"
//End for
ls_QueueString = "orpo79b" + "~v" + as_customer_info + "~v" + as_additional_data + "~v" + as_main_info_in
IF LenA(Trim(ls_QueueString)) > 0 Then
	IF Not IsValid(iu_oleprophet) Then 
		iu_oleprophet = Create u_OleCom
		ll_OleRtn = iu_OleProphet.ConnectToNewObject("ProphetQueue.ProphetSender.1")
		IF ll_OleRtn = 0 Then
			iu_OleProphet.Send(ls_QueueName,"Excel Data for Prophet",ls_QueueString)
		Else
			Destroy iu_oleprophet
		End if
	Else
		iu_OleProphet.Send(ls_QueueName,"Excel Data for Prophet",ls_QueueString)
	End if 
End if


nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
		
lt_startTime = Now()

// Call a Netwise external function to get the required information
li_rtn = orpo79br_multi_gen(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_customer_info, &
    				as_additional_data, &
					as_main_info_in, &
    				as_main_info_out, &
					as_so_info, &
					multi_return_code, &
					ls_start_time, &
					ls_end_time, &
					ls_task_number, &
    				ii_orp002_commhandle) 
					 
lt_endTime = Now()
ls_procedure_name = 'nf_orp79br'

//lt_start_RPC = Time(ls_start_time)
//lt_end_RPC = Time(ls_end_time)

lt_start_RPC = Time(ls_window_name)
lt_end_RPC = Time(ls_function_name)


lds_temp_datastore = create Datastore
lds_temp_datastore.dataobject = 'd_so_choices'
ll_row = lds_temp_datastore.ImportString(as_so_info)
if ll_row > 0 then
	ls_other_info = string(ll_row) + ' orders generated.'
else
	ls_other_info = ' No orders generated.'
end if
ls_task_number = ls_app_name

//benchmark call
nf_write_benchmark2(lt_startTime, lt_endtime, ls_procedure_name, &
					lt_start_RPC, lt_end_RPC, ls_other_info, ls_task_number)
			
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 
			
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp002_commhandle)

return li_rtn
end function

public function integer nf_orp80br_print_fax ();//int 		li_rtn
//
//String 	ls_app_name, &
//			ls_window_name, &
//			ls_function_name, &
//			ls_event_name, &
//			ls_procedure_name, &
//			ls_user_id, &
//			ls_return_code, &
//			ls_message
//
//SetPointer(HourGlass!)
//iw_frame.SetMicroHelp( "Wait...PC communicating with Mainframe")
//
//multi_return_code = Space(1000)
//multi_return_code = FILL(CHAR(0), 1000)
//as_main_info_out = Space(60000)
//as_main_info_out = FILL(CHAR(0), 60000)
//as_so_info = Space(20000)
//as_so_info = FILL(Char(0), 20000)
//
//nf_get_s_error_values ( &
//		astr_error_info, &
//		ls_app_name, &
//		ls_window_name, &
//		ls_function_name, &
//		ls_event_name, &
//		ls_procedure_name, &
//		ls_user_id,&
//		ls_return_code,&
//		ls_message )
//
//// Call a Netwise external function to get the required information
//
//li_rtn = orpo79br_multi_gen(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_customer_info, &
//    				as_additional_data, &
//					as_main_info_in, &
//    				as_main_info_out, &
//					as_so_info, &
//					multi_return_code, &
//    				ii_orp002_commhandle) 
//			
//nf_set_s_error ( astr_error_info,&
//					ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message )				 
//			
//
//// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
//THIS.nf_display_message(li_rtn, astr_error_info, ii_orp002_commhandle)
//
return 1
end function

public function integer nf_orpo80br_print_fax (ref s_error astr_error_info, character ac_process_option_ind, string as_input_info_in, ref string as_input_info_out);int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id, &
			ls_return_code, &
			ls_message

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

as_input_info_out = Space(10000)
as_input_info_out = FillA(CharA(0), 10000)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

// Call a Netwise external function to get the required information
li_rtn = orpo80br_print_fax(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ac_process_option_ind, &
					as_input_info_in, &
    				as_input_info_out, &
					ii_orp002_commhandle) 
			
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 
			

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp002_commhandle)

return li_rtn
end function

public function boolean nf_write_benchmark2 (time at_starttime, time at_endtime, string as_function_name, time at_start_rpc, time at_end_rpc, string as_other_info, string as_task_number);char 					lc_PCIP_Address[100], &
						lc_region

Integer				li_fileNum, &
						li_TimeVal
						
Long					ll_SecondsAfterPC, &
						ll_secondsAfterRPC

String				ls_FileName, &
						ls_ipaddr, &
						ls_pcip_address, &
						ls_path

u_ip_functions		lu_ip_address_functions


li_TimeVal = ProfileInt(gw_netwise_frame.is_WorkingDir + "ibp002.ini",&
								"BenchMark",&
								Message.nf_Get_App_ID(),6)
								
IF li_timeVal = 0 Then Return False								

ll_SecondsAfterPC = SecondsAfter(at_starttime, at_endtime) 
ll_secondsAfterRPC = SecondsAfter(at_start_rpc, at_end_rpc)

IF ll_SecondsAfterPC < li_TimeVal Then Return False 

lc_region = RightA(ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "nw_server", "server" + &
			String(1), ""), 1)
Choose Case lc_region
	Case 'p'
		ls_path = ProfileString(gw_netwise_frame.is_WorkingDir + 'ibp002.ini', "BenchMark", 'PATH', "f:\software\pb\pblog101\")
//		ls_path = "f:\software\pb\pblog101\"
	Case 't'
		ls_path = ProfileString(gw_netwise_frame.is_WorkingDir + 'ibp002.ini', "BenchMark", 'PATH', "j:\test\pb\")
//		ls_path = "j:\pb\test\exe32\"
	Case 'z'
		ls_path = ProfileString(gw_netwise_frame.is_WorkingDir + 'ibp002.ini', "BenchMark", 'PATH', "\\NTWHQ03\NTWHQ03\software\pb\pblog101\")
//		ls_path = "j:\software\pb\qa\pblog101\"
	Case ''
		Return False
End Choose

ls_fileName = ls_path + "BM" + &
					String(Month(Today()), '00') + &
					String(Day(Today()), '00') + &
					".log"

li_FileNum = FileOpen(ls_FileName,  &
	lineMode!, Write!, LockWrite! , Append! )
	
ls_ipaddr = lu_ip_address_functions.nf_get_ip_address()

FileWrite(li_FileNum, Message.nf_Get_App_ID() + &
			"~tUser: " + Trim(SQLCA.Userid) + &
			"~tIP Addr: " + Trim(ls_ipaddr) + &
			"~tTask Number: " + Trim(as_task_number) + &
			"~tProcedure Name: " + Trim(as_function_name) + &
			"~tPC Start: " + Trim(String(At_StartTime,"hh:mm:ss")) + &
			"~tPC End: " + Trim(String(At_EndTime,"hh:mm:ss")) + &
			"~tPC Elapsed: " + Trim(String(ll_SecondsAfterPC)) + &
			"~tRPC Start: " + Trim(String(at_start_rpc,"hh:mm:ss")) + &
			"~tRPC End: " + Trim(String(at_end_rpc,"hh:mm:ss")) + &
			"~tRPC Elapsed: " + Trim(String(ll_secondsAfterRPC)) + &
			"~t   " + Trim(as_other_info))

FileClose(li_FileNum)

Return True
end function

event constructor;call super::constructor;STRING	ls_MicroHelp

ls_MicroHelp = gw_NetWise_Frame.wf_GetMicroHelp()

gw_NetWise_Frame.SetMicroHelp( "Opening MainFrame Connections")

ii_orp002_commhandle = SQLCA.nf_GetCommHandle("orp002")

If ii_orp002_commhandle = 0 Then
	Message.ReturnValue = -1
End if

gw_NetWise_Frame.SetMicroHelp(ls_MicroHelp)

end event

on u_orp002.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_orp002.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

