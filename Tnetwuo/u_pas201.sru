HA$PBExportHeader$u_pas201.sru
$PBExportComments$IBDKDLD Contains pasp00br - pasp08br, p41b-p42b, p71br --
forward
global type u_pas201 from u_netwise_transaction
end type
end forward

global type u_pas201 from u_netwise_transaction
end type
global u_pas201 u_pas201

type prototypes
// PowerBuilder Script File: j:\dev\workbe~1\src32\pas201.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Wed Aug 04 15:14:14 1999
// Source Interface File: j:\dev\workbe~1\src32\pas201.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: pasp00br_inq_fab
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp00br_inq_fab( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char fab_product_code[11], &
    ref char projections_exist, &
    ref char product_status, &
    ref string tpasfab_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp00br_inq_fab;Ansi"


//
// Declaration for procedure: pasp01br_upd_fab
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp01br_upd_fab( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string tpasfab_update_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp01br_upd_fab;Ansi"


//
// Declaration for procedure: pasp02br_inq_tree
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp02br_inq_tree( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char fab_product_code[10], &
    char direction_ind, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string product_string, &
    ref string tree_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp02br_inq_tree;Ansi"


//
// Declaration for procedure: pasp03br_inq_fab_descr
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp03br_inq_fab_descr( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char fab_product[10], &
    ref string fab_description, &
    int CommHnd &
) library "pas201.dll" alias for "pasp03br_inq_fab_descr;Ansi"


//
// Declaration for procedure: pasp04br_inq_step
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp04br_inq_step( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char fab_product_code[10], &
    ref char fab_product_descr[30], &
    ref char fab_group[2], &
    ref string mfg_step_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp04br_inq_step;Ansi"


//
// Declaration for procedure: pasp05br_inq_step_detail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp05br_inq_step_detail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char fab_product_code[10], &
    int mfg_step_sequence, &
    ref int shift_duration, &
    ref char step_type, &
    ref string output_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp05br_inq_step_detail;Ansi"


//
// Declaration for procedure: pasp06br_upd_step
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp06br_upd_step( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    string detail_string, &
    ref int new_step_sequence, &
    int CommHnd &
) library "pas201.dll" alias for "pasp06br_upd_step;Ansi"


//
// Declaration for procedure: pasp07br_inq_pltchr
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp07br_inq_pltchr( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char fab_product_code[10], &
    int mfg_step_sequence, &
    ref char fab_group[2], &
    ref string plant_string, &
    ref string char_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp07br_inq_pltchr;Ansi"


//
// Declaration for procedure: pasp08br_upd_pltchr
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp08br_upd_pltchr( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char fab_product_code[10], &
    int mfg_step_sequence, &
    string plant_string, &
    string char_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp08br_upd_pltchr;Ansi"


//
// Declaration for procedure: pasp41br_inq_exceptions
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp41br_inq_exceptions( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string plant_string, &
    ref string char_string, &
    ref string exception_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp41br_inq_exceptions;Ansi"


//
// Declaration for procedure: pasp42br_upd_exceptions
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp42br_upd_exceptions( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    string exception_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp42br_upd_exceptions;Ansi"


//
// Declaration for procedure: pasp71br_inq_weekly_sources
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp71br_inq_weekly_sources( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp71br_inq_weekly_sources;Ansi"


//
// Declaration for procedure: pasp72br_inq_shift_duration
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp72br_inq_shift_duration( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp72br_inq_shift_duration;Ansi"


//
// Declaration for procedure: pasp73br_inq_weekly_avail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp73br_inq_weekly_avail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp73br_inq_weekly_avail;Ansi"


//
// Declaration for procedure: pasp74br_inq_locdiv_param
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp74br_inq_locdiv_param( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp74br_inq_locdiv_param;Ansi"


//
// Declaration for procedure: pasp75br_up_locdiv_param
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp75br_up_locdiv_param( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_header_string, &
    string se_input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp75br_up_locdiv_param;Ansi"


//
// Declaration for procedure: pasp76br_inq_sold_position_rept
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp76br_inq_sold_position_rept( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp76br_inq_sold_position_rept;Ansi"


//
// Declaration for procedure: pasp77br_inq_age_avail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp77br_inq_age_avail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp77br_inq_age_avail;Ansi"


//
// Declaration for procedure: pasp78br_inq_loadlist
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp78br_inq_loadlist( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp78br_inq_loadlist;Ansi"


//
// Declaration for procedure: pasp79br_inq_loadcombo
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp79br_inq_loadcombo( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string_in, &
    ref string header_string_out, &
    ref string output_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp79br_inq_loadcombo;Ansi"


//
// Declaration for procedure: pasp80br_inq_load_list_excpt
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp80br_inq_load_list_excpt( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp80br_inq_load_list_excpt;Ansi"


//
// Declaration for procedure: pasp81br_upd_load_list
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp81br_upd_load_list( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    string detail_string_in, &
    ref string detail_string_out, &
    string exception_string_in, &
    ref string exception_string_out, &
    int CommHnd &
) library "pas201.dll" alias for "pasp81br_upd_load_list;Ansi"


//
// Declaration for procedure: pasp82br_gen_load_list
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp82br_gen_load_list( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp82br_gen_load_list;Ansi"


//
// Declaration for procedure: pasp86br_inq_source_sequence
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp86br_inq_source_sequence( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp86br_inq_source_sequence;Ansi"


//
// Declaration for procedure: pasp87br_upd_source_sequence
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp87br_upd_source_sequence( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_header_string, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp87br_upd_source_sequence;Ansi"


//
// Declaration for procedure: pasp88br_inq_work_hours
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp88br_inq_work_hours( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp88br_inq_work_hours;Ansi"


//
// Declaration for procedure: pasp89br_upd_work_hours
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp89br_upd_work_hours( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp89br_upd_work_hours;Ansi"


//
// Declaration for procedure: PASP90BR_INQ_VALID_PLANT_TRANSFER
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int PASP90BR_INQ_VALID_PLANT_TRANSFER( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "PASP90BR_INQ_VALID_PLANT_TRANSFER;Ansi"


//
// Declaration for procedure: pasp91br_upd_valid_plant_transfer
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp91br_upd_valid_plant_transfer( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp91br_upd_valid_plant_transfer;Ansi"


//
// Declaration for procedure: pasp92br_inq_product_plant_parm
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp92br_inq_product_plant_parm( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp92br_inq_product_plant_parm;Ansi"


//
// Declaration for procedure: pasp93br_upd_product_plant_parm
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp93br_upd_product_plant_parm( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp93br_upd_product_plant_parm;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "pas201.dll"
function int WBpas201CkCompleted( int CommHnd ) &
    library "pas201.dll"
//
// ***********************************************************

//
// User Structure:  s_error
//
// global type s_error from structure
//     char        se_app_name[8]                
//     char        se_window_name[8]             
//     char        se_function_name[8]           
//     char        se_event_name[32]             
//     char        se_procedure_name[32]         
//     char        se_user_id[8]                 
//     char        se_return_code[6]             
//     char        se_message[71]                
// end type

end prototypes

type variables
Int		ii_pas201_commhandle


end variables

forward prototypes
public function boolean nf_pasp02br (ref s_error astr_error_info, character ac_fab_product_code[10], character ac_direction_ind, ref double ad_task_number, ref double ad_last_record_number, ref double ad_max_record_number, ref string as_product_string, ref string as_tree)
public function boolean nf_pasp01br (ref s_error astr_error_info, ref string as_tpasfab)
public function boolean nf_pasp04br (ref s_error astr_error_info, character ac_fab_product_code[10], ref character ac_fab_product_descr[30], ref character ac_fab_group[2], ref string as_mfg_step_string)
public function boolean nf_pasp06br (ref s_error astr_error_info, string as_header_string, string as_detail_string, ref integer ai_new_step_sequence)
public function boolean nf_pasp07br (ref s_error astr_error_info, character ac_fab_product_code[10], integer ai_mfg_step_sequence, ref character ac_fab_group[2], ref string as_plant_string, ref string as_char_string)
public function boolean nf_pasp08br (ref s_error astr_error_info, character ac_fab_product_code[10], integer ai_mfg_step_sequence, string as_plant_string, string as_char_string)
public function boolean nf_inq_exception (ref s_error astr_error_info, string as_input, ref string as_plant, ref string as_char, ref string as_exception)
public function boolean nf_upd_exception (ref s_error astr_error_info, string as_header, string as_exception)
public function boolean nf_pasp05br (ref s_error astr_error_info, string as_fab_product_code, integer ai_mfg_step_sequence, ref integer ai_shift_duration, ref character ac_step_type, ref string as_output_string)
public function boolean nf_pasp03br (ref s_error astr_error_info, character ac_fab_product_code[10], ref string as_fab_product_descr)
public function integer nf_pasp00br (ref s_error astr_error_info, character fab_product_code[10], ref character ac_projections_exist, ref character ac_product_status, ref string as_tpasfab_string)
public function integer nf_pasp71br_inq_weekly_sources (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp72br_inq_shift_duration (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp74br_inq_locdiv_parameters (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp75br_up_locdiv_param (ref s_error astr_error_info, string as_header_string, string as_input_string)
public function integer nf_pasp73br_inq_weekly_avail (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp76br_inq_sold_position (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp77br_inq_age_avail (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp80br_inq_load_list_except (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp78br_inq_load_list_maint (ref s_error astr_error_info, string as_input_string, ref string as_output_maint, ref string as_output_except)
public function integer nf_pasp79br_inq_load_combination (ref s_error astr_error_info, string as_header_stirng_in, ref string as_header_string_out, ref string as_output_string)
public function integer nf_pasp82br_send_load_list (ref s_error astr_error_info, string as_input_string)
public function boolean nf_pasp87br_upd_source_sequence (ref s_error astr_error_info, string as_header_string, string as_input_string)
public function boolean nf_pasp86br_inq_source_sequence (ref s_error astr_error_info, string as_input_string, ref string as_header_a, ref string as_shift_a, ref string as_header_b, ref string as_shift_b, ref string as_header_c, ref string as_shift_c)
public function integer nf_pasp81br_update_load_list_maint (ref s_error astr_error_info, string as_input_string, string as_detail_in, ref string as_detail_out, string as_exception_in, ref string as_exception_out)
public function boolean nf_pasp89br_upd_work_hours (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string)
public function integer nf_pasp92br_product_plant_parameters (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp93br_update_product_plant_para (ref s_error astr_error_info, string as_input_string)
public function boolean nf_pasp88br_inq_work_hours (ref s_error astr_error_info, ref string as_header_string, ref string as_prod, ref string as_ship, ref string as_tran)
public function integer nf_pasp90br_inq_valid_plant_transfer (s_error astr_error_info, ref string as_output_string)
public function boolean nf_pasp91br_upd_valid_plant_transfer (s_error astr_error_info, ref string as_input_string)
end prototypes

public function boolean nf_pasp02br (ref s_error astr_error_info, character ac_fab_product_code[10], character ac_direction_ind, ref double ad_task_number, ref double ad_last_record_number, ref double ad_max_record_number, ref string as_product_string, ref string as_tree);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp02br_inq_tree(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								ac_fab_product_code, &
									ac_direction_ind, &
									ad_task_number, &
									ad_last_record_number, &
									ad_max_record_number, &
									as_product_string, &
									as_tree, &
									ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)
end function

public function boolean nf_pasp01br (ref s_error astr_error_info, ref string as_tpasfab);Int li_rtn

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_rtn = pasp01br_upd_fab(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_tpasfab, &
									ii_pas201_commhandle) 

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_pasp04br (ref s_error astr_error_info, character ac_fab_product_code[10], ref character ac_fab_product_descr[30], ref character ac_fab_group[2], ref string as_mfg_step_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


as_mfg_step_string = Space(55501)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp04br_inq_step(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								ac_fab_product_code, &
									ac_fab_product_descr, &
									ac_fab_group, &
									as_mfg_step_string, &
									ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)
end function

public function boolean nf_pasp06br (ref s_error astr_error_info, string as_header_string, string as_detail_string, ref integer ai_new_step_sequence);Int	li_rtn

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_rtn = pasp06br_upd_step(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
									as_detail_string, &
									ai_new_step_sequence, &
									ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

Return nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)
end function

public function boolean nf_pasp07br (ref s_error astr_error_info, character ac_fab_product_code[10], integer ai_mfg_step_sequence, ref character ac_fab_group[2], ref string as_plant_string, ref string as_char_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


as_plant_string = Space(1001)
as_char_string = Space(4001)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp07br_inq_pltchr(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								ac_fab_product_code, &
										ai_mfg_step_sequence, &
										ac_fab_group, &
										as_plant_string, &
										as_char_string, &
										ii_pas201_commhandle) 

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

Return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_pasp08br (ref s_error astr_error_info, character ac_fab_product_code[10], integer ai_mfg_step_sequence, string as_plant_string, string as_char_string);Int	li_rtn

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_rtn = pasp08br_upd_pltchr(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								ac_fab_product_code, &
										ai_mfg_step_sequence, &
										as_plant_string, &
										as_char_string, &
										ii_pas201_commhandle)
										
lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

Return nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_inq_exception (ref s_error astr_error_info, string as_input, ref string as_plant, ref string as_char, ref string as_exception);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


as_plant     = Space(7201)
as_char      = Space(3601)
as_exception = Space(2001)

astr_error_info.se_procedure_name = "pasp41br_inq_exceptions"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp41br_inq_exceptions(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_input, &
											as_plant, &
											as_char, &
											as_exception, &
											ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_upd_exception (ref s_error astr_error_info, string as_header, string as_exception);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp42br_upd_exceptions"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp42br_upd_exceptions(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header, &
									as_exception, &
									ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_pasp05br (ref s_error astr_error_info, string as_fab_product_code, integer ai_mfg_step_sequence, ref integer ai_shift_duration, ref character ac_step_type, ref string as_output_string);Char	lc_product_code[10]
Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


as_output_string = Space(6301)
lc_product_code = Space(10)
lc_product_code = as_fab_product_code

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp05br_inq_step_detail(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								lc_product_code, &
											ai_mfg_step_sequence, &
											ai_shift_duration, &
											ac_step_type, &
											as_output_string, &
											ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)
end function

public function boolean nf_pasp03br (ref s_error astr_error_info, character ac_fab_product_code[10], ref string as_fab_product_descr);Int li_rtn

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


as_fab_product_descr = Space(42)

astr_error_info.se_user_id = SQLCA.Userid

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_rtn = pasp03br_inq_fab_descr(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								ac_fab_product_code, &
											as_fab_product_descr, &
											ii_pas201_commhandle) 

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp00br (ref s_error astr_error_info, character fab_product_code[10], ref character ac_projections_exist, ref character ac_product_status, ref string as_tpasfab_string);Time							lt_starttime, &
								lt_endtime
								
String			ls_app_name, &
					ls_window_name, &
					ls_function_name, &
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id,&
					ls_return_code,&
					ls_message

Int	li_rtn

as_tpasfab_string = Space(86)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_rtn = pasp00br_inq_fab(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								fab_product_code, &
									ac_projections_exist, &
									ac_product_status, &
									as_tpasfab_string, &
									ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

Return li_rtn
end function

public function integer nf_pasp71br_inq_weekly_sources (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp71br_inq_weekly_sources"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = FillA(CharA(0),20000)

Do
	lt_starttime = Now()

	li_rtn = pasp71br_inq_weekly_sources(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas201_commhandle)
Long		ll_temp
	ll_temp = LenA(ls_output)
	as_output_string += ls_output
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function integer nf_pasp72br_inq_shift_duration (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp72br_inq_shift_duration"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = FillA(CharA(0),20000)

Do
	lt_starttime = Now()

	li_rtn = pasp72br_inq_shift_duration(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas201_commhandle)
	as_output_string += ls_output
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function integer nf_pasp74br_inq_locdiv_parameters (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp74br_inq_locdiv_param"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(5000)
ls_output = FillA(CharA(0),5000)

Do
	lt_starttime = Now()

	li_rtn = pasp74br_inq_locdiv_param(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas201_commhandle)
	as_output_string += ls_output
	lt_endtime = Now()
	
	
//	messagebox('output',string(as_output_string))
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function boolean nf_pasp75br_up_locdiv_param (ref s_error astr_error_info, string as_header_string, string as_input_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp75br_up_locdiv_param"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp75br_up_locdiv_param(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
									as_input_string, &
									ii_pas201_commhandle)

lt_endtime = Now()

//messagebox ('function',string(ls_function_name))

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp73br_inq_weekly_avail (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp73br_inq_weekly_avail"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = FillA(CharA(0),20000)

Do
	lt_starttime = Now()

	li_rtn = pasp73br_inq_weekly_avail(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas201_commhandle)
	as_output_string += ls_output
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function integer nf_pasp76br_inq_sold_position (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp76br_inq_sold_position_rept"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(5000)
ls_output = FillA(CharA(0),5000)

Do
	lt_starttime = Now()

	li_rtn = pasp76br_inq_sold_position_rept(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas201_commhandle)
	as_output_string += ls_output
	lt_endtime = Now()
	
	
//	messagebox('output',string(as_output_string))
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function integer nf_pasp77br_inq_age_avail (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp77br_inq_age_avail"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = FillA(CharA(0),20000)

Do
	lt_starttime = Now()

	li_rtn = pasp77br_inq_age_avail(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas201_commhandle)
	as_output_string += ls_output
	lt_endtime = Now()
	
	
//	messagebox('output',string(as_output_string))
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function integer nf_pasp80br_inq_load_list_except (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp80br_load_list_except_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = FillA(CharA(0),20000)

Do
	lt_starttime = Now()

	li_rtn = pasp80br_inq_load_list_excpt(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas201_commhandle)
	as_output_string += ls_output
	lt_endtime = Now()
	
	
//	messagebox('output',string(as_output_string))
	
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function integer nf_pasp78br_inq_load_list_maint (ref s_error astr_error_info, string as_input_string, ref string as_output_maint, ref string as_output_except);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output_string, &
				ls_output, &
				ls_string_ind
				
Time			lt_starttime, &
				lt_endtime
				
u_string_functions	lu_string				
								
as_output_maint = ''
as_output_except = ''

astr_error_info.se_procedure_name = "pasp78br_auto_load_list_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

Do
	ls_output_string = Space(20000)
	ls_output_string = FillA(CharA(0),20000)

	lt_starttime = Now()
	
	li_rtn = pasp78br_inq_loadlist(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_output_string, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas201_commhandle)
	lt_endtime = Now()
	
	
//	messagebox('output',string(as_output_string))

	ls_string_ind = MidA(ls_output_string, 2, 1)
	ls_output_string = MidA(ls_output_string, 3)
	Do While LenA(Trim(ls_output_string)) > 0
		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		
		Choose Case ls_string_ind
			Case 'M'
				as_output_maint += ls_output
			Case 'E'
				as_output_except += ls_output
		End Choose
		ls_string_ind = LeftA(ls_output_string, 1)
		ls_output_string = MidA(ls_output_string, 2)
	Loop 

	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function integer nf_pasp79br_inq_load_combination (ref s_error astr_error_info, string as_header_stirng_in, ref string as_header_string_out, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output, &
				ls_header_out
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''
as_header_string_out = ''

astr_error_info.se_procedure_name = "pasp78br_auto_load_list_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = FillA(CharA(0),20000)
ls_header_out = Space(50)
ls_header_out = FillA(CharA(0),50)

lt_starttime = Now()

li_rtn = pasp79br_inq_loadcombo(ls_app_name, &
				ls_window_name,&
				ls_function_name,&
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id, &
				ls_return_code, &
				ls_message, &
				as_header_stirng_in, &
				ls_header_out, &
				ls_output, &
				ii_pas201_commhandle)
as_output_string += ls_output
as_header_string_out += ls_header_out 
lt_endtime = Now()
	
	
//	messagebox('output',string(as_output_string))
	
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function integer nf_pasp82br_send_load_list (ref s_error astr_error_info, string as_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message 
				
Time			lt_starttime, &
				lt_endtime
								

astr_error_info.se_procedure_name = "pasp82br_send_load_list"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

lt_starttime = Now()

li_rtn = pasp82br_gen_load_list(ls_app_name, &
				ls_window_name,&
				ls_function_name,&
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id, &
				ls_return_code, &
				ls_message, &
				as_input_string, &
				ii_pas201_commhandle)

lt_endtime = Now()
	
	
//	messagebox('output',string(as_output_string))
	
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function boolean nf_pasp87br_upd_source_sequence (ref s_error astr_error_info, string as_header_string, string as_input_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp87br_upd_source_sequence"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp87br_upd_source_sequence(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
									as_input_string, &
									ii_pas201_commhandle)

lt_endtime = Now()

//messagebox ('function',string(ls_function_name))

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_pasp86br_inq_source_sequence (ref s_error astr_error_info, string as_input_string, ref string as_header_a, ref string as_shift_a, ref string as_header_b, ref string as_shift_b, ref string as_header_c, ref string as_shift_c);Int	li_ret

Time						lt_starttime, &
							lt_endtime
								
Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number
					
String					ls_app_name, &
							ls_window_name, &
 							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_output,&
							ls_string_ind,&
							ls_output_string,&
							ls_return_code,&
							ls_message

u_string_functions	lu_string				



astr_error_info.se_procedure_name = "pasp86br_inq_source_sequence"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


	as_header_a		= ''
	as_shift_a     = ''
	as_header_b		= ''
	as_shift_b     = ''
	as_header_c		= ''
	as_shift_c     = ''
	ls_output_string = space(20000)
 	ls_output_string = FillA(CharA(0),20000)

Do
		lt_starttime = Now()
		li_ret = pasp86br_inq_source_sequence(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_output_string, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas201_commhandle)
		lt_endtime = Now()
	
	
		//	messagebox('output',string(as_output_string))

		ls_string_ind = MidA(ls_output_string, 2, 1)
		ls_output_string = MidA(ls_output_string, 3)
		Do While LenA(Trim(ls_output_string)) > 0
			ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		
			Choose Case ls_string_ind
				Case '1'
					as_header_a += ls_output
				Case '2'
					as_header_b += ls_output
				Case '3'
					as_header_c += ls_output
				Case 'A'
					as_shift_a += ls_output
				Case 'B'
					as_shift_b += ls_output
				Case 'C'
					as_shift_c += ls_output
			End Choose
			ls_string_ind = LeftA(ls_output_string, 1)
			ls_output_string = MidA(ls_output_string, 2)
		Loop 

		nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp81br_update_load_list_maint (ref s_error astr_error_info, string as_input_string, string as_detail_in, ref string as_detail_out, string as_exception_in, ref string as_exception_out);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp81_update_load_list_maint"
astr_error_info.se_message = Space(70)

as_detail_out = Space(20000)
as_detail_out = FillA(CharA(0),20000)
as_exception_out = Space(20000)
as_exception_out = FillA(CharA(0),20000)


nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp81br_upd_load_list(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_input_string, &
								as_detail_in, &
								as_detail_out, &
								as_exception_in, &
								as_exception_out, &
								ii_pas201_commhandle)

lt_endtime = Now()

//messagebox ('function',string(ls_function_name))

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

return li_ret


end function

public function boolean nf_pasp89br_upd_work_hours (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string);Double	ld_task_number, &
			ld_last_record_number, &
			ld_max_record_number
				
Int		li_ret

Time		lt_starttime, &
			lt_endtime
								

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


astr_error_info.se_procedure_name = "pasp89br_upd_work_hours"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )
								
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0								

lt_starttime = Now()

li_ret = pasp89br_upd_work_hours(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
								as_input_string, &
								ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number, &								
									ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp92br_product_plant_parameters (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp92br_product_plant_parameters"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = FillA(CharA(0),20000)

Do
	lt_starttime = Now()

	li_rtn = pasp92br_inq_product_plant_parm( &
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_input_string, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas201_commhandle)
					
//function int pasp92br_inq_product_plant_parm( &
//    ref string se_app_name, &
//    ref string se_window_name, &
//    ref string se_function_name, &
//    ref string se_event_name, &
//    ref string se_procedure_name, &
//    ref string se_user_id, &
//    ref string se_return_code, &
//    ref string se_message, &
//    string input_string, &
//    ref string output_string, &
//    ref double task_number, &
//    ref double last_record_number, &
//    ref double max_record_number, &
//    int CommHnd &
//) library "pas201.dll"

	as_output_string += ls_output
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function boolean nf_pasp93br_update_product_plant_para (ref s_error astr_error_info, string as_input_string);Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number


Int						li_ret

Time						lt_starttime, &
							lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp93br_update_product_plant_para"
astr_error_info.se_message = Space(70)
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp93br_upd_product_plant_parm(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								"",&
								as_input_string, &
								ld_task_number,&
								ld_last_record_number,& 
								ld_max_record_number,&
								ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_pasp88br_inq_work_hours (ref s_error astr_error_info, ref string as_header_string, ref string as_prod, ref string as_ship, ref string as_tran);Int						li_ret

Time						lt_starttime, &
							lt_endtime
								
Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number
					
String					ls_app_name, &
							ls_window_name, &
 							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_output,&
							ls_string_ind,&
							ls_output_string,&
							ls_return_code,&
							ls_message

u_string_functions	lu_string				



astr_error_info.se_procedure_name = "pasp88br_inq_work_hours"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


	as_prod				= ''
	as_ship				= ''
	as_tran				= ''
	ls_output_string 	= space(20000)
 	ls_output_string 	= FillA(CharA(0),20000)

Do
		lt_starttime = Now()
		li_ret = pasp88br_inq_work_hours(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_header_string, &
					ls_output_string, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas201_commhandle)
		lt_endtime = Now()
	
	
//		messagebox('output',string(ls_output_string))

		ls_string_ind = MidA(ls_output_string, 2, 1)
		ls_output_string = MidA(ls_output_string, 3)
		Do While LenA(Trim(ls_output_string)) > 0
			ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		
			Choose Case ls_string_ind
				Case 'P'
					as_prod += ls_output
				Case 'S'
					as_ship += ls_output
				Case 'T'
					as_tran += ls_output
			End Choose
			ls_string_ind = LeftA(ls_output_string, 1)
			ls_output_string = MidA(ls_output_string, 2)
		Loop 

		nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)



end function

public function integer nf_pasp90br_inq_valid_plant_transfer (s_error astr_error_info, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output_string
				
Time			lt_starttime, &
				lt_endtime
								



astr_error_info.se_procedure_name = "pasp90br_inq_valid_plant_transfer"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message)
		
//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output_string = Space(20000)
ls_output_string = FillA(CharA(0),20000)

Do
	lt_starttime = Now()

	li_rtn = pasp90br_inq_valid_plant_transfer(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ls_output_string, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas201_commhandle)
	as_output_string += ls_output_string
	lt_endtime = Now()
	


//	messagebox('output',string(as_output_string))
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Rmt001 li rtn',string(li_rtn))

Return li_rtn

end function

public function boolean nf_pasp91br_upd_valid_plant_transfer (s_error astr_error_info, ref string as_input_string);Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number


Int						li_ret

Time						lt_starttime, &
							lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp91br_upd_valid_plant_transfer"
astr_error_info.se_message = Space(71)
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp91br_upd_valid_plant_transfer(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_input_string, &
								ld_task_number,&
								ld_last_record_number,& 
								ld_max_record_number,&
								ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

event constructor;call super::constructor;// Get the CommHandle to be used for windows
ii_pas201_commhandle = sqlca.nf_getcommhandle("pas201")
If ii_pas201_commhandle = 0 Then
	// an error occured
	Message.ReturnValue = -1
End if
end event

on u_pas201.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_pas201.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

