HA$PBExportHeader$u_otr006.sru
forward
global type u_otr006 from u_netwise_transaction
end type
end forward

global type u_otr006 from u_netwise_transaction
end type
global u_otr006 u_otr006

type prototypes
// PowerBuilder Script File: c:\ibp\otr006\otr006.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Wed Oct 27 11:05:54 2004
// Source Interface File: c:\ibp\otr006\otr006.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: otrt68ar_214_exception_extract
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt68ar_214_exception_extract( &
    string req_exception_code, &
    string req_from_carr, &
    string req_to_carr, &
    string req_status_code, &
    string req_from_date, &
    string req_to_date, &
    ref string refetch_carrier_code, &
    ref string refetch_exception_code, &
    ref string refetch_date_added, &
    ref string refetch_time_added, &
    ref int refetch_seq_no, &
    ref string transaction_detail_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr006.dll" alias for "otrt68ar_214_exception_extract;Ansi"


//
// Declaration for procedure: otrt69ar_214_exception_update
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt69ar_214_exception_update( &
    string in_exception_code, &
    string in_date_added, &
    string in_time_added, &
    int in_sequence_no, &
    string in_load_key, &
    string in_stop_code, &
    string in_tran_date, &
    string in_tran_time, &
    string in_tran_time_code, &
    string in_status_code, &
    string in_status_date, &
    string in_status_time, &
    string in_status_time_code, &
    string in_status_city, &
    string in_status_state, &
    string in_status_country, &
    string in_carrier_code, &
    string in_carrier_equipment, &
    string in_carrier_invoice, &
    string in_reason_code, &
    string in_process_code, &
    string in_comment_line1, &
    string in_comment_line2, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr006.dll" alias for "otrt69ar_214_exception_update;Ansi"


//
// Declaration for procedure: otrt70ar_214_review_extract
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt70ar_214_review_extract( &
    string req_load_key, &
    string req_from_carrier, &
    string req_to_carrier, &
    string req_status_code, &
    string req_from_date, &
    string req_to_date, &
    string req_process_code, &
    ref string refetch_carrier_code, &
    ref string refetch_load_key, &
    ref string refetch_stop_code, &
    ref string refetch_tran_date, &
    ref string refetch_tran_time, &
    ref string refetch_status_code, &
    ref int refetch_seq_no, &
    ref string transaction_detail_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr006.dll" alias for "otrt70ar_214_review_extract;Ansi"


//
// Declaration for procedure: otrt71ar_214_detail_review
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt71ar_214_detail_review( &
    string in_from_carrier, &
    string in_to_carrier, &
    string in_from_date, &
    string in_to_date, &
    string in_from_time, &
    string in_to_time, &
    ref string refetch_carrier, &
    ref string refetch_date, &
    ref string refetch_time, &
    ref string refetch_load_key, &
    ref string refetch_stop_code, &
    ref int refetch_sequence, &
    ref string edi_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr006.dll" alias for "otrt71ar_214_detail_review;Ansi"


//
// Declaration for procedure: otrt72ar_carr_controls_extract
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt72ar_carr_controls_extract( &
    string req_carrier, &
    ref string carrier_info_string, &
    ref string carrier_edi_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr006.dll" alias for "otrt72ar_carr_controls_extract;Ansi"


//
// Declaration for procedure: otrt73ar_edi_carriers_past_due
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt73ar_edi_carriers_past_due( &
    ref string refetch_carrier_code, &
    ref string carrier_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr006.dll" alias for "otrt73ar_edi_carriers_past_due;Ansi"


//
// Declaration for procedure: otrt74ar_late_deliveries
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt74ar_late_deliveries( &
    string req_transmode, &
    string req_from_date, &
    string req_to_date, &
    string req_billto_customer, &
    string req_shipto_customer, &
    string req_complex, &
    string req_carrier, &
    string req_sales_location, &
    string req_tsr, &
    string req_svc_region, &
    string req_sales_division, &
    ref string refetch_customer_id, &
    ref string refetch_load_key, &
    ref string refetch_stop_code, &
    ref string refetch_sales_order, &
    ref string late_deliveries_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr006.dll" alias for "otrt74ar_late_deliveries;Ansi"


//
// Declaration for procedure: otrt75ar_booking_transmission
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt75ar_booking_transmission( &
    string req_from_carrier, &
    string req_to_carrier, &
    string req_complex, &
    string req_transmode, &
    string req_assigned_from_date, &
    string req_assigned_to_date, &
    string req_assigned_from_time, &
    string req_assigned_to_time, &
    ref string refetch_carrier, &
    ref string refetch_complex, &
    ref string refetch_load_key, &
    ref string transmission_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr006.dll" alias for "otrt75ar_booking_transmission;Ansi"


//
// Declaration for procedure: otrt76ar_inq_dispatch_event
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt76ar_inq_dispatch_event( &
    string req_load_key, &
    ref string refetch_complex_code, &
    ref int refetch_pickup_sequence, &
    ref string refetch_timestamp_added, &
    ref string se_dispatch_event_string, &
    ref s_error s_error_info, &
    int CommHnd &
) library "otr006.dll" alias for "otrt76ar_inq_dispatch_event;Ansi"


//
// Declaration for procedure: otrt77ar_inq_order_appointments
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt77ar_inq_order_appointments( &
    ref s_error s_error_info, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "otr006.dll" alias for "otrt77ar_inq_order_appointments;Ansi"


//
// Declaration for procedure: otrt78ar_upd_order_appointments
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int otrt78ar_upd_order_appointments( &
    ref s_error s_error_info, &
    string input_header_string, &
    string input_detail_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "otr006.dll" alias for "otrt78ar_upd_order_appointments;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "otr006.dll"
function int WBotr006CkCompleted( int CommHnd ) &
    library "otr006.dll"
//
// ***********************************************************

//
// User Structure:  s_error
//
// global type s_error from structure
//     char        se_app_name[8]                
//     char        se_window_name[8]             
//     char        se_function_name[8]           
//     char        se_event_name[32]             
//     char        se_procedure_name[32]         
//     char        se_user_id[8]                 
//     char        se_return_code[8]             
//     char        se_message[71]                
// end type

end prototypes

type variables
int ii_commhandle

end variables

forward prototypes
public function boolean nf_otrt68ar (string as_req_exception_code, string as_req_from_carr, string as_req_to_carr, string as_req_status_code, string as_req_from_date, string as_req_to_date, ref datawindow ad_transaction_detail_string, ref s_error astr_error_info)
public function integer nf_otrt69ar (string as_exception_code, string as_date_added, string as_time_added, integer ai_sequence_no, string as_load_key, string as_stop_code, string as_tran_date, string as_tran_time, string as_tran_time_code, string as_status_code, string as_status_date, string as_status_time, string as_status_time_code, string as_status_city, string as_status_state, string as_status_country, string as_carrier_code, string as_carrier_equipment, string as_carrier_invoice, string as_reason_code, string as_process_code, string as_comment_line1, string as_comment_line2, ref s_error astr_error_info, integer ai_comm_hnd)
public function boolean nf_otrt72ar (string as_req_carrier, ref datawindow ad_carrier_header_string, ref datawindow ad_carrier_edi_string, ref s_error astr_error_info)
public function boolean nf_otrt70ar (string as_req_load_key, string as_req_from_carr, string as_req_to_carr, string as_req_status_code, string as_req_from_date, string as_req_to_date, string as_req_process_code, ref datawindow ad_transaction_detail_string, ref s_error astr_error_info)
public function integer nf_otrt71ar (string as_from_carrier, string as_to_carrier, string as_from_date, string as_to_date, string as_from_time, string as_to_time, datawindow ad_edi_string, ref s_error astr_error_info)
public function integer nf_otrt73ar (ref datawindow ad_carrier_string, ref s_error astr_error_info)
public function integer nf_otrt74ar (string as_transmode, string as_from_date, string as_to_date, string as_billto_customer, string as_shipto_customer, string as_complex, string as_carrier, string as_sales_location, string as_tsr, string as_svc_region, string as_division, ref datawindow ad_late_deliveries_string, ref s_error astr_error_info)
public function integer nf_otrt75ar (string as_from_carrier, string as_to_carrier, string as_complex, string as_transmode, string as_assigned_from_date, string as_assigned_to_date, string as_assigned_from_time, string as_assigned_to_time, ref datawindow ad_transmission_string, ref s_error astr_error_info)
public function integer nf_otrt76ar (string as_req_load_key, ref string as_refetch_complex_code, ref integer ai_refetch_pickup_sequence, ref string as_refetch_timestamp_added, ref string as_se_dispatch_event_string, ref datawindow ad_dispatch_event, ref s_error astr_error_info)
public function integer nf_otrt77ar_inq_order_appt (ref s_error s_error_info, string as_input_string, ref string as_output_string)
public function integer nf_otrt78ar_upd_order_appt (ref s_error s_error_info, string as_input_header_string, string as_input_detail_string, ref string as_output_string)
end prototypes

public function boolean nf_otrt68ar (string as_req_exception_code, string as_req_from_carr, string as_req_to_carr, string as_req_status_code, string as_req_from_date, string as_req_to_date, ref datawindow ad_transaction_detail_string, ref s_error astr_error_info);Integer	li_rtn, &
			li_refetch_seq_no

String	ls_refetch_carrier_code, &
			ls_refetch_exception_code, &
			ls_refetch_date_added, &
 			ls_refetch_time_added, &			
			ls_output
			
ls_refetch_carrier_code   = space(4)
ls_refetch_exception_code = space(8)
ls_refetch_date_added     = space(10)
ls_refetch_time_added     = space(8)
li_refetch_seq_no         = 0

ad_transaction_detail_string.Reset()

gw_netwise_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

//Do
//	ls_output = Space(20000)
//	astr_error_info.se_message = Space(72)
//	astr_error_info.se_procedure_name = 'otrt68ar_214_exception_extract'
//
//
//call an external netwise function to get the required information
//li_rtn = otrt68ar_214_exception_extract(as_req_exception_code, &
//												    as_req_from_carr, &
//												    as_req_to_carr, &
//         									    as_req_status_code, &
//												    as_req_from_date, &
//												    as_req_to_date, &
//												    ls_refetch_carrier_code, &
//												    ls_refetch_exception_code, &
//												    ls_refetch_date_added, &
//											  	    ls_refetch_time_added, &
//												    li_refetch_seq_no, &
//												    ls_output, &
//												    astr_error_info, & 
//												    ii_commhandle)
//			
//	If This.nf_display_message(li_rtn, astr_error_info, ii_commhandle) Then
//		ad_transaction_detail_string.ImportString(ls_output)
//	end if
//
//Loop While len(Trim(ls_refetch_carrier_code)) > 0

gw_netwise_frame.SetMicroHelp( "Ready")
// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
END IF

SetPointer(Arrow!)
Return True
end function

public function integer nf_otrt69ar (string as_exception_code, string as_date_added, string as_time_added, integer ai_sequence_no, string as_load_key, string as_stop_code, string as_tran_date, string as_tran_time, string as_tran_time_code, string as_status_code, string as_status_date, string as_status_time, string as_status_time_code, string as_status_city, string as_status_state, string as_status_country, string as_carrier_code, string as_carrier_equipment, string as_carrier_invoice, string as_reason_code, string as_process_code, string as_comment_line1, string as_comment_line2, ref s_error astr_error_info, integer ai_comm_hnd);int li_rtn


gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")


//call an external netwise function to get the required information
//li_rtn = otrt69ar_214_exception_update(as_exception_code, as_date_added, as_time_added, &
//         ai_sequence_no, as_load_key, as_stop_code, as_tran_date, as_tran_time, as_tran_time_code, &
//         as_status_code, as_status_date, as_status_time, as_status_time_code, as_status_city, &
//         as_status_state, as_status_country, as_carrier_code, as_carrier_equipment, &
//			as_carrier_invoice, as_reason_code, as_process_code, as_comment_line1, &
//         as_comment_line2, astr_error_info, ii_commhandle)


//check for any RPC errors to display messages
nf_display_message(li_rtn, astr_error_info, ii_commhandle)

return li_rtn




end function

public function boolean nf_otrt72ar (string as_req_carrier, ref datawindow ad_carrier_header_string, ref datawindow ad_carrier_edi_string, ref s_error astr_error_info);Integer	li_rtn			

String	ls_carrier, &
			ls_carrier_header_output, &
			ls_carrier_edi_output
			
ad_carrier_header_string.Reset()
ad_carrier_edi_string.Reset()

gw_netwise_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)
 
ls_carrier_header_output = Space(1000)
ls_carrier_edi_output    = Space(5000)
astr_error_info.se_message = Space(72)
astr_error_info.se_procedure_name = 'otrt72ar_carr_controls_extract'


//call an external netwise function to get the required information
//li_rtn = otrt72ar_carr_controls_extract(as_req_carrier, &
//												    ls_carrier_header_output, &
//												    ls_carrier_edi_output, &
//												    astr_error_info, & 
//												    ii_commhandle)
			
If This.nf_display_message(li_rtn, astr_error_info, ii_commhandle) Then
	ad_carrier_header_string.ImportString(ls_carrier_header_output)
	ad_carrier_edi_string.ImportString(ls_carrier_edi_output)
end if



gw_netwise_frame.SetMicroHelp( "Ready")
// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
END IF

SetPointer(Arrow!)
Return True
end function

public function boolean nf_otrt70ar (string as_req_load_key, string as_req_from_carr, string as_req_to_carr, string as_req_status_code, string as_req_from_date, string as_req_to_date, string as_req_process_code, ref datawindow ad_transaction_detail_string, ref s_error astr_error_info);Integer	li_rtn, &
			li_refetch_seq_no

String	ls_refetch_carrier_code, &
			ls_refetch_load_key, &
			ls_refetch_stop_code, &
			ls_refetch_tran_date, &
 			ls_refetch_tran_time, &
			ls_refetch_status_code, &
			ls_output

ls_refetch_carrier_code  = space(4)
ls_refetch_load_key      = space(7)
ls_refetch_stop_code     = space(2)
ls_refetch_tran_date     = space(10)
ls_refetch_tran_time     = space(8)
ls_refetch_status_code   = space(2)
li_refetch_seq_no        = 0

ad_transaction_detail_string.Reset()

gw_netwise_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

//Do
//	ls_output = Space(20000)
//	astr_error_info.se_message = Space(72)
//	astr_error_info.se_procedure_name = 'otrt70ar_214_review_extract'
//
//
//call an external netwise function to get the required information
//li_rtn = otrt70ar_214_review_extract(as_req_load_key, &
//												 as_req_from_carr, &
//												 as_req_to_carr, &
//         									 as_req_status_code, &
//												 as_req_from_date, &
//												 as_req_to_date, &
//												 as_req_process_code, &
//												 ls_refetch_carrier_code, &
//												 ls_refetch_load_key, &
//												 ls_refetch_stop_code, &
//												 ls_refetch_tran_date, &
//											  	 ls_refetch_tran_time, &
//												 ls_refetch_status_code, &
//												 li_refetch_seq_no, &
//												 ls_output, &
//												 astr_error_info, & 
//												 ii_commhandle)
//			
//	If This.nf_display_message(li_rtn, astr_error_info, ii_commhandle) Then
//		ad_transaction_detail_string.ImportString(ls_output)
//	end if
//
//Loop While len(Trim(ls_refetch_stop_code)) > 0

gw_netwise_frame.SetMicroHelp( "Ready")
// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
END IF

SetPointer(Arrow!)
Return True
end function

public function integer nf_otrt71ar (string as_from_carrier, string as_to_carrier, string as_from_date, string as_to_date, string as_from_time, string as_to_time, datawindow ad_edi_string, ref s_error astr_error_info);int		li_rtn, &
			li_refetch_sequence

String	ls_refetch_carrier, &
			ls_refetch_date, &
			ls_refetch_time, &
 			ls_refetch_load_key, &
			ls_refetch_stop_code, &
			ls_output
			
ls_refetch_carrier	= space(4)
ls_refetch_date		= space(10)
ls_refetch_time		= space(8)
ls_refetch_load_key	= space(7)
ls_refetch_stop_code	= space(2)
li_refetch_sequence	= 0

ad_edi_string.Reset()

gw_netwise_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

//Do
//	ls_output = Space(20000)
//	astr_error_info.se_message = Space(72)
//	astr_error_info.se_procedure_name = 'otrt71ar_214_detail_review'
//
//
//call an external netwise function to get the required information
//li_rtn = otrt71ar_214_detail_review(as_from_carrier, &
//												as_to_carrier, &
//												as_from_date, &
//         									as_to_date, &
//												as_from_time, &
//												as_to_time, &
//												ls_refetch_carrier, &
//												ls_refetch_date, &
//												ls_refetch_time, &
//											   ls_refetch_load_key, &
//												ls_refetch_stop_code, &
//											   li_refetch_sequence, &
//												ls_output, &
//												astr_error_info, & 
//												ii_commhandle)
//			
//	If This.nf_display_message(li_rtn, astr_error_info, ii_commhandle) Then
//		ad_edi_string.ImportString(ls_output)
//	end if
//
//Loop While len(Trim(ls_refetch_carrier)) > 0

gw_netwise_frame.SetMicroHelp( "Ready")
// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
END IF

SetPointer(Arrow!)

return li_rtn




end function

public function integer nf_otrt73ar (ref datawindow ad_carrier_string, ref s_error astr_error_info);int		li_rtn			

String	ls_refetch_carrier, &
			ls_output
			
ls_refetch_carrier	= space(4)

ad_carrier_string.Reset()

gw_netwise_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

//Do
//	ls_output = Space(2000)
//	astr_error_info.se_message = Space(72)
//	astr_error_info.se_procedure_name = 'otrt73ar_edi_carriers_past_due'
//
//
//call an external netwise function to get the required information
//li_rtn = otrt73ar_edi_carriers_past_due(ls_refetch_carrier, &
//												    ls_output, &
//												    astr_error_info, & 
//												    ii_commhandle)
//			
//	If This.nf_display_message(li_rtn, astr_error_info, ii_commhandle) Then
//		ad_carrier_string.ImportString(ls_output)
//	end if
//
//Loop While len(Trim(ls_refetch_carrier)) > 0

gw_netwise_frame.SetMicroHelp( "Ready")
// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
END IF

SetPointer(Arrow!)

return li_rtn




end function

public function integer nf_otrt74ar (string as_transmode, string as_from_date, string as_to_date, string as_billto_customer, string as_shipto_customer, string as_complex, string as_carrier, string as_sales_location, string as_tsr, string as_svc_region, string as_division, ref datawindow ad_late_deliveries_string, ref s_error astr_error_info);int		li_rtn			

String	ls_refetch_customer_id, &
			ls_refetch_load_key, &
			ls_refetch_stop_code, &
 			ls_refetch_sales_order, &
			ls_output
			
ls_refetch_customer_id	= space(7)
ls_refetch_load_key		= space(7)
ls_refetch_stop_code		= space(2)
ls_refetch_sales_order	= space(7)

ad_late_deliveries_string.Reset()

gw_netwise_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

//Do
//	ls_output = Space(20000)
//	astr_error_info.se_message = Space(72)
//	astr_error_info.se_procedure_name = 'otrt74ar_late_deliveries'
//
//
//call an external netwise function to get the required information
//li_rtn = otrt74ar_late_deliveries(as_transmode, &
//											 as_from_date, &
//											 as_to_date, &
//         								 as_billto_customer, &
//											 as_shipto_customer, &
//											 as_complex, &
//											 as_carrier, &
//											 as_sales_location, &
//											 as_tsr, &
//											 as_svc_region, &
//											 as_division, &
//											 ls_refetch_customer_id, &
//											 ls_refetch_load_key, &
//											 ls_refetch_stop_code, &
//											 ls_refetch_sales_order, &
//											 ls_output, &
//											 astr_error_info, & 
//											 ii_commhandle)
//			
//	If This.nf_display_message(li_rtn, astr_error_info, ii_commhandle) Then
//		ad_late_deliveries_string.ImportString(ls_output)
//	end if
//
//Loop While len(Trim(ls_refetch_customer_id)) > 0

gw_netwise_frame.SetMicroHelp( "Ready")
// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
END IF

SetPointer(Arrow!)

return li_rtn





end function

public function integer nf_otrt75ar (string as_from_carrier, string as_to_carrier, string as_complex, string as_transmode, string as_assigned_from_date, string as_assigned_to_date, string as_assigned_from_time, string as_assigned_to_time, ref datawindow ad_transmission_string, ref s_error astr_error_info);int		li_rtn			

String	ls_refetch_carrier, &
			ls_refetch_complex, &
			ls_refetch_load_key, &
			ls_output
			
ls_refetch_carrier	   = space(7)
ls_refetch_complex		= space(7)
ls_refetch_load_key		= space(2)

ad_transmission_string.Reset()

gw_netwise_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

//Do
//	ls_output = Space(20000)
//	astr_error_info.se_message = Space(72)
//	astr_error_info.se_procedure_name = 'otrt75ar_booking_transmission'
//
//
//call an external netwise function to get the required information
//li_rtn = otrt75ar_booking_transmission(as_from_carrier, &
//											 		as_to_carrier, &
//											 		as_complex, &
//         								 		as_transmode, &
//											 		as_assigned_from_date, &
//											 		as_assigned_to_date, &
//											 		as_assigned_from_time, &
//											 		as_assigned_to_time, &
//											 		ls_refetch_carrier, &
//											 		ls_refetch_complex, &
//											 		ls_refetch_load_key, &
//											 		ls_output, &
//											 		astr_error_info, & 
//											 		ii_commhandle)
//			
//	If This.nf_display_message(li_rtn, astr_error_info, ii_commhandle) Then
//		ad_transmission_string.ImportString(ls_output)
//	end if
//
//Loop While len(Trim(ls_refetch_carrier)) > 0

gw_netwise_frame.SetMicroHelp( "Ready")
// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
END IF

SetPointer(Arrow!)

return li_rtn





end function

public function integer nf_otrt76ar (string as_req_load_key, ref string as_refetch_complex_code, ref integer ai_refetch_pickup_sequence, ref string as_refetch_timestamp_added, ref string as_se_dispatch_event_string, ref datawindow ad_dispatch_event, ref s_error astr_error_info);int		li_rtn	
long		ll_long

String	ls_output
as_refetch_complex_code = Space(3)
ai_refetch_pickup_sequence = 0
as_refetch_timestamp_added = Space(26)

gw_netwise_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

//Do
//	astr_error_info.se_message = Space(72)
//	as_se_dispatch_event_string = Space(6001)
//	astr_error_info.se_procedure_name = 'otrt76ar_inq_dispatch_event'
//
	//	call an external netwise function to get the required information
//	li_rtn = otrt76ar_inq_dispatch_event(as_req_load_key, &
//											 		as_refetch_complex_code, &
//											 		ai_refetch_pickup_sequence, &
//         								 		as_refetch_timestamp_added, &
//											 		as_se_dispatch_event_string, &
//											 		astr_error_info, & 
//											 		ii_commhandle)
//			
//	If This.nf_display_message(li_rtn, astr_error_info, ii_commhandle) Then
//		ll_long = ad_dispatch_event.ImportString(Trim(as_se_dispatch_event_string))
//	End If
//
//Loop While len(Trim(as_refetch_complex_code)) > 0

gw_netwise_frame.SetMicroHelp( "Ready")
// Check the return code from the above function call
IF li_rtn <> 0 THEN
	gw_netwise_frame.SetMicroHelp(astr_error_info.se_message)
END IF

SetPointer(Arrow!)

return li_rtn



end function

public function integer nf_otrt77ar_inq_order_appt (ref s_error s_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, ld_last_record_number, ld_max_record_number
Int			li_rtn
String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
								 
as_output_string = ''
s_error_info.se_procedure_name = "OTRT77AR_INQ_ORDER_APPT"
s_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		s_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Fill(Char(0),20000)

//Do
//	li_rtn = otrt77ar_inq_order_appointments(	s_error_info, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhandle)
//	as_output_string += ls_output
//Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0

ls_app_name = s_error_info.se_app_name
ls_window_name = s_error_info.se_window_name
ls_function_name = s_error_info.se_function_name
ls_event_name = s_error_info.se_event_name
ls_procedure_name = s_error_info.se_procedure_name
ls_user_id = s_error_info.se_user_id
ls_return_code = s_error_info.se_return_code
ls_message = s_error_info.se_message

nf_set_s_error ( s_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, s_error_info, ii_commhandle)

Return li_rtn

end function

public function integer nf_otrt78ar_upd_order_appt (ref s_error s_error_info, string as_input_header_string, string as_input_detail_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_ret
								
String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id, &
				ls_return_code, &
				ls_message

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

s_error_info.se_app_name = "OTR"
s_error_info.se_function_name = "wf_update"
s_error_info.se_procedure_name = "otrt78ar_upd_order_appt"
s_error_info.se_window_name = "w_review_queue"
s_error_info.se_message = Space(70)
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

nf_get_s_error_values (s_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

//li_ret = otrt78ar_upd_order_appointments( &
//								s_error_info, &
//								as_input_header_string, &
//								as_input_detail_string, &
//								as_output_string, &
//								ld_task_number,&
//								ld_last_record_number,& 
//								ld_max_record_number,&
//								ii_commhandle)

ls_app_name = s_error_info.se_app_name
ls_window_name = s_error_info.se_window_name
ls_function_name = s_error_info.se_function_name
ls_event_name = s_error_info.se_event_name
ls_procedure_name = s_error_info.se_procedure_name
ls_user_id = s_error_info.se_user_id
ls_return_code = s_error_info.se_return_code
ls_message = s_error_info.se_message

nf_set_s_error ( s_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
					
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_ret, s_error_info, ii_commhandle)

Return li_ret

end function

event constructor;call super::constructor;
//get the commhandle to be used for the windows
ii_commhandle = SQLCA.nf_GetCommHandle("otr006")

If ii_commhandle = 0 Then
	// an error occured
	Message.ReturnValue = -1
End if


end event

on u_otr006.create
call super::create
end on

on u_otr006.destroy
call super::destroy
end on

