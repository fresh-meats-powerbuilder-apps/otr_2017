HA$PBExportHeader$u_pfm001.sru
forward
global type u_pfm001 from u_netwise_transaction
end type
end forward

type s_tutlgrup from structure
    character se_user_id[8]
    character se_group_id[3]
    character se_job_id[3]
    character se_last_update_time[26]
    character se_system_id[3]
end type

type s_tutlloc_short from structure
    character se_location_code[3]
    character se_complex_code[3]
    character se_location_type
    string se_name
    character se_std_point_loc_code[9]
end type

type s_tutlprof from structure
    character se_group_id[3]
    character se_window_name[8]
    character se_appl_id[3]
    character se_inquire_auth
    character se_add_auth
    character se_modify_auth
    character se_delete_auth
    character se_last_update_time[26]
    character se_last_update_userid[8]
end type

global type u_pfm001 from u_netwise_transaction
end type
global u_pfm001 u_pfm001

type prototypes
// PowerBuilder Script File: c:\ibp\src\pfm001.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Mon Aug 17 08:59:52 1998
// Source Interface File: j:\pb\test\src32\pfm001.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: pfmf03ar_inq_object
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pfmf03ar_inq_object( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string output_string, &
    int CommHnd &
) library "pfm001.dll" alias for "pfmf03ar_inq_object;Ansi"


//
// Declaration for procedure: pfmf04ar_prod_grp_maint_tree
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pfmf04ar_prod_grp_maint_tree( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char process_option, &
    string header_string, &
    string products_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string output_string, &
    int CommHnd &
) library "pfm001.dll" alias for "pfmf04ar_prod_grp_maint_tree;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "pfm001.dll"
function int WBpfm001CkCompleted( int CommHnd ) &
    library "pfm001.dll"
//
// ***********************************************************


end prototypes

type variables
int 		ii_pfm001_commhandle
ConnectionInfo	ici_Myinf[]
Environment	ienv_Environment

end variables

forward prototypes
public function integer nf_pfmf03ar (ref s_error astr_error_info, string as_input, ref string as_output)
public function integer nf_pfmf04ar (ref s_error astr_error_info, character ac_process_option, string as_header, string as_products, ref string as_output)
end prototypes

public function integer nf_pfmf03ar (ref s_error astr_error_info, string as_input, ref string as_output);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_rtn
String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
Time			lt_starttime, lt_endtime
	
SetPointer(HourGlass!)

as_output = ''

astr_error_info.se_procedure_name = "pfmf03ar_inq_object"
astr_error_info.se_message = Space(71)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = FillA(CharA(0),20000)

Do
	lt_starttime = Now()

	li_rtn = pfmf03ar_inq_object(ls_app_name, &
											ls_window_name,&
											ls_function_name,&
											ls_event_name, &
											ls_procedure_name, &
											ls_user_id, &
											ls_return_code, &
											ls_message, &
											as_input, &
											ld_task_number, &
											ld_last_record_number, &
											ld_max_record_number, &
											ls_output, &
											ii_pfm001_commhandle)

	as_output += ls_output
	lt_endtime = Now()
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pfm001_commhandle)

Return li_rtn
// // //
end function

public function integer nf_pfmf04ar (ref s_error astr_error_info, character ac_process_option, string as_header, string as_products, ref string as_output);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_rtn
String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
Time			lt_starttime, lt_endtime
	
SetPointer(HourGlass!)

as_output = ''

astr_error_info.se_procedure_name = "pfmf04ar_prod_grp_maint_tree"
astr_error_info.se_message = Space(71)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = FillA(CharA(0),20000)

Do
	lt_starttime = Now()

	li_rtn = pfmf04ar_prod_grp_maint_tree(ls_app_name, &
													ls_window_name,&
													ls_function_name,&
													ls_event_name, &
													ls_procedure_name, &
													ls_user_id, &
													ls_return_code, &
													ls_message, &
													ac_process_option, &
													as_header, &
													as_products, &
													ld_task_number, &
													ld_last_record_number, &
													ld_max_record_number, &
													ls_output, &
													ii_pfm001_commhandle)

	as_output += ls_output
	lt_endtime = Now()
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pfm001_commhandle)

Return li_rtn
// // //
end function

event constructor;call super::constructor;// Get the CommHandle to be used for windows

//gw_netwise_frame.SetMicroHelp( "Opening MainFrame Connections")

ii_pfm001_commhandle = sqlca.nf_getcommhandle("pfm001")

//gw_netwise_frame.SetMicroHelp( "Ready")

IF ii_pfm001_commhandle < 0 Then
   message.ReturnValue = -1
END IF
GetEnvironment(ienv_environment)

end event

on u_pfm001.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_pfm001.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

