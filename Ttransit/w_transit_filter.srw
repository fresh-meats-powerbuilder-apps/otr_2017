HA$PBExportHeader$w_transit_filter.srw
$PBExportComments$Window contains plant code, division code, and complex code dddw.
forward
global type w_transit_filter from w_netwise_response
end type
type dw_1 from u_netwise_dw within w_transit_filter
end type
end forward

global type w_transit_filter from w_netwise_response
integer x = 1075
integer y = 485
integer width = 1577
integer height = 664
string title = "Transit Exception Filter"
long backcolor = 12632256
dw_1 dw_1
end type
global w_transit_filter w_transit_filter

type variables
DataWindowChild	idwc_plant_code, &
		idwc_division_code, &
		idwc_complex_code

nv_transit_info	inv_transit_info
end variables

forward prototypes
private function boolean wf_valid_filter_data (string as_complex_code, string as_plant_code, string as_division_code)
end prototypes

private function boolean wf_valid_filter_data (string as_complex_code, string as_plant_code, string as_division_code);// Name:		wf_valid_filter_data
// Purpose:	To validate data entered in the w_transit_filter window dddws.
// Input:	complex code, plant code, and division code to validate.
// Output:	Boolean - True if dddw data is valid, False otherwise.
// History:	08/28/1997 ibdkkam Initial development.

Long		ll_row

ll_row = idwc_complex_code.Find ( "complex_code = '" + as_complex_code + "'", 1, idwc_complex_code.RowCount( ) )
If ( ll_row < 1 ) Then
	MessageBox( "Transit Exception Inquiry", "Complex code " + as_complex_code + " is not a valid code" )
	dw_1.SetColumn( "complex_code" )
	dw_1.SetFocus( )
	Return( False )
End If

ll_row = idwc_plant_code.Find ( "location_code = '" + as_plant_code + "'", 1, idwc_plant_code.RowCount( ) )
If ( ll_row < 1 ) Then
	MessageBox( "Transit Exception Inquiry", "Plant code " + as_plant_code + " is not a valid code" )
	dw_1.SetColumn( "plant_code" )
	dw_1.SetFocus( )
	Return( False )
End If

ll_row = idwc_division_code.Find ( "type_code = '" + as_division_code + "'", 1, idwc_division_code.RowCount( ) )
If ( ll_row < 1 ) Then
	MessageBox( "Transit Exception Inquiry", "Division code " + as_division_code + " is not a valid code" )
	dw_1.SetColumn( "plant_code" )
	dw_1.SetFocus( )
	Return( False )
End If

// If we get here, all data is valid.
Return( True )
end function

on w_transit_filter.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_transit_filter.destroy
call super::destroy
destroy(this.dw_1)
end on

event ue_base_cancel;call super::ue_base_cancel;inv_transit_info.ic_cancel = "T" // T = True, cancel button was pressed.
CloseWithReturn( This, inv_transit_info )
end event

event ue_base_ok;String 	ls_complex_code, ls_plant_code, ls_division_code
Long		ll_row

If dw_1.AcceptText( ) <> 1 Then
	Return
End If

ls_complex_code = dw_1.GetItemString( 1, "complex_code" )
ls_plant_code = dw_1.GetItemString( 1, "plant_code" )
ls_division_code = dw_1.GetItemString( 1, "division_code" )

Choose Case inv_transit_info.is_app_id
	Case "OTR"
		
		If ( Len( Trim( ls_complex_code ) )= 0 ) OR ( IsNull( ls_complex_code ) ) Then
			MessageBox( "Transit Exception Inquiry", "Complex Code cannot be empty." )
			dw_1.SetColumn( "complex_code" )
			dw_1.SetFocus( )
			Return
		End If	
		
		ll_row = idwc_complex_code.Find ( "complex_code = '" + ls_complex_code + "'", 1, idwc_complex_code.RowCount( ) )
		If ( ll_row < 1 ) Then
			MessageBox( "Transit Exception Inquiry", "Complex Code " + ls_complex_code + " is not a valid code." )
			dw_1.SetColumn( "complex_code" )
			dw_1.SetFocus( )
			Return
		End if
		
		If ( Len( Trim( ls_plant_code ) ) <> 0 ) AND ( Not IsNull( ls_plant_code ) ) Then
			ll_row = idwc_plant_code.Find ( "location_code = '" + ls_plant_code + "'", 1, idwc_plant_code.RowCount( ) )
			If ( ll_row < 1 ) Then
				MessageBox( "Transit Exception Inquiry", "Plant Code " + ls_plant_code + " is not a valid code." )
				dw_1.SetColumn( "plant_code" )
				dw_1.SetFocus( )
				Return
			End If
		End If

		If ( Len( Trim( ls_division_code ) ) <> 0 ) AND ( Not IsNull( ls_division_code ) ) Then
			ll_row = idwc_division_code.Find ( "type_code = '" + ls_division_code + "'", 1, idwc_division_code.RowCount( ) )
			If ( ll_row < 1 ) Then
				MessageBox( "Transit Exception Inquiry", "Division Code " + ls_division_code + " is not a valid code." )
				dw_1.SetColumn( "division_code" )
				dw_1.SetFocus( )
				Return	
			ElseIf ( Len( Trim( ls_plant_code ) ) = 0 ) OR ( IsNull( ls_plant_code ) ) Then
				MessageBox( "Transit Exception Inquiry", "Plant Code must be selected before Division Code." )
				dw_1.SetColumn( "plant_code" )
				dw_1.SetFocus( )
				Return
			End If
		End If
		
	Case "PA"

		If ( Len( Trim( ls_plant_code ) ) = 0 ) OR ( IsNull( ls_plant_code ) ) Then
			MessageBox( "Transit Exception Inquiry", "Plant Code cannot be empty." )
			dw_1.SetColumn( "plant_code" )
			dw_1.SetFocus( )
			Return
		End If	
		
		ll_row = idwc_plant_code.Find ( "location_code = '" + ls_plant_code + "'", 1, idwc_plant_code.RowCount( ) )
		If ( ll_row < 1 ) Then
			MessageBox( "Transit Exception Inquiry", "Plant Code " + ls_plant_code + " is not a valid code." )
			dw_1.SetColumn( "plant_code" )
			dw_1.SetFocus( )
			Return
		End If
		
		If ( Len( Trim( ls_complex_code ) ) <> 0 ) AND ( Not IsNull( ls_complex_code ) ) Then
			ll_row = idwc_complex_code.Find ( "complex_code = '" + ls_complex_code + "'", 1, idwc_complex_code.RowCount( ) )
			If ( ll_row < 1 ) Then
				MessageBox( "Transit Exception Inquiry", "Complex Code " + ls_complex_code + " is not a valid code." )
				dw_1.SetColumn( "complex_code" )
				dw_1.SetFocus( )
				Return
			End if
		End If
		
		If ( Len( Trim( ls_division_code ) ) <> 0 ) AND ( Not IsNull( ls_division_code ) ) Then
			ll_row = idwc_division_code.Find ( "type_code = '" + ls_division_code + "'", 1, idwc_division_code.RowCount( ) )
			If ( ll_row < 1 ) Then
				MessageBox( "Transit Exception Inquiry", "Division Code " + ls_division_code + " is not a valid code." )
				dw_1.SetColumn( "division_code" )
				dw_1.SetFocus( )
				Return
			End If
		End If
		
End Choose

inv_transit_info.is_complex_code = ls_complex_code
inv_transit_info.is_plant_code = ls_plant_code
inv_transit_info.is_division_code = ls_division_code
inv_transit_info.ic_cancel = "F" // F = False, cancel button was not selected.
Message.PowerObjectParm = inv_transit_info

CloseWithReturn( This, inv_transit_info )
end event

event ue_postopen(unsignedlong wparam, long lparam);call super::ue_postopen;Long 		ll_rows
Integer	li_rc
String	ls_complex

dw_1.InsertRow( 0 )

inv_transit_info = Message.PowerObjectParm

li_rc = dw_1.GetChild( 'complex_code', idwc_complex_code )
If li_rc = -1 Then MessageBox("DataWindow Child Error", "Unable to get Child Handle for Complex Code !")

li_rc = idwc_complex_code.SetTrans( SQLCA )
ll_rows = idwc_complex_code.Retrieve( )
			
If ( Len( Trim( inv_transit_info.is_complex_code) ) = 0 ) OR ( IsNull( inv_transit_info.is_complex_code ) ) Then
	inv_transit_info.is_complex_code = ProfileString( iw_frame.is_userini, &
		"System Settings", "Default_Complex", inv_transit_info.is_complex_code )
	inv_transit_info.is_complex_code = Left( inv_transit_info.is_complex_code, 3 )
End If
dw_1.SetItem( 1, "complex_code", inv_transit_info.is_complex_code )
			
li_rc = dw_1.GetChild( 'plant_code', idwc_plant_code )
IF li_rc = -1 THEN MessageBox("DataWindow Child Error", "Unable to get Child Handle for Product Code !")

idwc_plant_code.SetTrans( SQLCA )
idwc_plant_code.Retrieve( )

dw_1.SetItem( 1, "plant_code", inv_transit_info.is_plant_code )

li_rc = dw_1.GetChild( 'division_code', idwc_division_code )
If li_rc = -1 Then MessageBox("DataWindow Child Error", "Unable to get Child Handle for Division Code !")

idwc_division_code.SetTrans( SQLCA )
idwc_division_code.Retrieve('DIVCODE')
			
dw_1.SetItem( 1, "division_code", inv_transit_info.is_division_code )

dw_1.SetColumn( "complex_code" )
dw_1.SelectText( 1, 99 )
end event

event closequery;call super::closequery;Message.PowerObjectParm = inv_transit_info
end event

type cb_base_help from w_netwise_response`cb_base_help within w_transit_filter
integer x = 905
integer y = 360
integer taborder = 40
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_transit_filter
integer x = 576
integer y = 360
integer taborder = 30
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_transit_filter
integer x = 261
integer y = 360
integer taborder = 20
end type

type dw_1 from u_netwise_dw within w_transit_filter
integer x = 128
integer y = 108
integer width = 1239
integer height = 188
integer taborder = 10
string dataobject = "d_transit_filter"
boolean border = false
end type

event itemchanged;call super::itemchanged;Long 		ll_rows, ll_found_row, ll_rc = 0
String 	ls_column, ls_complex_code, ls_plant_code

Choose Case dwo.Name
		
	Case "complex_code"
		
			// Clear plant code and division code when complex code is entered.
			dw_1.Object.plant_code[ Row ] = ""
			dw_1.Object.division_code[ Row ] = ""
			ls_column = "plant_code"

	Case "plant_code"
		
			Connect Using SQLCA;
		
			// Populate locations table complex code that corresponds to the plant code entered.
			Select locations.complex_code
				Into :ls_complex_code
				From locations
				Where location_code = :Data ;
				
			Disconnect Using SQLCA;
			
			dw_1.Object.complex_code[ Row ] = ls_complex_code
			ls_column = "division_code"

End Choose

If ( Not( IsNull( ls_column ) ) ) AND ( Len( Trim( ls_column ) ) > 0 ) Then
	dw_1.SetColumn( ls_column )
	dw_1.SetFocus( )
End If

Return( ll_rc )
		

end event

event itemerror;// Override ancestor and reject the data value with no message box.

Return( 1 )
end event

