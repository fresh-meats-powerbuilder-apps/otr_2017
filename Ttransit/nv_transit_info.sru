HA$PBExportHeader$nv_transit_info.sru
$PBExportComments$Non visual user object contains data passed between windows for the Transit Exception.
forward
global type nv_transit_info from nonvisualobject
end type
end forward

global type nv_transit_info from nonvisualobject autoinstantiate
end type

type variables
Char	ic_cancel

String	is_plant_code, &
	is_division_code, &
	is_complex_code, &
	is_app_id

end variables

on nv_transit_info.create
TriggerEvent( this, "constructor" )
end on

on nv_transit_info.destroy
TriggerEvent( this, "destructor" )
end on

