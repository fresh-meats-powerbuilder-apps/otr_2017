HA$PBExportHeader$w_transit.srw
$PBExportComments$Window contains Transit Exception Summary and Detail Tabs.
forward
global type w_transit from w_netwise_sheet
end type
type tab_1 from tab within w_transit
end type
type tabpage_summary from userobject within tab_1
end type
type dw_1 from u_netwise_dw within tabpage_summary
end type
type tabpage_summary from userobject within tab_1
dw_1 dw_1
end type
type tab_1 from tab within w_transit
tabpage_summary tabpage_summary
end type
end forward

global type w_transit from w_netwise_sheet
integer width = 2839
integer height = 1576
string title = "Transit Exception"
long backcolor = 12632256
tab_1 tab_1
end type
global w_transit w_transit

type variables
nv_transit_info	inv_transit_info

u_otr005		iu_otr005

DataWindowChild	idwc_apply_indicator
end variables

forward prototypes
public function boolean wf_retrieve ()
private function integer wf_changed ()
private function boolean wf_update ()
private function boolean wf_valid_data (long al_row)
end prototypes

public function boolean wf_retrieve ();Long 		ii, ll_rows
String	ls_load_key, ls_stop_code, ls_sales_order, ls_inquiry_string, ls_time
Integer	li_rtn, li_rc, li_mb
S_error	lstr_error_Info
Date		ld_date, ld_ship_date
Boolean	lb_rc = True

// Set ic_cancel = U for Undefined.  Cancel button has not been pressed.
inv_transit_info.ic_cancel = "U"
OpenWithParm( w_transit_filter, inv_transit_info, iw_frame )
inv_transit_info = Message.PowerObjectParm

// If cancel button was selected ( ic_cancel = T ), or cancel button was not pressed ( ic_cancel = U )
If ( inv_transit_info.ic_cancel = "T" ) Or ( inv_transit_info.ic_cancel = "U" ) Then
	If ( tab_1.tabpage_summary.dw_1.RowCount( ) = 1 ) Then
		If ( tab_1.tabpage_summary.dw_1.GetItemStatus( 1, 0, Primary! ) = New! ) Then
			Close( This )
			Return( False )
		End If
	End If
	Return( False )
End If

SetPointer( HourGlass! )
tab_1.tabpage_summary.dw_1.SetReDraw( False )

// Determine if dw data has changed since the retrieve.
li_rc = wf_changed( )

Choose Case li_rc
	Case 1 // Yes, changes were made to dw
		
		li_mb = MessageBox( "wf_retrieve", "Save modified data ?", Question!, YesNoCancel! )
		
		Choose Case li_mb
			Case 1 // Yes, save changes and then retrieve data.
				
				If ( wf_update( ) = False ) Then // Unable to save changes.
					
					li_mb = MessageBox( "wf_retrieve", "Unable to save changes, retrieve data anyway ?", &
						Question!, YesNo! )
					
					Choose Case li_mb
						Case 2 // No, do not retrieve without saving changes.
							lb_rc = False
					End Choose
					
				End If // wf_update
				
			// Case 2 // No, do not save changed before retrieving data.
			Case 3 // Cancel the retrieve
				lb_rc = False
		End Choose
		
	// Case 0 // No modifications were made to dw, go ahead and retrieve.
	
End Choose

Choose Case lb_rc
	Case False
		SetPointer( Arrow! )
		tab_1.tabpage_summary.dw_1.SetReDraw( True )
		Return( lb_rc )
End Choose
		
iw_frame.SetMicroHelp( "Retrieving..." )

// Retrieve data from mainframe

tab_1.tabpage_summary.dw_1.Reset( )

// Assign for RPC error checking
lstr_error_info.se_event_name = "wf_retrieve"
lstr_error_info.se_window_name = "w_transit"
lstr_error_info.se_procedure_name = "wf_retrieve"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ls_load_key 		= Space( 7 )
ls_stop_code 		= Space( 2 )
ls_sales_order		= Space( 7 )
ls_inquiry_string = Space( 30000 )
 
Do
//	li_rtn = iu_otr005.nf_otrt59ar( inv_transit_info.is_complex_code, inv_transit_info.is_plant_code, &
//			inv_transit_info.is_division_code, ls_load_key, ls_stop_code, ls_sales_order, &
//			ls_inquiry_string, lstr_error_info, 0 )
			
	Choose Case li_rtn
		Case 0
			iw_frame.SetMicroHelp( "Ready...")
		Case 1
			iw_frame.SetMicroHelp(lstr_error_info.se_message)
		Case Else
			iw_frame.SetMicroHelp(lstr_error_info.se_message)
			SetPointer( Arrow! )
			tab_1.tabpage_summary.dw_1.SetReDraw( True )
			Return( False )
	End Choose
		
	li_rtn = tab_1.tabpage_summary.dw_1.ImportString( Trim( ls_inquiry_string ) )
	
Loop While ( Len( Trim( ls_load_key ) ) > 0 )

tab_1.tabpage_summary.dw_1.ScrollToRow( 1 ) 
tab_1.tabpage_summary.dw_1.ResetUpdate( )

// Since there's only 1 tab ( Summary ), set the cursor in the 1st column of the 1st row.
tab_1.tabpage_summary.dw_1.SetColumn( "apply_indicator" )
tab_1.tabpage_summary.dw_1.Setfocus( )

// Add complex_code to window title.
This.Title = "Transit Exception - " + inv_transit_info.is_complex_code

SetPointer( Arrow! )
tab_1.tabpage_summary.dw_1.SetReDraw( True )

Return( True )
end function

private function integer wf_changed ();// Purpose:  	To examine the summary window dw_1 and determine if any data has changed.
// Input(s):	None
// Output(s):	1 if change has been made, 0 otherwise.



If tab_1.tabpage_summary.dw_1.ModifiedCount() + &
	tab_1.tabpage_summary.dw_1.DeletedCount() > 0 Then
//	is_Closemessage_txt = "Save Changes?"
	Return( 1 )
End If



// Nothing has been changed
Return( 0 )
end function

private function boolean wf_update ();Long 		ll_row
String 	ls_load_key, ls_stop_code, ls_sales_order, ls_apply_ind, ls_delivery_date, &
			ls_delivery_time, ls_delivery_contact, ls_estimated_load_date, ls_estimated_load_time, &
			ls_return_error_msg, ls_apply_comment
s_error	lstr_error_info
Integer	li_return_error_code, li_rtn

If ( tab_1.tabpage_summary.dw_1.AcceptText( ) = -1 ) Then
	Return( False )
End if

SetPointer(HourGlass!)
tab_1.tabpage_summary.dw_1.SetRedraw( False )

ll_row = tab_1.tabpage_summary.dw_1.GetNextModified( 0, Primary! )

If ( ll_row = 0 ) Then
	iw_frame.SetMicroHelp("No Modified Data.")
	tab_1.tabpage_summary.dw_1.SetRedraw( True )
	SetPointer( Arrow! )
	Return( True )
End If

Do While ll_row <> 0
	
	// Validate data.
	If ( wf_valid_data( ll_row ) = True ) Then
	
		ls_load_key 				= tab_1.tabpage_summary.dw_1.Object.load_key[ ll_row ]
		ls_stop_code 				= tab_1.tabpage_summary.dw_1.Object.stop_code[ ll_row ]
		ls_sales_order 			= tab_1.tabpage_summary.dw_1.Object.order_number[ ll_row ]
		ls_delivery_date 			= String( tab_1.tabpage_summary.dw_1.Object.delivery_date[ ll_row ], "mm/dd/yyyy" )
		ls_delivery_time 			= String( tab_1.tabpage_summary.dw_1.Object.delivery_time[ ll_row ], "hh:mm" )
		ls_delivery_contact 		= tab_1.tabpage_summary.dw_1.Object.delivery_contact[ ll_row ]
		ls_estimated_load_date 	= String( tab_1.tabpage_summary.dw_1.Object.estimated_load_date[ ll_row ], "mm/dd/yyyy" )
		ls_estimated_load_time 	= String( tab_1.tabpage_summary.dw_1.Object.estimated_load_time[ ll_row ], "hh:mm" )
		ls_apply_comment			= tab_1.tabpage_summary.dw_1.Object.apply_comment[ ll_row ]
		
		Choose Case inv_transit_info.is_app_id
			Case "OTR" 
				ls_apply_ind 		= tab_1.tabpage_summary.dw_1.Object.apply_indicator[ ll_row ]
			Case Else 
				ls_apply_ind 		= "N"
		End Choose
		
		If ( ls_delivery_date = "" ) OR ( IsNull( ls_delivery_date ) ) Then
			ls_delivery_date = "          "
		End If
		
		If ( ls_estimated_load_date = "" ) OR ( IsNull( ls_estimated_load_date ) ) Then
			ls_estimated_load_date = "          "
		End If
		
		If ( IsNull( ls_apply_comment ) ) Then 
			ls_apply_comment = ""
		End If
	
		// Assign for RPC error checking
		lstr_error_info.se_event_name = "wf_update"
		lstr_error_info.se_window_name = "w_transit"
		lstr_error_info.se_procedure_name = "wf_update"
		lstr_error_info.se_user_id = SQLCA.Userid
		lstr_error_info.se_message = Space(71)

//		li_rtn = iu_otr005.nf_otrt60ar( ls_load_key, ls_stop_code, ls_sales_order, ls_apply_ind, &
//			ls_delivery_date, ls_delivery_time, ls_delivery_contact, ls_estimated_load_date, &
//			ls_estimated_load_time, ls_apply_comment, lstr_error_info, 0 )
			
		// If Apply Indicator is Delete or Apply, delete this row from the Primary! buffer.
		If li_rtn = 0 Then
			If ( ls_apply_ind = "U" ) Then
				tab_1.tabpage_summary.dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
			Else	
				tab_1.tabpage_summary.dw_1.DeleteRow( ll_row )
				ll_row --
			End If
		End If
	
	Else // Ran accross some invalid data.
		tab_1.tabpage_summary.dw_1.SetRedraw( True )
		SetPointer( Arrow! )
		Return( True )
	End If
	
	ll_row = tab_1.tabpage_summary.dw_1.GetNextModified( ll_row, Primary! )
Loop

SetMicroHelp( "Update processing complete." )

tab_1.tabpage_summary.dw_1.SetRedraw( True )
SetPointer( Arrow! )
	
Return( True )
end function

private function boolean wf_valid_data (long al_row);// Name:			wf_valid_data
// Purpose:		To validate data on the Transit Exception Summary data window.
// Input(s):	al_row	Row that contains data to validate.
// Output(s):	Boolean  True if datawindow data is valid, False otherwise.
// Called by:	wf_update
//	09-02-1997	ibdkkam  Initial creation

Boolean	lb_rc = True
Date		ld_date, ld_ship_date
String	ls_apply_ind, ls_estimated_load_time, ls_title = "Data Entry Error", ls_col_name

ld_ship_date = tab_1.tabpage_summary.dw_1.Object.ship_date[ al_row ]

ld_date = tab_1.tabpage_summary.dw_1.Object.delivery_date[ al_row ]
If ( ld_date < ld_ship_date ) Then
	MessageBox( ls_title, "Delivery date must be >= shipping date.", Exclamation!, OK! )
	lb_rc = False
	ls_col_name = "delivery_date"
End If

ld_date = tab_1.tabpage_summary.dw_1.Object.estimated_load_date[ al_row ]
If ( ld_date < ld_ship_date ) Then
	MessageBox( ls_title, "Estimated load date must be >= shipping date.", Exclamation!, OK! )
	lb_rc = False
	ls_col_name = "estimated_load_date"
End If	

ls_apply_ind = tab_1.tabpage_summary.dw_1.Object.apply_indicator[ al_row ]
If ( Len( Trim( ls_apply_ind ) ) = 0 ) OR ( IsNull( ls_apply_ind ) ) OR ( Match( ls_apply_ind, "[AUD]" ) = False ) Then
	MessageBox( ls_title, "Apply indicator is required and must be 'A', 'U', or 'D'." )
	lb_rc = False
	ls_col_name = "apply_indicator"
End If

If ( ls_apply_ind = "A" ) Then
	If ( tab_1.tabpage_summary.dw_1.Object.apply_comment[ al_row ] = "" ) Or &
		IsNull( tab_1.tabpage_summary.dw_1.Object.apply_comment[ al_row ] ) Then	
		MessageBox( ls_title, "Comment must be entered when apply indicator is 'A'." )
		ls_col_name = "apply_comment"
		lb_rc = False
	End If
End If

If ( lb_rc = False ) Then
	tab_1.tabpage_summary.dw_1.SetRow( al_row )
	tab_1.tabpage_summary.dw_1.ScrollToRow( al_row )
	tab_1.tabpage_summary.dw_1.SetColumn( ls_col_name )
	tab_1.tabpage_summary.dw_1.SetFocus( )
	tab_1.tabpage_summary.dw_1.SelectText( 1, 99 )
End If

Return( lb_rc )

end function

on w_transit.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_transit.destroy
call super::destroy
destroy(this.tab_1)
end on

event ue_postopen;call super::ue_postopen;// Create object that contains mainframe function calls.
iu_otr005  =  Create u_otr005

// Set instance application id value (used by w_transit and w_transit_filter).
inv_transit_info.is_app_id = Message.nf_get_app_id( )

If ( This.wf_retrieve( ) = False ) Then Return

// Disable any menu items here.

// If this is not the OTR application running, protect and grey 
// apply_indicator, delivery_date, delivery time, and delivery contact.
If ( inv_transit_info.is_app_id <> "OTR" ) Then
	tab_1.tabpage_summary.dw_1.Object.apply_indicator.background.color 	= 12632256
	tab_1.tabpage_summary.dw_1.Object.apply_indicator.protect 				= 1
	tab_1.tabpage_summary.dw_1.Object.delivery_date.background.color 		= 12632256
	tab_1.tabpage_summary.dw_1.Object.delivery_date.protect 					= 1
	tab_1.tabpage_summary.dw_1.Object.delivery_time.background.color 		= 12632256
	tab_1.tabpage_summary.dw_1.Object.delivery_time.protect 					= 1
	tab_1.tabpage_summary.dw_1.Object.delivery_contact.background.color 	= 12632256
	tab_1.tabpage_summary.dw_1.Object.delivery_contact.protect 				= 1
	tab_1.tabpage_summary.dw_1.Object.apply_comment.background.color		= 12632256
	tab_1.tabpage_summary.dw_1.Object.apply_comment.protect					= 1
End If
end event

event ue_fileprint;call super::ue_fileprint;tab_1.tabpage_summary.dw_1.Print( )


end event

event open;call super::open;// Always reposition window to upper left corner.
This.X = 1
This.Y = 1
end event

event close;call super::close;Destroy iu_otr005
end event

type tab_1 from tab within w_transit
integer width = 2798
integer height = 1468
integer taborder = 1
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
boolean boldselectedtext = true
integer selectedtab = 1
tabpage_summary tabpage_summary
end type

on tab_1.create
this.tabpage_summary=create tabpage_summary
this.Control[]={this.tabpage_summary}
end on

on tab_1.destroy
destroy(this.tabpage_summary)
end on

type tabpage_summary from userobject within tab_1
integer x = 18
integer y = 112
integer width = 2761
integer height = 1340
long backcolor = 12632256
string text = "Summary"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_1 dw_1
end type

on tabpage_summary.create
this.dw_1=create dw_1
this.Control[]={this.dw_1}
end on

on tabpage_summary.destroy
destroy(this.dw_1)
end on

type dw_1 from u_netwise_dw within tabpage_summary
integer y = 4
integer width = 2757
integer height = 1324
integer taborder = 2
string dataobject = "d_transit_summary"
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;This.SetTrans( SQLCA )

This.InsertRow( 0 )

ib_Updateable 	= True

end event

event itemchanged;call super::itemchanged;Date		ld_date, ld_ship_date
String 	ls_title = "Data Entry Error"
Long		ll_rc = 0 // Assume we will accept the data value, if any error occur, return code is = 1.

Choose Case dwo.Name
			
	Case "apply_indicator"
			
		If ( Len( Trim( Data ) ) = 0 ) OR ( IsNull( Data ) ) OR ( Match( Data, "[AUD]" ) = False ) Then
			MessageBox( ls_title, "Apply indicator is required and must be 'A', 'U', or 'D'." )
			ll_rc = 1
		End If
		
	Case "apply_comment"
		
		If ( tab_1.tabpage_summary.dw_1.Object.apply_indicator[ Row ] = "A" ) Then
			If ( Data = "" ) Or IsNull( Data ) Then	
				MessageBox( ls_title, "Comment must be entered when apply indicator is 'A'." )
				ll_rc = 1
			End If
		End If
		
	Case "delivery_date", "estimated_load_date"
		
		ld_ship_date = tab_1.tabpage_summary.dw_1.Object.ship_date[ Row ]
		ld_date = Date( Data )
		
		If ( ld_date < ld_ship_date ) Then
			MessageBox( ls_title, "Date must be >= ship date." )
			ll_rc = 1
		End If
		
End Choose

If ( ll_rc = 1 ) Then
	This.SetColumn( is_ColumnName )
	This.SetFocus( )
	This.SelectText( 1,99 )
End If

Return( ll_rc )

	
end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText( 1,99 )
end event

event doubleclicked;call super::doubleclicked;String ls_order
w_load_detail lw_load_detail

Choose Case dwo.Name
	Case "load_key"
		ls_order = GetItemString( Row, "order_number" )
		OpenSheetWithParm( lw_load_detail, ls_order, iw_frame, 0, Original! )
End Choose

end event

event itemerror;// Override ancestor.  Return 1 to reject the data value with no message box.

Return( 1 )
end event

