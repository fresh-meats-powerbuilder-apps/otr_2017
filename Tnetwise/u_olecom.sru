HA$PBExportHeader$u_olecom.sru
forward
global type u_olecom from oleobject
end type
end forward

global type u_olecom from oleobject
end type
global u_olecom u_olecom

event error;String	ls_message


ls_message = String(errornumber) + " " + String(errortext)

MessageBox("u_ole_error",ls_message)

If errornumber = 803 Then 
	SetNull(returnvalue)
	action = ExceptionSubstituteReturnValue! 
End If
end event

event externalexception;String	ls_message


ls_message = String(resultcode) + " " + String(exceptioncode) + " " + String(description)

MessageBox("u_ole_externalexception",ls_message)

end event

on u_olecom.create
call oleobject::create
TriggerEvent( this, "constructor" )
end on

on u_olecom.destroy
call oleobject::destroy
TriggerEvent( this, "destructor" )
end on

