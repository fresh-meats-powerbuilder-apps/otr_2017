HA$PBExportHeader$u_netwise_message.sru
forward
global type u_netwise_message from u_base_message
end type
end forward

global type u_netwise_message from u_base_message
end type
global u_netwise_message u_netwise_message

type variables
//w_utl_data		iw_utldata
end variables

on u_netwise_message.create
call message::create
TriggerEvent( this, "constructor" )
end on

on u_netwise_message.destroy
call message::destroy
TriggerEvent( this, "destructor" )
end on

