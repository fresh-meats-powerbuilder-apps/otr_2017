HA$PBExportHeader$w_message_icons.srw
forward
global type w_message_icons from w_netwise_sheet
end type
type uo_other from u_icons within w_message_icons
end type
type uo_all from u_icons within w_message_icons
end type
type dw_classes from u_netwise_dw within w_message_icons
end type
end forward

global type w_message_icons from w_netwise_sheet
integer x = 32
integer y = 232
integer width = 1093
integer height = 700
string title = "Message Types"
long backcolor = 12632256
uo_other uo_other
uo_all uo_all
dw_classes dw_classes
end type
global w_message_icons w_message_icons

type variables
string is_exception_string, is_user_id, is_allstring

u_utl001  iu_utl001

s_error   istr_error_info



end variables

forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();Int			li_index, &
				li_ControlCount, &
				li_other, &
				li_old, &
				li_new
				
String		ls_msgclass, &
				ls_objectName, &
				ls_message_counttype, &
				ls_count, &
				ls_null
				

long			ll_rowcount, &
				ll_row

u_icons		luo_userobject[]

boolean		lb_return

SetPointer(HourGlass!)

Open(w_messages_inquire, gw_netwise_frame)
SetRedraw(False)
is_exception_string = ""
is_user_id = Message.StringParm

IF IsNull(is_user_id) or is_user_id = "" THEN 
	SetRedraw(True)
	RETURN FALSE
END IF

This.Title = "Messages for " + is_user_id

istr_error_info.se_window_name = "Message Icons"
istr_error_info.se_event_name = "query"
istr_error_info.se_procedure_name = "nf_utlu14ar"
istr_error_info.se_function_name = "wf_retrieve"

If Not Isvalid(iu_utl001) Then
	iu_utl001 = Create u_utl001
End If

ls_message_counttype = Space(1701)


//lb_return = iu_utl001.nf_utlu14ar(istr_error_info, ls_message_counttype, &
//							is_user_id, 0) 

IF lb_return THEN
	If Not gw_netwise_frame.iu_string.nf_IsEmpty(ls_message_counttype) Then
		ls_message_counttype = Trim(ls_message_counttype)
		dw_classes.Reset()
		ll_rowcount = dw_classes.ImportString(ls_message_counttype)
		IF ll_rowcount > 0 then SetMicroHelp(String(ll_rowcount) + &
													" Class Types Retrieved")
	End if
Else
	SetRedraw(True)
	IF istr_error_info.se_message[1] = "" THEN RETURN FALSE
END IF

// Loop through the windows's control array and find all controls.
// Get the total number of controls on this sheet
li_ControlCount = UpperBound(This.Control[])

For li_index = 1 To li_ControlCount
	If TypeOf(This.Control[li_index]) = UserObject! Then
		luo_userobject[li_index] = This.Control[li_index]
		ls_objectname = luo_userobject[li_index].ClassName()
		ls_msgclass = Right(ls_objectname, 	Len(ls_objectname) - &
									(Pos(ls_objectname, "_")))
		IF ls_msgclass = "all" THEN
			IF istr_error_info.se_message[1] <> "N" THEN
				ls_count = "New : " + String(dw_classes.GetItemNumber(&
						1, "all_new")) + "~r~n" + "Old : " + 	&
						String(dw_classes.GetItemNumber(1, "all_old"))
			ELSE
				ls_count = "New : " + "0" + "~r~n" + " Old : " + "0"
			END IF
			luo_userobject[li_index].uf_set_count(ls_count)
			CONTINUE
		END IF

		IF ls_msgclass = "other" THEN 
			li_other = li_index
			CONTINUE
		END IF
		ls_msgclass = Upper(ls_msgclass)
		is_exception_string = is_exception_string + ls_msgclass

		ll_row = dw_classes.Find("class ='" + ls_msgclass + "'", 1, ll_rowcount)
		IF ll_row > 0 THEN
			ls_count = "New : " + String(0 + dw_classes.GetItemNumber(&
						ll_row, "new")) + "~r~n" + "Old : " + 	&
						String(0 + dw_classes.GetItemNumber(ll_row, "old"))
			luo_userobject[li_index].uf_set_count(ls_count)
			dw_classes.SetItem(ll_row, "other", "YES")
		ELSE
			ls_count = "New : " + "0" + "~r~n" + " Old : " + "0"
			luo_userobject[li_index].uf_set_count(ls_count)
		END IF
	End If
Next


FOR li_index = 1 to ll_rowcount
	ls_null = dw_classes.GetItemString(li_index, "other")
	IF IsNull(ls_null) or ls_null = "" THEN
		li_old = li_old + dw_classes.GetItemNumber(li_index, "old")
		li_new = li_new + dw_classes.GetItemNumber(li_index, "new")
	END IF
NEXT	


ls_count = "New : " + String(li_new) + "~r~n" + " Old : " + 	&
						String(li_old)
luo_userobject[li_other].uf_set_count(ls_count)
SetRedraw(true)
dw_classes.ResetUpdate()
is_exception_string = is_exception_string + "~t" + is_user_id
is_allstring = ""+ "~t" + is_user_id
Return True
end function

on close;call w_netwise_sheet::close;IF ISValid( iu_utl001) Then Destroy( iu_utl001)
SetMicroHelp("Ready")
end on

on ue_query;call w_netwise_sheet::ue_query;wf_retrieve()

end on

on ue_postopen;call w_netwise_sheet::ue_postopen;PostEvent("ue_query")

end on

on w_message_icons.create
int iCurrent
call super::create
this.uo_other=create uo_other
this.uo_all=create uo_all
this.dw_classes=create dw_classes
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.uo_other
this.Control[iCurrent+2]=this.uo_all
this.Control[iCurrent+3]=this.dw_classes
end on

on w_message_icons.destroy
call super::destroy
destroy(this.uo_other)
destroy(this.uo_all)
destroy(this.dw_classes)
end on

type uo_other from u_icons within w_message_icons
integer x = 521
integer y = 12
integer width = 425
integer height = 488
integer taborder = 30
boolean border = false
end type

event ue_clicked;call super::ue_clicked;Window	lw_Temp	

SetPointer(HourGlass!)

OpenSheetWithParm(lw_temp, is_exception_string, "w_header_message",  &
								gw_netwise_frame, 0, gw_netwise_frame.im_netwise_menu.iao_arrangeopen)

end event

on constructor;call u_icons::constructor;uf_set_icon("msgs.bmp", "Other")
end on

on uo_other.destroy
call u_icons::destroy
end on

type uo_all from u_icons within w_message_icons
integer x = 114
integer y = 12
integer width = 425
integer height = 488
integer taborder = 20
boolean border = false
end type

event ue_clicked;call super::ue_clicked;Window	lw_Temp	

SetPointer(HourGlass!)

OpenSheetWithParm(lw_temp, is_allstring, "w_header_message", &
								gw_netwise_frame, 0, gw_netwise_frame.im_netwise_menu.iao_arrangeopen)

end event

on constructor;call u_icons::constructor;uf_set_icon("ibptile.bmp", "All")
end on

on uo_all.destroy
call u_icons::destroy
end on

type dw_classes from u_netwise_dw within w_message_icons
boolean visible = false
integer x = 210
integer y = 424
integer width = 795
integer height = 492
string dataobject = "d_message_classes"
end type

