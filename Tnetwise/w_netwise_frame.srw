HA$PBExportHeader$w_netwise_frame.srw
forward
global type w_netwise_frame from w_base_frame
end type
end forward

global type w_netwise_frame from w_base_frame
integer width = 3269
integer height = 1776
string menuname = "m_netwise_menu"
end type
global w_netwise_frame w_netwise_frame

type variables
m_netwise_menu	im_netwise_menu

// handle to utl002.exe
// This is the only tie to it
Long	il_utl002_handle

u_netwise_data	iu_netwise_data



//stores the last message displayed in microhelp
String	is_prev_microhelp
end variables

forward prototypes
public subroutine wf_create_netwisedata ()
public function string wf_getmicrohelp ()
public function integer setmicrohelp (string as_message)
public subroutine wf_login (string as_userini)
end prototypes

public subroutine wf_create_netwisedata ();SetMicroHelp("Loading System Tables...")
iu_netwise_data		=	Create u_netwise_data

end subroutine

public function string wf_getmicrohelp ();return is_prev_microhelp
end function

public function integer setmicrohelp (string as_message);is_prev_microhelp	=	as_message
SUPER::SetMicrohelp(as_message)
RETURN 1

end function

public subroutine wf_login (string as_userini);OpenWithParm(w_netwise_login, as_UserIni)
This.SetRedraw(True)
wf_create_netwisedata()


end subroutine

event ue_postopen;call super::ue_postopen;//gets out of this event if the user hit the cancel on the login window
IF ib_login_cancel THEN RETURN

// Force a redraw
This.SetRedraw(True)

// Set the default Help File
is_Help = ProfileString( is_UserIni, Message.nf_Get_App_ID(), "HelpFile", &
							'f:\software\pb\exe32\' + Message.nf_get_app_id() + '.hlp')

//Set the default for microhelp as "Ready"
ia_application.MicroHelpDefault = "Ready"
This.SetMicroHelp(ia_application.MicroHelpDefault)

// 03/2002 ibdkkam - Removed as part of move to SQL Server
// Create u_netwise_data to download all the tables
// wf_create_netwisedata()

end event

event close;call super::close;Open(w_close_mainframe, This)
Destroy	iu_netwise_data
SQLCA.nf_CloseCommHandle()
Close(w_close_mainframe)
If il_utl002_handle > 0 Then Post(il_utl002_handle, 2, 0, 0)
Destroy iu_base_data
end event

event open;call super::open;gw_netwise_frame = This
im_netwise_menu = This.MenuID
SQLCA.Trigger Event Constructor()
// Populate the Message Object's structure that holds CommHandle stuff
SQLCA.nf_GetServerNames()

end event

on w_netwise_frame.create
call super::create
if IsValid(this.MenuID) then destroy(this.MenuID)
if this.MenuName = "m_netwise_menu" then this.MenuID = create m_netwise_menu
end on

on w_netwise_frame.destroy
call super::destroy
if IsValid(MenuID) then destroy(MenuID)
end on

