HA$PBExportHeader$w_messages_inquire.srw
forward
global type w_messages_inquire from w_base_response
end type
type st_1 from statictext within w_messages_inquire
end type
type dw_userid from u_netwise_dw within w_messages_inquire
end type
end forward

global type w_messages_inquire from w_base_response
integer x = 526
integer y = 344
integer width = 997
integer height = 424
string title = "Inquire For Messages"
long backcolor = 12632256
st_1 st_1
dw_userid dw_userid
end type
global w_messages_inquire w_messages_inquire

type variables
s_error istr_error_info

u_utl001 iu_utl001
end variables

event open;call super::open;istr_error_info.se_user_id = SQLCA.UserID

dw_userid.SetItem(1, "userid", istr_error_info.se_user_id)
end event

on w_messages_inquire.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_userid=create dw_userid
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_userid
end on

on w_messages_inquire.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_userid)
end on

event ue_base_ok;call super::ue_base_ok;CloseWithreturn(This, dw_userid.GetItemString(1, "userid"))
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

type cb_base_help from w_base_response`cb_base_help within w_messages_inquire
integer x = 658
integer y = 168
integer width = 256
integer taborder = 40
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_messages_inquire
integer x = 366
integer y = 168
integer width = 256
integer taborder = 30
end type

type cb_base_ok from w_base_response`cb_base_ok within w_messages_inquire
integer x = 73
integer y = 168
integer width = 256
end type

type st_1 from statictext within w_messages_inquire
integer x = 105
integer y = 40
integer width = 247
integer height = 72
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "User ID"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_userid from u_netwise_dw within w_messages_inquire
event pbm_dwndropdown pbm_dwndropdown
integer x = 361
integer y = 32
integer width = 402
integer height = 96
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_userid"
boolean border = false
end type

event pbm_dwndropdown;long 		ll_rowcount

string	ls_list_users

boolean	lb_return

datawindowchild	ldw_child

u_string_functions	lu_string


istr_error_info.se_window_name = "Message Inquire"
istr_error_info.se_event_name = "dwndropdown"
istr_error_info.se_procedure_name = "nf_utlu15ar"
istr_error_info.se_function_name = ""

dw_userid.GetChild("userid", ldw_child)
ll_rowcount = ldw_child.RowCount()

If Not Isvalid(iu_utl001) Then
	iu_utl001 = Create u_utl001
End If

IF NOT ll_rowcount > 0 THEN
	ls_list_users = Space(20000)
	
//	lb_return = iu_utl001.nf_utlu15ar(istr_error_info, ls_list_users, 0) 

	IF lb_return THEN
		If Not lu_string.nf_IsEmpty(ls_list_users) Then
			ls_list_users = Trim(ls_list_users)
			ll_rowcount = ldw_child.ImportString(ls_list_users)
			IF ll_rowcount > 0 then SetMicroHelp(String(ll_rowcount) + &
													" User ID's Retrieved")
		END IF
	END IF
END IF


end event

on itemchanged;call u_netwise_dw::itemchanged;//dw_userid.SetItem(1, "userid", 
end on

on constructor;call u_netwise_dw::constructor;InsertRow(0)
end on

