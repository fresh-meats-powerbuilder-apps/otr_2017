HA$PBExportHeader$w_message_alias.srw
forward
global type w_message_alias from w_netwise_sheet
end type
type dw_alias from u_netwise_dw within w_message_alias
end type
end forward

global type w_message_alias from w_netwise_sheet
integer width = 2432
integer height = 1200
string title = "Message Alias"
long backcolor = 12632256
dw_alias dw_alias
end type
global w_message_alias w_message_alias

type variables
datawindowchild idwc_child

u_utl001 iu_utl001

s_error istr_error_info

string is_user_id_in
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_deleterow ()
public subroutine wf_delete ()
public function boolean wf_check_required (integer ai_row)
end prototypes

public function boolean wf_retrieve ();string		ls_output_string, &
				ls_text

			
long			ll_rowcount, &
				ll_row, ll_newrow


boolean		lb_return

integer		li_commhandle, &
				li_index

u_string_functions	lu_string


Open(w_messages_inquire, gw_netwise_frame)
SetRedraw(false)
is_user_id_in = Message.StringParm

IF IsNull(is_user_id_in) or is_user_id_in = "" THEN 
	SetRedraw(True)
	RETURN FALSE
END IF

This.Title = "Message Alias For " + is_user_id_in

istr_error_info.se_window_name = "Message Alias"
istr_error_info.se_function_name = "wf_retrieve"

If Not Isvalid(iu_utl001) Then
	iu_utl001 = Create u_utl001
End If

ls_output_string = Space(17501)

//lb_return = iu_utl001.nf_utlu16ar(istr_error_info, is_user_id_in,&
//				ls_output_string, li_commhandle)

ls_output_string = Trim(ls_output_string)


If lb_return Then
	If NOT lu_string.nf_IsEmpty(ls_output_string) Then
		dw_alias.Reset()
		ll_rowcount = dw_alias.ImportString(ls_output_string)
		For li_index = 1 to ll_rowcount
			ls_text = dw_alias.GetItemString(li_index, "alias_type")
			ll_row = idwc_child.Find('type_code = "' + ls_text + '"', 1, idwc_child.RowCount())
			If ll_row > 0 Then
				dw_alias.SetItem(li_index, "type_description", &
						idwc_child.GetItemString(ll_row, "type_desc"))
			END IF
		NEXT
		dw_alias.ResetUpdate()
		ll_newrow = dw_alias.InsertRow(0)
		dw_alias.ScrollToRow(ll_newrow)
	End If
ELSE
	dw_alias.Reset()
	ll_newrow = dw_alias.InsertRow(0)
	dw_alias.ScrollToRow(ll_newrow)
END IF

IF Trim(istr_error_info.se_user_id) <> Trim(is_user_id_in) THEN 
		gw_netwise_frame.im_netwise_menu.m_file.m_delete.disable()
ELSE
		gw_netwise_frame.im_netwise_menu.m_file.m_delete.enable()
END IF

SetRedraw(True)
return true

end function

public function boolean wf_update ();String		ls_input_string

boolean		lb_return

integer		li_commhandle, &
				li_index

long			ll_rowcount

dwItemStatus	l_status


istr_error_info.se_window_name = "Message Alias"
istr_error_info.se_function_name = "wf_update"

If Not Isvalid(iu_utl001) Then
	iu_utl001 = Create u_utl001
End If

ll_rowcount = dw_alias.RowCount()

IF dw_alias.AcceptText() < 1 THEN RETURN FALSE

For li_index = 1 to ll_rowcount
	l_status = dw_alias.GetItemStatus(li_index, 0, Primary!)
	IF l_status = NewModified! THEN
		IF wf_check_required(li_index) THEN
			IF Len(Trim(is_user_id_in)) = 7 THEN is_user_id_in = Trim(is_user_id_in) + " "
			ls_input_string = is_user_id_in + "~t" + "U" + "~t" + &
					Trim(dw_alias.GetItemString(li_index, "alias_type")) + &
					"~t" + Trim(dw_alias.GetItemString(li_index, "alias"))
//			lb_return = iu_utl001.nf_utlu17ar(istr_error_info, ls_input_string, &
//														li_commhandle)
			IF lb_return Then
				SetMicroHelp("Updating ...")	
			ELSE
				IF Trim(istr_error_info.se_return_code) = "803" THEN
					dw_alias.SetColumn("alias")
					dw_alias.ScrollToRow(li_index)
					dw_alias.SetFocus()
				END IF
				RETURN FALSE
			END IF
		ELSE
			RETURN FALSE	
		END IF
	END IF
NEXT

dw_alias.ResetUpdate()
SetMicroHelp("Update Successful")

return true	
end function

public function boolean wf_deleterow ();long		ll_row

Do
		ll_row = dw_alias.GetSelectedRow(0)
		IF ll_row > 0 THEN
			dw_alias.DeleteRow(ll_row)
		End IF
Loop Until ll_row = 0

return true
end function

public subroutine wf_delete ();long		ll_rowcount, &
			li_index, &
			ll_count

string	ls_input_string

integer 	li_commhandle, li_rtn

boolean	lb_return


ll_rowcount = dw_alias.RowCount()

	For li_index = 1 to ll_rowcount
		IF dw_alias.IsSelected(li_index) Then
			ll_count++
		End IF
	NEXT

li_rtn = MessageBox("Delete Messages", "Do you want to delete "&
		+ string(ll_count)+ " row(s) selected", INFORMATION!, YESNO!)

If li_rtn = 2 Then return 

SetRedraw(false)
istr_error_info.se_event_name = "wf_delete"
istr_error_info.se_procedure_name = "nf_utlu11ar"

If Not Isvalid(iu_utl001) Then
	iu_utl001 = Create u_utl001
End If

For li_index = 1 to ll_rowcount
		IF dw_alias.IsSelected(li_index) Then
			IF Len(Trim(is_user_id_in)) = 7 THEN is_user_id_in = &
															Trim(is_user_id_in) + " "
			ls_input_string = is_user_id_in + "~t" + "D" + "~t" + &
					dw_alias.GetItemString(li_index, "alias_type") + &
					"~t" + dw_alias.GetItemString(li_index, "alias")
//			lb_return = iu_utl001.nf_utlu17ar(istr_error_info, ls_input_string, &
//														li_commhandle)
			IF lb_return Then
				SetMicroHelp("Deleting ...")
			ELSE
				SetRedraw(True)
				RETURN 
			End IF
		END IF
	NEXT


IF lb_return Then
	wf_deleteRow()
	dw_alias.ResetUpdate()
	SetMicroHelp(String(ll_count) + " rows deleted ")
End IF

SetMicroHelp("Ready")
SetRedraw(true)
end subroutine

public function boolean wf_check_required (integer ai_row);
string		ls_alias_type, &
				ls_alias


ls_alias_type = dw_alias.GetItemString(ai_row, "alias_type")
IF IsNull(ls_alias_type) or ls_alias_type = "" THEN
	RETURN FALSE
ELSE
	ls_alias = dw_alias.GetItemString(ai_row, "alias")
	IF IsNull(ls_alias) or ls_alias = "" THEN
		// Tell the user which column to fill in.
		MessageBox("Required Value Missing",  "Please enter a value for column'"  &
			+ "Alias" + "', in Row " + String(ai_row) + ".", StopSign! )
		// Make the problem column current.
		dw_alias.SetColumn("alias")
		dw_alias.ScrollToRow(ai_row)
		dw_alias.SetFocus()
		RETURN FALSE
	END IF
END IF

RETURN TRUE
end function

on close;call w_netwise_sheet::close;IF ISValid( iu_utl001) Then Destroy( iu_utl001)
end on

event activate;call super::activate;IF Trim(istr_error_info.se_user_id) <> Trim(is_user_id_in) THEN 
	gw_netwise_frame.im_netwise_menu.m_file.m_delete.disable()
ELSE
	gw_netwise_frame.im_netwise_menu.m_file.m_delete.enable()
END IF

end event

event deactivate;call super::deactivate;gw_netwise_frame.im_netwise_menu.m_file.m_delete.enable()


end event

on ue_query;call w_netwise_sheet::ue_query;wf_retrieve()
end on

event ue_postopen;call super::ue_postopen;long 		ll_rowcount


istr_error_info.se_user_id = SQLCA.UserID
dw_alias.GetChild("alias_type", idwc_child)
idwc_child.SetTrans(SQLCA)
idwc_child.Retrieve("ALIATYPE")
ll_rowcount = idwc_child.RowCount()
idwc_child.ResetUpdate()
PostEvent("ue_query")
end event

on w_message_alias.create
int iCurrent
call super::create
this.dw_alias=create dw_alias
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_alias
end on

on w_message_alias.destroy
call super::destroy
destroy(this.dw_alias)
end on

type dw_alias from u_netwise_dw within w_message_alias
integer width = 2377
integer height = 992
string dataobject = "d_message_aliases"
boolean vscrollbar = true
boolean border = false
end type

on ue_keydown;// Used to not allow scrolling of a datawindow
long		ll_rowcount

string	ls_type

ll_rowcount = dw_alias.RowCount()

IF dw_alias.GetRow() = ll_rowcount THEN
	ls_type = dw_alias.GetItemString(ll_rowcount, "alias_type")
	IF IsNull(ls_type) or ls_type = "" THEN Return 
		If (KeyDown(KeyTab!))			or &
			(KeyDown(KeyEnter!))			or &
			(KeyDown(KeyDownArrow!))	THEN
			dw_alias.InsertRow(ll_rowcount + 1)
		END IF
End If


end on

event itemchanged;call super::itemchanged;Long			ll_row, &
				ll_rowcount, &
				ll_currentrow

String		ls_text

ll_rowcount   = this.RowCount()
is_columnname = this.GetColumnName()
IF is_columnname = "alias_type" THEN
	ls_text = This.GetText()
	If Len(Trim(ls_text)) = 0 Then 
		This.SetItem(1, "type_description", "")	
		return
	End if
	ll_currentrow = dw_alias.GetRow()
	ll_row = idwc_child.Find('type_code = "' + ls_text + '"', 1, idwc_child.RowCount())
	If ll_row <= 0 Then
		gw_netwise_frame.SetMicroHelp("Invalid Type Code")
		This.SelectText(1, Len(ls_text))
		RETURN 1
	 	return
	 	This.SetItem(1, "type_description", "")	
	Else
		gw_netwise_Frame.SetMicroHelp("Ready")
	End if
	This.SetItem(ll_currentrow, "type_description", idwc_child.GetItemString(ll_row, "type_desc"))
END IF


end event

on constructor;call u_netwise_dw::constructor;is_selection = "3"
end on

