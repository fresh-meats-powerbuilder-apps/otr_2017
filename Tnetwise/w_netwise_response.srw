HA$PBExportHeader$w_netwise_response.srw
forward
global type w_netwise_response from w_base_response
end type
end forward

global type w_netwise_response from w_base_response
end type
global w_netwise_response w_netwise_response

forward prototypes
public function integer setmicrohelp (string as_microhelp)
end prototypes

public function integer setmicrohelp (string as_microhelp);return gw_netwise_frame.SetMicroHelp(as_microhelp)

end function

on w_netwise_response.create
call w_base_response::create
end on

on w_netwise_response.destroy
call w_base_response::destroy
end on

