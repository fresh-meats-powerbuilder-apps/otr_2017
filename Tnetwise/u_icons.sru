HA$PBExportHeader$u_icons.sru
forward
global type u_icons from UserObject
end type
type st_1 from statictext within u_icons
end type
type st_icons from statictext within u_icons
end type
type pb_icons from picturebutton within u_icons
end type
end forward

global type u_icons from UserObject
int Width=426
int Height=489
boolean Border=true
long BackColor=12632256
event ue_clicked pbm_custom01
st_1 st_1
st_icons st_icons
pb_icons pb_icons
end type
global u_icons u_icons

type variables
string is_statictext
end variables

forward prototypes
public subroutine uf_set_count (string as_count)
public subroutine uf_set_icon (string as_picturename, string as_picturetext)
end prototypes

public subroutine uf_set_count (string as_count);This.st_icons.text = as_count
end subroutine

public subroutine uf_set_icon (string as_picturename, string as_picturetext);This.pb_icons.picturename = as_picturename
This.pb_icons.text = ''
This.st_1.Text = as_picturetext
end subroutine

on u_icons.create
this.st_1=create st_1
this.st_icons=create st_icons
this.pb_icons=create pb_icons
this.Control[]={ this.st_1,&
this.st_icons,&
this.pb_icons}
end on

on u_icons.destroy
destroy(this.st_1)
destroy(this.st_icons)
destroy(this.pb_icons)
end on

type st_1 from statictext within u_icons
int X=60
int Y=237
int Width=275
int Height=73
boolean Enabled=false
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_icons from statictext within u_icons
int X=60
int Y=341
int Width=275
int Height=129
boolean Enabled=false
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type pb_icons from picturebutton within u_icons
int X=60
int Y=21
int Width=279
int Height=209
int TabOrder=1
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;Parent.TriggerEvent("ue_clicked")
end on

