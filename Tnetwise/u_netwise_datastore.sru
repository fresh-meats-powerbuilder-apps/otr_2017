HA$PBExportHeader$u_netwise_datastore.sru
forward
global type u_netwise_datastore from u_base_datastore
end type
end forward

global type u_netwise_datastore from u_base_datastore
end type
global u_netwise_datastore u_netwise_datastore

on u_netwise_datastore.create
call datastore::create
TriggerEvent( this, "constructor" )
end on

on u_netwise_datastore.destroy
call datastore::destroy
TriggerEvent( this, "destructor" )
end on

