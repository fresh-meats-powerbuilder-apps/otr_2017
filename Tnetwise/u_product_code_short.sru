HA$PBExportHeader$u_product_code_short.sru
forward
global type u_product_code_short from u_netwise_dw
end type
end forward

global type u_product_code_short from u_netwise_dw
integer width = 1458
integer height = 92
string dataobject = "d_product_code_short"
boolean border = false
event ue_postgetfocus pbm_custom01
end type
global u_product_code_short u_product_code_short

type variables
Private:
Boolean		ib_save, &
		ib_Enabled, &
		ib_DropDown = True

Long		il_old_color, &
		il_lastFoundRow = 1

Int		ii_keystyped

String		is_whatwastyped, &
		is_division

u_division		iu_division
end variables

forward prototypes
public subroutine uf_disable ()
public subroutine uf_enable ()
public function string uf_getproductcode ()
public function boolean uf_setproductcode (string as_product_code)
public function boolean uf_setproductdesc (string as_product_short_descr)
public function string uf_get_division ()
public subroutine uf_set_division (u_division au_division)
public subroutine uf_setdropdown (boolean ab_dropdown)
public function boolean uf_getdropdown ()
public function string uf_getproductdesc ()
end prototypes

event ue_postgetfocus;call super::ue_postgetfocus;DataWindowChild	ldwc_code, &
						ldwc_descr
						
String	ls_first, &
			ls_last

u_string_functions	lu_string
			

If Not lu_string.nf_IsEmpty(is_division) Then
	ls_first = is_division
	ls_last = is_division
Else		
		ls_first = '  '
		ls_last = 'ZZ'
End If
This.GetChild('sku_product_code', ldwc_code)
ldwc_code.SetTrans(SQLCA)
ldwc_code.Retrieve(ls_first, ls_last)
This.GetChild("product_short_descr", ldwc_descr)
ldwc_code.ShareData(ldwc_descr)

end event

public subroutine uf_disable ();This.ib_Enabled = False
il_old_color = Long(This.Describe("fab_product_code.BackColor"))

This.Modify("sku_product_code.BackGround.Color = 12632256")
This.Modify("sku_product_code.Protect = 1")
return
end subroutine

public subroutine uf_enable ();This.ib_Enabled = True
This.Modify("sku_product_code.BackGround.Color = " + String(il_old_color))
This.Modify("sku_product_code.Protect = 0")
return
end subroutine

public function string uf_getproductcode ();string		ls_product_code, &
				ls_division_code
				
long ll_sqlcode
				

This.AcceptText()

ls_product_code = GetItemString( 1, "sku_product_code" )
IF IsNull( ls_product_code ) THEN return ""

Connect Using SQLCA;
Select product_division
  into :ls_division_code
  from sku_products
  where sku_product_code = :ls_product_code;
 ll_sqlcode = SQLCA.SqlCode
 Disconnect Using SQLCA;
  
If ll_sqlcode = 100 Then return ""
If IsValid(iu_division) And ls_division_code <> This.uf_get_division() Then
	This.Event getfocus()
	return ''
End if

Return( ls_product_code )
end function

public function boolean uf_setproductcode (string as_product_code);DataWindowChild	ldwc_code, &
						ldwc_descr 
String		ls_Description, &
				ls_division
integer		li_rc
long 			ll_sqlcode


li_rc = SetItem( 1, "sku_product_code", as_product_code )

// If this is disabled, then the drop down is empty
If Not This.ib_Enabled Then
	// It might have been added, check for it
	This.GetChild('sku_product_code', ldwc_code)
	If ldwc_code.Find("sku_product_code = '" + as_product_code + "'", 1, &
							ldwc_code.RowCount()) <= 0 Then
		Connect Using SQLCA;
		Select 	short_description, 
					product_division
		into 		:ls_Description,
			   	:ls_division
		from sku_products
		where sku_product_code = :as_product_code
		using SQLCA;
		ll_sqlcode = SQLCA.SQLCode
		Disconnect Using SQLCA;
		
		If ll_sqlcode = 0 Then
			ldwc_code.ImportString(as_product_code + "~t" + ls_description + "~t" + ls_division)
			This.GetChild('product_short_descr', ldwc_descr)
			ldwc_code.ShareData(ldwc_descr)
		End if
	End if
Else	
	IF li_rc = 1 THEN
		Return( true )
	ELSE
		Return( False )
	END IF
End if

return True
end function

public function boolean uf_setproductdesc (string as_product_short_descr);//integer		li_rc
//
//li_rc = SetItem( 1, "product_short_descr", as_product_short_descr)
//
//IF li_rc = 1 THEN
	Return( true )
//ELSE
//	Return( False )
//END IF
end function

public function string uf_get_division ();String		ls_division

If IsValid(iu_division) Then ls_division = iu_division.GetItemString( 1, "division_code" )

IF IsNull( ls_division) THEN ls_division = ""

Return(ls_division)
end function

public subroutine uf_set_division (u_division au_division);iu_division = au_division


end subroutine

public subroutine uf_setdropdown (boolean ab_dropdown);ib_dropdown = ab_DropDown

If ab_DropDown Then
Else
	This.Object.sku_product_code.Edit.Style = 'edit'
End if

end subroutine

public function boolean uf_getdropdown ();return ib_dropdown
end function

public function string uf_getproductdesc ();string		ls_product_short_descr, &
				ls_product_code


AcceptText()

ls_product_code = This.GetItemString( 1, "product_short_descr" )

Connect Using SQLCA;
select short_description
into :ls_product_short_descr
from sku_products
where sku_product_code = :ls_product_code;
Disconnect Using SQLCA;

IF IsNull( ls_product_short_descr ) THEN ls_product_short_descr = ""

Return( ls_product_short_descr )
end function

on itemfocuschanged;call u_netwise_dw::itemfocuschanged;This.SelectText(1, Len(This.GetText()))
end on

event getfocus;call super::getfocus;String				ls_division

u_string_functions	lu_string


This.SelectText(1, Len(This.GetText()))

ls_division = This.uf_Get_division()
If lu_string.nf_IsEmpty(ls_division) Then ls_division = '  '
If (ls_division <> is_division or IsNull(is_division)) and This.ib_Enabled Then
	is_division = ls_division
	This.PostEvent("ue_PostGetFocus")
End If

//If ldwc_code.Find("sku_product_code = '" + This.GetItemString(1, &
//					"sku_product_code") + "'", 1, ldwc_code.RowCount()) <= 0 Then
//	This.SetItem(1, "sku_product_code", "")
//End if

end event

event itemerror;call super::itemerror;If dwo.Name = 'sku_product_code' Then Return 1
Return 0
end event

event editchanged;call super::editchanged;///////////////////////////////////////////////////////////////////////
///  WARNING!!!!!!!
///  SetRedraw is False upon entering this code!!!
///////////////////////////////////////////////////////////////////////
DataWindowChild	ldwc_child

String	ls_FoundProduct, &
			ls_productcode
Long		ll_sqlcode

// If you remove the setRedraw, be sure to remove it out of the keydown
If Len(data) = 0 Or String(dwo.Name) <> 'sku_product_code' Then 
	This.SetRedraw(True)
	Return
End if
ii_keystyped ++

If ii_keystyped > Len(data) Then ii_keystyped = Len(data)
This.GetChild("sku_product_code", ldwc_child)

ib_save = Not ib_save
If Not ib_Save	Then return
If is_whatwastyped <> Data Then
	is_whatwastyped += Right(Data,1)
End if
//ls_FindString = "sku_product_code >= '" + Left(Data, ii_keystyped) + "'"

//il_LastFoundRow = ldwc_child.Find( ls_FindString, il_LastFoundRow, 100000)
ls_productCode = Left(Data, ii_KeysTyped) + "%"

Connect Using SQLCA;
Select Min(sku_product_code)
  into :ls_FoundProduct
  From sku_products
 Where sku_product_code like :ls_productcode;
ll_sqlcode = SQLCA.SQLCode
Disconnect Using SQLCA;

If ll_sqlcode = 0 Then 
	// Since the SQL is using a 'like', a 100 will not be returned from a not found.  
	// Instead, the ls_FoundProduct will be null
	
	If IsNull(ls_FoundProduct) Then
		gw_netwise_Frame.SetMicroHelp("The Product " + String(data) + " is not a valid SKU product code")
		ls_FoundProduct = ''
		is_whatwastyped = ''
		ii_keystyped = 0
		ib_save = False
	End if
	
	This.SetItem(1, "sku_product_code", ls_FoundProduct)
//	IF il_LastFoundRow + 1 <= ldwc_child.RowCount() Then
//		if pos(ldwc_child.GetItemString( il_LastFoundRow + 1,"sku_product_code"),&
//			Trim(Left(Data,ii_keystyped))) = 0 Then 
//			Beep(1)
//		END IF
//	END IF
	This.SelectText(ii_keystyped + 1,1000)
End If
This.SetRedraw(True)

end event

event ue_keydown;call super::ue_keydown;dwObject		ldwo_col


// This will get set to true in the editchanged event
This.SetRedraw(False)

Choose Case key
	Case	KeyBack!
		ib_save = False
		is_whatwastyped = Left(is_whatwastyped, Len(is_whatwastyped) - 1)
		ii_keystyped = Len(is_whatwastyped) - 1
		il_LastFoundRow = 1
		If ii_keystyped < 0 Then ii_keystyped = 0
		This.SetItem(1, 'sku_product_code', is_whatwastyped)
		ldwo_col = This.Object.sku_product_code
		This.Event Post editchanged(1, ldwo_col, is_whatwastyped)

	Case KeyLeftArrow!   
		ii_keystyped --
		il_LastFoundRow = 1
		if ii_keystyped < 2 then ii_keystyped = 2
	Case 	KeyEnd!
		ib_save = FALSE
		ii_Keystyped = 10
	Case	KeyHome!
		ib_save = False		
		ii_keystyped = 2
	Case	KeyRightArrow!
		ib_save = False		
		ii_keystyped ++
		if ii_keystyped > 10 then ii_keystyped = 10
	Case Keydelete!
		ib_save = False		
		ii_keystyped = Len(This.GetText())
END choose

end event

event itemchanged;call super::itemchanged;DataWindowChild		ldwc_code

Long						ll_find, &
							ll_rowcount
							
u_string_functions	lu_string

String					ls_microhelp
							

If dwo.name = 'sku_product_code' Then
	This.GetChild('sku_product_code', ldwc_code)
	ll_rowcount = ldwc_code.RowCount()
	ll_find = ldwc_code.Find('sku_product_code = "' + data + '"', 1, ll_rowcount)
	If ll_find < 1 Then
		If lu_string.nf_IsEmpty(is_division) Then
			ls_microhelp = data + ' is an invalid product code'
		Else
			ls_microhelp = data + ' is an invalid product code for Division ' + is_division 
		End If
		gw_netwise_frame.SetMicrohelp(ls_microhelp)
		This.SetFocus()
		This.SelectText(1, 100)
		Return 1
	End If
End If	
end event

event constructor;call super::constructor;is_selection = '0'
ib_Enabled = True
Setnull(is_division)

If Not ib_DropDown Then
	This.uf_SetDropDown(False)
End if
end event

event losefocus;call super::losefocus;// The redraw might be off here under certain circumstances
This.SetRedraw(True)
end event

on u_product_code_short.create
end on

on u_product_code_short.destroy
end on

