HA$PBExportHeader$w_netwise_login.srw
forward
global type w_netwise_login from w_base_login
end type
type p_1 from picture within w_netwise_login
end type
type dw_login_name from u_netwise_dw within w_netwise_login
end type
end forward

global type w_netwise_login from w_base_login
integer x = 731
integer y = 252
integer width = 1440
integer height = 1464
string title = "IBP Login"
p_1 p_1
dw_login_name dw_login_name
end type
global w_netwise_login w_netwise_login

type prototypes

end prototypes

type variables
Char	ic_Action_Indicator 

u_utl001  iu_utl001

u_ws_utl  iu_ws_utl


end variables

forward prototypes
public subroutine wf_login ()
end prototypes

public subroutine wf_login ();Boolean 	lb_password_changed

Int 		li_rtn

String	ls_Name_Password

s_error	s_error_info

dw_login_name.accepttext()
lb_password_changed = FALSE

IF LEN(TRIM(dw_login_name.GetItemString(1,"sle_password"))) < 7 THEN
	MessageBox("Invalid Password", "Please Enter your Password. Length must be at least 7 characters" )
	dw_login_name.SetFocus()
	dw_login_name.SetColumn("sle_password")
	sle_password.SelectText(1,7)
   	RETURN
END IF
IF Upper(TRIM(dw_login_name.GetItemString(1,"sle_username"))) <> Upper(Trim(ProfileString( gw_netwise_frame.is_userini, "System Settings", "UserId", ""))) THEN
	SetProfileString( gw_netwise_frame.is_userini, "Orp", "SalesCode", "")
  	SetProfileString( gw_netwise_frame.is_userini, "Orp", "Division", "")
	SetProfileString( gw_netwise_frame.is_userini, "Orp", "Location", "")
	SetProfileString( gw_netwise_frame.is_userini, "System Settings", "DefaultDate", "")
END IF
dw_login_name.SetRedraw(False)
//commented on 8/27/96
//This.Visible = TRUE
SetPointer(HourGlass!)
Choose Case ic_action_indicator
	Case 'X'
		ic_action_indicator = 'M'
	Case 'L'
		ic_action_indicator = 'M'	
	Case 'P'
		ic_action_indicator = 'M'	
	Case 'I'
		ic_action_indicator = 'V'
	Case 'S'
		ic_action_indicator = 'V'
End Choose

IF ic_action_indicator = 'V' Then  //Verify
	ls_Name_Password = UPPER(TRIM(dw_login_name.GetItemString(1,"sle_username"))) +"~t"+ UPPER(Trim(dw_login_name.GetItemString(1,"sle_password"))) +"~r~n"
ELSE
	IF ic_Action_indicator = 'M' Then

      IF (UPPER(dw_login_name.getItemString(1, "sle_newpassword")) <> UPPER(dw_login_name.GetItemString(1, "sle_verifypassword"))) AND ic_Action_indicator = 'M' Then
			dw_login_name.SetRedraw(True)
	  		MessageBox("Invalid Password", "Please Enter Your Password Twice. They must be the same")
			dw_login_name.Setrow(1)
			dw_login_name.setColumn("sle_verifypassword")
			Return
      END IF
		If Trim(UPPER(dw_login_name.getItemString(1, "sle_newpassword"))) = Trim(UPPER(dw_login_name.GetItemString(1,"sle_password"))) Then
			dw_login_name.SetRedraw(True)
			MessageBox("Invalid Password", "New Password must be different than old Password")
			dw_login_name.Setrow(1)
			dw_login_name.setcolumn("sle_newpassword")
			Return
		End if
      IF LEN(TRIM(dw_login_name.getItemString(1, "sle_newpassword"))) < 7  THEN
			dw_login_name.SetRedraw(True)
         MessageBox("Invalid Password", "Password must be at least seven characters long")
			Return
      END IF
		
		if (ic_action_indicator = 'L') or (ic_action_indicator = 'P') then
			dw_login_name.SetRedraw(True)
			Return
		else
			// Else all is well, change password
			ls_Name_Password = UPPER(TRIM(dw_login_name.GetItemString(1,"sle_username"))) +"~t"+ UPPER(TRIM(dw_login_name.GetItemString(1,"sle_password"))) + "~t"+ UPPER(TRIM(dw_login_name.getItemString(1, "sle_newpassword"))) +"~r~n"
  	   	dw_login_name.SetItem(1,"sle_password", UPPER(TRIM(dw_login_name.getItemString(1, "sle_newpassword"))))
   		lb_password_changed = TRUE
		end if	
	Else
		ic_action_indicator = 'V'
	END IF
END IF

s_Error_Info.se_user_id = UPPER(TRIM(dw_login_name.GetItemString(1,"sle_username")))

//li_rtn = iu_utl001.nf_utlu07ar( s_Error_Info, &
//    Ls_Name_Password, &
//    ic_action_indicator)
//
li_rtn = iu_ws_utl.nf_utlu07er( s_Error_Info, &
    Ls_Name_Password, &
    ic_action_indicator)

IF li_rtn < 0 Then 
   HALT CLOSE
END IF

dw_login_name.SetItem(1,"businessrule",ic_action_indicator)

Choose Case ic_action_indicator
   Case 'A'
		MessageBox("Security Violation", "You dont have access to this CICS region.~r~n            Exiting Application.")
		Halt Close
	Case 'X'
		dw_login_name.SetRedraw(True)
		MessageBox("Password Expired", "Your Password has expired, Please enter your new Password")
		dw_login_name.SetRow(1)
		dw_login_name.setColumn("sle_newpassword")
	Case 'I'
		dw_login_name.SetRedraw(True)
		MessageBox("Invalid Password", "Invalid Password/UserId")
	Case 'L'
		dw_login_name.SetRedraw(True)
		MessageBox("Invalid Password", "Password must be at least 7 characters")
		dw_login_name.SetRow(1)
		dw_login_name.setColumn("sle_verifypassword")
	Case 'P'
		dw_login_name.SetRedraw(True)
		MessageBox("Invalid Password", "Password has been used previously")	
		dw_login_name.SetRow(1)
		dw_login_name.setColumn("sle_verifypassword")
	Case 'S'
		dw_login_name.SetRedraw(True)
		MessageBox("Suspended Password", "Your Mainframe password has been suspended.~r~nPlease Call the Help desk at 3133")
	Case 'G'
      // If the User Name and Password is Valid, save them in the Message Object
			SQLCA.userid	=	Upper(dw_login_name.GetItemString(1,"sle_username"))
		IF lb_password_changed Then
			SQLCA.DBPass	=	UPPER(TRIM(dw_login_name.getItemString(1, "sle_newpassword")))
		ELSE
			SQLCA.DBPass	=	Upper(Trim( dw_login_name.GetItemString(1,"sle_password")))
		End if
		SetProfileString( gw_netwise_frame.is_userini, "System Settings", "UserId", Trim(SQLCA.UserID))
		Close(This)
	Case 'V'
		dw_login_name.SetRedraw(True)
		MessageBox("Warning", "Error in communication with mainframe.  Contact Help Desk at extension 3133")
	Case 'M'
		dw_login_name.SetRedraw(True)
		MessageBox("Warning", "Error in communication with mainframe.  Contact Help Desk at extension 3133")
	Case Else
   	dw_login_name.SetRedraw(True)
     	MessageBox("Error", "UNKNOWN ERROR RETURNED") 
End Choose


     

end subroutine

event open;//iu_netware_sdk = Create u_netware_sdk
//iu_utl001      = Create u_utl001

iu_ws_utl      = Create u_ws_utl

// Post the constructor event 
This.PostEvent("ue_postopen")

end event

event close;call super::close;//Destroy( iu_utl001)
Destroy( iu_ws_utl)

IF ic_action_indicator <> 'G' Then
	Halt CLose
END IF

end event

event ue_postopen;call super::ue_postopen;String ls_userid

//ib_connected_to_netWare  = wf_verify_connection()

ls_userid = ProfileString( gw_netwise_frame.is_UserIni,"System Settings","UserID","")
dw_login_name.setitem(1, "sle_username", Trim(ls_UserID))
dw_login_name.setitem(1, "sle_password", Trim(ProfileString( gw_netwise_frame.is_UserIni,"System Settings","UserPassword","")))

IF Len(Trim(ls_UserID)) > 0 THEN
   dw_login_name.SetFocus()
	dw_login_name.SetColumn("sle_password")
END IF
ic_action_indicator = 'V'
This.Visible = TRUE













//	dw_login_name.SetFocus()
//	dw_login_name.SetColumn("sle_username")
//Else

end event

on w_netwise_login.create
int iCurrent
call super::create
this.p_1=create p_1
this.dw_login_name=create dw_login_name
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.p_1
this.Control[iCurrent+2]=this.dw_login_name
end on

on w_netwise_login.destroy
call super::destroy
destroy(this.p_1)
destroy(this.dw_login_name)
end on

event ue_base_ok;call super::ue_base_ok;integer 	li_rtn
string	ls_userid, ls_application
//u_olecom	lu_oleReportList


ls_application = Message.nf_Get_App_ID()
//If ls_application = "ORP" Then
//	ls_userid = ProfileString( gw_netwise_frame.is_UserIni,"System Settings","UserID","")
//	lu_oleReportList = Create u_olecom
//	li_rtn = lu_oleReportList.ConnectToNewObject("ReportView.ReportList")
//	if li_rtn < 0 then
//		iw_frame.im_menu.m_holding1.m_viewlist.disable()
//	end if
//end if
end event

type cb_base_help from w_base_login`cb_base_help within w_netwise_login
integer taborder = 20
end type

type cb_base_cancel from w_base_login`cb_base_cancel within w_netwise_login
integer y = 1172
integer taborder = 60
end type

type cb_base_ok from w_base_login`cb_base_ok within w_netwise_login
integer y = 1172
integer taborder = 50
end type

type st_1 from w_base_login`st_1 within w_netwise_login
boolean visible = false
string text = "Base Login Ext"
end type

type st_username from w_base_login`st_username within w_netwise_login
boolean visible = false
integer x = 1746
integer y = 552
integer width = 434
end type

type sle_username from w_base_login`sle_username within w_netwise_login
boolean visible = false
integer x = 2203
integer y = 544
integer width = 434
integer height = 92
string facename = "MS Sans Serif"
integer limit = 8
end type

type st_password from w_base_login`st_password within w_netwise_login
boolean visible = false
integer x = 1746
integer y = 680
integer width = 434
end type

type sle_password from w_base_login`sle_password within w_netwise_login
boolean visible = false
integer x = 2199
integer y = 668
integer width = 434
integer height = 92
integer taborder = 40
string facename = "MS Sans Serif"
long backcolor = 16777215
integer limit = 8
end type

type st_3 from w_base_login`st_3 within w_netwise_login
boolean visible = false
end type

type gb_1 from w_base_login`gb_1 within w_netwise_login
integer y = 1112
end type

type p_1 from picture within w_netwise_login
integer x = 270
integer y = 120
integer width = 782
integer height = 384
boolean bringtotop = true
string picturename = "IBP.BMP"
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_login_name from u_netwise_dw within w_netwise_login
integer x = 219
integer y = 572
integer width = 878
integer height = 492
integer taborder = 10
string dataobject = "d_login_new"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

