HA$PBExportHeader$u_download_data.sru
$PBExportComments$Export Ibdkdld
forward
global type u_download_data from nonvisualobject
end type
end forward

global type u_download_data from nonvisualobject autoinstantiate
event ue_update_database ( )
end type

type prototypes

end prototypes

type variables
DataStore	ids_Addresses

u_base_datastore	ids_service_centers, &
		ids_location_codes, & 		
		ids_win_access , &		
		ids_type_description, &
		ids_customerdata, &
		ids_sales_people, &
		ids_carrier_info, &
		ids_carrier_dispreferance, &
		ids_billtocustdata, &
		ids_corpcustdata, &
		ids_menu_class , &
		ids_cust_defaults, &
		ids_product_codes, &
		ids_sku_plt

u_utl001		iu_utl001

end variables

forward prototypes
public subroutine nf_destroy_allinstances ()
public subroutine nf_rollback (string as_table)
public function boolean nf_checkformessages ()
end prototypes

public subroutine nf_destroy_allinstances ();IF IsValid (ids_addresses) 				Then Destroy	ids_addresses
IF IsValid (ids_location_codes) 			Then Destroy	ids_location_codes 		
IF IsValid (ids_win_access) 				Then Destroy	ids_win_access 			
IF IsValid (ids_type_description)		Then Destroy	ids_type_description 	
IF IsValid (ids_service_centers)			Then Destroy	ids_service_centers		
IF IsValid (ids_customerdata)				Then Destroy	ids_customerdata			
IF IsValid (ids_billtocustdata)			Then Destroy	ids_billtocustdata		
IF IsValid (ids_corpcustdata)				Then Destroy	ids_corpcustdata			
IF IsValid (ids_sales_people)				Then Destroy	ids_sales_people			
IF IsValid (ids_carrier_dispreferance)	Then Destroy	ids_carrier_dispreferance
IF IsValid (ids_carrier_info)				Then Destroy	ids_carrier_info			
IF IsValid (ids_cust_defaults)			Then Destroy	ids_cust_defaults
IF IsValid (ids_product_codes)			Then Destroy	ids_product_codes
IF IsValid (ids_sku_plt)					Then Destroy	ids_sku_plt
end subroutine

public subroutine nf_rollback (string as_table);MessageBox('Update Failed', 'Error in updating ' + as_table)
SQLCA.nf_RollBack()
HALT CLOSE

end subroutine

public function boolean nf_checkformessages ();Char			lc_messages

s_error		lstr_error_info

lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_app_name = Message.nf_Get_App_ID()
lstr_error_info.se_message = Space(70)

// Don't worry about return code, since it could set MicroHelp if successful
//iu_utl001.nf_utlu12ar(lstr_error_info, lc_messages)

If lc_messages = 'Y' Then
	gw_netwise_frame.im_netwise_menu.m_options.m_ViewMessages.ToolbarItemVisible = True
	return True
Else
	gw_netwise_frame.im_netwise_menu.m_options.m_ViewMessages.ToolbarItemVisible = False
	return False
End if

end function

on u_download_data.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_download_data.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;String	ls_server

Long		ll_count

u_sdkcalls	lu_sdkcalls

//iu_utl001   = Create u_utl001
lu_sdkcalls	= Create u_sdkcalls

SQLCA.nf_getsqlcafromini(gw_netwise_frame.is_WorkingDir+"ibp002.ini")

// 03/2002 ibdkkam Removed connection to pblocaldb.
// Will connect/disconnect from database every time pblocaldb is referenced.
//IF SQLCA.nf_connect() THEN
//	gw_netwise_frame.SetMicrohelp('Connected to the Database')
//END IF

gw_netwise_frame.SetMicroHelp("Checking for new messages ...")
nf_CheckforMessages()

gw_netwise_frame.SetMicroHelp("Ready")

end event

event destructor;nf_destroy_allinstances()
//IF Isvalid (iu_utl001) THEN Destroy	iu_utl001

end event

