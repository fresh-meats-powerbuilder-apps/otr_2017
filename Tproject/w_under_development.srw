HA$PBExportHeader$w_under_development.srw
forward
global type w_under_development from w_base_sheet_ext
end type
type st_1 from statictext within w_under_development
end type
end forward

global type w_under_development from w_base_sheet_ext
int Width=1916
int Height=613
boolean TitleBar=true
string Title="Under Development"
long BackColor=12632256
st_1 st_1
end type
global w_under_development w_under_development

on w_under_development.create
int iCurrent
call w_base_sheet_ext::create
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
end on

on w_under_development.destroy
call w_base_sheet_ext::destroy
destroy(this.st_1)
end on

type st_1 from statictext within w_under_development
int X=353
int Y=105
int Width=1107
int Height=113
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
string Text="This window is under development."
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=16777215
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

