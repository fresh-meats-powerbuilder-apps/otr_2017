HA$PBExportHeader$w_assignment_filter.srw
forward
global type w_assignment_filter from w_base_response_ext
end type
type dw_plants from datawindow within w_assignment_filter
end type
type dw_region from datawindow within w_assignment_filter
end type
type dw_division from datawindow within w_assignment_filter
end type
type dw_carriers from datawindow within w_assignment_filter
end type
type gb_other from groupbox within w_assignment_filter
end type
type cbx_savesettings from checkbox within w_assignment_filter
end type
type st_1 from statictext within w_assignment_filter
end type
type dw_complex from u_base_dw_ext within w_assignment_filter
end type
type dw_from_to_dates from u_base_dw_ext within w_assignment_filter
end type
type st_2 from statictext within w_assignment_filter
end type
end forward

global type w_assignment_filter from w_base_response_ext
integer x = 690
integer y = 404
integer width = 1541
integer height = 1156
string title = "Traffic Assignment Filter"
long backcolor = 12632256
dw_plants dw_plants
dw_region dw_region
dw_division dw_division
dw_carriers dw_carriers
gb_other gb_other
cbx_savesettings cbx_savesettings
st_1 st_1
dw_complex dw_complex
dw_from_to_dates dw_from_to_dates
st_2 st_2
end type
global w_assignment_filter w_assignment_filter

type variables
DataWindowChild	idwc_complex, &
                            idwc_plant, &
                            idwc_carrier, &
                            idwc_division, &
                            idwc_region

integer	ii_rc

w_base_response_ext iw_parent

u_carrier_assignment_str   iu_carr_assign
end variables

event open;call super::open;// load any info into the correct fields
date		ld_ship_from, &
			ld_ship_to

string	ls_complex, &
		ls_search

int	li_rtn,&
		li_pos, &
		li_string_len

long	ll_current_row, &
      ll_complex_array

iu_carr_assign = Message.PowerObjectParm
ls_complex = " "
IF iu_carr_assign.ship_from <> '' THEN 
	ld_ship_from = date(left(iu_carr_assign.ship_from, 4)+'-'+mid(iu_carr_assign.ship_from, 5, 2)+'-'+right(iu_carr_assign.ship_from, 2))
	dw_from_to_dates.SetItem( 1, "from_date", ld_ship_from)
END IF
 
IF iu_carr_assign.ship_to <> '' THEN 
	ld_ship_to = date(left(iu_carr_assign.ship_to, 4)+'-'+mid(iu_carr_assign.ship_to, 5, 2)+'-'+right(iu_carr_assign.ship_to, 2))
	dw_from_to_dates.SetItem( 1, "to_date", ld_ship_to)
END IF

IF iu_carr_assign.ship_from = '' THEN 
	ls_complex = ProfileString( iw_frame.is_userini, "System Settings", "Default_Complex", ls_complex )
	ll_current_row = 1
	li_pos = 1
	li_string_len = len(ls_complex)
	Do While li_pos  < li_string_len
		iu_carr_assign.complex_array[ll_current_row]	= mid(ls_complex,li_pos,3)
		li_pos = li_pos + 4
		ll_current_row ++
	Loop
	ll_current_row = 1
   ll_complex_array = upperbound(iu_carr_assign.complex_array)
	Do While ll_current_row  <= ll_complex_array
		ls_search = "type_code = '"+iu_carr_assign.complex_array[ll_current_row] +"'"
		li_rtn = dw_complex.find(ls_search,1,dw_complex.rowcount())
		if li_rtn > 0 then
			dw_complex.selectrow(li_rtn,true)
		end if
		ll_current_row ++
	Loop
ELSE
	ll_current_row = 1
   ll_complex_array = upperbound(iu_carr_assign.complex_array)
	Do While ll_current_row  <= ll_complex_array
		ls_search = "type_code = '"+iu_carr_assign.complex_array[ll_current_row] +"'"
		li_rtn = dw_complex.find(ls_search,1,dw_complex.rowcount())
		if li_rtn > 0 then
			dw_complex.selectrow(li_rtn,true)
		end if
		ll_current_row ++
	Loop
END IF
dw_carriers.SetItem( 1, "carrier_code", iu_carr_assign.carrier )
dw_division.SetItem( 1, "division_code", iu_carr_assign.division )
dw_plants.SetItem( 1, "plant_code", iu_carr_assign.location )
dw_region.SetItem( 1, "region_code", iu_carr_assign.region )
dw_from_to_dates.selecttext(1,10)


end event

on w_assignment_filter.create
int iCurrent
call super::create
this.dw_plants=create dw_plants
this.dw_region=create dw_region
this.dw_division=create dw_division
this.dw_carriers=create dw_carriers
this.gb_other=create gb_other
this.cbx_savesettings=create cbx_savesettings
this.st_1=create st_1
this.dw_complex=create dw_complex
this.dw_from_to_dates=create dw_from_to_dates
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plants
this.Control[iCurrent+2]=this.dw_region
this.Control[iCurrent+3]=this.dw_division
this.Control[iCurrent+4]=this.dw_carriers
this.Control[iCurrent+5]=this.gb_other
this.Control[iCurrent+6]=this.cbx_savesettings
this.Control[iCurrent+7]=this.st_1
this.Control[iCurrent+8]=this.dw_complex
this.Control[iCurrent+9]=this.dw_from_to_dates
this.Control[iCurrent+10]=this.st_2
end on

on w_assignment_filter.destroy
call super::destroy
destroy(this.dw_plants)
destroy(this.dw_region)
destroy(this.dw_division)
destroy(this.dw_carriers)
destroy(this.gb_other)
destroy(this.cbx_savesettings)
destroy(this.st_1)
destroy(this.dw_complex)
destroy(this.dw_from_to_dates)
destroy(this.st_2)
end on

event ue_base_ok;call super::ue_base_ok;string		ls_complex, &
				ls_plant

long		ll_current_row, &
			ll_rowcount



dw_complex.setfilter("IsSelected()")
dw_complex.filter()
ll_rowcount = dw_complex.rowcount()
IF ll_rowcount <> 0 Then
		
IF dw_complex.AcceptText()		= 1 AND dw_plants.AcceptText()		= 1 AND &
	dw_carriers.AcceptText()	= 1 AND dw_division.AcceptText()			= 1 AND &
	dw_region.AcceptText()		= 1 AND dw_from_to_dates.AcceptText()	= 1 THEN

	ls_plant		= trim( dw_plants.GetItemString( 1, "plant_code" ))
	
	dw_complex.setredraw(FALSE)
	dw_complex.setfilter("IsSelected()")
	dw_complex.filter()
	iu_carr_assign.complex_array[ ] = {" "}
	ll_rowcount = dw_complex.rowcount()
	ll_current_row = 1
	Do While ll_current_row <= ll_rowcount
		iu_carr_assign.complex_array[ll_current_row]	= trim(dw_complex.object.type_code[ll_current_row])
                ls_complex = ls_complex + trim(dw_complex.object.type_code[ll_current_row]) + ' '
		ll_current_row ++
	Loop
	iu_carr_assign.ship_from		= string( dw_from_to_dates.GetItemDate( 1, "from_date" ), "yyyymmdd" )
	iu_carr_assign.ship_to		= string( dw_from_to_dates.GetItemDate( 1, "to_date" ), "yyyymmdd" )
	iu_carr_assign.carrier		= trim( dw_carriers.GetItemString( 1, "carrier_code" ))
	iu_carr_assign.complex		= ls_complex
	iu_carr_assign.division		= trim( dw_division.GetItemString( 1, "division_code" ))
	iu_carr_assign.location		= ls_plant
	iu_carr_assign.region		= trim( dw_region.GetItemString( 1, "region_code" ))
	iu_carr_assign.cancel		= false

	IF cbx_savesettings.checked THEN
		SetProfileString( iw_frame.is_userini, "System Settings", "Default_Complex", trim(ls_complex) )
	END IF
	CloseWithReturn( this, iu_carr_assign )
END IF 

ELSE
	MessageBox("Complex Field Required", "Complex is required, please select one or more complexes")
	dw_complex.setfilter("")
	dw_complex.filter()
	dw_complex.SetSort("type_code A, type_desc A")
	dw_complex.Sort()
END IF
end event

event ue_base_cancel;call super::ue_base_cancel;iu_carr_assign.cancel = true
CloseWithReturn( this, iu_carr_assign )

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_assignment_filter
integer x = 1042
integer y = 928
integer taborder = 90
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_assignment_filter
integer x = 626
integer y = 924
integer taborder = 80
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_assignment_filter
integer x = 215
integer y = 920
integer taborder = 70
end type

type dw_plants from datawindow within w_assignment_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 1024
integer y = 28
integer width = 434
integer height = 184
integer taborder = 30
string dataobject = "d_dddw_plants"
boolean border = false
boolean livescroll = true
end type

event ue_dwndropdown;Long ll_row

ll_row = idwc_plant.Find( "location_code ='" + GetText() + "'", 1, idwc_plant.RowCount() )

If (ll_row = 0) Then
	idwc_plant.SetTrans( SQLCA )
	idwc_plant.Retrieve()
Else 
	idwc_plant.ScrollToRow( ll_row )
	idwc_plant.SelectRow( ll_row, True )
End If
end event

on itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)




end on

event itemchanged;long ll_FoundRow

integer	li_rtn

string ls_complex,ls_search

dwitemstatus l_status

IF Trim(data) = "" THEN Return

ll_FoundRow = idwc_plant.Find ( "location_code='"+data+"'", 1, idwc_plant.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Plant Error", data + " is Not a Valid Plant Code." )
	Return 1
else
	ls_complex = idwc_plant.getitemstring(ll_foundrow,"complex_code")
	dw_complex.selectrow(0,false)
	ls_search = "type_code ='"+ls_complex+"'"
	li_rtn = dw_complex.find(ls_search,1,dw_complex.rowcount())
	if li_rtn > 0 then
		dw_complex.selectrow(li_rtn,true)
	end if
END IF		



end event

on destructor;idwc_plant.setfilter("")

idwc_plant.filter()

end on

event constructor;insertRow(0)
ii_rc = GetChild( 'plant_code', idwc_plant )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for plant_code.")

//idwc_plant.SetTrans(SQLCA)
//idwc_plant.Retrieve()



end event

event itemerror;Return 1
end event

type dw_region from datawindow within w_assignment_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 1038
integer y = 632
integer width = 425
integer height = 172
integer taborder = 60
string dataobject = "d_dddw_region"
boolean border = false
boolean livescroll = true
end type

event ue_dwndropdown;Long ll_row

ll_row = idwc_region.Find( "type_code ='" + GetText() + "'", 1, idwc_region.RowCount() )

If (ll_row = 0) Then
	idwc_region.SetTrans(SQLCA)
	idwc_region.Retrieve('TRAFFREG')
Else 
	idwc_region.ScrollToRow( ll_row )
	idwc_region.SelectRow( ll_row, True )
End If

end event

on itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)


end on

event constructor;insertRow(0)

ii_rc = GetChild( 'region_code', idwc_region )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for region_code.")

//idwc_region.SetTrans(SQLCA)
//idwc_region.Retrieve('TRAFFREG')
end event

event itemchanged;long ll_FoundRow

IF Trim(data) = "" THEN Return

ll_FoundRow = idwc_region.Find ( "type_code='"+data+"'", 1, idwc_region.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Region Error", data + " is Not a Valid Region Code." )
	Return 1
END IF		

end event

event itemerror;Return 1

end event

type dw_division from datawindow within w_assignment_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 1038
integer y = 448
integer width = 425
integer height = 196
integer taborder = 50
string dataobject = "d_dddw_divisions"
boolean border = false
boolean livescroll = true
end type

event ue_dwndropdown;Long ll_row

ll_row = idwc_division.Find( "type_code ='" + GetText() + "'", 1, idwc_division.RowCount() )

If (ll_row = 0) Then
	idwc_division.SetTrans(SQLCA)
	idwc_division.Retrieve('DIVCODE')
Else 
	idwc_division.ScrollToRow( ll_row )
	idwc_division.SelectRow( ll_row, True )
End If


end event

event itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)


end event

event constructor;insertRow(0)
ii_rc = GetChild( 'division_code', idwc_division )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for division_code.")

//idwc_division.SetTrans(SQLCA)
//idwc_division.Retrieve('DIVCODE')
end event

event itemerror;Return 1
end event

event itemchanged;long ll_FoundRow
iw_frame.SetMicroHelp(dwo.name)
IF Trim(data) = "" THEN Return

ll_FoundRow = idwc_division.Find ( "type_code='"+data+"'", 1, idwc_division.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Division Error", data + " is Not a Valid Division Code." )
	Return 1
END IF		

end event

type dw_carriers from datawindow within w_assignment_filter
integer x = 1033
integer y = 284
integer width = 411
integer height = 180
integer taborder = 40
string dataobject = "d_dddw_carriers"
boolean border = false
boolean livescroll = true
end type

on itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)




end on

event itemchanged;long ll_FoundRow

IF Trim(data) = "" THEN Return

ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Carrier Error", data + " is Not a Valid Carrier Code." )
	Return 1
END IF		

end event

event constructor;
insertRow(0)
ii_rc = GetChild( 'carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()
end event

event itemerror;Return 1
end event

type gb_other from groupbox within w_assignment_filter
integer x = 1010
integer y = 232
integer width = 471
integer height = 596
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Other"
end type

type cbx_savesettings from checkbox within w_assignment_filter
integer x = 443
integer y = 848
integer width = 713
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Save Default Complexes"
boolean lefttext = true
end type

type st_1 from statictext within w_assignment_filter
integer x = 41
integer y = 8
integer width = 247
integer height = 72
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Complex"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_complex from u_base_dw_ext within w_assignment_filter
integer x = 46
integer y = 84
integer width = 882
integer height = 448
integer taborder = 10
string dataobject = "d_complex_info"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

on clicked;call u_base_dw_ext::clicked;dw_plants.settext(" ")
end on

on itemchanged;call u_base_dw_ext::itemchanged;dw_plants.settext(" ")
end on

event constructor;call super::constructor;dw_complex.SetTrans(SQLCA)
dw_complex.Retrieve()
is_selection = '3'


end event

type dw_from_to_dates from u_base_dw_ext within w_assignment_filter
integer x = 50
integer y = 616
integer width = 882
integer height = 212
integer taborder = 20
string dataobject = "d_from_and_to_dates"
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)

Date		ld_from, &
			ld_to


If dwo.name = "to_date" Then
	ld_from = dw_from_to_dates.GetItemDate(1,"from_date")
	ld_to = Date(data)
	If ld_from > ld_to Then
		MessageBox("Date error", "To Date can not be less than the From Date, please reenter dates")
		SelectText(1,10)
		Return 1
	End If
End If
end event

on constructor;call u_base_dw_ext::constructor;InsertRow(0)

date current
current = today()
SetItem( 1, "from_date", current )
SetItem( 1, "to_date", current )

end on

event itemerror;call super::itemerror;Return 1
end event

type st_2 from statictext within w_assignment_filter
integer x = 87
integer y = 564
integer width = 274
integer height = 72
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Ship Dates"
alignment alignment = center!
boolean focusrectangle = false
end type

