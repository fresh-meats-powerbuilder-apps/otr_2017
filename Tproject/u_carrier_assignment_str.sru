HA$PBExportHeader$u_carrier_assignment_str.sru
forward
global type u_carrier_assignment_str from nonvisualobject
end type
end forward

global type u_carrier_assignment_str from nonvisualobject autoinstantiate
end type

type variables
String    complex, &
             complex_array[], &
             location, &
             region, &
             division, &
             carrier, &
             ship_from, &
             ship_to


boolean  cancel
end variables

on u_carrier_assignment_str.create
TriggerEvent( this, "constructor" )
end on

on u_carrier_assignment_str.destroy
TriggerEvent( this, "destructor" )
end on

