HA$PBExportHeader$w_hot_list.srw
forward
global type w_hot_list from w_base_sheet_ext
end type
type dw_1 from u_base_dw_ext within w_hot_list
end type
end forward

global type w_hot_list from w_base_sheet_ext
integer x = 23
integer y = 56
integer width = 2862
integer height = 1444
string title = "Hot List"
long backcolor = 12632256
dw_1 dw_1
end type
global w_hot_list w_hot_list

type variables
character   ic_control

string	is_closemessage_txt, &
	is_group_id

integer	ii_rc

DataWindowChild	idwc_typecode
 
uo_hot_list	iuo_hot_list                   

u_otr003   iu_otr003

u_ws_hot_list iu_ws_hot_list
end variables

forward prototypes
public function integer wf_changed ()
public function boolean wf_update ()
public subroutine wf_delete ()
public function integer wf_load_hotlist ()
public function boolean wf_retrieve ()
end prototypes

public function integer wf_changed ();/*
	purpose:	this function will examine all datawindow controls on the window
				that have update capability against the database.  if anything has changed,
				and the change is valid, a 1 is returned.  if an invalid change has occurred
				then a -1 is returned.  if nothing has changed a 0 is returned.

*/

int i, totcontrols, rc, counter
datawindow dw[]

totcontrols = UpperBound(this.control)

FOR i = 1 to totcontrols
	IF TypeOf(this.control[i]) = datawindow! THEN
		counter ++
		dw[counter] = this.control[i]
			rc = dw[counter].AcceptText()
			IF rc = -1 THEN 
				RETURN -1
			ELSE
				CONTINUE
			END IF
			counter --
	END IF
NEXT

FOR i = 1 to counter
	IF dw[i].ModifiedCount() > 0 THEN
		is_closemessage_txt = "Save Changes?"
		RETURN 1
	END IF
NEXT

RETURN 0
	
end function

public function boolean wf_update ();Date		  ld_next_review_date

String	  ls_hours, &
			  ls_minutes, &
           ls_load_key, &
			  ls_type_code, &
			  ls_priority_level, &
			  ls_review_status, &
			  ls_next_review_date, &
   		  ls_next_review_time, &
           ls_update_control

integer    li_rtn, &
			  li_rc

s_error    lstr_Error_Info

long		  ll_Row,&
			  ll_RowCount, &
           ll_Count
							
dwItemStatus		status


IF dw_1.AcceptText() = -1 THEN Return(False)

ll_RowCount = dw_1.RowCount()
 
lstr_error_info.se_event_name = "wf_update"
lstr_error_info.se_window_name = "w_hot_list"
lstr_error_info.se_procedure_name = "wf_update"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

dw_1.Setredraw(FALSE)
SetPointer(HourGlass!)
li_rc = 0
DO WHILE ll_Row < ll_RowCount
   IF li_rc <> 6 THEN
   	ll_Row = dw_1.GetNextModified( ll_Row, Primary! )
   End IF
	IF ll_Row > 0  THEN 
		ll_Count = ll_Count + 1
		ls_load_key			= dw_1.object.load_key[ll_Row]
		ls_type_code 		= dw_1.object.type_code[ll_row]
		ls_priority_level	= dw_1.object.priority_code[ll_Row]
		ls_review_status	= dw_1.object.update_ind[ll_row]
		ld_next_review_date = dw_1.GetItemDate(ll_row, "req_review_date" )
		ls_next_review_date = String(ld_next_review_date, "yyyy-mm-dd") 
		ls_hours = Mid(String(dw_1.GetItemTime(ll_Row, "req_review_time"),"hh:mm"), 1, 2) 
      ls_minutes = Mid(String(dw_1.GetItemTime(ll_Row, "req_review_time"),"hh:mm"), 4, 2)
      ls_next_review_time = ls_hours + ls_minutes 
      ls_update_control = "U"

	//	li_rtn = iu_otr003.nf_otrt21ar(ls_load_key, ls_type_code, &
	//									ls_priority_level, ls_review_status, ls_next_review_date, &
	//									ls_next_review_time, ls_update_control, lstr_error_info, 0)
										
   	   li_rtn = iu_ws_hot_list.nf_otrt21er(ls_load_key, ls_type_code, &
				ls_priority_level, ls_review_status, ls_next_review_date, &
				ls_next_review_time, ls_update_control, lstr_Error_Info)									
										

		dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
		CHOOSE CASE li_rtn
			CASE is < 0 
              dw_1.SetRow(ll_row)
              EXIT
         CASE is > 0
            li_rc = SQLCA.ii_messagebox_rtn
            CHOOSE CASE li_rc
               CASE 1
                  li_rc = 0
               CASE 2
                  li_rc = 0
               CASE 3
                  dw_1.SetRow(ll_row)
                  EXIT
               CASE 4
                  dw_1.SetRow(ll_row)
                  EXIT
               CASE 5
                  dw_1.SetRow(ll_row)
                  dw_1.SetRedraw(True)
                  Return False
               CASE 6
                  li_rc = 6
               CASE ELSE
                  dw_1.SetRow(ll_row)
               END CHOOSE
        	CASE ELSE
				li_rc = 0
		END CHOOSE
	ELSE
		dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
		ll_Row = ll_RowCount + 1
	END IF
LOOP

iw_frame.SetMicroHelp( "Update Processing Completed")
SetPointer(Arrow!)
dw_1.setredraw(true)
SetPointer( Arrow! ) 
Return(True)
end function

public subroutine wf_delete ();Date       ld_next_review_date

integer    li_rtn, &
			  li_rc, &
           li_delete_count, &
           li_msg


long		  ll_Row,&
           ll_del_row, &
			  ll_RowCount

String     ls_load_key, &
			  ls_type_code, &
			  ls_priority_level, &
			  ls_review_status, &
			  ls_next_review_date, &
   		  ls_next_review_time, &
           ls_update_control, &
           ls_hours, &
           ls_minutes           
Boolean	  ib_any_selected_rows

dwItemStatus		status

s_error    lstr_Error_Info


lstr_error_info.se_event_name = "wf_delete"
lstr_error_info.se_window_name = "w_hot_list"
lstr_error_info.se_procedure_name = "wf_delete_?????_?????"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

/*   P10-delete-prompt
        This paragraph will prompt the # of rows to delete with a continue
        yes no box.
*/

//dw_1.SetRedraw(False)
//dw_1.SetFilter("IsSelected()")
//dw_1.Filter()
//li_delete_count = dw_1.rowcount()
//dw_1.SetFilter("")
//dw_1.Filter()
//dw_1.SetRedraw(True)

ll_RowCount = dw_1.RowCount()
ib_any_selected_rows = false
If ll_RowCount > 0 Then
  FOR ll_Row = 1  TO ll_RowCount
    If dw_1.IsSelected(ll_Row) = True Then
		ib_any_selected_rows = True
	 END IF
  NEXT
END IF

// If no rows are selected - get out
IF NOT ib_any_selected_rows Then Return

li_msg = MessageBox("Delete Verification", &
         "Do you want to delete selected rows?", &
         Question!, YesNo!, 1)

dw_1.SetRedraw(False)

//    P10-delete-prompt-end
IF li_msg = 2 then
	dw_1.SetRedraw(True)
	iw_frame.SetMicroHelp("No Rows Deleted")
	Return
End If



If ll_RowCount > 0 Then
  FOR ll_Row = 1  TO ll_RowCount
    If dw_1.IsSelected(ll_Row) = True Then
	   ls_load_key	= dw_1.object.load_key[ll_Row]
       ls_type_code 		= dw_1.object.type_code[ll_row]
		 ls_priority_level	= dw_1.object.priority_code[ll_Row]
		 ls_review_status	= dw_1.object.update_ind[ll_row]
		 ld_next_review_date = dw_1.GetItemDate(ll_row, "req_review_date" )
		 ls_next_review_date = String(ld_next_review_date, "yyyy-mm-dd") 
		 ls_hours = Mid(String(dw_1.GetItemTime(ll_Row, "req_review_time"),"hh:mm"), 1, 2) 
       ls_minutes = Mid(String(dw_1.GetItemTime(ll_Row, "req_review_time"),"hh:mm"), 4, 2)
       ls_next_review_time = ls_hours + ls_minutes 
       ls_update_control = "D"
   		//  li_rtn = iu_otr003.nf_otrt21ar(ls_load_key, ls_type_code, &
		//							ls_priority_level, ls_review_status, ls_next_review_date, &
		//							ls_next_review_time, ls_update_control, lstr_error_info, 0)
									
		   li_rtn = iu_ws_hot_list.nf_otrt21er(ls_load_key, ls_type_code, &
									ls_priority_level, ls_review_status, ls_next_review_date, &
									ls_next_review_time, ls_update_control,lstr_Error_Info)							
									

		 CHOOSE CASE li_rtn
		   CASE is < 0 
             dw_1.SetRow(ll_row)
             EXIT
         CASE is > 0
            li_rc = SQLCA.ii_messagebox_rtn
            CHOOSE CASE li_rc
              CASE 1
                 li_rc = 0
              CASE 2
                 li_rc = 0
              CASE 3
                 dw_1.SetRow(ll_row)
                 EXIT
              CASE 4
                 dw_1.SetRow(ll_row)
                 EXIT
              CASE 5
                 dw_1.SetRow(ll_row)
                 dw_1.SetRedraw(True)
                 Return 
              CASE 6
                 ll_row --              
            END CHOOSE
       	CASE ELSE
 			  	ll_del_row = ll_row
 				dw_1.DeleteRow( ll_del_row )
				IF dw_1.rowcount() = 0 or dw_1.rowcount() < ll_del_row then exit
				ll_Row --
			 	ll_RowCount --
				li_rc = 0
		  END CHOOSE
     END IF
   Next
 END IF
//End if
//dw_1.Scrolltorow(dw_1.RowCount())   // force to last row
dw_1.SetColumn("type_code")
dw_1.setredraw(true)
iw_frame.SetMicroHelp( "Delete Processing Completed") 
Return       


end subroutine

public function integer wf_load_hotlist ();integer		li_rtn				

String		ls_req_load_key, &
				ls_req_from_carrier, &
				ls_req_to_carrier, &
				ls_req_review_status, &
				ls_req_type_code, &
				ls_req_priority, &
				ls_req_ship_from_date, &
				ls_req_ship_to_date, &
				ls_req_delv_from_date, &
				ls_req_delv_to_date, &
				ls_refetch_load_key, &
				ls_req_complex, &
            ls_hot_list_info, &
            ls_modstring, &
	         ls_rtn,ls_req_division


s_error     lstr_Error_Info


// assign values
SetPointer( HourGlass! )

ls_req_load_key			= iuo_hot_list.load_key
ls_req_from_carrier		= iuo_hot_list.start_carrier
ls_req_to_carrier			= iuo_hot_list.end_carrier
ls_req_review_status		= iuo_hot_list.review_status_ind
//ibdkdld
ls_req_division			= iuo_hot_list.division

If iuo_hot_list.review_status_ind = "A" Then
   ls_req_review_status				= " "
Else 
   ls_req_review_status				= iuo_hot_list.review_status_ind
End if

ls_req_type_code		   = iuo_hot_list.hotlist_type
If iuo_hot_list.priority_level = "A" Then
   ls_req_priority			= " "
Else 
   ls_req_priority			= iuo_hot_list.priority_level
End if

ls_req_delv_from_date    = iuo_hot_list.del_from_date
	if ls_req_delv_from_date = '' then
		ls_req_delv_from_date = '          '
	end if
	
ls_req_delv_to_date    = iuo_hot_list.del_to_date
	if ls_req_delv_to_date = '' then
		ls_req_delv_to_date = '          '
	end if

ls_req_ship_from_date    = iuo_hot_list.ship_from_date
	if ls_req_ship_from_date = '' then
		ls_req_ship_from_date = '          '
	end if

ls_req_ship_to_date    = iuo_hot_list.ship_to_date
	if ls_req_ship_to_date = '' then
		ls_req_ship_to_date = '          '
	end if
	
	IF trim(ls_req_from_carrier) > "" and trim(ls_req_to_carrier) = "" then
		iuo_hot_list.end_carrier = ls_req_from_carrier
		ls_req_to_carrier        = ls_req_from_carrier
	ELSE
		iuo_hot_list.end_carrier 	= ls_req_to_carrier
	END IF

ls_req_complex			   = iuo_hot_list.complex

lstr_error_info.se_event_name = "wf_load_hotlist"
lstr_error_info.se_window_name = "w_hot_list"
lstr_error_info.se_procedure_name = "wf_load_hotlist"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)
 
ls_hot_list_info	   = Space(4401)
ls_refetch_load_key  = Space(7)
// ibdkdld
//li_rtn = iu_otr003.nf_otrt20ar(ls_req_load_key, ls_req_from_carrier, &
//         ls_req_to_carrier, ls_req_review_status, ls_req_type_code, &
//			ls_req_priority, ls_req_ship_from_date, ls_req_ship_to_date, &
//			ls_req_delv_from_date, ls_req_delv_to_date, &
//			ls_req_complex, ls_req_division,ls_hot_list_info, lstr_error_info, &
//			ls_refetch_load_key, 0)
			
li_rtn = iu_ws_hot_list.nf_otrt20er(ls_req_load_key, ls_req_from_carrier, &
         ls_req_to_carrier, ls_req_review_status, ls_req_type_code, &
			ls_req_priority, ls_req_ship_from_date, ls_req_ship_to_date, &
			ls_req_delv_from_date, ls_req_delv_to_date, &
			ls_req_complex, ls_req_division,ls_hot_list_info, lstr_Error_Info)			
			
			
			

// Check the return code from the above function call
IF li_rtn <> 0 THEN
	return(-1)
END IF

//dw_1.Setredraw(FALSE)
li_rtn = dw_1.ImportString(trim(ls_hot_list_info))

DO WHILE li_rtn = 50
	ls_hot_list_info	= Space(4401)
//	li_rtn = iu_otr003.nf_otrt20ar(ls_req_load_key, ls_req_from_carrier, &
 //        ls_req_to_carrier, ls_req_review_status, ls_req_type_code, &
//			ls_req_priority, ls_req_ship_from_date, ls_req_ship_to_date, &
//			ls_req_delv_from_date, ls_req_delv_to_date, &
//			ls_req_complex, ls_req_division,ls_hot_list_info, lstr_error_info, &
//			ls_refetch_load_key, 0)
			
	li_rtn = iu_ws_hot_list.nf_otrt20er(ls_req_load_key, ls_req_from_carrier, &
         ls_req_to_carrier, ls_req_review_status, ls_req_type_code, &
			ls_req_priority, ls_req_ship_from_date, ls_req_ship_to_date, &
			ls_req_delv_from_date, ls_req_delv_to_date, &
			ls_req_complex, ls_req_division,ls_hot_list_info, lstr_Error_Info)		
			
			

	li_rtn = dw_1.ImportString(trim(ls_hot_list_info))
 
LOOP


if is_group_id = '603' then
	ls_modstring = "type_code.BackGround.Color='16777215 ~tIf(type_code =~~'PUL~~'or type_code = ~~'EXPEDITE~~'or type_code = ~~'NOTGONE~~'or type_code = ~~'DEADLINE~~'or type_code = ~~'SCD/PROM~~'or type_code = ~~'TRANSIT~~',12632256,16777215)'" 

	ls_rtn = dw_1.Modify(ls_modstring)
	
	string ls_string
	ls_string = dw_1.describe("type_code.background.color")
	

	ls_modstring = "priority_code.BackGround.Color='16777215 ~tIf(type_code =~~'PUL~~'or type_code = ~~'EXPEDITE~~'or type_code = ~~'NOTGONE~~' or type_code = ~~'DEADLINE~~'or type_code = ~~'SCD/PROM~~'or type_code = ~~'TRANSIT~~',12632256,16777215)'"

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "update_ind.Color='16777215 ~tIf(type_code =~~'PUL~~'or type_code = ~~'EXPEDITE~~'or type_code = ~~'NOTGONE~~' or type_code = ~~'DEADLINE~~'or type_code = ~~'SCD/PROM~~'or type_code = ~~'TRANSIT~~',12632256,16777215)'"

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "req_review_date.BackGround.Color='16777215 ~tIf(type_code =~~'PUL~~'or type_code = ~~'EXPEDITE~~'or type_code = ~~'NOTGONE~~' or type_code = ~~'DEADLINE~~'or type_code = ~~'SCD/PROM~~'or type_code = ~~'TRANSIT~~',12632256,16777215)'"

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "req_review_time.BackGround.Color='16777215 ~tIf(type_code =~~'PUL~~'or type_code = ~~'EXPEDITE~~'or type_code = ~~'NOTGONE~~' or type_code = ~~'DEADLINE~~'or type_code = ~~'SCD/PROM~~'or type_code = ~~'TRANSIT~~',12632256,16777215)'"

	ls_rtn = dw_1.Modify(ls_modstring)


	ls_modstring = "type_code.Protect='0 ~tIf(type_code =~~'PUL~~'or type_code = ~~'EXPEDITE~~'or type_code = ~~'NOTGONE~~' or type_code = ~~'DEADLINE~~'or type_code = ~~'SCD/PROM~~'or type_code = ~~'TRANSIT~~',1,0)'"

	ls_rtn = dw_1.Modify(ls_modstring)
	
	ls_string = dw_1.describe("type_code.protect")

	ls_modstring = "priority_code.Protect='0 ~tIf(type_code =~~'PUL~~'or type_code = ~~'EXPEDITE~~'or type_code = ~~'NOTGONE~~' or type_code = ~~'DEADLINE~~'or type_code = ~~'SCD/PROM~~'or type_code = ~~'TRANSIT~~',1,0)'"

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "update_ind.Protect='0 ~tIf(type_code =~~'PUL~~'or type_code = ~~'EXPEDITE~~'or type_code = ~~'NOTGONE~~' or type_code = ~~'DEADLINE~~'or type_code = ~~'SCD/PROM~~'or type_code = ~~'TRANSIT~~',1,0)'"

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "req_review_date.Protect='0 ~tIf(type_code =~~'PUL~~'or type_code = ~~'EXPEDITE~~'or type_code = ~~'NOTGONE~~' or type_code = ~~'DEADLINE~~'or type_code = ~~'SCD/PROM~~'or type_code = ~~'TRANSIT~~',1,0)'"

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "req_review_time.Protect='0 ~tIf(type_code =~~'PUL~~'or type_code = ~~~'EXPEDITE~~'or type_code = ~~'NOTGONE~~' or type_code = ~~'DEADLINE~~'or type_code = ~~'SCD/PROM~~'or type_code = ~~'TRANSIT~~',1,0)'"

	ls_rtn = dw_1.Modify(ls_modstring)

end if

if is_group_id = '601' then
	ls_modstring = "type_code.BackGround.Color='16777215 ~tIf(type_code <>~~'PUL~~'and type_code <> ~~'EXPEDITE~~'and type_code <> ~~'NOTGONE~~'and type_code <> ~~'DEADLINE~~'and type_code <> ~~'SCD/PROM~~'and type_code <> ~~'TRANSIT~~'and type_code <> ~~' ~~',12632256,16777215)'" 

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "priority_code.BackGround.Color='16777215 ~tIf(type_code <>~~'PUL~~'and type_code <> ~~'EXPEDITE~~'and type_code <> ~~'NOTGONE~~'and type_code <> ~~'DEADLINE~~'and type_code <> ~~'SCD/PROM~~'and type_code <> ~~'TRANSIT~~'and type_code <> ~~' ~~',12632256,16777215)'"

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "update_ind.Color='16777215 ~tIf(type_code <>~~'PUL~~'and type_code <> ~~'EXPEDITE~~'and type_code <> ~~'NOTGONE~~'and type_code <> ~~'DEADLINE~~'and type_code <> ~~'SCD/PROM~~'and type_code <> ~~'TRANSIT~~'and type_code <> ~~' ~~',12632256,16777215)'"

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "req_review_date.BackGround.Color='16777215 ~tIf(type_code <>~~'PUL~~'and type_code <> ~~'EXPEDITE~~'and type_code <> ~~'NOTGONE~~'and type_code <> ~~'DEADLINE~~'and type_code <> ~~'SCD/PROM~~'and type_code <> ~~'TRANSIT~~'and type_code <> ~~' ~~',12632256,16777215)'"

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "req_review_time.BackGround.Color='16777215 ~tIf(type_code <>~~'PUL~~'and type_code <> ~~'EXPEDITE~~'and type_code <> ~~'NOTGONE~~'and type_code <> ~~'DEADLINE~~'and type_code <> ~~'SCD/PROM~~'and type_code <> ~~'TRANSIT~~'and type_code <> ~~' ~~',12632256,16777215)'"

	ls_rtn = dw_1.Modify(ls_modstring)

/// proctect fields

	ls_modstring = "type_code.Protect='0 ~tIf(type_code <>~~'PUL~~'and type_code <> ~~'EXPEDITE~~'and type_code <> ~~'NOTGONE~~'and type_code <> ~~'DEADLINE~~'and type_code <> ~~'SCD/PROM~~'and type_code <> ~~'TRANSIT~~'and type_code <> ~~' ~~',1,0)'"

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "priority_code.Protect='0 ~tIf(type_code <>~~'PUL~~'and type_code <> ~~'EXPEDITE~~'and type_code <> ~~'NOTGONE~~'and type_code <> ~~'DEADLINE~~'and type_code <> ~~'SCD/PROM~~'and type_code <> ~~'TRANSIT~~'and type_code <> ~~' ~~',1,0)'"

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "update_ind.Protect='0 ~tIf(type_code <>~~'PUL~~'and type_code <> ~~'EXPEDITE~~'and type_code <> ~~'NOTGONE~~'and type_code <> ~~'DEADLINE~~'and type_code <> ~~'SCD/PROM~~'and type_code <> ~~'TRANSIT~~'and type_code <> ~~' ~~',1,0)'"

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "req_review_date.Protect='0 ~tIf(type_code <>~~'PUL~~'and type_code <> ~~'EXPEDITE~~'and type_code <> ~~'NOTGONE~~'and type_code <> ~~'DEADLINE~~'and type_code <> ~~'SCD/PROM~~'and type_code <> ~~'TRANSIT~~'and type_code <> ~~' ~~',1,0)'"

	ls_rtn = dw_1.Modify(ls_modstring)

	ls_modstring = "req_review_time.Protect='0 ~tIf(type_code <>~~'PUL~~'and type_code <> ~~'EXPEDITE~~'and type_code <> ~~'NOTGONE~~'and type_code <> ~~'DEADLINE~~'and type_code <> ~~'SCD/PROM~~'and type_code <> ~~'TRANSIT~~'and type_code <> ~~' ~~',1,0)'"

	ls_rtn = dw_1.Modify(ls_modstring)

end if

//dw_1.SetRedraw(TRUE)
return(1)      
end function

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer
Boolean	lb_division			
String	ls_temp

li_rc = wf_Changed()

CHOOSE CASE li_rc	//begin 1
	CASE  1	//	a valid change occurred ...
		li_answer = MessageBox("Wait",is_closemessage_txt,question!,yesnocancel!)
		CHOOSE CASE li_answer
			CASE 1	//	yes, save changes
				IF wf_Update() THEN	//	the save succeeded
				ELSE	//	the save failed
					li_answer = MessageBox("Save Failed","Retrieve Anyway?",question!,yesno!)
					IF li_answer = 1 THEN	//	retrieve w/o saving changes
					ELSE	//	cancel the close
						Return( false )
					END IF
				END IF	
			CASE 2 	//	retrieve w/o saving changes
			CASE 3	//	cancel the retrieve
				Return(false)
		END CHOOSE
	CASE  -1 		//	a validation error occurred
		li_answer = MessageBox("Data Entry Error","Retrieve w/o Save?",question!,okcancel!)
		CHOOSE CASE li_answer
			CASE 1	//	retrieve w/o saving changes
			CASE 2	//	cancel the retrieve
				Return( false )
		END CHOOSE
	CASE  -2 	//	a serious validation error, window may not close in this state
		MessageBox("A Serious Error Has Occurred","Correct Error Before Retrieving!",exclamation!,ok!)
		Return(False)
	CASE ELSE	//	nothing has changed
END CHOOSE

OpenWithParm( w_hot_list_filter, iuo_hot_list, iw_frame )
iuo_hot_list = Message.PowerObjectParm

IF NOT IsValid(iuo_hot_list) then
    return TRUE
end if
// check to see if cancel was pressed
IF iuo_hot_list.cancel = 'T' THEN
	Return TRUE
ELSE
	dw_1.SetRedraw( False )
	dw_1.Reset()
  	li_rc = wf_load_hotlist ()
// ibdkdld
	if not ic_control = "L" THEN
		ls_temp = iuo_hot_list.division
		if ls_temp > "  " Then	lb_division = True
	End If	
	IF li_rc = 1 THEN
		IF ic_control = "L" THEN
				this.Title = "Hot List Load - " + iuo_hot_list.load_key 
		END IF
		IF ic_control = "B" THEN
			if lb_division Then
				this.Title = "Hot List Carrier - " + iuo_hot_list.start_carrier + " to " + iuo_hot_list.end_carrier + ', Division = ' + ls_temp
			Else
				this.Title = "Hot List Carrier - " + iuo_hot_list.start_carrier + " to " + iuo_hot_list.end_carrier + ', Division = ALL'
			End If
		END IF
		IF ic_control = "C" THEN
			if lb_division Then
				this.Title = "Hot List Complex - " + iuo_hot_list.complex + ', Division = ' + ls_temp
			Else
				this.Title = "Hot List Complex - " + iuo_hot_list.complex + ', Division = ALL'
			End IF
		END IF
		dw_1.Sort()
		dw_1.ResetUpdate()
      iw_frame.SetMicroHelp( "Ready")       
	END IF
END IF
dw_1.SetRedraw( True )
Return FALSE

end function

event ue_postopen;call super::ue_postopen;s_error	lstr_error_info


ic_control = Message.StringParm
iuo_hot_list.control_type = ic_control

lstr_error_info.se_event_name = "ue_postopen"
lstr_error_info.se_window_name = "w_hot_list"
lstr_error_info.se_procedure_name = "ue_postopen"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

is_group_id = iw_frame.iu_netwise_data.is_groupid

IF wf_retrieve() THEN Close( this )

end event

event open;call super::open;iu_otr003  =  CREATE u_otr003
iu_ws_hot_list = CREATE u_ws_hot_list

dw_1.InsertRow(0)
end event

event close;call super::close; DESTROY iu_otr003
 DESTROY iu_ws_hot_list
end event

on resize;call w_base_sheet_ext::resize;dw_1.height = WorkSpaceHeight() - 30
dw_1.Width	= WorkSpaceWidth()  -20


end on

on w_hot_list.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_hot_list.destroy
call super::destroy
destroy(this.dw_1)
end on

type dw_1 from u_base_dw_ext within w_hot_list
integer width = 2811
integer height = 1316
string dataobject = "d_hot_list"
boolean vscrollbar = true
end type

on ue_postconstructor;call u_base_dw_ext::ue_postconstructor;ib_updateable = TRUE
end on

event doubleclicked;call super::doubleclicked;character			lc_load_key[7], &
						lc_stop_code[2], &
                  lc_msg_ind[1]

string            ls_input_string,&
                  ls_type_code, &
						ls_msg_ind, &
						ls_load_key,&
						ls_stop_code, &
						ls_hot_msg, &
						ls_xmessage


IF is_ObjectAtPointer = "msg_box_ind" THEN
	IF Row > 0 THEN
		lc_load_key = This.object.load_key[Row]
		lc_msg_ind = This.object.msg_box_ind[Row]
      IF lc_msg_ind[1] = 'X' or  lc_msg_ind[1] = 'Y'  THEN
          ls_msg_ind = 'Y'
      END IF
		ls_load_key = lc_load_key
		ls_hot_msg = ls_load_key + '~t'  + ls_msg_ind 
		OpenWithParm( w_hot_list_messages, ls_hot_msg )
		ls_xmessage = Message.StringParm
		If ls_xmessage = 'T' then
		  This.SetItem(Row, "msg_box_ind", "X") 
   	END IF
	END IF
END IF

If is_ObjectAtPointer = "add_check_call_ind" Then 
     lc_load_key = This.object.load_key[Row]
     lc_stop_code = "01"
     ls_input_string =  lc_load_key +  lc_stop_code + "H"
     OpenWithParm(w_check_call_add_child, ls_input_string) 
     This.SetItem(Row, "add_check_call_ind", "X")  
End if

if row > 0 then
   ls_type_code = Trim(UPPER(dw_1.object.type_code[row]))
   CHOOSE CASE is_group_id
   CASE "603"
    	CHOOSE CASE ls_type_code
		CASE 'PUL', 'EXPEDITE', 'NOTGONE', 'DEADLINE', 'SCD/PROM', 'TRANSIT'
		CASE ELSE
 			dw_1.SelectRow( 0,false )
	      dw_1.SelectRow( row,true )
	      dw_1.ScrolltoRow(row)
		END CHOOSE
	CASE "601"
 		CHOOSE CASE ls_type_code
		CASE 'PUL', 'EXPEDITE', 'NOTGONE', 'DEADLINE', 'SCD/PROM', 'TRANSIT', ' '		
 			dw_1.SelectRow( 0,false )
	      dw_1.SelectRow( row,true )
	      dw_1.ScrolltoRow(row)
		END CHOOSE
	CASE ELSE
      CALL SUPER::CLICKED
   END CHOOSE
end if
end event

event clicked;string ls_type_code

IF row < 1 Then Return

ls_type_code = Trim(UPPER(dw_1.object.type_code[row]))

CHOOSE CASE is_group_id
   CASE "603"
    	CHOOSE CASE ls_type_code
		CASE 'PUL', 'EXPEDITE', 'NOTGONE', 'DEADLINE', 'SCD/PROM', 'TRANSIT'
		CASE ELSE
 			CALL SUPER::CLICKED
		END CHOOSE
	CASE "601"
 		CHOOSE CASE ls_type_code
		CASE 'PUL', 'EXPEDITE', 'NOTGONE', 'DEADLINE', 'SCD/PROM', 'TRANSIT', ' '		
 			CALL SUPER::CLICKED
		END CHOOSE
	CASE ELSE
      CALL SUPER::CLICKED
END CHOOSE

end event

event constructor;call super::constructor;//is_selection = "3"
ii_rc = GetChild( 'type_code', idwc_typecode )
datawindowchild dwc
dwc = idwc_typecode
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for hot list types ")

idwc_typecode.SetTrans(SQLCA)
idwc_typecode.Retrieve('HOTLIST')

end event

event itemchanged;call super::itemchanged;date	ld_req_review_date
Integer	li_cnt
string	ls_rectype

IF dwo.name = "req_review_time" THEN
	ld_req_review_date = dw_1.GetItemDate(row,"req_review_date")
	if isnull(ld_req_review_date) then
		dw_1.object.req_review_date[row] = today()
	end if
END IF

IF upper(dwo.name) = 'TYPE_CODE' THEN
	
	IF Trim(data) = "" THEN Return
	
	CONNECT USING SQLCA;

	ls_rectype = "HOTLIST"
	select count (type_code) 
			into  :li_cnt
			FROM tutltypes  
         WHERE record_type = :ls_rectype
         AND type_code = :Data;

	DISCONNECT USING SQLCA;

	IF li_cnt = 0  THEN
		MessageBox( "Error", data + " is Not a Hot List Type." )
		This.SelectText(1,Len(Data))
		Return 1
	END IF		
END IF

end event

event itemerror;call super::itemerror;If TRIM(This.GetText()) = "" Then
	RETURN 3
Else
	Return 2
End If


end event

