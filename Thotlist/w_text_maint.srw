HA$PBExportHeader$w_text_maint.srw
forward
global type w_text_maint from w_base_sheet_ext
end type
type dw_text_maint from u_base_dw_ext within w_text_maint
end type
end forward

global type w_text_maint from w_base_sheet_ext
integer width = 2318
integer height = 1404
string title = "Text Maintenance"
long backcolor = 12632256
dw_text_maint dw_text_maint
end type
global w_text_maint w_text_maint

type variables
u_otr003   iu_otr003
u_ws_text_maint   iu_ws_text_maint

char ic_req_type[1]

integer   ii_error_column

long   il_error_row

end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_pre_save ()
end prototypes

public function boolean wf_retrieve ();integer  li_rtn

long     ll_total_no_rows
	
string   ls_text_maint_string,  &
        	ls_req_record_type, &
         ls_refrom_ship_date, &
         ls_refetch_record_type, &
         ls_refetch_record_type_code 

s_error  lstr_Error_Info

setRedraw(False)

dw_text_maint.Reset()

IF ic_req_type[1] = 'H' then
	ls_req_record_type = 'HOTLIST '
else
	ls_req_record_type = 'LATECODE'
end if

ls_text_maint_string         = Space(3201)
ls_refetch_record_type       = Space(8)
ls_refetch_record_type_code  = Space(8)

//OpenWithParm( w_working_scroll,"Wait...Inquiring,TRUE",This)
iw_frame.SetMicroHelp ("Wait.. Inquiring Database")

//li_rtn = iu_otr003.nf_otrt38ar(ls_req_record_type, ls_text_maint_string,&
//	 		ls_refetch_record_type, ls_refetch_record_type_code, &
//			lstr_error_info, 0)
  li_rtn = iu_ws_text_maint.uf_otrt38er(ls_req_record_type, ls_text_maint_string, lstr_error_info)

// Check the return code from the above function call
If li_rtn <> 0 Then
 		dw_text_maint.ResetUpdate()
      Return False 
End If

li_rtn = dw_text_maint.ImportString(Trim(ls_text_maint_string))

	
IF ic_req_type[1] = 'H' then
	   ls_refetch_record_type  = 'HOTLIST '
Else
	   ls_refetch_record_type  = 'LATECODE'        
End If

ls_refetch_record_type_code = &
	dw_text_maint.GetItemString(dw_text_maint.RowCount(), "record_type_code")

Do While li_rtn = 90
		ls_text_maint_string = Space(3201)
	    
      li_rtn = iu_ws_text_maint.uf_otrt38er(ls_req_record_type, ls_text_maint_string, lstr_error_info)

     // Check the return code from the above function call       
        If li_rtn <> 0 Then
      		dw_text_maint.ResetUpdate()
            Return False 
        End If

   li_rtn = dw_text_maint.ImportString(Trim(ls_text_maint_string))

	IF ic_req_type[1] = 'H' then
	   ls_refetch_record_type  = 'HOTLIST '
   Else
	   ls_refetch_record_type  = 'LATECODE'        
   End If

   ls_refetch_record_type_code = &
		dw_text_maint.GetItemString(dw_text_maint.RowCount(), "record_type_code")

Loop

dw_text_maint.SetFocus()

dw_text_maint.ResetUpdate()

ll_total_no_rows = dw_text_maint.RowCount()
ll_total_no_rows = dw_text_maint.InsertRow(ll_total_no_rows + 1)
dw_text_maint.SetRow(ll_total_no_rows)

SetRedraw(True)

Return TRUE



end function

public function boolean wf_update ();integer  li_rtn, &
			li_modified_ind 

long     ll_Row, &
         ll_Modified_Ctr, &
         ll_newrow
	
string   ls_return_error_msg, &   
         ls_text_maint_string, &
         ls_record_type, &
         ls_record_type_code, &
         ls_type_short_descr, & 
         ls_type_description, &
         ls_refetch_record_type, &
         ls_refetch_record_type_code   

s_error  lstr_Error_Info


dw_text_maint.AcceptText()

ll_row = 0

If wf_pre_save() = TRUE Then 
	
ll_Row = dw_text_maint.GetNextModified( ll_Row, Primary! )

Do While ll_row <> 0
      IF ic_req_type[1] = 'H' then
	       ls_record_type  = 'HOTLIST '
      Else
	       ls_record_type  = 'LATECODE'        
      End If
 
      ls_record_type_code = dw_text_maint.object.record_type_code[ll_row]
      ls_type_short_descr = dw_text_maint.object.type_short_descr[ll_row]
      ls_type_description = dw_text_maint.object.type_description[ll_row]
         
      iw_frame.SetMicroHelp ("Wait.. Updating Database")

//      li_rtn = iu_otr003.nf_otrt39ar(ls_record_type, ls_record_type_code, &
//	 	      	ls_type_short_descr, ls_type_description, &
//			      lstr_error_info, 0)

        li_rtn = iu_ws_text_maint.uf_otrt39er(ls_record_type, ls_record_type_code, ls_type_short_descr, ls_type_description, lstr_error_info)

// Check the return code from the above function call
      If li_rtn < 0 Then
 	   	Return False 
      End If
ll_Row = dw_text_maint.GetNextModified( ll_Row, Primary! ) 
LOOP
End if


dw_text_maint.SetFocus()
dw_text_maint.SetColumn("record_type_code")

ll_newrow = dw_text_maint.InsertRow(0) 
dw_text_maint.ScrollToRow(ll_newrow)

Return TRUE
end function

public function boolean wf_pre_save ();long      ll_Row

string    ls_RecordTypeCode, &
          ls_TypeShortDescr, &
          ls_TypeDescription 


ll_Row = 0

dw_text_maint.AcceptText()

ll_Row = dw_text_maint.GetNextModified(ll_Row, PRIMARY!)

Do while ll_row <> 0
  ls_RecordTypeCode = dw_text_maint.GetItemString(ll_Row, "record_type_code")
  If IsNull(ls_RecordTypeCode) or ls_RecordTypeCode = '          ' then
     	MessageBox("Record Type Code", "Please enter a Record Type Code") 
      dw_text_maint.SetColumn("record_type_code")     
//			ii_error_column = dw_text_maint.getcolumn()
//         il_error_row = ll_Row  
//			dw_text_maint.postevent("ue_post_tab")
      Return FALSE
   End if
   ls_TypeShortDescr = dw_text_maint.GetItemString(ll_Row, "type_short_descr")
   If IsNull(ls_TypeShortDescr) or ls_TypeShortDescr = '              ' then
     	MessageBox("Short Description", "Please enter a Short Description") 
      dw_text_maint.SetColumn("type_short_descr")     
//			ii_error_column = dw_text_maint.getcolumn()
//         il_error_row = ll_Row  
//			dw_text_maint.postevent("ue_post_tab")
      Return FALSE
    End if
    ls_TypeDescription = dw_text_maint.GetItemString(ll_Row, "type_description")
    If IsNull(ls_TypeDescription) or ls_TypeDescription = '                                     ' then
    	MessageBox("Type Description", "Please enter a Type Description") 
      dw_text_maint.SetColumn("type_description")     
//			ii_error_column = dw_text_maint.getcolumn()
//         il_error_row = ll_Row  
//			dw_text_maint.postevent("ue_post_tab")
     Return FALSE
   End if
 
ll_Row = dw_text_maint.GetNextModified(ll_Row, PRIMARY!)

Loop 

Return TRUE
end function

event close;call super::close;//DESTROY iu_otr003
  DESTROY iu_ws_text_maint
end event

event open;call super::open;//iu_otr003  =  CREATE u_otr003
  iu_ws_text_maint = CREATE u_ws_text_maint

ic_req_type = Message.StringParm

IF ic_req_type[1] = "L" THEN
	Title = "Late Reasons"
   wf_retrieve()
ELSE
	Title = "Hot List Reasons"
   wf_retrieve()  
END IF




end event

on w_text_maint.create
int iCurrent
call super::create
this.dw_text_maint=create dw_text_maint
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_text_maint
end on

on w_text_maint.destroy
call super::destroy
destroy(this.dw_text_maint)
end on

type dw_text_maint from u_base_dw_ext within w_text_maint
integer x = 50
integer y = 32
integer width = 2217
integer height = 1228
string dataobject = "d_text_maint"
boolean vscrollbar = true
boolean border = false
end type

