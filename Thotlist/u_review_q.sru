HA$PBExportHeader$u_review_q.sru
forward
global type u_review_q from nonvisualobject
end type
end forward

global type u_review_q from nonvisualobject autoinstantiate
end type

type variables
String	complex, &
			s_carrier, &
			e_carrier, &
			division, &
			plant

Boolean	cancel
end variables

on u_review_q.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_review_q.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

