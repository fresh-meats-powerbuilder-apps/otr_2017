HA$PBExportHeader$w_hot_list_messages.srw
forward
global type w_hot_list_messages from w_base_response_ext
end type
type dw_1 from u_base_dw_ext within w_hot_list_messages
end type
end forward

global type w_hot_list_messages from w_base_response_ext
integer x = 110
integer y = 268
integer width = 2747
integer height = 1404
string title = "Hot List Messages"
long backcolor = 12632256
dw_1 dw_1
end type
global w_hot_list_messages w_hot_list_messages

type variables
DataWindowChild	idwc_typecode
                    
integer  ii_rc

u_otr003   iu_otr003

u_ws_hot_list iu_ws_hot_list
end variables

forward prototypes
public function boolean wf_update_hot_msg ()
public subroutine wf_load_hot_msg (string as_hot_msg)
end prototypes

public function boolean wf_update_hot_msg ();String      ls_load_key, &
				ls_stop_code, &
				ls_type_code, & 
				ls_msg

integer    li_rtn, &
			  li_rc

s_error    lstr_Error_Info

long		  ll_Row			  
							

dwItemStatus		status

IF dw_1.AcceptText() = -1 THEN Return(False)

lstr_error_info.se_event_name = "wf_update"
lstr_error_info.se_window_name = "w_hot_list"
lstr_error_info.se_procedure_name = "wf_update_hot_msg"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

dw_1.Setredraw(FALSE)
ll_row = 0
ll_Row = dw_1.GetNextModified( ll_Row, Primary! )

DO WHILE ll_Row <> 0
		ls_load_key			= dw_1.object.load_key[ll_Row]
		ls_stop_code		= dw_1.object.stop_code[ll_Row]
		ls_type_code 		= dw_1.object.type_code[ll_row]
		ls_msg				= dw_1.object.msg[ll_row]

//		li_rtn = iu_otr003.nf_otrt42ar(ls_load_key, &
//						         ls_stop_code, ls_type_code, ls_msg, &
//									lstr_error_info, 0)

		CHOOSE CASE li_rtn
			CASE is < 0 
				dw_1.Setredraw(TRUE)
				li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
										ls_load_key + " has failed do to an Error.  Do you want to Continue?", &
										Question!, YesNo! )
				dw_1.Setredraw(FALSE)
			CASE is > 0 
				dw_1.Setredraw(TRUE)
				li_rc = MessageBox( "Update Warning", lstr_Error_Info.se_message + &
										" Load Key = " +ls_load_key, &
										Information!, OK! )
				dw_1.Setredraw(FALSE)
			CASE ELSE
				li_rc = 0
		END CHOOSE

		IF li_rc = 2 THEN 			// NO  do not continue processing
			dw_1.Setredraw(TRUE)
			Return(False)
		END IF
	ll_Row = dw_1.GetNextModified( ll_Row, Primary! )
LOOP

dw_1.setredraw(true)

Return(True)
end function

public subroutine wf_load_hot_msg (string as_hot_msg);integer                 li_rtn, &
								li_column_count

string  						ls_cust_serv_msg_inq_string, &
                        ls_req_load_key, &
								ls_refetch_stop_code, &
								ls_refetch_update_date, &
								ls_refetch_update_time, &
								ls_string, &
								ls_temp, &
								ls_msg_ind

s_error                 lstr_Error_Info

SetPointer( HourGlass! )


lstr_error_info.se_event_name = "wf_load_hot_msg"
lstr_error_info.se_window_name = "w_hot_list_messages"
lstr_error_info.se_procedure_name = "wf_load_hot_msg"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ls_string = as_hot_msg
li_column_count = 1
DO WHILE li_column_count <= 2
	ls_temp = iw_frame.iu_string.nf_GetToken(ls_string, '~t')
	CHOOSE CASE li_column_count
		CASE 1
			ls_req_load_key = ls_temp
		CASE 2
			ls_msg_ind = ls_temp
	END CHOOSE
	li_column_count++
LOOP

if ls_msg_ind = 'Y' then
	ls_cust_serv_msg_inq_string = space(2341)
   ls_refetch_stop_code        = Space(2)
	ls_refetch_update_date      = space(10)
	ls_refetch_update_time      = Space(8)

//	li_rtn = iu_otr003.nf_otrt41ar(ls_req_load_key, &
//				ls_cust_serv_msg_inq_string, lstr_error_info, &
//				ls_refetch_stop_code, ls_refetch_update_date, &
//				ls_refetch_update_time, 0)

li_rtn = iu_ws_hot_list.nf_otrt41er(ls_req_load_key,ls_cust_serv_msg_inq_string,lstr_error_info)
			
	SetPointer( HourGlass! )

	// Check the return code from the above function call
	IF li_rtn <> 0 THEN
		Close( this )
		Return
	END IF

	li_rtn = dw_1.ImportString(Trim(ls_cust_serv_msg_inq_string))

	ls_cust_serv_msg_inq_string = space(2341)

	DO WHILE li_rtn = 20
	//	li_rtn = iu_otr003.nf_otrt41ar(ls_req_load_key, &
	//				ls_cust_serv_msg_inq_string, lstr_error_info, &
	//				ls_refetch_stop_code, ls_refetch_update_date, &
	//				ls_refetch_update_time, 0)
li_rtn = iu_ws_hot_list.nf_otrt41er(ls_req_load_key,ls_cust_serv_msg_inq_string,lstr_error_info)		
					
					
					
					

		li_rtn = dw_1.ImportString(Trim(ls_cust_serv_msg_inq_string))

		SetPointer( HourGlass! )
	LOOP
End if
dw_1.ResetUpdate()
//dw_1.insertrow(0)
//dw_1.Scrolltorow(dw_1.RowCount())   // force to last row
//dw_1.SetItem(dw_1.rowcount(),"load_key",ls_req_load_key)
//dw_1.SetItem(dw_1.rowcount(),"stop_code",'01')

integer li_ret_code

li_ret_code = dw_1.SetItemStatus (dw_1.rowcount(), 0, Primary!, NotModified! )

this.Title = "Hot List Messages Load - " + ls_req_load_key

SetPointer( Arrow! )
end subroutine

event open;call super::open;int li_rtn
String   ls_hot_msg

iu_otr003  =  CREATE u_otr003

iu_ws_hot_list = CREATE u_ws_hot_list

ls_hot_msg = Message.StringParm

wf_load_hot_msg(ls_hot_msg)
end event

event close;call super::close;DESTROY iu_otr003

DESTROY iu_ws_hot_list
end event

on w_hot_list_messages.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_hot_list_messages.destroy
call super::destroy
destroy(this.dw_1)
end on

event ue_base_ok;call super::ue_base_ok;Integer        li_rc
String			ls_xmessage

//dw_1.accepttext()

//IF dw_1.ModifiedCount( ) > 0 THEN 
//	li_rc = MessageBox( "Modified Data" , "Save changes before closing ?", &
//           Question! , YesNoCancel! )
//ELSE
//   Close ( w_hot_list_messages )
   iw_frame.SetMicroHelp ("Ready...")
//END IF 

//IF li_rc = 1 Then
//   ls_xmessage = 'T'
//	if wf_update_hot_msg() then closeWithReturn(This, ls_xmessage)
//ELSE
//	If li_rc = 2 then
//	   ls_xmessage = 'F'
		CloseWithReturn ( w_hot_list_messages, ls_xmessage )
//	End if	
//END IF
//
end event

event ue_base_cancel;call super::ue_base_cancel;String	ls_xmessage

ls_xmessage = 'F'
ClosewithReturn ( w_hot_list_messages, ls_xmessage)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_hot_list_messages
integer x = 1632
integer y = 1160
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_hot_list_messages
integer x = 1275
integer y = 1160
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_hot_list_messages
integer x = 919
integer y = 1160
integer taborder = 20
end type

type dw_1 from u_base_dw_ext within w_hot_list_messages
integer x = 5
integer width = 2725
integer height = 1120
integer taborder = 10
string dataobject = "d_hot_list_messages"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event itemerror;call super::itemerror;Return 2 
end event

event itemchanged;call super::itemchanged;long	ll_foundrow

IF upper(dwo.name) = 'TYPE_CODE' THEN
	IF Trim(data) = "" THEN Return
	
	long ll_nbrrows

	ll_nbrrows = idwc_typecode.RowCount()

	ll_FoundRow = idwc_typecode.Find ( "type_code='"+data+"'", 1, ll_nbrrows )

	IF ll_FoundRow < 1 THEN
		MessageBox( "Error", data + " is Not a Hot List Type." )
		Return 1
	END IF		
END IF
end event

event constructor;call super::constructor;ii_rc = GetChild( 'type_code', idwc_typecode )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for hot list types ")

idwc_typecode.SetTrans(SQLCA)
idwc_typecode.Retrieve('HOTLIST')

end event

