HA$PBExportHeader$w_hot_list_filter.srw
forward
global type w_hot_list_filter from w_base_response_ext
end type
type dw_hot_list_filter from u_base_dw_ext within w_hot_list_filter
end type
type dw_1 from u_base_dw_ext within w_hot_list_filter
end type
type dw_2 from u_base_dw_ext within w_hot_list_filter
end type
type dw_division from datawindow within w_hot_list_filter
end type
type gb_1 from groupbox within w_hot_list_filter
end type
type gb_2 from groupbox within w_hot_list_filter
end type
end forward

global type w_hot_list_filter from w_base_response_ext
integer x = 521
integer y = 392
integer width = 1842
integer height = 1136
string title = "Hot List Filter"
long backcolor = 12632256
dw_hot_list_filter dw_hot_list_filter
dw_1 dw_1
dw_2 dw_2
dw_division dw_division
gb_1 gb_1
gb_2 gb_2
end type
global w_hot_list_filter w_hot_list_filter

type variables
uo_hot_list	iuo_hot_list

integer	ii_rc

datawindowchild	idwc_typecode, &
		idwc_complex,idwc_division

String is_division
end variables

event open;call super::open;Date			ld_hold_date

iuo_hot_list = Message.PowerObjectParm
dw_hot_list_filter.Object.Data = iuo_hot_list
dw_1.insertrow(0)
dw_2.insertrow(0)

IF  iuo_hot_list.control_type = 'L' THEN
 	dw_hot_list_filter.SetTabOrder("start_carrier",0)
	dw_hot_list_filter.SetTabOrder("end_carrier",0)
 	dw_hot_list_filter.SetTabOrder("complex",0)
	dw_hot_list_filter.SetTabOrder("hotlist_type",0)
	dw_hot_list_filter.SetTabOrder("priority_level",0)
	dw_hot_list_filter.SetTabOrder("review_status_ind",0)
//	dw_1.SetTabOrder("from_date",0)
//	dw_1.SetTabOrder("to_date",0)
//	dw_2.SetTabOrder("from_date",0)
//	dw_2.SetTabOrder("to_date",0)
	dw_1.Enabled = False
	dw_2.Enabled = False
	dw_division.enabled = False
END IF
IF  iuo_hot_list.control_type = 'B'  THEN
	dw_hot_list_filter.SetTabOrder("load_number",0)
	dw_hot_list_filter.SetTabOrder("complex",0)
	IF iuo_hot_list.del_from_date <> '' THEN 
		ld_hold_date = date(left(dw_hot_list_filter.object.del_from_date[1], 4)+'-'+mid(dw_hot_list_filter.object.del_from_date[1], 6, 2)+'-'+right(dw_hot_list_filter.object.del_from_date[1], 2))
		dw_1.object.from_date[1] = ld_hold_date
	END IF
	IF iuo_hot_list.del_to_date <> '' THEN 
		ld_hold_date = date(left(dw_hot_list_filter.object.del_to_date[1], 4)+'-'+mid(dw_hot_list_filter.object.del_to_date[1], 6, 2)+'-'+right(dw_hot_list_filter.object.del_to_date[1], 2))
		dw_1.object.to_date[1] = ld_hold_date
	END IF
	IF iuo_hot_list.ship_from_date <> '' THEN 
		ld_hold_date = date(left(dw_hot_list_filter.object.ship_from_date[1], 4)+'-'+mid(dw_hot_list_filter.object.ship_from_date[1], 6, 2)+'-'+right(dw_hot_list_filter.object.ship_from_date[1], 2))
		dw_2.object.from_date[1] = ld_hold_date
	END IF
	IF iuo_hot_list.ship_to_date <> '' THEN 
		ld_hold_date = date(left(dw_hot_list_filter.object.ship_to_date[1], 4)+'-'+mid(dw_hot_list_filter.object.ship_to_date[1], 6, 2)+'-'+right(dw_hot_list_filter.object.ship_to_date[1], 2))
		dw_2.object.to_date[1] = ld_hold_date
	END IF
END IF
IF   iuo_hot_list.control_type  = 'C'  THEN
	dw_hot_list_filter.SetTabOrder("load_number",0)
	dw_hot_list_filter.SetTabOrder( "start_carrier", 0)
	dw_hot_list_filter.SetTabOrder( "end_carrier", 0)
	IF iuo_hot_list.del_from_date <> '' THEN 
		ld_hold_date = date(left(dw_hot_list_filter.object.del_from_date[1], 4)+'-'+mid(dw_hot_list_filter.object.del_from_date[1], 6, 2)+'-'+right(dw_hot_list_filter.object.del_from_date[1], 2))
		dw_1.object.from_date[1] = ld_hold_date
	END IF
	IF iuo_hot_list.del_to_date <> '' THEN 
		ld_hold_date = date(left(dw_hot_list_filter.object.del_to_date[1], 4)+'-'+mid(dw_hot_list_filter.object.del_to_date[1], 6, 2)+'-'+right(dw_hot_list_filter.object.del_to_date[1], 2))
		dw_1.object.to_date[1] = ld_hold_date
	END IF
	IF iuo_hot_list.ship_from_date <> '' THEN 
		ld_hold_date = date(left(dw_hot_list_filter.object.ship_from_date[1], 4)+'-'+mid(dw_hot_list_filter.object.ship_from_date[1], 6, 2)+'-'+right(dw_hot_list_filter.object.ship_from_date[1], 2))
		dw_2.object.from_date[1] = ld_hold_date
	END IF
	IF iuo_hot_list.ship_to_date <> '' THEN 
		ld_hold_date = date(left(dw_hot_list_filter.object.ship_to_date[1], 4)+'-'+mid(dw_hot_list_filter.object.ship_to_date[1], 6, 2)+'-'+right(dw_hot_list_filter.object.ship_to_date[1], 2))
		dw_2.object.to_date[1] = ld_hold_date
	END IF

	dw_hot_list_filter.SetTabOrder("load_number",0)
END IF
//ibdkdld
//if iuo_hot_list.division
dw_division.setitem(1,"division_code", iuo_hot_list.division)
is_division = iuo_hot_list.division
dw_hot_list_filter.SetFocus()
end event

on w_hot_list_filter.create
int iCurrent
call super::create
this.dw_hot_list_filter=create dw_hot_list_filter
this.dw_1=create dw_1
this.dw_2=create dw_2
this.dw_division=create dw_division
this.gb_1=create gb_1
this.gb_2=create gb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_hot_list_filter
this.Control[iCurrent+2]=this.dw_1
this.Control[iCurrent+3]=this.dw_2
this.Control[iCurrent+4]=this.dw_division
this.Control[iCurrent+5]=this.gb_1
this.Control[iCurrent+6]=this.gb_2
end on

on w_hot_list_filter.destroy
call super::destroy
destroy(this.dw_hot_list_filter)
destroy(this.dw_1)
destroy(this.dw_2)
destroy(this.dw_division)
destroy(this.gb_1)
destroy(this.gb_2)
end on

event ue_base_cancel;call super::ue_base_cancel;iuo_hot_list.cancel = 'T'
CloseWithReturn( This, iuo_hot_list )

end event

event ue_base_ok;call super::ue_base_ok;char			lc_req_load_key[7], &
				lc_req_from_carrier[4], &
				lc_req_to_carrier[4], &
				lc_req_review_status, &
				lc_req_type_code[8], &
				lc_req_priority, &
				lc_req_ship_from_date[10], &
				lc_req_ship_to_date[10], &
				lc_req_delv_from_date[10], &
				lc_req_delv_to_date[10], &
				lc_refetch_load_key[7], &
				lc_req_complex[3]
							

Date			ld_ship_from_date, &
				ld_ship_to_date, &
				ld_delv_from_date, &
				ld_delv_to_date

dw_1.AcceptText()
dw_2.AcceptText()
dw_division.accepttext()

If  dw_hot_list_filter.AcceptText() = 1 Then
  iuo_hot_list = dw_hot_list_filter.object.data[1]
  IF iuo_hot_list.control_type = 'L' And iuo_hot_list.load_key = ""  Then
			MessageBox("Require Load Key", "Load key is required, please enter a value.")
			dw_hot_list_filter.SetColumn("load_number")
			dw_hot_list_filter.SetFocus()
		ElseIf iuo_hot_list.control_type = 'C' and iuo_hot_list.complex = "" Then
			MessageBox("Require Complex", "Complex is required, please enter a value.")
			dw_hot_list_filter.SetColumn("complex")
			dw_hot_list_filter.SetFocus()
   Else	
			
	ld_delv_from_date    = dw_1.GetItemDate( 1, "from_date" )
	if string(ld_delv_from_date,"yyyy-mm-dd") = '1900-01-01' then
		lc_req_delv_from_date = '          '
	else
		lc_req_delv_from_date = String(ld_delv_from_date, "yyyy-mm-dd") 
	end if
	ld_delv_to_date    = dw_1.GetItemDate( 1, "to_date" )
	if string(ld_delv_to_date,"yyyy-mm-dd") = '1900-01-01' then
		lc_req_delv_to_date = '          '
	else
		lc_req_delv_to_date = String(ld_delv_to_date, "yyyy-mm-dd") 
	end if
	
	ld_ship_from_date    = dw_2.GetItemDate( 1, "from_date" )
	if string(ld_ship_from_date,"yyyy-mm-dd") = '1900-01-01' then
		lc_req_ship_from_date = '          '
	else
		lc_req_ship_from_date = String(ld_ship_from_date, "yyyy-mm-dd") 
	end if

	ld_ship_to_date    = dw_2.GetItemDate( 1, "to_date" )
	if string(ld_ship_to_date,"yyyy-mm-dd") = '1900-01-01' then
		lc_req_ship_to_date = '          '
	else
		lc_req_ship_to_date = String(ld_ship_to_date, "yyyy-mm-dd") 
	end if

//ibdkdld
	dw_division.accepttext()
	If IsNull(is_division) Then
		is_division = "  "
	End If

	iuo_hot_list.ship_from_date	 = lc_req_ship_from_date
	iuo_hot_list.ship_to_date		 = lc_req_ship_to_date
	iuo_hot_list.del_from_date	 	= lc_req_delv_from_date
	iuo_hot_list.del_to_date		 = lc_req_delv_to_date
//ibdkdld	
	iuo_hot_list.division			= is_division	
	iuo_hot_list.cancel = 'F'
	CloseWithReturn( This, iuo_hot_list )
	Return
  End If
Else
	 dw_hot_list_filter.SetFocus()
End if	 

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_hot_list_filter
integer x = 1125
integer y = 904
integer taborder = 70
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_hot_list_filter
integer x = 786
integer y = 908
integer taborder = 60
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_hot_list_filter
integer x = 453
integer y = 908
integer taborder = 50
end type

type dw_hot_list_filter from u_base_dw_ext within w_hot_list_filter
event ue_dropdown pbm_dwndropdown
integer x = 37
integer y = 32
integer width = 1797
integer height = 484
integer taborder = 10
string dataobject = "d_hot_list_filter"
boolean border = false
end type

event ue_dropdown;String	ls_getcolumnname

long		ll_RowCount

ls_getColumnname	=	This.GetColumnName()
CHOOSE CASE	Upper(ls_getcolumnname)
	CASE	'HOTLIST_TYPE'
		ll_RowCount	=	idwc_typecode.RowCount()
		IF ll_RowCount <= 0 THEN
			idwc_typecode.SetTrans(SQLCA)
			idwc_typecode.Retrieve('HOTLIST')
		END IF
	CASE	'COMPLEX'
		ll_RowCount	=	idwc_complex.RowCount()
		IF ll_RowCount <= 0 THEN
			idwc_complex.SetTrans(SQLCA)
			idwc_complex.Retrieve()
		END IF
		// ibdkdld
	CASE	'DIVISION'
		ll_RowCount	=	idwc_division.RowCount()
		IF ll_RowCount <= 0 THEN
			idwc_division.SetTrans(SQLCA)
			idwc_division.Retrieve()
		END IF
END CHOOSE

end event

event itemchanged;call super::itemchanged;long ll_FoundRow, &
	  ll_nbrRow 	


IF upper(dwo.name) = 'COMPLEX' THEN
//	IF Trim(data) = "" THEN Return
	
	ll_nbrRow = idwc_complex.RowCount()
	ll_FoundRow = idwc_complex.Find ( "type_code='"+data+"'", 1, ll_nbrRow )

	IF ll_FoundRow < 1 THEN
		MessageBox( "Complex Error", data + " is Not a Valid Complex Code." )
		This.SelectText(1,Len(data))
		Return 1
	END IF		
END IF
end event

event itemerror;call super::itemerror;Return 1
end event

event constructor;call super::constructor;ii_rc = GetChild( 'hotlist_type', idwc_typecode )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for hot list types ")

ii_rc = GetChild( 'complex', idwc_complex )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for complex codes ")


idwc_typecode.SetTrans(SQLCA)
idwc_typecode.Retrieve('HOTLIST')

idwc_complex.SetTrans(SQLCA)
idwc_complex.Retrieve()


end event

event getfocus;call super::getfocus;THIS.SelectText (1,99)
end event

type dw_1 from u_base_dw_ext within w_hot_list_filter
integer x = 55
integer y = 580
integer width = 800
integer height = 204
integer taborder = 30
string dataobject = "d_from_and_to_dates"
boolean border = false
end type

event itemchanged;call super::itemchanged;Date		ld_from, &
			ld_to


If dwo.name = "to_date" Then
	ld_from = dw_1.GetItemDate(1,"from_date")
	ld_to = Date(data)
	If ld_from > ld_to Then
		MessageBox("Date error", "To Date can not be less than the From Date, please reenter dates")
		SelectText(1,10)
		Return 1
	End If
End If
end event

event itemerror;call super::itemerror;Return 1
end event

type dw_2 from u_base_dw_ext within w_hot_list_filter
integer x = 978
integer y = 580
integer width = 795
integer height = 204
integer taborder = 40
string dataobject = "d_from_and_to_dates"
boolean border = false
end type

event itemchanged;call super::itemchanged;Date		ld_from, &
			ld_to


If dwo.name = "to_date" Then
	ld_from = dw_2.GetItemDate(1,"from_date")
	ld_to = Date(data)
	If ld_from > ld_to Then
			MessageBox("Date error", "To Date can not be less than the From Date, please reenter dates")
			SelectText(1,10)
			Return 1
	End If
End If
end event

event itemerror;call super::itemerror;Return 1
end event

type dw_division from datawindow within w_hot_list_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 1207
integer y = 332
integer width = 398
integer height = 172
integer taborder = 20
boolean bringtotop = true
string title = "none"
string dataobject = "d_dddw_divisions"
boolean controlmenu = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_dwndropdown;Long ll_row

ll_row = idwc_division.Find( "type_code ='" + GetText() + "'", 1, idwc_division.RowCount() )

If (ll_row = 0) Then
	idwc_division.SetTrans(SQLCA)
	idwc_division.Retrieve('DIVCODE')
Else 
	idwc_division.ScrollToRow( ll_row )
	idwc_division.SelectRow( ll_row, True )
End If

end event

event constructor;insertRow(0)
ii_rc = GetChild( 'division_code', idwc_division )
IF ii_rc = -1 THEN 
	MessageBox("DataWindwoChild Error", "Unable to get Child Handle for division_code.")
else
	idwc_division.SetTrans(SQLCA)
	idwc_division.Retrieve('DIVCODE')
End IF
end event

event getfocus;THIS.SelectText (1,99)
end event

event itemchanged;long ll_FoundRow
iw_frame.SetMicroHelp(dwo.name)
IF Trim(data) = "" THEN 
	is_division = data
	Return
End if

ll_FoundRow = idwc_division.Find ( "type_code='"+data+"'", 1, idwc_division.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Division Error", data + " is Not a Valid Division Code." )
	Return 1
else
	is_division = data
	
END IF		

end event

event itemerror;Return 1
end event

type gb_1 from groupbox within w_hot_list_filter
integer x = 32
integer y = 512
integer width = 846
integer height = 296
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Delivery"
end type

type gb_2 from groupbox within w_hot_list_filter
integer x = 951
integer y = 512
integer width = 846
integer height = 296
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Ship"
end type

