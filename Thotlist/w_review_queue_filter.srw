HA$PBExportHeader$w_review_queue_filter.srw
forward
global type w_review_queue_filter from w_base_response_ext
end type
type dw_start_end_carriers from u_base_dw_ext within w_review_queue_filter
end type
type gb_1 from groupbox within w_review_queue_filter
end type
type dw_complex from u_base_dw_ext within w_review_queue_filter
end type
type dw_division from datawindow within w_review_queue_filter
end type
type dw_plants from u_base_dw_ext within w_review_queue_filter
end type
end forward

global type w_review_queue_filter from w_base_response_ext
integer x = 974
integer y = 488
integer width = 978
integer height = 1096
string title = "Review Queue Filter"
long backcolor = 12632256
dw_start_end_carriers dw_start_end_carriers
gb_1 gb_1
dw_complex dw_complex
dw_division dw_division
dw_plants dw_plants
end type
global w_review_queue_filter w_review_queue_filter

type variables
integer	ii_rc

String	is_start_carrier, &
	is_end_carrier, &
	is_complex, &
	is_cancel, is_division, is_plant

u_review_q	iu_review_q


DataWindowChild    idwc_division, idwc_plant


end variables

event open;call super::open;Long		ll_FoundRow

dw_start_end_carriers.InsertRow(0)
dw_complex.InsertRow(0)

iu_review_q = Message.PowerObjectParm

IF iu_review_q.s_carrier <> '' THEN 
	dw_start_end_carriers.SetItem( 1, "start_carrier", iu_review_q.s_carrier)
END IF

IF iu_review_q.e_carrier <> '' THEN 
	dw_start_end_carriers.SetItem( 1, "end_carrier", iu_review_q.e_carrier)
END IF


IF iu_review_q.complex <> '' THEN 
	ll_FoundRow = dw_complex.Find ( "type_code='"+iu_review_q.complex+"'", 0, dw_complex.RowCount())
	If ll_FoundRow > 0 then
	   dw_complex.ScrollToRow(ll_FoundRow)
	   dw_complex.SelectRow(ll_FoundRow, True)
	END IF
END IF

IF iu_review_q.division <> '' THEN 
	dw_division.SetItem( 1, "division_code", iu_review_q.division)
	is_division = iu_review_q.division
END IF

If iu_review_q.plant <> '' Then
	dw_plants.SetItem( 1, "plant_code", iu_review_q.plant)
	is_plant = iu_review_q.plant
End If

dw_start_end_carriers.SetFocus()
This.dw_start_end_carriers.ResetUpdate()
This.dw_complex.ResetUpdate()

end event

on w_review_queue_filter.create
int iCurrent
call super::create
this.dw_start_end_carriers=create dw_start_end_carriers
this.gb_1=create gb_1
this.dw_complex=create dw_complex
this.dw_division=create dw_division
this.dw_plants=create dw_plants
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_start_end_carriers
this.Control[iCurrent+2]=this.gb_1
this.Control[iCurrent+3]=this.dw_complex
this.Control[iCurrent+4]=this.dw_division
this.Control[iCurrent+5]=this.dw_plants
end on

on w_review_queue_filter.destroy
call super::destroy
destroy(this.dw_start_end_carriers)
destroy(this.gb_1)
destroy(this.dw_complex)
destroy(this.dw_division)
destroy(this.dw_plants)
end on

event ue_base_cancel;
iu_review_q.cancel = TRUE
CloseWithReturn( This, iu_review_q )
Return
 
end event

event ue_base_ok;Boolean						lb_cancel

Char                    lc_req_from_carrier[4], &
								lc_req_to_carrier[4]

							
String						ls_review_queue, &
					      	ls_cancel, &
								ls_complex_code
								
Long							ll_row								

dw_start_end_carriers.AcceptText()
If ((len(Trim(dw_start_end_carriers.object.start_carrier[1])) > 0) or &
    (len(Trim(dw_start_end_carriers.object.end_carrier[1])) > 0)) Then
	is_start_carrier	= trim( dw_start_end_carriers.object.start_carrier[1])
	is_end_carrier 	= trim( dw_start_end_carriers.object.end_carrier[1])
	is_cancel = 'false'
	lb_cancel = false
	If Trim(is_start_carrier) = "" then
		is_start_carrier = space(4)
	end if
	If Trim(is_end_carrier) = "" then
		is_end_carrier = is_start_carrier
	end if
else
	is_start_carrier = space(4)
	is_end_carrier   = space(4)
End if

dw_complex.AcceptText()
ll_row = dw_complex.GetSelectedRow(0)
If ll_row > 0 Then
	If len(Trim(dw_complex.object.type_code[ll_row])) > 0 Then
		is_complex = trim( dw_complex.object.type_code[ll_row])
		is_cancel = 'false'
		lb_cancel = false
	Else
		is_complex = space(3)
	End If
Else
	is_complex = space(3)
End If

dw_division.AcceptText()
If IsNull(is_division) Then
	is_division = "  "
End If

dw_plants.AcceptText()
If IsNull(is_plant) Then
	is_plant = "   "
Else
	is_plant = dw_plants.GetItemString(1 , "plant_code")
End If

lb_cancel = False
iu_review_q.s_carrier  = is_start_carrier
iu_review_q.e_carrier  = is_end_carrier
iu_review_q.complex    = is_complex
iu_review_q.cancel     = lb_cancel
iu_review_q.division   = is_division
iu_review_q.plant		  = is_plant

CloseWithReturn( this, iu_review_q )
Return

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_review_queue_filter
integer x = 649
integer y = 872
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_review_queue_filter
integer x = 343
integer y = 872
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_review_queue_filter
integer x = 41
integer y = 872
integer taborder = 40
end type

type dw_start_end_carriers from u_base_dw_ext within w_review_queue_filter
integer x = 101
integer y = 84
integer width = 782
integer height = 156
integer taborder = 10
string dataobject = "d_start_end_carriers"
boolean border = false
end type

on itemfocuschanged;call u_base_dw_ext::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)

end on

type gb_1 from groupbox within w_review_queue_filter
integer x = 50
integer y = 20
integer width = 850
integer height = 244
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Carriers"
end type

type dw_complex from u_base_dw_ext within w_review_queue_filter
integer x = 50
integer y = 324
integer width = 850
integer height = 328
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_complex_info"
boolean vscrollbar = true
end type

event constructor;call super::constructor;
dw_complex.SetTrans(SQLCA)
dw_complex.Retrieve()
is_selection = '1'


end event

event itemchanged;call super::itemchanged;long ll_FoundRow

IF Trim(data) = "" THEN Return

ll_FoundRow = dw_complex.Find ( "type_code='"+data+"'", 1, dw_complex.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Complex Error", data + " is Not a Valid Complex Code." )
//   dw_carriers.SetFocus()  
//   dw_carriers.SetColumn("carrier_code")
	This.SelectText(1,Len(Data))
   Return 1
END IF		

end event

event clicked;If row < 1 Then
	Return
End If

If Not IsSelected(row) Then 
	SelectRow(row, True)
Else
	SelectRow(row, False)
End if

//If IsSelected(row) Then 
//	SelectRow(row, False)
//End if
end event

type dw_division from datawindow within w_review_queue_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 46
integer y = 656
integer width = 389
integer height = 180
integer taborder = 30
boolean bringtotop = true
string title = "none"
string dataobject = "d_dddw_divisions"
boolean border = false
boolean livescroll = true
end type

event ue_dwndropdown;Long ll_row

ll_row = idwc_division.Find( "type_code ='" + GetText() + "'", 1, idwc_division.RowCount() )

If (ll_row = 0) Then
	idwc_division.SetTrans(SQLCA)
	idwc_division.Retrieve('DIVCODE')
Else 
	idwc_division.ScrollToRow( ll_row )
	idwc_division.SelectRow( ll_row, True )
End If


end event

event constructor;insertRow(0)
ii_rc = GetChild( 'division_code', idwc_division )
IF ii_rc = -1 THEN 
	MessageBox("DataWindwoChild Error", "Unable to get Child Handle for division_code.")
else
	idwc_division.SetTrans(SQLCA)
	idwc_division.Retrieve('DIVCODE')
End IF
end event

event getfocus;THIS.SelectText (1,99)
end event

event itemchanged;long ll_FoundRow
iw_frame.SetMicroHelp(dwo.name)
IF Trim(data) = "" THEN 
	is_division = data
	Return
End if

ll_FoundRow = idwc_division.Find ( "type_code='"+data+"'", 1, idwc_division.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Division Error", data + " is Not a Valid Division Code." )
	Return 1
else
	is_division = data
END IF
end event

event itemerror;Return 1
end event

type dw_plants from u_base_dw_ext within w_review_queue_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 494
integer y = 680
integer width = 425
integer height = 164
integer taborder = 21
boolean bringtotop = true
string dataobject = "d_dddw_plants"
boolean border = false
end type

event ue_dwndropdown;Long ll_row

ll_row = idwc_plant.Find( "location_code ='" + GetText() + "'", 1, idwc_plant.RowCount() )

If (ll_row = 0) Then
	idwc_plant.SetTrans( SQLCA )
	idwc_plant.Retrieve()
Else 
	idwc_plant.ScrollToRow( ll_row )
	idwc_plant.SelectRow( ll_row, True )
End If
end event

event constructor;call super::constructor;String 		ls_value

ii_rc = GetChild( 'plant_code', idwc_plant )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for plant_code.")

dw_plants.insertrow(0)

ls_value = ProfileString(iw_frame.is_userini, "OTR", "lastplant", "") 

dw_plants.SetItem( 1, "plant_code", ls_value)

end event

event destructor;call super::destructor;idwc_plant.setfilter("")
idwc_plant.filter()
end event

event itemchanged;call super::itemchanged;long ll_FoundRow

integer	li_rtn

string ls_complex,ls_search

dwitemstatus l_status

is_plant = Trim(data)

//IF Trim(data) = "" THEN Return
//
//ll_FoundRow = idwc_plant.Find ( "location_code='"+data+"'", 1, idwc_plant.RowCount() )
//IF ll_FoundRow < 1 THEN
//	MessageBox( "Plant Error", data + " is Not a Valid Plant Code." )
//	Return 1
//else
//	ls_complex = idwc_plant.getitemstring(ll_foundrow,"complex_code")
//	dw_complex.selectrow(0,false)
//	ls_search = "type_code ='"+ls_complex+"'"
//	li_rtn = dw_complex.find(ls_search,1,dw_complex.rowcount())
//	if li_rtn > 0 then
//		dw_complex.selectrow(li_rtn,true)
//	end if
//END IF		

SetProfileString( iw_frame.is_UserINI, "OTR", "lastplant",data)
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)




end event

