HA$PBExportHeader$uo_hot_list.sru
forward
global type uo_hot_list from nonvisualobject
end type
end forward

global type uo_hot_list from nonvisualobject autoinstantiate
end type

type variables
String	start_carrier, &
	end_carrier, &
	complex, &
	load_key, &
	hotlist_type, &
	priority_level, &
	review_status_ind, &
	del_from_date, &
	del_to_date, &
	ship_to_date,&
	ship_from_date, &
	cancel, &
	control_type, &
	division

end variables

on uo_hot_list.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_hot_list.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

