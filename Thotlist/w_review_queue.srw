HA$PBExportHeader$w_review_queue.srw
forward
global type w_review_queue from w_base_sheet_ext
end type
type tab_que from tab within w_review_queue
end type
type tabpage_load_detail from userobject within tab_que
end type
type dw_load_detail_hdr from u_base_dw_ext within tabpage_load_detail
end type
type dw_stop_detail_info from u_base_dw_ext within tabpage_load_detail
end type
type r_1 from rectangle within tabpage_load_detail
end type
type st_1 from statictext within tabpage_load_detail
end type
type st_2 from statictext within tabpage_load_detail
end type
type uo_next_previous from u_spin_otr within tabpage_load_detail
end type
type dw_messages from u_base_dw_ext within tabpage_load_detail
end type
type tabpage_load_detail from userobject within tab_que
dw_load_detail_hdr dw_load_detail_hdr
dw_stop_detail_info dw_stop_detail_info
r_1 r_1
st_1 st_1
st_2 st_2
uo_next_previous uo_next_previous
dw_messages dw_messages
end type
type tabpage_check_calls from userobject within tab_que
end type
type dw_check_call_info from u_base_dw_ext within tabpage_check_calls
end type
type tabpage_check_calls from userobject within tab_que
dw_check_call_info dw_check_call_info
end type
type tabpage_ship_info from userobject within tab_que
end type
type dw_ship_info from u_base_dw_ext within tabpage_ship_info
end type
type tabpage_ship_info from userobject within tab_que
dw_ship_info dw_ship_info
end type
type tabpage_stop_info from userobject within tab_que
end type
type dw_stop_info from u_base_dw_ext within tabpage_stop_info
end type
type tabpage_stop_info from userobject within tab_que
dw_stop_info dw_stop_info
end type
type tabpage_single_appt from userobject within tab_que
end type
type dw_single_appt from u_base_dw_ext within tabpage_single_appt
end type
type tabpage_single_appt from userobject within tab_que
dw_single_appt dw_single_appt
end type
type tabpage_instructions from userobject within tab_que
end type
type dw_instructions from u_base_dw_ext within tabpage_instructions
end type
type tabpage_instructions from userobject within tab_que
dw_instructions dw_instructions
end type
type tab_que from tab within w_review_queue
tabpage_load_detail tabpage_load_detail
tabpage_check_calls tabpage_check_calls
tabpage_ship_info tabpage_ship_info
tabpage_stop_info tabpage_stop_info
tabpage_single_appt tabpage_single_appt
tabpage_instructions tabpage_instructions
end type
end forward

global type w_review_queue from w_base_sheet_ext
integer x = 9
integer y = 28
integer width = 3374
integer height = 1544
string title = "Review Queue"
long backcolor = 12632256
tab_que tab_que
end type
global w_review_queue w_review_queue

type variables
DATASTORE	ids_review_queue

//NUEVO BOOL
Boolean	cancel

Integer    il_ClickedRow

u_otr002            iu_otr002

u_otr003            iu_otr003

u_otr005	          iu_otr005

u_otr006	          iu_otr006

u_review_q	iu_review_q

u_ws_hot_list iu_ws_hot_list

u_ws_review_queue_load_list iu_ws_review_queue_load_list 

u_ws_appointments iu_ws_appointments


boolean              ib_add, &
                           ib_modify, &
                           ib_inquire, &
                           ib_delete, &
                           ib_new_check_call, &
                           ib_new_stop_info, &
                           ib_new_ship_info, &
                           ib_new_appt, &
	           ib_new_instruction, &
	           ib_new_edi_review, &
	           ib_datastore_delete_ind
                    
DataWindowChild  idwc_latecode
DataWindowChild  idwc_address_code

Integer                  ii_rc, &
                            ii_error_column
    
long                      il_error_row, &
                             il_RowCount, &
                             il_Row
                      
string                    is_closemessage_txt, &
                            is_load_key, &
                            is_group_id, &
          	            is_start_carrier, &
	            is_end_carrier, &
	            is_complex




end variables

forward prototypes
public function integer wf_changed ()
public function boolean wf_next ()
public function boolean wf_presave ()
public function boolean wf_previous ()
public function boolean wf_update ()
public subroutine wf_delete ()
public function integer wf_stop_info_inq ()
public function integer wf_load_single_appt ()
public function boolean wf_presave_appt ()
public function boolean wf_update_appt ()
public function boolean wf_update_review_que ()
public function boolean wf_update_instruct ()
public subroutine wf_messages_inq ()
public function boolean wf_retrieve ()
public function integer wf_header_inq ()
public function integer wf_review_queue_inq ()
end prototypes

public function integer wf_changed ();/*
	purpose:	this function will examine all datawindow controls on the window
				that have update capability against the database.  if anything has changed,
				and the change is valid, a 1 is returned.  if an invalid change has occurred
				then a -1 is returned.  if nothing has changed a 0 is returned.

*/


//   tab_que.tabpage_load_detail.dw_load_detail_hdr.ModifiedCount() + &
//   tab_que.tabpage_load_detail.dw_load_detail_hdr.DeletedCount() + &
//   tab_que.tabpage_ship_info.dw_ship_info.ModifiedCount() + &
//   tab_que.tabpage_ship_info.dw_ship_info.DeletedCount() + &
//   tab_que.tabpage_stop_info.dw_stop_info.ModifiedCount() + &
//   tab_que.tabpage_stop_info.dw_stop_info.DeletedCount() + &
IF tab_que.tabpage_load_detail.dw_messages.ModifiedCount() + &
   tab_que.tabpage_load_detail.dw_messages.DeletedCount() + &
   tab_que.tabpage_load_detail.dw_stop_detail_info.ModifiedCount() + &
	tab_que.tabpage_load_detail.dw_stop_detail_info.DeletedCount() + &
	tab_que.tabpage_check_calls.dw_check_call_info.ModifiedCount() + &
   tab_que.tabpage_check_calls.dw_check_call_info.DeletedCount() + &
   tab_que.tabpage_single_appt.dw_single_appt.ModifiedCount() + &
   tab_que.tabpage_single_appt.dw_single_appt.DeletedCount() > 0 THEN
	is_closemessage_txt = "Save Changes?"
	RETURN 1
END IF

// nothing has been changed
RETURN 0
	
end function

public function boolean wf_next ();This.TriggerEvent(Closequery!)
IF Message.returnvalue = 1 Then
   Return False
End IF

IF il_Row < il_RowCount  THEN 
   This.SetRedraw(False)
   il_Row = il_Row + 1
   IF ids_review_queue.object.load_key[il_Row] > Space(7) Then
	   is_load_key = ids_review_queue.object.load_key[il_Row]
      tab_que.tabpage_load_detail.dw_load_detail_hdr.Reset()   
      tab_que.tabpage_load_detail.dw_stop_detail_info.Reset()   
      tab_que.tabpage_load_detail.dw_messages.Reset()   
      wf_header_inq ()
      wf_stop_info_inq ()
      wf_messages_inq ()
      tab_que.tabpage_load_detail.dw_load_detail_hdr.ResetUpdate()
      tab_que.tabpage_load_detail.dw_stop_detail_info.ResetUpdate()
      tab_que.tabpage_load_detail.dw_messages.ResetUpdate()
   End If 
		ib_new_check_call = true
		ib_new_ship_info = true
		ib_new_stop_info = true
		ib_new_appt = true
		ib_new_instruction = true
		ib_new_edi_review = true
   This.SetRedraw(True) 
ELSE  
   MessageBox( "Boundary " , "No more records found")  
   Return FALSE
END IF

Return True



end function

public function boolean wf_presave ();long      ll_Row          
       
Integer   li_late_reason_err_ind



ll_Row = 0

//tab_que.tabpage_load_detail.dw_stop_detail_info.AcceptText()

ll_Row = tab_que.tabpage_load_detail.dw_stop_detail_info.GetNextModified(ll_Row, PRIMARY!)

Do while ll_row <> 0
   li_late_reason_err_ind = tab_que.tabpage_load_detail.dw_stop_detail_info.GetItemNumber(ll_Row, "late_reason_err_ind")
      If li_late_reason_err_ind = 1 Then
       	MessageBox("Late Reason Code Error", "Please enter a valid Late Reason Code") 
         tab_que.tabpage_load_detail.dw_stop_detail_info.SetColumn("late_reason")     
	      Return FALSE
      End if
ll_Row = tab_que.tabpage_load_detail.dw_stop_detail_info.GetNextModified(ll_Row, PRIMARY!)
Loop

Return TRUE
end function

public function boolean wf_previous ();This.TriggerEvent(Closequery!)
IF Message.returnvalue = 1 Then
   Return False
End IF

If ib_datastore_delete_ind and il_row > 0 Then 
	il_Row ++
	ib_datastore_delete_ind = False
End If
	
If il_row > 1 Then
     This.SetRedraw(False)
     il_Row = il_Row - 1  
	  is_load_key = ids_review_queue.object.load_key[il_Row]
     If is_load_key > Space(7) Then
        tab_que.tabpage_load_detail.dw_load_detail_hdr.Reset()   
        tab_que.tabpage_load_detail.dw_stop_detail_info.Reset()   
        tab_que.tabpage_load_detail.dw_messages.Reset()   
        wf_header_inq ()
        wf_stop_info_inq ()
        wf_messages_inq ()
//        tab_que.tabpage_load_detail.dw_load_detail_hdr.ResetUpdate()   
        tab_que.tabpage_load_detail.dw_stop_detail_info.ResetUpdate()   
//        tab_que.tabpage_load_detail.dw_messages.ResetUpdate() 
     End If 
		   ib_new_check_call = true
			ib_new_ship_info = true
			ib_new_stop_info = true
			ib_new_appt = true
			ib_new_instruction = true
			ib_new_edi_review = true
     This.SetRedraw(True) 
Else
   MessageBox( "Boundary " , "No previous records")  
   Return FALSE
End if

Return True

end function

public function boolean wf_update ();boolean           lb_return


IF  Tab_que.SelectedTab = 5 Then
   lb_return = wf_update_appt()
Else
	If Tab_que.SelectedTab = 6 Then
		lb_return = wf_update_instruct()
	Else
	   lb_return = wf_update_review_que()
	End If
End IF

If lb_return = FALSE Then
   Return False
Else
	Return True
End If

end function

public subroutine wf_delete ();//integer    li_rtn, &
//			  li_rc
//
//long		  ll_Row,&
//           ll_del_row, &
//			  ll_RowCount
//
//String     ls_load_key, &
//           ls_stop_code, &
//           ls_update_date, &
//           ls_update_time 
//
//dwItemStatus		status
//
//s_error    lstr_Error_Info
//
//
//SetPointer(HourGlass!)
//
//tab_que.tabpage_load_detail.dw_messages.setredraw(false)
//lstr_error_info.se_event_name = "wf_delete"
//lstr_error_info.se_window_name = "w_review_queue"
//lstr_error_info.se_procedure_name = "wf_delete_review_queue"
//lstr_error_info.se_user_id = SQLCA.Userid
//lstr_error_info.se_message = Space(71)
//
//ll_RowCount = tab_que.tabpage_load_detail.dw_messages.RowCount()
//
//ib_datastore_delete_ind = false
//
//If ll_RowCount > 0 Then
//	ll_Row = 1
//// FOR ll_Row = 1  TO ll_RowCount
////  If tab_que.tabpage_load_detail.dw_messages.IsSelected(ll_Row) = True Then
//        ls_load_key		= tab_que.tabpage_load_detail.dw_messages.object.load_key[ll_Row]
//	     ls_stop_code		= space(02)
//	     ls_update_date 	= space(10)
//		  ls_update_time 	= space(08)
//
//        li_rtn = iu_otr003.nf_otrt36ar(ls_load_key, ls_stop_code, &
//                           ls_update_date, ls_update_time, &
//			                  lstr_error_info, 0)
//
//   		tab_que.tabpage_load_detail.dw_messages.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
//	   	CHOOSE CASE li_rtn
//			CASE is < 0 
//				tab_que.tabpage_load_detail.dw_messages.Setredraw(TRUE)
//				li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
//										"     " + " has failed due to an Error.  Do you want to Continue?", &
//										Question!, YesNo! )
//				tab_que.tabpage_load_detail.dw_messages.Setredraw(FALSE)
//			CASE is > 0 
//				tab_que.tabpage_load_detail.dw_messages.Setredraw(TRUE)
//				li_rc = MessageBox( "Update Warning", lstr_Error_Info.se_message + &
//										" Load Key = " +ls_load_key, &
//										Information!, OK! )
//				tab_que.tabpage_load_detail.dw_messages.Setredraw(FALSE)
//			CASE ELSE
// 			  	Long 	li_return
//				ll_del_row = ll_row
// 				tab_que.tabpage_load_detail.dw_messages.DeleteRow( ll_del_row )
////				IF tab_que.tabpage_load_detail.dw_messages.rowcount() = 0  &
////						or tab_que.tabpage_load_detail.dw_messages.rowcount() < ll_del_row then exit
////				ll_Row --
////			 	ll_RowCount --
//				li_rc = 0
////				IF ll_RowCount <= 1 Then
//					li_return = ids_review_queue.DeleteRow(il_Row)
//					il_RowCount --
//					il_Row --
//					ib_datastore_delete_ind = TRUE
//					ids_review_queue.ResetUpdate()
//					wf_next ()
////				End If
//			END CHOOSE
//
//		   IF li_rc = 2 THEN 			// NO  do not continue processing
//			   tab_que.tabpage_load_detail.dw_messages.Setredraw(TRUE)
//			   Return 
//         ELSE
//            iw_frame.SetMicroHelp( "Ready...") 
//	   	END IF
////	else
////		tab_que.tabpage_load_detail.dw_messages.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )		
////    END IF
////Next
//
////tab_que.tabpage_load_detail.dw_messages.Scrolltorow(tab_que.tabpage_load_detail.dw_messages.RowCount())   // force to last row
////tab_que.tabpage_load_detail.dw_messages.SetItem(tab_que.tabpage_load_detail.dw_messages.rowcount(),"load_key",is_load_key)
////tab_que.tabpage_load_detail.dw_messages.SetItem(tab_que.tabpage_load_detail.dw_messages.rowcount(),"stop_code",'01')
////tab_que.tabpage_load_detail.dw_messages.SetColumn("type_code")
//tab_que.tabpage_load_detail.dw_messages.ResetUpdate()
//tab_que.tabpage_load_detail.dw_messages.setredraw(true)
//
//
//End if
//Return       
//
//
end subroutine

public function integer wf_stop_info_inq ();integer               li_rtn				          

string                ls_stop_detail_inq_string, &
                      ls_req_load_key, &
							 ls_refetch_stop_code

s_error               lstr_Error_Info


SetPointer( HourGlass! )

ls_req_load_key      = is_load_key
ls_refetch_stop_code = "  " 

lstr_error_info.se_event_name = "wf_load_det_stop_info_inq"
lstr_error_info.se_window_name = "w_load_detail"
lstr_error_info.se_procedure_name = "wf_load_det_stop_info_inq"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)
 
ls_stop_detail_inq_string	= Space(1101)
ls_refetch_stop_code       = Space(2)

Do
	ls_stop_detail_inq_string	= Space(1101)
	li_rtn = iu_ws_review_queue_load_list.nf_otrt12er(ls_req_load_key, ls_stop_detail_inq_string,lstr_Error_Info)


// Check the return code from the above function call
	IF li_rtn <> 0 THEN
   	return(-1)
	ELSE 
   	iw_frame.SetMicroHelp( "Ready...")
	END IF
 

	li_rtn = tab_que.tabpage_load_detail.dw_stop_detail_info.ImportString(Trim(ls_stop_detail_inq_string))

Loop While trim(ls_refetch_stop_code) > ""
 
return(1)      
end function

public function integer wf_load_single_appt ();integer  li_rtn
			
string   ls_appt_update_string, &
      	ls_carrier_code, &
         ls_from_ship_date, &
         ls_to_ship_date, &
         ls_refetch_load_key, &
         ls_refetch_stop_code, &
         ls_refetch_delv_date, &
         ls_date_ind, &
			ls_load_key

    
	 

// Turn the redraw off for the window while we retrieve
This.SetRedraw(FALSE)

s_error  lstr_Error_Info

tab_que.tabpage_single_appt.dw_single_appt.Reset()

ls_load_key = tab_que.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]

lstr_error_info.se_event_name = "wf_retrieve"
lstr_error_info.se_window_name = "w_single_appt_update"
lstr_error_info.se_procedure_name = "wf_retrieve"
lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_message = Space(71)

ls_refetch_load_key   = Space(7)
ls_refetch_stop_code  = Space(2)
ls_refetch_delv_date  = Space(10)
ls_date_ind           = Space(1)
string ls_div

Do

   ls_appt_update_string = Space(2926)
 	
//	li_rtn = iu_otr002.nf_otrt18ar(ls_carrier_code, ls_from_ship_date,&
//      		ls_to_ship_date, ls_load_key, ls_date_ind, &
//            ls_div, ls_appt_update_string, ls_refetch_load_key, &
//            ls_refetch_stop_code, ls_refetch_delv_date, lstr_error_info, 0)
				
	li_rtn = iu_ws_appointments.uf_otrt18er(ls_carrier_code, ls_from_ship_date,&
      		ls_to_ship_date, ls_load_key, ls_date_ind, &
            ls_div, ls_appt_update_string, lstr_error_info)				

// Check the return code from the above function call
If li_rtn <> 0 Then
 		tab_que.tabpage_single_appt.dw_single_appt.ResetUpdate()
		This.SetRedraw(TRUE)
      Return  -1
ELSE
      iw_frame.SetMicroHelp( "Ready...")
END IF

     li_rtn = tab_que.tabpage_single_appt.dw_single_appt.ImportString(Trim(ls_appt_update_string))

loop while len(Trim(ls_refetch_load_key)) > 0

ls_carrier_code = tab_que.tabpage_single_appt.dw_single_appt.GetItemString(1,"carrier_code") 

tab_que.tabpage_single_appt.dw_single_appt.SetFocus()

tab_que.tabpage_single_appt.dw_single_appt.ResetUpdate()

// Turn the redraw on for the window after we retrieve
This.SetRedraw(TRUE)

Return 1



end function

public function boolean wf_presave_appt ();date		 ld_ApptDate
		  
long      ll_Row

string	 ls_ApptContact

time      lt_ApptTime


tab_que.tabpage_single_appt.dw_single_appt.AcceptText()

ll_Row = tab_que.tabpage_single_appt.dw_single_appt.GetNextModified(0, PRIMARY!)

Do while ll_Row <> 0
  ls_ApptContact = tab_que.tabpage_single_appt.dw_single_appt.GetItemString(ll_Row, "appt_contact")
  If IsNull(ls_ApptContact) or ls_apptContact = '          ' then
     MessageBox("Appointment Contact", "Please enter an Appointment Contact") 
     tab_que.tabpage_single_appt.dw_single_appt.SetColumn("appt_contact")     
	  ii_error_column = tab_que.tabpage_single_appt.dw_single_appt.getcolumn()
     il_error_row = ll_Row  
	  tab_que.tabpage_single_appt.dw_single_appt.postevent("ue_post_tab")
     Return FALSE
  End if
  lt_ApptTime = tab_que.tabpage_single_appt.dw_single_appt.GetItemTime(ll_Row, "appt_time")
  If IsTime(String(lt_ApptTime)) = FALSE then
     MessageBox("Appointment Time", "Please enter an Appointment Time")
     tab_que.tabpage_single_appt.dw_single_appt.SetColumn("appt_Time")
	  ii_error_column = tab_que.tabpage_single_appt.dw_single_appt.getcolumn()
     il_error_row = ll_Row
	  tab_que.tabpage_single_appt.dw_single_appt.postevent("ue_post_tab")
     Return FALSE    
  End if
  If String(lt_ApptTime) > '00:00:00' then
     ld_ApptDate = tab_que.tabpage_single_appt.dw_single_appt.GetItemDate(ll_Row, "appt_date")
     If IsDate(String(ld_ApptDate))= FALSE then      
        MessageBox("Appointment Date", "Please enter an Appointment Date")
        tab_que.tabpage_single_appt.dw_single_appt.SetColumn("appt_date")
		  ii_error_column = tab_que.tabpage_single_appt.dw_single_appt.getcolumn()
        il_error_row = ll_Row
		  tab_que.tabpage_single_appt.dw_single_appt.postevent("ue_post_tab")
        Return FALSE     
     End if
  End If
ll_Row = tab_que.tabpage_single_appt.dw_single_appt.GetNextModified(ll_Row, PRIMARY!)
Loop 

Return TRUE
end function

public function boolean wf_update_appt ();
Integer  li_return_error_code, &    
         li_rtn
			
long     ll_Row
	
string   ls_return_error_msg, &   
         ls_appt_update_string, &
      	ls_month, &
         ls_day, &
         ls_year, &
         ls_hours, &
         ls_minutes, &
         ls_appt_contact, &
         ls_load_key, &
         ls_stop_code, &
         ls_override_ind, & 
         ls_notify_carrier_ind, &
         ls_appt_date, &
         ls_appt_time, &
         ls_eta_date, &
         ls_eta_time

date     ld_date
time     lt_time

s_error  lstr_Error_Info

If wf_presave_appt() = TRUE Then  

SetPointer(HourGlass!)
tab_que.tabpage_single_appt.dw_single_appt.SetRedraw(False)

ll_row = tab_que.tabpage_single_appt.dw_single_appt.GetNextModified(0, Primary!)

IF ll_row = 0 Then
	SetMicroHelp("No Modified Data.")
	tab_que.tabpage_single_appt.dw_single_appt.SetRedraw(True)
	Return True
End IF

do while ll_row <> 0
	ls_load_key = tab_que.tabpage_single_appt.dw_single_appt.object.load_key[ll_Row]           
   ls_stop_code = tab_que.tabpage_single_appt.dw_single_appt.object.stop_code[ll_Row] 
   ls_appt_contact = tab_que.tabpage_single_appt.dw_single_appt.object.appt_contact[ll_Row] 
	ls_appt_contact = ls_appt_contact + space(10 - Len(ls_appt_contact))
   ls_eta_date = String(tab_que.tabpage_single_appt.dw_single_appt.object.appt_date[ll_Row], "yyyymmdd")
	ls_appt_date = ls_eta_date
   lt_time = tab_que.tabpage_single_appt.dw_single_appt.object.appt_time[ll_Row]
	ls_hours = Mid(String(lt_time,"hh:mm"), 1, 2) 
   ls_minutes = Mid(String(lt_time,"hh:mm"), 4, 2)
   ls_appt_time = ls_hours + ls_minutes 
   ls_eta_time = ls_appt_time 
   ls_override_ind = tab_que.tabpage_single_appt.dw_single_appt.object.override_ind[ll_Row] 
   ls_notify_carrier_ind = tab_que.tabpage_single_appt.dw_single_appt.object.notify_carrier_ind[ll_Row] 
   ls_return_error_msg = space(80)
	
   lstr_error_info.se_event_name = "wf_update"
   lstr_error_info.se_window_name = "w_mass_appt_update"
   lstr_error_info.se_procedure_name = "wf_update"
   lstr_error_info.se_user_id = SQLCA.UserID
   lstr_error_info.se_message = Space(71)

  
/*   li_rtn = iu_otr002.nf_otrt19ar(ls_load_key, ls_stop_code, &
 	 	ls_appt_contact, ls_appt_date, ls_appt_time, ls_eta_date, &
	   ls_eta_time, ls_override_ind, ls_notify_carrier_ind, &
      li_return_error_code, ls_return_error_msg, lstr_error_info, 0)
*/
 li_rtn =iu_ws_appointments.uf_otrt19er(ls_load_key, ls_stop_code, &
 	 	ls_appt_contact, ls_appt_date, ls_appt_time, ls_eta_date, &
	   ls_eta_time, ls_override_ind, ls_notify_carrier_ind,lstr_error_info)	

// Check the return code from the above function call

   If li_rtn = 0 or li_rtn = 1 Then
      tab_que.tabpage_single_appt.dw_single_appt.DeleteRow(ll_Row) 
      ll_Row --
   Else 
      If li_rtn < 0 Then
         tab_que.tabpage_single_appt.dw_single_appt.SetRow(ll_row)
 	      tab_que.tabpage_single_appt.dw_single_appt.SetRedraw(True)
         Return False 
      Else
			tab_que.tabpage_single_appt.dw_single_appt.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
         If SQLCA.ii_messagebox_rtn = 3 Then
            tab_que.tabpage_single_appt.dw_single_appt.SetRow(ll_row)
            tab_que.tabpage_single_appt.dw_single_appt.SetRedraw(True)
            Return False
         End If 
      End If  
    End If
 	  
	ll_row = tab_que.tabpage_single_appt.dw_single_appt.GetNextModified(ll_row, PRIMARY!)
loop

ib_new_appt = True

End if



tab_que.tabpage_single_appt.dw_single_appt.SetColumn("carrier_code")

SetMicroHelp("Update Processing Complete.")

tab_que.tabpage_single_appt.dw_single_appt.SetRedraw(True)
Return TRUE
end function

public function boolean wf_update_review_que ();Boolean     lb_return 

Date       ld_work_date

integer    li_rtn, &
			  li_rc
           

long		  ll_Row,& 
           ll_NewRow, &
           ll_RowNumber, &
			  ll_RowCount, &
           ll_Count
  
string      ls_load_key, &
			   ls_stop_code, &
				ls_type_code, & 
				ls_msg, & 
            ls_update_date, &
            ls_update_time, &
            ls_appt_date, &
            ls_appt_time, &
            ls_appt_contact, &  
            ls_stop_eta_date, &
            ls_stop_eta_time, & 
            ls_actual_delivery_date, &
            ls_actual_delivery_time, &
            ls_carrier_notified_ind, &
            ls_update_error_ind, &
            ls_late_reason_code 

Time       lt_work_time

s_error				lstr_Error_Info

dwItemStatus		status

lstr_error_info.se_event_name = "wf_update"
lstr_error_info.se_window_name = "w_hot_list"
lstr_error_info.se_procedure_name = "wf_update_hot_msg"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

IF tab_que.tabpage_load_detail.dw_messages.AcceptText() = -1  or &
     tab_que.tabpage_load_detail.dw_stop_detail_info.AcceptText() = -1 THEN
     Return(False)
End if

lb_return = wf_presave()

If lb_return = FALSE Then Return False

SetPointer(HourGlass!)

ll_RowCount = tab_que.tabpage_load_detail.dw_messages.RowCount()
ll_Row = 0   
ll_RowNumber = 1
 
//for dw_messages datawindow
ll_Row = tab_que.tabpage_load_detail.dw_messages.GetNextModified( ll_Row, Primary! ) 
DO WHILE ll_row <> 0
	ls_load_key			= tab_que.tabpage_load_detail.dw_messages.object.load_key[ll_Row]
	ls_stop_code		= tab_que.tabpage_load_detail.dw_messages.object.stop_code[ll_Row]
	ls_type_code 		= tab_que.tabpage_load_detail.dw_messages.object.type_code[ll_Row]
	ls_msg				= tab_que.tabpage_load_detail.dw_messages.object.msg[ll_Row]

//	li_rtn = iu_otr003.nf_otrt42ar(ls_load_key, &
//					         ls_stop_code, ls_type_code, ls_msg, &
//								lstr_error_info, 0)

	tab_que.tabpage_load_detail.dw_messages.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
	CHOOSE CASE li_rtn
		CASE is < 0 
			tab_que.tabpage_load_detail.dw_messages.Setredraw(TRUE)
			li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
									ls_load_key + " has failed do to an Error.  Do you want to Continue?", &
									Question!, YesNo! )
			ll_Count --
			tab_que.tabpage_load_detail.dw_messages.Setredraw(FALSE)
		CASE is > 0 
			tab_que.tabpage_load_detail.dw_messages.Setredraw(TRUE)
			li_rc = MessageBox( "Update Warning", lstr_Error_Info.se_message + &
									" Load Key = " +ls_load_key, &
									Information!, OK! )
			tab_que.tabpage_load_detail.dw_messages.Setredraw(FALSE)
		CASE ELSE
			li_rc = 0
           tab_que.tabpage_load_detail.dw_messages.SetItemStatus ( ll_Row, 0, Primary!, DataModified! )
           tab_que.tabpage_load_detail.dw_messages.SelectRow( 0, False)           
           tab_que.tabpage_load_detail.dw_messages.setredraw(true)
           tab_que.tabpage_load_detail.dw_messages.insertrow(0)
           tab_que.tabpage_load_detail.dw_messages.Scrolltorow(tab_que.tabpage_load_detail.dw_messages.RowCount())   // force to last row
           tab_que.tabpage_load_detail.dw_messages.SetItem(tab_que.tabpage_load_detail.dw_messages.rowcount(),"load_key",is_load_key)
           tab_que.tabpage_load_detail.dw_messages.SetItem(tab_que.tabpage_load_detail.dw_messages.rowcount(),"stop_code",'01')
           tab_que.tabpage_load_detail.dw_messages.SetColumn("type_code")
           ll_NewRow = ll_RowCount + 1
           tab_que.tabpage_load_detail.dw_messages.SetItemStatus ( ll_NewRow, 0, Primary!, NotModified! )
           tab_que.tabpage_load_detail. dw_messages.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
			
       	END CHOOSE
		IF li_rc = 2 THEN 			// NO  do not continue processing
		tab_que.tabpage_load_detail.dw_messages.Setredraw(TRUE)
		Return(False)
     ELSE
        iw_frame.SetMicroHelp( "Ready...")
	END IF
ll_Row = tab_que.tabpage_load_detail.dw_messages.GetNextModified( ll_Row, Primary! ) 
LOOP



tab_que.tabpage_load_detail.dw_messages.ResetUpdate()


//for dw_stop_detail_info datawindow
SetPointer(HourGlass!)
ll_Row = 0
ll_RowNumber = 1 
ll_RowCount = tab_que.tabpage_load_detail.dw_stop_detail_info.RowCount()

ll_Row = tab_que.tabpage_load_detail.dw_stop_detail_info.GetNextModified( ll_Row, Primary! )
 DO WHILE ll_row <> 0
	     ls_load_key	  	        = tab_que.tabpage_load_detail.dw_stop_detail_info.object.load_key[1]
        ls_stop_code 	        = tab_que.tabpage_load_detail.dw_stop_detail_info.object.stop_code[ll_Row]	
        ls_late_reason_code     = tab_que.tabpage_load_detail.dw_stop_detail_info.object.late_reason[ll_Row]
        ls_appt_date            = '          '
        ls_appt_time            = '    '       
        ls_appt_contact         = tab_que.tabpage_load_detail.dw_stop_detail_info.object.appt_contact[ll_Row]
        ld_work_date            = tab_que.tabpage_load_detail.dw_stop_detail_info.GetItemDate( ll_Row, "act_delv_date" )
        ls_actual_delivery_date = String(ld_work_date, "yyyy-mm-dd")
        lt_work_time            = tab_que.tabpage_load_detail.dw_stop_detail_info.GetItemTime( ll_Row, "act_delv_time" )
        ls_actual_delivery_time = String(lt_work_time, "hhmm")
        ld_work_date            = tab_que.tabpage_load_detail.dw_stop_detail_info.GetItemDate( ll_Row, "eta_date" )
        ls_stop_eta_date        = String(ld_work_date, "yyyy-mm-dd")
        lt_work_time            = tab_que.tabpage_load_detail.dw_stop_detail_info.GetItemTime( ll_Row, "eta_time" )
        ls_stop_eta_time        = String(lt_work_time, "hhmm")
        ls_carrier_notified_ind = tab_que.tabpage_load_detail.dw_stop_detail_info.object.carrier_notified_ind[ll_Row]
 

		//Added code for Netwise convertion 09/07/16
        li_rtn = iu_ws_review_queue_load_list.nf_otrt17er(ls_actual_delivery_date,ls_late_reason_code, ls_appt_contact, &
		  ls_stop_eta_date, ls_stop_eta_time, ls_actual_delivery_time, ls_load_key, ls_stop_code, &
              ls_carrier_notified_ind, ls_appt_date, ls_appt_time,lstr_error_info)

      tab_que.tabpage_load_detail.dw_stop_detail_info.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
		CHOOSE CASE li_rtn
			CASE is < 0 
				tab_que.tabpage_load_detail.dw_stop_detail_info.Setredraw(TRUE)
				li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
										"  Load Key - " + ls_load_key + " has failed do to an Error.  Do you want to Continue?", &
										Question!, YesNo! )

       	CASE ELSE
				li_rc = 0
		END CHOOSE


      IF li_rc = 1 THEN
         li_rc = 0
      END IF

		IF li_rc = 2 THEN 			// NO  do not continue processing
			tab_que.tabpage_load_detail.dw_stop_detail_info.Setredraw(TRUE)
			Return(False)
      ELSE
         iw_frame.SetMicroHelp( "Ready...") 
	 	END IF
  ll_Row = tab_que.tabpage_load_detail.dw_stop_detail_info.GetNextModified( ll_Row, Primary! )
LOOP


tab_que.tabpage_load_detail.dw_stop_detail_info.ResetUpdate()


Return TRUE

end function

public function boolean wf_update_instruct ();
Integer  li_rtn

long		ll_row, ll_cur_row

string   ls_req_load_key, &   
         ls_req_order, &
         ls_ffw_code, &
			ls_str

s_error  lstr_Error_Info

SetPointer(HourGlass!)

tab_que.tabpage_instructions.dw_instructions.AcceptText()  

ll_row = tab_que.tabpage_instructions.dw_instructions.GetNextModified(0, Primary!)

IF ll_row = 0 Then
	SetMicroHelp("No Modified Data.")
	Return True
Else

    ls_str = tab_que.tabpage_instructions.dw_instructions.&
	          GetItemString(1,"frtforwd_code")
    IF Trim(ls_str) <> "" THEN 
		 ll_cur_row = idwc_address_code.Find ( "address_code='"+ &
	              ls_str+"'", 1, idwc_address_code.RowCount() )
       
    IF ll_cur_row < 1 THEN
       MessageBox( "Freight Forwarder Error", &
	                ls_str + " is Not a Valid Freight Forwarder code." )
       Return False
    else
	     ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_name")
	     tab_que.tabpage_instructions.dw_instructions.setitem(ll_row, "frtforwd_name", ls_str)
	     ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_city")
	     tab_que.tabpage_instructions.dw_instructions.setitem(ll_row, "frtforwd_city", ls_str)
	     ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_state")
	     tab_que.tabpage_instructions.dw_instructions.setitem(ll_row, "frtforwd_state_abrv", ls_str)
    End if
End if
   ls_req_load_key = tab_que.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]          
   ls_req_order = tab_que.tabpage_instructions.dw_instructions.object.order_num[ll_Row] 
   ls_ffw_code = tab_que.tabpage_instructions.dw_instructions.object.frtforwd_code[ll_Row]
	
   lstr_error_info.se_event_name = "wf_update_instruct"
   lstr_error_info.se_window_name = "w_instructions"
   lstr_error_info.se_procedure_name = "wf_update_instruct"
   lstr_error_info.se_user_id = SQLCA.UserID
   lstr_error_info.se_message = Space(71)
/*
   li_rtn = iu_otr005.nf_otrt57ar(ls_req_load_key, ls_req_order, &
 	 	ls_ffw_code, lstr_error_info, 0)

*/
   li_rtn = iu_ws_review_queue_load_list.nf_otrt57er(ls_req_load_key, ls_req_order, &
 	 	ls_ffw_code, lstr_error_info)

// Check the return code from the above function call

   If li_rtn <> 0 Then
	   Return False 
   End If
 	  
 End if
tab_que.tabpage_instructions.dw_instructions.ResetUpdate()
ib_new_instruction = True
SetMicroHelp("Update Processing Complete.")

return TRUE
end function

public subroutine wf_messages_inq ();// 07/23/2002 ibdkkam	Increased ls_cust_serv_msg_inq_string to 2401 and added 
//								"space" inside loop before every call to the mainframe.

integer                 li_rtn
 				
string  						ls_cust_serv_msg_inq_string, &
                        ls_req_load_key, &
								ls_refetch_stop_code, &
								ls_refetch_update_date, &
								ls_refetch_update_time 


s_error                 lstr_Error_Info

SetPointer( HourGlass! )


lstr_error_info.se_event_name = "wf_load_hot_msg"
lstr_error_info.se_window_name = "w_hot_list_messages"
lstr_error_info.se_procedure_name = "wf_load_hot_msg"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ls_req_load_key = is_load_key

ls_cust_serv_msg_inq_string = space(2401)
ls_refetch_stop_code        = space(7)
ls_refetch_update_date      = space(10)
ls_refetch_update_time      = space(8)

//li_rtn = iu_otr003.nf_otrt41ar(ls_req_load_key, &
//	   	ls_cust_serv_msg_inq_string, lstr_error_info, &
//			ls_refetch_stop_code, ls_refetch_update_date, &
//			ls_refetch_update_time, 0)

li_rtn = iu_ws_hot_list.nf_otrt41er(ls_req_load_key,ls_cust_serv_msg_inq_string,lstr_Error_Info)

SetPointer( HourGlass! )

// Check the return code from the above function call
	IF li_rtn <> 0 THEN
		Close( this )
		Return
   ELSE 
      iw_frame.SetMicroHelp( "Ready...")
   END IF
 
 	li_rtn = tab_que.tabpage_load_detail.dw_messages.ImportString(Trim(ls_cust_serv_msg_inq_string))

	DO WHILE li_rtn = 20
		
		ls_cust_serv_msg_inq_string = space(2401)
		
	//	li_rtn = iu_otr003.nf_otrt41ar(ls_req_load_key, &
	//				ls_cust_serv_msg_inq_string, lstr_error_info, &
	//				ls_refetch_stop_code, ls_refetch_update_date, &
	//				ls_refetch_update_time, 0)
    li_rtn = iu_ws_hot_list.nf_otrt41er(ls_req_load_key,ls_cust_serv_msg_inq_string,lstr_error_info)

		li_rtn = tab_que.tabpage_load_detail.dw_messages.ImportString(Trim(ls_cust_serv_msg_inq_string))

		SetPointer( HourGlass! )
	LOOP
//End if

tab_que.tabpage_load_detail.dw_messages.ResetUpdate()

// 07/2002 ibdkkam  Moved Protect expression from dw to here, AFTER the reset update.
//		Corrects Protect problem where columns were not being protected.

String ls_modstring, ls_rtn
ls_modstring = "type_code.Protect='0~tif(IsRowNew(),0,1)'"
ls_rtn = tab_que.tabpage_load_detail.dw_messages.Modify(ls_modstring)
ls_modstring = tab_que.tabpage_load_detail.dw_messages.describe("type_code.protect")

ls_modstring = "msg.Protect='0~tif(IsRowNew(),0,1)'"
ls_rtn = tab_que.tabpage_load_detail.dw_messages.Modify(ls_modstring)
ls_modstring = tab_que.tabpage_load_detail.dw_messages.describe("type_code.msg")


tab_que.tabpage_load_detail.dw_messages.insertrow(0)
tab_que.tabpage_load_detail.dw_messages.Scrolltorow(1) // force to first row      ---------      tab_que.tabpage_load_detail.dw_messages.RowCount())   // force to last row
tab_que.tabpage_load_detail.dw_messages.SetItem(tab_que.tabpage_load_detail.dw_messages.rowcount(),"load_key",is_load_key)
tab_que.tabpage_load_detail.dw_messages.SetItem(tab_que.tabpage_load_detail.dw_messages.rowcount(),"stop_code",'01')

integer li_ret_code

li_ret_code = tab_que.tabpage_load_detail.dw_messages.SetItemStatus (tab_que.tabpage_load_detail.dw_messages.rowcount(), 0, Primary!, NotModified! )

this.Title = "Review Queue Messages Load - " + is_load_key

SetPointer( Arrow! )
end subroutine

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer, &
			li_count

String   ls_input_string, &
			ls_detail_filter, &
			ls_detail, &
			ls_temp, &
			ls_review_queue, &
			ls_cancel
		
       
li_rc = wf_Changed()

CHOOSE CASE li_rc
	CASE  1	//	a valid change occurred ...
		li_answer = MessageBox("Wait",is_closemessage_txt,question!,yesnocancel!)
		CHOOSE CASE li_answer
			CASE 1	//	yes, save changes
				IF wf_Update() THEN	//	the save succeeded
				ELSE	//	the save failed
					li_answer = MessageBox("Save Failed","Retrieve Anyway?",question!,yesno!)
					IF li_answer = 1 THEN	//	retrieve w/o saving changes
					ELSE	//	cancel the close
						Return( false )
					END IF
				END IF	
			CASE 2 	//	retrieve w/o saving changes
			CASE 3	//	cancel the retrieve
				Return(false)
		END CHOOSE
	CASE  -1 		//	a validation error occurred
		li_answer = MessageBox("Data Entry Error","Retrieve w/o Save?",question!,okcancel!)
		CHOOSE CASE li_answer
			CASE 1	//	retrieve w/o saving changes
			CASE 2	//	cancel the retrieve
				Return( false )
		END CHOOSE
	CASE  -2 	//	a serious validation error, window may not close in this state
		MessageBox("A Serious Error Has Occurred","Correct Error Before Retrieving!",exclamation!,ok!)
		Return(False)
	CASE ELSE	//	nothing has changed
END CHOOSE

tab_que.SelectTab(1)

//ls_review_queue = is_start_carrier + '~t' + is_end_carrier + '~t' + is_complex
//OpenWithParm( w_review_queue_filter, ls_review_queue, iw_frame )
//ls_review_queue = Message.StringParm
OpenWithParm( w_review_queue_filter, iu_review_q, iw_frame )
iu_review_q = Message.PowerObjectParm

If isvalid(iu_review_q) = False then
	Return True
End If

//li_count = 1
//DO while li_count <= 4
//	ls_temp = iw_frame.iu_string.nf_getToken(ls_review_queue, '~t')
//	CHOOSE CASE li_count
//		CASE 1
//			is_start_carrier = ls_temp
//		CASE 2
//			is_end_carrier = ls_temp
//		CASE 3
//			is_complex = ls_temp
//		CASE 4 
//			ls_cancel = ls_temp
//	END CHOOSE
//	li_count++
//LOOP
//       
//IF ls_cancel = 'true' THEN
//	Return TRUE


//
IF iu_review_q.cancel = True  Then
   Return True
ELSE
      This.SetRedraw(False)
		ib_new_check_call = true
		ib_new_ship_info = true
		ib_new_stop_info = true
		ib_new_appt = true
		ib_new_instruction = true
		ib_new_edi_review = true
	   tab_que.tabpage_load_detail.dw_load_detail_hdr.Reset()
	   tab_que.tabpage_load_detail.dw_stop_detail_info.Reset()
	   tab_que.tabpage_load_detail.dw_messages.Reset()
  	   li_rc = wf_review_queue_inq() 
	   IF li_rc <> 0 THEN
         tab_que.tabpage_load_detail.dw_load_detail_hdr.InsertRow(0)
			tab_que.tabpage_check_calls.Enabled = False
			tab_que.tabpage_ship_info.Enabled = False
			tab_que.tabpage_stop_info.Enabled = False
			tab_que.tabpage_single_appt.Enabled = False
			// ibdkdld
			tab_que.tabpage_instructions.enabled  = false
			//tab_que.tabpage_edi.enabled = false
         This.SetRedraw(True)
		   Return false
      ELSE
         wf_header_inq ()
         wf_stop_info_inq ()
         wf_messages_inq ()
         This.SetRedraw(True)
         tab_que.tabpage_load_detail.dw_load_detail_hdr.ResetUpdate()
         tab_que.tabpage_load_detail.dw_stop_detail_info.ResetUpdate()
         tab_que.tabpage_load_detail.dw_messages.ResetUpdate()
		   Return FALSE
   	END IF
END IF

return false



end function

public function integer wf_header_inq ();integer               li_rtn

string                ls_review_queue_inq_string, &
                      ls_req_load_key, &
							 ls_req_bol_number, &
						    ls_req_customer_id, &
						    ls_req_customer_po, &
                      ls_order_number, &
							 ls_carrier, &
							 ls_key


s_error               lstr_Error_Info


SetPointer( HourGlass! )

ls_req_load_key      = is_load_key

lstr_error_info.se_event_name = "wf_review_queue_hdr_inq"
lstr_error_info.se_window_name = "w_review_queue"
lstr_error_info.se_procedure_name = "wf_review_queue_hdr_inq"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)
// ibdkdld 
ls_review_queue_inq_string	= Space(175)

//li_rtn = iu_otr003.nf_otrt11ar(ls_req_load_key, ls_req_bol_number, &
//           ls_req_customer_id, ls_req_customer_po, ls_review_queue_inq_string, &
//           lstr_error_info, 0)

li_rtn = iu_ws_review_queue_load_list.nf_otrt11er(ls_req_load_key, ls_req_bol_number, &
           ls_req_customer_id, ls_req_customer_po, ls_review_queue_inq_string, &
           lstr_error_info)



// Check the return code from the above function call
IF li_rtn <> 0 THEN
    tab_que.tabpage_load_detail.dw_load_detail_hdr.InsertRow(0)
 	 Return(-1)
ELSE 
   iw_frame.SetMicroHelp( "Ready...")
END IF
 
li_rtn = tab_que.tabpage_load_detail.dw_load_detail_hdr.ImportString(ls_review_queue_inq_string)
tab_que.tabpage_load_detail.dw_load_detail_hdr.ResetUpdate()

IF li_rtn < 0 THEN
    tab_que.tabpage_load_detail.dw_load_detail_hdr.InsertRow(0)
 END IF

ls_key = tab_que.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
IF iw_frame.iu_string.nf_IsEmpty(ls_key) Then
	   tab_que.tabpage_check_calls.Enabled = False
		tab_que.tabpage_ship_info.Enabled = False
		tab_que.tabpage_stop_info.Enabled = False
		tab_que.tabpage_single_appt.Enabled = False
		//ibdkdld
		tab_que.tabpage_instructions.Enabled = False
		//tab_que.tabpage_edi.Enabled = False

Else
		tab_que.tabpage_check_calls.Enabled = True
		tab_que.tabpage_ship_info.Enabled = True
		tab_que.tabpage_stop_info.Enabled = True
		tab_que.tabpage_single_appt.Enabled = True
		//ibdkdld
		tab_que.tabpage_instructions.Enabled = True
		//tab_que.tabpage_edi.Enabled = True
End If

ls_carrier = tab_que.tabpage_load_detail.dw_load_detail_hdr.object.carrier[1]

IF iw_frame.im_menu.m_holding1.m_appointments.m_single.Enabled = False Then
	tab_que.tabpage_single_appt.Enabled = False
ElseIF iw_frame.im_menu.m_file.m_save.Enabled = False Then
	tab_que.tabpage_single_appt.Enabled = False
ElseIF iw_frame.iu_string.nf_IsEmpty(ls_carrier) Then
	tab_que.tabpage_single_appt.Enabled = False
End IF


return(0)      
end function

public function integer wf_review_queue_inq ();integer               li_rtn, li_count							

long	                ll_row

string                ls_cust_serv_summary_string, &
                      ls_req_from_carrier, &
							 ls_req_to_carrier, &
							 ls_req_complex, &
                      ls_refetch_carrier_code, &
                      ls_refetch_ship_date, &
                      ls_refetch_load_key, &
							 ls_review_queue, &
							 ls_start_carrier, &
							 ls_end_carrier, &
							 ls_complex, &
							 ls_temp, ls_division, ls_plant
							 

s_error               lstr_Error_Info

SetPointer( HourGlass! )
ids_review_queue.Reset()

ls_req_from_carrier = iu_review_q.s_carrier 
ls_req_to_carrier   = iu_review_q.e_carrier 
ls_req_complex		  = iu_review_q.complex
ls_division 		  = iu_review_q.division	
ls_plant				  = iu_review_q.plant

lstr_error_info.se_event_name = "wf_review_queue_inq"
lstr_error_info.se_window_name = "w_review_queue"
lstr_error_info.se_procedure_name = "wf_review_queue_inq"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)
 
ls_cust_serv_summary_string	= Space(3601)
ls_refetch_carrier_code       = Space(4)
ls_refetch_ship_date          = Space(10)
ls_refetch_load_key           = Space(7)

Do
  //Added code for the netwise convertion 09/06/2016
    li_rtn = iu_ws_review_queue_load_list.nf_otrt34er(ls_req_from_carrier,ls_req_to_carrier,ls_req_complex, &
		         														ls_division,ls_plant, ls_cust_serv_summary_string, lstr_Error_info)
/*
   li_rtn = iu_otr003.nf_otrt34ar(ls_req_from_carrier, ls_req_to_carrier, &
            ls_req_complex, ls_division, ls_cust_serv_summary_string, ls_refetch_carrier_code, &
            ls_refetch_ship_date, ls_refetch_load_key, lstr_error_info, ls_plant, 0)
*/
// Check the return code from the above function call
   IF li_rtn <> 0 THEN
      il_RowCount = 0
 	   return(-1)
   ELSE 
      iw_frame.SetMicroHelp( "Ready...") 
   END IF
	 
   il_RowCount = ids_review_queue.ImportString(Trim(ls_cust_serv_summary_string))

loop while ls_refetch_ship_date > '0001-01-01'

//ibdkdld make sure there was data sent back
if ids_review_queue.rowcount( ) < 1 Then 
	ids_review_queue.ResetUpdate()
   iw_frame.SetMicroHelp( "No Records Found...") 
	return (-1)
Else	
	il_Row = 1
	is_load_key    = ids_review_queue.object.load_key[il_Row] 
	ids_review_queue.ResetUpdate()
	return(0)
End IF
end function

on w_review_queue.create
int iCurrent
call super::create
this.tab_que=create tab_que
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_que
end on

on w_review_queue.destroy
call super::destroy
destroy(this.tab_que)
end on

event ue_postopen;call super::ue_postopen;iu_otr002  =  CREATE u_otr002
iu_otr003  =  CREATE u_otr003
iu_otr005  =  CREATE u_otr005
iu_otr006  =  CREATE u_otr006
iu_ws_hot_list = CREATE u_ws_hot_list
iu_ws_appointments = CREATE u_ws_appointments
iu_ws_review_queue_load_list = CREATE u_ws_review_queue_load_list


IF This.wf_retrieve() THEN Close( this )

end event

event open;call super::open;// Get the passed load key (if any)
is_Load_Key = Message.StringParm

// Insert a row into each datawindow (cosmetic reason only)
tab_que.tabpage_load_detail.dw_load_detail_hdr.InsertRow(0)
tab_que.tabpage_load_detail.dw_stop_detail_info.InsertRow(0)
tab_que.tabpage_load_detail.dw_messages.InsertRow(0)

//Create DataStore
ids_review_queue = CREATE DataStore
ids_review_queue.DataObject = "d_review_queue"


//iu_ws_review_queue_load_list = CREATE u_ws_review_queue_load_list
end event

event close;call super::close;DESTROY iu_otr002
DESTROY iu_otr003
DESTROY iu_otr005
DESTROY iu_otr006
DESTROY ids_review_queue
DESTROY iu_ws_hot_list
DESTROY iu_ws_review_queue_load_list 
DESTROY iu_ws_appointments
end event

event activate;call super::activate;iw_frame.im_menu.m_edit.m_next.Enable()
iw_frame.im_menu.m_edit.m_previous.Enable()
end event

event deactivate;call super::deactivate;iw_frame.im_menu.m_edit.m_next.Disable()
iw_frame.im_menu.m_edit.m_previous.Disable()
end event

event resize;//Overide ancestor script here to stop multiple redraws of next/prev buttons.

end event

type tab_que from tab within w_review_queue
integer width = 3323
integer height = 1432
integer taborder = 1
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean raggedright = true
boolean boldselectedtext = true
boolean createondemand = true
integer selectedtab = 1
tabpage_load_detail tabpage_load_detail
tabpage_check_calls tabpage_check_calls
tabpage_ship_info tabpage_ship_info
tabpage_stop_info tabpage_stop_info
tabpage_single_appt tabpage_single_appt
tabpage_instructions tabpage_instructions
end type

on tab_que.create
this.tabpage_load_detail=create tabpage_load_detail
this.tabpage_check_calls=create tabpage_check_calls
this.tabpage_ship_info=create tabpage_ship_info
this.tabpage_stop_info=create tabpage_stop_info
this.tabpage_single_appt=create tabpage_single_appt
this.tabpage_instructions=create tabpage_instructions
this.Control[]={this.tabpage_load_detail,&
this.tabpage_check_calls,&
this.tabpage_ship_info,&
this.tabpage_stop_info,&
this.tabpage_single_appt,&
this.tabpage_instructions}
end on

on tab_que.destroy
destroy(this.tabpage_load_detail)
destroy(this.tabpage_check_calls)
destroy(this.tabpage_ship_info)
destroy(this.tabpage_stop_info)
destroy(this.tabpage_single_appt)
destroy(this.tabpage_instructions)
end on

event selectionchanged;This.SetRedraw(false)
SetMicroHelp("Ready...")
CHOOSE CASE newindex
	CASE 2
		IF ib_new_check_call = TRUE Then
			tabpage_check_calls.dw_check_call_info.PostEvent("ue_retrieve")
			ib_new_check_call = FALSE
		Else
			If tabpage_check_calls.dw_check_call_info.Rowcount() = 0 Then
				setmicrohelp("No Check Call Found For This Primary")
		   End If
		END IF
	CASE 3
		IF ib_new_ship_info = TRUE Then
			tabpage_ship_info.dw_ship_info.PostEvent("ue_retrieve")
			ib_new_ship_info = FALSE
		END IF
	CASE 4
		IF ib_new_stop_info = TRUE Then
			tabpage_stop_info.dw_stop_info.PostEvent("ue_retrieve")
			ib_new_stop_info = FALSE
		END IF
	CASE 5
		IF ib_new_appt = TRUE Then
		   wf_load_single_appt()
			ib_new_appt = False
		End IF
	CASE 6
		IF ib_new_instruction = TRUE Then
			tabpage_instructions.dw_instructions.PostEvent("ue_retrieve")
			ib_new_instruction = False
		End IF 
//	CASE 7
//		IF ib_new_edi_review = TRUE Then
//			tabpage_edi.dw_214_review.PostEvent("ue_retrieve")
//			ib_new_edi_review = False
//		End IF 
END CHOOSE

This.SetRedraw(true)
end event

event selectionchanging;long ll_row, &
     ll_rc


IF  oldindex = 5 Then
	tab_que.tabpage_single_appt.dw_single_appt.AcceptText()
	ll_Row = tab_que.tabpage_single_appt.dw_single_appt.GetNextModified(0, PRIMARY!)
   IF ll_row > 0 Then
	   ll_rc = messagebox("Update", "Something changed in Appointment Tab.  Do you wish to save?", QUESTION!, YesNoCancel!)
		if ll_rc = 1 then
		  wf_update()
	   elseif ll_rc = 2 then
		   return 0
	   else
		   return 1
	   end if
	End IF
End IF



IF  oldindex = 6 Then
	tab_que.tabpage_instructions.dw_instructions.AcceptText()
	ll_Row = tab_que.tabpage_instructions.dw_instructions.GetNextModified(0, PRIMARY!)
   IF ll_row > 0 Then
	   ll_rc = messagebox("Update", "Freight Forwarder changed.  Do you wish to save?", QUESTION!, YesNoCancel!)
		if ll_rc = 1 then
		  wf_update()
	   elseif ll_rc = 2 then
		   return 0
	   else
		   return 1
	   end if
	End IF
End IF




end event

type tabpage_load_detail from userobject within tab_que
integer x = 18
integer y = 100
integer width = 3287
integer height = 1316
long backcolor = 12632256
string text = "Load Detail"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
string powertiptext = "Load Order Details"
dw_load_detail_hdr dw_load_detail_hdr
dw_stop_detail_info dw_stop_detail_info
r_1 r_1
st_1 st_1
st_2 st_2
uo_next_previous uo_next_previous
dw_messages dw_messages
end type

on tabpage_load_detail.create
this.dw_load_detail_hdr=create dw_load_detail_hdr
this.dw_stop_detail_info=create dw_stop_detail_info
this.r_1=create r_1
this.st_1=create st_1
this.st_2=create st_2
this.uo_next_previous=create uo_next_previous
this.dw_messages=create dw_messages
this.Control[]={this.dw_load_detail_hdr,&
this.dw_stop_detail_info,&
this.r_1,&
this.st_1,&
this.st_2,&
this.uo_next_previous,&
this.dw_messages}
end on

on tabpage_load_detail.destroy
destroy(this.dw_load_detail_hdr)
destroy(this.dw_stop_detail_info)
destroy(this.r_1)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.uo_next_previous)
destroy(this.dw_messages)
end on

type dw_load_detail_hdr from u_base_dw_ext within tabpage_load_detail
integer y = 8
integer width = 2574
integer height = 452
integer taborder = 2
string dataobject = "d_load_det_hdr"
boolean border = false
end type

type dw_stop_detail_info from u_base_dw_ext within tabpage_load_detail
event ue_post_tab pbm_custom24
integer y = 464
integer width = 2926
integer height = 444
integer taborder = 2
string dataobject = "d_load_det_stop_info"
boolean vscrollbar = true
end type

event ue_post_tab;call super::ue_post_tab;scrolltorow(il_error_row)
setrow(il_error_row)
setcolumn(ii_error_column)
setfocus()

end event

event clicked;call super::clicked;//long    ll_FoundRow
//
//string  ls_gettext
//
//If row < 1 Then Return
//
//ls_gettext = gettext()
//If dwo.name = "late_reason" Then
//    IF Trim(ls_gettext) = "" THEN Return
//      ll_FoundRow = idwc_latecode.Find ( "late_reason='"+ls_gettext+"'", 1, idwc_latecode.RowCount() )
//		IF ll_foundRow < 1 then return 1
//End if
end event

event constructor;call super::constructor;ii_rc = GetChild( 'late_reason', idwc_latecode )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for late code ")

//iw_frame.iu_netwise_data.nf_Gettutltype( "LATECODE", idwc_latecode )
//taken out and replace with code below
idwc_latecode.SetTrans(SQLCA)
idwc_latecode.Retrieve("LATECODE")
end event

event doubleclicked;call super::doubleclicked;String          ls_load_key, &
                ls_stop_code, &
                ls_input_string, &
                ls_app_id, &
                ls_window_name

window          lw_to_open
            
If row < 1 Then Return

ls_load_key = dw_load_detail_hdr.object.load_key[1]

If dwo.name = "stop_code" Then
      ls_stop_code = This.object.stop_code[Row]
      ls_input_string = ls_load_key + ls_stop_code
      OpenWithParm(w_stops_order_po_resp, ls_input_string) 
End if



If dwo.name = "stop_comment_ind" Then 
//   If ib_modify Then
        ls_stop_code = This.object.stop_code[Row]
//        ist_load_detail.stop_code  =  ls_stop_code
        ls_input_string =  ls_load_key + ls_stop_code
        OpenWithParm(w_stop_comments_add_child, ls_input_string) 
        This.SetItem(Row, "stop_comment_ind", "X")    
//    End if
End if




end event

event itemchanged;call super::itemchanged;Char       lc_carrier_notify_ind

//Integer    li_CurrentRow, &
Integer    li_CurrentCol 
//
long 		  ll_FoundRow

//String     ls_GetText
//
//li_CurrentRow = This.GetRow()
li_CurrentCol = This.GetColumn() 

//tab_que.tabpage_load_detail.dw_stop_detail_info.SetItemStatus(li_CurrentRow, 0, Primary!, DataModified!) 
//
//is_ObjectAtPointer = Left( is_ObjectAtPointer, Pos( is_ObjectAtPointer, "~t") - 1 )
//
If li_currentCol = 3 Then
   tab_que.tabpage_load_detail.dw_stop_detail_info.SetItem(Row, "late_reason_err_ind", 0) 
//   ls_GetText = GetText()
   IF Trim(data) = "" THEN Return
       ll_FoundRow = idwc_latecode.Find ( "type_code = '"+data+"'", 1, idwc_latecode.RowCount() )
       IF ll_FoundRow < 1 THEN
//          MessageBox ("Late Reason Error", ls_GetText + " is not a valid late reason code.", &
//          StopSign!, OK!)
//   		 ii_error_column = tab_que.tabpage_load_detail.dw_stop_detail_info.getcolumn()
//          il_error_row = This.GetRow()
//			 tab_que.tabpage_load_detail.dw_stop_detail_info.postevent("ue_post_tab")
//          This.SetColumn("late_reason") 
//          tab_que.tabpage_load_detail.dw_stop_detail_info.SetItem(li_CurrentRow, "late_reason_err_ind", 1) 
	       Return 1
		else
			IF Row = 1 Then
				tab_que.tabpage_load_detail.dw_load_detail_hdr.SetItem(1, "late_reason", data)
			End If
      End if 
End If 
end event

event ue_postconstructor;call super::ue_postconstructor;ib_updateable = TRUE
end event

type r_1 from rectangle within tabpage_load_detail
long linecolor = 8421504
integer linethickness = 4
long fillcolor = 12632256
integer x = 2642
integer y = 40
integer width = 160
integer height = 348
end type

type st_1 from statictext within tabpage_load_detail
integer x = 2651
integer y = 48
integer width = 146
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean enabled = false
string text = "Prev."
boolean focusrectangle = false
end type

type st_2 from statictext within tabpage_load_detail
integer x = 2661
integer y = 304
integer width = 137
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean enabled = false
string text = "Next"
boolean focusrectangle = false
end type

type uo_next_previous from u_spin_otr within tabpage_load_detail
integer x = 2679
integer y = 140
integer width = 82
integer height = 136
integer taborder = 4
end type

event resize;// overide ancestor script to stop me from flashing
end event

on uo_next_previous.destroy
call u_spin_otr::destroy
end on

type dw_messages from u_base_dw_ext within tabpage_load_detail
integer x = 5
integer y = 916
integer width = 2921
integer height = 392
integer taborder = 2
string dataobject = "d_hot_list_messages"
boolean vscrollbar = true
end type

event constructor;call super::constructor;ii_rc = GetChild( 'type_code', idwc_latecode )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for late code ")

//iw_frame.iu_netwise_data.nf_Gettutltype( "LATECODE", idwc_latecode )
//taken out and replace with code below
idwc_latecode.SetTrans(SQLCA)
idwc_latecode.Retrieve("LATECODE")

is_selection = "3"
end event

type tabpage_check_calls from userobject within tab_que
integer x = 18
integer y = 100
integer width = 3287
integer height = 1316
long backcolor = 12632256
string text = "Check Calls"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
string powertiptext = "Check Call History"
dw_check_call_info dw_check_call_info
end type

on tabpage_check_calls.create
this.dw_check_call_info=create dw_check_call_info
this.Control[]={this.dw_check_call_info}
end on

on tabpage_check_calls.destroy
destroy(this.dw_check_call_info)
end on

type dw_check_call_info from u_base_dw_ext within tabpage_check_calls
integer x = 14
integer y = 8
integer width = 2839
integer height = 1224
integer taborder = 2
string dataobject = "d_load_det_last_check_call"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;String          ls_load_key, &
                ls_stop_code, &
                ls_input_string


If row < 1 Then Return


If dwo.name = "stop_code" Then
      ls_load_key = tab_que.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
      ls_stop_code = This.object.stop_code[Row]
      ls_input_string = ls_load_key + ls_stop_code
      OpenWithParm(w_stops_order_po_resp, ls_input_string) 
End if



end event

event ue_retrieve;call super::ue_retrieve;integer               li_rtn				          

string                ls_check_call_inq_string, &
                      ls_req_load_key, &
                      ls_refetch_stop_code, &
                      ls_refetch_check_call_date, & 
	                   ls_refetch_check_call_time
						 
s_error               lstr_Error_Info

dwItemStatus	l_status

SetPointer( HourGlass! )

ls_req_load_key      = tab_que.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
ls_refetch_stop_code = " "
ls_refetch_check_call_date   = " " 
ls_refetch_check_call_time   = " "

lstr_error_info.se_event_name = "wf_get_check_calls_history"
lstr_error_info.se_window_name = "w_load_detail"
lstr_error_info.se_procedure_name = "wf_get_check_calls_history"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ls_check_call_inq_string	= Space(1181)
ls_refetch_stop_code       = Space(2)
ls_refetch_check_call_date = Space(10)
ls_refetch_check_call_time = Space(8)

tab_que.tabpage_check_calls.dw_check_call_info.Reset()
SetRedraw(false)

DO
	ls_check_call_inq_string	= Space(1181)	
	
	li_rtn =  iu_ws_review_queue_load_list.nf_otrt14er(ls_req_load_key, ls_check_call_inq_string,lstr_Error_Info)
	
//	li_rtn = iu_otr003.nf_otrt14ar(ls_req_load_key, ls_check_call_inq_string, &
//         ls_refetch_stop_code, ls_refetch_check_call_date, ls_refetch_check_call_time, &
//         lstr_error_info, 0)

	// Check the return code from the above function call
	IF li_rtn <> 0 THEN
   	return(-1)
	ELSE
   	iw_frame.SetMicroHelp( "Ready")
	END IF

	li_rtn = tab_que.tabpage_check_calls.dw_check_call_info.ImportString(Trim(ls_check_call_inq_string))

LOOP WHILE trim(ls_refetch_check_call_date) > "" 

//to prevent background color of columns from changing to white
//when coming from tab 3 or 4
tab_que.tabpage_check_calls.dw_check_call_info.object.stop_code.background.color = 12632256
tab_que.tabpage_check_calls.dw_check_call_info.object.cc_date.background.color = 12632256
tab_que.tabpage_check_calls.dw_check_call_info.object.cc_time.background.color = 12632256
tab_que.tabpage_check_calls.dw_check_call_info.object.city.background.color = 12632256
tab_que.tabpage_check_calls.dw_check_call_info.object.state.background.color = 12632256
tab_que.tabpage_check_calls.dw_check_call_info.object.late_reason.background.color = 12632256
tab_que.tabpage_check_calls.dw_check_call_info.object.miles_out.background.color = 12632256
tab_que.tabpage_check_calls.dw_check_call_info.object.eta_date.background.color = 12632256
tab_que.tabpage_check_calls.dw_check_call_info.object.eta_time.background.color = 12632256
tab_que.tabpage_check_calls.dw_check_call_info.object.eta_contact.background.color = 12632256
SetRedraw(true)

tab_que.tabpage_check_calls.dw_check_call_info.Describe("DataWindow.ReadOnly")
tab_que.tabpage_check_calls.dw_check_call_info.Modify("DataWindow.ReadOnly=Yes")

tab_que.tabpage_check_calls.dw_check_call_info.ResetUpdate()
ib_new_check_call = false    
end event

type tabpage_ship_info from userobject within tab_que
event create ( )
event destroy ( )
integer x = 18
integer y = 100
integer width = 3287
integer height = 1316
long backcolor = 12632256
string text = "Ship Info"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
string powertiptext = "Shipping Information"
dw_ship_info dw_ship_info
end type

on tabpage_ship_info.create
this.dw_ship_info=create dw_ship_info
this.Control[]={this.dw_ship_info}
end on

on tabpage_ship_info.destroy
destroy(this.dw_ship_info)
end on

type dw_ship_info from u_base_dw_ext within tabpage_ship_info
integer x = 201
integer y = 68
integer width = 2405
integer height = 1144
integer taborder = 2
string dataobject = "d_ship_info"
boolean border = false
end type

event ue_retrieve;call super::ue_retrieve;integer               li_rtn				          

string                ls_ship_info_inq_string, &
                      ls_req_load_key

s_error               lstr_Error_Info


SetPointer( HourGlass! )

ls_req_load_key      = tab_que.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]

lstr_error_info.se_event_name = "wf_get_ship_info"
lstr_error_info.se_window_name = "w_load_detail"
lstr_error_info.se_procedure_name = "wf_get_ship_info"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)
 
ls_ship_info_inq_string	= Space(257)

//Added code for the netwise convertion 09/06/2016
li_rtn = iu_ws_review_queue_load_list.nf_otrt15er(ls_req_load_key, ls_ship_info_inq_string, lstr_Error_info)

//li_rtn = iu_otr003.nf_otrt15ar(ls_req_load_key, ls_ship_info_inq_string, &
//         lstr_error_info, 0)

// Check the return code from the above function call
IF li_rtn < 0 THEN
  	return(-1)
ELSE
   iw_frame.SetMicroHelp( "Ready...")
END IF

tab_que.tabpage_ship_info.dw_ship_info.Reset()

li_rtn = tab_que.tabpage_ship_info.dw_ship_info.ImportString(Trim(ls_ship_info_inq_string) )
  

      
end event

type tabpage_stop_info from userobject within tab_que
event create ( )
event destroy ( )
integer x = 18
integer y = 100
integer width = 3287
integer height = 1316
long backcolor = 12632256
string text = "Stop Info"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
string powertiptext = "Stop Information"
dw_stop_info dw_stop_info
end type

on tabpage_stop_info.create
this.dw_stop_info=create dw_stop_info
this.Control[]={this.dw_stop_info}
end on

on tabpage_stop_info.destroy
destroy(this.dw_stop_info)
end on

type dw_stop_info from u_base_dw_ext within tabpage_stop_info
integer width = 3237
integer height = 1188
integer taborder = 2
string dataobject = "d_stop_info"
boolean vscrollbar = true
boolean border = false
end type

event doubleclicked;call super::doubleclicked;String          ls_load_key, &
                ls_stop_code, &
                ls_input_string


If row < 1 Then Return

If dwo.name = "stop_code" Then
      ls_load_key = tab_que.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
      ls_stop_code = This.object.stop_code[Row]
      ls_input_string = ls_load_key + ls_stop_code
      OpenWithParm(w_stops_order_po_resp, ls_input_string) 
End if




end event

event clicked;call super::clicked;string ls_load_key, &
       ls_stop_code, &
       ls_input_string

If row < 1 Then Return

If is_objectatpointer = "instructions_ind_t" Then
      ls_load_key = This.object.load_number[1]
      ls_stop_code = This.object.stop_code[Row]
      ls_input_string = ls_load_key + ls_stop_code
      OpenWithParm(w_stop_instructions_child, ls_input_string, iw_frame.iw_active_sheet)
End if


end event

event ue_retrieve;call super::ue_retrieve;integer               li_rtn		                

string                ls_stop_info_inq_string, &
                      ls_req_load_key, &
                      ls_refetch_stop_code, &
                      ls_refetch_comment_date, &
                      ls_refetch_comment_time,&
					ls_se_app_name		 

s_error               lstr_Error_Info


SetPointer( HourGlass! )

ls_req_load_key      = tab_que.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
ls_se_app_name = 'OTR'


lstr_error_info.se_app_name = "OTR"
lstr_error_info.se_event_name = "wf_get_stop_info"
lstr_error_info.se_window_name = "w_stop_info_child"
lstr_error_info.se_procedure_name = "wf_get_stop_info"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)
 
ls_refetch_stop_code    = Space(2)
ls_refetch_comment_date = Space(10)
ls_refetch_comment_time = Space(8)

tab_que.tabpage_stop_info.dw_stop_info.Reset()

do
ls_stop_info_inq_string	= Space(4651)

//Added code for Netwise convertion

li_rtn = iu_ws_review_queue_load_list.nf_otrt31er(ls_req_load_key,ls_se_app_name,&
					ls_stop_info_inq_string,lstr_error_info)	

//li_rtn = iu_otr003.nf_otrt31ar(ls_req_load_key, ls_stop_info_inq_string, &
//         ls_refetch_stop_code, ls_refetch_comment_date, ls_refetch_comment_time, &
//         lstr_error_info, 0)

// Check the return code from the above function call
IF li_rtn < 0 THEN
    return(-1)
ELSE
    iw_frame.SetMicroHelp( "Ready...")
END IF

li_rtn = tab_que.tabpage_stop_info.dw_stop_info.ImportString(ls_stop_info_inq_string) 

LOOP WHILE trim(ls_refetch_stop_code) > ""      

tab_que.tabpage_stop_info.dw_stop_info.SetSort("sort_number asc")
tab_que.tabpage_stop_info.dw_stop_info.Sort()
tab_que.tabpage_stop_info.dw_stop_info.GroupCalc()
end event

type tabpage_single_appt from userobject within tab_que
integer x = 18
integer y = 100
integer width = 3287
integer height = 1316
long backcolor = 12632256
string text = "Single Appt"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
string powertiptext = "Single Appointment"
dw_single_appt dw_single_appt
end type

on tabpage_single_appt.create
this.dw_single_appt=create dw_single_appt
this.Control[]={this.dw_single_appt}
end on

on tabpage_single_appt.destroy
destroy(this.dw_single_appt)
end on

type dw_single_appt from u_base_dw_ext within tabpage_single_appt
event ue_post_tab ( )
integer x = 9
integer y = 28
integer width = 2825
integer height = 1284
integer taborder = 2
string dataobject = "d_appt_update"
boolean vscrollbar = true
boolean border = false
end type

event ue_post_tab;call super::ue_post_tab;scrolltorow(il_error_row)
setrow(il_error_row)
setcolumn(ii_error_column)
setfocus()

end event

event constructor;call super::constructor;This.is_selection = "0"
end event

event doubleclicked;call super::doubleclicked;String          ls_load_key, &
                ls_stop_code, &
                ls_input_string, &
                ls_GetText

If is_ObjectAtPointer = "stop_code" Then
      ls_load_key = This.object.load_key[Row]
   ls_stop_code = This.object.stop_code[Row]
   ls_input_string =  ls_load_key + ls_stop_code
   OpenWithParm(w_stops_order_po_resp, ls_input_string)     
End if

end event

event itemchanged;call super::itemchanged;date      ld_ApptDate

Long     ll_deviation

string   ls_temp_hour, &
         ls_temp_min, &
         ls_temp_time, &
			ls2_temp_hour, &
			ls2_temp_min, &
			ls2_temp_time

time     lt_ApptTime, &   
         lt_deviation_time, &
         lt_to_hours, &
			lt2_to_hours


      
ld_ApptDate = This.GetItemDate(Row,"appt_date")

If is_ColumnName = "appt_date" Then
   If IsNull(Data) then      
      MessageBox("Appointment Date", "Please enter an Appointment Date") 
      This.SetColumn("appt_Date")     
   Else
      If This.object.stop_code[Row] = "01" Then
         If ld_ApptDate <> This.object.promised_deliv_date[Row] Then
            This.SetItem(Row,"override_ind","E")             
         End if
      Else
         If ld_ApptDate <> This.object.sched_deliv_date[Row] Then
            This.SetItem(Row,"override_ind","E")             
         End if
      End if
   End if
End if

If is_ColumnName = "appt_time" Then
   lt_ApptTime = Time(This.GetText())
   This.SetItem(Row, "override_ind", " ")
   If This.object.late_load_ind[Row] <>  "T"  and  &
      This.object.late_load_ind[Row] <>  "W"  and  &   
      This.object.late_load_ind[Row] <>  "O"  and  &
      This.object.late_load_ind[Row] <>  "L"  Then
      If This.object.stop_code[Row] = "01" Then
         If ld_ApptDate <> This.object.promised_deliv_date[Row] Then
            This.SetItem(Row,"override_ind","E")                 
         Else      
            If This.object.keyed_ind[Row] = "Y" Then  
               ll_deviation = This.object.appt_deviation_hours[Row] * 3600              
               lt_deviation_time = RelativeTime(This.object.promised_deliv_time[Row], ll_deviation) 
               If lt_ApptTime > lt_deviation_time Then
                    This.SetItem(Row,"override_ind","E")
               End if
            Else  
               If (This.object.recv_from_hours[Row] = "0000") and &
                  (This.object.recv_to_hours[Row] = "0000") and &
						(This.object.recv2_from_hours[Row] = "0000") and &
						(This.object.recv2_to_hours[Row] = "0000") Then
                   ll_deviation = This.object.appt_deviation_hours[Row] * 3600    
                   lt_deviation_time = RelativeTime(This.object.promised_deliv_time[Row], ll_deviation) 
                   If lt_ApptTime > lt_deviation_time Then
                       This.SetItem(Row,"override_ind","E")
                   End if
               Else
                   ls_temp_time = This.object.recv_to_hours[Row]
                   ls_temp_hour = Left(ls_temp_time,2)
                   ls_temp_min = Mid(ls_temp_time,3,2)
                   ls_temp_time = ls_temp_hour + ":" + ls_temp_min 
                   lt_to_hours = Time(ls_temp_time)
						 
						 ls2_temp_time = This.object.recv2_to_hours[Row]
                   ls2_temp_hour = Left(ls2_temp_time,2)
                   ls2_temp_min = Mid(ls2_temp_time,3,2)
                   ls2_temp_time = ls2_temp_hour + ":" + ls2_temp_min 
                   lt2_to_hours = Time(ls2_temp_time)
						 
                   If lt_ApptTime > lt_to_hours Then
							 If lt2_to_hours = Time('00:00') Then
                         This.SetItem(Row,"override_ind","E")     
							 End if
                   End if
						 
						 If lt_ApptTime > lt_to_hours Then
							 If lt_ApptTime > lt2_to_hours Then
                         This.SetItem(Row,"override_ind","E")     
							 End if
                   End if
						 
                 End if
              End if 
        End if   
     Else
         If ld_ApptDate <> This.object.sched_deliv_date[Row] Then
            This.SetItem(Row,"override_ind","E")                 
         Else
            If (This.object.recv_from_hours[Row] = "0000") and &
               (This.object.recv_to_hours[Row] = "0000") and &
					(This.object.recv2_from_hours[Row] = "0000") and &
					(This.object.recv2_to_hours[Row] = "0000") Then
                ll_deviation = This.object.appt_deviation_hours[Row] * 3600    
                lt_deviation_time = RelativeTime(This.object.sched_deliv_time[Row], ll_deviation) 
                If lt_ApptTime > lt_deviation_time Then
                    This.SetItem(Row,"override_ind","E")
                End if
            Else
                ls_temp_time = This.object.recv_to_hours[Row]
                ls_temp_hour = Left(ls_temp_time,2)
                ls_temp_min = Mid(ls_temp_time,3,2)
                ls_temp_time = ls_temp_hour + ":" + ls_temp_min 
                lt_to_hours = Time(ls_temp_time) 
					 
					 ls2_temp_time = This.object.recv2_to_hours[Row]
                ls2_temp_hour = Left(ls2_temp_time,2)
                ls2_temp_min = Mid(ls2_temp_time,3,2)
                ls2_temp_time = ls2_temp_hour + ":" + ls2_temp_min 
                lt2_to_hours = Time(ls2_temp_time) 
					 
                If lt_ApptTime > lt_to_hours Then
						 If lt2_to_hours = Time('00:00') Then
                      This.SetItem(Row,"override_ind","E")     
						 End if
                End if
					 
					 If lt_ApptTime > lt_to_hours Then
						 If lt_ApptTime > lt2_to_hours Then
                      This.SetItem(Row,"override_ind","E")     
						 End if
                End if
					 
            End if
           End if
         End if
    Else
       This.SetItem(Row,"override_ind","Y")  
    End if
End if

 
If This.object.override_ind[Row] = "E" Then
    This.SelectRow(Row,TRUE)
Else
    This.SelectRow(Row,FALSE)
End if

end event

type tabpage_instructions from userobject within tab_que
integer x = 18
integer y = 100
integer width = 3287
integer height = 1316
long backcolor = 12632256
string text = "Instructions"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_instructions dw_instructions
end type

on tabpage_instructions.create
this.dw_instructions=create dw_instructions
this.Control[]={this.dw_instructions}
end on

on tabpage_instructions.destroy
destroy(this.dw_instructions)
end on

type dw_instructions from u_base_dw_ext within tabpage_instructions
integer y = 16
integer width = 2821
integer height = 1296
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_instructions"
boolean vscrollbar = true
boolean border = false
end type

event ue_retrieve;integer  li_rtn
			
string   ls_order_info_string, &
         ls_req_load_key, &
         ls_req_order, &
         ls_refetch_stop_num, &
         ls_refetch_order
			
	 
SetPointer( HourGlass! )


s_error  lstr_Error_Info

tab_que.tabpage_instructions.dw_instructions.Reset()

// Turn the redraw off for the window while we retrieve
This.SetRedraw(FALSE)


ls_req_load_key = tab_que.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
ls_req_order = tab_que.tabpage_load_detail.dw_load_detail_hdr.object.order_number[1]

lstr_error_info.se_event_name = "ue_retrieve"
lstr_error_info.se_window_name = "w_instructions"
lstr_error_info.se_procedure_name = "dw_instructions"
lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_message = Space(71)

ls_refetch_stop_num  = Space(2)
ls_refetch_order     = Space(7)


Do

   ls_order_info_string = Space(15000)
 	
 //Added Code for Netwise convertion 09/06/16	
  li_rtn =iu_ws_review_queue_load_list.nf_otrt56er(ls_req_load_key, ls_req_order,ls_order_info_string, lstr_Error_info)
	 

// Check the return code from the above function call
If li_rtn <> 0 Then
		tab_que.tabpage_instructions.dw_instructions.ResetUpdate()
		This.SetRedraw(TRUE)
      Return -1
ELSE
      iw_frame.SetMicroHelp( "Ready...")
END IF

     li_rtn = tab_que.tabpage_instructions.dw_instructions.ImportString(Trim(ls_order_info_string))

loop while len(Trim(ls_refetch_order)) > 0

//ls_carrier_code = tab_load_detail.tabpage_single_appt.dw_single_appt.GetItemString(1,"carrier_code") 

tab_que.tabpage_instructions.dw_instructions.SetFocus()

tab_que.tabpage_instructions.dw_instructions.ResetUpdate()

// Turn the redraw on for the window after we retrieve
This.SetRedraw(TRUE)

SetPointer( arrow! )

Return 1



end event

event constructor;call super::constructor;//IF message.nf_get_app_id() <> 'OTR' Then Return

ii_rc = GetChild( 'frtforwd_code', idwc_address_code )
IF ii_rc = -1 THEN MessageBox("DataWindow Child Error", "Unable to get Child Handle for address code ")

idwc_address_code.SetTrans(SQLCA)
idwc_address_code.Retrieve("FRTFORWD")
end event

event itemchanged;call super::itemchanged;THIS.SelectText (1,15)      
string ls_str 
long ll_cur_row
If is_ColumnName = "frtforwd_code" Then
	
   IF Trim(data) <> "" THEN 
      ll_cur_row = idwc_address_code.Find ( "address_code='"+ &
		             data+"'", 1, idwc_address_code.RowCount() )
						 
      IF ll_cur_row < 1 THEN
         MessageBox( "Freight Forwarder Error", &
			            data + " is not a Valid Freight Forwarder code." )
      	Return 1
      else
	       ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_name")
	       this.setitem(1, "frtforwd_name", ls_str)
	       ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_city")
	       this.setitem(1, "frtforwd_city", ls_str)
	       ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_state")
	       this.setitem(1, "frtforwd_state_abrv", ls_str)
      End if
   Else 
		 this.setitem(1, "frtforwd_name", " ")
		 this.setitem(1, "frtforwd_city", " ")
		 this.setitem(1, "frtforwd_state_abrv", " ")
	End if		
End if
end event

event itemerror;call super::itemerror;Return 1
end event

