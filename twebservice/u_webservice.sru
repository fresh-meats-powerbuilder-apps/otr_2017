HA$PBExportHeader$u_webservice.sru
forward
global type u_webservice from nonvisualobject
end type
end forward

global type u_webservice from nonvisualobject
end type
global u_webservice u_webservice

type variables
// Declare soap connection variable
soapconnection ispcn_conn

// Declare input and output program interface variables
otrt01sr_programinterface	ipgif_output
otrt01sr_programinterface1	ipgif_input

//Declare container interface variables
otrt01sr_programinterfaceotrt01ci1	ictif_cics_container_in
otrt01sr_programinterfaceotrt01in1	ictif_input_container_in
otrt01sr_programinterfaceotrt01ot	ictif_output_container_out
otrt01sr_programinterfaceotrt01pg1	ictif_program_container_in
otrt01sr_programinterfaceotrt01pg	ictif_program_container_out

//Declare container variables
otrt01sr_programinterfaceotrt01ciotr000sr_cics_container1 ict_otr000sr_cics_container
otrt01sr_programinterfaceotrt01pgotr000sr_program_container1 ict_otr000sr_program_container_in
otrt01sr_programinterfaceotrt01pgotr000sr_program_container ict_otr000sr_program_container_out

//Web Service path
String		is_web_service_address

//u_abstractdefaultmanager	iu_defaultmanager


//Error Handle Variables

// Error structure used in all RPC Calls
s_error	istr_error

// Holds information on the CommHandles
//str_CommHandles		istr_CommHandles[]

integer	ii_messagebox_rtn

//u_wrkbench16_externals		luo_wkb16
//u_wrkbench32_externals		luo_wkb32
end variables

forward prototypes
public function boolean uf_initialize ()
public function integer uf_rpccall (string as_imput_string, ref string as_output_string, ref s_error astr_error_info, string as_program_name, string as_tran_id)
public function integer uf_rpcupd (string as_input_string, ref s_error astr_error_info, string as_program_name, string as_tran_id)
public function boolean uf_display_message_ws (integer ai_rtn, s_error astr_errorinfo, integer ai_commhandle)
public function integer uf_checkrpcerror_ws (integer ai_returnval, s_error astr_errorinfo, integer ai_commhandle, boolean ab_visual)
public function integer uf_rpcupd (string as_input_string, ref s_error astr_error_info, string as_program_name, string as_tran_id, integer ai_version_number)
public function integer uf_rpccall (string as_imput_string, ref string as_output_string, ref s_error astr_error_info, string as_program_name, string as_tran_id, integer ai_version_number)
end prototypes

public function boolean uf_initialize ();//Boolean		lb_initialized
//
//// Get web service address from IBP002.ini file
//
//lb_initialized = au_classfactory.uf_GetObject("u_inidefaultmanager", iu_defaultmanager, au_errorcontext)
//If Not au_ErrorContext.uf_IsSuccessful() Then
//	Return False
//End If
//
//// We initialize it no matter what
//If Not iu_defaultmanager.uf_Initialize(au_classfactory, "IBP002.INI", "Web Service URL", au_errorcontext) Then
//	// Error initializing default manager
//	// Populate error context and return False
//	au_errorcontext.uf_AppendText("Can't initialize default manager for web service info")
//	au_errorcontext.uf_SetReturnCode(-1)
//	Return False
//End If
//
//If Not iu_defaultmanager.uf_GetData("otrt01SRpath", is_web_service_address, au_errorcontext) Then
//	// Error getting server prefix
//	// Populate error context and return false
//	au_errorcontext.uf_AppendText("Unable to obtain web service path")
//	au_errorcontext.uf_SetReturnCode(-1)
//	Return False
//End If

s_error lstr_error_info

is_web_service_address = ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "Web Service URL", "OTRT01SRpath", "") 
If is_web_service_address = '' then
	lstr_error_info.se_procedure_name = ''
	lstr_error_info.se_return_code = ''
	lstr_error_info.se_message = "Can't initialize default manager for web service info"
	uf_display_message_ws(-1, lstr_error_info, 0) 
	Return False
end if

//Instantiate soapconnection
ispcn_conn = create soapconnection

//Instantiate input and output program interfaces
ipgif_output = create otrt01sr_programinterface
ipgif_input = create otrt01sr_programinterface1

//Instantiate input and output container interfaces
ictif_cics_container_in = create otrt01sr_programinterfaceotrt01ci1
ipgif_input.otrt01ci = ictif_cics_container_in

ictif_input_container_in = create otrt01sr_programinterfaceotrt01in1
ipgif_input.otrt01in = ictif_input_container_in

ictif_output_container_out = create otrt01sr_programinterfaceotrt01ot
ipgif_output.otrt01ot = ictif_output_container_out

ictif_program_container_in = create otrt01sr_programinterfaceotrt01pg1
ipgif_input.otrt01pg = ictif_program_container_in

ictif_program_container_out = create otrt01sr_programinterfaceotrt01pg
ipgif_output.otrt01pg = ictif_program_container_out

//Instantiate container variables
ict_otr000sr_cics_container = create otrt01sr_programinterfaceotrt01ciotr000sr_cics_container1
ictif_cics_container_in.otr000sr_cics_container = ict_otr000sr_cics_container

ict_otr000sr_program_container_in  = create otrt01sr_programinterfaceotrt01pgotr000sr_program_container1
ictif_program_container_in.otr000sr_program_container = ict_otr000sr_program_container_in

ict_otr000sr_program_container_out  = create otrt01sr_programinterfaceotrt01pgotr000sr_program_container
ictif_program_container_out.otr000sr_program_container = ict_otr000sr_program_container_out

return True

end function

public function integer uf_rpccall (string as_imput_string, ref string as_output_string, ref s_error astr_error_info, string as_program_name, string as_tran_id);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_temp, ls_target_address, ls_OutputString, ls_output, ls_Stringind

u_String_Functions		lu_string

gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

otrt01sr_otrt01srservice p_obj

ict_otr000sr_cics_container.otr000sr_req_password = SQLCA.DBPass
ict_otr000sr_cics_container.otr000sr_req_userid =  SQLCA.Userid
ict_otr000sr_cics_container.otr000sr_req_program = as_program_name
ict_otr000sr_cics_container.otr000sr_req_tranid = as_tran_id

ict_otr000sr_program_container_in.otr000sr_rval = 0
ict_otr000sr_program_container_in.otr000sr_message = space(200)
ict_otr000sr_program_container_in.otr000sr_version_number = 0

ipgif_input.otrt01in.value = as_imput_string

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string( SQLCA.Userid)+"~",Password=~""+string(SQLCA.DBPass)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "otrt01sr_otrt01srservice", is_web_service_address)
if ll_ret <> 0 then
	MessageBox("Error", "Cannot create instance of proxy")
	Return ll_ret
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

as_output_string= ''

Do
	
	ict_otr000sr_program_container_in.otr000sr_last_record_num = ll_last_record_num
	ict_otr000sr_program_container_in.otr000sr_max_record_num = ll_max_record_num
	ict_otr000sr_program_container_in.otr000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.otrt01sroperation(ipgif_input)
		  catch (SoapException e)
				//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				astr_error_info.se_procedure_name = ''
				astr_error_info.se_return_code = ''
				astr_error_info.se_message = e.Text
				uf_display_message_ws(-1, astr_error_info, 0) 
				Return -1
	end try
		
	ictif_program_container_out = ipgif_output.otrt01pg 
	li_rval = ictif_program_container_out.otr000sr_program_container.otr000sr_rval
	if li_rval < 0 then
//		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
		//check for any RPC errors to display messages
		ls_message = ictif_program_container_out.otr000sr_program_container.otr000sr_message
		
		If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
			astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
			astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
			astr_error_info.se_message = ls_message
		else
			astr_error_info.se_procedure_name = ''
			astr_error_info.se_return_code = ''
			astr_error_info.se_message = ls_message
		end if
		
		uf_display_message_ws(li_rval, astr_error_info, 0) 
		Return -1
	else
		ls_message = ictif_program_container_out.otr000sr_program_container.otr000sr_message
//		au_ErrorContext.uf_AppendText(ls_message)
//		au_ErrorContext.uf_SetReturnCode(li_rval)		
		
		ll_last_record_num = ictif_program_container_out.otr000sr_program_container.otr000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.otr000sr_program_container.otr000sr_max_record_num
		ll_task_number = ictif_program_container_out.otr000sr_program_container.otr000sr_task_num
// strings are coming in with spaces to reserve space for Netwise calls, so clear strings 		
		
				
		
		if isnull(ipgif_output.otrt01ot) then
//			as_header_string = ''
//			as_recv_window_inq_string = ''
		else	
			as_output_string +=  trim(ipgif_output.otrt01ot.value)
		end if
	end if
Loop while ll_last_record_num <> ll_max_record_num	

//check for any RPC errors to display messages
ls_message = ictif_program_container_out.otr000sr_program_container.otr000sr_message

If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
	astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_message = ls_message
else
	astr_error_info.se_procedure_name = ''
	astr_error_info.se_return_code = ''
	astr_error_info.se_message = ls_message
end if

uf_display_message_ws(li_rval, astr_error_info, 0)  

Return li_rval

end function

public function integer uf_rpcupd (string as_input_string, ref s_error astr_error_info, string as_program_name, string as_tran_id);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_temp, ls_target_address, ls_OutputString, ls_output, ls_Stringind

u_String_Functions		lu_string

gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

otrt01sr_otrt01srservice p_obj

ict_otr000sr_cics_container.otr000sr_req_password = SQLCA.DBPass
ict_otr000sr_cics_container.otr000sr_req_userid =  SQLCA.Userid
ict_otr000sr_cics_container.otr000sr_req_program = as_program_name
ict_otr000sr_cics_container.otr000sr_req_tranid = as_tran_id

ict_otr000sr_program_container_in.otr000sr_rval = 0
ict_otr000sr_program_container_in.otr000sr_message = space(200)
ict_otr000sr_program_container_in.otr000sr_version_number = 0

ipgif_input.otrt01in.value = as_input_string

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string( SQLCA.Userid)+"~",Password=~""+string(SQLCA.DBPass)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "otrt01sr_otrt01srservice", is_web_service_address)
if ll_ret <> 0 then
	MessageBox("Error", "Cannot create instance of proxy")
	Return ll_ret
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

//as_output_string= ''

//Do
	
	ict_otr000sr_program_container_in.otr000sr_last_record_num = ll_last_record_num
	ict_otr000sr_program_container_in.otr000sr_max_record_num = ll_max_record_num
	ict_otr000sr_program_container_in.otr000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.otrt01sroperation(ipgif_input)
		  catch (SoapException e)
				astr_error_info.se_procedure_name = ''
				astr_error_info.se_return_code = ''
				astr_error_info.se_message = e.Text
				uf_display_message_ws(-1, astr_error_info, 0) 
				
				//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				ls_message = ictif_program_container_out.otr000sr_program_container.otr000sr_message
				
				If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
					astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
					astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
					astr_error_info.se_message = ls_message
				else
					astr_error_info.se_procedure_name = ''
					astr_error_info.se_return_code = ''
					astr_error_info.se_message = ls_message
				end if
				
				uf_display_message_ws(li_rval, astr_error_info, 0) 
				Return -1
	end try
		
	ictif_program_container_out = ipgif_output.otrt01pg 
	li_rval = ictif_program_container_out.otr000sr_program_container.otr000sr_rval
	if li_rval < 0 then
//		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.otr000sr_program_container.otr000sr_message
//		au_ErrorContext.uf_AppendText(ls_message)
//		au_ErrorContext.uf_SetReturnCode(li_rval)		
		
//		ll_last_record_num = ictif_program_container_out.otr000sr_program_container.otr000sr_last_record_num
//		ll_max_record_num = ictif_program_container_out.otr000sr_program_container.otr000sr_max_record_num
		ll_task_number = ictif_program_container_out.otr000sr_program_container.otr000sr_task_num
// strings are coming in with spaces to reserve space for Netwise calls, so clear strings 		
		
				
		
		if isnull(ipgif_output.otrt01ot) then
//			as_header_string = ''
//			as_recv_window_inq_string = ''
		else	
//			as_output_string +=  trim(ipgif_output.otrt01ot.value)
		end if
	end if
//Loop while ll_last_record_num <> ll_max_record_num	

//check for any RPC errors to display messages
ls_message = ictif_program_container_out.otr000sr_program_container.otr000sr_message

If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
	astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_message = ls_message
else
	astr_error_info.se_procedure_name = ''
	astr_error_info.se_return_code = ''
	astr_error_info.se_message = ls_message
end if

uf_display_message_ws(li_rval, astr_error_info, 0) 

Return li_rval

end function

public function boolean uf_display_message_ws (integer ai_rtn, s_error astr_errorinfo, integer ai_commhandle);Boolean  lb_retval
			
Int      li_rpc_error_rtn, &
			li_rtn
String	ls_microhelp_str, &
			ls_active_sheet_title
			
s_rpc_error 			lstr_rpc_error_info			
			
window					lw_RPCError			

LB_RETVAL = TRUE
 
//li_rpc_error_rtn = This.uf_CheckRPCError_ws(ai_rtn,	astr_errorinfo, ai_commhandle, True)

IF ai_rtn < 0 THEN
	lstr_rpc_error_info.se_app_name = astr_errorinfo.se_app_name
	lstr_rpc_error_info.se_window_name = astr_errorinfo.se_window_name
	lstr_rpc_error_info.se_function_name = astr_errorinfo.se_function_name
	lstr_rpc_error_info.se_event_name = astr_errorinfo.se_event_name
	lstr_rpc_error_info.se_procedure_name = astr_errorinfo.se_procedure_name
	lstr_rpc_error_info.se_user_id = astr_errorinfo.se_user_id
	lstr_rpc_error_info.se_return_code = astr_errorinfo.se_return_code
	lstr_rpc_error_info.se_message = astr_errorinfo.se_message
	lstr_rpc_error_info.se_rval = ai_rtn
//		lstr_rpc_error_info.se_commerror = li_commerror
//		lstr_rpc_error_info.se_commerrmsg = ls_commerrmsg
//		lstr_rpc_error_info.se_neterror = ll_neterror
//		if lstr_rpc_error_info.se_neterror <> 0 then
//			lstr_rpc_error_info.se_neterrmsg = ls_neterrmsg
//		else
//			lstr_rpc_error_info.se_neterrmsg = space(100)
//		end if
//		lstr_rpc_error_info.se_primaryerror = ll_primaryerror
//		lstr_rpc_error_info.se_secondaryerror = ll_secondaryerror
	openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")
	if ai_rtn >= 0 then
		li_rpc_error_rtn = -25
	else
		li_rpc_error_rtn = ai_rtn
	end if
END IF

IF li_rpc_error_rtn >= 0 THEN
	IF ai_rtn = 0 and li_rpc_error_rtn = 0 then
		IF LenA(TRIM( astr_errorinfo.se_Message)) > 0 THEN
			ls_microHelp_str = astr_errorinfo.se_Message
			ls_microHelp_str = LeftA( ls_microHelp_str,71)
			gw_netwise_frame.SetMicroHelp( Ls_MicroHelp_Str)
			RETURN TRUE
		END IF
	END IF
	IF ai_rtn < 0 THEN
		MessageBox("Error Updating", astr_errorinfo.se_Message)
		lb_retval = FALSE		
  	ELSE
		IF ai_rtn > 0 and ai_rtn < 10 THEN
			// Set Micro Help      
			ls_microHelp_str = astr_errorinfo.se_Message
		   ls_microHelp_str = LeftA( ls_microHelp_str,71)
			gw_netwise_frame.SetMicroHelp( Ls_MicroHelp_str)
			lb_retval = FALSE
		END IF
	END IF
	IF ai_rtn >= 10 THEN
		If IsValid( gw_netwise_frame) Then
			Window	lw_active_sheet

			lw_active_sheet = gw_netwise_frame.GetActiveSheet()
			If IsValid(lw_active_sheet) Then
				ls_Active_Sheet_Title = lw_active_sheet.Title
			End If
		END IF
		CHOOSE CASE ai_rtn
			CASE	10
				MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message)
				ii_messagebox_rtn = 1
			CASE	11
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, OKCANCEL!)	
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = li_rtn
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	12
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, YESNO!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSE
					ii_messagebox_rtn = 3
				END IF
			CASE	13
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, YESNOCANCEL!)		
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 3
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	14
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, RETRYCANCEL!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	15
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, ABORTRETRYIGNORE!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 5
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 7
				END IF
			CASE	20
				MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, OK!)
				ii_messagebox_rtn = 1
			CASE	21
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, OKCANCEL!)	
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = li_rtn
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	22
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, YESNO!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSE
					ii_messagebox_rtn = 3
				END IF
			CASE	23
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, YESNOCANCEL!)		
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 3
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	24
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, RETRYCANCEL!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	25
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, ABORTRETRYIGNORE!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 5
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 7
				END IF
			CASE	30
				MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, OK!)
				ii_messagebox_rtn = 1
			CASE	31
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, OKCANCEL!)	
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = li_rtn
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	32
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, YESNO!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSE
					ii_messagebox_rtn = 3
				END IF
			CASE	33
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, YESNOCANCEL!)		
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 3
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	34
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, RETRYCANCEL!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	35
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, ABORTRETRYIGNORE!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 5
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 7
				END IF
			CASE	40
				MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, OK!)
				ii_messagebox_rtn = 1
			CASE	41
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, OKCANCEL!)	
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = li_rtn
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	42
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, YESNO!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSE
					ii_messagebox_rtn = 3
				END IF
			CASE	43
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, YESNOCANCEL!)		
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 3
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	44
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, RETRYCANCEL!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	45
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, ABORTRETRYIGNORE!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 5
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 7
				END IF
		END CHOOSE	
			lb_retval = FALSE
		END IF					
ELSE
		lb_retval = FALSE
END IF



Return lb_retval
end function

public function integer uf_checkrpcerror_ws (integer ai_returnval, s_error astr_errorinfo, integer ai_commhandle, boolean ab_visual);// To be called after running an RPC.

integer 					li_result, &
							li_commerror, &
							li_file, &
							li_temp, &
							li_upper_bound
				
long 						ll_neterror, &
							ll_primaryerror, &
							ll_secondaryerror
				
string 					ls_neterrmsg, &
							ls_commerrmsg, &
							ls_server_name

s_rpc_error 			lstr_rpc_error_info

u_string_functions	lu_string

window					lw_RPCError



ls_neterrmsg = space(100)
ls_commerrmsg = space(100)

//IF IsValid( luo_wkb32) Then
//	li_result = luo_wkb32.WBGetErrorInfo( li_commerror, ls_commerrmsg, ll_neterror,&
//				 ls_neterrmsg, ll_primaryerror, ll_secondaryerror, ai_CommHandle)
//ELSE				 
//	li_result = luo_wkb16.WBGetErrorInfo( li_commerror, ls_commerrmsg, ll_neterror,&
//				 ls_neterrmsg, ll_primaryerror, ll_secondaryerror, ai_CommHandle)
//END IF
//
//if li_result = 0 then
//	if li_commerror = 0 and ai_ReturnVal >= 0 then
//		return(ai_ReturnVal)
//	else
////		lstr_rpc_error_info.se_app_name = astr_ErrorInfo.se_app_name
//		lstr_rpc_error_info.se_window_name = astr_ErrorInfo.se_window_name
//		lstr_rpc_error_info.se_function_name = astr_ErrorInfo.se_function_name
//		lstr_rpc_error_info.se_event_name = astr_ErrorInfo.se_event_name
//		lstr_rpc_error_info.se_procedure_name = astr_ErrorInfo.se_procedure_name
//		lstr_rpc_error_info.se_user_id = astr_ErrorInfo.se_user_id
//		lstr_rpc_error_info.se_return_code = astr_ErrorInfo.se_return_code
//		lstr_rpc_error_info.se_message = astr_ErrorInfo.se_message
//		if ai_ReturnVal >= 0 then
//			ai_ReturnVal = -25
//		end if
//		lstr_rpc_error_info.se_rval = ai_ReturnVal
//		lstr_rpc_error_info.se_commerror = li_commerror
//		lstr_rpc_error_info.se_commerrmsg = ls_commerrmsg
//		lstr_rpc_error_info.se_neterror = ll_neterror
//		if lstr_rpc_error_info.se_neterror <> 0 then
//			lstr_rpc_error_info.se_neterrmsg = ls_neterrmsg
//		else
//			lstr_rpc_error_info.se_neterrmsg = space(100)
//		end if
//		lstr_rpc_error_info.se_primaryerror = ll_primaryerror
//		lstr_rpc_error_info.se_secondaryerror = ll_secondaryerror
//
//		If Not (li_commerror = 0 And ll_primaryerror = 0 And &
//				  ll_secondaryerror = 0 And ll_neterror = 0) Then
//			// If they all equal zero, don't log
///*			
//			li_upper_bound = UpperBound(istr_commhandles)
//			For li_temp = 1 to li_upper_bound
//				If ai_commhandle = istr_commhandles[li_temp].commhandle Then
//					ls_server_name = istr_commhandles[li_temp].server_name
//					Exit
//				End if
//			Next
//*/
////			If lu_string.nf_IsEmpty(ls_server_name) Then
////				ls_server_name = "Unknown Server Name"
////			End if
////
//			li_file = FileOpen("f:\software\pb\pblog101\pberror.log", &
//									LineMode!, Write!, LockWrite!, Append!)
//			If li_file >  0 Then
//				If LenA(Trim(astr_errorinfo.se_user_id)) < 1 Then
//					astr_errorInfo.se_user_id = SQLCA.UserID
//				End if
//				FileWrite(li_file, String(Today(), "mm-dd-yyyy") + '~t' + &
//							String(Now(), "hh:mm:ss.ff") + '~t' + astr_errorinfo.se_user_id + &
//							'~t' + ls_server_name + '~t' + astr_errorinfo.se_window_name + &
//							'~t' + astr_errorinfo.se_procedure_name + '~t' + &
//							String(li_commerror) + '~t' + String(ll_primaryerror) + '~t' + &
//							String(ll_secondaryerror) + '~t' + String(ll_neterror))
//				FileClose(li_file)
//			End if
//		End if
//		
If ab_visual Then
	openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")
End if
return(ai_ReturnVal)


end function

public function integer uf_rpcupd (string as_input_string, ref s_error astr_error_info, string as_program_name, string as_tran_id, integer ai_version_number);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_temp, ls_target_address, ls_OutputString, ls_output, ls_Stringind

u_String_Functions		lu_string

gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")

otrt01sr_otrt01srservice p_obj

ict_otr000sr_cics_container.otr000sr_req_password = SQLCA.DBPass
ict_otr000sr_cics_container.otr000sr_req_userid =  SQLCA.Userid
ict_otr000sr_cics_container.otr000sr_req_program = as_program_name
ict_otr000sr_cics_container.otr000sr_req_tranid = as_tran_id

ict_otr000sr_program_container_in.otr000sr_rval = 0
ict_otr000sr_program_container_in.otr000sr_message = space(200)
ict_otr000sr_program_container_in.otr000sr_version_number = ai_version_number

ipgif_input.otrt01in.value = as_input_string

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string( SQLCA.Userid)+"~",Password=~""+string(SQLCA.DBPass)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "otrt01sr_otrt01srservice", is_web_service_address)
if ll_ret <> 0 then
	MessageBox("Error", "Cannot create instance of proxy")
	Return ll_ret
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

//as_output_string= ''

//Do
	
	ict_otr000sr_program_container_in.otr000sr_last_record_num = ll_last_record_num
	ict_otr000sr_program_container_in.otr000sr_max_record_num = ll_max_record_num
	ict_otr000sr_program_container_in.otr000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.otrt01sroperation(ipgif_input)
		  catch (SoapException e)
				astr_error_info.se_procedure_name = ''
				astr_error_info.se_return_code = ''
				astr_error_info.se_message = e.Text
				uf_display_message_ws(-1, astr_error_info, 0) 
				
				//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				ls_message = ictif_program_container_out.otr000sr_program_container.otr000sr_message
				
				If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
					astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
					astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
					astr_error_info.se_message = ls_message
				else
					astr_error_info.se_procedure_name = ''
					astr_error_info.se_return_code = ''
					astr_error_info.se_message = ls_message
				end if
				
				uf_display_message_ws(li_rval, astr_error_info, 0) 
				Return -1
	end try
		
	ictif_program_container_out = ipgif_output.otrt01pg 
	li_rval = ictif_program_container_out.otr000sr_program_container.otr000sr_rval
	if li_rval < 0 then
//		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.otr000sr_program_container.otr000sr_message
//		au_ErrorContext.uf_AppendText(ls_message)
//		au_ErrorContext.uf_SetReturnCode(li_rval)		
		
//		ll_last_record_num = ictif_program_container_out.otr000sr_program_container.otr000sr_last_record_num
//		ll_max_record_num = ictif_program_container_out.otr000sr_program_container.otr000sr_max_record_num
		ll_task_number = ictif_program_container_out.otr000sr_program_container.otr000sr_task_num
// strings are coming in with spaces to reserve space for Netwise calls, so clear strings 		
		
				
		
		if isnull(ipgif_output.otrt01ot) then
//			as_header_string = ''
//			as_recv_window_inq_string = ''
		else	
//			as_output_string +=  trim(ipgif_output.otrt01ot.value)
		end if
	end if
//Loop while ll_last_record_num <> ll_max_record_num	

//check for any RPC errors to display messages
ls_message = ictif_program_container_out.otr000sr_program_container.otr000sr_message

If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
	astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_message = ls_message
else
	astr_error_info.se_procedure_name = ''
	astr_error_info.se_return_code = ''
	astr_error_info.se_message = ls_message
end if

uf_display_message_ws(li_rval, astr_error_info, 0) 

Return li_rval

end function

public function integer uf_rpccall (string as_imput_string, ref string as_output_string, ref s_error astr_error_info, string as_program_name, string as_tran_id, integer ai_version_number);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_temp, ls_target_address, ls_OutputString, ls_output, ls_Stringind

u_String_Functions		lu_string

gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")

otrt01sr_otrt01srservice p_obj

ict_otr000sr_cics_container.otr000sr_req_password = SQLCA.DBPass
ict_otr000sr_cics_container.otr000sr_req_userid =  SQLCA.Userid
ict_otr000sr_cics_container.otr000sr_req_program = as_program_name
ict_otr000sr_cics_container.otr000sr_req_tranid = as_tran_id

ict_otr000sr_program_container_in.otr000sr_rval = 0
ict_otr000sr_program_container_in.otr000sr_message = space(200)
ict_otr000sr_program_container_in.otr000sr_version_number = ai_version_number

ipgif_input.otrt01in.value = as_imput_string

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string( SQLCA.Userid)+"~",Password=~""+string(SQLCA.DBPass)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "otrt01sr_otrt01srservice", is_web_service_address)
if ll_ret <> 0 then
	MessageBox("Error", "Cannot create instance of proxy")
	Return ll_ret
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

as_output_string= ''

Do
	
	ict_otr000sr_program_container_in.otr000sr_last_record_num = ll_last_record_num
	ict_otr000sr_program_container_in.otr000sr_max_record_num = ll_max_record_num
	ict_otr000sr_program_container_in.otr000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.otrt01sroperation(ipgif_input)
		  catch (SoapException e)
				//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				astr_error_info.se_procedure_name = ''
				astr_error_info.se_return_code = ''
				astr_error_info.se_message = e.Text
				uf_display_message_ws(-1, astr_error_info, 0) 
				Return -1
	end try
		
	ictif_program_container_out = ipgif_output.otrt01pg 
	li_rval = ictif_program_container_out.otr000sr_program_container.otr000sr_rval
	if li_rval < 0 then
//		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
		//check for any RPC errors to display messages
		ls_message = ictif_program_container_out.otr000sr_program_container.otr000sr_message
		
		If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
			astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
			astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
			astr_error_info.se_message = ls_message
		else
			astr_error_info.se_procedure_name = ''
			astr_error_info.se_return_code = ''
			astr_error_info.se_message = ls_message
		end if
		
		uf_display_message_ws(li_rval, astr_error_info, 0) 
		Return -1
	else
		ls_message = ictif_program_container_out.otr000sr_program_container.otr000sr_message
//		au_ErrorContext.uf_AppendText(ls_message)
//		au_ErrorContext.uf_SetReturnCode(li_rval)		
		
		ll_last_record_num = ictif_program_container_out.otr000sr_program_container.otr000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.otr000sr_program_container.otr000sr_max_record_num
		ll_task_number = ictif_program_container_out.otr000sr_program_container.otr000sr_task_num
// strings are coming in with spaces to reserve space for Netwise calls, so clear strings 		
		
				
		
		if isnull(ipgif_output.otrt01ot) then
//			as_header_string = ''
//			as_recv_window_inq_string = ''
		else	
			as_output_string +=  trim(ipgif_output.otrt01ot.value)
		end if
	end if
Loop while ll_last_record_num <> ll_max_record_num	

//check for any RPC errors to display messages
ls_message = ictif_program_container_out.otr000sr_program_container.otr000sr_message

If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
	astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_message = ls_message
else
	astr_error_info.se_procedure_name = ''
	astr_error_info.se_return_code = ''
	astr_error_info.se_message = ls_message
end if

uf_display_message_ws(li_rval, astr_error_info, 0)  

Return li_rval

end function

on u_webservice.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_webservice.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;uf_initialize()
end event

