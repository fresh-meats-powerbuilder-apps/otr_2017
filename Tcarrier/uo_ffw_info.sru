HA$PBExportHeader$uo_ffw_info.sru
forward
global type uo_ffw_info from nonvisualobject
end type
end forward

global type uo_ffw_info from nonvisualobject autoinstantiate
end type

type variables
String	ffwcode, &
	startdate, &
	ffwname, &
	ffwcity, &
	ffwstate

Boolean	cancel
end variables

on uo_ffw_info.create
TriggerEvent( this, "constructor" )
end on

on uo_ffw_info.destroy
TriggerEvent( this, "destructor" )
end on

