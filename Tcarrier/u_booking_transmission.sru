HA$PBExportHeader$u_booking_transmission.sru
forward
global type u_booking_transmission from nonvisualobject
end type
end forward

global type u_booking_transmission from nonvisualobject autoinstantiate
end type

type variables
String	from_carrier, &
	to_carrier, &
	complex

Date	from_date, &
	to_date

Time	from_time, &
	to_time

Boolean	cancel
end variables

on u_booking_transmission.create
TriggerEvent( this, "constructor" )
end on

on u_booking_transmission.destroy
TriggerEvent( this, "destructor" )
end on

