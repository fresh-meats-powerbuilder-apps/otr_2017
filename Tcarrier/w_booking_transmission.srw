HA$PBExportHeader$w_booking_transmission.srw
forward
global type w_booking_transmission from w_base_sheet_ext
end type
type dw_1 from u_base_dw_ext within w_booking_transmission
end type
end forward

global type w_booking_transmission from w_base_sheet_ext
integer width = 2528
integer height = 1196
string title = "Booking Transmission"
long backcolor = 79741120
dw_1 dw_1
end type
global w_booking_transmission w_booking_transmission

type variables
character   ic_control

string	is_closemessage_txt, &
	is_group_id

integer	ii_rc

u_otr006   iu_otr006

u_booking_transmission	iu_booking_transmission
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_booking_trans_inq ()
end prototypes

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer

OpenWithParm( w_booking_transmission_filter, iu_booking_transmission, iw_frame )
iu_booking_transmission = Message.PowerObjectParm

IF isvalid(iu_booking_transmission) = False Then
	Return True
End If

IF iu_booking_transmission.cancel = True  Then
   Return True
End If
	

// Turn the redraw off for the window while we retrieve
SetPointer(HourGlass!)
This.SetRedraw(FALSE)

// Populate the datawindows
This.wf_booking_trans_inq ()


// Turn the redraw on after the window retrieve
This.SetRedraw(TRUE)
SetPointer(Arrow!)

Return False
end function

public function boolean wf_booking_trans_inq ();integer	li_rtn

s_error	lstr_error_info

string 	ls_from_carrier, &
			ls_to_carrier, &
			ls_complex, &
			ls_transmode, &
			ls_from_date, &
			ls_to_date, &
			ls_from_time, &
			ls_to_time, &
			ls_transmission
			
date		ld_from_date, &
			ld_to_date
			
time		lt_from_time, &
			lt_to_time
			
								 
dw_1.SetRedraw(False)
dw_1.Reset( ) 

ls_from_carrier	= iu_booking_transmission.from_carrier
ls_to_carrier		= iu_booking_transmission.to_carrier
ls_complex			= iu_booking_transmission.complex
ld_from_date		= iu_booking_transmission.from_date
ld_to_date			= iu_booking_transmission.to_date
lt_from_time 		= iu_booking_transmission.from_time
lt_to_time			= iu_booking_transmission.to_time
ls_transmode		= space(1)
ls_from_date		= String(ld_from_date, "mm/dd/yyyy")
ls_to_date			= String(ld_to_date, "mm/dd/yyyy")
ls_from_time		= String(lt_from_time, "hh:mm:ss")
ls_to_time			= String(lt_to_time, "hh:mm:ss")
 
lstr_error_info.se_event_name = "starts in wf_retrieve "
lstr_error_info.se_window_name = "w_booking_transmission "
lstr_error_info.se_procedure_name = "wf_booking_trans_inq"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)


//li_rtn = iu_otr006.nf_otrt75ar(ls_from_carrier, &
//										 ls_to_carrier, &
//										 ls_complex, &
//										 ls_transmode, &
//										 ls_from_date, &
//										 ls_to_date, &
//										 ls_from_time, &
//										 ls_to_time, &
//										 dw_1, &
// 										 lstr_error_info) 


// Check the return code from the above function call
IF li_rtn <> 0 THEN
 	iw_frame.SetMicroHelp( "Invalid Inquiry Attempt" )
   dw_1.InsertRow(0)
	return(False)
ELSE
      iw_frame.SetMicroHelp( "Ready...")
END IF	

This.Title = "Booking Transmission for " + ls_from_date + " to " + ls_to_date 
If ls_from_date = ls_to_date then
   This.Title = "Booking Transmission for " + ls_from_date
End if


											 
dw_1.ResetUpdate()
dw_1.SetRedraw(true)
Return( True )

end function

on w_booking_transmission.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_booking_transmission.destroy
call super::destroy
destroy(this.dw_1)
end on

event close;call super::close;DESTROY iu_otr006
end event

event open;call super::open;dw_1.InsertRow(0)
end event

event activate;call super::activate;iw_frame.im_menu.mf_disable("m_delete")
iw_frame.im_menu.mf_disable("m_save")
iw_frame.im_menu.mf_disable("m_insert")

end event

event ue_postopen;call super::ue_postopen;iu_otr006  =  CREATE u_otr006


IF wf_retrieve() THEN Close( this )
end event

type dw_1 from u_base_dw_ext within w_booking_transmission
integer x = 9
integer y = 12
integer width = 2473
integer height = 1068
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_booking_transmission"
boolean vscrollbar = true
end type

