HA$PBExportHeader$w_ffw_open_close_filter.srw
forward
global type w_ffw_open_close_filter from w_base_response_ext
end type
type dw_1 from u_base_dw_ext within w_ffw_open_close_filter
end type
end forward

global type w_ffw_open_close_filter from w_base_response_ext
integer width = 1691
integer height = 824
string title = "Border Crossing Open/Close Hours"
long backcolor = 79741120
dw_1 dw_1
end type
global w_ffw_open_close_filter w_ffw_open_close_filter

type variables
Integer		ii_rc

uo_ffw_info	iuo_ffw_info

DataWindowChild  idwc_address_code

end variables

on w_ffw_open_close_filter.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_ffw_open_close_filter.destroy
call super::destroy
destroy(this.dw_1)
end on

event open;call super::open;Date		ld_start_date

dw_1.InsertRow(0)

iuo_ffw_info = Message.PowerObjectParm

IF iuo_ffw_info.ffwcode <> '' THEN 
	dw_1.SetItem( 1, "ffwcode", iuo_ffw_info.ffwcode)
	dw_1.SetItem( 1, "ffwname", iuo_ffw_info.ffwname)
	dw_1.SetItem( 1, "ffwcity", iuo_ffw_info.ffwcity)
	dw_1.SetItem( 1, "ffwstate", iuo_ffw_info.ffwstate)
END IF

//IF iuo_ffw_info.startdate <> '' THEN 
//	dw_1.SetItem( 1, "startdate", iuo_ffw_info.startdate)
//END IF

//ld_to_date = date(this.gettext())

dw_1.setitem(1,"startdate",today())
//dw_1.SetItem( 1, "ffwname", iuo_ffw_info.ffwname)
//dw_1.SetItem( 1, "ffwcity", iuo_ffw_info.ffwcity)
//dw_1.SetItem( 1, "ffwstate", iuo_ffw_info.ffwstate)
end event

event ue_base_cancel;iuo_ffw_info.cancel = TRUE

CloseWithReturn( This, iuo_ffw_info )

Return
 
end event

event ue_base_ok;Boolean   lb_parameter_ind

string    ls_ffw_code, &
	       ls_work_start_date, &
			 ls_ffw_name, &
			 ls_ffw_city, &
			 ls_ffw_state
			 
Date 		 ld_start_date

//string ls_str
//
//long ll_cur_row

lb_parameter_ind = FALSE

//ll_cur_row = idwc_address_code.Find ( "address_code='"+ &
//		             data+"'", 1, idwc_address_code.RowCount() )

//ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_name")
//dw_1.setitem(1, "ffwname", ls_str)
//ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_city")
//dw_1.setitem(1, "ffwcity", ls_str)
//ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_state")
//dw_1.setitem(1, "ffwstate", ls_str)

If dw_1.AcceptText() = 1 Then
   
	ls_ffw_code	       = dw_1.GetItemString(1, "ffwcode")
	ld_start_date      = dw_1.GetItemDate(1, "startdate")
   ls_work_start_date = String(ld_start_date,"yyyy-mm-dd")
	ls_ffw_name        = dw_1.GetItemString(1, "ffwname")
	ls_ffw_city        = dw_1.GetItemString(1, "ffwcity")
	ls_ffw_state       = dw_1.GetItemString(1, "ffwstate")
   

   IF ls_ffw_code > " " and not IsNull(ls_ffw_code) Then
   	lb_parameter_ind = TRUE
   Else
      dw_1.SetFocus()
      dw_1.SetColumn("ffw_code")
      dw_1.SelectText(1, 10)
      MessageBox("Invalid Parameters", "Please enter a FFW Code")
      Return
   End if
     
   iuo_ffw_info.ffwcode       = ls_ffw_code
   iuo_ffw_info.startdate   	= ls_work_start_date
	iuo_ffw_info.ffwname    	= ls_ffw_name
	iuo_ffw_info.ffwcity    	= ls_ffw_city
	iuo_ffw_info.ffwstate    	= ls_ffw_state
   iuo_ffw_info.cancel        = FALSE

   CloseWithReturn( This, iuo_ffw_info )

End if



end event

type cb_base_help from w_base_response_ext`cb_base_help within w_ffw_open_close_filter
integer x = 1010
integer y = 608
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_ffw_open_close_filter
integer x = 713
integer y = 608
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_ffw_open_close_filter
integer x = 416
integer y = 608
integer taborder = 20
end type

type dw_1 from u_base_dw_ext within w_ffw_open_close_filter
integer x = 41
integer y = 40
integer width = 1595
integer height = 500
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_ffw_start"
boolean border = false
end type

event itemchanged;call super::itemchanged;THIS.SelectText (1,15)

string ls_str

long ll_cur_row
If is_ColumnName = "ffwcode" Then
	
   IF Trim(data) <> "" THEN 
      ll_cur_row = idwc_address_code.Find ( "address_code='"+ &
		             data+"'", 1, idwc_address_code.RowCount() )
						 
      IF ll_cur_row < 1 THEN
         MessageBox( "Freight Forwarder Error", &
			            data + " is not a Valid Freight Forwarder code." )
      	Return 1
      else
	       ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_name")
	       this.setitem(1, "ffwname", ls_str)
	       ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_city")
	       this.setitem(1, "ffwcity", ls_str)
	       ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_state")
	       this.setitem(1, "ffwstate", ls_str)
      End if
   Else 
		 this.setitem(1, "ffwname", " ")
		 this.setitem(1, "ffwcity", " ")
		 this.setitem(1, "ffwstate", " ")
	End if		
End if
end event

event itemerror;call super::itemerror;Return 1
end event

event constructor;call super::constructor;ii_rc = GetChild( 'ffwcode', idwc_address_code )
IF ii_rc = -1 THEN MessageBox("DataWindow Child Error", "Unable to get Child Handle for address code ")

idwc_address_code.SetTrans(SQLCA)
idwc_address_code.Retrieve("FRTFORWD")
end event

