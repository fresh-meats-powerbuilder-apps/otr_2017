HA$PBExportHeader$w_carrier_data_filter.srw
forward
global type w_carrier_data_filter from w_base_response_ext
end type
type dw_carrier from u_base_dw_ext within w_carrier_data_filter
end type
end forward

global type w_carrier_data_filter from w_base_response_ext
integer width = 773
integer height = 492
string title = "Carrier Data Filter"
long backcolor = 79741120
dw_carrier dw_carrier
end type
global w_carrier_data_filter w_carrier_data_filter

type variables
integer		ii_rc

DataWindowChild   idwc_carrier 

u_carrier		iu_carrier
end variables

on w_carrier_data_filter.create
int iCurrent
call super::create
this.dw_carrier=create dw_carrier
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_carrier
end on

on w_carrier_data_filter.destroy
call super::destroy
destroy(this.dw_carrier)
end on

event open;call super::open;// load any info into the correct fields
string	ls_carrier
			
int		li_rtn,&
			li_pos, &
			li_string_len

long		ll_current_row

iu_carrier = Message.PowerObjectParm

dw_carrier.SetFocus()
dw_carrier.SetColumn("carrier_code")
dw_carrier.SelectText(1, 4)

dw_carrier.SetItem( 1, "carrier_code", iu_carrier.carrier )
dw_carrier.SelectText(1, 4)


end event

event ue_base_cancel;iu_carrier.cancel = true
CloseWithReturn( this, iu_carrier )

end event

event ue_base_ok;string	ls_carrier

If dw_carrier.AcceptText() = 1 Then
	ls_carrier	= " "
   ls_carrier	= trim( dw_carrier.object.carrier_code[1])
Else
	ls_carrier = " "
End If
	
	
iu_carrier.carrier	     = ls_carrier
iu_carrier.cancel		     = false

Message.PowerObjectParm = iu_carrier

CloseWithReturn( This, iu_carrier )
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_carrier_data_filter
integer x = 462
integer y = 280
integer taborder = 10
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_carrier_data_filter
integer x = 462
integer y = 156
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_carrier_data_filter
integer x = 462
integer y = 32
integer taborder = 20
end type

type dw_carrier from u_base_dw_ext within w_carrier_data_filter
integer x = 23
integer y = 80
integer width = 407
integer height = 272
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_dddw_carriers"
end type

event itemfocuschanged;call super::itemfocuschanged;integer	li_len
string	ls_temp

ls_temp = this.GetText()

li_len = len(ls_temp)

this.SelectText(1, li_len)
end event

event constructor;call super::constructor;InsertRow(0)

ii_rc = GetChild( 'carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()


end event

event itemchanged;call super::itemchanged;long ll_FoundRow

IF Trim(data) = "" THEN Return

IF dwo.name = "carrier_code" Then
   ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
   IF ll_FoundRow < 1 THEN
	  MessageBox( "Carrier Error", data + " is Not a Valid Carrier Code." )
	  dw_carrier.SetItem(1,"carrier_code", "")
	  dw_carrier.SetFocus()  
     dw_carrier.SetColumn("carrier_code")
     Return 1
   END IF		
End if

end event

event itemerror;call super::itemerror;Return 1
end event

