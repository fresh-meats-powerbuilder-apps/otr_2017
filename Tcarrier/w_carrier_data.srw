HA$PBExportHeader$w_carrier_data.srw
forward
global type w_carrier_data from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_carrier_data
end type
type dw_data from u_base_dw_ext within w_carrier_data
end type
end forward

global type w_carrier_data from w_base_sheet_ext
integer width = 2359
integer height = 1580
long backcolor = 79741120
dw_header dw_header
dw_data dw_data
end type
global w_carrier_data w_carrier_data

type variables
character   ic_control

long       il_RowCount, &
              il_Row

string	is_closemessage_txt 
                 
integer	ii_rc, &
                ii_rowcount, &
                ii_ind = 0

date         id_date

u_otr006 		iu_otr006

u_carrier	iu_carrier

u_ws_edi iu_ws_edi
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_carrier_inq ()
end prototypes

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer

 
//li_rc = wf_Changed()

CHOOSE CASE li_rc	//begin 1
	CASE  1	//	a valid change occurred ...
		li_answer = MessageBox("Wait",is_closemessage_txt,question!,yesnocancel!)
		CHOOSE CASE li_answer
			CASE 1	//	yes, save changes
				IF wf_Update() THEN	//	the save succeeded
				ELSE	//	the save failed
					li_answer = MessageBox("Save Failed","Retrieve Anyway?",question!,yesno!)
					IF li_answer = 1 THEN	//	retrieve w/o saving changes
					ELSE	//	cancel the close
						Return( false )
					END IF
				END IF	
			CASE 2 	//	retrieve w/o saving changes
			CASE 3	//	cancel the retrieve
				Return(false)
		END CHOOSE
	CASE  -1 		//	a validation error occurred
		li_answer = MessageBox("Data Entry Error","Retrieve w/o Save?",question!,okcancel!)
		CHOOSE CASE li_answer
			CASE 1	//	retrieve w/o saving changes
			CASE 2	//	cancel the retrieve
				Return( false )
		END CHOOSE
	CASE  -2 	//	a serious validation error, window may not close in this state
		MessageBox("A Serious Error Has Occurred","Correct Error Before Retrieving!",exclamation!,ok!)
		Return(False)
	CASE ELSE	//	nothing has changed
END CHOOSE


OpenWithParm( w_carrier_data_filter, iu_carrier, iw_frame )
iu_carrier = Message.PowerObjectParm

IF isvalid(iu_carrier) = False Then
	Return True
End If

IF iu_carrier.cancel = True  Then
   Return True
End If
	

// Turn the redraw off for the window while we retrieve
SetPointer(HourGlass!)
This.SetRedraw(FALSE)

// Populate the datawindows
This.wf_carrier_inq ()
//li_rc = dw_data.InsertRow(0)
dw_data.setrow(li_rc)

//dw_3.SetItemStatus ( 1, 0, Primary!, NotModified! )



// Turn the redraw off for the window while we retrieve
This.SetRedraw(TRUE)
SetPointer(Arrow!)

Return False
end function

public function boolean wf_carrier_inq ();boolean	lb_rtn

s_error	lstr_error_info

string 	ls_carrier
								 
dw_header.SetRedraw(False)
dw_header.Reset( ) 
dw_data.SetRedraw(False)
dw_data.Reset( ) 

ls_carrier =	iu_carrier.carrier
 
lstr_error_info.se_event_name = "starts in wf_retrieve "
lstr_error_info.se_window_name = "w_carrier_data "
lstr_error_info.se_procedure_name = "wf_carrier_inq"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)


//lb_rtn = iu_otr006.nf_otrt72ar(ls_carrier, &				
// 										 dw_header, &
//										 dw_data, & 
// 										 lstr_error_info) 
											 

lb_rtn = iu_ws_edi.nf_otrt72er(ls_carrier, &				
 										 dw_header, &
										 dw_data, & 
 										 lstr_error_info) 
											 											 
											 
											 
This.Title = "Carrier Data for " + ls_carrier
dw_header.ResetUpdate()
dw_header.SetRedraw(true)
dw_data.ResetUpdate()
dw_data.SetRedraw(true)
Return( True )

end function

on w_carrier_data.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_data=create dw_data
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_data
end on

on w_carrier_data.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_data)
end on

event close;call super::close;Destroy	u_otr006
DESTROY iu_ws_edi
end event

event open;call super::open;dw_header.InsertRow(0)
//dw_data.InsertRow(0)
end event

event ue_postopen;call super::ue_postopen;iu_otr006  =  CREATE u_otr006
iu_ws_edi = CREATE u_ws_edi


IF wf_retrieve() THEN Close( this )
end event

event activate;call super::activate;iw_frame.im_menu.mf_disable("m_delete")
iw_frame.im_menu.mf_disable("m_save")
iw_frame.im_menu.mf_disable("m_insert")
iw_frame.im_menu.mf_disable("m_print")
end event

type dw_header from u_base_dw_ext within w_carrier_data
integer x = 18
integer y = 24
integer width = 2286
integer height = 844
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_carrier_data_header"
end type

type dw_data from u_base_dw_ext within w_carrier_data
integer x = 18
integer y = 876
integer width = 2286
integer height = 596
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_carrier_data_edi_set"
boolean vscrollbar = true
end type

