HA$PBExportHeader$w_edi_214_tran_detail.srw
forward
global type w_edi_214_tran_detail from w_base_sheet_ext
end type
type dw_1 from u_base_dw_ext within w_edi_214_tran_detail
end type
end forward

global type w_edi_214_tran_detail from w_base_sheet_ext
integer width = 2930
integer height = 1156
string title = "EDI 214 Transaction Details"
long backcolor = 79741120
dw_1 dw_1
end type
global w_edi_214_tran_detail w_edi_214_tran_detail

type variables
u_otr006		iu_otr006

string		is_closemessage_txt

u_214_details	iu_214_details
end variables

forward prototypes
public function boolean wf_retrieve ()
public function integer wf_edi_214_detail_inq ()
end prototypes

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer

// 
//li_rc = wf_Changed()
//
//CHOOSE CASE li_rc	//begin 1
//	CASE  1	//	a valid change occurred ...
//		li_answer = MessageBox("Wait",is_closemessage_txt,question!,yesnocancel!)
//		CHOOSE CASE li_answer
//			CASE 1	//	yes, save changes
//				IF wf_Update() THEN	//	the save succeeded
//				ELSE	//	the save failed
//					li_answer = MessageBox("Save Failed","Retrieve Anyway?",question!,yesno!)
//					IF li_answer = 1 THEN	//	retrieve w/o saving changes
//					ELSE	//	cancel the close
//						Return( false )
//					END IF
//				END IF	
//			CASE 2 	//	retrieve w/o saving changes
//			CASE 3	//	cancel the retrieve
//				Return(false)
//		END CHOOSE
//	CASE  -1 		//	a validation error occurred
//		li_answer = MessageBox("Data Entry Error","Retrieve w/o Save?",question!,okcancel!)
//		CHOOSE CASE li_answer
//			CASE 1	//	retrieve w/o saving changes
//			CASE 2	//	cancel the retrieve
//				Return( false )
//		END CHOOSE
//	CASE  -2 	//	a serious validation error, window may not close in this state
//		MessageBox("A Serious Error Has Occurred","Correct Error Before Retrieving!",exclamation!,ok!)
//		Return(False)
//	CASE ELSE	//	nothing has changed
//END CHOOSE
//

OpenWithParm( w_edi_214_tran_detail_filter, iu_214_details, iw_frame )
iu_214_details = Message.PowerObjectParm

If isvalid(iu_214_details) = False then
	Return True
End If

IF iu_214_details.cancel = True  Then
     Return True
End If

// Turn the redraw off for the window while we retrieve
This.SetRedraw(FALSE)

// Populate the datawindows
This.wf_edi_214_detail_inq ()

// Turn the redraw off for the window while we retrieve
This.SetRedraw(TRUE)

Return FALSE




end function

public function integer wf_edi_214_detail_inq ();integer			li_rtn

string         ls_from_carrier, &
               ls_to_carrier,  &
               ls_from_date, &
               ls_to_date, &
               ls_from_time, &
               ls_to_time
               

s_error        lstr_Error_Info


datawindowchild lddwc_edilater


dw_1.getchild("reason_code",lddwc_edilater)
lddwc_edilater.settrans(sqlca)
lddwc_edilater.retrieve("EDILATER")


dw_1.Reset()
SetPointer( HourGlass! )

ls_from_carrier    = iu_214_details.from_carrier
ls_to_carrier      = iu_214_details.to_carrier
ls_from_date       = iu_214_details.from_date 
ls_to_date         = iu_214_details.to_date
ls_from_time       = iu_214_details.from_time
ls_to_time         = iu_214_details.to_time 

lstr_error_info.se_event_name = "wf_edi_214_detail_inq"
lstr_error_info.se_window_name = "w_edi_214_tran_detail"
lstr_error_info.se_procedure_name = "wf_edi_214_detail_inq"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)


//li_rtn = iu_otr006.nf_otrt71ar(ls_from_carrier, ls_to_carrier, &
//         ls_from_date, ls_to_date, ls_from_time, ls_to_time, &
//         dw_1, lstr_error_info)


// Check the return code from the above function call
IF li_rtn <> 0 THEN
 	iw_frame.SetMicroHelp( "Invalid Inquiry Attempt" )
   dw_1.InsertRow(0)
	return(-1)
ELSE
      iw_frame.SetMicroHelp( "Ready...")
END IF										  
		
This.Title = "EDI 214 Transaction Details for "+ls_from_carrier + " TO " + ls_to_carrier 
If ls_to_carrier = ls_from_carrier then
   This.Title = "EDI 214 Transaction Details for "+ls_from_carrier 
End if


dw_1.ResetUpdate()
dw_1.SetRedraw(true)
Return( 1 )









//// Check the return code from the above function call
//IF li_rtn <> 0 THEN
//    return(-1)
//ELSE
//    iw_frame.SetMicroHelp( "Ready...")
//END IF
//
//li_rtn = dw_1.ImportString(Trim(ls_edi_string))
//If li_rtn < 1 Then iw_frame.SetMicroHelp("No Records Found")
//
//DO WHILE Trim(ls_refetch_carrier) > ""
//
//	ls_mass_conf_inq_string	= Space(4851)
// 
//  	li_rtn = iu_otr004.nf_otrt47ar(ls_req_start_carrier_code, &
//         ls_req_end_carrier_code, ls_req_delv_date, ls_mass_conf_inq_string, &
//         ls_refetch_carrier_code, ls_refetch_load_key, ls_refetch_stop_code, &
//         lstr_error_info, 0)
//
//    // Check the return code from the above function call
//    IF li_rtn <> 0 THEN
// 	     return(-1)
//    ELSE
//        iw_frame.SetMicroHelp( "Ready...")
//    END IF
//
//    li_rtn = dw_1.ImportString(Trim(ls_mass_conf_inq_string))
// 
//LOOP
// 
//ls_work_date = ls_req_delv_date
//ls_work_yyyy = Left(ls_work_date, 4)
//ls_work_mm   = Mid(ls_work_date, 5, 2)
//ls_work_dd   = Mid(ls_work_date, 7, 2)
//ls_work_date = ls_work_mm + "/" + ls_work_dd + "/" + ls_work_yyyy     
//
//This.Title = "Unconfirmed Deliveries for "+ls_req_start_carrier_code + " - " + ls_req_end_carrier_code  + ", " + ls_work_date 
//
//dw_1.Sort()
//dw_1.SetFocus()
//dw_1.ScrollToRow(1)
//dw_1.SetColumn("confirmation_ind")
//dw_1.ResetUpdate()
//		
//Return(1)      


      
end function

on w_edi_214_tran_detail.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_edi_214_tran_detail.destroy
call super::destroy
destroy(this.dw_1)
end on

event close;call super::close;DESTROY iu_otr006
end event

event open;call super::open;dw_1.InsertRow(0) 
end event

event ue_postopen;call super::ue_postopen;iu_otr006  =  CREATE u_otr006

IF wf_retrieve() THEN Close( this )

end event

event activate;call super::activate;iw_frame.im_menu.mf_disable("m_delete")
iw_frame.im_menu.mf_disable("m_save")
iw_frame.im_menu.mf_disable("m_insert")

end event

event ue_fileprint;call super::ue_fileprint;dw_1.Print( )

end event

type dw_1 from u_base_dw_ext within w_edi_214_tran_detail
integer x = 5
integer width = 2871
integer height = 1032
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_edi_214_tran_detail"
boolean vscrollbar = true
end type

event ue_retrieve;//datawindowchild lddwc_edilater
//
//boolean  lb_rtn
//
//
//
//
//This.getchild("reason_code",lddwc_edilater)
//lddwc_edilater.settransobject(sqlca)
//lddwc_edilater.retrieve("EDILATER")
//
//
//This.ResetUpdate()
//
//// Turn the redraw on for the window after we retrieve
//This.SetRedraw(TRUE)
//
//SetPointer( arrow! )
//
//Return 1
//
//
//
end event

