HA$PBExportHeader$w_ffw_open_close.srw
forward
global type w_ffw_open_close from w_base_sheet_ext
end type
type dw_1 from u_base_dw_ext within w_ffw_open_close
end type
type dw_2 from u_base_dw_ext within w_ffw_open_close
end type
type dw_3 from u_base_dw_ext within w_ffw_open_close
end type
end forward

global type w_ffw_open_close from w_base_sheet_ext
integer x = 5
integer y = 12
integer width = 2802
integer height = 1552
string title = "Border Crossing  Open/Close Hours"
long backcolor = 79741120
dw_1 dw_1
dw_2 dw_2
dw_3 dw_3
end type
global w_ffw_open_close w_ffw_open_close

type variables
character   ic_control

long       il_RowCount, &
             il_Row

string	is_closemessage_txt, &
                is_start_date, &
                is_ffw_code 

integer	ii_rc, &
                ii_rowcount, &
                ii_ind = 0

date         id_date

 

uo_ffw_info	iuo_ffw_info

u_otr005   iu_otr005

u_ws_traffic		iu_ws_traffic

end variables

forward prototypes
public function boolean wf_retrieve ()
public function integer wf_changed ()
public function boolean wf_ffw_info_inq ()
public function boolean wf_update ()
public subroutine wf_delete ()
public function boolean wf_pre_save ()
end prototypes

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer

 
li_rc = wf_Changed()

CHOOSE CASE li_rc	//begin 1
	CASE  1	//	a valid change occurred ...
		li_answer = MessageBox("Wait",is_closemessage_txt,question!,yesnocancel!)
		CHOOSE CASE li_answer
			CASE 1	//	yes, save changes
				IF wf_Update() THEN	//	the save succeeded
				ELSE	//	the save failed
					li_answer = MessageBox("Save Failed","Retrieve Anyway?",question!,yesno!)
					IF li_answer = 1 THEN	//	retrieve w/o saving changes
					ELSE	//	cancel the close
						Return( false )
					END IF
				END IF	
			CASE 2 	//	retrieve w/o saving changes
			CASE 3	//	cancel the retrieve
				Return(false)
		END CHOOSE
	CASE  -1 		//	a validation error occurred
		li_answer = MessageBox("Data Entry Error","Retrieve w/o Save?",question!,okcancel!)
		CHOOSE CASE li_answer
			CASE 1	//	retrieve w/o saving changes
			CASE 2	//	cancel the retrieve
				Return( false )
		END CHOOSE
	CASE  -2 	//	a serious validation error, window may not close in this state
		MessageBox("A Serious Error Has Occurred","Correct Error Before Retrieving!",exclamation!,ok!)
		Return(False)
	CASE ELSE	//	nothing has changed
END CHOOSE



//IF Trim(is_ffw_code) = ""  or IsNull(is_ffw_code) Then
	
	OpenWithParm( w_ffw_open_close_filter, iuo_ffw_info, iw_frame )
	iuo_ffw_info = Message.PowerObjectParm
	
	If isvalid(iuo_ffw_info) = False then
		Return True
	End If
	
	IF iuo_ffw_info.cancel = True  Then
     Return True
	End If
	
//   iuo_ffw_info.ffwcode = is_ffw_code 
//End If
//
// Turn the redraw off for the window while we retrieve
SetPointer(HourGlass!)
This.SetRedraw(FALSE)

// Populate the datawindows
This.wf_ffw_info_inq ()
li_rc = dw_2.InsertRow(0)
dw_2.setrow(li_rc)

dw_3.SetItemStatus ( 1, 0, Primary!, NotModified! )


This.Title = "Border Crossing  Open/Close Hours" + " - " + iuo_ffw_info.ffwcode

// Turn the redraw off for the window while we retrieve
This.SetRedraw(TRUE)
SetPointer(Arrow!)

Return False
end function

public function integer wf_changed ();/*
	purpose:	this function will examine all datawindow controls on the window
				that have update capability against the database.  if anything has changed,
				and the change is valid, a 1 is returned.  if an invalid change has occurred
				then a -1 is returned.  if nothing has changed a 0 is returned.

*/

int i, totcontrols, rc, counter
datawindow dw[]

totcontrols = UpperBound(this.control)

FOR i = 1 to totcontrols
	IF TypeOf(this.control[i]) = datawindow! THEN
		counter ++
		dw[counter] = this.control[i]
			rc = dw[counter].AcceptText()
			IF rc = -1 THEN 
				RETURN -1
			ELSE
				CONTINUE
			END IF
			counter --
	END IF
NEXT

FOR i = 1 to counter
	IF dw[i].ModifiedCount() > 0 THEN
		is_closemessage_txt = "Save Changes?"
		RETURN 1
	END IF
NEXT

RETURN 0
	
end function

public function boolean wf_ffw_info_inq ();boolean lb_rtn

S_error lstr_error_info

string ls_ffw_code, &
	    ls_inq_date
		 
dw_1.SetRedraw(False)

dw_1.Reset( ) 
dw_2.Reset( ) 


ls_ffw_code = iuo_ffw_info.ffwcode
ls_inq_date = iuo_ffw_info.startdate
 
lstr_error_info.se_event_name = "starts in ue_postopen"
lstr_error_info.se_window_name = "w_ffw_open_close"
lstr_error_info.se_procedure_name = "wf_ffw_info_inq"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

/*
lb_rtn = iu_otr005.nf_otrt64ar(ls_ffw_code, &
 										 ls_inq_date, & 
 										 dw_1, & 
  										 dw_2, & 
 										 lstr_error_info) 
*/											 

lb_rtn = iu_ws_traffic.nf_otrt64er(ls_ffw_code, &
 										 ls_inq_date, & 
 										 dw_1, & 
  										 dw_2, & 
 										 lstr_error_info) 

dw_3.object.address_code[1]  = iuo_ffw_info.ffwcode
dw_3.object.address_name[1]  = iuo_ffw_info.ffwname
dw_3.object.address_city[1]  = iuo_ffw_info.ffwcity
dw_3.object.address_state[1] = iuo_ffw_info.ffwstate

This.dw_1.ResetUpdate()
This.dw_2.ResetUpdate()
//// Code by JK
//ii_rowcount = dw_2.rowcount()
//
dw_1.SetRedraw(true)
Return( True )

end function

public function boolean wf_update ();// Returns FALSE immediately if wf_pre_save returns FALSE (i.e., there
// is invalid data on the window.

Boolean    lb_ret

integer    li_rtn, &
           li_rc

long		  ll_Row,&
		     ll_RowCount	

String     ls_hours_string, &
           ls_ffw_code, &
			  ls_closed_date_string, &
			  ls_action_ind
			  
Date		  ld_closed_date

s_error    lstr_Error_Info

//dwItemStatus		status

IF dw_1.AcceptText() = -1 and dw_2.AcceptText() = -1 THEN 
     Return(False)
END IF

lstr_error_info.se_event_name = "wf_update"
lstr_error_info.se_window_name = "w_ffw_open_close"
lstr_error_info.se_procedure_name = "wf_update"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)


lb_ret = wf_pre_save()

If lb_ret = FALSE Then
   Return False
End If


If dw_1.AcceptText() = 1 Then 
   ll_Row = dw_1.GetNextModified( ll_Row, Primary! )
	IF ll_Row > 0  THEN 
		ls_hours_string = dw_1.Object.DataWindow.Data
		ls_ffw_code     = iuo_ffw_info.ffwcode

		//lb_ret = iu_otr005.nf_otrt65ar(ls_ffw_code, &
 			//								    ls_hours_string, & 
 				//							 	 lstr_error_info) 
	
		lb_ret = iu_ws_traffic.nf_otrt65er(ls_ffw_code, &
 											    ls_hours_string, & 
 											 	 lstr_error_info) 
											 
//		dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
//		dw_1.ResetUpdate()
		CHOOSE CASE lb_ret
			CASE False 
				dw_1.Setredraw(TRUE)
				li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
										ls_ffw_code + " has failed due to an Error.  Do you want to Continue?", &
										Question!, YesNo! )
				dw_1.Setredraw(FALSE)
			CASE Else
				li_rc = 0
				dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
				dw_1.ResetUpdate()
		END CHOOSE

		IF li_rc = 2 THEN 			// NO  do not continue processing
			dw_1.Setredraw(TRUE)
			Return(False)
		END IF
	else
		dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
	END IF
END IF


If dw_2.AcceptText() = 1 Then
	ll_RowCount = dw_2.RowCount()
	ll_Row = dw_2.GetNextModified( ll_Row, Primary! )
	DO WHILE ll_Row > 0 
		IF ll_Row > 0  THEN 

			ls_ffw_code           = iuo_ffw_info.ffwcode
			ld_closed_date	       = dw_2.GetItemDate( ll_row, "closed_date" )
			ls_closed_date_string = String(ld_closed_date, "yyyy-mm-dd")
			ls_action_ind         = "A"
			
			//lb_ret = iu_otr005.nf_otrt66ar(ls_ffw_code, &
 				//							  		 ls_action_ind, &
					//								 ls_closed_date_string, &
 					//						 		 lstr_error_info) 
											 
			lb_ret = iu_ws_traffic.nf_otrt66er(ls_ffw_code, &
 											  		 ls_action_ind, &
													 ls_closed_date_string, &
 											 		 lstr_error_info) 	
 
//			dw_2.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
			CHOOSE CASE lb_ret
				CASE False
					dw_2.Setredraw(TRUE)
					li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
											ls_ffw_code + " has failed due to an Error.  Do you want to Continue?", &
											Question!, YesNo! )
					dw_2.Setredraw(FALSE)
				CASE Else 
					li_rc = 0
					dw_2.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
//					dw_2.ResetUpdate()
			END CHOOSE

			IF li_rc = 2 THEN 			// NO  do not continue processing
				dw_2.Setredraw(TRUE)
				Return(False)
			END IF
		ELSE
			dw_2.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
		END IF
		ll_Row = dw_2.GetNextModified( ll_Row, Primary! )	
   LOOP	  
END IF	

dw_1.setredraw(true)
dw_2.SetRedraw(True)

Return(True)
end function

public subroutine wf_delete ();Date       ld_closed_date

integer    li_rtn, &
			  li_rc, &
           li_delete_count, &
           li_msg

long		  ll_Row,&
           ll_del_row, &
			  ll_RowCount

String     ls_closed_date_string, &
			  ls_ffw_code, &
			  ls_action_ind
                   
Boolean	  ib_any_selected_rows, &
			  lb_ret

dwItemStatus		status

s_error    lstr_Error_Info


lstr_error_info.se_event_name = "wf_delete"
lstr_error_info.se_window_name = "w_ffw_open_close"
lstr_error_info.se_procedure_name = "wf_delete w_ffw_open_close"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

/*   P10-delete-prompt
        This paragraph will prompt the # of rows to delete with a continue
        yes no box.
*/

//dw_1.SetRedraw(False)
//dw_1.SetFilter("IsSelected()")
//dw_1.Filter()
//li_delete_count = dw_1.rowcount()
//dw_1.SetFilter("")
//dw_1.Filter()
//dw_1.SetRedraw(True)

ll_RowCount = dw_2.RowCount()
ib_any_selected_rows = false
If ll_RowCount > 0 Then
  FOR ll_Row = 1  TO ll_RowCount
    If dw_2.IsSelected(ll_Row) = True Then
		ib_any_selected_rows = True
	 END IF
  NEXT
END IF

// If no rows are selected - get out
IF NOT ib_any_selected_rows Then Return

li_msg = MessageBox("Delete Verification", &
         "Do you want to delete selected rows?", &
         Question!, YesNo!, 1)

dw_2.SetRedraw(False)

//    P10-delete-prompt-end
IF li_msg = 2 then
	dw_1.SetRedraw(True)
	iw_frame.SetMicroHelp("No Rows Deleted")
	Return
End If



If ll_RowCount > 0 Then
  FOR ll_Row = 1  TO ll_RowCount
    If dw_2.IsSelected(ll_Row) = True Then
		
		 ls_ffw_code           = iuo_ffw_info.ffwcode
	    ld_closed_date        = dw_2.GetItemDate(ll_row, "closed_date" )
		 ls_closed_date_string = String(ld_closed_date, "yyyy-mm-dd") 
		 ls_action_ind         = "D"
		 
//		 lb_ret = iu_otr005.nf_otrt66ar(ls_ffw_code, &
// 										  		  ls_action_ind, &
//												  ls_closed_date_string, &
// 											 	  lstr_error_info) 
													 
		lb_ret = iu_ws_traffic.nf_otrt66er(ls_ffw_code, &
 											  		 ls_action_ind, &
													 ls_closed_date_string, &
 											 		 lstr_error_info) 	
 											 
		 CHOOSE CASE lb_ret
		   CASE False
             dw_2.SetRow(ll_row)
             EXIT
         CASE ELSE
 			  	ll_del_row = ll_row
 				dw_2.DeleteRow( ll_del_row )
				
				ll_Row --
			 	ll_RowCount --
				li_rc = 0
				
		 END CHOOSE
    END IF
  Next
END IF
 
dw_2.ResetUpdate()
dw_2.setredraw(true)
iw_frame.SetMicroHelp( "Delete Processing Completed") 
Return      


end subroutine

public function boolean wf_pre_save ();long	ll_dw1_rows, &
		ll_dw2_rows, &
		ll_rowcount, &
		ll_row, &
		ll_rownumber
		
time	lt_from_hours, &
		lt_to_hours

ll_dw1_rows = dw_1.ModifiedCount()
ll_dw2_rows = dw_2.ModifiedCount()

ll_rowcount  = dw_1.RowCount()
ll_row       = 0
ll_rownumber = 0

If ll_dw1_rows > 0 then
  Do While ll_rownumber <= ll_rowcount
	
	ll_row = dw_1.GetNextModified(ll_rownumber, Primary!)
	If ll_row > 0 Then
	
		lt_from_hours = dw_1.GetItemTime(ll_row, "from_time")
		lt_to_hours   = dw_1.GetItemTime(ll_row, "to_time")
	
   	If lt_to_hours  <  lt_from_hours Then
     		MessageBox("Hours Error", "To hours must be greater than the From hours.") 
			dw_1.SetRow(ll_row)
			dw_1.SetColumn("to_time")
     		Return FALSE        
  		End if
		  
	else
		ll_rownumber = ll_rowcount + 1
	End if
	
  ll_rownumber++	
  Loop
End if


If ll_dw1_rows > 0 or ll_dw2_rows > 0 then
	Return True
Else
	gw_netwise_frame.SetMicroHelp("No changes to Save....Ready")
	Return False
End If
//Return True
end function

on w_ffw_open_close.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.dw_2=create dw_2
this.dw_3=create dw_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.dw_2
this.Control[iCurrent+3]=this.dw_3
end on

on w_ffw_open_close.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.dw_2)
destroy(this.dw_3)
end on

event ue_postopen;call super::ue_postopen;iu_otr005  =  CREATE u_otr005

iu_ws_traffic = CREATE u_ws_traffic


IF wf_retrieve() THEN Close( this )
end event

event close;call super::close;Destroy u_otr005

Destroy u_ws_traffic
end event

event open;call super::open;//dw_1.InsertRow(0)

//dw_3.modify("DataWindow.Color = '79741120'")

end event

type dw_1 from u_base_dw_ext within w_ffw_open_close
integer x = 55
integer y = 316
integer width = 2670
integer height = 464
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_ffw_hours"
end type

type dw_2 from u_base_dw_ext within w_ffw_open_close
integer x = 55
integer y = 804
integer width = 2670
integer height = 624
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_ffw_closed"
end type

event doubleclicked;call super::doubleclicked;//integer li_rowcount, li_getclickedrow
//
//li_rowcount = dw_2.RowCount()
//li_getclickedrow   = dw_2.GetClickedRow()
//
//if li_rowcount = li_getclickedrow then
//	dw_2.insertrow(0)
//	return
//end if

//dwitemstatus ldwis_status
//ldwis_status = dw_2.GetItemStatus(li_getclickedrow, "closed_date", Primary!)
//if li_rowcount = li_getclickedrow and ldwis_status = DataModified! then
//else
//	if dw_2.getrow() <= ii_rowcount then
//		messagebox("Message","This cannot be modified")
//		dw_2.settext(string(id_date,"mm/dd/yyyy"))
//	end if

end event

event itemfocuschanged;call super::itemfocuschanged;//id_date = dw_2.getitemdate(dw_2.getrow(),1)
end event

event ue_postconstructor;call super::ue_postconstructor;is_selection = "3"
end event

event itemchanged;call super::itemchanged;integer li_rowcount, li_getrow

li_rowcount = dw_2.RowCount()
li_getrow   = dw_2.GetRow()

if li_rowcount = li_getrow then
	dw_2.insertrow(0)
	return
end if
end event

type dw_3 from u_base_dw_ext within w_ffw_open_close
integer x = 55
integer y = 64
integer width = 2670
integer height = 224
integer taborder = 0
boolean bringtotop = true
string dataobject = "d_ffw_header"
end type

