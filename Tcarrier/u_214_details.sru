HA$PBExportHeader$u_214_details.sru
forward
global type u_214_details from nonvisualobject
end type
end forward

global type u_214_details from nonvisualobject autoinstantiate
end type

type variables
string	from_carrier, &
	to_carrier, &
	from_date, &
	to_date, &
	from_time, &
	to_time 

boolean	cancel
end variables

on u_214_details.create
TriggerEvent( this, "constructor" )
end on

on u_214_details.destroy
TriggerEvent( this, "destructor" )
end on

