HA$PBExportHeader$w_edi_214_tran_detail_filter.srw
forward
global type w_edi_214_tran_detail_filter from w_base_response_ext
end type
type dw_1 from u_base_dw_ext within w_edi_214_tran_detail_filter
end type
type dw_2 from u_base_dw_ext within w_edi_214_tran_detail_filter
end type
type dw_3 from u_base_dw_ext within w_edi_214_tran_detail_filter
end type
type st_1 from statictext within w_edi_214_tran_detail_filter
end type
type st_2 from statictext within w_edi_214_tran_detail_filter
end type
type st_3 from statictext within w_edi_214_tran_detail_filter
end type
end forward

global type w_edi_214_tran_detail_filter from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 1157
integer height = 1188
string title = "EDI 214 Transaction Details Filter"
long backcolor = 79741120
dw_1 dw_1
dw_2 dw_2
dw_3 dw_3
st_1 st_1
st_2 st_2
st_3 st_3
end type
global w_edi_214_tran_detail_filter w_edi_214_tran_detail_filter

type variables
DataWindowChild	idwc_carrier

integer		ii_rc

u_214_details	iu_214_details
		
end variables

on w_edi_214_tran_detail_filter.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.dw_2=create dw_2
this.dw_3=create dw_3
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.dw_2
this.Control[iCurrent+3]=this.dw_3
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.st_3
end on

on w_edi_214_tran_detail_filter.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.dw_2)
destroy(this.dw_3)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
end on

event open;call super::open;// load any info into the correct fields
date		ld_from_date, &
			ld_to_date, &
			current

time		lt_from_time, &
			lt_to_time, &
			when
			
string	ls_from_carrier, &
			ls_to_carrier  
									
int		li_rtn,&
			li_pos, &
			li_string_len

long		ll_current_row

iu_214_details = Message.PowerObjectParm

current	= today()
when		= now()


IF iu_214_details.to_carrier <> '' THEN 
    If iu_214_details.to_carrier = iu_214_details.from_carrier  THEN
        dw_1.object.end_carrier_code[1] = "    "
    End if  
END IF

dw_1.SetFocus()
dw_1.SetColumn("start_carrier_code")
dw_1.SelectText(1, 4)

//
//

 
IF iu_214_details.from_date <> '' THEN
	ld_from_date = Date(iu_214_details.from_date)
	dw_2.SetItem( 1, "from_date", ld_from_date)
	IF iu_214_details.to_date <> '' THEN 
		ld_to_date = Date(iu_214_details.to_date)
		dw_2.SetItem( 1, "to_date", ld_to_date)
	Else
		dw_2.SetItem( 1, "to_date", ld_from_date)
	END IF
Else
	dw_2.SetItem( 1, "from_date", RelativeDate(Today(), 0))
	IF iu_214_details.to_date <> '' THEN 
		ld_to_date = Date(iu_214_details.to_date)
		dw_2.SetItem( 1, "to_date", ld_to_date)
		dw_2.SetItem( 1, "from_date", ld_to_date)
	Else
		dw_2.SetItem( 1, "to_date", current)
	END IF
END IF
 
 
IF iu_214_details.from_time <> '' THEN
	lt_from_time = Time(iu_214_details.from_time)
	dw_3.SetItem( 1, "from_time", lt_from_time)
	IF iu_214_details.to_time <> '' THEN 
		lt_to_time = Time(iu_214_details.to_time)
		dw_3.SetItem( 1, "to_time", lt_to_time)
	Else
		dw_3.SetItem( 1, "to_time", lt_from_time)
	END IF
Else
	dw_3.SetItem( 1, "from_time", RelativeTime(Now(), -7200))
	IF iu_214_details.to_time <> '' THEN 
		lt_to_time = Time(iu_214_details.to_time)
		dw_3.SetItem( 1, "to_time", lt_to_time)
		dw_3.SetItem( 1, "from_time", lt_to_time)
	Else
		dw_3.SetItem( 1, "to_time", current)
	END IF
END IF
 

dw_1.SetItem( 1, "end_carrier_code", iu_214_details.to_carrier )
dw_1.SetItem( 1, "start_carrier_code", iu_214_details.from_carrier )
dw_2.SetItem( 1, "from_date", iu_214_details.from_date )
dw_2.SetItem( 1, "to_date", iu_214_details.to_date )
dw_3.SetItem( 1, "from_time", iu_214_details.from_time )
dw_3.SetItem( 1, "to_time", iu_214_details.to_time )

dw_1.SetFocus()

dw_1.SelectText(1, 4)

end event

event ue_base_cancel;iu_214_details.cancel = true
CloseWithReturn( this, iu_214_details )

end event

event ue_base_ok;long		ll_foundrow

string	ls_from_carrier, &
			ls_to_carrier, &
			ls_from_date, &
			ls_to_date, &
			ls_from_time, &
			ls_to_time
			
date		ld_from_date, &
			ld_to_date
			
time		lt_from_time, &
			lt_to_time
			
				
If dw_1.AcceptText() = 1 then
	ls_from_carrier	= " "
   ls_to_carrier  	= " "
     
   ls_from_carrier	= trim( dw_1.object.start_carrier_code[1])
   ls_to_carrier   	= trim( dw_1.object.end_carrier_code[1])
	
	If Trim(ls_from_carrier) = "" then
		ls_from_carrier = " "
	End if
	
	If ls_from_carrier = " " Then
  		MessageBox("Error","From Carrier must be filled in.")
  		dw_1.SetFocus()
  		Return
	End if
	
	
   If Trim(ls_to_carrier) = "" Then
      ls_to_carrier = ls_from_carrier
   End if
Else
	ls_from_carrier = " "
	ls_to_carrier   = " "
End if
	

	
If dw_2.AcceptText() = 1 Then
	ld_from_date  = dw_2.GetItemDate( 1, "from_date" )
	ld_to_date    = dw_2.GetItemDate( 1, "to_date" )
   ls_from_date  = string( ld_from_date, "yyyy-mm-dd" )
	ls_to_date    = string( ld_to_date, "yyyy-mm-dd" )
End If

If ld_from_date > ld_to_date Then
  	MessageBox("Error","From Date must be earlier than To Date.")
  	dw_2.SetFocus()
  	Return
End if
	
If dw_3.AcceptText() = 1 Then
	lt_from_time  = dw_3.GetItemTime( 1, "from_time" )
	lt_to_time    = dw_3.GetItemTime( 1, "to_time" )
   ls_from_time  = string( lt_from_time, "hh.mm.ss" )
	ls_to_time    = string( lt_to_time, "hh.mm.ss" )
End If

If ls_from_time > ls_to_time Then
  	MessageBox("Error","From Time must be earlier than To Time.")
  	dw_3.SetFocus()
  	Return
End if


iu_214_details.from_carrier		= ls_from_carrier
iu_214_details.to_carrier			= ls_to_carrier
iu_214_details.from_date			= ls_from_date
iu_214_details.to_date				= ls_to_date
iu_214_details.from_time			= ls_from_time
iu_214_details.to_time				= ls_to_time
iu_214_details.cancel				= false

Message.PowerObjectParm = iu_214_details

CloseWithReturn( This, iu_214_details )
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_edi_214_tran_detail_filter
integer x = 731
integer y = 976
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_edi_214_tran_detail_filter
integer x = 430
integer y = 976
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_edi_214_tran_detail_filter
integer x = 128
integer y = 976
integer taborder = 40
end type

type dw_1 from u_base_dw_ext within w_edi_214_tran_detail_filter
integer x = 110
integer y = 104
integer width = 923
integer height = 212
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_dddw_start_end_carriers"
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;InsertRow(0)

ii_rc = GetChild( 'start_carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()

ii_rc = GetChild( 'end_carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()


dw_1.SetFocus()
SelectText(1,4)
end event

event getfocus;SelectText(1,6)
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer	li_len
string	ls_temp

ls_temp = this.GetText()

li_len = len(ls_temp)

this.SelectText(1, li_len)
end event

type dw_2 from u_base_dw_ext within w_edi_214_tran_detail_filter
integer x = 110
integer y = 420
integer width = 923
integer height = 212
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_from_and_to_dates"
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;InsertRow(0)

 
dw_2.SetItem(1, "from_date", RelativeDate(Today(), 0))
dw_2.SetItem(1, "to_date", RelativeDate(Today(), 0))
dw_2.SetFocus()
SelectText(1,10)
end event

event getfocus;SelectText(1,10)
end event

event itemchanged;call super::itemchanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)

Date		ld_from, &
			ld_to


If dwo.name = "to_date" Then
	ld_from = dw_2.GetItemDate(1,"from_date")
	ld_to = Date(data)
	If ld_from > ld_to Then
		MessageBox("Date error", "To Date can not be less than the From Date, please reenter dates")
		SelectText(1,10)
		Return 1
	End If
End If


dw_3.SetItem(1, "from_time", RelativeTime(00:01:00, 0))
dw_3.SetItem(1, "to_time", RelativeTime(23:59:00, 0))





end event

event itemerror;call super::itemerror;Return 1
end event

type dw_3 from u_base_dw_ext within w_edi_214_tran_detail_filter
integer x = 110
integer y = 736
integer width = 923
integer height = 212
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_from_and_to_times"
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;integer	now_hours, &
			now_minutes, &
			hours_2
			
time		time_2

InsertRow(0)
time_2 = now()
 
dw_3.SetItem(1, "from_time", RelativeTime(now(), - 7200))
dw_3.SetItem(1, "to_time", RelativeTime(Now(), 0))

//now_hours	= Hour(Now())
//now_minutes	= Minute(Now())
//hours_2 		= now_hours - 2 
//time_2 		= Time(hours_2, now_minutes, 0)

//dw_3.SetItem(1, "from_time", time_2)


dw_3.SetFocus()
SelectText(1,8)
end event

event getfocus;SelectText(1,8)
end event

event itemchanged;call super::itemchanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)

Time		lt_from, &
			lt_to


If dwo.name = "to_time" Then
	lt_from = dw_3.GetItemTime(1,"from_time")
	lt_to = Time(data)
	If lt_from > lt_to Then
		MessageBox("Time error", "To Time can not be less than the From Time, please reenter times")
		SelectText(1,8)
		Return 1
	End If
End If
end event

event itemerror;call super::itemerror;Return 1
end event

type st_1 from statictext within w_edi_214_tran_detail_filter
integer x = 110
integer y = 656
integer width = 329
integer height = 76
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Time Range"
boolean focusrectangle = false
end type

type st_2 from statictext within w_edi_214_tran_detail_filter
integer x = 110
integer y = 340
integer width = 325
integer height = 76
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Date Range"
boolean focusrectangle = false
end type

type st_3 from statictext within w_edi_214_tran_detail_filter
integer x = 110
integer y = 24
integer width = 375
integer height = 76
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Carrier Range"
boolean focusrectangle = false
end type

