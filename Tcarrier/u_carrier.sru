HA$PBExportHeader$u_carrier.sru
forward
global type u_carrier from nonvisualobject
end type
end forward

global type u_carrier from nonvisualobject autoinstantiate
end type

type variables
string	carrier

boolean	cancel
end variables

on u_carrier.create
TriggerEvent( this, "constructor" )
end on

on u_carrier.destroy
TriggerEvent( this, "destructor" )
end on

