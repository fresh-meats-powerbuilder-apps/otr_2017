HA$PBExportHeader$w_booking_transmission_filter.srw
forward
global type w_booking_transmission_filter from w_base_response_ext
end type
type dw_carrier from u_base_dw_ext within w_booking_transmission_filter
end type
type dw_date from u_base_dw_ext within w_booking_transmission_filter
end type
type dw_time from u_base_dw_ext within w_booking_transmission_filter
end type
type dw_complex from u_base_dw_ext within w_booking_transmission_filter
end type
type gb_3 from groupbox within w_booking_transmission_filter
end type
type gb_2 from groupbox within w_booking_transmission_filter
end type
type gb_1 from groupbox within w_booking_transmission_filter
end type
end forward

global type w_booking_transmission_filter from w_base_response_ext
integer width = 2249
integer height = 976
string title = "Booking Transmission Filter"
long backcolor = 79741120
dw_carrier dw_carrier
dw_date dw_date
dw_time dw_time
dw_complex dw_complex
gb_3 gb_3
gb_2 gb_2
gb_1 gb_1
end type
global w_booking_transmission_filter w_booking_transmission_filter

type variables
DataWindowChild	idwc_carrier, &
		idwc_complex

integer	ii_rc

String	is_from_carrier, &
	is_to_carrier, &
	is_complex, &
	is_cancel

Date	id_from_date, &
	id_to_date

Time	it_from_time, &
	it_to_time

u_booking_transmission	iu_booking_transmission

end variables

on w_booking_transmission_filter.create
int iCurrent
call super::create
this.dw_carrier=create dw_carrier
this.dw_date=create dw_date
this.dw_time=create dw_time
this.dw_complex=create dw_complex
this.gb_3=create gb_3
this.gb_2=create gb_2
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_carrier
this.Control[iCurrent+2]=this.dw_date
this.Control[iCurrent+3]=this.dw_time
this.Control[iCurrent+4]=this.dw_complex
this.Control[iCurrent+5]=this.gb_3
this.Control[iCurrent+6]=this.gb_2
this.Control[iCurrent+7]=this.gb_1
end on

on w_booking_transmission_filter.destroy
call super::destroy
destroy(this.dw_carrier)
destroy(this.dw_date)
destroy(this.dw_time)
destroy(this.dw_complex)
destroy(this.gb_3)
destroy(this.gb_2)
destroy(this.gb_1)
end on

event open;call super::open;iu_booking_transmission = Message.PowerObjectParm
date	ld_from_date, ld_to_date, current
time	lt_from_time, lt_to_time, when

current	= today()
when		= now()

IF IsNull(iu_booking_transmission.from_date) THEN
	dw_date.SetItem(1, "from_date", current)
ELSE
	IF String(iu_booking_transmission.from_date) = '1/1/00' THEN
		dw_date.SetItem(1, "from_date", current)
	ELSE
		IF String(iu_booking_transmission.from_date) <> '' THEN 
			ld_from_date = iu_booking_transmission.from_date
			dw_date.SetItem(1, "from_date", ld_from_date)
		ELSE
			dw_date.SetItem(1, "from_date", current)
		END IF
	END IF
END IF

IF IsNull(iu_booking_transmission.to_date) THEN
	dw_date.SetItem( 1, "to_date", current)
ELSE
	IF String(iu_booking_transmission.to_date) = '1/1/00' THEN
		dw_date.SetItem( 1, "to_date", current)
	ELSE
	IF String(iu_booking_transmission.to_date) <> '' THEN 
   	ld_to_date = iu_booking_transmission.to_date
		dw_date.SetItem(1, "to_date", ld_to_date)
	ELSE
		dw_date.SetItem( 1, "to_date", current)
	END IF
	END IF
END IF

IF IsNull(iu_booking_transmission.from_time) THEN
	dw_time.SetItem(1, "from_time", RelativeTime(00:00:00, 0))
ELSE
	IF String(iu_booking_transmission.from_time) = '00:00:00' THEN
		dw_time.SetItem( 1, "from_time", RelativeTime(00:00:00, 0))
	ELSE
		IF String(iu_booking_transmission.from_time) <> '' THEN 
			lt_from_time = iu_booking_transmission.from_time
			dw_time.SetItem(1, "from_time", lt_from_time)
		ELSE
			dw_time.SetItem(1, "from_time", RelativeTime(00:00:00, 0))
		END IF
	END IF		
END IF

IF IsNull(iu_booking_transmission.to_time) THEN
	dw_time.SetItem( 1, "to_time", RelativeTime(23:59:59, 0))
ELSE
	IF String(iu_booking_transmission.to_time) = '00:00:00' THEN
		dw_time.SetItem( 1, "to_time", RelativeTime(23:59:59, 0))
	ELSE
		IF String(iu_booking_transmission.to_time) <> '' THEN 
   		lt_to_time = iu_booking_transmission.to_time
			dw_time.SetItem(1, "to_time", lt_to_time)
		ELSE
			dw_time.SetItem( 1, "to_time", RelativeTime(23:59:59, 0))
		END IF
	END IF
END IF

IF IsNull(iu_booking_transmission.from_carrier) THEN
	dw_carrier.SetItem( 1, "start_carrier_code", "    ")
ELSE
   IF iu_booking_transmission.from_carrier <> '    ' THEN 
	   dw_carrier.SetItem( 1, "start_carrier_code", iu_booking_transmission.from_carrier)
		is_from_carrier = iu_booking_transmission.from_carrier
	ELSE
		dw_carrier.SetItem( 1, "start_carrier_code", "    ")
   END IF
END IF

IF IsNull(iu_booking_transmission.to_carrier) THEN
	dw_carrier.SetItem( 1, "end_carrier_code", "    ")
ELSE
   IF iu_booking_transmission.to_carrier <> '    ' THEN 
	   dw_carrier.SetItem( 1, "end_carrier_code", iu_booking_transmission.to_carrier)
		is_to_carrier = iu_booking_transmission.to_carrier
	ELSE
		dw_carrier.SetItem( 1, "end_carrier_code", "    ")
   END IF
END IF

IF IsNull(iu_booking_transmission.complex) THEN
	dw_complex.SetItem( 1, "complex", "   ")
ELSE
   IF iu_booking_transmission.complex <> '   ' THEN 
	   dw_complex.SetItem( 1, "complex", iu_booking_transmission.complex)
		is_complex = iu_booking_transmission.complex
	ELSE
		dw_complex.SetItem( 1, "complex", "   ")
   END IF
END IF


dw_carrier.SetFocus()
end event

event ue_base_cancel;iu_booking_transmission.cancel = true
CloseWithReturn( this, iu_booking_transmission )

end event

event ue_base_ok;long		ll_foundrow

string	ls_from_carrier, &
			ls_to_carrier, &
			ls_complex, &
			ls_from_date, &
			ls_to_date, &
			ls_from_time, &
			ls_to_time
			
date		ld_from_date, &
			ld_to_date
			
time		lt_from_time, &
			lt_to_time
			
				
If dw_carrier.AcceptText() = 1 then
	ls_from_carrier	= " "
   ls_to_carrier  	= " "
     
   ls_from_carrier	= trim( dw_carrier.object.start_carrier_code[1])
   ls_to_carrier   	= trim( dw_carrier.object.end_carrier_code[1])
	
	If Trim(ls_from_carrier) = "" then
		ls_from_carrier = "    "
	End if	
	
   If Trim(ls_to_carrier) = "" Then
      ls_to_carrier = ls_from_carrier
   End if
	
Else
	ls_from_carrier = "    "
	ls_to_carrier   = "    "
End if
	

	
If dw_date.AcceptText() = 1 Then
	ld_from_date  = dw_date.GetItemDate( 1, "from_date" )
	ld_to_date    = dw_date.GetItemDate( 1, "to_date" )
//   ls_from_date  = string( ld_from_date, "yyyy-mm-dd" )
//	ls_to_date    = string( ld_to_date, "yyyy-mm-dd" )
End If

If ld_from_date > ld_to_date Then
  	MessageBox("Error","From Date must be earlier than To Date.")
  	dw_date.SetFocus()
  	Return
End if

If ld_to_date > ld_from_date Then
	dw_time.SetItem(1, "from_time", RelativeTime(00:00:00, 0))
	dw_time.SetItem(1, "to_time", RelativeTime(23:59:59, 0))
	lt_from_time  = dw_time.GetItemTime( 1, "from_time" )
	lt_to_time    = dw_time.GetItemTime( 1, "to_time" )
Else
	If dw_time.AcceptText() = 1 Then
		lt_from_time  = dw_time.GetItemTime( 1, "from_time" )
		lt_to_time    = dw_time.GetItemTime( 1, "to_time" )
   	ls_from_time  = string( lt_from_time, "hh.mm.ss" )
		ls_to_time    = string( lt_to_time, "hh.mm.ss" )
		If ls_from_time > ls_to_time Then
  			MessageBox("Error","From Time must be earlier than To Time.")
  			dw_time.SetFocus()
  			Return
		End if
	End if
End if

If dw_complex.AcceptText() = 1 then
	ls_complex	= " "
   ls_complex	= trim( dw_complex.object.complex[1])
	If Trim(ls_complex) = "" then
		ls_complex = "   "
	End if	
Else
	ls_complex = "   "
End if

If ls_from_carrier = "    " Then
	If ls_complex = "   " Then
		MessageBox("Error","A Carrier Range or a Complex Must be entered.")
  		dw_carrier.SetFocus()
		Return
	End If
End If

	


iu_booking_transmission.from_carrier		= ls_from_carrier
iu_booking_transmission.to_carrier			= ls_to_carrier
iu_booking_transmission.from_date			= ld_from_date
iu_booking_transmission.to_date				= ld_to_date
iu_booking_transmission.from_time			= lt_from_time
iu_booking_transmission.to_time				= lt_to_time
iu_booking_transmission.complex				= ls_complex
iu_booking_transmission.cancel				= false


Message.PowerObjectParm = iu_booking_transmission

CloseWithReturn( This, iu_booking_transmission )
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_booking_transmission_filter
integer x = 1280
integer y = 768
integer taborder = 70
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_booking_transmission_filter
integer x = 978
integer y = 768
integer taborder = 60
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_booking_transmission_filter
integer x = 677
integer y = 768
integer taborder = 50
end type

type dw_carrier from u_base_dw_ext within w_booking_transmission_filter
integer x = 78
integer y = 92
integer width = 933
integer height = 216
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_dddw_start_end_carriers"
boolean border = false
end type

event constructor;call super::constructor;InsertRow(0)

ii_rc = GetChild( 'start_carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()

ii_rc = GetChild( 'end_carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()

dw_carrier.SetFocus()
SelectText(1,4)
end event

event getfocus;SelectText(1,6)
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer	li_len
string	ls_temp

ls_temp = this.GetText()

li_len = len(ls_temp)

this.SelectText(1, li_len)
end event

type dw_date from u_base_dw_ext within w_booking_transmission_filter
integer x = 78
integer y = 476
integer width = 933
integer height = 216
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_from_and_to_dates"
boolean border = false
end type

event getfocus;SelectText(1,10)
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer	li_len
string	ls_temp

ls_temp = this.GetText()

li_len = len(ls_temp)

this.SelectText(1, li_len)
end event

event constructor;call super::constructor;InsertRow(0)

date current
current = today()
SetItem( 1, "from_date", current )
dw_date.SetItem(1, "from_date", current )

SetItem( 1, "to_date", current )
dw_date.SetItem(1, "to_date", current)

end event

type dw_time from u_base_dw_ext within w_booking_transmission_filter
integer x = 1175
integer y = 476
integer width = 933
integer height = 216
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_from_and_to_times"
boolean border = false
end type

event getfocus;SelectText(1,8)
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer	li_len
string	ls_temp

ls_temp = this.GetText()

li_len = len(ls_temp)

this.SelectText(1, li_len)
end event

event constructor;call super::constructor;InsertRow(0)

time when
when = now()
SetItem(1, "from_time", RelativeTime(00:00:00, 0))
//dw_time.SetItem(1, "from_time", when)

SetItem( 1, "to_time", RelativeTime(23:59:59, 0))
//dw_time.SetItem(1, "to_time", when)

dw_time.SetFocus()
SelectText(1,8)
end event

event itemchanged;call super::itemchanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)

Time		lt_from, &
			lt_to


If dwo.name = "to_time" Then
	lt_from = dw_time.GetItemTime(1,"from_time")
	lt_to = Time(data)
	If lt_from > lt_to Then
		MessageBox("Time error", "To Time can not be less than the From Time, please reenter times")
		SelectText(1,8)
		Return 1
	End If
End If
end event

type dw_complex from u_base_dw_ext within w_booking_transmission_filter
integer x = 1143
integer y = 64
integer width = 1051
integer height = 304
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_dddw_complex"
end type

event getfocus;SelectText(1,3)
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer	li_len
string	ls_temp

ls_temp = this.GetText()

li_len = len(ls_temp)

this.SelectText(1, li_len)
end event

event constructor;call super::constructor;insertRow(0)
ii_rc = GetChild( 'complex', idwc_complex )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for complex.")
idwc_complex.Reset()
idwc_complex.SetTrans(SQLCA)
idwc_complex.Retrieve()

dw_complex.SetFocus()
SelectText(1,3)
end event

event itemchanged;call super::itemchanged;long ll_FoundRow
iw_frame.SetMicroHelp(dwo.name)
IF Trim(data) = "" THEN
	is_complex = space(3)
	Return
END IF

ll_FoundRow = idwc_complex.Find ( "complex_code='"+data+"'", 1, idwc_complex.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Complex Error", data + " is Not a Valid Complex Code." )
	SelectText(1,3)
	Return 1
ELSE
	is_complex = data
END IF		

//dw_complex.settext(" ")
end event

event clicked;call super::clicked;//dw_complex.settext(" ")
end event

type gb_3 from groupbox within w_booking_transmission_filter
integer x = 1143
integer y = 416
integer width = 1051
integer height = 332
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 79741120
string text = "Assigned Time Range"
end type

type gb_2 from groupbox within w_booking_transmission_filter
integer x = 46
integer y = 416
integer width = 1051
integer height = 332
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 79741120
string text = "Assigned Date Range"
end type

type gb_1 from groupbox within w_booking_transmission_filter
integer x = 50
integer y = 36
integer width = 1051
integer height = 332
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 79741120
string text = "Carrier Range"
end type

