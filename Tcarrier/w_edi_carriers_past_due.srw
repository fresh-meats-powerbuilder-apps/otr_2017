HA$PBExportHeader$w_edi_carriers_past_due.srw
forward
global type w_edi_carriers_past_due from w_base_sheet_ext
end type
type dw_1 from u_base_dw_ext within w_edi_carriers_past_due
end type
end forward

global type w_edi_carriers_past_due from w_base_sheet_ext
integer x = 14
integer y = 4
integer width = 2240
integer height = 1200
string title = "Carriers w/Past Due Deliveries"
long backcolor = 79741120
dw_1 dw_1
end type
global w_edi_carriers_past_due w_edi_carriers_past_due

type variables
u_otr006   iu_otr006
u_otr004	iu_otr004
u_ws_edi iu_ws_edi

char ic_req_type[1]

integer   ii_error_column

long   il_error_row

end variables

forward prototypes
public function boolean wf_retrieve ()
public function integer wf_load_edi_carriers_past_due ()
end prototypes

public function boolean wf_retrieve ();s_error  lstr_Error_Info

setRedraw(False)

dw_1.Reset()

iw_frame.SetMicroHelp ("Wait.. Inquiring Database")

dw_1.Reset()
wf_load_edi_carriers_past_due()
//dw_1.ResetUpdate()
SetRedraw(True) 

Return TRUE


end function

public function integer wf_load_edi_carriers_past_due ();integer  li_rtn

string   ls_carrier_string, &
			ls_refetch_carrier
			 
s_error                 lstr_Error_Info

SetPointer(HourGlass!)

lstr_error_info.se_event_name = "wf_retrieve"
lstr_error_info.se_window_name = "w_edi_carriers_past_due"
lstr_error_info.se_procedure_name = "wf_retrieve"
lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_message = Space(71)

ls_carrier_string  = Space(2000)
ls_refetch_carrier = Space(4)

//li_rtn = iu_otr006.nf_otrt73ar(dw_1, lstr_error_info)

li_rtn = iu_ws_edi.nf_otrt73er(dw_1, lstr_error_info)

// Check the return code from the above function call
If li_rtn <> 0 Then
 		dw_1.ResetUpdate()
	   Return -1
ELSE
      iw_frame.SetMicroHelp( "Ready...")
END IF

 
//li_rtn = dw_1.ImportString(Trim(ls_carrier_string))
//
//If li_rtn <> 0 Then 
//	iw_frame.SetMicroHelp("No Records Found.")
//End If

dw_1.ResetUpdate()
dw_1.SetRedraw(true)
Return( 1 )


end function

on w_edi_carriers_past_due.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_edi_carriers_past_due.destroy
call super::destroy
destroy(this.dw_1)
end on

event close;call super::close;DESTROY iu_otr006
Destroy iu_otr004
DESTROY iu_ws_edi
end event

event open;call super::open;dw_1.InsertRow(0)
end event

event ue_postopen;call super::ue_postopen;iu_otr006  =  CREATE u_otr006
iu_otr004  =  CREATE u_otr004
iu_ws_edi =  CREATE u_ws_edi

wf_retrieve() 

//THEN Close( this )
end event

event activate;call super::activate;iw_frame.im_menu.mf_disable("m_delete")
iw_frame.im_menu.mf_disable("m_save")
iw_frame.im_menu.mf_disable("m_insert")

end event

event ue_fileprint;call super::ue_fileprint;dw_1.Print( )

end event

type dw_1 from u_base_dw_ext within w_edi_carriers_past_due
integer y = 4
integer width = 2190
integer height = 1084
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_edi_carriers_past_due"
boolean vscrollbar = true
end type

event doubleclicked;call super::doubleclicked;char     lc_delv_date[8]         

long     ll_Column
      
string   ls_input_string, &
         ls_start_carrier, &
			ls_end_carrier
			
date		ld_work_date

w_mass_conf  lw_mass_conf
  

IF row > 0 and row <= RowCount() and (is_ObjectAtPointer = "carrier_code_1" or is_ObjectAtPointer = "carrier_code_2" or is_ObjectAtPointer = "carrier_code_3" or is_ObjectAtPointer = "carrier_code_4" or is_ObjectAtPointer = "carrier_code_5" or is_ObjectAtPointer = "carrier_code_6")  Then
	ll_column = Integer(dwo.id)  //find out correct id (always 1)
   ls_start_carrier = This.object.data[Row, ll_column]
   ls_end_carrier = ls_start_carrier
	
	ld_work_date 	=  today()
   lc_delv_date = String(ld_work_date, "yyyymmdd")
	//ibdkdld
   ls_input_string = ls_start_carrier + ls_end_carrier + lc_delv_date + "  " 
   OpenSheetWithParm(lw_mass_conf, ls_input_string, "w_mass_conf", iw_frame, 0, Original!)  
End IF

end event

