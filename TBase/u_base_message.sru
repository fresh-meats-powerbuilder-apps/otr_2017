HA$PBExportHeader$u_base_message.sru
$PBExportComments$Base Message Object
forward
global type u_base_message from message
end type
end forward

global type u_base_message from message
end type
global u_base_message u_base_message

type variables
// Application INI Name
string	is_ApplicationINI

Private:
String	is_App_ID

end variables

forward prototypes
public subroutine nf_set_app_id (string as_app_id)
public function string nf_get_app_id ()
end prototypes

public subroutine nf_set_app_id (string as_app_id);is_app_id = as_app_id
end subroutine

public function string nf_get_app_id ();return is_app_id
end function

on u_base_message.create
call message::create
TriggerEvent( this, "constructor" )
end on

on u_base_message.destroy
call message::destroy
TriggerEvent( this, "destructor" )
end on

