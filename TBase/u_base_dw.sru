HA$PBExportHeader$u_base_dw.sru
$PBExportComments$Base Standard Datawindow Control
forward
global type u_base_dw from datawindow
end type
type validation_struct from structure within u_base_dw
end type
end forward

type validation_struct from structure
    string expression
    string error_message
end type

global type u_base_dw from datawindow
integer width = 2043
integer height = 864
integer taborder = 1
event ue_retrieve pbm_custom75
event ue_saveas pbm_custom74
event ue_insertrow pbm_custom73
event ue_deleterow pbm_custom72
event ue_deleteallrows pbm_custom71
event ue_sort pbm_custom70
event ue_filter pbm_custom69
event ue_next pbm_custom68
event ue_previous pbm_custom67
event ue_first pbm_custom66
event ue_last pbm_custom65
event ue_cut pbm_custom64
event ue_copy pbm_custom63
event ue_paste pbm_custom62
event ue_clear pbm_custom61
event ue_resetsyntax pbm_custom60
event ue_no_scroll pbm_custom59
event ue_keydown pbm_dwnkey
event ue_duplicaterow pbm_custom58
event ue_update pbm_custom57
event ue_maketoprowcurrent pbm_custom56
event ue_lbuttonup pbm_lbuttonup
event ue_postconstructor pbm_custom55
event ue_import pbm_custom54
event ue_finddlg ( )
event ue_replacedlg ( )
event ue_post_keydown ( )
event ue_mousemove pbm_dwnmousemove
end type
global u_base_dw u_base_dw

type variables
// Datawindow Row Selection (0,1,2,3)
// 	0 - No rows selected (default)
//	1 - One row selected
//	2 - Multiple rows selected
//	3 - Multiple rows with CTRL and ALT support
string	is_selection= "0"

// Does datawindow have update capabilities
boolean	ib_Updateable

// Reset Datawindow on insert of new row
//	TRUE - Reset datawindow on Insert
// 	FALSE- Do not Reset DW on Insert (default)
boolean	ib_ResetOnInsert

//used to set the top row in datawindow as current row
boolean	ib_ScrollSetsCurrentRow
// Parent window established in constructor event
window	iw_parent

// Contains the row number of the current row
long	il_dwRow

// Holds the number of the last clicked row
// Used for selecting rows with Shift Key
long	il_last_clicked_row

// Draggable Datawindow
// Need to issue Drag(Begin!) in clicked event
// ue_lbuttonup automatically turns Drag Off
boolean ib_draggable

// Holds the name of the object at pointer the user clicked on
string	is_ObjectAtPointer

// Holds the name of the band the user clicked in
string	is_BandAtPointer

// Holds the name of the current column
string	is_ColumnName

// Set this variable to Move the focus to the first column 
// in next row when enter is hit
boolean	ib_firstcolumnonnextrow

// Enable scrolling in the datawindow
// FALSE	- Do not allow scrolling
// TRUE	- Allow scrolling (default)
boolean	ib_scrollable = TRUE

//nonvisual used for find replace dialog boxes
u_findreplace	inv_find

end variables

forward prototypes
public subroutine uf_changerowstatus (long ai_row, dwitemstatus ae_desiredstatus)
public function boolean uf_check_required ()
public function boolean uf_loaddwsyntax ()
public function integer uf_modified ()
public subroutine uf_preview_mode ()
public subroutine uf_resize_control ()
public subroutine uf_resize_height ()
public subroutine uf_resize_width ()
public subroutine uf_resize_wplace ()
public function boolean uf_savedwsyntax ()
public subroutine uf_settabkeyzero ()
public function boolean uf_undo_edit_control ()
public function boolean uf_required (boolean ab_required)
public function integer uf_dw_getvisiblecolumns (powerobject adw_arg, ref string as_colnames[])
end prototypes

on ue_saveas;// Open the default File Save As dialog box
This.SaveAs()

// Set focus back to this control after the response window closes
This.SetFocus()
end on

on ue_insertrow;// Insert a row above the current row and then set
// scroll to that row

This.ScrollToRow(This.InsertRow( il_dwrow))
end on

event ue_deleterow;// Delete the current row or all selected rows

// If rows are selected first capture all selected rows in array
// then delete



long		ll_selected_row, &
			ll_rows[]
integer	li_inc, &
			i

If GetRow() = 0 Then Return

ll_selected_row  = GetSelectedRow(0)

If ll_selected_row  >  0 Then 
	
	Do while ll_selected_row > 0 
		li_inc = li_inc + 1
		ll_rows[li_inc] = ll_selected_row
		ll_selected_row  = GetSelectedRow(ll_selected_row)
	Loop

	For i = li_inc to 1 step -1
		DeleteRow(ll_rows[i])
	Next

Else
	DeleteRow(GetRow())
End if

end event

event ue_sort;String			ls_Null


// Pop up the default PB Filter Dialog
SetNull(ls_Null)
SetSort(ls_Null)
Sort()


// Set focus back to this control after the response window closes
This.SetFocus()
end event

on ue_filter;String			ls_Null


// Pop up the default PB Filter Dialog
SetNull(ls_Null)
SetFilter(ls_Null)

Filter()

// Set focus back to this control after the response window closes
This.SetFocus()

end on

event ue_next;// Scroll to the next row in the datawindow.

Long	ll_Row, &
		ll_RowCount

ll_Row = This.GetRow()
ll_RowCount = This.RowCount()

IF ll_Row > 0 AND ll_Row < ll_RowCount THEN
	This.ScrollToRow( ll_Row + 1 )
END IF

This.SetFocus()
end event

on ue_previous;// Scroll to the previous row in the datawindow.

Long	ll_Row

ll_Row = This.GetRow()

IF ll_Row > 1 THEN
	This.ScrollToRow( ll_Row - 1 )
END IF

This.SetFocus()
end on

on ue_first;// Scroll to the first row in the datawindow.

Long	ll_RowCount

ll_RowCount = This.RowCount()

IF ll_RowCount > 0 THEN
	This.ScrollToRow( 1 )
END IF

This.SetFocus()
end on

on ue_last;// Changes were made to one of the datawindows

Long	ll_RowCount

ll_RowCount = This.RowCount()

IF ll_RowCount > 0 THEN
	This.ScrollToRow(ll_RowCount)
END IF

This.SetFocus()
end on

on ue_cut;// Cuts the selected data from an edit control
Cut()
end on

on ue_copy;Copy()
end on

on ue_paste;// Pastes the text contents of the ClipBoard into the current edit control
Paste()
end on

event ue_clear;// Clear the currently selected text or set the item to null if it is
// a non-editable drop down list box or drop down datawindow, and also
// set the item to null if it is an edit mask style.

Date		ld_Null
DateTime	ldt_Null
Decimal	lc_Null
Integer	li_Null
Long		ll_Row
String	ls_ColumnName, &
			ls_ColType, &
			ls_EditStyle, &
			ls_Null
Time		lt_Null

ls_ColumnName = This.GetColumnName()
IF ls_ColumnName > "" THEN

	ls_EditStyle = This.Describe( ls_ColumnName + ".Edit.Style" )
	CHOOSE CASE ls_EditStyle

		CASE "ddlb", "dddw", "editmask"
			IF This.Describe( ls_ColumnName + ".ddlb.AllowEdit" ) = &
				"yes" OR &
				This.Describe( ls_ColumnName + ".dddw.AllowEdit" ) = &
				"yes" THEN
				This.Clear()
			ELSE
				ll_Row = This.GetRow()
				IF ll_Row > 0 THEN
					ls_ColType = This.Describe( ls_ColumnName + &
						".ColType" )
					IF Left( ls_ColType, 4 ) = "char" THEN
						SetNull( ls_Null )
						This.SetItem( ll_Row, ls_ColumnName, ls_Null )
					ELSEIF ls_ColType = "number" THEN
						SetNull( li_Null )
						This.SetItem( ll_Row, ls_ColumnName, li_Null )
					ELSEIF ls_ColType = "date" THEN
						SetNull( ld_Null )
						This.SetItem( ll_Row, ls_ColumnName, ld_Null )
					ELSEIF ls_ColType = "time" THEN
						SetNull( lt_Null )
						This.SetItem( ll_Row, ls_ColumnName, lt_Null )
					ELSEIF ls_ColType = "datetime" THEN
						SetNull( ldt_Null )
						This.SetItem( ll_Row, ls_ColumnName, ldt_Null )
					ELSEIF Left( ls_ColType, 7 )  = "decimal" THEN
						SetNull( lc_Null )
						This.SetItem( ll_Row, ls_ColumnName, lc_Null )
					END IF
				END IF
			END IF

		CASE ELSE
			This.Clear()

	END CHOOSE

END IF
end event

on ue_resetsyntax;// Restore the datawindow to its original syntax.
LibraryDelete( "dwsyntax.pbl", This.DataObject, ImportDatawindow! )
This.DataObject = This.DataObject
This.TriggerEvent( "ue_Retrieve" )
end on

on ue_no_scroll;				
// Used to restrict scrolling when ib_scrollable is false
// Posted from ue_keydown

long	ll_Primary_row

// Get the current Row
ll_Primary_row = This.GetRow()

if GetRow() <> ll_Primary_row then
	SetRow(ll_Primary_row)
	ScrollToRow(ll_Primary_row)
end if




end on

event ue_keydown;INTEGER		li_Column, &
				li_ColumnCount


long			ll_CurrentRow, &
				ll_CurrentColumn
				
String		ls_date, &
				ls_ColumnType

str_parms	lstr_parms

CHOOSE CASE TRUE
CASE	KeyDown(KeyEnter!)
	IF NOT ib_firstcolumnonnextrow THEN RETURN
	IF il_dwrow < This.RowCount() THEN
		li_ColumnCount = INTEGER(This.Describe("DataWindow.Column.Count"))
		FOR li_Column = 1 TO li_ColumnCount
			IF This.Describe("#" + STRING(li_Column) + ".TabSequence") = "10" THEN
				This.SetColumn(li_Column)
			END IF
		NEXT
	END IF
CASE	Keydown(KeyControl!) and Keydown(KeyDownArrow!)
	IF This.Describe("DataWindow.ReadOnly") = 'yes' THEN RETURN 
	
	// Get the current row and column that has focus
			ll_CurrentRow = This.GetRow()
			ll_CurrentColumn = This.GetColumn()
			
	// Get the current column name
			is_ColumnName = GetColumnName()

	// Get the column type of the clicked field
			ls_ColumnType = Lower(This.Describe("#" + String( &
												ll_CurrentColumn) + ".ColType"))

	// Get the X and Y coordinates of the place that was clicked
		lstr_parms.integer_arg[1] = long(This.Describe(is_ColumnName + &
									".X")) + iw_parent.X + 200
	CHOOSE CASE	lstr_parms.integer_arg[1]
		CASE IS > 2253
			lstr_parms.integer_arg[1] = 2253
		CASE is < 113
			lstr_parms.integer_arg[1] = 113
	END CHOOSE

	lstr_parms.integer_arg[2] = long(This.Describe(is_ColumnName + &
								".Y")) + iw_parent.Y - 400
	CHOOSE CASE	lstr_parms.integer_arg[2]
		CASE IS > 1053
			lstr_parms.integer_arg[2] = 1053
		CASE is < 0
			lstr_parms.integer_arg[2] = 0
	END CHOOSE
	
	IF ls_ColumnType = "date" or &
		ls_ColumnType = "datetime" THEN
			
			Choose Case ls_ColumnType
				Case "date"
					lstr_parms.date_arg[1] = This.GetItemDate(ll_CurrentRow, &
																	ll_CurrentColumn)
					IF IsNull(lstr_parms.date_arg[1]) THEN
						lstr_parms.date_arg[1] = Today()
					END IF
					OpenWithParm(w_Calendar, lstr_parms)
					// Get the return string (date) from the calendar window
					// If an empty string do not do anything
					ls_date = Message.StringParm
// If you hit cntl downarrow the calendar pops up.  When the datawindow gets
// focus it scrolls to the next row.
// This bug has been fixed, however, it was determined to be more trouble than it was
// worth.  So, If it has been decided to implement, uncomment the Down arrow bug
// below.
//downarrow bug					This.SetRedraw(False)
					If ls_date <> "" Then
//downarrow bug						Message.StringParm = String(ll_currentrow)
//downarrow bug						This.PostEvent('ue_post_keydown')
						SetText(ls_Date)
					Else
//downarrow bug						Message.StringParm = String(ll_currentrow)
//downarrow bug						This.PostEvent('ue_post_keydown')
						Return
					End If
					
				Case "datetime"
					lstr_parms.date_arg[1] = Date(This.GetItemDateTime(ll_CurrentRow, &
																		ll_CurrentColumn))
					OpenWithParm(w_Calendar, lstr_parms)
					// Get the return string (date) from the calendar window
					// If an empty string do not do anything
					ls_date = Message.StringParm
						This.SetRedraw(False)
					If ls_date <> "" Then
//downarrow bug						Message.StringParm = String(ll_currentrow)
//downarrow bug						This.PostEvent('ue_post_keydown')
						SetText(ls_Date)
					Else
//downarrow bug						Message.StringParm = String(ll_currentrow)
//downarrow bug						This.PostEvent('ue_post_keydown')
						Return
					End If
				End Choose
	END IF
CASE ELSE

	// Used to not allow scrolling of a datawindow
	// If scroll ins allowed then return out of script
	If ib_scrollable Then Return

	// Turn the vertical scroll bar off
	vscrollbar=False

		IF (KeyDown(KeyTab!))			or &
			(KeyDown(KeyEnter!))			or &
			(KeyDown(KeyDownArrow!))	or &
		 	(KeyDown(KeyUpArrow!))		or &
		 	(KeyDown(KeyPageDown!))		or &
		 	(KeyDown(KeyPageUp!))			THEN
			 MessageBox("","Im here")
				SetRedraw(False)				
				PostEvent("ue_no_scroll")
		END IF
END CHOOSE

end event

on ue_duplicaterow;// Duplicate the current row and then scroll to it
This.RowsCopy(il_dwrow, il_dwrow, Primary!, This, il_dwrow, Primary!)

This.ScrollToRow(il_dwrow - 1)
end on

on ue_maketoprowcurrent;//////////////////////////////////////////////////////////////////////
//
//	User Event:		ue_MakeTopRowCurrent
//	Description:	Make the top row in the datawindow the current row.
//
//	Developer		Date		Description
//	---------		----		-----------
//
//////////////////////////////////////////////////////////////////////

Long	ll_TopRow

ll_TopRow = Long( This.Describe( "Datawindow.FirstRowOnPage" ) )

IF ll_TopRow > 0 AND ll_TopRow <> This.GetRow() THEN
	This.SetRow( ll_TopRow )
END IF
end on

on ue_lbuttonup;// Reset ib_draggable so user cannot enter object with
// left button down and begin drag mode

ib_draggable = FALSE
end on

event ue_postconstructor;integer	li_save_columnlayouts
// Load the datawindow syntax if the ib_storedatawindowsyntax is TRUE

gw_base_frame.iu_base_data.Trigger Event ue_get_dwsettings(li_save_columnlayouts)
IF li_save_columnlayouts <> 0 and &
	Long(This.Describe("datawindow.processing")) = 1 THEN
	This.uf_LoadDWSyntax()
End If

end event

on ue_import;int li_value
string ls_pathname, ls_filename

li_value = GetFileOpenName("Select File",  &
	+ ls_pathname, ls_filename, "DOC",  &
	+ "Text Files (*.TXT),*.TXT," )

IF li_value = 1 THEN 
   li_value = ImportFile(ls_pathname)
   If li_value <= 0 Then
      Messagebox("Error", "Import failed. Check the "+ ls_pathname + &
																				" file.")
   End if 
End if
end on

event ue_finddlg;IF NOT IsValid(inv_find) THEN
	inv_find	=	Create	u_findreplace
END IF

// Call for the Find Dialog box.
inv_find.nf_setrequestor(this)
inv_find.Event ue_finddlg()

Return
end event

event ue_replacedlg;
IF NOT IsValid(inv_find) THEN
	inv_find	=	Create	u_findreplace
END IF

// Call for the Find/Replace Dialog.
inv_find.nf_setrequestor(this)
inv_find.Event ue_replacedlg()

Return
end event

event ue_post_keydown;Long			ll_currentrow


ll_currentrow = Long(Message.StringParm)

ScrollToRow(ll_currentrow)
This.SetRedraw(True)
Return

end event

event ue_mousemove;// If the left mouse button is down while the mouse is
// moving, drag this object.

If ib_draggable then
	IF il_DWRow > 0 AND Message.WordParm = 1 THEN
		Drag( Begin! )
	End If
End If
end event

public subroutine uf_changerowstatus (long ai_row, dwitemstatus ae_desiredstatus);//////////////////////////////////////////////////////////////////////////
//
//	Function:	uf_ChangeRowStatus(ai_row, le_DesiredStatus)
//
//	Purpose:		Changes the Item Status of a row or all the rows
//
//	Arguments:	ai_Row				A long identifying the row(s) to change
//											the itemstatus.  If ai_row is 0 then all
//											 rows in the datawindow will be changed.
//					ae_DesiredStatus	The dwItemStatus that you want the row(s)
//											to change to.
//
//	Author:		T.J. Cox
//////////////////////////////////////////////////////////////////////////

long				ll_RowCount, &
					ll_RowIndex, &
					ll_StartRow
integer			li_ChangeSteps, &
					li_ChangeIndex
dwitemstatus	le_CurrentStatus, &
					le_ChangeStatus[]


// Find out if the status should be changed for a single row or
// for every row in this datawindow
If ai_row = 0 Then
	ll_StartRow = 1
	ll_RowCount = This.RowCount()	
Else
	ll_StartRow = ai_row
	ll_RowCount = ai_row
End If

// Loop through the row(s) and change the status to desired itemstatus
For ll_RowIndex = ll_StartRow to ll_RowCount

	// Get the status of the current row
	le_CurrentStatus = This.GetItemStatus(ll_RowIndex, 0, Primary!)

	Choose Case	le_CurrentStatus
		Case	ae_DesiredStatus
			// Do not do anything - it is already at the specified status

		Case	New!
			Choose Case ae_DesiredStatus
				Case NewModified!
					le_ChangeStatus[1] = NewModified!
				Case DataModified!
					le_ChangeStatus[1] = DataModified!
				Case NotModified!
					le_ChangeStatus[1] = DataModified!
					le_ChangeStatus[2] = NotModified!
			End Choose

		Case	NewModified!
			Choose Case ae_DesiredStatus
				Case	New!
					le_ChangeStatus[1] = NotModified!
				Case	DataModified!
					le_ChangeStatus[1] = DataModified!
				Case 	NotModified!
					le_ChangeStatus[1] = DataModified!
					le_ChangeStatus[2] = NotModified!
			End Choose

		Case DataModified!
			Choose Case ae_DesiredStatus
				Case	New!
					le_ChangeStatus[1] = NotModified!
					le_ChangeStatus[2] = New!					
				Case	NewModified!
					le_ChangeStatus[1] = NewModified!
				Case NotModified!
					le_ChangeStatus[1] = NotModified!
			End Choose

		Case NotModified!
			Choose Case ae_DesiredStatus
				Case New!
					le_ChangeStatus[1] = New!					
				Case NewModified!
					le_ChangeStatus[1] = NewModified!
				Case DataModified!
					le_ChangeStatus[1] = DataModified!
			End Choose
	End Choose	

	// Find out the number of steps needed to change the status of the row
	li_ChangeSteps = UpperBound(le_ChangeStatus[])

	// Change the status of the row
	For li_ChangeIndex = 1 to li_ChangeSteps
		This.SetItemStatus(ll_RowIndex, 0, Primary!, le_ChangeStatus[li_ChangeIndex])	
	Next

Next
end subroutine

public function boolean uf_check_required ();/////////////////////////////////////////////////////////////// 
//
//   Function:  uf_check_required		 Scope: Public
//   
//	  Purpose:   Loops through datawindow and identifies if 
//           	 any column with the required attribute turned 'on'
//					 is empty. If one is found the column will be set 
//					 to the current column.
//  
//	  Arguments: (NONE)
//   
//	  Returns:   boolean      
//  
//            ******* Version  Log ******** 
//   DATE         WHO        WHAT
//  --------     -----      ------------------------------------ 
//   3/29/94   Powersoft     Initial Version
//	  9/07/95	Anil Peggerla Documentation
//	  7/13/96   Tim Bornholtz Change dwFindRequired to FindRequired
//////////////////////////////////////////////////////////////// 

// Check to see if all required fields have been entered.
// Position the user to the row and column in error.

long	   ll_row =1
integer	li_col = 1
string	ls_colname

// Dwfindrequired will populate 'ROW' with the row# in error
// when all rows are evaluated 'ROW' is set to 0   
// If row is not 0, a required row,column was found without a value.
Do While ll_row <> 0
   If FindRequired(primary!, ll_row, li_col, ls_colname, TRUE) < 0  &
		then exit
   If ll_row <> 0 then
	   MessageBox("Required Information","Required Value Missing for "+&
						 ls_colname +" on row " + string (ll_row)+ &
							'. Please enter a value.',stopsign! )
      // Make the column and row without a value current.
   	SetColumn(li_col)
	   SetRow(ll_row)
	   ScrollToRow(ll_row)
	   return false
   end if

// This row and column was ok, increment the column to check this 
// row in the next column.
	li_col++
LOOP
return true
end function

public function boolean uf_loaddwsyntax ();///////////////////////////////////////////////////////////////////////
//
//	Function:	uf_LoaddwSyntax
//
//	Purpose:		Loads the saved datawindow syntax from the storage PBL.
//
//	Arguments:	(None)
//
//	Returns:		boolean	True	- The syntax was saved successfully
//								False - There was a problem saving the syntax
//
////////////////////////////////////////////////////////////////////////
string	ls_Syntax, &
			ls_Error


// Get the syntax for the datawindow from the PBL
ls_Syntax = LibraryExport( gw_base_frame.is_DWSyntaxPBL, &
				This.DataObject, ExportDatawindow! )

// If there were no errors then create the create the datwindow
IF ls_Syntax > "" THEN
	This.Create(ls_Syntax, ls_Error)
	IF ls_Error > "" THEN
		MessageBox( "Datawindow Creation Error", ls_Error )
		Return FALSE
	ELSE
		Return TRUE
	END IF
ELSE
	// Unable to locate the datawindow syntax.
	Return FALSE
END IF
end function

public function integer uf_modified ();////////////////////////////////////////////////////////////////////////
//		
//	Function:	uf_modified()
//
//	Purpose:	Checks to see if the datawindow has been modified
//
//	Arguments: None 
//
//	Returns:	AcceptText Failure		= -1
//				DataWindow Modified		=  1
//				DataWindow Not Modified =  0
//
//	Documentation: Anil Peggerla		9/07/95
//////////////////////////////////////////////////////////////////////////

string	ls_DataObject

// Get dataobject (Usefull while stepping through Debugger)
ls_DataObject = This.DataObject

// If the datawindow does not have update capability or it has no
// update table do not check to see if it has been modified (who cares)
//If	This.ib_updateable = FALSE OR &
//		This.Describe("DataWindow.Table.UpdateTable") = "?" Then
//	Return 0
//End If
// nEXT LINE CHANGE
If	This.ib_updateable = FALSE THEN	Return 0

If This.AcceptText() = -1 Then
	Return -1
ElseIf (This.ModifiedCount() + This.DeletedCount()) > 0 Then
	Return 1
Else
	Return 0
End If

end function

public subroutine uf_preview_mode ();/////////////////////////////////////////////////////////////// 
//
//    Function: uf_preview_mode		 Scope: Public
//
//     Purpose: Changes the value of the text object on
// 				 the datawindow named "Profile"
//
//   Arguments: None
//     Returns: None
//            *********** Version  Log ************* 
//
//   DATE         WHO        WHAT
//  --------     -----      ------------------------------------ 
//   3/29/94   Powersoft     Initial Version
//	  9/07/95	Anil Pegerla  Documentation
//
//////////////////////////////////////////////////////////////// 

OpenWithParm(w_printzoom, this)
end subroutine

public subroutine uf_resize_control ();/////////////////////////////////////////////////////////////// 
//
//    Function: uf_resize_control	 Scope: Public
//
//     Purpose: Changes the the Control to View All Contents
//					of the DataObject
//
//   Arguments: None
//
//     Returns: None
//
//            *********** Version  Log ************* 
//
//   Anil Peggerla 	Documentation		9/07/95
//	  11/20/97	Tim Bornholtz	Removed reference to iw_frame.iu_string
//////////////////////////////////////////////////////////////////////////

string	ls_obj_list[]
int		li_new_dw_height, li_zcount, i, li_new_y, li_new_height
int 		li_new_dw_width, li_new_x, li_new_width
window 	lw_parent
u_string_functions	lu_string

lw_parent = parent

// parse the object list ...
li_zcount = lu_string.nf_parseobjstring(this, ls_obj_list, "*", "*")

// find the lowest object in the DW ...
li_new_dw_height = &
	Integer(Describe(ls_obj_list[1]+".y")) + &
	Integer(Describe(ls_obj_list[1]+".height")) 
li_new_dw_width = &
    Integer(Describe(ls_obj_list[1]+".x")) + &
    Integer(Describe(ls_obj_list[1]+".width")) 
	
FOR i = 2 to li_zcount
if Describe(ls_obj_list[i]+".visible") = "1" then
	li_new_y = Integer(Describe(ls_obj_list[i]+".y"))
	li_new_height =  Integer(Describe(ls_obj_list[i]+".height"))
	IF li_new_y + li_new_height > li_new_dw_height THEN
		li_new_dw_height = li_new_y + li_new_height
	END IF

	li_new_x = Integer(Describe(ls_obj_list[i]+".x"))
	li_new_width =  Integer(Describe(ls_obj_list[i]+".width"))
	IF li_new_x + li_new_width > li_new_dw_width THEN
		li_new_dw_width = li_new_x + li_new_width
	END IF
End If
NEXT	

width = li_new_dw_width + 100

//make the window wider if necessary ...
	IF width >= iw_parent.width THEN
		iw_parent.width = width +100
	END IF

// 
height = li_new_dw_height + 100

// make the window taller if necessary ...
	//IF height >= iw_winparent.height THEN
	//	iw_parent.height = height +200
	//END IF

//center the DW control on the screen ...
	x = (iw_parent.width/2 - width/2)







end subroutine

public subroutine uf_resize_height ();/////////////////////////////////////////////////////////////// 
//
//    Function: uf_resize_height	 Scope: Public
//
//     Purpose: Changes the height of the the datawindow
//	             
//		Arguments: None
//   
//     Returns: None
//
//            *********** Version  Log ************* 
//
//   DATE         WHO        WHAT
//  --------     -----      ------------------------------------ 
//   3/29/94   Powersoft     Initial Version
//	  9/07/95   Anil Peggerla Documentation
//	  11/20/97	Tim Bornholtz	Removed reference to iw_frame.iu_string
//////////////////////////////////////////////////////////////// 
string	ls_obj_list[]
int		li_new_dw_height, li_zcount, i, li_new_y, li_new_height
u_string_functions	lu_string

// parse the object list ...
li_zcount = lu_string.nf_parseobjstring(this, ls_obj_list, "*", "*")

// find the lowest object in the DW ...
li_new_dw_height = &
    Integer(Describe(ls_obj_list[1]+".y")) + &
    Integer(Describe(ls_obj_list[1]+".height")) 

FOR i = 2 to li_zcount
if Describe(ls_obj_list[i]+".visible") = "1" then
	li_new_y = Integer(Describe(ls_obj_list[i]+".y"))
	li_new_height =  Integer(Describe(ls_obj_list[i]+".height"))
	IF li_new_y + li_new_height > li_new_dw_height THEN
		li_new_dw_height = li_new_y + li_new_height
	END IF
End If
NEXT	

height = li_new_dw_height + 100

// make the window taller if necessary ...
//IF height >= iw_winparent.height THEN
	iw_parent.height = height +200
//END IF

//center the DW control on the screen ...
x = (iw_parent.width/2 - width/2)

end subroutine

public subroutine uf_resize_width ();/////////////////////////////////////////////////////////////// 
//
//    Function: uf_resize_height	 Scope: Public
//
//     Purpose: Changes the height of the the datawindow
//	             
//		Arguments: None
//   
//     Returns: None
//
//            *********** Version  Log ************* 
//
//   DATE         WHO        WHAT
//  --------     -----      ------------------------------------ 
//   3/29/94   Powersoft     Initial Version
//	  9/07/95   Anil Peggerla Documentation
//	  11/20/97	Tim Bornholtz	Removed reference to iw_frame.iu_string
//////////////////////////////////////////////////////////////// 
string	ls_obj_list[]
int		li_new_dw_height, li_zcount, i, li_new_y, li_new_height
u_string_functions	lu_string

// parse the object list ...
li_zcount = lu_string.nf_parseobjstring(this, ls_obj_list, "*", "*")

// find the lowest object in the DW ...
li_new_dw_height = &
    Integer(Describe(ls_obj_list[1]+".y")) + &
    Integer(Describe(ls_obj_list[1]+".height")) 

FOR i = 2 to li_zcount
if Describe(ls_obj_list[i]+".visible") = "1" then
	li_new_y = Integer(Describe(ls_obj_list[i]+".y"))
	li_new_height =  Integer(Describe(ls_obj_list[i]+".height"))
	IF li_new_y + li_new_height > li_new_dw_height THEN
		li_new_dw_height = li_new_y + li_new_height
	END IF
End If
NEXT	

height = li_new_dw_height + 100

// make the window taller if necessary ...
//IF height >= iw_winparent.height THEN
	iw_parent.height = height +200
//END IF

//center the DW control on the screen ...
x = (iw_parent.width/2 - width/2)

end subroutine

public subroutine uf_resize_wplace ();/////////////////////////////////////////////////////////////// 
//
//    Function: uf_resize_wplace		 Scope: Public
//
//     Purpose: Resizes parentwindow to size of workplace 
//
//   Arguments: None
//		
//     Returns: None
//
//            *********** Version  Log ************* 
//
//   DATE         WHO        WHAT
//  --------     -----      ------------------------------------ 
//   3/29/94   Powersoft     Initial Version
//	  9/07/95	Anil Peggerla Documentation
//////////////////////////////////////////////////////////////// 

Resize(iw_parent.WorkSpaceWidth (),iw_parent.WorkSpaceHeight() - this.y)

end subroutine

public function boolean uf_savedwsyntax ();///////////////////////////////////////////////////////////////////////
//
//	Function:	uf_SaveDWSyntax
//
//	Purpose:		Saves the datawindow syntax out to a storage PBL.
//					This enables the user to make modications to a datawindow
//					(ie move columns around in a grid dw) and then have
//					have those changes saved.
//
//	Arguments:	(None)
//
//	Returns:		boolean	True	- The syntax was saved successfully
//								False - There was a problem saving the syntax
//
//	9/07/94		Anil Peggerla		Documentation
////////////////////////////////////////////////////////////////////////

string	ls_Errors


// Check to see if the PBL that will be used to store the
// datawindow syntax actaully exists - if not create it!
IF NOT FileExists(gw_base_frame.is_DWSyntaxPBL ) THEN
	IF LibraryCreate(gw_base_frame.is_DWSyntaxPBL, "Default syntax save PBL.") < 0 THEN
		MessageBox( This.ClassName(), "Unable to create '" + &
						gw_base_frame.is_DWSyntaxPBL + &
						"' file used to store the datawindow syntax." )
		Return FALSE
	END IF
END IF

// Save the syntax out to the PBL
IF LibraryImport(gw_base_frame.is_DWSyntaxPBL, This.DataObject, &
	ImportDatawindow!, This.Describe( "Datawindow.Syntax" ), &
	ls_Errors ) < 0 THEN
	MessageBox( This.ClassName(), "Unable to save the datawindow syntax.")
	Return FALSE
ELSE
	Return TRUE
END IF

Return FALSE
end function

public subroutine uf_settabkeyzero ();/////////////////////////////////////////////////////////////// 
//
//    Function: uf_settabkeyzero		 Scope: Public
//
//     Purpose: Changes all columns designated as key columns
//	             to have a tab value of zero.
//
//   Arguments: None
//
//     Returns: None
//
//            *********** Version  Log ************* 
//
//   DATE         WHO        	WHAT
//  --------     -----      	------------------------------------ 
//   3/29/94   Powersoft     	Initial Version
//	  9/07/95	Anil Peggerla 	Documentation
//	  11/20/97	Tim Bornholtz	Removed reference to iw_frame.iu_string
//////////////////////////////////////////////////////////////// 

string ls_obj_list [], ls_key
int li_zcount, i
u_string_functions	lu_string

//parse the object list ...
li_zcount = lu_string.nf_parseobjstring ( this, ls_obj_list, "column", "*" )


FOR i = 1 to li_zcount
	ls_key = this.Describe ( ls_obj_list [i] + ".key" )
	if ls_key = "yes" then
		this.settaborder ( ls_obj_list [i], 0 )
	end if
NEXT	

end subroutine

public function boolean uf_undo_edit_control ();string ls_type
string ls_null

SetNull(ls_null)

ls_type =  describe(getcolumnname() + ".coltype")

IF ls_type = "!" THEN 
	Messagebox('uo_dw.uf_undo_edit_control', &
	'Unable to identify column '+ getcolumnname(), Exclamation!)
	return False
END IF 

IF mid(ls_type, 5, 1) = "(" THEN ls_type = mid(ls_type, 1, 4)
	Choose Case ls_type
		Case "char" 
			If isnull(GetItemString(getrow(), getcolumnname())) then
				SetText("")
			Else
				SetText(GetItemString( getrow(), getcolumnname()))
			End if
		
		Case "date" 
			if isnull(GetItemDate(getrow(), getcolumnname())) then
				 SetText(ls_null)
			Else
				SetText(string(GetItemDate(getrow(), getcolumnname())))
			End if
		
		Case "datetime" , "timestamp" 
			if isnull(GetItemDatetime( getrow(), getcolumnname())) then
				 SetText(ls_null)
			Else
				SetText(string( GetItemDatetime( getrow(), getcolumnname())))
			End if
			 
		Case "time" 
			if isnull(GetItemTime( getrow(), getcolumnname())) then
				 SetText(ls_null)
			Else
				SetText(string( GetItemTime( getrow(), getcolumnname())))
			End if
			 
		Case "number" 
			if isnull(GetItemNumber( getrow(), getcolumnname())) then
				 SetText('')
			Else
				SetText(string( GetItemNumber( getrow(), getcolumnname())))		
			End if

		Case "decimal"
			if isnull(GetItemDecimal( getrow(), getcolumnname())) then
				 SetText(ls_null)
			Else
				SetText(string( GetItemDecimal( getrow(), getcolumnname())))		
			End if

		Case ELSE
			MessageBox('Undo Incomplete', &
					'Undefined column type: '+ls_type, Exclamation!)
		End Choose
	
return true
end function

public function boolean uf_required (boolean ab_required);// loop through the visible columns and make them required or not

Int	li_Col_count, &
		li_counter

String	ls_ColNames[], &
			ls_edit, &
			ls_required


If ab_required Then
	ls_required = 'Yes'
Else
	ls_required = 'No'
End if

li_Col_count = uf_dw_GetVisibleColumns(This, ls_ColNames)

for li_counter = 1 to li_Col_count
	ls_edit = This.Describe(ls_ColNames[li_counter] + '.Edit.Style')
	If This.Modify(ls_ColNames[li_counter] + '.' + ls_edit + &
							".Required = " + ls_required) <> "" Then
		MessageBox("u_base_dw.uf_required", "Unable to modify column " + &
					ls_ColNames[li_counter] + " to " + ls_required)
		return false
	End if
Next

return True
end function

public function integer uf_dw_getvisiblecolumns (powerobject adw_arg, ref string as_colnames[]);/*
purpose:	get all VISIBLE column names in a datawindow

args:	ldw_arg				powerobject		... ldw_arg or ldwc_arg who's column names you want
		colnames		string array	... array passed by reference to be filled with column names

returns:				int				... number of columns found
*/

string 				ls_dummy_cols[]

int 					li_i , &
						li_totcols, &
						li_colcount

datawindow			ldw_arg

datawindowchild	ldwc_arg

CHOOSE CASE  TypeOf(adw_arg)
	CASE datawindow!
		ldw_arg = adw_arg
	CASE datawindowchild!
		ldwc_arg = adw_arg
	CASE ELSE
		MessageBox("Error","1st arg to f_dw_getvisiblecolumns must be a datawindow or a datawindowchild!",stopsign!)
		RETURN -1
END CHOOSE

IF IsValid(ldw_arg) THEN
	li_totcols = Integer(ldw_arg.Describe("datawindow.column.count"))
ELSE
	li_totcols = Integer(ldwc_arg.Describe("datawindow.column.count"))
END IF

//	re-initialize array before we begin ...
as_colnames = ls_dummy_cols

IF IsValid(ldw_arg) THEN
	FOR li_i = 1 to li_totcols
		IF ldw_arg.Describe("#"+String(li_i)+".visible") = "1" THEN
			li_colcount ++
			as_colnames[li_colcount] = ldw_arg.Describe("#"+String(li_i)+".name")
		END IF
	NEXT
ELSE
	FOR li_i = 1 to li_totcols
		IF ldwc_arg.Describe("#"+String(li_i)+".visible") = "1" THEN
			li_colcount ++
			as_colnames[li_colcount] = ldwc_arg.Describe("#"+String(li_i)+".name")
		END IF
	NEXT
END IF

RETURN li_colcount


end function

event clicked;integer	li_idx

// Get the name of object at pointer that the user clicked on
is_ObjectAtPointer = This.GetObjectAtPointer( )

// Get the name of band at pointer that the user clicked in
is_BandAtPointer = This.GetBandAtPointer( )

// Verify if a valid row was clicked on
If row < 1 Then
	Return
End If

Choose Case is_selection
	Case '0'
		Return
	Case '1'
		If row = il_dwrow Then
			If Not IsSelected(row) Then SelectRow(row, True)
		Else
			SetRow(row)
		End if
	Case '2'
		If IsSelected(row) Then
			SelectRow(row, FALSE)
		Else
			SelectRow(row, TRUE)
		End If
	Case '3'
		If KeyDown(KeyShift!) Then
			SetRedraw(FALSE)
			SelectRow(0, FALSE)

			If il_last_clicked_row > row Then
				For li_idx = il_last_clicked_row To row STEP -1
					SelectRow(li_idx, TRUE)	
				End For
			Elseif il_last_clicked_row = 0 Then
				SelectRow(row, TRUE)	
			Else
				For li_idx = il_last_clicked_row To row
					SelectRow(li_idx, TRUE)	
				Next	
			End If
			SetRedraw(TRUE)
			Return
		End If
		If Keydown(KeyControl!) Then
			If This.IsSelected(row) Then
				SelectRow(row, FALSE)
			Else
				SelectRow(row, TRUE)
			End If
		Else
			SelectRow(0, FALSE)
			SelectRow(row, TRUE)
		End If

		il_last_clicked_row = row
	Case '4'
		If KeyDown(KeyShift!) Then
			SetRedraw(FALSE)
			SelectRow(0, FALSE)

			If il_last_clicked_row > row Then
				For li_idx = il_last_clicked_row To row STEP -1
					SelectRow(li_idx, TRUE)	
				End For
			Elseif il_last_clicked_row = 0 Then
				SelectRow(row, TRUE)	
			Else
				For li_idx = il_last_clicked_row To row
					SelectRow(li_idx, TRUE)	
				Next	
			End If
			SetRedraw(TRUE)
			Return
		End If
			If IsSelected(row) Then
			SelectRow(row, FALSE)
		Else
			SelectRow(row, TRUE)
		End If
		il_last_clicked_row = row
End Choose

end event

event destructor;integer	li_save_columnlayouts

IF IsValid(gw_base_frame.iu_base_data) THEN
	gw_base_frame.iu_base_data.Trigger Event ue_get_dwsettings(li_save_columnlayouts)
END IF

IF IsValid(inv_find) THEN 	Destroy	inv_find

// Saves the datawindow syntax (only saves grid datawindow when the
// the save option is set).
IF li_save_columnlayouts <> 0 and &
	Long(This.Describe("datawindow.processing")) = 1 THEN
	uf_SaveDWSyntax()
End If
end event

event constructor;graphicobject	lgr_object, &
					lgr_tab
					



// Get the pointer to the parent window of this UO
lgr_object	=	Parent
CHOOSE CASE TypeOf(lgr_object) 
	CASE userobject!
		lgr_tab	=	lgr_object.GetParent()	
		IF TypeOf(lgr_tab) = tab! THEN
			iw_parent	=	lgr_tab.GetParent()
		ELSE
			iw_parent	=	lgr_object.GetParent()
		END IF
	CASE window!
		iw_parent	=	Parent
END CHOOSE

This.Postevent("ue_postconstructor")



end event

event doubleclicked;// If the double clicked field is a date or datetime, open w_Calendar
// to allow date editing.

long			ll_ClickedColumn
string		ls_ColumnType, &
				ls_date, &
				ls_protected, &
				ls_ColName
str_parms	lstr_parms


// Make sure a valid row/column was clicked
IF String(dwo.Type) = "column" THEN
	ls_ColName = String(dwo.Name)
	ll_ClickedColumn = Long(dwo.ID)
END IF
IF Row < 1 OR ll_ClickedColumn < 1 THEN Return

If This.Describe("DataWindow.ReadOnly") = 'yes' Then return

// Get the column type of the clicked field
ls_ColumnType = Lower(This.Describe("#" + String( &
						ll_ClickedColumn) + ".ColType"))

ls_protected = This.Describe("#" + String(ll_ClickedColumn) + ".Protect")

If Not IsNumber(ls_protected) Then
	// then it is an expression, evaluate it
	// first char is default value, then tab, then expression
	// the whole thing is surrounded by quotes
	ls_protected = This.Describe('Evaluate("' + Mid(ls_protected, &
									Pos(ls_protected, '~t') + 1, &
									Len(ls_protected) - Pos(ls_protected, '~t') - 1) + &
									'", ' + String(Row) + ')')
End if

// Only display the clock if the column has edit capablilities
If This.Describe("#" + String(ll_ClickedColumn) + ".Edit.DisplayOnly") = "yes" OR &
     This.Describe("#" + String(ll_ClickedColumn) + ".DDDW.AllowEdit") = "no" OR &
	  This.Describe("#" + String(ll_ClickedColumn) + ".DDLB.AllowEdit") = "no" OR &
	  This.Describe("#" + String(ll_ClickedColumn) + ".TabSequence") = "0" OR &
	  ls_protected = '1' Then
	Return
End If

// Get the X and Y coordinates of the place that was clicked
lstr_parms.integer_arg[1] = gw_base_Frame.PointerX() + gw_base_Frame.WorkSpaceX() - 50
CHOOSE CASE	lstr_parms.integer_arg[1]
CASE IS > 2253
	lstr_parms.integer_arg[1] = 2253
CASE is < 113
	lstr_parms.integer_arg[1] = 113
END CHOOSE

lstr_parms.integer_arg[2] = gw_base_Frame.PointerY() + gw_base_Frame.WorkSpaceY() - 500
CHOOSE CASE	lstr_parms.integer_arg[2]
CASE IS > 1053
	lstr_parms.integer_arg[2] = 1053
CASE is < 0
	lstr_parms.integer_arg[2] = 0
END CHOOSE

Choose Case ls_ColumnType
	Case "date"
		lstr_parms.date_arg[1] = This.GetItemDate(Row, ls_ColName)
		OpenWithParm(w_Calendar, lstr_parms)
		// Get the return string (date) from the calendar window
		// If an empty string do not do anything
		ls_date = Message.StringParm
		If ls_date <> "" Then
			SetText(ls_Date)
		Else
			Return
		End If
	Case "datetime"
		lstr_parms.date_arg[1] = Date(This.GetItemDateTime(Row, ls_ColName))
		OpenWithParm(w_Calendar, lstr_parms)
		// Get the return string (date) from the calendar window
		// If an empty string do not do anything
		ls_date = Message.StringParm
		If ls_date <> "" Then
			SetText(ls_Date)
		Else
			Return
		End If
End Choose

end event

event itemerror;// If the data in the column is NULL or empty string
// allow the user to change columns
If TRIM(This.GetText()) = "" Then
	RETURN 3
End If


end event

event itemchanged;// Get the current column name
IF  IsValid(dwo) THEN is_ColumnName = String(dwo.Name)
end event

on rbuttondown;// Get the name of object at pointer that the user clicked on
is_ObjectAtPointer = This.GetObjectAtPointer( )

// Get the name of band at pointer that the user clicked in
is_BandAtPointer = This.GetBandAtPointer( )

end on

event rowfocuschanged;// SELECTION PROFILES
// 0 = No rows should be selected
// 1 = select only one row at a a time
// 2 = select multiple rows
// 3 = select multiple rows with support for Control and Shift keys  

// Update the il_dwrow with the current row
il_dwrow = currentrow

// Highlight the proper row(s) for is_selection = '1'
IF is_selection = "1" THEN
	SelectRow(0,False)
	SelectRow(il_dwrow, TRUE)
End IF

end event

event scrollvertical;//////////////////////////////////////////////////////////////////////
//
//	Event:			ScrollVertical
//	Description:	Post the event to make the top row in the datawindow
//						the current row if: ib_ScrollSetsCurrentRow = TRUE.
//
//	Developer		Date		Description
//	---------		----		-----------
//
//////////////////////////////////////////////////////////////////////

IF ib_ScrollSetsCurrentRow THEN
	This.PostEvent( "ue_MakeTopRowCurrent" )
END IF


end event

event itemfocuschanged;il_dwrow	=	row
end event

on u_base_dw.create
end on

on u_base_dw.destroy
end on

