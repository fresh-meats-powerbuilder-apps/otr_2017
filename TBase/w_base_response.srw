HA$PBExportHeader$w_base_response.srw
$PBExportComments$Base Response Window
forward
global type w_base_response from Window
end type
type cb_base_help from u_base_commandbutton within w_base_response
end type
type cb_base_cancel from u_base_commandbutton within w_base_response
end type
type cb_base_ok from u_base_commandbutton within w_base_response
end type
end forward

global type w_base_response from Window
int X=1075
int Y=485
int Width=2501
int Height=1493
boolean TitleBar=true
string Title="Untitled"
boolean ControlMenu=true
WindowType WindowType=response!
event ue_postopen ( unsignedlong wparam,  long lparam )
event ue_help ( unsignedlong wparam,  long lparam )
event ue_base_ok ( )
event ue_base_cancel ( )
event type string ue_get_data ( string as_value )
event ue_set_data ( string as_data_item,  string as_value )
cb_base_help cb_base_help
cb_base_cancel cb_base_cancel
cb_base_ok cb_base_ok
end type
global w_base_response w_base_response

event ue_help;gw_base_frame.wf_context(this.title)

end event

event open;// Post an event that handles post-opening processing
PostEvent("ue_postopen")

end event

on w_base_response.create
this.cb_base_help=create cb_base_help
this.cb_base_cancel=create cb_base_cancel
this.cb_base_ok=create cb_base_ok
this.Control[]={ this.cb_base_help,&
this.cb_base_cancel,&
this.cb_base_ok}
end on

on w_base_response.destroy
destroy(this.cb_base_help)
destroy(this.cb_base_cancel)
destroy(this.cb_base_ok)
end on

type cb_base_help from u_base_commandbutton within w_base_response
int X=334
int Y=341
int Width=270
int TabOrder=3
string Text="&Help"
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
end type

event clicked;call super::clicked;Parent.TriggerEvent ("ue_help")
end event

type cb_base_cancel from u_base_commandbutton within w_base_response
int X=334
int Y=217
int Width=270
int TabOrder=20
string Text="&Cancel"
boolean Cancel=true
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
end type

event clicked;call super::clicked;Parent.TriggerEvent ("ue_base_cancel")
end event

type cb_base_ok from u_base_commandbutton within w_base_response
int X=334
int Y=93
int Width=270
int TabOrder=10
string Text="&OK"
boolean Default=true
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
end type

event clicked;call super::clicked;Parent.TriggerEvent ("ue_base_ok")
end event

