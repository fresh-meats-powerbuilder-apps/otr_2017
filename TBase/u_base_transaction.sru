HA$PBExportHeader$u_base_transaction.sru
$PBExportComments$Base Transaction Object
forward
global type u_base_transaction from transaction
end type
end forward

global type u_base_transaction from transaction
end type
global u_base_transaction u_base_transaction

type prototypes


end prototypes

type variables


end variables

forward prototypes
public subroutine nf_autocommit (boolean ab_switch)
public function boolean nf_checkreturn ()
public subroutine nf_getsqlcafromini (string as_inifile)
public function boolean nf_rollback ()
public function boolean nf_commit ()
public function boolean nf_connect ()
public function boolean nf_disconnect ()
public function integer nf_db_error (transaction atr_error_object, string as_error_msg)
end prototypes

public subroutine nf_autocommit (boolean ab_switch);///////////////////////////////////////////////////////////////////////
//
//	Function:	nvof_AutoCommit
//
//	Purpose:		Set the AutoCommit Attribute of the Transaction to the 
// 				Passed Parameter
//
//	Arguments:	boolean - New Value of AutoCommit
//
////////////////////////////////////////////////////////////////////////

This.AutoCommit = ab_switch


end subroutine

public function boolean nf_checkreturn ();///////////////////////////////////////////////////////////////////////
//
//	Function:	nf_CheckReturn
//
//	Purpose:		Checks the return SQLCode from the current 
//					transaction object.	
//
//	Arguments:	(None)
//
//	Returns:		boolean	True	- Transaction was successful
//								False - Transaction was NOT successful
//
// Modifications: 8/1/97 - Remove Reference to SQLCA and replaced it with This
//                         IBDKTJB
//
////////////////////////////////////////////////////////////////////////

IF This.SQLCode < 0 THEN
	nf_db_error(This, "Failure on the current transaction.")
	Return FALSE
ELSE
	Return TRUE
END IF

end function

public subroutine nf_getsqlcafromini (string as_inifile);///////////////////////////////////////////////////////////////////////
//
//	Function:	nvof_GetSQLCAFromIni
//
//	Purpose:		Gets the transaction object attributes from the
//					passed application INI file.
//
//	Arguments:	string	as_IniFile
//
//	Returns:		boolean	(None)
//
////////////////////////////////////////////////////////////////////////

SetPointer(HourGlass!)

	This.dbms =  &
		ProfileString(as_inifile, "Primary Database", "dbms", "")

	This.database = &
		ProfileString(as_inifile, "Primary Database", "database", "")

	This.logid    = &
		ProfileString(as_inifile, "Primary Database", "logid", "")

	This.logpass  = &
		ProfileString(as_inifile, "Primary Database", "logpass", "")

	This.servername =  &
		ProfileString(as_inifile,"Primary Database", "servername", "")

	This.userid   = &
		ProfileString(as_inifile, "Primary Database", "userid", "")

	This.dbPass   = &
		ProfileString(as_inifile, "Primary Database", "dbpass", "")

	This.lock   = &
				ProfileString(as_inifile, "Primary Database", "lock", "")

	This.dbParm   = &
				ProfileString(as_inifile, "Primary Database", "dbParm", "")


SetPointer(Arrow!)
end subroutine

public function boolean nf_rollback ();///////////////////////////////////////////////////////////////////////
//
//	Function:	nf_Rollback
//
//	Purpose:		Roolbacks the changes to the database using the 
//					current transaction
//
//	Arguments:	(None)
//
//	Returns:		boolean	True	- Rollback was successful
//								False - Rollback was Unsuccessful
//
// Modifications: 8/1/97 - Remove Reference to SQLCA and replaced it with This
//                         IBDKTJB
//
////////////////////////////////////////////////////////////////////////


// Make sure there is an open transaction available to rollback
If This.DBHandle() = 0 Then
	Return FALSE
End If

ROLLBACK USING This ;

If This.nf_CheckReturn() Then
	Return TRUE
Else
	MessageBox("Rolling Back Transactions", "RollBack was not successful." + &
		"~r~n" +  "Please Re-Enter the Application.")
	Return FALSE
End If

end function

public function boolean nf_commit ();///////////////////////////////////////////////////////////////////////
//
//	Function:	nf_Commit
//
//	Purpose:		Commits changes to the database
//
//	Arguments:	(None)
//
//	Returns:		boolean	True	- Commit was successful
//								False - Commit was Unsuccessful
//
// Modifications: 8/1/97 - Remove Reference to SQLCA and replaced it with This
//                         IBDKTJB
//
////////////////////////////////////////////////////////////////////////


// Make sure there is an open transaction available to commit
If This.DBHandle()	=	0 Then
	Return FALSE
End If

// Commit the changes to the database
COMMIT USING This ;

// Check the SQLCode from the current transaction
If This.nf_CheckReturn() Then
	Return TRUE
Else
	This.Nf_Rollback()
	Return False
End If

end function

public function boolean nf_connect ();///////////////////////////////////////////////////////////////////////
//
//	Function:	nf_Connect
//
//	Purpose:		Connects to the database using the current
//					transaction attributes.
//
//	Arguments:	(None)
//
//	Returns:		boolean	True	- Connect was successful
//								False - Connect was Unsuccessful
//
// Modifications: 8/1/97 - Remove Reference to SQLCA and replaced it with This
//                         IBDKTJB
//
////////////////////////////////////////////////////////////////////////

SetPointer(HourGlass!)

Connect Using This;

If This.nf_CheckReturn() Then
	SetPointer(Arrow!)
	Return TRUE
Else
	SetPointer(Arrow!)
	Return FALSE
End If


end function

public function boolean nf_disconnect ();///////////////////////////////////////////////////////////////////////
//
//	Function:	nf_Disconnect
//
//	Purpose:		Disconnects from the database.
//
//	Arguments:	(None)
//
//	Returns:		boolean	True	- Disconnect was successful
//								False - Disconnect was Unsuccessful
//
// Modifications: 8/1/97 - Remove Reference to SQLCA and replaced it with This
//                         IBDKTJB
//
////////////////////////////////////////////////////////////////////////

Disconnect Using This;

If This.nf_CheckReturn() Then
	Return TRUE
Else
	Return FALSE
End If

end function

public function integer nf_db_error (transaction atr_error_object, string as_error_msg);///////////////////////////////////////////////////////////////////////////
//
// Function:	nf_db_error
//
//	Purpose:
//
// 	Function f_db_error is passed a transaction object (i.e. SQLCA). If
//		the transaction object's sqlcode identifies an error, the function
//		then displays a window containing an error message passed from the
//		application in addition to the error message pertaining to the
//		specific RDBMS currently in use.
// 
// 	The window also allows the user to print the message
//
//
// Arguments:
// 				atr_error_object 		: transaction object (i.e. SQLCA )
//					as_error_msg 	 		: string (application specific message)
//
// Returns : integer 
//
// Modifications: 8/1/97 - Open w_db_error as an instance
//                         IBDKTJB
//
/////////////////////////////////////////////////////////////////////////

string ls_string

Window	lw_DB_Error

u_conversion_functions	lu_conv
if atr_error_object.sqlcode = 0 then return 0

ls_string = as_error_msg + "~r~n~r~n"
ls_string = ls_string + "SQL Error Code~t: " + string(atr_error_object.sqlcode) + "~r~n"
ls_string = ls_string + "DB Error Code~t: " + string(atr_error_object.sqldbcode)+ "~r~n"
ls_string = ls_string + "DBMS~t~t: " + atr_error_object.dbms+ "~r~n"
ls_string = ls_string + "Database~t~t: " + atr_error_object.database+ "~r~n"
ls_string = ls_string + "User ID~t~t: " + atr_error_object.userid+ "~r~n"
ls_string = ls_string + "DBParm~t~t: " + atr_error_object.dbparm+ "~r~n"
ls_string = ls_string + "Login ID~t~t: " + atr_error_object.logid+ "~r~n"
ls_string = ls_string + "ServerName~t: " + atr_error_object.servername+ "~r~n"
ls_string = ls_string + "AutoCommit~t: " + lu_conv.nf_string(atr_error_object.autocommit)+ "~r~n"
ls_string = ls_string + "DB Error Message~t: " + atr_error_object.sqlerrtext + "~r~n"


OpenWithParm(lw_DB_Error, 'w_db_error', ls_string)

Return 1

		
end function

on u_base_transaction.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_base_transaction.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

