HA$PBExportHeader$u_base_multilineedit.sru
$PBExportComments$Base Standard Multiline edit
forward
global type u_base_multilineedit from multilineedit
end type
end forward

global type u_base_multilineedit from multilineedit
int Width=494
int Height=361
int TabOrder=1
BorderStyle BorderStyle=StyleLowered!
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
event ue_cut pbm_custom75
event ue_copy pbm_custom74
event ue_paste pbm_custom73
end type
global u_base_multilineedit u_base_multilineedit

event ue_cut;This.Cut()
end event

event ue_copy;This.Copy()
end event

event ue_paste;This.Paste()
end event

