HA$PBExportHeader$u_base_checkbox.sru
$PBExportComments$Base Standard Check Box
forward
global type u_base_checkbox from checkbox
end type
end forward

global type u_base_checkbox from checkbox
int Width=247
int Height=73
string Text="none"
BorderStyle BorderStyle=StyleLowered!
long TextColor=33554432
long BackColor=12632256
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global u_base_checkbox u_base_checkbox

