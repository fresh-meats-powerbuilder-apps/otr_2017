HA$PBExportHeader$w_base_frame.srw
$PBExportComments$Base MDI-Frame window
forward
global type w_base_frame from window
end type
type mdi_1 from mdiclient within w_base_frame
end type
end forward

global type w_base_frame from window
integer width = 4681
integer height = 3072
boolean titlebar = true
string title = "Base Frame Window"
string menuname = "m_base_menu"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
windowtype windowtype = mdihelp!
long backcolor = 268435456
event ue_postopen pbm_custom75
event ue_move pbm_move
event ue_reset_settings ( )
event ue_get_data ( string as_value )
event ue_set_data ( string as_data_item,  string as_value )
mdi_1 mdi_1
end type
global w_base_frame w_base_frame

type variables
// Application INI file name used to save user settings
// as_WorkingDir is the directory to put ALL files
string	is_ApplicationINI, &
	is_UserINI, &
	is_WorkingDir, &
	is_ipaddr

// Contains the name of the pbl that we will use
// to store the user's modified grid datawindow syntax.
// Change the anme of this in the Open even of the
// w_base_frame_ext window if need be.
string	is_DWSyntaxPBL = "dwsyntax.pbl"

// Pointer to the application object
application	ia_Application

//This is set to on the cancel of base login to prevent 
// the effect of code in events posted in the que(postopen)
boolean    ib_login_cancel
// Holds the name of the 'section' in the application
// INI file where system settings will be stored
// (i.e. ToolBarText, ToolBarVisible, MultipleApps, etc...)
string	is_SystemSettings = "System Settings"

m_base_menu	im_base_menu

// This holds the string to the help file
String		is_help

u_base_data	iu_base_data

//boolean to check if user is exiting the app
boolean	ib_exit

//nonvisual with string functions
u_string_functions	iu_string
end variables

forward prototypes
public subroutine wf_savetoolbarsettings ()
public subroutine wf_settoolbar ()
public subroutine wf_setsize ()
public subroutine wf_savesize ()
public subroutine wf_context (string as_title)
public subroutine wf_login (string as_userini)
public subroutine wf_create_basedata ()
end prototypes

event ue_postopen;
// get the last used windows from ini file
This.im_base_menu.mf_getlastused()

// Open the login window and pass
// it the name of the Application INI file name
wf_Login(is_ApplicationINI)


end event

event ue_get_data;//Choose Case as_value
//End Choose
//
end event

event ue_set_data;//Choose Case as_data_item
//End Choose
//
end event

public subroutine wf_savetoolbarsettings ();//////////////////////////////////////////////////////////////////////
//
//	Function:	wf_SaveToolBarSettings( )
//
//	Description:	Saves the toolbar settings to the INI file
//
//	Arguments:		(None)
//
//	Returns:			(None)
//
/////////////////////////////////////////////////////////////////////

u_conversion_functions	lu_conv

// Save the ToolBarVisible attribute out to the INI file
SetProfileString(is_UserIni, is_SystemSettings, &
		"ToolBarVisible", String(lu_conv.nf_Integer(This.ToolBarVisible)))

// Save the ToolBarText attribute out to the INI file
SetProfileString(is_UserIni, is_SystemSettings, &
		"ToolBarText", String(lu_conv.nf_Integer(ia_Application.ToolBarText)))

// Save the ToolBarTips attribute out to the INI file
SetProfileString(is_UserIni, is_SystemSettings, &
		"ToolBarTips", String(lu_conv.nf_Integer(ia_Application.ToolBarTips)))

// Save the ToolBarAlignment attribute out to the INI file
Choose Case This.ToolBarAlignment

	Case AlignAtBottom!
		SetProfileString(is_UserIni, is_SystemSettings, &
				"ToolBarAlignMent", "AlignAtBottom!")
	Case AlignAtLeft!
		SetProfileString(is_UserIni, is_SystemSettings, &
				"ToolBarAlignMent", "AlignAtLeft!")
	Case AlignAtRight!
		SetProfileString(is_UserIni, is_SystemSettings, &
				"ToolBarAlignMent", "AlignAtRight!")
	Case AlignAtTop!
		SetProfileString(is_UserIni, is_SystemSettings, &
				"ToolBarAlignMent", "AlignAtTop!")
	Case Floating!
		SetProfileString(is_UserIni, is_SystemSettings, &
				"ToolBarAlignMent", "Floating!")
End Choose
end subroutine

public subroutine wf_settoolbar ();u_conversion_functions	lu_conv


// Get the value from the INI file for the ToolBarVisible attribute
This.ToolBarVisible = &
	lu_conv.nf_Boolean(ProfileString(is_UserIni, is_SystemSettings, "ToolBarVisible", "1"))

// Get the value from the INI file for the ToolBarText attribute
ia_Application.ToolBarText = &
	lu_conv.nf_Boolean(ProfileString(is_UserIni, is_SystemSettings, "ToolBarText", "0"))

// Get the value from the INI file for the ToolBarVisible attribute
ia_Application.ToolBarTips = &
		lu_conv.nf_Boolean(ProfileString(is_UserIni, is_SystemSettings, "ToolBarTips", "1"))

// Get the value from the INI file for the ToolBarAlignment attribute
Choose Case Lower(ProfileString(is_UserIni, is_SystemSettings, &
			"ToolBarAlignMent", "AlignAtTop!"))
	Case "alignatbottom!"
		This.ToolBarAlignMent = AlignAtBottom!
	Case "alignatleft!"
		This.ToolBarAlignMent = AlignAtLeft!
	Case "alignatright!"
		This.ToolBarAlignMent = AlignAtRight!
	Case "alignattop!"
		This.ToolBarAlignMent = AlignAtTop!
	Case "floating!"
		This.ToolBarAlignMent = Floating!
End Choose

end subroutine

public subroutine wf_setsize ();Long	ll_x, &
		ll_y, &
		ll_height, &
		ll_width

Environment		le_env

WindowState		lws_state


GetEnvironment(le_env)

Choose Case ProfileString(is_UserIni, is_SystemSettings, "WindowState", "Normal!")
Case "Maximized!"
	lws_state = Maximized!
Case "Minimized!"
	lws_state = Minimized!
Case Else
	lws_state = Normal!
End Choose

This.WindowState = lws_state

ll_x = Long(ProfileString(is_UserIni, is_SystemSettings, "X", "1"))
ll_y = Long(ProfileString(is_UserIni, is_SystemSettings, "Y", "1"))

ll_width = Long(ProfileString(is_UserIni, is_SystemSettings, "Width", &
					String(PixelsToUnits(le_env.ScreenWidth, XPixelsToUnits!))))
ll_height = Long(ProfileString(is_UserIni, is_SystemSettings, "Height", &
					String(PixelsToUnits(le_env.ScreenHeight, YPixelsToUnits!))))

This.Move(ll_x, ll_y)
This.Resize(ll_Width, ll_Height)

return
end subroutine

public subroutine wf_savesize ();String	ls_state


Choose Case This.WindowState
Case Maximized!
	ls_state = "Maximized!"
Case Minimized!
	ls_state = "Minimized!"
Case Normal!
	ls_state = "Normal!"
End Choose

SetProfileString(is_UserIni, is_SystemSettings, "WindowState", ls_state)

If ls_state = "Normal!" Then
	// If it is minimized or maximized, dont change settings
	SetProfileString(is_UserIni, is_SystemSettings, "X", String(This.X))
	SetProfileString(is_UserIni, is_SystemSettings, "Y", String(This.Y))
	SetProfileString(is_UserIni, is_SystemSettings, "Width", String(This.Width))
	SetProfileString(is_UserIni, is_SystemSettings, "Height", String(This.Height))
End if
return
end subroutine

public subroutine wf_context (string as_title);string ls_title
Window	lw_Active_Sheet


lw_Active_Sheet = This.GetActiveSheet()

If trim(as_title) = "" Then
	If IsValid(lw_active_sheet) Then
	   ls_title = lw_active_sheet.title
	Else
		ls_title = ""
	End if
Else
   ls_title = as_title
End if

If Pos(ls_title,"-") > 0 Then
	ls_title = Left(ls_title,Pos(ls_title,"-") - 1)
End If

If ls_title = "" Then
   Showhelp(is_help, Index!)
Else
   Showhelp(is_help, keyword!, ls_title)
End if

end subroutine

public subroutine wf_login (string as_userini);OpenWithParm(w_base_login, as_UserIni)



end subroutine

public subroutine wf_create_basedata ();iu_base_data	=	Create	u_base_data
end subroutine

event open;Long 							ll_Prev_Inst

u_sdkcalls					lu_sdkcalls

String						ls_app_user_ini

Integer						li_pos


// Point to the corect frame for all base functionality
gw_base_frame = This

// Get the name of the passed Application INI file name (if any)
ls_app_user_ini = Message.StringParm
li_pos = Pos(ls_app_user_ini, ",")
If li_pos > 0 Then
	is_applicationini = Trim(Left(ls_app_user_ini, (li_pos - 1)))
	is_userini = Trim(Right(ls_app_user_ini, (Len(ls_app_user_ini) - li_pos)))
Else
	is_applicationIni = ls_app_user_ini
	is_userini = ''
End if

IF IsNull(is_userini) or Trim(is_userini) = "" THEN is_userini = "IBPUSER.INI"

//this.menuId.m_open.text = ""
this.im_base_menu = this.menuID

// Get Absolute path to ibpuser.ini file
lu_sdkcalls = Create u_sdkcalls
lu_sdkcalls.nf_GetUserIniPath(is_userini, is_workingdir)

// Get a pointer to the application Object
ia_Application = GetApplication()

ll_Prev_Inst = Handle( ia_Application ,True)
IF ll_Prev_Inst > 0 Then
	This.Title = This.Title + "[Multiple Versions Running]"
	IF Len(Trim(Message.nf_get_app_id())) > 0 Then
		IF Not(lu_sdkcalls.nf_setactiveapp(Message.nf_get_app_id ( ), is_userini)) Then
			MessageBox("Application Already Running",  &
			ia_Application.AppName + " is already running."  &
			+ " You cannot start it again.")
		END IF 
	ELSE
		MessageBox("Application Already Running",  &
		ia_Application.AppName + " is already running."  &
		+ " You cannot start it again.")
	END IF
	HALT CLOSE
END IF
Destroy lu_sdkcalls

// Call a function that will set the tool bar settings to what was
// saved in the user's INI file
This.wf_SetToolBar()
This.wf_SetSize()

//Create the nonvisual and get all the settings
wf_create_basedata()
// Post an event to finish the rest of the Open script code
PostEvent("ue_postopen")

end event

event close;// Call a function that will save all of the toolbar
// settings out to the INI file
This.wf_SaveToolBarSettings( )
This.wf_SaveSize()


end event

on w_base_frame.create
if this.MenuName = "m_base_menu" then this.MenuID = create m_base_menu
this.mdi_1=create mdi_1
this.Control[]={this.mdi_1}
end on

on w_base_frame.destroy
if IsValid(MenuID) then destroy(MenuID)
destroy(this.mdi_1)
end on

event closequery;// Declare variable for active sheet
window		lw_activesheet, &
				lw_nextactivesheet

lw_activesheet = This.GetFirstSheet( )
Do while IsValid( lw_activesheet) 
	lw_nextactivesheet = This.GetNextSheet( lw_activeSheet)
	Close(lw_ActiveSheet)
	IF Message.ReturnValue <> 0 Then Return -1
	lw_activesheet = lw_nextactivesheet
Loop



end event

type mdi_1 from mdiclient within w_base_frame
long BackColor=268435456
end type

