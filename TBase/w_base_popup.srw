HA$PBExportHeader$w_base_popup.srw
$PBExportComments$Base Popup Window
forward
global type w_base_popup from Window
end type
end forward

global type w_base_popup from Window
int X=1079
int Y=485
int Width=2524
int Height=1509
boolean TitleBar=true
string Title="Untitled"
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
boolean Resizable=true
WindowType WindowType=popup!
event ue_postopen pbm_custom75
event ue_help pbm_custom74
end type
global w_base_popup w_base_popup

event ue_help;gw_base_frame.wf_context(this.title)
end event

on open;PostEvent("ue_postopen")
end on

on w_base_popup.create
end on

on w_base_popup.destroy
end on

