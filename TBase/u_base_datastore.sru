HA$PBExportHeader$u_base_datastore.sru
forward
global type u_base_datastore from datastore
end type
end forward

global type u_base_datastore from datastore
end type
global u_base_datastore u_base_datastore

type variables
window	iw_parent
end variables

forward prototypes
public subroutine nf_error_box (string as_title, string as_msg)
end prototypes

public subroutine nf_error_box (string as_title, string as_msg);///////////////////////////////////////////////////////////////////////////
//
// Function:	nf_error_box
//
//	Purpose:
//
//		Opens the non-modal window w_error_box to display
//		an error message when messagebox is not appropriate.
//
// Scope:		public
//
// Parameters:
//					as_title			: string
//					as_msg 	 		: string
//
// Returns : None
//
//	DATE			NAME		REVISION
// ----		------------------------------------------------------------
// Powersoft Corporation	INITIAL VERSION
//
//////////////////////////////////////////////////////////////////////
// not open yet
if handle(w_error_box) = 0 then
	open(w_error_box)
	w_error_box.title 		 = as_title
	w_error_box.mle_msg.text = as_msg
else
// it's already been opened by some other error so append the error 
// message
	w_error_box.mle_msg.text = w_error_box.mle_msg.text + "~r~n~n" + as_msg
end if

end subroutine

on u_base_datastore.create
call datastore::create
TriggerEvent( this, "constructor" )
end on

on u_base_datastore.destroy
call datastore::destroy
TriggerEvent( this, "destructor" )
end on

event constructor;//graphicobject	lgr_object, &
//					lgr_tab
//					
//
//// Get the pointer to the parent window of this UO
//lgr_object	=	Parent
//CHOOSE CASE TypeOf(lgr_object) 
//	CASE userobject!
//		lgr_tab	=	lgr_object.GetParent()	
//		IF TypeOf(lgr_tab) = tab! THEN
//			iw_parent	=	lgr_tab.GetParent()
//		ELSE
//			iw_parent	=	lgr_object.GetParent()
//		END IF
//	CASE window!
//		iw_parent	=	Parent
//END CHOOSE
//
//This.Postevent("ue_postconstructor")
//
//
//
//// Save the initial SQL Select
//This.SetTransObject(SQLCA)
////is_InitialSQLStatement = This.Describe("Datawindow.Table.SQLSelect")
end event

event dberror;/////////////////////////////////////////////////////////////////////////////////////
//		Event:	dberror
//
//	Purpose:		determine the action that has caused the error, build 
//					a nice error message, open a popup window ( not a 
//					response because we do not want to delay the rollback that
//					should occur in the script that called the update function)
//					to display the message, position the user to the offending row,
//					tell the datawindow to stop processing and then exit.
//
/////////////////////////////////////////////////////////////////////////////////////

dwItemStatus	le_dwStatus
string			ls_err_type, &
					ls_err_msg



// Find out what type of DB action the user was trying to perform
Choose Case buffer
	Case delete!
		ls_err_type = "Deleting"
	Case primary!
		le_dwStatus = this.GetItemStatus(row, 0, buffer)
		If le_dwStatus = new! or le_dwStatus = newmodified! Then   // it's an insert
			ls_err_type = "Inserting"
		Else
			ls_err_type = "Updating"
		End If
End Choose

// Build the error message to be displayed to the user
ls_err_msg =	"Error while " + ls_err_type + " row " + String(row) + "." + "~r~n" + &
					"DB Error Code:~t" + String(sqldbcode) + "~r~n" + &
					"DB Error Message:~t" + sqlerrtext

// Call a function that opens a window and displays the error message
	nf_error_box ('Update Error', ls_err_msg )



// Do not display the default dbError Message
Return 1
end event

