HA$PBExportHeader$w_base_login.srw
$PBExportComments$Base Login Window (Response)
forward
global type w_base_login from w_base_response
end type
type st_1 from statictext within w_base_login
end type
type st_username from statictext within w_base_login
end type
type sle_username from singlelineedit within w_base_login
end type
type st_password from statictext within w_base_login
end type
type sle_password from singlelineedit within w_base_login
end type
type st_3 from statictext within w_base_login
end type
type gb_1 from groupbox within w_base_login
end type
end forward

shared variables

end variables

global type w_base_login from w_base_response
int X=298
int Y=453
int Width=1509
int Height=965
boolean TitleBar=true
string Title="Base Login"
long BackColor=12632256
st_1 st_1
st_username st_username
sle_username sle_username
st_password st_password
sle_password sle_password
st_3 st_3
gb_1 gb_1
end type
global w_base_login w_base_login

type variables
// Holds the name of the passed Application INI file name
string	is_ApplicationINI

// Holds the number of login attempts that can be made
integer ii_attempts

// Passing structure
str_parms istr_parms

end variables

forward prototypes
public subroutine wf_login ()
end prototypes

public subroutine wf_login ();
end subroutine

on w_base_login.create
int iCurrent
call w_base_response::create
this.st_1=create st_1
this.st_username=create st_username
this.sle_username=create sle_username
this.st_password=create st_password
this.sle_password=create sle_password
this.st_3=create st_3
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
this.Control[iCurrent+2]=st_username
this.Control[iCurrent+3]=sle_username
this.Control[iCurrent+4]=st_password
this.Control[iCurrent+5]=sle_password
this.Control[iCurrent+6]=st_3
this.Control[iCurrent+7]=gb_1
end on

on w_base_login.destroy
call w_base_response::destroy
destroy(this.st_1)
destroy(this.st_username)
destroy(this.sle_username)
destroy(this.st_password)
destroy(this.sle_password)
destroy(this.st_3)
destroy(this.gb_1)
end on

event ue_base_cancel;call super::ue_base_cancel;/////////////////////////////////////////////////////////////////////////
//
// Event	 :  cb_base_cancel
//
// Purpose:	The event passes a boolean of  "false" back 
//			and closes this response window.
//
// Log:
// 
// DATE		NAME				REVISION
//------		-------------------------------------------------------------
// Powersoft Corporation	INITIAL VERSION
//
/////////////////////////////////////////////////////////////////////////
gw_base_frame.iu_base_data	=	Create u_base_data
gw_base_frame.iu_base_data.ii_prompt_on_exit	=	0
gw_base_frame.ib_login_cancel = True
gw_base_frame.im_base_menu.m_file.m_exit.PostEvent(clicked!)

end event

event ue_base_ok;call super::ue_base_ok;wf_login()
end event

type cb_base_help from w_base_response`cb_base_help within w_base_login
int X=1153
int Y=329
int TabOrder=10
boolean Visible=false
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_base_login
int X=778
int Y=637
int Width=417
int Height=101
int TabOrder=40
end type

type cb_base_ok from w_base_response`cb_base_ok within w_base_login
int X=197
int Y=637
int Width=417
int Height=101
int TabOrder=34
end type

type st_1 from statictext within w_base_login
int X=147
int Y=29
int Width=1249
int Height=173
boolean Enabled=false
string Text="Base Login"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-28
int Weight=700
string FaceName="Bodoni Book"
FontFamily FontFamily=Roman!
FontPitch FontPitch=Variable!
end type

type st_username from statictext within w_base_login
int X=14
int Y=293
int Width=517
int Height=85
string Text="&User Name:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_username from singlelineedit within w_base_login
event leftmousebuttondown pbm_lbuttondown
int X=558
int Y=293
int Width=563
int Height=81
int TabOrder=30
BorderStyle BorderStyle=StyleLowered!
int Accelerator=117
string Pointer="arrow!"
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on leftmousebuttondown;selecttext(this, 1, len(this.text))
end on

on getfocus;selecttext(this, 1, len(this.text))
end on

type st_password from statictext within w_base_login
int X=14
int Y=433
int Width=517
int Height=85
string Text="User &Password:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;setfocus ( sle_password )
end on

type sle_password from singlelineedit within w_base_login
event leftmousebuttondown pbm_lbuttondown
int X=558
int Y=421
int Width=563
int Height=81
int TabOrder=33
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
boolean PassWord=true
int Accelerator=112
string Pointer="arrow!"
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on leftmousebuttondown;selecttext(this, 1, len(this.text))
end on

on getfocus;selecttext(this, 1, len(this.text))
end on

type st_3 from statictext within w_base_login
int X=275
int Y=781
int Width=805
int Height=53
boolean Enabled=false
string Text="XYZ Company Inc. 1995"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-10
int Weight=400
string FaceName="Monotype Corsiva"
FontFamily FontFamily=Script!
FontPitch FontPitch=Variable!
end type

type gb_1 from groupbox within w_base_login
int X=69
int Y=581
int Width=1267
int Height=181
BorderStyle BorderStyle=StyleLowered!
long TextColor=33554432
long BackColor=12632256
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

