HA$PBExportHeader$u_base_dropdownlistbox.sru
$PBExportComments$Base Standard Drop Down List Box
forward
global type u_base_dropdownlistbox from dropdownlistbox
end type
end forward

global type u_base_dropdownlistbox from dropdownlistbox
int Width=247
int Height=217
int TabOrder=1
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
long TextColor=33554432
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global u_base_dropdownlistbox u_base_dropdownlistbox

