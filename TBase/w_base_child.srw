HA$PBExportHeader$w_base_child.srw
$PBExportComments$Base Child Window
forward
global type w_base_child from Window
end type
end forward

global type w_base_child from Window
int X=1079
int Y=485
int Width=2524
int Height=1509
boolean TitleBar=true
string Title="Untitled"
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
boolean Resizable=true
WindowType WindowType=child!
event ue_postopen pbm_custom75
event ue_help pbm_custom74
event ue_get_data ( string as_value )
event ue_set_data ( string as_data_item,  string as_value )
end type
global w_base_child w_base_child

event ue_help;gw_base_frame.wf_context(this.title)
end event

on open;PostEvent("ue_postopen")
end on

on w_base_child.create
end on

on w_base_child.destroy
end on

