HA$PBExportHeader$u_base_editmask.sru
$PBExportComments$Base Standard Edit Mask
forward
global type u_base_editmask from editmask
end type
end forward

global type u_base_editmask from editmask
int Width=247
int Height=101
int TabOrder=1
BorderStyle BorderStyle=StyleLowered!
long TextColor=33554432
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global u_base_editmask u_base_editmask

