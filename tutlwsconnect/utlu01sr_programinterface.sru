HA$PBExportHeader$utlu01sr_programinterface.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type utlu01sr_ProgramInterface from nonvisualobject
    end type
end forward

global type utlu01sr_ProgramInterface from nonvisualobject
end type

type variables
    utlu01sr_ProgramInterfaceUtlu01ci utlu01ci
    utlu01sr_ProgramInterfaceUtlu01pg utlu01pg
    utlu01sr_ProgramInterfaceUtlu01in utlu01in
    utlu01sr_ProgramInterfaceUtlu01ot utlu01ot
    boolean channel
end variables

on utlu01sr_ProgramInterface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on utlu01sr_ProgramInterface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

