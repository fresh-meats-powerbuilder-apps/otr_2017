HA$PBExportHeader$utlu01sr_programinterface1.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type utlu01sr_ProgramInterface1 from nonvisualobject
    end type
end forward

global type utlu01sr_ProgramInterface1 from nonvisualobject
end type

type variables
    utlu01sr_ProgramInterfaceUtlu01ci1 utlu01ci
    utlu01sr_ProgramInterfaceUtlu01pg1 utlu01pg
    utlu01sr_ProgramInterfaceUtlu01in1 utlu01in
    utlu01sr_ProgramInterfaceUtlu01ot1 utlu01ot
    boolean channel
end variables

on utlu01sr_ProgramInterface1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on utlu01sr_ProgramInterface1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

