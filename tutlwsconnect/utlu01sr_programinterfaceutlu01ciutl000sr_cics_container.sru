HA$PBExportHeader$utlu01sr_programinterfaceutlu01ciutl000sr_cics_container.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type utlu01sr_ProgramInterfaceUtlu01ciUtl000sr_cics_container from nonvisualobject
    end type
end forward

global type utlu01sr_ProgramInterfaceUtlu01ciUtl000sr_cics_container from nonvisualobject
end type

type variables
    string utl000sr_req_tranid
    string utl000sr_req_program
    string utl000sr_req_userid
    string utl000sr_req_password
end variables

on utlu01sr_ProgramInterfaceUtlu01ciUtl000sr_cics_container.create
call super::create
TriggerEvent( this, "constructor" )
end on

on utlu01sr_ProgramInterfaceUtlu01ciUtl000sr_cics_container.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

