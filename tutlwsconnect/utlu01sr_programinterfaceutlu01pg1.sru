HA$PBExportHeader$utlu01sr_programinterfaceutlu01pg1.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type utlu01sr_ProgramInterfaceUtlu01pg1 from nonvisualobject
    end type
end forward

global type utlu01sr_ProgramInterfaceUtlu01pg1 from nonvisualobject
end type

type variables
    utlu01sr_ProgramInterfaceUtlu01pgUtl000sr_program_container1 utl000sr_program_container
    boolean structuredContainer
end variables

on utlu01sr_ProgramInterfaceUtlu01pg1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on utlu01sr_ProgramInterfaceUtlu01pg1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

