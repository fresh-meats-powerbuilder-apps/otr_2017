HA$PBExportHeader$w_receive_hrs.srw
forward
global type w_receive_hrs from w_base_sheet_ext
end type
type dw_1 from u_base_dw_ext within w_receive_hrs
end type
type dw_2 from u_base_dw_ext within w_receive_hrs
end type
end forward

global type w_receive_hrs from w_base_sheet_ext
integer width = 2747
integer height = 1504
string title = "Receiving Hours"
long backcolor = 12632256
dw_1 dw_1
dw_2 dw_2
end type
global w_receive_hrs w_receive_hrs

type variables
character		ic_control

String		is_closemessage_txt, &
             	 	is_customer_id   

Integer		ii_rc

DataWindowChild	idwc_division, &
		idwc_address_code
                    
u_otr003		iu_otr003

u_ws_customer	iu_ws_customer

u_receive_hrs	ui_receive_hrs

end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function integer wf_changed ()
public function boolean wf_pre_save ()
public function boolean wf_organize_hrs (string as_day, long al_row)
private function integer wf_load_receive_hrs ()
end prototypes

public function boolean wf_retrieve ();// 01-15-1997 ibdkkam  Updated for PB v5.0.02.  Modifications:
// 1) Changes to reference nvo instance variables instead of 
// structure variables.  2) Modified comments, inserted blank 
// lines for increased readability.

Integer li_answer, li_rc, li_close_rtn
li_rc = wf_changed( )

CHOOSE CASE li_rc
	CASE  1	//	Valid change
		
		li_answer = MessageBox("Wait",is_closemessage_txt,Question!,YesNoCancel!)
		
		CHOOSE CASE li_answer
				
			CASE 1 // Yes, save changes
				IF wf_Update() THEN // Successful save, do nothing
				ELSE // Save failed
					li_answer = MessageBox("Save Failed","Retrieve Anyway?",Question!,YesNo!)
					IF li_answer = 1 THEN // Retrieve w/o saving changes
					ELSE // Cancel close
						Return( False )
					END IF
				END IF
				
			CASE 2 	//	No, retrieve w/o saving changes
				
			CASE 3	//	Cancel retrieve
				Return( False )
				
		END CHOOSE
		
	CASE -1 // Validation error from wf_changed( )
		li_answer = MessageBox("Data Entry Error","Retrieve w/o Save?",Question!,OkCancel!)
		CHOOSE CASE li_answer
				
			CASE 1	//	OK, retrieve w/o saving changes
			CASE 2	//	Cancel retrieve
				Return( False )
				
		END CHOOSE
		
	CASE  -2 //	Serious validation error from wf_changed, window might not close in this state
		MessageBox("A Serious Error Has Occurred","Correct Error Before Retrieving!",exclamation!,ok!)
		Return(False)
	
	CASE ELSE // Nothing changed
		
END CHOOSE

IF Trim(is_customer_id) = ""  or IsNull(is_customer_id) Then
	
   OpenWithParm( w_receive_hrs_filter, ui_receive_hrs, iw_frame )
   ui_receive_hrs  = Message.PowerObjectParm
	
	If isvalid(ui_receive_hrs) = False then
		Return True
	End If
	
   IF ui_receive_hrs.cancel = TRUE  Then
    	Return True
   End If
ELSE
	
   ui_receive_hrs.customer_id = is_customer_id 
End If

SetPointer(HourGlass!)
This.SetRedraw(FALSE)
// Populate datawindows
This.wf_load_receive_hrs ( )

// Turn the redraw off for the window while we retrieve
This.SetRedraw(TRUE)
SetPointer(Arrow!)

Return FALSE

 





end function

public function boolean wf_update ();// Returns FALSE immediately if wf_pre_save returns FALSE (i.e., there
// is invalid data on the window.
// ibdkcjr added all variables that end with a 2
Boolean    lb_ret

integer    li_rtn, &
           li_rc

long		  ll_Row,&
			  ll_RowCount	

String   	ls_customer_id, &
            ls_req_customer_id, &
	         ls_division_code, &	
		      ls_mon_from_hrs, &	 
            ls_mon_to_hrs, &	 
            ls_tue_from_hrs, &	 
            ls_tue_to_hrs, &	 
            ls_wed_from_hrs, &	 
            ls_wed_to_hrs, &	 
            ls_thur_from_hrs, &	 
            ls_thur_to_hrs, &
            ls_fri_from_hrs, &	 
            ls_fri_to_hrs, &	 
            ls_sat_from_hrs, &	 
            ls_sat_to_hrs, &
            ls_sun_from_hrs, &	 
            ls_sun_to_hrs, &
				ls_mon_from_hrs2, &	 
            ls_mon_to_hrs2, &	 
            ls_tue_from_hrs2, &	 
            ls_tue_to_hrs2, &	 
            ls_wed_from_hrs2, &	 
            ls_wed_to_hrs2, &	 
            ls_thur_from_hrs2, &	 
            ls_thur_to_hrs2, &
            ls_fri_from_hrs2, &	 
            ls_fri_to_hrs2, &	 
            ls_sat_from_hrs2, &	 
            ls_sat_to_hrs2, &
            ls_sun_from_hrs2, &	 
            ls_sun_to_hrs2, &
            ls_recv_phone, &
            ls_cust_contact, &
            ls_recv_type, &
				ls_frt_forwarder

Time       lt_work_time
						
s_error    lstr_Error_Info

dwItemStatus		status

IF dw_1.AcceptText() = -1 and dw_2.AcceptText() = -1 THEN 
     Return(False)
END IF

ll_RowCount = dw_1.RowCount()
 
lstr_error_info.se_event_name = "wf_update"
lstr_error_info.se_window_name = "w_receive_hrs"
lstr_error_info.se_procedure_name = "wf_update"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

lb_ret = wf_pre_save()

If lb_ret = FALSE Then
   Return False
End If

If dw_1.AcceptText() = 1 Then 
 DO WHILE ll_Row < ll_RowCount 
	ll_Row = dw_1.GetNextModified( ll_Row, Primary! )
	IF ll_Row > 0  THEN 
		ls_customer_id	    = dw_1.GetItemString( ll_Row, "customer" )
		ls_division_code	 = dw_1.GetItemString( ll_row, "division_code")
      ls_cust_contact	 = dw_1.GetItemString( ll_row, "cust_contact")
      ls_recv_phone   	 = dw_1.GetItemString( ll_row, "phone")
		
      lt_work_time       = dw_1.GetItemTime( ll_Row, "mon_from_hours")
      ls_mon_from_hrs	 = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "mon_to_hours")
      ls_mon_to_hrs	    = String(lt_work_time, "hhmm")
  
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "tue_from_hours")
      ls_tue_from_hrs	 = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "tue_to_hours")
      ls_tue_to_hrs	    = String(lt_work_time, "hhmm")
  
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "wed_from_hours")
      ls_wed_from_hrs	 = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "wed_to_hours")
      ls_wed_to_hrs	    = String(lt_work_time, "hhmm")

		lt_work_time       = dw_1.GetItemTime( ll_Row, "thur_from_hours")
      ls_thur_from_hrs   = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "thur_to_hours")
      ls_thur_to_hrs	    = String(lt_work_time, "hhmm")
  
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "fri_from_hours")
      ls_fri_from_hrs	 = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "fri_to_hours")
      ls_fri_to_hrs	    = String(lt_work_time, "hhmm")

	   lt_work_time       = dw_1.GetItemTime( ll_Row, "sat_from_hours")
      ls_sat_from_hrs    = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "sat_to_hours")
      ls_sat_to_hrs	    = String(lt_work_time, "hhmm")
  
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "sun_from_hours")
      ls_sun_from_hrs	 = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "sun_to_hours")
      ls_sun_to_hrs	    = String(lt_work_time, "hhmm")
   
      lt_work_time       = dw_1.GetItemTime( ll_Row, "mon_from_hours2")
      ls_mon_from_hrs2	 = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "mon_to_hours2")
      ls_mon_to_hrs2	    = String(lt_work_time, "hhmm")
  
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "tue_from_hours2")
      ls_tue_from_hrs2	 = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "tue_to_hours2")
      ls_tue_to_hrs2	    = String(lt_work_time, "hhmm")
  
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "wed_from_hours2")
      ls_wed_from_hrs2	 = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "wed_to_hours2")
      ls_wed_to_hrs2	    = String(lt_work_time, "hhmm")

		lt_work_time       = dw_1.GetItemTime( ll_Row, "thur_from_hours2")
      ls_thur_from_hrs2  = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "thur_to_hours2")
      ls_thur_to_hrs2	   = String(lt_work_time, "hhmm")
  
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "fri_from_hours2")
      ls_fri_from_hrs2	 = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "fri_to_hours2")
      ls_fri_to_hrs2	    = String(lt_work_time, "hhmm")

	   lt_work_time       = dw_1.GetItemTime( ll_Row, "sat_from_hours2")
      ls_sat_from_hrs2   = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "sat_to_hours2")
      ls_sat_to_hrs2	    = String(lt_work_time, "hhmm")
  
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "sun_from_hours2")
      ls_sun_from_hrs2	 = String(lt_work_time, "hhmm")
	   lt_work_time       = dw_1.GetItemTime( ll_Row, "sun_to_hours2")
      ls_sun_to_hrs2	    = String(lt_work_time, "hhmm")
/*		
      li_rtn = iu_otr003.nf_cfmc30br(ls_customer_id, ls_division_code, &
         ls_recv_phone, ls_cust_contact, ls_mon_from_hrs, ls_mon_to_hrs, ls_tue_from_hrs, &
         ls_tue_to_hrs, ls_wed_from_hrs, ls_wed_to_hrs, ls_thur_from_hrs, &  
         ls_thur_to_hrs, ls_fri_from_hrs, ls_fri_to_hrs, ls_sat_from_hrs, &
         ls_sat_to_hrs, ls_sun_from_hrs, ls_sun_to_hrs, ls_mon_from_hrs2, ls_mon_to_hrs2, ls_tue_from_hrs2, &
         ls_tue_to_hrs2, ls_wed_from_hrs2, ls_wed_to_hrs2, ls_thur_from_hrs2, &  
         ls_thur_to_hrs2, ls_fri_from_hrs2, ls_fri_to_hrs2, ls_sat_from_hrs2, &		
         ls_sat_to_hrs2, ls_sun_from_hrs2, ls_sun_to_hrs2, lstr_error_info, 0)
*/

li_rtn = iu_ws_customer.uf_cfmc30er(ls_customer_id, ls_division_code, &
         ls_recv_phone, ls_cust_contact, ls_mon_from_hrs, ls_mon_to_hrs, ls_tue_from_hrs, &
         ls_tue_to_hrs, ls_wed_from_hrs, ls_wed_to_hrs, ls_thur_from_hrs, &  
         ls_thur_to_hrs, ls_fri_from_hrs, ls_fri_to_hrs, ls_sat_from_hrs, &
         ls_sat_to_hrs, ls_sun_from_hrs, ls_sun_to_hrs, ls_mon_from_hrs2, ls_mon_to_hrs2, ls_tue_from_hrs2, &
         ls_tue_to_hrs2, ls_wed_from_hrs2, ls_wed_to_hrs2, ls_thur_from_hrs2, &  
         ls_thur_to_hrs2, ls_fri_from_hrs2, ls_fri_to_hrs2, ls_sat_from_hrs2, &		
         ls_sat_to_hrs2, ls_sun_from_hrs2, ls_sun_to_hrs2, lstr_error_info)
	
		dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
		CHOOSE CASE li_rtn
			CASE is < 0 
				dw_1.Setredraw(TRUE)
				li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
										ls_customer_id + " has failed due to an Error.  Do you want to Continue?", &
										Question!, YesNo! )
				dw_1.Setredraw(FALSE)
			CASE is > 0 
				dw_1.Setredraw(TRUE)
				li_rc = MessageBox( "Update Warning", lstr_Error_Info.se_message + &
										" Customer Id = " +ls_customer_id, &
										Information!, OK! )
				dw_1.Setredraw(FALSE)
			CASE ELSE
				li_rc = 0
		END CHOOSE

		IF li_rc = 2 THEN 			// NO  do not continue processing
			dw_1.Setredraw(TRUE)
			Return(False)
		END IF
	else
		dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
		ll_Row = ll_RowCount + 1
	END IF
 LOOP
End if


If dw_2.AcceptText() = 1 Then
   ll_Row = 0
   ll_Row = dw_2.GetNextModified( ll_Row, Primary! )
	IF ll_Row > 0  THEN 

		ls_req_customer_id	 = dw_1.GetItemString( 1, "customer" )
      ls_recv_type          = dw_2.GetItemString( 1, "recv_type")
		ls_frt_forwarder      = dw_2.GetItemString( 1, "frt_forwarder")
/*       
      li_rtn = iu_otr003.nf_otrt35ar(ls_req_customer_id, &
         ls_recv_type, ls_frt_forwarder,  lstr_error_info, 0)
*/
	 li_rtn = iu_ws_customer.uf_otrt35er(ls_req_customer_id, &
         ls_recv_type, ls_frt_forwarder,  lstr_error_info)
			
		dw_2.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
		CHOOSE CASE li_rtn
			CASE is < 0 
				dw_2.Setredraw(TRUE)
				li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
										ls_req_customer_id + " has failed due to an Error.  Do you want to Continue?", &
										Question!, YesNo! )
				dw_2.Setredraw(FALSE)
			CASE is > 0 
				dw_2.Setredraw(TRUE)
				li_rc = MessageBox( "Update Warning", lstr_Error_Info.se_message + &
										" Customer Id = " +ls_req_customer_id, &
										Information!, OK! )
				dw_2.Setredraw(FALSE)
			CASE ELSE
				li_rc = 0
		END CHOOSE

		IF li_rc = 2 THEN 			// NO  do not continue processing
			dw_2.Setredraw(TRUE)
			Return(False)
		END IF
  End if
End if	

dw_1.setredraw(true)
dw_2.SetRedraw(True)

Return(True)
end function

public function integer wf_changed ();/*
	purpose:	this function will examine all datawindow controls on the window
				that have update capability against the database.  if anything has changed,
				and the change is valid, a 1 is returned.  if an invalid change has occurred
				then a -1 is returned.  if nothing has changed a 0 is returned.

*/

IF dw_1.ModifiedCount() + &
	dw_1.DeletedCount() + &
   dw_2.ModifiedCount() + &
	dw_2.DeletedCount() > 0 THEN
	is_closemessage_txt = "Save Changes?"
	RETURN 1
END IF

// nothing has been changed
RETURN 0
	
end function

public function boolean wf_pre_save ();// 01-23-1997  ibdkkam  Modified to return False if division code
// is not listed in the dddw.

long     ll_FoundRow, &
			ll_Row, &
         ll_RowCount, &
         ll_RowNumber

string   ls_division, &
         ls_contact  

time     lt_from_work_time, &
         lt_to_work_time  

ll_RowCount = dw_1.RowCount()
ll_Row = 0
ll_RowNumber = 0

dw_1.AcceptText()
 
DO WHILE ll_RowNumber <= ll_RowCount
	
	ll_Row = dw_1.GetNextModified( ll_RowNumber, Primary! )
	IF ll_Row > 0  THEN
		
      ls_division  =   dw_1.GetItemString(ll_Row, "division_code")
		ll_FoundRow = idwc_division.Find ( "type_code='"+ls_division+"'", 1, idwc_division.RowCount() )
      If ( ls_division = "  " ) OR ( IsNull( ls_division ) ) OR &
			( ll_FoundRow < 1 ) Then
			
			MessageBox( "Division Code Error", ls_division + " is Not a Valid Division Code." )
	   	dw_1.SetColumn( "division_code" )
			dw_1.SelectText( 1, Len( ls_division ) )
			Return ( False )		     
      End if

      ls_contact  =   dw_1.GetItemString(ll_Row, "cust_contact")
      If ls_contact = "          " or IsNull(ls_contact) Then 
			
          MessageBox("Customer Contact Error", "Customer must have a customer contact entered") 
          dw_1.SetColumn("cust_contact")
          Return FALSE   
			 
      End if

      lt_to_work_time   = dw_1.GetItemtime(ll_Row, "mon_to_hours")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "mon_from_hours")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <=  lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("mon_to_hours")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("mon_to_hours")
            Return FALSE        
         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("mon_from_hours")
              Return FALSE     
          End if
      End if
		
		lt_to_work_time   = dw_1.GetItemtime(ll_Row, "mon_to_hours2")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "mon_from_hours2")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <=  lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("mon_to_hours2")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("mon_to_hours2")
            Return FALSE        
         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("mon_from_hours2")
              Return FALSE     
          End if
      End if
		
		wf_organize_hrs("mon", ll_row)
   
      lt_to_work_time   = dw_1.GetItemtime(ll_Row, "tue_to_hours")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "tue_from_hours")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <=  lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("tue_to_hours")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("tue_to_hours")
            Return FALSE        
         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("tue_from_hours")
              Return FALSE     
          End if
      End if
   
	 	lt_to_work_time   = dw_1.GetItemtime(ll_Row, "tue_to_hours2")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "tue_from_hours2")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <=  lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("tue_to_hours2")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("tue_to_hours2")
            Return FALSE        
         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("tue_from_hours2")
              Return FALSE     
          End if
      End if
		
		wf_organize_hrs("tue", ll_row)
		
      lt_to_work_time   = dw_1.GetItemtime(ll_Row, "wed_to_hours")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "wed_from_hours")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <=  lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("wed_to_hours")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("wed_to_hours")
            Return FALSE        

         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("wed_from_hours")
              Return FALSE     
          End if
      End if
		
		lt_to_work_time   = dw_1.GetItemtime(ll_Row, "wed_to_hours2")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "wed_from_hours2")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <= lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("wed_to_hours2")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("wed_to_hours2")
            Return FALSE        
         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("wed_from_hours2")
              Return FALSE     
          End if
      End if
   	
		wf_organize_hrs("wed", ll_row)
	
      lt_to_work_time   = dw_1.GetItemtime(ll_Row, "thur_to_hours")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "thur_from_hours")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <=  lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("thur_to_hours")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("thur_to_hours")
            Return FALSE        
         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("thur_from_hours")
              Return FALSE     
          End if
      End if
		
		lt_to_work_time   = dw_1.GetItemtime(ll_Row, "thur_to_hours2")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "thur_from_hours2")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <=  lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("thur_to_hours2")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("thur_to_hours2")
            Return FALSE        
         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("thur_from_hours2")
              Return FALSE     
          End if
      End if
		
		wf_organize_hrs("thur", ll_row)
      
      lt_to_work_time   = dw_1.GetItemtime(ll_Row, "fri_to_hours")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "fri_from_hours")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <=  lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("fri_to_hours")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("fri_to_hours")
            Return FALSE        
         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("fri_from_hours")
              Return FALSE     
          End if
      End if
		
		lt_to_work_time   = dw_1.GetItemtime(ll_Row, "fri_to_hours2")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "fri_from_hours2")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <=  lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("fri_to_hours2")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("fri_to_hours2")
            Return FALSE        
         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("fri_from_hours2")
              Return FALSE     
          End if
      End if
		
		wf_organize_hrs("fri", ll_row)
   
      lt_to_work_time   = dw_1.GetItemtime(ll_Row, "sat_to_hours")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "sat_from_hours")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <=  lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("sat_to_hours")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("sat_to_hours")
            Return FALSE        
         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("sat_from_hours")
              Return FALSE     
          End if
      End if
		
		lt_to_work_time   = dw_1.GetItemtime(ll_Row, "sat_to_hours2")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "sat_from_hours2")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <=  lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("sat_to_hours2")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("sat_to_hours2")
            Return FALSE        
         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("sat_from_hours2")
              Return FALSE     
          End if
      End if
		
		wf_organize_hrs("sat", ll_row)
   
      lt_to_work_time   = dw_1.GetItemtime(ll_Row, "sun_to_hours")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "sun_from_hours")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <=  lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("sun_to_hours")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("sun_to_hours")
            Return FALSE        
         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("sun_from_hours")
              Return FALSE     
          End if
      End if
		
		lt_to_work_time   = dw_1.GetItemtime(ll_Row, "sun_to_hours2")
      lt_from_work_time = dw_1.GetItemTime(ll_Row, "sun_from_hours2")      
    
      If String(lt_from_work_time) <> "00:00:00" Then
         If String(lt_to_work_time) <> "00:00:00" Then
            If lt_to_work_time  <=  lt_from_work_time Then
               MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
               dw_1.SetColumn("sun_to_hours2")
               Return FALSE        
            End if
         Else
            MessageBox("Hour Error", "Customer Receiving To hours must be greater than the Customer Receiving From hours.") 
            dw_1.SetColumn("sun_to_hours2")
            Return FALSE        
         End if
      Else
          If String(lt_to_work_time) <> "00:00:00" Then
              MessageBox("Hour Error", "Customer Receiving From hours must be greater than zero.") 
              dw_1.SetColumn("sun_from_hours2")
              Return FALSE     
          End if
      End if
		
		wf_organize_hrs("sun", ll_row)

   End if 
   ll_RowNumber = ll_RowNumber + 1
LOOP

Return TRUE
end function

public function boolean wf_organize_hrs (string as_day, long al_row);time     lt_from_work_time2, &
         lt_to_work_time2, &
			lt_from_work_time, &
			lt_to_work_time
			
string	ls_from_hrs1, ls_to_hrs1, &
			ls_from_hrs2, ls_to_hrs2

ls_from_hrs1 = string(as_day + '_from_hours')
ls_to_hrs1 = string( as_day + '_to_hours')

ls_from_hrs2 = string(as_day + '_from_hours2')
ls_to_hrs2 = string( as_day + '_to_hours2')
			
lt_from_work_time = dw_1.GetItemtime(al_row, ls_from_hrs1)
lt_to_work_time = dw_1.GetItemtime(al_row, ls_to_hrs1)

lt_from_work_time2 = dw_1.GetItemtime(al_row, ls_from_hrs2)
lt_to_work_time2 = dw_1.GetItemtime(al_row, ls_to_hrs2)
			

if string(lt_from_work_time) <> "00:00:00" and &
	string(lt_to_work_time) <> "00:00:00" then
	if dw_1.GetItemtime(al_row, ls_from_hrs1) > dw_1.GetItemtime(al_row, ls_from_hrs2) and &
		dw_1.GetItemtime(al_row, ls_from_hrs2) <> time("00:00:00") then
		dw_1.setitem(al_row, ls_from_hrs2, dw_1.GetItemtime(al_row, ls_from_hrs1))
		dw_1.setitem(al_row, ls_to_hrs2, dw_1.GetItemtime(al_row, ls_to_hrs1))
		dw_1.setitem(al_row, ls_from_hrs1, lt_from_work_time2)
		dw_1.setitem(al_row, ls_to_hrs1, lt_to_work_time2)
	end if 	
	if dw_1.GetItemtime(al_row, ls_from_hrs2) >= dw_1.GetItemtime(al_row, ls_from_hrs1) and &
		dw_1.GetItemtime(al_row, ls_from_hrs2) <= dw_1.GetItemtime(al_row, ls_to_hrs1) then
		if dw_1.GetItemtime(al_row, ls_to_hrs2) >= dw_1.GetItemtime(al_row, ls_to_hrs1) then
			dw_1.setitem(al_row, ls_to_hrs1, dw_1.GetItemtime(al_row, ls_to_hrs2))
			dw_1.setitem(al_row, ls_from_hrs2, time("00:00:00"))
			dw_1.setitem(al_row, ls_to_hrs2, time("00:00:00"))
		else
			dw_1.setitem(al_row, ls_from_hrs2, time("00:00:00"))
			dw_1.setitem(al_row, ls_to_hrs2, time("00:00:00"))
		end if
	end if
else
	if lt_from_work_time2 <> time("00:00:00") or &
		lt_to_work_time2 <> time("00:00:00") then
		dw_1.setitem(al_row, ls_from_hrs2, dw_1.GetItemtime(al_row, ls_from_hrs1))
		dw_1.setitem(al_row, ls_to_hrs2, dw_1.GetItemtime(al_row, ls_to_hrs1))
		dw_1.setitem(al_row, ls_from_hrs1, lt_from_work_time2)
		dw_1.setitem(al_row, ls_to_hrs1, lt_to_work_time2)
	end if
end if

return true
end function

private function integer wf_load_receive_hrs ();// 01-15-1997 ibdkkam  Upgraded for PB v5.0.02.  Modified to
// 1) Check value returned from RPC via case stmt.  2) Reworked 
// do loop so that code is not duplicated.  3) Changes to reference
// nvo instance variables instead of structure variables.

Integer	   li_rtn

String		ls_receive_hrs_info, &
	         ls_req_customer_id, &
 				ls_req_division, &
				ls_refetch_division, &
				ls_header_hrs_info
		

s_error		lstr_Error_Info

Long			ll_row

dw_1.Reset( ) 
dw_2.Reset( ) 

SetPointer( HourGlass! )

ls_req_customer_id = ui_receive_hrs.customer_id
If Len(ui_receive_hrs.division) = 0 Then 
	ls_req_division = Space(2)
Else 
	ls_req_division	 = ui_receive_hrs.division
End If

// Assign for RPC error checking
lstr_error_info.se_event_name = "wf_load_receive_hrs"
lstr_error_info.se_window_name = "w_receive_hrs"
lstr_error_info.se_procedure_name = "wf_load_receive_hrs"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)
 
ls_refetch_division  = Space(2)

Do
	ls_receive_hrs_info = Space( 5000 )
	ls_header_hrs_info   = Space(50)
	
//	li_rtn = iu_otr003.nf_cfmc29br(ls_req_customer_id, &
//         ls_req_division, ls_receive_hrs_info, ls_header_hrs_info, &
//         ls_refetch_division, &
//			lstr_error_info, 0)

	li_rtn = iu_ws_customer.uf_cfmc29er(ls_req_customer_id, &
         ls_req_division, ls_receive_hrs_info, ls_header_hrs_info, lstr_error_info) 			
			
	Choose Case li_rtn
		Case 0
			iw_frame.SetMicroHelp( "Ready...")
		Case 1
			iw_frame.SetMicroHelp(lstr_error_info.se_message)
		Case Else
			iw_frame.SetMicroHelp(lstr_error_info.se_message)
			return(-1)
	End Choose
		
	li_rtn = dw_1.ImportString(trim(ls_receive_hrs_info))
	
	
Loop While ( Len( Trim( ls_refetch_division ) ) > 0 )

li_rtn = dw_2.ImportString(Trim(ls_header_hrs_info))
dw_1.InsertRow(0)
dw_1.ScrollToRow(1) 
dw_1.SetColumn("mon_from_hours")

dw_1.SetItem(dw_1.rowcount(), "customer", ls_req_customer_id)
dw_1.SetItemStatus (dw_1.RowCount(), 0, Primary!, NotModified! )
//dw_2.SetItemStatus ( 1, 0, Primary!, NotModified! )

//st_cust_id.Text = ls_req_customer_id

dw_1.ResetUpdate() 
dw_2.ResetUpdate() 

Return(1)      
end function

event ue_postopen;call super::ue_postopen;iu_otr003  =  CREATE u_otr003

iu_ws_customer  =  CREATE u_ws_customer

is_customer_id = " "
is_customer_id = Message.StringParm

IF wf_retrieve() THEN Close( this )

end event

event open;call super::open;
dw_2.InsertRow(0)
Return 0

 
end event

event close;call super::close;DESTROY u_otr003

Destroy u_ws_customer

end event

on w_receive_hrs.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.dw_2=create dw_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.dw_2
end on

on w_receive_hrs.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.dw_2)
end on

event closequery;
//////////////////////////////////////////////////////////////////////
//		
//	Event:	Closequery
//
//	Purpose:	Before closing the window, check to see if any changes
//				have been made to any of the datawindows.  If so, prompt
//				the user to save the data. The window function wf_modified()
//				is used to determine if modifications have been made to any
//				of the datawindows on the sheet.
//
//////////////////////////////////////////////////////////////////////

integer	li_prompt_onsave

//gets the settings values
gw_base_frame.iu_base_data.Trigger Event ue_get_sheetsettings(li_prompt_onsave)
IF gw_base_frame.ib_exit and li_prompt_onsave = 0 THEN RETURN

// Do a security check for save and do nothing if the user does not have priviliges
IF NOT gw_base_frame.im_base_menu.m_file.m_save.enabled THEN RETURN

CHOOSE CASE This.wf_modified( )

	CASE 1
		// Changes were made to one of the datawindows
			CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)

				CASE 1	// Save Changes
					If This.wf_update() = FALSE Then
						Message.ReturnValue = 1	 // Update failed - do not close window
					Else
						Return
					End If

				CASE 2	// Do not save changes

				CASE 3	// Cancel the closing of window
					Message.ReturnValue = 1
					Return
			END CHOOSE

	CASE 0
		// No modifications were made to any datawindows on this sheet...closing of sheet allowed

	CASE -1
		// Problem with the AcceptText() function against a datawindow.
		// The item error will handle setting focus to the correct column. 
//		if ui_receive_hrs.cancel then
//			return
//		else
//			Message.ReturnValue = 1
//		end if
		Return

END CHOOSE

end event

type dw_1 from u_base_dw_ext within w_receive_hrs
integer x = 18
integer y = 184
integer width = 2679
integer height = 1192
integer taborder = 10
string dataobject = "d_receive_hrs"
boolean vscrollbar = true
end type

on ue_postconstructor;call u_base_dw_ext::ue_postconstructor;ib_updateable = TRUE

end on

event itemchanged;call super::itemchanged;// 01-21-1997 ibdkkam  Modified to reference "type_code" in data
// window child.  Highlights division code data when an invalid
// division code is entered.

long    ll_FoundRow, ll_rowcount, ll_count
integer li_status

IF Trim(data) = "" THEN Return

If ( dw_1.GetColumnName() = "division_code" ) Then
	
	ll_FoundRow = idwc_division.Find ( "type_code='"+data+"'", 1, idwc_division.RowCount() )
   If ll_FoundRow < 1 Then // No rows found, or error
   	MessageBox( "Division Code Error", data + " is Not a Valid Division Code." )
	   li_status = dw_1.SetColumn( "division_code" )
		li_status = dw_1.SelectText( 1, Len( data ) )
		Return ( 1 )
	End if
	
	ll_rowcount = dw_1.rowcount()
	ll_count = 1
	do while ll_count < ll_rowcount
		if data = dw_1.getitemstring(ll_count,'division_code') then
			dw_1.setitem(row,'division_code','')
			dw_1.SetItemStatus ( row,0, PRIMARY!, NotModified!)
			dw_1.setcolumn("division_code")
			dw_1.setrow(ll_count)
			dw_1.ScrollToRow(ll_count)
			dw_1.setfocus()
			iw_frame.setmicrohelp("Division " + data + " is already listed.")
			return 1
		end if
		ll_count ++
	loop
			
	 
End if
end event

event constructor;call super::constructor;
ii_rc = GetChild( 'division_code', idwc_division )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for division_code.")

idwc_division.SetTrans(SQLCA)
idwc_division.Retrieve('DIVCODE')

end event

event itemerror;call super::itemerror;Return ( 1 )
end event

type dw_2 from u_base_dw_ext within w_receive_hrs
integer x = 23
integer y = 8
integer width = 2674
integer height = 172
integer taborder = 20
string dataobject = "d_receive_hrs_header"
boolean border = false
end type

on ue_postconstructor;call u_base_dw_ext::ue_postconstructor;ib_updateable = TRUE

end on

event constructor;call super::constructor;//fill frt forwarder drop down data window
ii_rc = GetChild( 'frt_forwarder', idwc_address_code )
IF ii_rc = -1 THEN MessageBox("DataWindow Child Error", "Unable to get Child Handle for address code ")

idwc_address_code.SetTrans(SQLCA)
idwc_address_code.Retrieve("FRTFORWD")
end event

event itemchanged;call super::itemchanged;//THIS.SelectText (1,15)      
string ls_str 
long ll_cur_row
If is_ColumnName = "frt_forwarder" Then
	
   IF Trim(data) <> "" THEN 
      ll_cur_row = idwc_address_code.Find ( "address_code='"+ &
		             data+"'", 1, idwc_address_code.RowCount() )
						 
      IF ll_cur_row < 1 THEN
         MessageBox( "Freight Forwarder Error", &
			            data + " is not a Valid Freight Forwarder code." )
      	Return 1
        
      End if
  	End if		
End if
end event

event itemerror;call super::itemerror;return(1)

end event

