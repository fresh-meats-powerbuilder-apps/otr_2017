HA$PBExportHeader$w_cust_order_browse.srw
forward
global type w_cust_order_browse from w_base_sheet_ext
end type
type dw_1 from u_base_dw_ext within w_cust_order_browse
end type
end forward

global type w_cust_order_browse from w_base_sheet_ext
integer x = 544
integer y = 208
integer width = 2185
string title = "Order Browse"
long backcolor = 12632256
dw_1 dw_1
end type
global w_cust_order_browse w_cust_order_browse

type variables
//u_otr004		iu_otr004

u_ws_customer	iu_ws_customer

string		is_closemessage_txt, &
		is_customer_id

u_order_browse	iu_order_browse

end variables

forward prototypes
public function boolean wf_retrieve ()
private function integer wf_load_order_browse ()
end prototypes

public function boolean wf_retrieve ();// 01-15-1997 ibdkkam  Updated for PB v5.0.02.  Modifications:
// 1) Opens w_cust_name_filter iff customer id instance variable 
// is NULL or empty string.  2) Accesses user object instance 
// variables instead of structure variables.
// 01-21-1997 ibdkkam  Modified to set date values as NULL when 
// invoked from the customer name browse window.

Integer	  li_rc

Long       ll_row_count

If Trim( is_customer_id ) = "" or IsNull( is_customer_id ) Then
	
	OpenWithParm( w_cust_order_filter, iu_order_browse, iw_frame )
	iu_order_browse  = Message.PowerObjectParm
	
	If isvalid(iu_order_browse) = False then
		Return False
	End If
	
	If iu_order_browse.cancel = TRUE  Then
      ll_row_count = This.dw_1.rowcount()
      If ll_row_count = 0 Then
      	Close (This)
      End If
		
      Return FALSE
	End If
	
Else
		iu_order_browse.req_customer_id = is_customer_id
		SetNull( iu_order_browse.req_from_ship_date )
		SetNull( iu_order_browse.req_to_ship_date )
End If

dw_1.SetRedraw( FALSE )
dw_1.Reset( )

li_rc = wf_load_order_browse ()

This.title = "Customer Order Browse - " + iu_order_browse.req_customer_id

If li_rc = 1 Then
    dw_1.Sort()
    dw_1.ResetUpdate()
End If
	
dw_1.SetRedraw( TRUE )

Return FALSE 
end function

private function integer wf_load_order_browse ();// 01-15-1997 ibdkkam  Upgraded for PB v5.0.02.  Modified to
// 1) Check value returned from RPC via case stmt.  2) Reworked 
// do loop so that code is not duplicated.  3) Changes to reference
// nvo instance variables instead of structure variables.

Integer	   li_rtn

String		ls_customer_string, &
            ls_req_cust_id, &
				ls_refetch_customer_id, &
            ls_req_from_ship_date, &
            ls_req_to_ship_date, &
            ls_refetch_ship_date

s_error		lstr_Error_Info

Long			ll_row

SetPointer( HourGlass! )

// Assign for RPC error message.
lstr_error_info.se_event_name = "wf_load_order_browse"
lstr_error_info.se_window_name = "w_cust_browse"
lstr_error_info.se_procedure_name = "wf_load_order_browse"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ls_req_cust_id	= iu_order_browse.req_customer_id
ls_req_from_ship_date = String(iu_order_browse.req_from_ship_date, "yyyy-mm-dd")
ls_req_to_ship_date = String(iu_order_browse.req_to_ship_date, "yyyy-mm-dd")

ls_refetch_ship_date   =  Space(6) 
ls_refetch_customer_id =  Space( 7 )

Do 
	ls_customer_string = Space( 1451 )
/*	
	li_rtn = iu_otr004.nf_otrt46ar( ls_req_cust_id, &
         	ls_req_from_ship_date, ls_req_to_ship_date, &
         	ls_customer_string, ls_refetch_customer_id, &
				ls_refetch_ship_date, lstr_error_info, 0 )
*/				
	li_rtn = iu_ws_customer.uf_otrt46er( ls_req_cust_id, &
         	ls_req_from_ship_date, ls_req_to_ship_date, &
         	ls_customer_string, lstr_error_info )				

	Choose Case li_rtn
		Case 0
			iw_frame.SetMicroHelp( "Ready..." )
		Case Else
			Return( -1 )
	End Choose
	
	li_rtn = dw_1.ImportString( Trim( ls_customer_string ) )
	
Loop While ( Len( Trim( ls_refetch_customer_id ) ) > 0 )

This.title = "Customer Order Browse - " + iu_order_browse.req_customer_id
      
Return( 1 )
end function

event deactivate;call super::deactivate;//iw_frame.im_menu.mf_enable("m_save")
//iw_frame.im_menu.mf_enable("m_delete")
end event

event close;call super::close;// DESTROY iu_otr004

DESTROY u_ws_customer
end event

on resize;call w_base_sheet_ext::resize;
dw_1.resize(workspacewidth(), workspaceheight())
//dw_1.resize(workspacewidth() - 1, workspaceheight() - 1)
end on

on w_cust_order_browse.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_cust_order_browse.destroy
call super::destroy
destroy(this.dw_1)
end on

event activate;call super::activate;//iw_frame.im_menu.mf_disable("m_save")
//iw_frame.im_menu.mf_disable("m_delete")
end event

event ue_postopen;call super::ue_postopen;//iu_otr004      =  CREATE u_otr004

iu_ws_customer = CREATE u_ws_customer

is_customer_id = Message.StringParm

wf_retrieve() 
end event

type dw_1 from u_base_dw_ext within w_cust_order_browse
integer x = 5
integer y = 4
integer width = 2126
integer height = 1308
string dataobject = "d_cust_order_browse"
boolean hscrollbar = true
boolean vscrollbar = true
end type

on doubleclicked;call u_base_dw_ext::doubleclicked;//
end on

event clicked;call super::clicked;//
end event

