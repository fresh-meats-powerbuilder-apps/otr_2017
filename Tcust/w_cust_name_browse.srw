HA$PBExportHeader$w_cust_name_browse.srw
forward
global type w_cust_name_browse from w_base_sheet_ext
end type
type dw_1 from u_base_dw_ext within w_cust_name_browse
end type
end forward

global type w_cust_name_browse from w_base_sheet_ext
integer x = 206
integer y = 292
integer width = 2519
integer height = 1328
string title = "Customer Browse"
long backcolor = 12632256
dw_1 dw_1
end type
global w_cust_name_browse w_cust_name_browse

type variables
//u_otr004		iu_otr004

u_ws_customer  	iu_ws_customer

string		is_closemessage_txt, &
		is_customer_id
u_cust_browse	ui_cust_browse

end variables

forward prototypes
private function integer wf_load_cust_browse ()
private function boolean wf_retrieve ()
end prototypes

private function integer wf_load_cust_browse ();// 01-06-1997 ibdkkam  Upgraded for PB v5.0.02.  Modified to
// 1) Check value returned from RPC via case stmt.  2) Reworked 
// do loop so that code is not duplicated.  3) Changes to reference
// nvo instance variables instead of structure variables.

Integer	   li_rtn

String		ls_customer_string, &
            ls_req_cust_name, &
				ls_refetch_customer_id     

s_error		lstr_Error_Info

Long			ll_row

SetPointer( HourGlass! )

// Assign for RPC error message 
lstr_error_info.se_event_name = "wf_load_cust_browse"
lstr_error_info.se_window_name = "w_cust_browse"
lstr_error_info.se_procedure_name = "wf_load_cust_browse"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ls_req_cust_name	= ui_cust_browse.req_customer_name
ls_refetch_customer_id  = Space(7)

Do
	ls_customer_string = Space(1726)
/*
	li_rtn = iu_otr004.nf_otrt45ar( ls_req_cust_name, &
         	ls_customer_string, ls_refetch_customer_id, &
				lstr_error_info, 0 )
*/				
	li_rtn = iu_ws_customer.uf_otrt45er( ls_req_cust_name, &
         	ls_customer_string, lstr_error_info )
		
	Choose Case li_rtn
		Case 0
			iw_frame.SetMicroHelp( "Ready..." )
		Case Else
			Return -1
	End Choose

	li_rtn = dw_1.ImportString( Trim( ls_customer_string ) )
	
Loop While ( Len( Trim( ls_refetch_customer_id ) ) > 0 )

Return(1)      
end function

private function boolean wf_retrieve ();// 01-06-1997 ibdkkam  Updated for PB v5.0.02.  Modifications:
// 1) Opens w_cust_name_filter iff customer id instance variable 
// is NULL or empty string.  2) Accesses user object instance 
// variables instead of structure variables.

Integer	li_rc, & 
			li_answer

If Trim(is_customer_id) = "" or IsNull(is_customer_id) Then
	
	OpenWithParm( w_cust_name_filter, ui_cust_browse, iw_frame )
	ui_cust_browse  = Message.PowerObjectParm
	
	If isvalid(ui_cust_browse) = False then
		Return True
	End If
	
   If ui_cust_browse.cancel = TRUE Then
     If iw_frame.iu_string.nf_isempty(ui_cust_browse.req_customer_name) Then
         Close (This)
      End if
      Return TRUE
   End if
	
Else 
	ui_cust_browse.customer_id = is_customer_id
End if

dw_1.SetRedraw( False )
dw_1.Reset() 

li_rc = wf_load_cust_browse ( )
This.title = "Customer Name Browse - " + ui_cust_browse.req_customer_name
IF li_rc = 1 THEN  
   dw_1.Sort()
	dw_1.ResetUpdate()
end if

dw_1.SetRedraw( True )
	
Return FALSE
   






end function

event close;call super::close;//DESTROY iu_otr004

DESTROY u_ws_customer
end event

event resize;call super::resize;dw_1.resize(workspacewidth(), workspaceheight())

end event

on w_cust_name_browse.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_cust_name_browse.destroy
call super::destroy
destroy(this.dw_1)
end on

event ue_postopen;call super::ue_postopen;// 01-06-1997 ibdkkam  Updated for PB v5.0.02.  Moved   
// to this event (ue_postopen) from open event.

//iu_otr004 = CREATE u_otr004

iu_ws_customer = CREATE u_ws_customer

is_customer_id = Message.StringParm

wf_retrieve() 
end event

type dw_1 from u_base_dw_ext within w_cust_name_browse
integer width = 2482
integer height = 1204
string dataobject = "d_cust_name_browse"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event doubleclicked;call super::doubleclicked;long     ll_Column

string   ls_customer_id

w_cust_order_browse lw_cust_order_browse

//IF dwo.id > "0" and row <= RowCount() Then
//   If dwo.id = "1" Then
   IF dwo.name = "customer_id" Then
	   ll_Column = long(dwo.id)
      ls_customer_id = This.GetItemString( Row, ll_Column )
      OpensheetWithParm( lw_cust_order_browse, ls_customer_id, iw_frame, 0 , Original! )
   End if
	ls_customer_id = "NALLY"
//End if


end event

