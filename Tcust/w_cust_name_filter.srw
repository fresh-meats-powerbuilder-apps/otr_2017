HA$PBExportHeader$w_cust_name_filter.srw
forward
global type w_cust_name_filter from w_base_response_ext
end type
type dw_cust_name_filter from u_base_dw_ext within w_cust_name_filter
end type
end forward

global type w_cust_name_filter from w_base_response_ext
int X=727
int Y=376
int Width=1417
int Height=716
boolean TitleBar=true
string Title="Customer Browse"
long BackColor=12632256
dw_cust_name_filter dw_cust_name_filter
end type
global w_cust_name_filter w_cust_name_filter

type variables
u_cust_browse    ui_cust_browse
end variables

event open;call super::open;// 01-13-1997 ibdkkam  Updated for PB 5.0.2  Modified to access 
// nvo instance variables instead of structure.

dw_cust_name_filter.Insertrow( 0 )

ui_cust_browse = Message.PowerObjectParm

if Len( Trim( ui_cust_browse.req_customer_name ) ) > 2 then 
	dw_cust_name_filter.SetItem( 1,"customer_name",ui_cust_browse.req_customer_name )
end if
end event

on w_cust_name_filter.create
int iCurrent
call super::create
this.dw_cust_name_filter=create dw_cust_name_filter
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_cust_name_filter
end on

on w_cust_name_filter.destroy
call super::destroy
destroy(this.dw_cust_name_filter)
end on

event ue_base_cancel;call super::ue_base_cancel;// 01-13-1997 ibdkkam  Updated for PB 5.0.2  Modified to access 
// nvo instance variables instead of structure.

ui_cust_browse.cancel = TRUE

CloseWithReturn( This, ui_cust_browse )

Return
 

end event

event ue_base_ok;call super::ue_base_ok;// 01-13-1997 ibdkkam  Updated for PB 5.0.2  Modified to access 
// nvo instance variables instead of structure variables.

String ls_req_cust_name

If  dw_cust_name_filter.AcceptText( ) = 1 Then
	
	ls_req_cust_name = dw_cust_name_filter.GetItemString( 1, "customer_name" )
   If Len( ls_req_cust_name ) < 3 or IsNull( ls_req_cust_name ) Then
		
   	MessageBox( "Error","Customer Name must be at least 3 characters." )
     	dw_cust_name_filter.SetFocus()
      Return
		
   End if
	
   ui_cust_browse.req_customer_name = ls_req_cust_name
   ui_cust_browse.cancel = FALSE
   CloseWithReturn( This, ui_cust_browse )
	
Else
	
    MessageBox("Invalid Parameters", "Please enter Inquire Parameters")

End if

Return




end event

type cb_base_help from w_base_response_ext`cb_base_help within w_cust_name_filter
int X=878
int Y=388
int TabOrder=40
int TextSize=-8
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_cust_name_filter
int X=512
int Y=388
int TabOrder=30
int TextSize=-8
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_cust_name_filter
int X=146
int Y=388
int TabOrder=20
int TextSize=-8
end type

type dw_cust_name_filter from u_base_dw_ext within w_cust_name_filter
int X=18
int Y=196
int Width=1367
int Height=136
int TabOrder=10
string DataObject="d_cust_name_filter"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event itemchanged;call super::itemchanged;This.SelectText(1, Len(Data))
end event

event ue_postconstructor;call super::ue_postconstructor;This.SelectText(1,30)
end event

