HA$PBExportHeader$w_cust_order_filter.srw
forward
global type w_cust_order_filter from w_base_response_ext
end type
type dw_cust_order_filter from u_base_dw_ext within w_cust_order_filter
end type
end forward

global type w_cust_order_filter from w_base_response_ext
int X=627
int Y=489
int Width=1692
int Height=717
boolean TitleBar=true
string Title="Customer Order Filter"
long BackColor=12632256
dw_cust_order_filter dw_cust_order_filter
end type
global w_cust_order_filter w_cust_order_filter

type variables
u_order_browse	ui_order_browse
end variables

event open;call super::open;// 01-15-1997 ibdkkam  Updated for PB 5.0.2.  Modified to access 
// nvo instance variables instead of structure variables.

String ls_from_date, &
       ls_to_date 

dw_cust_order_filter.InsertRow( 0 )
dw_cust_order_filter.SetFocus( )

ui_order_browse = Message.PowerObjectParm
	
If ui_order_browse.req_customer_id[1] > " " Then
	dw_cust_order_filter.SetItem(1,"req_customer_id",ui_order_browse.req_customer_id)
 	dw_cust_order_filter.SetColumn("req_customer_id")
 	dw_cust_order_filter.SelectText(1,7)

 	ls_from_date = String(ui_order_browse.req_from_ship_date,"mm/dd/yyyy")
 	ls_to_date = String(ui_order_browse.req_to_ship_date,"mm/dd/yyyy")

 	If String(ls_from_date) > "01/01/1900" Then
		dw_cust_order_filter.SetItem(1,"req_from_ship_date",ui_order_browse.req_from_ship_date)
 	Else
 		dw_cust_order_filter.SetItem(1,"req_from_ship_date", "00/00/0000")
 	End if

 	If String(ls_to_date) > "01/01/1900" Then  
 		dw_cust_order_filter.SetItem(1,"req_to_ship_date",ui_order_browse.req_to_ship_date)
 	Else
  		dw_cust_order_filter.SetItem(1,"req_to_ship_date", "00/00/0000")
 	End if
 
End if

end event

on w_cust_order_filter.create
int iCurrent
call w_base_response_ext::create
this.dw_cust_order_filter=create dw_cust_order_filter
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_cust_order_filter
end on

on w_cust_order_filter.destroy
call w_base_response_ext::destroy
destroy(this.dw_cust_order_filter)
end on

event ue_base_cancel;call super::ue_base_cancel;// 01-15-1997 ibdkkam  Updated for PB 5.0.2.  Modified to access 
// nvo instance variables instead of structure variables.

ui_order_browse.cancel = TRUE

CloseWithReturn( This, ui_order_browse )

 

end event

event ue_base_ok;call super::ue_base_ok;// 01-15-1997 ibdkkam  Updated for PB 5.0.2.  Modified to access 
// nvo instance variables instead of structure variables.

Character	lc_req_cust_id[7]

Date			ld_from_date, &
          	ld_to_date

If  dw_cust_order_filter.AcceptText( ) = 1 Then
  
	lc_req_cust_id = dw_cust_order_filter.GetItemString( 1, "req_customer_id" )
  	ld_from_date = dw_cust_order_filter.GetItemDate( 1, "req_from_ship_date" )
  	ld_to_date = dw_cust_order_filter.GetItemDate( 1, "req_to_ship_date" )
  
	IF lc_req_cust_id[1] = " " or IsNull( lc_req_cust_id[1] ) Then
   	MessageBox( "Error","Customer I.D. must be entered." )
   	dw_cust_order_filter.SetFocus( )
   	Return
	End if

	If String(ld_from_date) <> "00/00/0000" Then
		
		If string(ld_to_date) <> "00/00/0000" Then
      	If ld_from_date > ld_to_date Then
         	MessageBox("Error","From Date must be earlier than To Date.")
         	dw_cust_order_filter.SetFocus()
         	Return
         End if
      Else
         MessageBox("Error","To Date must be entered with From Date.")
         dw_cust_order_filter.SetFocus( )
         Return
      End if
		
   Else
		
      If string(ld_to_date) <> "00/00/0000" Then
      	MessageBox("Error","From Date must be entered with To Date.")
         dw_cust_order_filter.SetFocus( )
         Return
      End if 
		
   End if       

   ui_order_browse.req_customer_id = lc_req_cust_id
   ui_order_browse.req_from_ship_date = ld_from_date
   ui_order_browse.req_to_ship_date = ld_to_date
   ui_order_browse.cancel = FALSE
	
   CloseWithReturn( This, ui_order_browse )
	
Else
	
	MessageBox("Invalid Parameters", "Please enter Inquire Parameters")
   Return
	 
End if




end event

type cb_base_help from w_base_response_ext`cb_base_help within w_cust_order_filter
int X=1061
int Y=449
int TabOrder=40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_cust_order_filter
int X=723
int Y=453
int TabOrder=30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_cust_order_filter
int X=375
int Y=449
int TabOrder=20
end type

type dw_cust_order_filter from u_base_dw_ext within w_cust_order_filter
int X=23
int Y=53
int Width=1550
int Height=333
int TabOrder=10
string DataObject="d_cust_order_filter"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

