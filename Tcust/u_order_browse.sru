HA$PBExportHeader$u_order_browse.sru
forward
global type u_order_browse from nonvisualobject
end type
end forward

global type u_order_browse from nonvisualobject autoinstantiate
end type

type variables
Character	req_customer_id[7]
Date	req_from_ship_date, &
	req_to_ship_date
Boolean	cancel
end variables

on u_order_browse.create
TriggerEvent( this, "constructor" )
end on

on u_order_browse.destroy
TriggerEvent( this, "destructor" )
end on

