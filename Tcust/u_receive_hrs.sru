HA$PBExportHeader$u_receive_hrs.sru
forward
global type u_receive_hrs from nonvisualobject
end type
end forward

global type u_receive_hrs from nonvisualobject autoinstantiate
end type

type variables
Character	customer_id[7], &
	division[7]
Boolean	cancel
end variables

on u_receive_hrs.create
TriggerEvent( this, "constructor" )
end on

on u_receive_hrs.destroy
TriggerEvent( this, "destructor" )
end on

