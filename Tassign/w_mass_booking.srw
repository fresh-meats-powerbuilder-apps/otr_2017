HA$PBExportHeader$w_mass_booking.srw
forward
global type w_mass_booking from w_base_response_ext
end type
type st_1 from statictext within w_mass_booking
end type
type dw_1 from datawindow within w_mass_booking
end type
end forward

global type w_mass_booking from w_base_response_ext
integer x = 416
integer y = 368
integer width = 1385
integer height = 384
string title = "Mass Booking"
long backcolor = 12632256
st_1 st_1
dw_1 dw_1
end type
global w_mass_booking w_mass_booking

type variables
datawindowchild idwc_carrier

integer   ii_rc

end variables

on w_mass_booking.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_1
end on

on w_mass_booking.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_1)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn( This, "CANCEL" )

end event

event ue_base_ok;call super::ue_base_ok;string		ls_Carrier

IF dw_1.AcceptText() = -1 THEN Return

//ls_carrier = dw_1.GetItemString( 1, "carrier_code" )
ls_carrier = dw_1.object.carrier_code[1]

CloseWithReturn( This, ls_carrier )

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_mass_booking
integer x = 1065
integer y = 148
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_mass_booking
integer x = 773
integer y = 148
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_mass_booking
integer x = 485
integer y = 148
integer taborder = 20
end type

type st_1 from statictext within w_mass_booking
integer x = 32
integer y = 28
integer width = 1312
integer height = 68
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "All Selected Records will be Booked with Carrier"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_1 from datawindow within w_mass_booking
integer x = 32
integer y = 96
integer width = 494
integer height = 172
integer taborder = 10
string dataobject = "d_dddw_carriers"
boolean border = false
boolean livescroll = true
end type

event itemchanged;long ll_FoundRow


IF Trim(data) = "" THEN Return

ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Carrier Error", data + " is Not a Valid Carrier Code." )
	Return 1
END IF		

end event

event itemerror;Return 1
end event

event constructor;InsertRow(0)

ii_rc = GetChild( 'carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()
end event

