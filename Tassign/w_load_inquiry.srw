HA$PBExportHeader$w_load_inquiry.srw
forward
global type w_load_inquiry from w_base_response_ext
end type
type dw_load_inquiry from datawindow within w_load_inquiry
end type
type dw_load_inquiry_detail from datawindow within w_load_inquiry
end type
end forward

global type w_load_inquiry from w_base_response_ext
integer x = 270
integer y = 216
integer width = 3086
integer height = 1584
string title = "Load Inquiry"
long backcolor = 12632256
dw_load_inquiry dw_load_inquiry
dw_load_inquiry_detail dw_load_inquiry_detail
end type
global w_load_inquiry w_load_inquiry

type variables
u_otr002   iu_otr002

u_ws_traffic iu_ws_traffic
end variables

forward prototypes
public subroutine wf_loaddetailinquiry (string as_load_key)
end prototypes

public subroutine wf_loaddetailinquiry (string as_load_key);// 03/11/1997 ibdkkam  Added ls_refetch_queue_id and 
//	li_refetch_queue_item to iu_otr002.nf_otrt27ar call.
// Added loop to process report string while the length 
// of the returned ls_refetch_queue_id is > 0.

integer	li_rtn, &
			li_refetch_queue_item

string   ls_detail_header, &
			ls_detail_rpt, &
			ls_len_data, &
			ls_refetch_queue_id, &
			ls_detail_work_rpt

s_error  lstr_Error_Info

lstr_error_info.se_event_name 	= "wf_loaddetailinquiry"
lstr_error_info.se_window_name 	= "w_load_inquiry"
lstr_error_info.se_procedure_name= "wf_loaddetailinquiry"
lstr_error_info.se_user_id 		= SQLCA.Userid
lstr_error_info.se_message 		= Space(71)

ls_detail_header			= Space(144)
ls_detail_rpt				= Space(7201)
li_refetch_queue_item 	= 0
ls_refetch_queue_id 		= Space(8)

//li_rtn = iu_otr002.nf_otrt27ar( as_load_key, ls_detail_header, &
	//ls_detail_rpt, ls_refetch_queue_id, &
	//li_refetch_queue_item, lstr_Error_Info, 0 )

li_rtn = iu_ws_traffic.nf_otrt27er( as_load_key, ls_detail_header, &
	ls_detail_rpt, lstr_Error_Info)

If ( li_rtn <> 0 ) Then
	Close( This )
	Return
End If
	
ls_detail_header 	= Trim( ls_detail_header )
ls_detail_rpt 		= Trim( ls_detail_rpt )

If ( Len( ls_detail_header ) > 0 ) Then
	dw_load_inquiry.ImportString( ls_detail_header )
	dw_load_inquiry_detail.ImportString( ls_detail_rpt )
End If

Do While ( Len( Trim( ls_refetch_queue_id ) ) > 0 )
	
	ls_detail_rpt = Space(7201)

    li_rtn = iu_ws_traffic.nf_otrt27er( as_load_key, ls_detail_header, &
     	ls_detail_rpt, lstr_Error_Info)
    
	//li_rtn = iu_otr002.nf_otrt27ar( as_load_key, ls_detail_header, &
	  // ls_detail_rpt, ls_refetch_queue_id, &
	   //lI_refetch_queue_item, lstr_Error_Info, 0 )

	If ( li_rtn <> 0 ) Then
		Close( This )
		Return
	End If
	
	ls_detail_rpt = Trim( ls_detail_rpt )
	dw_load_inquiry_detail.ImportString( ls_detail_rpt )

Loop

Return  
end subroutine

event open;call super::open;int li_rtn

string   ls_load_key


iu_otr002  =  CREATE u_otr002

iu_ws_traffic = CREATE u_ws_traffic

ls_load_key = Message.StringParm

wf_loaddetailinquiry( ls_load_key )


end event

event close;call super::close;DESTROY iu_otr002
DESTROY iu_ws_traffic
end event

on w_load_inquiry.create
int iCurrent
call super::create
this.dw_load_inquiry=create dw_load_inquiry
this.dw_load_inquiry_detail=create dw_load_inquiry_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_load_inquiry
this.Control[iCurrent+2]=this.dw_load_inquiry_detail
end on

on w_load_inquiry.destroy
call super::destroy
destroy(this.dw_load_inquiry)
destroy(this.dw_load_inquiry_detail)
end on

event ue_base_ok;call super::ue_base_ok;Close(this)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_load_inquiry
integer x = 2117
integer y = 220
integer taborder = 20
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_load_inquiry
boolean visible = false
integer x = 2117
integer y = 204
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_load_inquiry
integer x = 2117
integer y = 80
end type

type dw_load_inquiry from datawindow within w_load_inquiry
integer x = 9
integer y = 16
integer width = 2057
integer height = 744
string dataobject = "d_load_inquiry"
boolean border = false
boolean livescroll = true
end type

type dw_load_inquiry_detail from datawindow within w_load_inquiry
integer x = 41
integer y = 772
integer width = 2944
integer height = 688
integer taborder = 21
boolean bringtotop = true
string dataobject = "d_load_inquiry_detail"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

