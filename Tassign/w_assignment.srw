HA$PBExportHeader$w_assignment.srw
forward
global type w_assignment from w_base_sheet_ext
end type
type cb_massbook from commandbutton within w_assignment
end type
type dw_2 from u_base_dw_ext within w_assignment
end type
type dw_1 from u_base_dw_ext within w_assignment
end type
end forward

global type w_assignment from w_base_sheet_ext
integer x = 23
integer y = 248
integer height = 1516
string title = "Carrier Assignment"
long backcolor = 12632256
cb_massbook cb_massbook
dw_2 dw_2
dw_1 dw_1
end type
global w_assignment w_assignment

type variables
character   ic_control, &
                ic_complex[3], &
                ic_carrier_code[4] 

string is_closemessage_txt

u_carrier_assignment_str   iu_carr_assign

u_otr002   iu_otr002

u_ws_traffic iu_ws_traffic

datawindowchild    idwc_trailer, &
                            idwc_auth_carrier, &
                            idwc_carrier_pref



end variables

forward prototypes
public function boolean wf_retrieve ()
public function integer wf_changed ()
public function boolean wf_update ()
public function integer wf_loadtrailerinfo (string as_complex, string as_assign_status, string as_carrier_code)
public function integer wf_loadreportdetail ()
end prototypes

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer
		
string	ls_title, &
			ls_from_date, &
			ls_to_date

li_rc = wf_Changed()

CHOOSE CASE li_rc	//begin 1
	CASE  1	//	a valid change occurred ...
		li_answer = MessageBox("Wait",is_closemessage_txt,question!,yesnocancel!)
		CHOOSE CASE li_answer
			CASE 1	//	yes, save changes
				IF wf_Update() THEN	//	the save succeeded
				ELSE	//	the save failed
					li_answer = MessageBox("Save Failed","Retrieve Anyway?",question!,yesno!)
					IF li_answer = 1 THEN	//	retrieve w/o saving changes
					ELSE	//	cancel the close
						Return( false )
					END IF
				END IF	
			CASE 2 	//	retrieve w/o saving changes
			CASE 3	//	cancel the retrieve
				Return(false)
		END CHOOSE
	CASE  -1 		//	a validation error occurred
		li_answer = MessageBox("Data Entry Error","Retrieve w/o Save?",question!,okcancel!)
		CHOOSE CASE li_answer
			CASE 1	//	retrieve w/o saving changes
			CASE 2	//	cancel the retrieve
				Return( false )
		END CHOOSE
	CASE  -2 	//	a serious validation error, window may not close in this state
		MessageBox("A Serious Error Has Occurred","Correct Error Before Retrieving!",exclamation!,ok!)
		Return(False)
	CASE ELSE	//	nothing has changed
END CHOOSE

OpenWithParm( w_assignment_filter, iu_carr_assign, iw_frame )
iu_carr_assign = Message.PowerObjectParm
IF isvalid(iu_carr_assign)= false then
    return TRUE
end if
// check to see if cancel was pressed
IF iu_carr_assign.cancel THEN
	Return TRUE
ELSE
	dw_1.SetRedraw( False )
	dw_1.Reset()
	li_rc = wf_loadreportdetail ()
	
	ls_from_date = left(iu_carr_assign.ship_from,4) + "-" + mid(iu_carr_assign.ship_from,5,2) + &
					   "-" +  mid(iu_carr_assign.ship_from,7,2)
	ls_to_date   = left(iu_carr_assign.ship_to,4) + "-" + mid(iu_carr_assign.ship_to,5,2) + &
					   "-" +  mid(iu_carr_assign.ship_to,7,2)

	IF li_rc = 1 THEN
		IF ic_control = "A" THEN
			this.Title = "Detail All Loads" + " - " + iu_carr_assign.complex
			ls_title = this.title
		ELSE
			this.Title = "Detailed Unbooked" + " - " + iu_carr_assign.complex
			ls_title = this.title
		END IF
		dw_1.Sort()
		dw_1.GroupCalc()	
		dw_1.ResetUpdate()
		dw_1.SetRedraw( True )
	
		dw_2.modify("txt_complex.text = '" + ls_title + "'")
		dw_2.modify("txt_from_date.text = '" + ls_from_date + "'")
		dw_2.modify("txt_to_date.text = '" + ls_to_date + "'")
		Return FALSE
	ELSE
		IF ic_control = "A" THEN
			this.Title = "Detail All Loads" + " - " + iu_carr_assign.complex
			ls_title = this.title
		ELSE
			this.Title = "Detailed Unbooked" + " - " + iu_carr_assign.complex
			ls_title = this.title
		END IF
		dw_1.SetRedraw( True )
	
		dw_2.modify("txt_complex.text = '" + ls_title + "'")
		dw_2.modify("txt_from_date.text = " + iu_carr_assign.ship_from)
		dw_2.modify("txt_to_date.text = '" + iu_carr_assign.ship_to + "'")
		Return false
	END IF
	
END IF


end function

public function integer wf_changed ();/*
	purpose:	this function will examine all datawindow controls on the window
				that have update capability against the database.  if anything has changed,
				and the change is valid, a 1 is returned.  if an invalid change has occurred
				then a -1 is returned.  if nothing has changed a 0 is returned.

*/

int i, totcontrols, rc, counter
datawindow dw[]

totcontrols = UpperBound(this.control)

FOR i = 1 to totcontrols
	IF TypeOf(this.control[i]) = datawindow! THEN
		counter ++
		dw[counter] = this.control[i]
			rc = dw[counter].AcceptText()
			IF rc = -1 THEN 
				RETURN -1
			ELSE
				CONTINUE
			END IF
			counter --
	END IF
NEXT

FOR i = 1 to counter
	IF dw[i].ModifiedCount() > 0 THEN
		is_closemessage_txt = "Save Changes?"
		RETURN 1
	END IF
NEXT

RETURN 0
	
end function

public function boolean wf_update ();long			 		ll_Row,&
						ll_RowCount, &
						ll_Count, &
						ll_del_row

integer           li_rtn, &
						li_refetch_queue_item , &
						li_rc

string				ls_carrier_assigned_update, &
						ls_space, &
						ls_cr, &
						ls_carrier_comment, &
						ls_vehicle_number, &
						ls_loadkey, &
	               ls_req_control, &
						ls_load_key, &
						ls_complex, &
						ls_auth_carrier, &
						ls_deliv_carrier, &
						ls_expedite_ind

dwItemStatus		status

s_error				lstr_Error_Info

IF dw_1.AcceptText() = -1 THEN Return(False)

SetPointer(HourGlass!)

ll_RowCount = dw_1.RowCount()
ls_space		= " "
 
lstr_error_info.se_event_name = "wf_update"
lstr_error_info.se_window_name = "w_assign"
lstr_error_info.se_procedure_name = "wf_update"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

dw_1.Setredraw(FALSE)
DO WHILE ll_Row <= ll_RowCount 
	ll_Row = dw_1.GetNextModified( ll_Row, Primary! )
	IF ll_Row > 0  THEN 
		IF dw_1.GetItemString(ll_row, "update_ind") = 'Y' then
			ll_Count = ll_Count + 1
			ls_load_key			= dw_1.GetItemString( ll_Row, "primary_order" )
			ls_complex			= dw_1.GetItemString( ll_Row, "complex" )		
			ls_auth_carrier	= dw_1.GetItemString( ll_Row, "auth_carrier" )
			ls_auth_carrier   = ls_auth_carrier + space(4 - len(ls_auth_carrier))
			ls_auth_carrier	= ls_auth_carrier
			ls_vehicle_number	= dw_1.GetItemString( ll_Row, "vehicle_number" )
			ls_vehicle_number = ls_vehicle_number + space(15 - len(ls_vehicle_number))
			ls_vehicle_number = ls_vehicle_number
			ls_carrier_comment= dw_1.GetItemString( ll_Row, "carrier_comments" )
			ls_carrier_comment= ls_carrier_comment + space(40 - len(ls_carrier_comment))
			ls_carrier_comment= ls_carrier_comment
			ls_expedite_ind	= dw_1.GetItemString( ll_Row, "expedite_ind" )

			IF trim(ls_auth_carrier) = "" THEN
				ls_req_control = "U"
				ls_deliv_carrier = ls_auth_carrier
			ELSE
				ls_req_control = "B"
				IF ls_auth_carrier = "PBFE" THEN
					ls_deliv_carrier	= "IBPX"
				ELSE
					ls_deliv_carrier	= ls_auth_carrier
				END IF
			END IF

			ls_carrier_assigned_update = ls_load_key +'~t' + ls_complex+'~t' + &
												  ls_auth_carrier +'~t' + ls_deliv_carrier + &
											    '~t' + ls_vehicle_number +'~t' + &
											     ls_carrier_comment +'~t' + ls_expedite_ind
			li_rtn = iu_ws_traffic.nf_otrt30er( ls_req_control, ls_carrier_assigned_update, &
													 lstr_Error_Info)
													 

			CHOOSE CASE li_rtn
				CASE is < 0 
					dw_1.Setredraw(TRUE)
					li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
											ls_load_key + " has failed due to an Error.  Do you want to Continue?", &
											Question!, YesNo! )
					ll_Count --
					dw_1.Setredraw(FALSE)
				CASE is > 0 
					dw_1.Setredraw(TRUE)
					li_rc = MessageBox( "Update Warning", lstr_Error_Info.se_message + &
											" Load Key = " +ls_load_key, &
											Information!, OK! )
					dw_1.Setredraw(FALSE)
					IF ic_control = "A" THEN
						dw_1.SetItem(ll_row,'update_ind','N')
						dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
					ELSE
						if len(trim(dw_1.GetItemString(ll_row,"auth_carrier"))) > 0 then
							ll_del_row = ll_row
							dw_1.Setredraw(FALSE)
							ls_loadkey = dw_1.GetItemString(ll_row, "primary_order")
							ll_del_row = ll_row
  							DO WHILE ls_loadkey = dw_1.GetItemString(ll_del_row, "primary_order")
								dw_1.DeleteRow( ll_del_row )
								IF dw_1.rowcount() = 0 or dw_1.rowcount() < ll_del_row then exit
							LOOP
							ll_Row --
							ll_RowCount --
						ELSE
							dw_1.SetItem(ll_row,'update_ind','N')
						end if
					END IF
				CASE 0
					IF ic_control = "A" THEN
						dw_1.SetItem(ll_row,'update_ind','N')
						dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
					ELSE
						if len(trim(dw_1.GetItemString(ll_row,"auth_carrier"))) > 0 then
							ls_loadkey = dw_1.GetItemString(ll_row, "primary_order")
							ll_del_row = ll_row
							dw_1.Setredraw(FALSE)
  							DO WHILE ls_loadkey = dw_1.GetItemString(ll_del_row, "primary_order")
								dw_1.DeleteRow( ll_del_row )
								IF dw_1.rowcount() = 0 or dw_1.rowcount() < ll_del_row then exit
							LOOP
							ll_Row --
							ll_RowCount --
						ELSE
							dw_1.SetItem(ll_row,'update_ind','N')
						end if
					END IF
					li_rc = 0
			END CHOOSE

			IF li_rc = 2 THEN 			// NO  do not continue processing
				dw_1.Setredraw(TRUE)
				Return(False)
			END IF
		END IF
	ELSE
		dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
		ll_Row = ll_RowCount + 1
	END IF
LOOP
dw_1.scrolltorow(dw_1.getrow())
dw_1.ResetUpdate()
dw_1.setredraw(true)

Return(True)
end function

public function integer wf_loadtrailerinfo (string as_complex, string as_assign_status, string as_carrier_code);integer                 li_rtn, &
								li_RPCrtn

string                  ls_trailer_info, &
								ls_refetch_carrier_code, &
      				   	ls_refetch_trailer_number


s_error                 lstr_Error_Info


idwc_trailer.Reset()

lstr_error_info.se_event_name = "wf_loadtrailerinfo"
lstr_error_info.se_window_name = "w_assign"
lstr_error_info.se_procedure_name = "wf_loadtrailerinfo"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ls_trailer_info	= Space(20000)
ls_refetch_trailer_number = Space(15)
ls_refetch_carrier_code   = Space(4)
///li_rtn = iu_otr002.nf_otrt29ar( as_complex, as_assign_status, as_carrier_code, &
	//										ls_trailer_info, ls_refetch_carrier_code, &	
		//									ls_refetch_trailer_number, lstr_Error_Info, 0 )


li_rtn = iu_ws_traffic.nf_otrt29er( as_complex, as_assign_status, as_carrier_code, &
										 	ls_trailer_info, lstr_Error_Info)
// Check the return code from the above function call
IF li_rtn <> 0 THEN
	return (-1)
END IF

li_rtn = idwc_trailer.ImportString(Trim(ls_trailer_info))

DO WHILE trim(ls_refetch_carrier_code) <> "" 
//	ac_carrier_code	= ls_carrier_code
	ls_trailer_info	= Space(3451)
	//li_rtn = iu_otr002.nf_otrt29ar( as_complex, as_assign_status, as_carrier_code, &
		//									 	ls_trailer_info, ls_refetch_carrier_code, &	
			//									ls_refetch_trailer_number, lstr_Error_Info, 0 )
 
    li_rtn = iu_ws_traffic.nf_otrt29er( as_complex, as_assign_status, as_carrier_code, &
										 	ls_trailer_info, lstr_Error_Info)
											 
	li_rtn = idwc_trailer.ImportString(Trim(ls_trailer_info))
LOOP

return(1)
end function

public function integer wf_loadreportdetail ();integer         li_rtn, &
      			 li_RPCrtn, &
			       li_refetch_queue_item 

string          ls_stops_detail, &
                ls_req_complex, &
		          ls_req_control, &
		          ls_req_plant, &
		          ls_req_region, &
		          ls_req_division, &
		          ls_req_transmode, &
                ls_req_carrier_code, &
			       ls_req_ship_from_date, &
			       ls_req_ship_to_date, &
                ls_refetch_queue_id

s_error                 lstr_Error_Info

long			ll_row,&
				ll_rowcount, &
				ll_currentrow, &
				ll_total_records

// assign values
SetPointer( HourGlass! )
ll_total_records = 0
ls_req_complex				= iu_carr_assign.complex
ls_req_control				= ic_control
ls_req_plant				= iu_carr_assign.location
ls_req_region				= iu_carr_assign.region
ls_req_division			= iu_carr_assign.division
ls_req_transmode			= "T"
ls_req_carrier_code		= iu_carr_assign.carrier
ls_req_ship_from_date	= iu_carr_assign.ship_from
ls_req_ship_to_date		= iu_carr_assign.ship_to

lstr_error_info.se_event_name = "wf_loadreportdetail"
lstr_error_info.se_window_name = "w_assign"
lstr_error_info.se_procedure_name = "wf_loadreportdetail"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ll_rowcount =  upperbound(iu_carr_assign.complex_array)
ll_currentrow = 1
DO WHILE ll_currentRow <= ll_RowCount 

ls_req_complex = iu_carr_assign.complex_array[ll_currentrow]
ls_refetch_queue_id = Space(8)
ls_stops_detail	= Space(4826)
li_rtn = iu_ws_traffic.nf_otrt26er(	ls_req_complex, ls_req_control, ls_req_plant, &
											ls_req_region, ls_req_division, ls_req_transmode, &
											ls_req_carrier_code, ls_req_ship_from_date, &
											ls_req_ship_to_date, ls_stops_detail, lstr_Error_Info)
// Check the return code from the above function call

dw_1.Setredraw(FALSE)
IF li_rtn = 0 then
	li_rtn = dw_1.ImportString(Trim(ls_stops_detail))
	ll_total_records = li_rtn + ll_total_records
End if
DO WHILE len(trim(ls_refetch_queue_id)) > 0 
	ls_stops_detail	= Space(4826)
	li_rtn = iu_ws_traffic.nf_otrt26er(	ls_req_complex, ls_req_control, ls_req_plant, &
											ls_req_region, ls_req_division, ls_req_transmode, &
											ls_req_carrier_code, ls_req_ship_from_date, &
											ls_req_ship_to_date, ls_stops_detail, lstr_Error_Info)
	li_rtn = dw_1.ImportString(Trim(ls_stops_detail))
	ll_total_records = li_rtn + ll_total_records
 
LOOP
ll_currentrow ++
LOOP
dw_1.SetRedraw(TRUE)
If ll_total_records > 0 then
	iw_frame.SetMicroHelp( "Ready...")
end if
return(1)      
end function

on ue_postopen;call w_base_sheet_ext::ue_postopen;iw_frame.iw_active_sheet = this
IF wf_retrieve() THEN Close( this )

end on

event open;call super::open;iu_otr002 = CREATE u_otr002
iu_ws_traffic = CREATE u_ws_traffic

ic_control = Message.StringParm

IF ic_control = "A" THEN
	Title = "Detail All Loads"
ELSE
	Title = "Detailed Unbooked"
	cb_massbook.visible = True
	dw_1.x = 5
	dw_1.y = 10
END IF
dw_1.insertrow(0)



end event

event resize;call super::resize;IF ic_control = "A" THEN
	dw_1.height = WorkSpaceHeight() - 5
	dw_1.Width	= WorkSpaceWidth() 
ELSE
	dw_1.height = WorkSpaceHeight() - 5
	dw_1.Width	= WorkSpaceWidth() 
//	Title = "Detailed Unbooked"
//	cb_massbook.visible = True
//	dw_1.x = 23
//	dw_1.y = 125
END IF

end event

event close;call super::close;DESTROY iu_otr002
DESTROY iu_ws_traffic
end event

on w_assignment.create
int iCurrent
call super::create
this.cb_massbook=create cb_massbook
this.dw_2=create dw_2
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_massbook
this.Control[iCurrent+2]=this.dw_2
this.Control[iCurrent+3]=this.dw_1
end on

on w_assignment.destroy
call super::destroy
destroy(this.cb_massbook)
destroy(this.dw_2)
destroy(this.dw_1)
end on

event ue_fileprint;call super::ue_fileprint;dw_1.sharedata(dw_2)
dw_2.Print( )
end event

event ue_printwithsetup;//override anchestor
this.triggerevent("ue_fileprint")
end event

type cb_massbook from commandbutton within w_assignment
boolean visible = false
integer x = 18
integer width = 315
integer height = 76
integer taborder = 10
integer textsize = -7
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Mass Book"
end type

event clicked;long				ll_row

string			ls_carrier

ll_row = dw_1.GetSelectedRow ( ll_row )

IF ll_row < 1 THEN
	MessageBox( "Select Row", "No Rows are Selected for Mass Bookings."  )
	return
ELSE
	Open( w_mass_booking, iw_frame )

	ls_carrier = Message.StringParm
	IF ls_carrier <> "CANCEL" THEN
		// DO SOME MASS BOOKING
		DO UNTIL ll_row = 0
			dw_1.SetItem(ll_row,"update_ind",'Y')
			dw_1.SetItem( ll_row, "auth_carrier", ls_carrier )
			ll_row = dw_1.GetSelectedRow ( ll_row )
		LOOP
	END IF
END IF

end event

type dw_2 from u_base_dw_ext within w_assignment
integer x = 9
integer y = 312
integer width = 2747
integer taborder = 0
boolean bringtotop = true
string dataobject = "d_carrier_assign_prn"
end type

type dw_1 from u_base_dw_ext within w_assignment
event ue_dwndropdown pbm_dwndropdown
event ue_set_carrier pbm_custom24
event ue_set_vehicle pbm_custom25
integer y = 8
integer width = 2807
integer height = 1400
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_carrier_assign"
boolean vscrollbar = true
end type

event ue_dwndropdown;call super::ue_dwndropdown;string		ls_ColName, &
				ls_complex, &
				ls_carrier_code, &
				ls_old_complex, &
				ls_old_carrier_code, &
         	ls_assign_status

long			ll_row

integer		li_rc

 
ls_ColName	= GetColumnName( ) 

IF ls_ColName = "vehicle_number" THEN
	ll_row					= GetRow()
	ls_assign_status		= Space(1)
	ls_complex				= GetItemString( ll_row, "complex" )
	ls_carrier_code		= GetItemString( ll_row, "auth_carrier" )
	ls_complex				= ls_complex
	ls_old_complex			= ic_complex
	ls_carrier_code		= ls_carrier_code
	ls_old_carrier_code	= ic_carrier_code

	IF (ls_complex <> ls_old_complex) OR (ls_carrier_code <> ls_old_carrier_code) THEN
		li_rc	= wf_loadtrailerinfo ( ls_complex, ls_assign_status, ls_carrier_code )
		IF li_rc = 1 THEN
			ic_complex			= ls_complex
			ic_carrier_code	= ls_carrier_code
		ELSE
			
		END IF
	END IF
END IF
end event

on ue_set_carrier;call u_base_dw_ext::ue_set_carrier;SetItem( GetRow(), "auth_carrier", ic_carrier_code )

end on

on ue_set_vehicle;call u_base_dw_ext::ue_set_vehicle;SetItem( GetRow(), "vehicle_number", "" )

end on

event doubleclicked;call super::doubleclicked;character			lc_load_key[7]
w_load_detail		lw_load_detail

IF is_ObjectAtPointer <> "load_msg_ind" THEN
	IF Row > 0 THEN
		lc_load_key = GetItemString( Row, "primary_order" )
//	   lc_load_key = This.object.primary_order[Row]
      IF is_ObjectAtPointer <> "primary_order" THEN
		   OpenWithParm( w_load_inquiry, lc_load_key )
      Else
         OpenSheetWithParm(lw_load_detail, lc_load_key, iw_frame, 0 , Original!)     
      END IF
	END IF
END IF
end event

event clicked;call super::clicked;string		ls_loadkey

long			ll_sel_row, &
            ll_rowcount

dw_1.setredraw(false)
if Row > 0 then
	ll_rowcount = dw_1.rowcount()
	ls_loadkey = dw_1.GetItemString(row, "primary_order")
	ll_sel_row = row
	DO WHILE ls_loadkey = this.object.primary_order[ll_sel_row]
		if	dw_1.IsSelected(ll_sel_row) then
			dw_1.SelectRow( ll_sel_row,false )
      else
			dw_1.SelectRow( ll_sel_row,true )
		end if
		ll_sel_row ++
     	if ll_sel_row > ll_rowcount then exit
	LOOP
	ll_sel_row = row
	DO WHILE ls_loadkey = this.object.primary_order[ll_sel_row]
		if	dw_1.IsSelected(ll_sel_row) then
			dw_1.SelectRow( ll_sel_row,false )
      else
			dw_1.SelectRow( ll_sel_row,true )
		end if
		ll_sel_row --
		if ll_sel_row > ll_rowcount or ll_sel_row = 0 then exit
	LOOP
	
end if 

dw_1.setredraw(true)

IF is_ObjectAtPointer = "load_msg_ind" THEN
	IF GetItemString( Row, "load_msg_ind" ) = "Y" THEN
	 	ls_loadkey = GetItemString( Row, "primary_order" )
	  	OpenWithParm( w_loadmessagetext, ls_loadkey )
	ELSE
		Beep(2)
	END IF
END IF

end event

event constructor;call super::constructor;int li_rc
ib_updateable = TRUE
is_selection = '3'
long ll_Long

li_rc = dw_1.GetChild('vehicle_number', idwc_trailer)
IF li_rc = -1 THEN MessageBox( "DataWindowChild Error", "Unable to get the Child Handle for Vehicle Number." )

li_rc = dw_1.GetChild('auth_carrier', idwc_auth_carrier)
IF li_rc = -1 THEN MessageBox( "DataWindowChild Error", "Unable to get the Child Handle for Auth Carrier." )

idwc_trailer.SetTrans(SQLCA)
idwc_trailer.Retrieve()

idwc_auth_carrier.SetTrans(SQLCA)
idwc_auth_carrier.Retrieve()


end event

event itemchanged;call super::itemchanged;string		ls_order_number, &
				ls_carrier, &
				ls_custid

long			ll_FoundRow
int         li_cnt

IF GetItemString(row,"primary_order") <> GetItemString(row,"order_number") then
	Beep(1)
	Return
End if
dw_1.SetItem(row,"update_ind",'Y')
CHOOSE CASE is_ColumnName

	CASE "vehicle_number"

	CASE "auth_carrier"
		IF Trim(data) <> "" THEN
			ll_FoundRow = idwc_auth_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_auth_carrier.RowCount() )
			IF ll_FoundRow < 1 THEN
				MessageBox( "Warning", data + " is Not Found on Carrier Master." )
				SelectText(1,4)
	 			Return 1
			END IF		

         ls_custid = This.object.shipto_cust_id[Row] 
			
			Connect Using SQLCA;

         select count (customer_id) 
			into  :li_cnt
			FROM carrier_dispreferance  
         WHERE ( customer_id = :ls_custid ) AND  
               ( carrier_code = :data );

			Disconnect Using SQLCA;
			
		   if li_cnt > 0 then
				MessageBox( "Warning", data+ " is Not Preferred by Customer "+ls_custid+"." )
			end if
      END IF
		PostEvent( "ue_set_vehicle" )
	
END CHOOSE


end event

event itemerror;call super::itemerror;
Return 1
end event

event ue_keydown;//override ancestor script

end event

