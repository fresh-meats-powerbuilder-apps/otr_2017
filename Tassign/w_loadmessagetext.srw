HA$PBExportHeader$w_loadmessagetext.srw
forward
global type w_loadmessagetext from w_base_response_ext
end type
type dw_loadmessage from datawindow within w_loadmessagetext
end type
end forward

global type w_loadmessagetext from w_base_response_ext
integer x = 567
integer y = 656
integer width = 1774
integer height = 856
string title = "Load Message"
long backcolor = 12632256
dw_loadmessage dw_loadmessage
end type
global w_loadmessagetext w_loadmessagetext

type variables
u_otr002     iu_otr002

u_ws_traffic iu_ws_traffic
end variables

forward prototypes
public subroutine wf_loadmessagetext (string as_load_key)
end prototypes

public subroutine wf_loadmessagetext (string as_load_key);integer                 li_rtn, &
								li_RPCrtn

string                  ls_load_message

s_error                 lstr_Error_Info

lstr_error_info.se_event_name = "wf_loadmessageext"
lstr_error_info.se_window_name = "w_loadmessagetext"
lstr_error_info.se_procedure_name = "wf_loadmessageext"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ls_load_message	= Space(2000)
//li_rtn = iu_otr002.nf_otrt28ar( as_load_key, ls_load_message, lstr_Error_Info, 0 )

li_rtn = iu_ws_traffic.nf_otrt28er( as_load_key, ls_load_message, lstr_Error_Info)

IF li_rtn <> 0 THEN
	Return 
END IF

dw_loadmessage.ImportString(Trim(ls_load_message))

Return 
end subroutine

event open;call super::open;long		ll_row
string	ls_loadkey

iu_otr002  =  CREATE u_otr002

iu_ws_traffic = CREATE u_ws_traffic

ls_loadkey = Message.StringParm

wf_loadmessagetext ( ls_loadkey )


end event

on w_loadmessagetext.create
int iCurrent
call super::create
this.dw_loadmessage=create dw_loadmessage
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_loadmessage
end on

on w_loadmessagetext.destroy
call super::destroy
destroy(this.dw_loadmessage)
end on

event close;call super::close;DESTROY iu_otr002
DESTROY iu_ws_traffic
end event

event ue_base_ok;call super::ue_base_ok;Close(this)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_loadmessagetext
integer x = 923
integer y = 636
integer taborder = 20
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_loadmessagetext
boolean visible = false
integer x = 78
integer y = 380
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_loadmessagetext
integer x = 590
integer y = 636
end type

type dw_loadmessage from datawindow within w_loadmessagetext
integer x = 23
integer y = 16
integer width = 1673
integer height = 576
string dataobject = "d_loadmessage"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

