HA$PBExportHeader$otrt01sr_programinterfaceotrt01pg.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type otrt01sr_ProgramInterfaceOtrt01pg from nonvisualobject
    end type
end forward

global type otrt01sr_ProgramInterfaceOtrt01pg from nonvisualobject
end type

type variables
    otrt01sr_ProgramInterfaceOtrt01pgOtr000sr_program_container otr000sr_program_container
    boolean structuredContainer
end variables

on otrt01sr_ProgramInterfaceOtrt01pg.create
call super::create
TriggerEvent( this, "constructor" )
end on

on otrt01sr_ProgramInterfaceOtrt01pg.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

