HA$PBExportHeader$otrt01sr_programinterfaceotrt01pg1.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type otrt01sr_ProgramInterfaceOtrt01pg1 from nonvisualobject
    end type
end forward

global type otrt01sr_ProgramInterfaceOtrt01pg1 from nonvisualobject
end type

type variables
    otrt01sr_ProgramInterfaceOtrt01pgOtr000sr_program_container1 otr000sr_program_container
    boolean structuredContainer
end variables

on otrt01sr_ProgramInterfaceOtrt01pg1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on otrt01sr_ProgramInterfaceOtrt01pg1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

