HA$PBExportHeader$otrt01sr_programinterface1.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type otrt01sr_ProgramInterface1 from nonvisualobject
    end type
end forward

global type otrt01sr_ProgramInterface1 from nonvisualobject
end type

type variables
    otrt01sr_ProgramInterfaceOtrt01ci1 otrt01ci
    otrt01sr_ProgramInterfaceOtrt01pg1 otrt01pg
    otrt01sr_ProgramInterfaceOtrt01in1 otrt01in
    otrt01sr_ProgramInterfaceOtrt01ot1 otrt01ot
    boolean channel
end variables

on otrt01sr_ProgramInterface1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on otrt01sr_ProgramInterface1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

