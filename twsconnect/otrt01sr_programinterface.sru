HA$PBExportHeader$otrt01sr_programinterface.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type otrt01sr_ProgramInterface from nonvisualobject
    end type
end forward

global type otrt01sr_ProgramInterface from nonvisualobject
end type

type variables
    otrt01sr_ProgramInterfaceOtrt01ci otrt01ci
    otrt01sr_ProgramInterfaceOtrt01pg otrt01pg
    otrt01sr_ProgramInterfaceOtrt01in otrt01in
    otrt01sr_ProgramInterfaceOtrt01ot otrt01ot
    boolean channel
end variables

on otrt01sr_ProgramInterface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on otrt01sr_ProgramInterface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

