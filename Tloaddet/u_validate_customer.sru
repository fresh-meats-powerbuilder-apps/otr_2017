HA$PBExportHeader$u_validate_customer.sru
forward
global type u_validate_customer from nonvisualobject
end type
end forward

global type u_validate_customer from nonvisualobject
end type
global u_validate_customer u_validate_customer

forward prototypes
public function boolean nf_validate_customer (character customer_type, string customer_id)
end prototypes

public function boolean nf_validate_customer (character customer_type, string customer_id);// 03/2002 ibdkkam Removed quotes for SQL Server

integer li_customer_count

Connect Using SQLCA;

if customer_type = "S" then
  	SELECT count(customers.customer_id  )
    	INTO :li_customer_count  
  	FROM customers 
	 	WHERE customers.customer_id = :customer_id;
  
  	if li_customer_count >= 1 then
		return true
  	else
			return false
  	end if
end if

if customer_type = "B" then
  SELECT count(billtocust.bill_to_id)
    INTO :li_customer_count  
  FROM billtocust 
	 WHERE billtocust.bill_to_id = :customer_id;
  
  if li_customer_count >= 1 then
		return true
  else
		return false
  end if
		
end if

Disconnect Using SQLCA;

end function

on u_validate_customer.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_validate_customer.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

