HA$PBExportHeader$w_load_detail_filter.srw
forward
global type w_load_detail_filter from w_base_response_ext
end type
type dw_load_detail_filter from datawindow within w_load_detail_filter
end type
end forward

global type w_load_detail_filter from w_base_response_ext
integer x = 494
integer y = 612
integer width = 2075
integer height = 704
string title = "Load Detail Filter"
long backcolor = 12632256
dw_load_detail_filter dw_load_detail_filter
end type
global w_load_detail_filter w_load_detail_filter

type variables

end variables

on w_load_detail_filter.create
int iCurrent
call super::create
this.dw_load_detail_filter=create dw_load_detail_filter
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_load_detail_filter
end on

on w_load_detail_filter.destroy
call super::destroy
destroy(this.dw_load_detail_filter)
end on

event ue_base_cancel;call super::ue_base_cancel;String	ls_return_empty

ls_return_empty = ""

CloseWithReturn( this, ls_return_empty)

Return
 

end event

event ue_base_ok;call super::ue_base_ok;Boolean     lb_parameter_ind

char     	lc_order_number[7], &
				lc_bol_number[9], &
	         lc_customer_id[7], &
	         lc_customer_po[18]
String		ls_filter

lb_parameter_ind = FALSE

If dw_load_detail_filter.AcceptText() = 1 Then

   lc_order_number[] = " "
   lc_bol_number[]   = " "
   lc_customer_id[]	= " "
   lc_customer_po[]  = " "

   lc_order_number	= dw_load_detail_filter.object.order_number[1]
   lc_bol_number  = dw_load_detail_filter.object.bol_number[1]
   lc_customer_id	= dw_load_detail_filter.object.customer_number[1]
   lc_customer_po = dw_load_detail_filter.object.customer_po[1]

   IF Trim(lc_order_number[1]) > " " Then
      lc_bol_number  = " "
      lc_customer_id = " "
      lc_customer_po = " "
      lb_parameter_ind = TRUE
   End if

   IF Trim(lc_bol_number[1]) > " " Then
      lc_order_number    = " "
      lc_customer_id = " "
      lc_customer_po = " "
      lb_parameter_ind = TRUE
   End if


   IF lc_customer_id[1] > " " and not IsNull(lc_customer_id[1]) Then
      If lc_customer_po[1] > " " and not IsNull(lc_customer_po[1])Then  
         lc_order_number    = " "
         lc_bol_number = " "
         lb_parameter_ind = TRUE
      Else
         MessageBox("Invalid Parameters", "Please enter a Customer P.O. Number")
			dw_load_detail_filter.setfocus()
         dw_load_detail_filter.setcolumn("customer_po")
			Return
      End if
   End if
	
	IF lc_customer_po[1] > " " and not IsNull(lc_customer_po[1])Then  
	   If lc_customer_id[1] > " " and not IsNull(lc_customer_id[1]) Then
         lc_order_number    = " "
         lc_bol_number = " "
         lb_parameter_ind = TRUE
      Else
         MessageBox("Invalid Parameters", "Please enter a Customer Number")
         dw_load_detail_filter.setfocus()
			dw_load_detail_filter.setcolumn("customer_number")
		   Return
      End If
   End IF

ls_filter = lc_order_number + '~t' + lc_bol_number + '~t' + lc_customer_id + '~t' + lc_customer_po
   IF lb_parameter_ind = TRUE Then
       CloseWithReturn( this, ls_filter) 
   ELSE
       MessageBox("Invalid Parameters", "Please enter Inquire Parameters")
       dw_load_detail_filter.setfocus()
		 dw_load_detail_filter.setcolumn("order_number")
		 Return
   End if
Else
    MessageBox("Invalid Parameters", "Please enter Inquire Parameters")
    dw_load_detail_filter.setfocus()
	 dw_load_detail_filter.setcolumn("order_number")
	 Return
End if


end event

event open;call super::open;String	ls_filter, &
			ls_load, &
			ls_bol, &
			ls_cust_num, &
			ls_cust_po, &
			ls_temp
			
Integer	li_count
			
ls_filter = Message.StringParm

li_count = 1
DO while li_count <= 4
	ls_temp = iw_frame.iu_string.nf_getToken(ls_filter, '~t')
	CHOOSE CASE li_count
		CASE 1
			ls_load = ls_temp
		CASE 2
			ls_bol = ls_temp
		CASE 3 
			ls_cust_num = ls_temp
		CASE 4
			ls_cust_po = ls_temp
	END CHOOSE
	li_count++
LOOP


dw_load_detail_filter.InsertRow(0)

dw_load_detail_filter.object.order_number[1] = ls_load
dw_load_detail_filter.object.bol_number[1] = ls_bol
dw_load_detail_filter.object.customer_number[1] = ls_cust_num
dw_load_detail_filter.object.customer_po[1] = ls_cust_po

dw_load_detail_filter.SetFocus()
dw_load_detail_filter.SetColumn("order_number")
dw_load_detail_filter.SelectText(1, 999)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_load_detail_filter
integer x = 1125
integer y = 440
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_load_detail_filter
integer x = 800
integer y = 440
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_load_detail_filter
integer x = 475
integer y = 440
integer taborder = 20
end type

event cb_base_ok::getfocus;call super::getfocus;IF  KeyDown(KeyTab!) Then
   IF Not dw_load_detail_filter.GetColumnName() = "customer_po" Then
      dw_load_detail_filter.SetFocus()
   End IF
End IF 

end event

type dw_load_detail_filter from datawindow within w_load_detail_filter
integer x = 55
integer y = 44
integer width = 1975
integer height = 348
integer taborder = 10
string dataobject = "d_load_det_filter"
boolean border = false
boolean livescroll = true
end type

event itemchanged;Integer        li_CurrentColumn


li_CurrentColumn = This.GetColumn()

If li_CurrentColumn = 3 Then
   This.SetColumn("customer_po")
Else
  cb_base_ok.SetFocus( ) 
End if
end event

event itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, 999)

end event

