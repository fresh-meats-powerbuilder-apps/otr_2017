HA$PBExportHeader$w_load_detail.srw
forward
global type w_load_detail from w_base_sheet_ext
end type
type tab_load_detail from tab within w_load_detail
end type
type tabpage_load_detail from userobject within tab_load_detail
end type
type gb_1 from groupbox within tabpage_load_detail
end type
type dw_load_detail_hdr from u_base_dw_ext within tabpage_load_detail
end type
type dw_stop_detail_info from u_base_dw_ext within tabpage_load_detail
end type
type dw_check_call from u_base_dw_ext within tabpage_load_detail
end type
type tabpage_load_detail from userobject within tab_load_detail
gb_1 gb_1
dw_load_detail_hdr dw_load_detail_hdr
dw_stop_detail_info dw_stop_detail_info
dw_check_call dw_check_call
end type
type tabpage_check_calls from userobject within tab_load_detail
end type
type dw_check_call_info from u_base_dw_ext within tabpage_check_calls
end type
type tabpage_check_calls from userobject within tab_load_detail
dw_check_call_info dw_check_call_info
end type
type tabpage_ship_info from userobject within tab_load_detail
end type
type dw_dispatch_event from u_base_dw within tabpage_ship_info
end type
type dw_ship_info from u_base_dw_ext within tabpage_ship_info
end type
type tabpage_ship_info from userobject within tab_load_detail
dw_dispatch_event dw_dispatch_event
dw_ship_info dw_ship_info
end type
type tabpage_stop_info from userobject within tab_load_detail
end type
type dw_stop_info from u_base_dw_ext within tabpage_stop_info
end type
type tabpage_stop_info from userobject within tab_load_detail
dw_stop_info dw_stop_info
end type
type tabpage_single_appt from userobject within tab_load_detail
end type
type dw_single_appt from u_base_dw_ext within tabpage_single_appt
end type
type tabpage_single_appt from userobject within tab_load_detail
dw_single_appt dw_single_appt
end type
type tabpage_instructions from userobject within tab_load_detail
end type
type dw_instructions from u_base_dw_ext within tabpage_instructions
end type
type tabpage_instructions from userobject within tab_load_detail
dw_instructions dw_instructions
end type
type tab_load_detail from tab within w_load_detail
tabpage_load_detail tabpage_load_detail
tabpage_check_calls tabpage_check_calls
tabpage_ship_info tabpage_ship_info
tabpage_stop_info tabpage_stop_info
tabpage_single_appt tabpage_single_appt
tabpage_instructions tabpage_instructions
end type
end forward

global type w_load_detail from w_base_sheet_ext
integer x = 7
integer y = 29
integer width = 3471
integer height = 1674
string title = "Load Detail"
long backcolor = 12632256
tab_load_detail tab_load_detail
end type
global w_load_detail w_load_detail

type variables
Integer    			il_ClickedRow, &
						ii_rc, &
               	ii_error_column
			
Long					il_row, &
						il_LastRow, &
						il_xpos, &
						il_ypos, &
						il_error_row

String				is_lastcolumn, &
						is_description, &
						is_closemessage_txt, &
               	is_load_key, &
               	is_group_id           

boolean           ib_add, &
                  ib_modify, &
                  ib_inquire, &
                  ib_delete, &
	           		ib_new_check_call, &
                  ib_new_stop_info, &
                  ib_new_ship_info, &
                  ib_new_appt, &
                  ib_new_instruction, &
                  ib_valid_stop_number = true, &
	           		ib_new_edi_review

char              ic_order[7], &
                  ic_bol[9], &
                  ic_cust_num[7], &
	           		ic_cust_po[12]

u_otr002          iu_otr002
u_otr003          iu_otr003
u_otr004          iu_otr004
u_otr005	         iu_otr005
u_otr006	         iu_otr006


 
DataWindowChild  	idwc_latecode
DataWindowChild  	idwc_address_code

u_ws_review_queue_load_list  iu_ws_review_queue_load_list
u_ws_appointments iu_ws_appointments

Application			ia_apptool
w_tooltip			iw_tooltip


end variables

forward prototypes
public function integer wf_changed ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function integer wf_check_call_inq (string as_load_key)
public function boolean wf_presave_load ()
public function integer wf_load_single_appt ()
public function boolean wf_update_instruct ()
public function boolean wf_update_appt ()
public function boolean wf_edi_review_inq ()
public function integer wf_load_info_strings ()
public function boolean wf_presave_appt ()
public function boolean wf_update_load ()
end prototypes

public function integer wf_changed ();/*
	purpose:	this function will examine all datawindow controls on the window
				that have update capability against the database.  if anything has changed,
				and the change is valid, a 1 is returned.  if an invalid change has occurred
				then a -1 is returned.  if nothing has changed a 0 is returned.

*/
long	ll_cc_mod_chk, &
		ll_cc_del_chk, &
		ll_stopdet_mod_chk, &
		ll_stopdet_del_chk, &
		ll_ccinfo_mod_chk, &
		ll_ccinfo_del_chk, &
		ll_appt_mod_chk, &
		ll_appt_del_chk, &
		ll_instruct_mod_chk, &
		ll_instruct_del_chk
		

ll_cc_mod_chk = tab_load_detail.tabpage_load_detail.dw_check_call.ModifiedCount()
ll_cc_del_chk = tab_load_detail.tabpage_load_detail.dw_check_call.DeletedCount()
ll_stopdet_mod_chk = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.ModifiedCount()
ll_stopdet_del_chk = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.DeletedCount()
ll_ccinfo_mod_chk = tab_load_detail.tabpage_check_calls.dw_check_call_info.ModifiedCount()
ll_ccinfo_del_chk = tab_load_detail.tabpage_check_calls.dw_check_call_info.DeletedCount()
ll_appt_mod_chk = tab_load_detail.tabpage_single_appt.dw_single_appt.ModifiedCount()
ll_appt_del_chk = tab_load_detail.tabpage_single_appt.dw_single_appt.DeletedCount()
ll_instruct_mod_chk = tab_load_detail.tabpage_instructions.dw_instructions.ModifiedCount()
ll_instruct_del_chk = tab_load_detail.tabpage_instructions.dw_instructions.DeletedCount()
	

IF tab_load_detail.tabpage_load_detail.dw_check_call.ModifiedCount() + &
	tab_load_detail.tabpage_load_detail.dw_check_call.DeletedCount() + & 
   tab_load_detail.tabpage_load_detail.dw_stop_detail_info.ModifiedCount() + &
	tab_load_detail.tabpage_load_detail.dw_stop_detail_info.DeletedCount() + &	
   tab_load_detail.tabpage_check_calls.dw_check_call_info.ModifiedCount() + &
	tab_load_detail.tabpage_check_calls.dw_check_call_info.DeletedCount() + &	
   tab_load_detail.tabpage_single_appt.dw_single_appt.ModifiedCount() + &
	tab_load_detail.tabpage_single_appt.dw_single_appt.DeletedCount() + &	
   tab_load_detail.tabpage_instructions.dw_instructions.ModifiedCount() + &
	tab_load_detail.tabpage_instructions.dw_instructions.DeletedCount() > 0 THEN	
	is_closemessage_txt = "Save Changes?"
   RETURN 1
END IF


// nothing has been changed
RETURN 0
	
end function

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer, &
			li_count

String   ls_detail_filter, &
			ls_detail, &
			ls_temp
	 
li_rc = wf_Changed()

CHOOSE CASE li_rc
	CASE  1	//	a valid change occurred ...
		li_answer = MessageBox("Wait",is_closemessage_txt,question!,yesnocancel!)
		CHOOSE CASE li_answer
			CASE 1	//	yes, save changes
				IF wf_Update() THEN	//	the save succeeded
				ELSE	//	the save failed
					li_answer = MessageBox("Save Failed","Retrieve Anyway?",question!,yesno!)
					IF li_answer = 1 THEN	//	retrieve w/o saving changes
					ELSE	//	cancel the close
						Return( false )
					END IF
				END IF	
			CASE 2 	//	retrieve w/o saving changes
			CASE 3	//	cancel the retrieve
				Return(false)
		END CHOOSE
	CASE  -1 		//	a validation error occurred
		li_answer = MessageBox("Data Entry Error","Retrieve w/o Save?",question!,okcancel!)
		CHOOSE CASE li_answer
			CASE 1	//	retrieve w/o saving changes
			CASE 2	//	cancel the retrieve
				Return( false )
		END CHOOSE
	CASE  -2 	//	a serious validation error, window may not close in this state
		MessageBox("A Serious Error Has Occurred","Correct Error Before Retrieving!",exclamation!,ok!)
		Return(False)
	CASE ELSE	//	nothing has changed
END CHOOSE



tab_load_detail.SelectTab(1)
IF iw_frame.iu_string.nf_isempty(is_load_key) Then
	ls_detail = ic_order + '~t' + ic_bol + '~t' + ic_cust_num + '~t' + ic_cust_po
	 OpenWithParm(w_load_detail_filter, ls_detail, iw_frame )
	 ls_detail_filter = Message.StringParm
    IF iw_frame.iu_string.nf_amiempty(ls_detail_filter) Then
		IF tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.RowCount() > 0 Then
      	 IF Trim(tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.GetItemString(1,"order_number")) > "" Then
         	 Return True
       	 Else
         	 Close(This)
				 Return False
       	 End IF
		Else
          Close(This)
			 Return False
      End IF
	Else
		li_count = 1
		DO while li_count <= 4
			ls_temp = iw_frame.iu_string.nf_getToken(ls_detail_filter, '~t')
			CHOOSE CASE li_count
			CASE 1
				ic_order = ls_temp
			CASE 2
				ic_bol = ls_temp
			CASE 3 
				ic_cust_Num = ls_temp
			CASE 4
				ic_cust_po = ls_temp
			END CHOOSE
			li_count++
		LOOP
End If
	 
End If

//Sets boolean so when a tabpage is selected it will retrieve new data
ib_new_check_call = true
ib_new_ship_info = true
ib_new_stop_info = true
ib_new_appt = true
ib_new_instruction = true
ib_new_edi_review = true

SetPointer(HourGlass!)

// Turn the redraw off for the window while we retrieve
This.SetRedraw(FALSE)

// Populate the datawindows
This.wf_load_info_strings() 

This.SetRedraw(TRUE)

Return TRUE



end function

public function boolean wf_update ();boolean           lb_return


IF  Tab_load_detail.SelectedTab = 5 Then
   lb_return = wf_update_appt()
Else
	IF  Tab_load_detail.SelectedTab = 6 Then
   	lb_return = wf_update_instruct()
	else 
   	lb_return = wf_update_load()
	End if
End IF

If lb_return = FALSE Then
   Return False
Else
	Return True
End If


end function

public function integer wf_check_call_inq (string as_load_key);integer               li_rtn, & 
                      li_RPCrtn							 

string                ls_check_call_inq_string
                     					 
s_error               lstr_Error_Info
 

SetPointer( HourGlass! )

lstr_error_info.se_event_name = "wf_check_call_inq"
lstr_error_info.se_window_name = "w_load_detail"
lstr_error_info.se_procedure_name = "wf_check_call_inq"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)
 
ls_check_call_inq_string	= Space(119)

//tab_load_detail.tabpage_load_detaiL.dw_check_call.Object.stop_code.Protect=0 
//tab_load_detail.tabpage_load_detaiL.dw_check_call.Object.stop_code.Background.Color = 16777215 

 
tab_load_detail.tabpage_load_detail.dw_check_call.ResetUpdate()

//li_rtn = iu_otr003.nf_otrt13ar(as_load_key, ls_check_call_inq_string, &
//           lstr_error_info, 0)


// Check the return code from the above function call
IF li_rtn < 0 THEN
 	li_RPCrtn = SQLCA.nf_CheckRPCError(li_rtn, lstr_Error_Info, 0)
	return(-1)
ELSE
      iw_frame.SetMicroHelp( "Ready...")
END IF

li_rtn = tab_load_detail.tabpage_load_detail.dw_check_call.ImportString(Trim(ls_check_call_inq_string))
IF li_rtn = 0 then  
   return 1
END IF

tab_load_detail.tabpage_load_detail.dw_check_call.SetItemStatus (1, 0, Primary!, DataModified! )
tab_load_detail.tabpage_load_detail.dw_check_call.ResetUpdate()
tab_load_detail.tabpage_load_detail.dw_check_call.InsertRow(0)
tab_load_detail.tabpage_load_detail.dw_check_call.ScrollToRow(2)
tab_load_detail.tabpage_load_detail.dw_check_call.SetItem(2, "stop_code", "01") 
tab_load_detail.tabpage_load_detail.dw_check_call.SetItem(2, "cc_date", Today()) 
tab_load_detail.tabpage_load_detail.dw_check_call.SetItemStatus (2, 0, Primary!, NotModified!)
//tab_load_detail.tabpage_load_detaiL.dw_check_call.Object.stop_code.Protect=0 
//tab_load_detail.tabpage_load_detaiL.dw_check_call.Object.stop_code.Background.Color = 16777215 
 
Return(1)      
end function

public function boolean wf_presave_load ();date		 ld_cc_date
		  
integer   li_late_reason_err_ind

long      ll_Row

string	 ls_cc_city, &
          ls_cc_state, &
          ls_stop_code

time      lt_cc_time

ll_Row = 0

//tab_load_detail.tabpage_load_detail.dw_check_call.AcceptText()

ll_Row = tab_load_detail.tabpage_load_detail.dw_check_call.GetNextModified(ll_Row, PRIMARY!)

SetPointer(HourGlass!)

Do While ll_row <> 0
      ls_stop_code = tab_load_detail.tabpage_load_detail.dw_check_call.object.stop_code[ll_Row]
      If IsNull(ls_stop_code) Then
       	MessageBox("Stop Code", "Please enter a Stop Code") 
         tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("stop_code")     
	      Return FALSE
      End if
      li_late_reason_err_ind = tab_load_detail.tabpage_load_detail.dw_check_call.GetItemNumber(ll_Row, "late_reason_err_ind")
      If li_late_reason_err_ind = 1 Then
       	MessageBox("Late Reason Code Error", "Please enter a valid Late Reason Code") 
         tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("late_reason")     
	      Return FALSE
      End if
      ld_cc_date = tab_load_detail.tabpage_load_detail.dw_check_call.GetItemDate(ll_Row, "cc_date")
      If IsNull(ld_cc_date) Then
       	MessageBox("Check Call Date", "Please enter a Check Call Date") 
         tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("cc_date")     
	      Return FALSE
      End if
      If IsDate(String(ld_cc_date)) = FALSE then      
        	MessageBox("Check Call Date", "Please enter a Check Call Date") 
         tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("cc_date")   
         Return FALSE
      End if
      lt_cc_time = tab_load_detail.tabpage_load_detail.dw_check_call.GetItemTime(ll_Row, "cc_time")
      If IsTime(String(lt_cc_time)) = FALSE then
       	MessageBox("Check Call Time", "Please enter a Check Call Time") 
         tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("cc_time")     
	      Return FALSE
      End if
      If String(lt_cc_time) = "00:00:00"  then
       	MessageBox("Check Call Time", "Please enter a Check Call Time") 
         tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("cc_time")     
	      Return FALSE
      End if
      ls_cc_city = tab_load_detail.tabpage_load_detail.dw_check_call.object.city[ll_Row]
      If IsNull(ls_cc_city)  then
         MessageBox("Check Call City", "Please enter a Check Call City") 
         tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("city") 
         Return FALSE    
      End if 
      If ls_cc_city = "                         "  then
         MessageBox("Check Call City", "Please enter a Check Call City") 
         tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("city") 
         Return FALSE    
      End if  
      ls_cc_state = tab_load_detail.tabpage_load_detail.dw_check_call.object.state[ll_Row]
      If IsNull(ls_cc_state)  then
         MessageBox("Check Call State", "Please enter a Check Call State") 
         tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("state") 
         Return FALSE    
      End if 
      If ls_cc_state = "  "  then
         MessageBox("Check Call State", "Please enter a Check Call State") 
         tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("state") 
         Return FALSE    
      End if  

ll_Row = tab_load_detail.tabpage_load_detail.dw_check_call.GetNextModified(ll_Row, PRIMARY!)
Loop 


ll_Row = 0

//tab_load_detail.tabpage_load_detail.dw_stop_detail_info.AcceptText()

ll_Row = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.GetNextModified(ll_Row, PRIMARY!)


Do While ll_row <> 0
      li_late_reason_err_ind = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.GetItemNumber(ll_Row, "late_reason_err_ind")
      If li_late_reason_err_ind = 1 Then
       	MessageBox("Late Reason Code Error", "Please enter a valid Late Reason Code") 
         tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetColumn("late_reason")     
	      Return FALSE
      End if

ll_Row = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.GetNextModified(ll_Row, PRIMARY!)
Loop 

Return TRUE
end function

public function integer wf_load_single_appt ();integer  li_rtn
			
string   ls_appt_update_string, &
      	ls_carrier_code, &
         ls_from_ship_date, &
         ls_to_ship_date, &
         ls_refetch_load_key, &
         ls_refetch_stop_code, &
         ls_refetch_delv_date, &
         ls_date_ind, &
			ls_load_key

    
SetPointer( HourGlass! )

// Turn the redraw off for the window while we retrieve
This.SetRedraw(FALSE)

s_error  lstr_Error_Info

tab_load_detail.tabpage_single_appt.dw_single_appt.Reset()

ls_load_key = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]

lstr_error_info.se_event_name = "wf_retrieve"
lstr_error_info.se_window_name = "w_single_appt_update"
lstr_error_info.se_procedure_name = "wf_retrieve"
lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_message = Space(71)

ls_refetch_load_key   = Space(7)
ls_refetch_stop_code  = Space(2)
ls_refetch_delv_date  = Space(10)
ls_date_ind           = Space(1)


Do

   ls_appt_update_string = Space(2926)
 	
//	li_rtn = iu_otr002.nf_otrt18ar(ls_carrier_code, ls_from_ship_date,&
//      		ls_to_ship_date, ls_load_key, ls_date_ind, &
//            "  " ,ls_appt_update_string, ls_refetch_load_key, &
//            ls_refetch_stop_code, ls_refetch_delv_date, lstr_error_info, 0)

	li_rtn = iu_ws_appointments.uf_otrt18er(ls_carrier_code, ls_from_ship_date,&
      		ls_to_ship_date, ls_load_key, ls_date_ind, &
            "  " ,ls_appt_update_string, lstr_error_info)

// Check the return code from the above function call
If li_rtn <> 0 Then
		tab_load_detail.tabpage_single_appt.dw_single_appt.ResetUpdate()
		This.SetRedraw(TRUE)
      Return -1
ELSE
      iw_frame.SetMicroHelp( "Ready...")
END IF

     li_rtn = tab_load_detail.tabpage_single_appt.dw_single_appt.ImportString(Trim(ls_appt_update_string))

loop while len(Trim(ls_refetch_load_key)) > 0

ls_carrier_code = tab_load_detail.tabpage_single_appt.dw_single_appt.GetItemString(1,"carrier_code") 

tab_load_detail.tabpage_single_appt.dw_single_appt.SetFocus()

tab_load_detail.tabpage_single_appt.dw_single_appt.ResetUpdate()

// Turn the redraw on for the window after we retrieve
This.SetRedraw(TRUE)

SetPointer( Arrow! )

Return 1



end function

public function boolean wf_update_instruct ();
Integer  li_rtn

long		ll_row, ll_cur_row

string   ls_req_load_key, &   
         ls_req_order, &
         ls_ffw_code, &
			ls_str

s_error  lstr_Error_Info

SetPointer(HourGlass!)

tab_load_detail.tabpage_instructions.dw_instructions.AcceptText()  

ll_row = tab_load_detail.tabpage_instructions.dw_instructions.GetNextModified(0, Primary!)

IF ll_row = 0 Then
	SetMicroHelp("No Modified Data.")
	Return True
Else

    ls_str = tab_load_detail.tabpage_instructions.dw_instructions.&
	          GetItemString(1,"frtforwd_code")
    IF Trim(ls_str) <> "" THEN 
		 ll_cur_row = idwc_address_code.Find ( "address_code='"+ &
	              ls_str+"'", 1, idwc_address_code.RowCount() )
       
    IF ll_cur_row < 1 THEN
       MessageBox( "Freight Forwarder Error", &
	                ls_str + " is Not a Valid Freight Forwarder code." )
       Return False
    else
	     ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_name")
	     tab_load_detail.tabpage_instructions.dw_instructions.setitem(ll_row, "frtforwd_name", ls_str)
	     ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_city")
	     tab_load_detail.tabpage_instructions.dw_instructions.setitem(ll_row, "frtforwd_city", ls_str)
	     ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_state")
	     tab_load_detail.tabpage_instructions.dw_instructions.setitem(ll_row, "frtforwd_state_abrv", ls_str)
    End if
End if
   ls_req_load_key = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]          
   ls_req_order = tab_load_detail.tabpage_instructions.dw_instructions.object.order_num[ll_Row] 
   ls_ffw_code = tab_load_detail.tabpage_instructions.dw_instructions.object.frtforwd_code[ll_Row]
	
   lstr_error_info.se_event_name = "wf_update_instruct"
   lstr_error_info.se_window_name = "w_instructions"
   lstr_error_info.se_procedure_name = "wf_update_instruct"
   lstr_error_info.se_user_id = SQLCA.UserID
   lstr_error_info.se_message = Space(71)
/*
   li_rtn = iu_otr005.nf_otrt57ar(ls_req_load_key, ls_req_order, &
 	 	ls_ffw_code, lstr_error_info, 0)
*/

	li_rtn = iu_ws_review_queue_load_list.nf_otrt57er(ls_req_load_key, ls_req_order, &
 	 	ls_ffw_code, lstr_error_info)
		  
// Check the return code from the above function call

   If li_rtn <> 0 Then
	   Return False 
   End If
 	  
 End if
tab_load_detail.tabpage_instructions.dw_instructions.ResetUpdate()
ib_new_instruction = True
SetMicroHelp("Update Processing Complete.")

return TRUE
end function

public function boolean wf_update_appt ();Integer  li_return_error_code, &    
         li_rtn
			
long     ll_Row
	
string   ls_return_error_msg, &   
      	ls_hours, &
         ls_minutes, &
         ls_appt_contact, &
         ls_load_key, &
         ls_stop_code, &
         ls_override_ind, & 
         ls_notify_carrier_ind, &
         ls_appt_date, &
         ls_appt_time, &
         ls_eta_date, &
         ls_eta_time

time     lt_time

s_error  lstr_Error_Info

If wf_presave_appt() = TRUE Then  

SetPointer(HourGlass!)
tab_load_detail.tabpage_single_appt.dw_single_appt.SetRedraw(False)

ll_row = tab_load_detail.tabpage_single_appt.dw_single_appt.GetNextModified(0, Primary!)

IF ll_row = 0 Then
	SetMicroHelp("No Modified Data.")
	tab_load_detail.tabpage_single_appt.dw_single_appt.SetRedraw(True)
	Return True
End IF

do while ll_row <> 0
	ls_load_key = tab_load_detail.tabpage_single_appt.dw_single_appt.object.load_key[ll_Row]           
   ls_stop_code = tab_load_detail.tabpage_single_appt.dw_single_appt.object.stop_code[ll_Row] 
   ls_appt_contact = tab_load_detail.tabpage_single_appt.dw_single_appt.object.appt_contact[ll_Row] 
	ls_appt_contact = ls_appt_contact + space(10 - Len(ls_appt_contact))
   ls_eta_date = String(tab_load_detail.tabpage_single_appt.dw_single_appt.object.appt_date[ll_Row], "yyyymmdd")
	ls_appt_date = ls_eta_date
   lt_time = tab_load_detail.tabpage_single_appt.dw_single_appt.object.appt_time[ll_Row]
	ls_hours = Mid(String(lt_time,"hh:mm"), 1, 2) 
   ls_minutes = Mid(String(lt_time,"hh:mm"), 4, 2)
   ls_appt_time = ls_hours + ls_minutes 
   ls_eta_time = ls_appt_time 
   ls_override_ind = tab_load_detail.tabpage_single_appt.dw_single_appt.object.override_ind[ll_Row] 
   ls_notify_carrier_ind = tab_load_detail.tabpage_single_appt.dw_single_appt.object.notify_carrier_ind[ll_Row] 
   ls_return_error_msg = space(80)
	
   lstr_error_info.se_event_name = "wf_update"
   lstr_error_info.se_window_name = "w_mass_appt_update"
   lstr_error_info.se_procedure_name = "wf_update"
   lstr_error_info.se_user_id = SQLCA.UserID
   lstr_error_info.se_message = Space(71)

  //Added code for the netwise convertion
/*   li_rtn = iu_otr002.nf_otrt19ar(ls_load_key, ls_stop_code, &
 	 	ls_appt_contact, ls_appt_date, ls_appt_time, ls_eta_date, &
	   ls_eta_time, ls_override_ind, ls_notify_carrier_ind, &
      li_return_error_code, ls_return_error_msg, lstr_error_info, 0)
*/
 li_rtn =iu_ws_appointments.uf_otrt19er(ls_load_key, ls_stop_code, &
 	 	ls_appt_contact, ls_appt_date, ls_appt_time, ls_eta_date, &
	   ls_eta_time, ls_override_ind, ls_notify_carrier_ind,lstr_error_info)	

// Check the return code from the above function call

   If li_rtn = 0 or li_rtn = 1 Then
      tab_load_detail.tabpage_single_appt.dw_single_appt.DeleteRow(ll_Row) 
      ll_Row --
   Else 
      If li_rtn < 0 Then
         tab_load_detail.tabpage_single_appt.dw_single_appt.SetRow(ll_row)
 	      tab_load_detail.tabpage_single_appt.dw_single_appt.SetRedraw(True)
         Return False 
      Else
			tab_load_detail.tabpage_single_appt.dw_single_appt.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
         If SQLCA.ii_messagebox_rtn = 3 Then
            tab_load_detail.tabpage_single_appt.dw_single_appt.SetRow(ll_row)
            tab_load_detail.tabpage_single_appt.dw_single_appt.SetRedraw(True)
            Return False
         End If 
      End If  
    End If
 	  
	ll_row = tab_load_detail.tabpage_single_appt.dw_single_appt.GetNextModified(ll_row, PRIMARY!)
loop

ib_new_appt = True

End if



tab_load_detail.tabpage_single_appt.dw_single_appt.SetColumn("carrier_code")

SetMicroHelp("Update Processing Complete.")

tab_load_detail.tabpage_single_appt.dw_single_appt.SetRedraw(True)
Return TRUE
end function

public function boolean wf_edi_review_inq ();//boolean  lb_rtn
//
//string   ls_req_load_key, &
//         ls_req_from_carr, &
//			ls_req_to_carr, &
//			ls_req_status_code, &
//			ls_req_from_date, &
//			ls_req_to_date, &
//			ls_req_process_code
//			
//s_error  lstr_Error_Info
//						
//// Turn the redraw off for the window while we retrieve
////tab_load_detail.tabpage_edi.dw_214_review.SetRedraw(FALSE)
//
//
////tab_load_detail.tabpage_edi.dw_214_review.InsertRow(0)
//
//ls_req_load_key = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
////ls_req_order = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.order_number[1]
//
//lstr_error_info.se_event_name = "ue_retrieve"
//lstr_error_info.se_window_name = "load_detail tabpage_edi"
//lstr_error_info.se_procedure_name = "dw_214_review"
//lstr_error_info.se_user_id = SQLCA.UserID
//lstr_error_info.se_message = Space(71)
//
//ls_req_from_carr     = Space(4)
//ls_req_to_carr       = Space(4)
//ls_req_status_code   = space(2)
//ls_req_from_date     = space(10)
//ls_req_to_date       = space(10)
//ls_req_process_code  = space(2)
//
//
//lb_rtn = iu_otr006.nf_otrt70ar(ls_req_load_key, &
//										 ls_req_from_carr, &
//										 ls_req_to_carr, &
//										 ls_req_status_code, &
//        								 ls_req_from_date, &
//										 ls_req_to_date, &
//										 ls_req_process_code, &
//        								 tab_load_detail.tabpage_edi.dw_214_review, &
//										 lstr_error_info)
//
//// Check the return code from the above function call
//If lb_rtn = False Then
//		tab_load_detail.tabpage_edi.dw_214_review.ResetUpdate()
//		This.SetRedraw(TRUE)
//      Return False
//ELSE
//      iw_frame.SetMicroHelp( "Ready...")
//END IF
//
//
//tab_load_detail.tabpage_edi.dw_214_review.SetFocus()
//
//tab_load_detail.tabpage_edi.dw_214_review.ResetUpdate()
//
//// Turn the redraw on for the window after we retrieve
//This.SetRedraw(TRUE)
//
//SetPointer( arrow! )
//
Return True
//
//
//
end function

public function integer wf_load_info_strings ();boolean               lb_add, &
                      lb_delete, &
                      lb_modify, &
                      lb_inquire

integer               li_rtn


string                ls_load_detail_inq_string, &
                      ls_check_call_inq_string, &
                      ls_stop_detail_inq_string, &
                      ls_total_stops, &
                      ls_stop_number, &
                      ls_order_number, &     
                      ls_req_load_key, &
                      ls_req_order_number, &
 							 ls_req_bol_number, &
						    ls_req_customer_id, &
						    ls_req_customer_po, &
							 ls_carrier, &
							 ls_key

s_error               lstr_Error_Info

//SetRedraw(false)
lstr_error_info.se_event_name = "wf_load_info_strings"
lstr_error_info.se_window_name = "w_load_detail"
lstr_error_info.se_procedure_name = "wf_load_info_strings"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

iw_frame.iu_netwise_data.nf_getaccess(Classname(), lb_add, lb_modify, lb_inquire, lb_delete)

ls_req_order_number = ic_order
ls_req_customer_po = ic_cust_po
ls_req_bol_number = ic_bol
ls_req_customer_id = ic_cust_num


IF iw_frame.iu_string.nf_IsEmpty(is_load_key) Then
   ls_req_order_number     = ls_req_order_number
Else
   ls_req_order_number     = is_load_key
End if

tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.Reset()
tab_load_detail.tabpage_load_detail.dw_check_call.Reset()
tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Reset()
// idbkdld increase string
ls_load_detail_inq_string	= Space(177)
ls_check_call_inq_string   = Space(119)
ls_stop_detail_inq_string  = Space(25000)

//li_rtn = iu_otr005.nf_otrt61ar(ls_req_order_number, ls_req_bol_number, &
//           ls_req_customer_id, ls_req_customer_po, ls_load_detail_inq_string, &
//           ls_check_call_inq_string, ls_stop_detail_inq_string, &
//           lstr_error_info, 0)
			  
li_rtn = iu_ws_review_queue_load_list.nf_otrt61er(ls_req_order_number, ls_req_bol_number, &
           ls_req_customer_id, ls_req_customer_po, ls_load_detail_inq_string, &
           ls_check_call_inq_string, ls_stop_detail_inq_string, &
           lstr_error_info)

iw_frame.SetMicroHelp( "Ready")

// Check the return code from the above function call
IF li_rtn < 0 THEN
	iw_frame.SetMicroHelp( "Invalid Attempt" )
   tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.InsertRow(0)
	return(-1)
Else
	iw_frame.SetMicroHelp(lstr_error_info.se_message)
END IF

li_rtn = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.ImportString(Trim(ls_load_detail_inq_string))
tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.ResetUpdate()
If li_rtn < 1 Then tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.InsertRow(0)
If IsNull(tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]) Then
	ls_key = " "
Else
	ls_key = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
End if
IF iw_frame.iu_string.nf_IsEmpty(ls_key) Then
	   tab_load_detail.tabpage_check_calls.Enabled = False
		tab_load_detail.tabpage_ship_info.Enabled = False
		tab_load_detail.tabpage_stop_info.Enabled = False
		tab_load_detail.tabpage_single_appt.Enabled = False
		//tab_load_detail.tabpage_edi.Enabled = False
		//ibdkdld
		iF isNull(tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.order_number[1]) Then
				tab_load_detail.tabpage_instructions.Enabled = False
		Else
				tab_load_detail.tabpage_instructions.Enabled = True
		End If
Else
		tab_load_detail.tabpage_check_calls.Enabled = True
		tab_load_detail.tabpage_ship_info.Enabled = True
		tab_load_detail.tabpage_stop_info.Enabled = True
		tab_load_detail.tabpage_single_appt.Enabled = True
		//tab_load_detail.tabpage_edi.Enabled = True
		tab_load_detail.tabpage_instructions.Enabled = True
End If

ls_carrier = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.carrier[1]

IF message.nf_get_app_id() <> 'OTR' Then 
	tab_load_detail.tabpage_single_appt.Enabled = False
//ElseIF iw_frame.im_menu.m_holding1.m_appointments.m_single.Enabled = False Then
//	tab_load_detail.tabpage_single_appt.Enabled = False
ElseIF iw_frame.im_menu.m_file.m_save.Enabled = False Then
	tab_load_detail.tabpage_single_appt.Enabled = False
//ElseIF iw_frame.iu_string.nf_IsEmpty(ls_carrier) Then
//	tab_load_detail.tabpage_single_appt.Enabled = False
End IF

IF li_rtn < 0 then return 1

ls_order_number = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.order_number[1]

This.Title = "Load Detail for Order " + ls_order_number

li_rtn = tab_load_detail.tabpage_load_detail.dw_check_call.ImportString(Trim(ls_check_call_inq_string))

IF li_rtn < 0 then return 1
 
ls_stop_number = tab_load_detail.tabpage_load_detail.dw_check_call.object.stop_code[1]


IF Trim(ls_stop_number) = ""  Then
    If not ib_modify   Then
         tab_load_detail.tabpage_load_detail.dw_check_call.Modify("DataWindow.ReadOnly=Yes")
         tab_load_detail.tabpage_load_detail.dw_check_call.SetItem(1, "stop_code", "01") 
         tab_load_detail.tabpage_load_detail.dw_check_call.SetItem(1, "miles_out", "     ") 
         tab_load_detail.tabpage_load_detail.dw_check_call.SetItemStatus (1, 0, Primary!, DataModified!)
    Else
         tab_load_detail.tabpage_load_detail.dw_check_call.SetItem(1, "stop_code", "01") 
	      tab_load_detail.tabpage_load_detail.dw_check_call.SetItem(1, "cc_date", Today())
         tab_load_detail.tabpage_load_detail.dw_check_call.SetItem(1, "miles_out", "     ") 
         tab_load_detail.tabpage_load_detail.dw_check_call.SetItemStatus (1, 0, Primary!, NotModified! ) 
    End if
ELSE
   If not ib_modify  Then
         tab_load_detail.tabpage_load_detail.dw_check_call.Modify("DataWindow.ReadOnly=Yes")
   Else
    	tab_load_detail.tabpage_load_detail.dw_check_call.InsertRow(0)
	   tab_load_detail.tabpage_load_detail.dw_check_call.SetItem(2, "stop_code", "01") 
	   tab_load_detail.tabpage_load_detail.dw_check_call.SetItem(2, "cc_date", Today())
      tab_load_detail.tabpage_load_detail.dw_check_call.SetItem(2, "miles_out", "     ") 
	   tab_load_detail.tabpage_load_detail.dw_check_call.ScrollToRow(2)
      tab_load_detail.tabpage_load_detail.dw_check_call.SetItemStatus (2, 0, Primary!, NotModified! )
   End if
END IF

tab_load_detail.tabpage_load_detail.dw_check_call.ResetUpdate() 

li_rtn = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.ImportString(Trim(ls_stop_detail_inq_string))
 
IF li_rtn < 0 then return 1

ls_total_stops =  tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.total_stops[1]

//IF ls_total_stops > "02" THEN 
//	 ls_stop_number = "02" 
//    DO While Integer(ls_stop_number) < Integer(ls_total_stops) 
//       ls_stop_detail_refetch_string  = Space(1101)
//       ls_req_load_key = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
//       li_rtn = iu_otr004.nf_otrt50ar(ls_req_load_key, ls_stop_detail_refetch_string, &
//                ls_refetch_stop_code, lstr_error_info, 0)
//
//      // Check the return code from the above function call
//      IF li_rtn < 0 THEN
//			lstr_error_info.se_event_name = "wf_load_info_strings"
//			lstr_error_info.se_window_name = "w_load_detail"
//			lstr_error_info.se_procedure_name = "wf_load_info_strings"
//			lstr_error_info.se_user_id = SQLCA.Userid
//			lstr_error_info.se_message = Space(71)
//  	      return(-1)
//      END IF
//   
//      li_rtn = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.ImportString(Trim(ls_stop_detail_refetch_string))  
//      ls_stop_number = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.GetItemString(tab_load_detail.tabpage_load_detail.dw_stop_detail_info.RowCount(), "stop_code")
//    
//  LOOP
//END IF 

If not ib_modify    Then
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("DataWindow.ReadOnly=Yes")
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("appt_date.BackGround.Color='12632256'")
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("appt_time.BackGround.Color='12632256'")
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("appt_contact.BackGround.Color='12632256'")
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("late_reason.BackGround.Color='12632256'")
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("carrier_notified_ind.BackGround.Color='12632256'")
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("route_miles.BackGround.Color='12632256'")
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("eta_date.BackGround.Color='12632256'")
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("eta_time.BackGround.Color='12632256'")
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("act_delv_date.BackGround.Color='12632256'")
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("act_delv_time.BackGround.Color='12632256'")
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("stop_comment_ind.BackGround.Color='12632256'")
End if

tab_load_detail.tabpage_load_detail.dw_stop_detail_info.ResetUpdate()


return(0)      
end function

public function boolean wf_presave_appt ();date		 ld_ApptDate
		  
long      ll_Row

string	 ls_ApptContact

time      lt_ApptTime


tab_load_detail.tabpage_single_appt.dw_single_appt.AcceptText()

ll_Row = tab_load_detail.tabpage_single_appt.dw_single_appt.GetNextModified(0, PRIMARY!)

Do while ll_Row <> 0
  ls_ApptContact = tab_load_detail.tabpage_single_appt.dw_single_appt.GetItemString(ll_Row, "appt_contact")
  If IsNull(ls_ApptContact) or LEN(TRIM(ls_apptContact)) = 0 then
     MessageBox("Appointment Contact", "Please enter an Appointment Contact") 
     tab_load_detail.tabpage_single_appt.dw_single_appt.SetColumn("appt_contact")     
	  ii_error_column = tab_load_detail.tabpage_single_appt.dw_single_appt.getcolumn()
     il_error_row = ll_Row  
	  tab_load_detail.tabpage_single_appt.dw_single_appt.postevent("ue_post_tab")
     Return FALSE
  End if
  lt_ApptTime = tab_load_detail.tabpage_single_appt.dw_single_appt.GetItemTime(ll_Row, "appt_time")
  If IsTime(String(lt_ApptTime)) = FALSE then
     MessageBox("Appointment Time", "Please enter an Appointment Time")
     tab_load_detail.tabpage_single_appt.dw_single_appt.SetColumn("appt_Time")
	  ii_error_column = tab_load_detail.tabpage_single_appt.dw_single_appt.getcolumn()
     il_error_row = ll_Row
	  tab_load_detail.tabpage_single_appt.dw_single_appt.postevent("ue_post_tab")
     Return FALSE    
  End if
  If String(lt_ApptTime) > '00:00:00' then
     ld_ApptDate = tab_load_detail.tabpage_single_appt.dw_single_appt.GetItemDate(ll_Row, "appt_date")
     If IsDate(String(ld_ApptDate))= FALSE then      
        MessageBox("Appointment Date", "Please enter an Appointment Date")
        tab_load_detail.tabpage_single_appt.dw_single_appt.SetColumn("appt_date")
		  ii_error_column = tab_load_detail.tabpage_single_appt.dw_single_appt.getcolumn()
        il_error_row = ll_Row
		  tab_load_detail.tabpage_single_appt.dw_single_appt.postevent("ue_post_tab")
        Return FALSE     
     End if
  End If
ll_Row = tab_load_detail.tabpage_single_appt.dw_single_appt.GetNextModified(ll_Row, PRIMARY!)
Loop 

Return TRUE
end function

public function boolean wf_update_load ();boolean           lb_return, &
                  lb_check_call_updated

String         	ls_load_key, &
						ls_stop_code, &
						ls_check_call_date, &
						ls_check_call_time, &
						ls_check_call_city, &
						ls_check_call_state, &
						ls_late_reason_code, &
	               ls_eta_date, &
						ls_eta_time, &
                  ls_no_eta_ind, &
                  ls_eta_contact, &
                  ls_miles_out,  &
                  ls_update_date, &
                  ls_update_time, &
                  ls_appt_date, &
                  ls_appt_time, &
                  ls_appt_contact, &  
                  ls_stop_eta_date, &
                  ls_stop_eta_time, & 
                  ls_actual_delivery_date, &
                  ls_actual_delivery_time, &
                  ls_carrier_notified_ind, &
               	ls_detail_string, &
						ls_update_eta_ind

date              ld_work_date

Integer           li_rtn, &
						li_rc                       

Time              lt_work_time        

long				   ll_Row
						
s_error				lstr_Error_Info

SetPointer(HourGlass!)

lstr_error_info.se_event_name = "wf_update"
lstr_error_info.se_window_name = "w_load_detail"
lstr_error_info.se_procedure_name = "wf_update_load_detail"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

IF tab_load_detail.tabpage_load_detail.dw_check_call.AcceptText() = -1  OR &
   tab_load_detail.tabpage_load_detail.dw_stop_detail_info.AcceptText () = -1 THEN
   Return(False)
End if

lb_check_call_updated = FALSE
lb_return = wf_presave_load()

If lb_return = FALSE Then
   Return False
End If

//tab_load_detail.tabpage_load_detail.dw_check_call.AcceptText()
ll_Row = 0
ll_Row = tab_load_detail.tabpage_load_detail.dw_check_call.GetNextModified( ll_Row, Primary! )
DO WHILE ll_row <> 0
   lb_check_call_updated = True 
     ls_load_key	  		 = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]        
     ls_stop_code 	    = tab_load_detail.tabpage_load_detail.dw_check_call.object.stop_code[ll_Row]		
	  If Len(Trim(ls_stop_code)) = 1 Then
			ls_stop_code = '0' + ls_stop_code
	  END IF
     ld_work_date        = tab_load_detail.tabpage_load_detail.dw_check_call.GetItemDate( ll_Row, "cc_date" )
     ls_check_call_date  = String(ld_work_date, "yyyymmdd")
     lt_work_time        = tab_load_detail.tabpage_load_detail.dw_check_call.GetItemTime( ll_Row, "cc_time" )
     ls_check_call_time  = String(lt_work_time, "hhmm")    
     ls_check_call_city  = tab_load_detail.tabpage_load_detail.dw_check_call.object.city[ll_Row]           
     ls_check_call_state = tab_load_detail.tabpage_load_detail.dw_check_call.object.state[ll_Row]
     ls_late_reason_code = tab_load_detail.tabpage_load_detail.dw_check_call.object.late_reason[ll_Row]
     ls_miles_out        = tab_load_detail.tabpage_load_detail.dw_check_call.object.miles_out[ll_Row]   
     ls_no_eta_ind       = tab_load_detail.tabpage_load_detail.dw_check_call.object.no_eta_ind[ll_Row]
     If ls_no_eta_ind = "X" Then
        ls_eta_date      = "00000000"            
        ls_eta_time      = "0000"
      Else
        ld_work_date     = tab_load_detail.tabpage_load_detail.dw_check_call.GetItemDate( ll_Row, "eta_date" )
        ls_eta_date      = String(ld_work_date, "yyyymmdd") 
        lt_work_time     = tab_load_detail.tabpage_load_detail.dw_check_call.GetItemTime( ll_Row, "eta_time" )
        ls_eta_time      = String(lt_work_time, "hhmm")
     End if
     ls_eta_contact      = tab_load_detail.tabpage_load_detail.dw_check_call.object.eta_contact[ll_Row]
    // li_rtn = iu_otr003.nf_otrt16ar(ls_load_key, ls_stop_code, &
     //     ls_check_call_date, ls_check_call_time, ls_check_call_city, &
      //    ls_check_call_state, ls_late_reason_code, ls_miles_out, &
       //   ls_eta_date, ls_eta_time, ls_eta_contact, ls_update_date, &
       //   ls_update_time, lstr_Error_Info, 0 )
			 
			      li_rtn = iu_ws_review_queue_load_list.nf_otrt16er(ls_load_key, ls_stop_code, &
          ls_check_call_date, ls_check_call_time, ls_check_call_city, &
          ls_check_call_state, ls_late_reason_code, ls_miles_out, &
          ls_eta_date, ls_eta_time, ls_eta_contact, ls_update_date, &
          ls_update_time, lstr_Error_Info)
			 

     CHOOSE CASE li_rtn
		CASE is < 0 
			tab_load_detail.tabpage_load_detail.dw_check_call.Setredraw(TRUE)
			li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
								"  Load Key - " + ls_load_key + " has failed due to an Error.  Do you want to Continue?", &
								Question!, YesNo! )
      CASE  1 
          li_rc = 0
		CASE 2 
          tab_load_detail.tabpage_load_detail.dw_check_call.SetRedraw( True )
          li_rc = MessageBox( "UNABLE TO CALCULATE MILEAGE",  &
					"Please enter the Check Call miles out and save the record again.", &
							Information!, OK! )
          tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("miles_out") 
          Return (False)
		CASE 3
          tab_load_detail.tabpage_load_detail.dw_check_call.SetRedraw( True )
          li_rc = MessageBox( "INVALID ETA DATE",  &
			 		"ETA Date Cannot be less than Scheduled Ship Date.", &
							Information!, OK! )
          tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("eta_date") 
          Return (False)
     	CASE ELSE
			li_rc = 0
     END CHOOSE
 
 tab_load_detail.tabpage_load_detail.dw_check_call.SetItemStatus( ll_Row, 0, Primary!, NotModified! )		
 ll_Row = tab_load_detail.tabpage_load_detail.dw_check_call.GetNextModified( ll_Row, Primary! )
LOOP

If lb_check_call_updated = TRUE Then
	 tab_load_detail.tabpage_load_detail.dw_check_call.Reset()
//    tab_load_detail.tabpage_load_detail.dw_check_call.SetRedraw(True) 
	 ls_detail_string = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
    wf_check_call_inq(ls_detail_string)
    tab_load_detail.tabpage_load_detail.dw_check_call.ResetUpdate()
	 ib_new_check_call = true
End if       

SetPointer(HourGlass!)

//tab_load_detail.tabpage_load_detail.dw_stop_detail_info.AcceptText()
ll_Row = 0
ll_Row = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.GetNextModified( ll_Row, Primary! )
DO WHILE ll_row <> 0
		  ls_load_key	  	        = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.object.load_key[1]
        ls_stop_code 	        = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.object.stop_code[ll_Row]		
        ls_late_reason_code     = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.object.late_reason[ll_Row]
        ls_appt_date            = '          '
        ls_appt_time            = '    '
        ls_appt_contact         = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.object.appt_contact[ll_Row]
        ld_work_date            = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.GetItemDate( ll_Row, "act_delv_date" )
        ls_actual_delivery_date = String(ld_work_date, "yyyy-mm-dd")
        lt_work_time            = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.GetItemTime( ll_Row, "act_delv_time" )
        ls_actual_delivery_time = String(lt_work_time, "hhmm")
        ld_work_date            = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.GetItemDate( ll_Row, "eta_date" )
        ls_stop_eta_date        = String(ld_work_date, "yyyy-mm-dd")
        lt_work_time            = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.GetItemTime( ll_Row, "eta_time" )
        ls_stop_eta_time        = String(lt_work_time, "hhmm")
        ls_carrier_notified_ind = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.object.carrier_notified_ind[ll_Row]
		  ls_update_eta_ind       = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.object.update_eta_ind[ll_Row]
 			
		  if ls_stop_eta_date = "" then 
			  ls_stop_eta_date = "0000000000"
			  ls_stop_eta_time = "0000"
		  End if
			 
		  if ls_update_eta_ind <> 'Y' then 
			  ls_stop_eta_date = space(10)
			  ls_stop_eta_time = space(4)
		  End if
			 

      

    	//Added code for Netwise convertion 09/07/16
        li_rtn = iu_ws_review_queue_load_list.nf_otrt17er(ls_actual_delivery_date,ls_late_reason_code, ls_appt_contact, &
		  ls_stop_eta_date, ls_stop_eta_time, ls_actual_delivery_time, ls_load_key, ls_stop_code, &
              ls_carrier_notified_ind, ls_appt_date, ls_appt_time,lstr_error_info)

		IF ls_stop_code = "01" Then
			tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.SetItem(1, "late_reason", ls_late_reason_code)
		End if
//      tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
		CHOOSE CASE li_rtn
			CASE is < 0 
				tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Setredraw(TRUE)
				li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
										"  Load Key - " + ls_load_key + " has failed due to an Error.  Do you want to Continue?", &
										Question!, YesNo! )
			        If li_rc = 2 Then
                    tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetRow(ll_row)
                    tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetRedraw(True)
                    Return False
				     END IF
			CASE 1
				li_rc = 0
				tab_load_detail.tabpage_load_detail.dw_stop_detail_info.object.update_eta_ind[ll_Row] = space(01)
			CASE 2
				tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Setredraw(TRUE)
		  	   Return(False)
		CASE ELSE
			If SQLCA.ii_messagebox_rtn = 3 Then
               tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetRow(ll_row)
               tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetRedraw(True)
               Return False
				END IF
				li_rc = 0
		END CHOOSE            
     
//tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )		
ll_Row = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.GetNextModified( ll_Row, Primary! )
LOOP

tab_load_detail.tabpage_load_detail.dw_stop_detail_info.ResetUpdate()
tab_load_detail.tabpage_load_detail.dw_check_call.SetRedraw(TRUE)

Return TRUE

end function

on w_load_detail.create
int iCurrent
call super::create
this.tab_load_detail=create tab_load_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_load_detail
end on

on w_load_detail.destroy
call super::destroy
destroy(this.tab_load_detail)
end on

event ue_postopen;call super::ue_postopen;iu_otr002  =  CREATE u_otr002
iu_otr003  =  CREATE u_otr003
iu_otr004  =  CREATE u_otr004
iu_otr005  =  CREATE u_otr005
iu_otr006  =  CREATE u_otr006
iu_ws_review_queue_load_list = CREATE u_ws_review_queue_load_list
iu_ws_appointments = CREATE u_ws_appointments

iu_ws_review_queue_load_list = CREATE u_ws_review_queue_load_list

is_group_id = iw_frame.iu_netwise_data.is_groupid

iw_frame.iu_netwise_data.nf_getaccess(Classname(), ib_add, ib_modify, ib_inquire, ib_delete)

SetRedraw(false)
IF not ib_modify or message.nf_get_app_id() <> 'OTR' Then 	
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("DataWindow.ReadOnly=Yes")
        tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("appt_date.BackGround.Color='12632256'")
        tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("appt_time.BackGround.Color='12632256'")
        tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("appt_contact.BackGround.Color='12632256'")
        tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("late_reason.BackGround.Color='12632256'")
        tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("carrier_notified_ind.BackGround.Color='12632256'")
        tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("route_miles.BackGround.Color='12632256'")
        tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("eta_date.BackGround.Color='12632256'")
        tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("eta_time.BackGround.Color='12632256'")
        tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("act_delv_date.BackGround.Color='12632256'")
        tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("act_delv_time.BackGround.Color='12632256'")
        tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("stop_comment_ind.BackGround.Color='12632256'")
		  tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Modify("update_eta_ind.BackGround.Color='12632256'")
End if
SetRedraw(True)

This.wf_retrieve()

end event

event open;call super::open;// Get the passed load key (if any)
is_Load_Key = Message.StringParm

// Insert a row into each datawindow (cosmetic reason only)
tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.InsertRow(0)
tab_load_detail.tabpage_load_detail.dw_check_call.InsertRow(0)
end event

event close;call super::close;DESTROY iu_otr002
DESTROY iu_otr003
DESTROY iu_otr004
DESTROY iu_otr005
DESTROY iu_otr006
DESTROY iu_ws_review_queue_load_list 
DESTROY iu_ws_appointments

If IsValid(iw_ToolTip) Then 
	Close(iw_ToolTip)
End If

end event

event ue_get_data;call super::ue_get_data;CHOOSE CASE as_value
	CASE 'ToolTip'
		Message.StringParm = is_description
	case  "xpos"
		Message.StringParm = string(il_xpos)
	case  "ypos"
		Message.StringParm = string(il_ypos)
	case else
		Message.StringParm = ''	
END CHOOSE

end event

event deactivate;call super::deactivate;
IF IsValid(iw_tooltip) Then
	Close(iw_tooltip)
END IF
end event

type tab_load_detail from tab within w_load_detail
integer y = 3
integer width = 3423
integer height = 1498
integer taborder = 1
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean raggedright = true
boolean boldselectedtext = true
boolean createondemand = true
integer selectedtab = 1
tabpage_load_detail tabpage_load_detail
tabpage_check_calls tabpage_check_calls
tabpage_ship_info tabpage_ship_info
tabpage_stop_info tabpage_stop_info
tabpage_single_appt tabpage_single_appt
tabpage_instructions tabpage_instructions
end type

on tab_load_detail.create
this.tabpage_load_detail=create tabpage_load_detail
this.tabpage_check_calls=create tabpage_check_calls
this.tabpage_ship_info=create tabpage_ship_info
this.tabpage_stop_info=create tabpage_stop_info
this.tabpage_single_appt=create tabpage_single_appt
this.tabpage_instructions=create tabpage_instructions
this.Control[]={this.tabpage_load_detail,&
this.tabpage_check_calls,&
this.tabpage_ship_info,&
this.tabpage_stop_info,&
this.tabpage_single_appt,&
this.tabpage_instructions}
end on

on tab_load_detail.destroy
destroy(this.tabpage_load_detail)
destroy(this.tabpage_check_calls)
destroy(this.tabpage_ship_info)
destroy(this.tabpage_stop_info)
destroy(this.tabpage_single_appt)
destroy(this.tabpage_instructions)
end on

event selectionchanged;This.SetRedraw(false)
setmicrohelp("Ready...")

IF IsValid(w_tooltip) Then
	Close(w_tooltip)
END IF

CHOOSE CASE newindex
	CASE 2
		IF ib_new_check_call = TRUE Then
			tabpage_check_calls.dw_check_call_info.PostEvent("ue_retrieve")
			ib_new_check_call = FALSE
		Else
			If tabpage_check_calls.dw_check_call_info.Rowcount() = 0 Then
				setmicrohelp("No Check Call Found For This Primary")
		   End If
		END IF
	CASE 3
		IF ib_new_ship_info = TRUE Then
			tabpage_ship_info.dw_ship_info.PostEvent("ue_retrieve")
			tabpage_ship_info.dw_dispatch_event.PostEvent("ue_retrieve")
			ib_new_ship_info = FALSE
		END IF
	CASE 4
		IF ib_new_stop_info = TRUE Then
			tabpage_stop_info.dw_stop_info.PostEvent("ue_retrieve")
			ib_new_stop_info = FALSE
		END IF
	CASE 5
		IF ib_new_appt = TRUE Then
		   wf_load_single_appt()
			ib_new_appt = False
	   End IF 
	CASE 6
		IF ib_new_instruction = TRUE Then
			tabpage_instructions.dw_instructions.PostEvent("ue_retrieve")
			ib_new_instruction = False
		End IF 
	CASE 7
		IF ib_new_edi_review = TRUE Then
//			tab_load_detail.tabpage_edi.dw_214_review.InsertRow(0)
//			tabpage_edi.dw_214_review.PostEvent("ue_retrieve")
			ib_new_edi_review = False
		End IF 
	END CHOOSE

This.SetRedraw(true)
end event

event selectionchanging;long ll_row, &
     ll_rc

If IsValid(iw_ToolTip) Then 
	Close(iw_ToolTip)
End If

IF  oldindex = 5 Then
	tab_load_detail.tabpage_single_appt.dw_single_appt.AcceptText()
	ll_Row = tab_load_detail.tabpage_single_appt.dw_single_appt.GetNextModified(0, PRIMARY!)
   IF ll_row > 0 Then
	   ll_rc = messagebox("Update", "Something changed in Appointment Tab.  Do you wish to save?", QUESTION!, YesNoCancel!)
		if ll_rc = 1 then
		  wf_update()
	   elseif ll_rc = 2 then
		   return 0
	   else
		   return 1
	   end if
	End IF
End IF


IF  oldindex = 6 Then
	tab_load_detail.tabpage_instructions.dw_instructions.AcceptText()
	ll_Row = tab_load_detail.tabpage_instructions.dw_instructions.GetNextModified(0, PRIMARY!)
   IF ll_row > 0 Then
	   ll_rc = messagebox("Update", "Freight Forwarder changed.  Do you wish to save?", QUESTION!, YesNoCancel!)
		if ll_rc = 1 then
		  wf_update()
	   elseif ll_rc = 2 then
		   return 0
	   else
		   return 1
	   end if
	End IF
End IF




end event

type tabpage_load_detail from userobject within tab_load_detail
integer x = 15
integer y = 90
integer width = 3394
integer height = 1395
long backcolor = 12632256
string text = "Load Detail"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
string powertiptext = "Load Order Details"
gb_1 gb_1
dw_load_detail_hdr dw_load_detail_hdr
dw_stop_detail_info dw_stop_detail_info
dw_check_call dw_check_call
end type

on tabpage_load_detail.create
this.gb_1=create gb_1
this.dw_load_detail_hdr=create dw_load_detail_hdr
this.dw_stop_detail_info=create dw_stop_detail_info
this.dw_check_call=create dw_check_call
this.Control[]={this.gb_1,&
this.dw_load_detail_hdr,&
this.dw_stop_detail_info,&
this.dw_check_call}
end on

on tabpage_load_detail.destroy
destroy(this.gb_1)
destroy(this.dw_load_detail_hdr)
destroy(this.dw_stop_detail_info)
destroy(this.dw_check_call)
end on

type gb_1 from groupbox within tabpage_load_detail
integer x = 15
integer y = 467
integer width = 2911
integer height = 368
integer taborder = 3
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Last Check Call"
end type

type dw_load_detail_hdr from u_base_dw_ext within tabpage_load_detail
integer y = 3
integer width = 3178
integer height = 464
integer taborder = 2
string dataobject = "d_load_det_hdr_str"
boolean border = false
end type

event ue_mousemove;call super::ue_mousemove;// 09/11/1997 ibdkkam	This code was written by Tim Bornholtz and has 
// 							been copied directly from the Pricing Application.

String	ls_description, &
			ls_carrier, &
			ls_late_reason_code

If Row > 0 AND string(dwo.name) = 'carrier' Then
	If is_lastcolumn <> 'carrier' Or il_Row <> Row Then
		ls_carrier = dw_load_detail_hdr.GetItemString(Row, "carrier")

		IF IsValid(w_tooltip) Then
			Close(w_tooltip)
		END IF
		
		// 03/2002 ibdkkam Connect/Disconnect SQL Server and removed quotes.
		Connect Using SQLCA;

		SELECT Distinct carrier_short_name
		  INTO :ls_description
		  FROM carrier  
		WHERE carrier_code = :ls_carrier;
			
		If SQLCA.SQLCode = 0 And len(Trim(ls_description)) > 0 Then 
			Disconnect Using SQLCA;
			OpenWithParm(w_tooltip,ls_description)
		Else
			Disconnect Using SQLCA;
		END IF
	End if
ELSE
// RevGLL	*(Adding tool tip for Late Reason Code.)
	If Row > 0 AND string(dwo.name) = 'late_reason' Then
		IF is_lastcolumn <> 'late_reason' Or il_Row <> Row Then
			ls_late_reason_code = dw_load_detail_hdr.GetItemString(Row, "late_reason")
		
			IF IsValid(w_tooltip) Then
				Close(w_tooltip)
			END IF
		
			Connect Using SQLCA;
		
			SELECT type_desc
			INTO :ls_description
			FROM tutltypes
			WHERE record_type = 'latecode'
			AND type_code = :ls_late_reason_code;
		
			If SQLCA.SQLCode = 0 AND len(Trim(ls_description)) > 0 Then
				Disconnect Using SQLCA;
				OpenWithParm(w_tooltip,ls_description)
			Else
				Disconnect Using SQLCA;
			END IF
		END IF
//	RevGLL	*(End)		
	Else
		IF IsValid(w_tooltip) AND il_Row <> Row  Then
			close(w_tooltip)
		END IF
		IF il_Row = Row AND dwo.name <> 'carrier' Then
			IF IsValid(w_tooltip) Then
				Close(w_tooltip)
			END IF
		END IF
	END IF
END IF

il_Row = Row
is_lastcolumn = String(dwo.name)
end event

type dw_stop_detail_info from u_base_dw_ext within tabpage_load_detail
event ue_post_tab pbm_custom24
integer x = 7
integer y = 848
integer width = 2918
integer height = 496
integer taborder = 2
string dataobject = "d_load_det_stop_info"
boolean vscrollbar = true
end type

event ue_post_tab;call super::ue_post_tab;scrolltorow(il_error_row)
setrow(il_error_row)
setcolumn(ii_error_column)
setfocus()

end event

event clicked;call super::clicked;long 				ll_FoundRow

String          ls_GetText

If row < 1 Then Return

If not ib_modify Then Return

If is_ObjectAtPointer = "late_reason" Then
   ls_GetText = GetText()
   IF Trim(ls_GetText) = "" THEN Return
      ll_FoundRow = idwc_latecode.Find ( "late_reason='"+ls_GetText+"'", 1, idwc_latecode.RowCount() )
End if
end event

event constructor;call super::constructor;IF message.nf_get_app_id() <> 'OTR' Then Return

ii_rc = GetChild( 'late_reason', idwc_latecode )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for late code ")

//iw_frame.iu_netwise_data.nf_Gettutltype( "LATECODE", idwc_latecode )
//taken out and replace with code below
idwc_latecode.SetTrans(SQLCA)
idwc_latecode.Retrieve("LATECODE")
end event

event doubleclicked;call super::doubleclicked;String          ls_load_key, &
                ls_stop_code, &
                ls_input_string

If row < 1 Then Return

ls_load_key = dw_load_detail_hdr.object.load_key[1]

If dwo.name = "stop_code" Then
      ls_stop_code = This.object.stop_code[Row]
      ls_input_string = ls_load_key + ls_stop_code
      OpenWithParm(w_stops_order_po_resp, ls_input_string) 
End if



If is_ObjectAtPointer = "stop_comment_ind" Then 
   If ib_modify Then
        ls_stop_code = This.object.stop_code[Row]
        ls_input_string =  ls_load_key + ls_stop_code
        OpenWithParm(w_stop_comments_add_child, ls_input_string) 
        This.SetItem(Row, "stop_comment_ind", "X")    
    End if
End if



//ls_window_name = "w_single_appt_update"
//
//If is_ObjectAtPointer = "appt_date" Then 
//    If ib_modify   Then
//          ls_input_string =  ls_load_key 
//          SetPointer( HourGlass! )
//          OpensheetWithParm(lw_to_open, ls_input_string, ls_window_name, iw_frame, 0, Original!) 
//    End if
//End if
// 
//
//
//If is_ObjectAtPointer = "appt_time" Then 
//    If ib_modify   Then
//          ls_input_string =  ls_load_key 
//          SetPointer( HourGlass! )
//          OpensheetWithParm(lw_to_open, ls_input_string, ls_window_name, iw_frame, 0, Original!) 
//    End if
//End if
//  
end event

event itemchanged;call super::itemchanged;Integer    li_CurrentRow, &
           li_CurrentCol 

long 		  ll_FoundRow

String     ls_GetText

li_CurrentRow = This.GetRow()
li_CurrentCol = This.GetColumn() 


tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItemStatus(li_CurrentRow, 0, Primary!, DataModified!) 

is_ObjectAtPointer = Left( is_ObjectAtPointer, Pos( is_ObjectAtPointer, "~t") - 1 )

// Added Choose case to determine if ETA date or time was changed
// added 9-25-97 because of the new program otrt61ar
Choose Case dwo.Name
	Case "eta_date"  
     tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem(row, "update_eta_ind", "Y") 
     tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem(row, "verify_eta_ind", "N") 
   Case "eta_time"  
     tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem(row, "update_eta_ind", "Y") 
     tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem(row, "verify_eta_ind", "N") 
	Case "update_eta_ind"  
     tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem(row, "verify_eta_ind", "N") 
End Choose


If li_CurrentCol = 3 Then
   tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem(li_CurrentRow, "late_reason_err_ind", 0) 
   ls_GetText = GetText()
   IF Trim(ls_GetText) = "" THEN Return
       ll_FoundRow = idwc_latecode.Find ( "type_code = '"+ls_GetText+"'", 1, idwc_latecode.RowCount() )
       IF ll_FoundRow < 1 THEN
          MessageBox ("Late Reason Error", ls_GetText + " is not a valid late reason code.", &
          StopSign!, OK!)
   		 ii_error_column = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.getcolumn()
          il_error_row = This.GetRow()
			 tab_load_detail.tabpage_load_detail.dw_stop_detail_info.postevent("ue_post_tab")
          This.SetColumn("late_reason") 
          tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem(li_CurrentRow, "late_reason_err_ind", 1) 
	       Return
		else
			IF li_CurrentRow = 1 Then
				tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.SetItem(1, "late_reason", ls_gettext)
			End If
      End if 
End If 
end event

event ue_postconstructor;call super::ue_postconstructor;ib_updateable = TRUE
end event

event ue_mousemove;call super::ue_mousemove;String	ls_late_reason_code, &
			ls_description, &
			ls_xpos, &
			ls_ypos

IF IsValid(w_tooltip) Then
	Close(w_tooltip)
END IF

ia_apptool = GetApplication()
il_xpos = gw_netwise_frame.PointerX() + 50
il_ypos = gw_netwise_frame.PointerY() + 50

Choose Case dwo.name 
	Case 'late_reason'
		If il_LastRow = row Then return
		ls_late_reason_code = dw_stop_detail_info.GetItemString(Row, "late_reason")
		
		IF IsValid(iw_tooltip) THEN
			Close(iw_tooltip)
		END IF
		
		Connect Using SQLCA;
		
		SELECT type_desc
		INTO :is_description
		FROM tutltypes
		WHERE record_type = 'latecode'
		AND type_code = :ls_late_reason_code;
		
		IF SQLCA.SQLCode = 0 AND len(trim(is_description)) > 0 THEN
			Disconnect Using SQLCA;

			il_LastRow = row
			ls_description = is_description
			ls_xpos = String(il_xpos)
			ls_ypos = String(il_ypos)

			OpenWithParm(iw_tooltip, tab_load_detail.getparent(), iw_frame)
			is_description = ""
		ELSE
			il_LastRow = 0
			Disconnect Using SQLCA;
			iw_frame.SetMicroHelp("Ready")
		End IF
		
	Case 'appt_contact'
		If il_LastRow = row Then return
		is_description = dw_stop_detail_info.GetItemString(Row, "appt_contact")
		
		IF IsValid(iw_tooltip) THEN
			Close(iw_tooltip)
		END IF
		
		IF len(trim(is_description)) > 0 THEN
			il_LastRow = row
			ls_xpos = String(il_xpos)
			ls_ypos = String(il_ypos)

			OpenWithParm(iw_tooltip, tab_load_detail.getparent(), iw_frame)
			is_description = ""
		ELSE
			il_LastRow = 0
			iw_frame.SetMicroHelp("Ready")
		End IF
	Case ELSE	
		il_LastRow = 0
		iw_frame.SetMicroHelp("Ready")
		IF IsValid(iw_tooltip) Then
			Close(iw_tooltip)
		END IF	
END Choose


end event

type dw_check_call from u_base_dw_ext within tabpage_load_detail
event ue_post_tab pbm_custom24
integer x = 33
integer y = 522
integer width = 2882
integer height = 307
integer taborder = 2
boolean bringtotop = true
string dataobject = "d_load_det_last_check_call"
boolean border = false
end type

event ue_post_tab;call super::ue_post_tab;scrolltorow(il_error_row)
setrow(il_error_row)
setcolumn(ii_error_column)
setfocus()

end event

event clicked;call super::clicked;//long       ll_FoundRow
//
//If row < 1 Then Return
//
//If not ib_modify  Then Return
//
//
//If is_ObjectAtPointer = "late_reason" Then
//	 string ls_gettext
//	 ls_gettext = gettext()
//    IF Trim(ls_gettext) = "" THEN Return
//    ll_FoundRow = idwc_latecode.Find ( "late_reason='"+ls_gettext+"'", 1, idwc_latecode.RowCount() )
//End If 

 

end event

event constructor;call super::constructor;IF message.nf_get_app_id() <> 'OTR' Then Return

ii_rc = GetChild( 'late_reason', idwc_latecode )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for late code ")

idwc_latecode.SetTrans(SQLCA)
idwc_latecode.Retrieve("LATECODE")
end event

event doubleclicked;call super::doubleclicked;String          ls_load_key, &
                ls_stop_code, &
                ls_input_string
            
If row < 1 Then Return

ls_load_key = dw_load_detail_hdr.Object.load_key[1]

If dwo.name = "stop_code" Then
      ls_stop_code = This.Object.stop_code[1]
      ls_input_string = ls_load_key + ls_stop_code
      OpenWithParm(w_stops_order_po_resp, ls_input_string) 
End if



end event

event itemchanged;call super::itemchanged;Integer      li_CurrentCol, &
             li_itemstatus
  
Long       ll_Stop_Detail_Row, &
           ll_FoundRow 

String     ls_total_stops, &
			  ls_stop_code, &
			  ls_late_reason, &
			  ls_work_date, &
			  ls_work_time, &
			  ls_no_eta_ind

Date       ld_eta_date, &
			  ld_saved_date, &
			  ld_work_date

Time       lt_eta_time, &
			  lt_saved_time, &
			  lt_work_time


li_CurrentCol = This.GetColumn()
if dwo.name = "stop_code" Then
	ls_stop_code = data
Else
	ls_stop_code = this.GetItemString(Row, "stop_code")
END IF


ib_valid_stop_number = true

Choose Case dwo.name
	Case "stop_code"
		ls_late_reason = this.GetItemString(Row, "late_reason")
		ls_no_eta_ind = this.GetItemString(Row, "no_eta_ind")
		ld_work_date = this.GetItemDate(Row, "eta_date")
      ls_work_date = String(ld_work_date,"mm/dd/yyyy")
		lt_work_time = this.GetItemTime(Row, "eta_time")
      ls_work_time = String(lt_work_time,"hhmm")
	   ls_total_stops  = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.total_stops[1]
  	   If Integer(ls_stop_code) > Integer(ls_total_stops)  Then
  	      MessageBox ("Error", "Stop Code entered must be less than the total number of stops.", &
  		     StopSign!, OK!)
		   ib_valid_stop_number = false
			tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("stop_code")
			tab_load_detail.tabpage_load_detail.dw_check_call.Setfocus()
			tab_load_detail.tabpage_load_detail.dw_check_call.SelectText(1,2)
         Return        
 	  Else
  	      IF Integer(ls_stop_code) > 0 Then
				 ll_Stop_Detail_Row = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Find( "stop_code = '" + ls_stop_code+"'" , 1 , tab_load_detail.tabpage_load_detail.dw_stop_detail_info.RowCount()) 
		       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItemStatus(ll_Stop_Detail_Row,0,Primary!, DataModified!)
         ELSE        
           MessageBox ("Error", "Stop Code entered must be greater than zero.", &
           StopSign!, OK!)
           ib_valid_stop_number = false
			  tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("stop_code")
			  tab_load_detail.tabpage_load_detail.dw_check_call.Setfocus()
			  SelectText(1,2)
           Return           
        END IF
     end if
//	  If len(Trim(dw_check_call.object.late_reason.data[row])) > 0 or isnull(dw_check_call.object.eta_date.data[row]) &
//	  or isnull(dw_check_call.object.eta_time.data[row])  or len(Trim(dw_check_call.object.no_eta_ind.data[row])) > 0 Then
	  If len(Trim(ls_late_reason)) > 0 or len(Trim(ls_no_eta_ind)) > 0 &
	  or ls_work_date > "01/01/1900"   or len(Trim(ls_work_time)) > 0 Then
		     MessageBox ("Check Call Stop Code Error", "You cannot change Stop Code because data has already been entered. ~n If you want to change the Stop Code, please re-inquire without saving changes.", StopSign!, OK!)
//			  ib_valid_stop_number = false
//			  tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("stop_code")
			  tab_load_detail.tabpage_load_detail.dw_check_call.Setfocus()
			  SelectText(1,2)
			  Return           
		End if

Case "late_reason"
   This.SetItem(Row, "late_reason_err_ind", 0) 
	IF Trim(data) = "" THEN Return
       ll_FoundRow = idwc_latecode.Find ( "type_code = '"+data+"'", 1, idwc_latecode.RowCount() )
       IF ll_FoundRow < 1 THEN
          tab_load_detail.tabpage_load_detail.dw_check_call.Setfocus()
          This.SetItem(Row, "late_reason_err_ind", 1) 
	       Return 1
       ELSE  
          ll_Stop_Detail_Row = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Find( "stop_code = '" + ls_stop_code+"'" , 1 , tab_load_detail.tabpage_load_detail.dw_stop_detail_info.RowCount())  
          tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_Stop_Detail_Row , "late_reason", data )
//			 tab_load_detail.tabpage_load_detaiL.dw_check_call.Object.stop_code.Protect=1 
//			 tab_load_detail.tabpage_load_detaiL.dw_check_call.Object.stop_code.Background.Color = 12632256 
//          tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItemStatus( ll_Stop_Detail_Row , 0 , PRIMARY!,  DataModified! )
       End if
 
Case "eta_date"
     ld_eta_date = date(data)
     ll_Stop_Detail_Row = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Find( "stop_code = '" + ls_stop_code+"'" , 1 , tab_load_detail.tabpage_load_detail.dw_stop_detail_info.RowCount())  
     tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_Stop_Detail_Row , "eta_date", ld_eta_date )
	  tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_Stop_Detail_Row , "update_eta_ind", "Y" )
	  tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_Stop_Detail_Row , "verify_eta_ind", "N")
//	  tab_load_detail.tabpage_load_detaiL.dw_check_call.Object.stop_code.Protect=1 
//	  tab_load_detail.tabpage_load_detaiL.dw_check_call.Object.stop_code.Background.Color = 12632256 
//     li_itemstatus = dw_stop_detail_info.SetItemStatus( ll_Stop_Detail_Row , 0 , PRIMARY!,  DataModified! ) 

Case "eta_time"
    lt_eta_time = time(data)
    ll_Stop_Detail_Row = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Find( "stop_code = '" + ls_stop_code+"'" , 1 , tab_load_detail.tabpage_load_detail.dw_stop_detail_info.RowCount())  
    tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_Stop_Detail_Row , "eta_time", lt_eta_time )
	 tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_Stop_Detail_Row , "update_eta_ind", "Y" )
	 tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_Stop_Detail_Row , "verify_eta_ind", "N")
//	 tab_load_detail.tabpage_load_detaiL.dw_check_call.Object.stop_code.Protect=1 
//	 tab_load_detail.tabpage_load_detaiL.dw_check_call.Object.stop_code.Background.Color = 12632256 
//    li_itemstatus = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItemStatus( ll_Stop_Detail_Row , 0 , PRIMARY!,  DataModified! ) 


Case "no_eta_ind"
	 ll_Stop_Detail_Row = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.Find( "stop_code = '" + ls_stop_code+"'" , 1 , tab_load_detail.tabpage_load_detail.dw_stop_detail_info.RowCount())
//	 ls_no_eta_ind = tab_load_detail.tabpage_load_detail.dw_check_call.object.no_eta_ind[ll_Stop_Detail_Row]
	 If Trim(data) = 'X' then
	 	 SetNull (ld_eta_date)
	    lt_eta_time = time("00:00")
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_Stop_Detail_Row , "eta_date", ld_eta_date )
		 tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_stop_detail_Row , "update_eta_ind", "Y" )
		 tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_Stop_Detail_Row , "verify_eta_ind", "N")
//       li_itemstatus = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItemStatus( ll_Stop_Detail_Row , 0 , PRIMARY!,  DataModified! ) 
       tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_Stop_Detail_Row , "eta_time", lt_eta_time )
		 tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_stop_detail_Row , "update_eta_ind", "Y" )
		 tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_Stop_Detail_Row , "verify_eta_ind", "N")
//       li_itemstatus = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItemStatus( ll_Stop_Detail_Row , 0 , PRIMARY!,  DataModified! ) 
	 Else
		  ld_saved_date = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.GetItemDate( ll_Stop_Detail_Row , "eta_date", PRIMARY!, True)
		  tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_Stop_Detail_Row , "eta_date", ld_saved_date )
		  lt_saved_time = tab_load_detail.tabpage_load_detail.dw_stop_detail_info.GetItemTime( ll_Stop_Detail_Row , "eta_time", PRIMARY!, True)
		  tab_load_detail.tabpage_load_detail.dw_stop_detail_info.SetItem( ll_Stop_Detail_Row , "eta_time", lt_saved_time )
	 end if
//	 tab_load_detail.tabpage_load_detaiL.dw_check_call.Object.stop_code.Protect=1 
//	 tab_load_detail.tabpage_load_detaiL.dw_check_call.Object.stop_code.Background.Color = 12632256 

End Choose


end event

event ue_postconstructor;call super::ue_postconstructor;ib_updateable = TRUE
end event

event itemfocuschanged;call super::itemfocuschanged;IF not ib_valid_stop_number Then
	This.SetColumn("stop_code")
End IF
end event

event itemerror;//Overriden ancestor
end event

event ue_mousemove;call super::ue_mousemove;String	ls_late_reason_code, &
			ls_description, &
			ls_xpos, &
			ls_ypos

IF IsValid(w_tooltip) Then
	Close(w_tooltip)
END IF

ia_apptool = GetApplication()
il_xpos = gw_netwise_frame.PointerX() + 50
il_ypos = gw_netwise_frame.PointerY() + 50

Choose Case dwo.name
	Case "late_reason" 
		If il_LastRow = row Then return
		ls_late_reason_code = dw_check_call.GetItemString(Row, "late_reason")
		
		IF IsValid(iw_tooltip) THEN
			Close(iw_tooltip)
		END IF
		
		Connect Using SQLCA;
		
		SELECT type_desc
		INTO :is_description
		FROM tutltypes
		WHERE record_type = 'latecode'
		AND type_code = :ls_late_reason_code;
		
		IF SQLCA.SQLCode = 0 AND len(trim(is_description)) > 0 THEN
			Disconnect Using SQLCA;

			il_LastRow = row
			ls_description = is_description
			ls_xpos = String(il_xpos)
			ls_ypos = String(il_ypos)

			OpenWithParm(iw_tooltip, tab_load_detail.getparent(), iw_frame)
			is_description = ""
		ELSE
			il_LastRow = 0
			Disconnect Using SQLCA;
			iw_frame.SetMicroHelp("Ready")
		End IF
	Case 'eta_contact'
		If il_LastRow = row Then return
		is_description = dw_check_call.GetItemString(Row, "eta_contact")
		
		IF IsValid(iw_tooltip) THEN
			Close(iw_tooltip)
		END IF
		
		IF len(trim(is_description)) > 0 THEN

			ls_xpos = String(il_xpos)
			ls_ypos = String(il_ypos)

			il_LastRow = row
			OpenWithParm(iw_tooltip, tab_load_detail.getparent(), iw_frame)
			is_description = ""
		ELSE
			il_LastRow = 0
			iw_frame.SetMicroHelp("Ready")
		End IF
	Case ELSE	
		il_LastRow = 0
		IF IsValid(iw_tooltip) Then
			Close(iw_tooltip)
		END IF	
		iw_frame.SetMicroHelp("Ready")
END Choose


end event

type tabpage_check_calls from userobject within tab_load_detail
integer x = 15
integer y = 90
integer width = 3394
integer height = 1395
long backcolor = 12632256
string text = "Check Calls"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
string powertiptext = "Check Call History"
dw_check_call_info dw_check_call_info
end type

on tabpage_check_calls.create
this.dw_check_call_info=create dw_check_call_info
this.Control[]={this.dw_check_call_info}
end on

on tabpage_check_calls.destroy
destroy(this.dw_check_call_info)
end on

type dw_check_call_info from u_base_dw_ext within tabpage_check_calls
integer x = 15
integer y = 13
integer width = 2871
integer height = 1216
integer taborder = 2
string dataobject = "d_load_det_last_check_call"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;String          ls_load_key, &
                ls_stop_code, &
                ls_input_string


If row < 1 Then Return


If dwo.name = "stop_code" Then
      ls_load_key = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
      ls_stop_code = This.object.stop_code[Row]
      ls_input_string = ls_load_key + ls_stop_code
      OpenWithParm(w_stops_order_po_resp, ls_input_string) 
End if



end event

event ue_retrieve;call super::ue_retrieve;integer               li_rtn				         

string                ls_check_call_inq_string, &
                      ls_req_load_key, &
                      ls_refetch_stop_code, &
                      ls_refetch_check_call_date, & 
	                   ls_refetch_check_call_time
						 
s_error               lstr_Error_Info

SetPointer( HourGlass! )

SetRedraw(false)

ls_req_load_key      = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
ls_refetch_stop_code = " "
ls_refetch_check_call_date   = " " 
ls_refetch_check_call_time   = " "

lstr_error_info.se_event_name = "wf_get_check_calls_history"
lstr_error_info.se_window_name = "w_load_detail"
lstr_error_info.se_procedure_name = "wf_get_check_calls_history"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ls_check_call_inq_string	= Space(1181)
ls_refetch_stop_code       = Space(2)
ls_refetch_check_call_date = Space(10)
ls_refetch_check_call_time = Space(8)

tab_load_detail.tabpage_check_calls.dw_check_call_info.Reset()

DO
	ls_check_call_inq_string	= Space(1181)	
	li_rtn =  iu_ws_review_queue_load_list.nf_otrt14er(ls_req_load_key, ls_check_call_inq_string,lstr_Error_Info)
	
	//li_rtn =iu_otr003.nf_otrt14ar(ls_req_load_key, ls_check_call_inq_string, &
    //   ls_refetch_stop_code, ls_refetch_check_call_date, ls_refetch_check_call_time, &
    //   lstr_error_info, 0)

	// Check the return code from the above function call
	IF li_rtn <> 0 THEN
		return(-1)
	END IF

	li_rtn = tab_load_detail.tabpage_check_calls.dw_check_call_info.ImportString(Trim(ls_check_call_inq_string))

LOOP WHILE trim(ls_refetch_check_call_date) > "" 

iw_frame.SetMicroHelp( "Ready")

//to prevent background color of columns from changing to white
//when coming from tab 3 or 4
/* commented to test, fix by moving setredraw before reset()
 when redraw was put back after the reset it still grayed so this code should not be needed.
tab_load_detail.tabpage_check_calls.dw_check_call_info.object.stop_code.background.color = 12632256
tab_load_detail.tabpage_check_calls.dw_check_call_info.object.cc_date.background.color = 12632256
tab_load_detail.tabpage_check_calls.dw_check_call_info.object.cc_time.background.color = 12632256
tab_load_detail.tabpage_check_calls.dw_check_call_info.object.city.background.color = 12632256
tab_load_detail.tabpage_check_calls.dw_check_call_info.object.state.background.color = 12632256
tab_load_detail.tabpage_check_calls.dw_check_call_info.object.late_reason.background.color = 12632256
tab_load_detail.tabpage_check_calls.dw_check_call_info.object.miles_out.background.color = 12632256
tab_load_detail.tabpage_check_calls.dw_check_call_info.object.eta_date.background.color = 12632256
tab_load_detail.tabpage_check_calls.dw_check_call_info.object.eta_time.background.color = 12632256
tab_load_detail.tabpage_check_calls.dw_check_call_info.object.eta_contact.background.color = 12632256
*/
tab_load_detail.tabpage_check_calls.dw_check_call_info.Describe("DataWindow.ReadOnly")
tab_load_detail.tabpage_check_calls.dw_check_call_info.Modify("DataWindow.ReadOnly=Yes")

tab_load_detail.tabpage_check_calls.dw_check_call_info.ResetUpdate()

SetRedraw(true)
end event

event ue_mousemove;call super::ue_mousemove;
IF IsValid(w_tooltip) Then
	Close(w_tooltip)
END IF

end event

type tabpage_ship_info from userobject within tab_load_detail
event create ( )
event destroy ( )
integer x = 15
integer y = 90
integer width = 3394
integer height = 1395
long backcolor = 12632256
string text = "Ship Info"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
string powertiptext = "Shipping Information"
dw_dispatch_event dw_dispatch_event
dw_ship_info dw_ship_info
end type

on tabpage_ship_info.create
this.dw_dispatch_event=create dw_dispatch_event
this.dw_ship_info=create dw_ship_info
this.Control[]={this.dw_dispatch_event,&
this.dw_ship_info}
end on

on tabpage_ship_info.destroy
destroy(this.dw_dispatch_event)
destroy(this.dw_ship_info)
end on

type dw_dispatch_event from u_base_dw within tabpage_ship_info
integer x = 55
integer y = 928
integer width = 2710
integer height = 445
integer taborder = 11
string dataobject = "d_dispatch_event"
boolean vscrollbar = true
boolean border = false
end type

event ue_retrieve;call super::ue_retrieve;integer              li_rtn, &
							li_refetch_pickup_sequence

string					ls_req_load_key, &
							ls_refetch_complex_code, &
							ls_refetch_timestamp_added, &
							ls_se_dispatch_event_string
							 
Integer					li_refetch_complex_code
							 

s_error              lstr_Error_Info


SetPointer( HourGlass! )

ls_req_load_key      = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]

lstr_error_info.se_event_name = "wf_get_dispatch_event"
lstr_error_info.se_window_name = "w_load_detail"
lstr_error_info.se_procedure_name = "wf_get_dispatch_event"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)
 
ls_se_dispatch_event_string	= Space(6001)
ls_refetch_complex_code			= Space(3)
ls_refetch_timestamp_added		= Space(26)

tab_load_detail.tabpage_ship_info.dw_dispatch_event.Reset()

//li_rtn = iu_otr006.nf_otrt76ar(ls_req_load_key, &
//							ls_refetch_complex_code, &
//							li_refetch_pickup_sequence, &
//         				ls_refetch_timestamp_added, &
//							ls_se_dispatch_event_string, &
//							dw_dispatch_event, &
//							lstr_Error_Info)
							
li_rtn= iu_ws_review_queue_load_list.nf_otrt76er(ls_req_load_key, &							
							dw_dispatch_event, &
							lstr_Error_Info)					
							

iw_frame.SetMicroHelp( "Ready...")
// Check the return code from the above function call
IF li_rtn < 0 THEN  return(-1)


      
end event

event ue_mousemove;call super::ue_mousemove;
IF IsValid(w_tooltip) Then
	Close(w_tooltip)
END IF

end event

type dw_ship_info from u_base_dw_ext within tabpage_ship_info
integer x = 187
integer y = 29
integer width = 2436
integer height = 973
integer taborder = 2
string dataobject = "d_ship_info"
boolean border = false
end type

event ue_retrieve;call super::ue_retrieve;integer               li_rtn

string                ls_ship_info_inq_string, &
                      ls_req_load_key

s_error               lstr_Error_Info


SetPointer( HourGlass! )

ls_req_load_key      = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]

lstr_error_info.se_event_name = "wf_get_ship_info"
lstr_error_info.se_window_name = "w_load_detail"
lstr_error_info.se_procedure_name = "wf_get_ship_info"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)
 
ls_ship_info_inq_string	= Space(257)

tab_load_detail.tabpage_ship_info.dw_ship_info.Reset()

//Added code for the netwise convertion 09/06/2016
li_rtn = iu_ws_review_queue_load_list.nf_otrt15er(ls_req_load_key, ls_ship_info_inq_string, lstr_Error_info)


//li_rtn = iu_otr003.nf_otrt15ar(ls_req_load_key, ls_ship_info_inq_string, &
//         lstr_error_info, 0)

iw_frame.SetMicroHelp( "Ready...")
// Check the return code from the above function call
IF li_rtn < 0 THEN  return(-1)

li_rtn = tab_load_detail.tabpage_ship_info.dw_ship_info.ImportString(Trim(ls_ship_info_inq_string) )
  

      
end event

event ue_mousemove;call super::ue_mousemove;
IF IsValid(w_tooltip) Then
	Close(w_tooltip)
END IF

end event

type tabpage_stop_info from userobject within tab_load_detail
event create ( )
event destroy ( )
integer x = 15
integer y = 90
integer width = 3394
integer height = 1395
long backcolor = 12632256
string text = "Stop Info"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
string powertiptext = "Stop Information"
dw_stop_info dw_stop_info
end type

on tabpage_stop_info.create
this.dw_stop_info=create dw_stop_info
this.Control[]={this.dw_stop_info}
end on

on tabpage_stop_info.destroy
destroy(this.dw_stop_info)
end on

type dw_stop_info from u_base_dw_ext within tabpage_stop_info
integer width = 3200
integer height = 1306
integer taborder = 2
string dataobject = "d_stop_info"
boolean vscrollbar = true
boolean border = false
end type

event doubleclicked;call super::doubleclicked;String          ls_load_key, &
                ls_stop_code, &
                ls_input_string


If row < 1 Then Return

If dwo.name = "stop_code" Then
      ls_load_key = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
      ls_stop_code = This.object.stop_code[Row]
      ls_input_string = ls_load_key + ls_stop_code
      OpenWithParm(w_stops_order_po_resp, ls_input_string) 
End if




end event

event clicked;call super::clicked;string ls_load_key, &
       ls_stop_code, &
       ls_input_string, ls_temp

If row < 1 Then Return
// ibdkdld fix problem in orp
IF message.nf_get_app_id() <> 'OTR' Then
	ls_temp = Left( is_ObjectAtPointer, Pos( is_ObjectAtPointer, "~t") - 1 )	
else
	ls_temp = is_objectatpointer
End If

If ls_temp = "instructions_ind_t" Then
      ls_load_key = This.object.load_number[1]
      ls_stop_code = This.object.stop_code[Row]
      ls_input_string = ls_load_key + ls_stop_code
      OpenWithParm(w_stop_instructions_child, ls_input_string) //, iw_frame.iw_active_sheet)
End if


end event

event ue_retrieve;call super::ue_retrieve;integer               li_rtn		                

string                ls_stop_info_inq_string, &
                      ls_req_load_key, &
                      ls_refetch_stop_code, &
                      ls_refetch_comment_date, &
                      ls_refetch_comment_time,&
					ls_se_app_name		 

s_error               lstr_Error_Info


SetPointer( HourGlass! )

ls_req_load_key      = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
ls_se_app_name = 'OTR'


lstr_error_info.se_app_name = "OTR"
lstr_error_info.se_event_name = "wf_get_stop_info"
lstr_error_info.se_window_name = "w_stop_info_child"
lstr_error_info.se_procedure_name = "wf_get_stop_info"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)
 
ls_refetch_stop_code    = Space(2)
ls_refetch_comment_date = Space(10)
ls_refetch_comment_time = Space(8)

tab_load_detail.tabpage_stop_info.dw_stop_info.Reset()

do
ls_stop_info_inq_string	= Space(4651)

//Added code for Netwise convertion

li_rtn = iu_ws_review_queue_load_list.nf_otrt31er(ls_req_load_key,ls_se_app_name,&
					ls_stop_info_inq_string,lstr_error_info)	

//li_rtn = iu_otr003.nf_otrt31ar(ls_req_load_key, ls_stop_info_inq_string, &
//         ls_refetch_stop_code, ls_refetch_comment_date, ls_refetch_comment_time, &
//         lstr_error_info, 0)

// Check the return code from the above function call
IF li_rtn < 0 THEN
	 iw_frame.SetMicroHelp( "Ready...")
    return(-1)
END IF

li_rtn = tab_load_detail.tabpage_stop_info.dw_stop_info.ImportString(ls_stop_info_inq_string) 

LOOP WHILE trim(ls_refetch_stop_code) > ""   


iw_frame.SetMicroHelp( "Ready...")

tab_load_detail.tabpage_stop_info.dw_stop_info.SetSort("sort_number asc")
tab_load_detail.tabpage_stop_info.dw_stop_info.Sort()
tab_load_detail.tabpage_stop_info.dw_stop_info.GroupCalc()

end event

event ue_mousemove;call super::ue_mousemove;
IF IsValid(w_tooltip) Then
	Close(w_tooltip)
END IF

end event

event ue_sort;//
end event

type tabpage_single_appt from userobject within tab_load_detail
integer x = 15
integer y = 90
integer width = 3394
integer height = 1395
long backcolor = 12632256
string text = "Single Appt"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
string powertiptext = "Single Appointment"
dw_single_appt dw_single_appt
end type

on tabpage_single_appt.create
this.dw_single_appt=create dw_single_appt
this.Control[]={this.dw_single_appt}
end on

on tabpage_single_appt.destroy
destroy(this.dw_single_appt)
end on

type dw_single_appt from u_base_dw_ext within tabpage_single_appt
event ue_post_tab ( )
event ue_pre_save ( )
integer x = 4
integer y = 26
integer width = 2842
integer height = 1277
integer taborder = 2
string dataobject = "d_appt_update"
boolean vscrollbar = true
boolean border = false
end type

event ue_post_tab;call super::ue_post_tab;scrolltorow(il_error_row)
setrow(il_error_row)
setcolumn(ii_error_column)
setfocus()

end event

event constructor;call super::constructor;This.is_selection = "0"
end event

event doubleclicked;call super::doubleclicked;String          ls_load_key, &
                ls_stop_code, &
                ls_input_string

If is_ObjectAtPointer = "stop_code" Then
      ls_load_key = This.object.load_key[Row]
   ls_stop_code = This.object.stop_code[Row]
   ls_input_string =  ls_load_key + ls_stop_code
   OpenWithParm(w_stops_order_po_resp, ls_input_string)     
End if

end event

event itemchanged;call super::itemchanged;date      ld_ApptDate

Long     ll_deviation

string   ls_temp_hour, &
         ls_temp_min, &
         ls_temp_time, &
			ls2_temp_hour, &
			ls2_temp_min, &
			ls2_temp_time

time     lt_ApptTime, &   
         lt_deviation_time, &
         lt_to_hours, &
			lt2_to_hours


      
ld_ApptDate = this.GetItemDate(Row,"appt_date")

If is_ColumnName = "appt_date" Then
	ld_ApptDate = date(data) //dw_appt_update.GetItemDate(Row,"appt_date")
   If IsNull(Data) then      
      MessageBox("Appointment Date", "Please enter an Appointment Date") 
      This.SetColumn("appt_Date")     
   Else
      If This.object.stop_code[Row] = "01" Then
         If ld_ApptDate <> This.object.promised_deliv_date[Row] Then
            This.SetItem(Row,"override_ind","E")             
         End if
      Else
         If ld_ApptDate <> This.object.sched_deliv_date[Row] Then
            This.SetItem(Row,"override_ind","E")             
         End if
      End if
   End if
End if

If is_ColumnName = "appt_time" Then
   lt_ApptTime = Time(This.GetText())

   If This.object.late_load_ind[Row] =  "T"  or  &
      This.object.late_load_ind[Row] =  "W"  or  &   
      This.object.late_load_ind[Row] =  "O"  or  &
      This.object.late_load_ind[Row] =  "L"  			Then
		This.SetItem(Row,"override_ind","Y")  
	ELSE
		
      If This.object.stop_code[Row] = "01" Then
         If ld_ApptDate <> This.object.promised_deliv_date[Row] Then
            This.SetItem(Row,"override_ind","E")                 
         Else      
            If This.object.keyed_ind[Row] = "Y" Then  
               ll_deviation = This.object.appt_deviation_hours[Row] * 3600              
               lt_deviation_time = RelativeTime(This.object.promised_deliv_time[Row], ll_deviation) 
               If lt_ApptTime > lt_deviation_time Then
                    This.SetItem(Row,"override_ind","E")
               End if
            Else  
               If (This.object.recv_from_hours[Row] = "0000") and &
                  (This.object.recv_to_hours[Row] = "0000") and &
						(This.object.recv2_from_hours[Row] = "0000") and &
                  (This.object.recv2_to_hours[Row] = "0000") Then
                   ll_deviation = This.object.appt_deviation_hours[Row] * 3600    
                   lt_deviation_time = RelativeTime(This.object.promised_deliv_time[Row], ll_deviation) 
                   If lt_ApptTime > lt_deviation_time Then
                       This.SetItem(Row,"override_ind","E")
                   End if
               Else
                   ls_temp_time = This.object.recv_to_hours[Row]
                   ls_temp_hour = Left(ls_temp_time,2)
                   ls_temp_min = Mid(ls_temp_time,3,2)
                   ls_temp_time = ls_temp_hour + ":" + ls_temp_min 
                   lt_to_hours = Time(ls_temp_time)
						 
						 ls2_temp_time = This.object.recv2_to_hours[Row]
                   ls2_temp_hour = Left(ls2_temp_time,2)
                   ls2_temp_min = Mid(ls2_temp_time,3,2)
                   ls2_temp_time = ls2_temp_hour + ":" + ls2_temp_min 
                   lt2_to_hours = Time(ls2_temp_time)
						 
                   If lt_ApptTime > lt_to_hours Then
							 If lt2_to_hours = Time('00:00') Then
                         This.SetItem(Row,"override_ind","E")     
							 End if
                   End if
						 
                   If lt_ApptTime > lt_to_hours Then
							 If lt_ApptTime > lt2_to_hours Then
                         This.SetItem(Row,"override_ind","E")     
							 End if
                   End if
						 
                 End if
              End if 
        End if   
     Else
         If ld_ApptDate <> This.object.sched_deliv_date[Row] Then
            This.SetItem(Row,"override_ind","E")                 
         Else
            If (This.object.recv_from_hours[Row] = "0000") and &
               (This.object.recv_to_hours[Row] = "0000") and &
					(This.object.recv2_from_hours[Row] = "0000") and &
               (This.object.recv2_to_hours[Row] = "0000") Then
                ll_deviation = This.object.appt_deviation_hours[Row] * 3600    
                lt_deviation_time = RelativeTime(This.object.sched_deliv_time[Row], ll_deviation) 
                If lt_ApptTime > lt_deviation_time Then
                    This.SetItem(Row,"override_ind","E")
                End if
            Else
                ls_temp_time = This.object.recv_to_hours[Row]
                ls_temp_hour = Left(ls_temp_time,2)
                ls_temp_min = Mid(ls_temp_time,3,2)
                ls_temp_time = ls_temp_hour + ":" + ls_temp_min 
                lt_to_hours = Time(ls_temp_time) 
					 
					 ls2_temp_time = This.object.recv2_to_hours[Row]
                ls2_temp_hour = Left(ls2_temp_time,2)
                ls2_temp_min = Mid(ls2_temp_time,3,2)
                ls2_temp_time = ls2_temp_hour + ":" + ls2_temp_min 
                lt2_to_hours = Time(ls2_temp_time)
						 
                If lt_ApptTime > lt_to_hours Then
						 If lt2_to_hours = Time('00:00') Then
                      This.SetItem(Row,"override_ind","E")
					    End if
                End if 
					 
                If lt_ApptTime > lt_to_hours Then
						 If lt_ApptTime > lt2_to_hours Then
                      This.SetItem(Row,"override_ind","E")     
						 End if
                End if
					 
            End if
           End if
         End if
   End if
End if

 
If This.object.override_ind[Row] = "E" Then
    This.SelectRow(Row,TRUE)
Else
    This.SelectRow(Row,FALSE)
End if


end event

event ue_mousemove;call super::ue_mousemove;String			ls_name, &
					ls_appt_contact


Choose Case dwo.name
	Case 'appt_contact'
		If il_LastRow = row Then return
	   is_description = This.GetItemString(row, 'appt_contact')
		  

		If IsValid(w_tooltip) Then Close(w_tooltip)
		This.SetFocus()
		il_LastRow = row
		If iw_frame.iu_string.nf_IsEmpty(is_description) Then Return
		OpenWithParm(w_tooltip, iw_parent)
	Case Else
		il_LastRow = 0
		If IsValid(w_tooltip) Then Close(w_tooltip)
End Choose


end event

type tabpage_instructions from userobject within tab_load_detail
integer x = 15
integer y = 90
integer width = 3394
integer height = 1395
long backcolor = 12632256
string text = "Instructions"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_instructions dw_instructions
end type

on tabpage_instructions.create
this.dw_instructions=create dw_instructions
this.Control[]={this.dw_instructions}
end on

on tabpage_instructions.destroy
destroy(this.dw_instructions)
end on

type dw_instructions from u_base_dw_ext within tabpage_instructions
integer x = 37
integer y = 32
integer width = 3350
integer height = 1315
integer taborder = 2
boolean bringtotop = true
string dataobject = "d_instructions"
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;//IF message.nf_get_app_id() <> 'OTR' Then Return

ii_rc = GetChild( 'frtforwd_code', idwc_address_code )
IF ii_rc = -1 THEN MessageBox("DataWindow Child Error", "Unable to get Child Handle for address code ")

idwc_address_code.SetTrans(SQLCA)
idwc_address_code.Retrieve("FRTFORWD")
end event

event itemchanged;call super::itemchanged;THIS.SelectText (1,15)      
string ls_str 
long ll_cur_row
If is_ColumnName = "frtforwd_code" Then
	
   IF Trim(data) <> "" THEN 
      ll_cur_row = idwc_address_code.Find ( "address_code='"+ &
		             data+"'", 1, idwc_address_code.RowCount() )
						 
      IF ll_cur_row < 1 THEN
         MessageBox( "Freight Forwarder Error", &
			            data + " is not a Valid Freight Forwarder code." )
      	Return 1
      else
	       ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_name")
	       this.setitem(1, "frtforwd_name", ls_str)
	       ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_city")
	       this.setitem(1, "frtforwd_city", ls_str)
	       ls_str = idwc_address_code.GetItemString(ll_cur_row, "address_state")
	       this.setitem(1, "frtforwd_state_abrv", ls_str)
      End if
   Else 
		 this.setitem(1, "frtforwd_name", " ")
		 this.setitem(1, "frtforwd_city", " ")
		 this.setitem(1, "frtforwd_state_abrv", " ")
	End if		
End if
end event

event ue_retrieve;call super::ue_retrieve;integer  li_rtn
			
string   ls_order_info_string, &
         ls_req_load_key, &
         ls_req_order, &
         ls_refetch_stop_num, &
         ls_refetch_order
			
	 
SetPointer( HourGlass! )


s_error  lstr_Error_Info

tab_load_detail.tabpage_instructions.dw_instructions.Reset()

// Turn the redraw off for the window while we retrieve
This.SetRedraw(FALSE)


ls_req_load_key = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.load_key[1]
ls_req_order = tab_load_detail.tabpage_load_detail.dw_load_detail_hdr.object.order_number[1]

lstr_error_info.se_event_name = "ue_retrieve"
lstr_error_info.se_window_name = "w_instructions"
lstr_error_info.se_procedure_name = "dw_instructions"
lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_message = Space(71)

ls_refetch_stop_num  = Space(2)
ls_refetch_order     = Space(7)


Do

   ls_order_info_string = Space(15000)
 //Added Code for Netwise convertion 09/06/16	
  li_rtn =iu_ws_review_queue_load_list.nf_otrt56er(ls_req_load_key, ls_req_order,ls_order_info_string, lstr_Error_info)

// Check the return code from the above function call
If li_rtn <> 0 Then
		tab_load_detail.tabpage_instructions.dw_instructions.ResetUpdate()
		This.SetRedraw(TRUE)
      Return -1
ELSE
      iw_frame.SetMicroHelp( "Ready...")
END IF

     li_rtn = tab_load_detail.tabpage_instructions.dw_instructions.ImportString(Trim(ls_order_info_string))

loop while len(Trim(ls_refetch_order)) > 0

//ls_carrier_code = tab_load_detail.tabpage_single_appt.dw_single_appt.GetItemString(1,"carrier_code") 

tab_load_detail.tabpage_instructions.dw_instructions.SetFocus()

tab_load_detail.tabpage_instructions.dw_instructions.ResetUpdate()

// Turn the redraw on for the window after we retrieve
This.SetRedraw(TRUE)

SetPointer( arrow! )

Return 1



end event

event itemerror;call super::itemerror;return 1
end event

event ue_mousemove;call super::ue_mousemove;
IF IsValid(w_tooltip) Then
	Close(w_tooltip)
END IF

end event

