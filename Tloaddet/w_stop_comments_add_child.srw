HA$PBExportHeader$w_stop_comments_add_child.srw
forward
global type w_stop_comments_add_child from w_base_response_ext
end type
type mle_stop_comments from multilineedit within w_stop_comments_add_child
end type
end forward

global type w_stop_comments_add_child from w_base_response_ext
integer x = 174
integer y = 472
integer width = 2048
integer height = 932
string title = "Stop Comments Add"
long backcolor = 12632256
mle_stop_comments mle_stop_comments
end type
global w_stop_comments_add_child w_stop_comments_add_child

type variables
u_otr003              iu_otr003

u_ws_review_queue_load_list  iu_ws_review_queue_load_list

Character            ic_load_key[7], &
                          ic_stop_code[2]


end variables

forward prototypes
public function integer wf_add_stop_comments ()
end prototypes

public function integer wf_add_stop_comments ();String   ls_load_key, &
         ls_stop_code, &
         ls_comments

integer  li_rtn, &
			li_length, & 
         li_difference

long     ll_Row, &
         ll_RowCount
s_error  lstr_Error_Info

lstr_error_info.se_event_name = "wf_add_stop_comments"
lstr_error_info.se_window_name = "w_stop_comments_add_child"
lstr_error_info.se_procedure_name = "wf_add_stop_comments"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ls_comments = mle_stop_comments.text

ls_load_key  = ic_load_key     
ls_stop_code = ic_stop_code

li_rtn = iu_ws_review_queue_load_list.nf_otrt32er(ls_load_key, ls_stop_code, &
	ls_comments, lstr_error_info)
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
// Check the return code from the above function call
If li_rtn < 0 Then
	Return -1
Else 
   iw_frame.SetMicroHelp( "Ready...")
   Close ( w_stop_comments_add_child) 
End If

Return 1


end function

event ue_base_ok();call super::ue_base_ok;Integer        li_rc
string ls_temp

if Trim(mle_stop_comments.text) <> "" THEN

li_rc = MessageBox( "Modified Data" , "Save changes before closing ?", &
        Question! , YesNoCancel! )
ELSE
   Close ( This )
   iw_frame.SetMicroHelp ("Ready...")
END IF 

IF li_rc = 1 Then
   wf_add_stop_comments()
ELSE
  IF li_rc = 2 Then
     Close ( This )
  END IF
END IF
end event

event close;call super::close;DESTROY iu_otr003
DESTROY iu_ws_review_queue_load_list
end event

event open;call super::open;iu_otr003  =  CREATE u_otr003

iu_ws_review_queue_load_list = CREATE u_ws_review_queue_load_list
 
long          ll_newrow

string        ls_input_string, &
              ls_temp

ls_input_String = Message.StringParm

ls_temp = Left(ls_input_string, 7)	
ic_load_key =  ls_temp

ls_temp =  Mid(ls_input_string, 8, 2)	
ic_stop_code =  ls_temp

This.Title = "Comments for Load Key "+ic_load_key[] + " Stop " + ic_stop_code[]  






end event

on w_stop_comments_add_child.create
int iCurrent
call super::create
this.mle_stop_comments=create mle_stop_comments
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.mle_stop_comments
end on

on w_stop_comments_add_child.destroy
call super::destroy
destroy(this.mle_stop_comments)
end on

event ue_base_cancel;call super::ue_base_cancel;Close(this)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_stop_comments_add_child
integer x = 1179
integer y = 712
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_stop_comments_add_child
integer x = 859
integer y = 712
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_stop_comments_add_child
integer x = 539
integer y = 712
integer taborder = 20
end type

type mle_stop_comments from multilineedit within w_stop_comments_add_child
integer x = 27
integer y = 24
integer width = 1984
integer height = 628
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
boolean border = false
textcase textcase = upper!
integer limit = 700
end type

