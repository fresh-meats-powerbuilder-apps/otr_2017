HA$PBExportHeader$w_214_exception_filter.srw
forward
global type w_214_exception_filter from w_base_response_ext
end type
type dw_carrier from u_base_dw_ext within w_214_exception_filter
end type
type gb_2 from groupbox within w_214_exception_filter
end type
type gb_1 from groupbox within w_214_exception_filter
end type
type dw_from_to_dates from u_base_dw_ext within w_214_exception_filter
end type
type dw_status from u_base_dw_ext within w_214_exception_filter
end type
type dw_exception from u_base_dw_ext within w_214_exception_filter
end type
end forward

global type w_214_exception_filter from w_base_response_ext
integer width = 1719
integer height = 1160
string title = "Shipment Status Exception Filter"
long backcolor = 79741120
dw_carrier dw_carrier
gb_2 gb_2
gb_1 gb_1
dw_from_to_dates dw_from_to_dates
dw_status dw_status
dw_exception dw_exception
end type
global w_214_exception_filter w_214_exception_filter

type variables
integer		ii_rc

DataWindowChild	idwc_carrier, &
		idwc_exception

u_214_exception	iu_214_exception
end variables

on w_214_exception_filter.create
int iCurrent
call super::create
this.dw_carrier=create dw_carrier
this.gb_2=create gb_2
this.gb_1=create gb_1
this.dw_from_to_dates=create dw_from_to_dates
this.dw_status=create dw_status
this.dw_exception=create dw_exception
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_carrier
this.Control[iCurrent+2]=this.gb_2
this.Control[iCurrent+3]=this.gb_1
this.Control[iCurrent+4]=this.dw_from_to_dates
this.Control[iCurrent+5]=this.dw_status
this.Control[iCurrent+6]=this.dw_exception
end on

on w_214_exception_filter.destroy
call super::destroy
destroy(this.dw_carrier)
destroy(this.gb_2)
destroy(this.gb_1)
destroy(this.dw_from_to_dates)
destroy(this.dw_status)
destroy(this.dw_exception)
end on

event open;call super::open;// load any info into the correct fields
date		ld_from_date, &
			ld_to_date, &
			current

string	ls_from_carrier, &
			ls_to_carrier, &
			ls_load_key, &
			ls_status_code 
						
int		li_rtn,&
			li_pos, &
			li_string_len

long		ll_current_row

iu_214_exception = Message.PowerObjectParm

current = today()

IF iu_214_exception.to_carrier <> '' THEN 
    If iu_214_exception.to_carrier = iu_214_exception.from_carrier  THEN
        dw_carrier.object.end_carrier_code[1] = "    "
    End if  
END IF

dw_carrier.SetFocus()
dw_carrier.SetColumn("start_carrier_code")
dw_carrier.SelectText(1, 4)

 
IF iu_214_exception.from_date <> '' THEN
	ld_from_date = Date(iu_214_exception.from_date)
	dw_from_to_dates.SetItem( 1, "from_date", ld_from_date)
	IF iu_214_exception.to_date <> '' THEN 
		ld_to_date = Date(iu_214_exception.to_date)
		dw_from_to_dates.SetItem( 1, "to_date", ld_to_date)
	Else
		dw_from_to_dates.SetItem( 1, "to_date", ld_from_date)
	END IF
Else
	dw_from_to_dates.SetItem( 1, "from_date", RelativeDate(Today(), -1))
	IF iu_214_exception.to_date <> '' THEN 
		ld_to_date = Date(iu_214_exception.to_date)
		dw_from_to_dates.SetItem( 1, "to_date", ld_to_date)
		dw_from_to_dates.SetItem( 1, "from_date", ld_to_date)
	Else
		dw_from_to_dates.SetItem( 1, "to_date", current)
	END IF
END IF
 

dw_carrier.SetItem( 1, "end_carrier_code", iu_214_exception.to_carrier )
dw_carrier.SetItem( 1, "start_carrier_code", iu_214_exception.from_carrier )
dw_exception.SetItem( 1, "exception_code", iu_214_exception.exception_code )
dw_status.SetItem( 1, "status_code", iu_214_exception.status_code )

dw_carrier.SetFocus()

dw_carrier.SelectText(1, 4)

end event

event ue_base_cancel;iu_214_exception.cancel = true
CloseWithReturn( this, iu_214_exception )

end event

event ue_base_ok;long		ll_foundrow

string	ls_exception_code, &
			ls_from_carrier, &
			ls_to_carrier, &
			ls_status_code, &
			ls_from_date, &
			ls_to_date, &
			ls_from_carr, &
			ls_to_carr
				
If dw_carrier.AcceptText() = 1 then
	ls_from_carrier	= " "
   ls_to_carrier  	= " "
     
   ls_from_carrier	= trim( dw_carrier.object.start_carrier_code[1])
   ls_to_carrier   	= trim( dw_carrier.object.end_carrier_code[1])
	
	If Trim(ls_from_carrier) = "" then
		ls_from_carrier = " "
	End if
   If Trim(ls_to_carrier) = "" Then
      ls_to_carrier = ls_from_carrier
   End if
Else
	ls_from_carrier = " "
	ls_to_carrier   = " "
End if
	

	
If dw_from_to_dates.AcceptText() = 1 Then
   ls_from_date  = string( dw_from_to_dates.GetItemDate( 1, "from_date" ), "yyyy-mm-dd" )
	ls_to_date    = string( dw_from_to_dates.GetItemDate( 1, "to_date" ), "yyyy-mm-dd" )
End If

If dw_status.Accepttext() = 1 Then
	ls_status_code         = dw_status.GetItemString( 1, "status_code" )
End If

If dw_exception.Accepttext() = 1 Then
	ls_exception_code         = dw_exception.GetItemString( 1, "exception_code" )
End If

		
iu_214_exception.exception_code    = ls_exception_code
iu_214_exception.from_carrier	     = ls_from_carrier
iu_214_exception.to_carrier	     = ls_to_carrier
iu_214_exception.status_code       = ls_status_code 
iu_214_exception.from_date         = ls_from_date
iu_214_exception.to_date           = ls_to_date
iu_214_exception.cancel		        = false

Message.PowerObjectParm = iu_214_exception

CloseWithReturn( This, iu_214_exception )
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_214_exception_filter
integer x = 1042
integer y = 936
integer taborder = 70
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_214_exception_filter
integer x = 718
integer y = 936
integer taborder = 60
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_214_exception_filter
integer x = 393
integer y = 936
integer taborder = 50
end type

type dw_carrier from u_base_dw_ext within w_214_exception_filter
integer x = 398
integer y = 80
integer width = 933
integer height = 296
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_dddw_start_end_carriers"
end type

event constructor;call super::constructor;InsertRow(0)

ii_rc = GetChild( 'start_carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()

ii_rc = GetChild( 'end_carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()


dw_carrier.SetFocus()
SelectText(1,4)
end event

event itemchanged;call super::itemchanged;//long ll_FoundRow
//
//IF Trim(data) = "" THEN Return
//
//IF dwo.name = "start_carrier_code" Then
//   ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
//   IF ll_FoundRow < 1 THEN
//	  MessageBox( "Carrier Error", data + " is Not a Valid Carrier Code." )
////	  dw_carrier.SetItem(1,"start_carrier_code", "")
//	  dw_carrier.SetFocus()  
//     dw_carrier.SetColumn("start_carrier_code")
//     Return 1
//   END IF		
//End if
//
//IF dwo.name = "end_carrier_code" Then
//   ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
//   IF ll_FoundRow < 1 THEN
//  	  MessageBox( "Carrier Error", data + " is Not a Valid Carrier Code." )
////     dw_carrier.SetItem(1,"end_carrier_code", "")
//     dw_carrier.SetFocus()  
//     dw_carrier.SetColumn("end_carrier_code")
//	  Return 1
//   END IF		
//End if
//
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer	li_len
string	ls_temp

ls_temp = this.GetText()

li_len = len(ls_temp)

this.SelectText(1, li_len)
end event

event getfocus;SelectText(1,6)
end event

type gb_2 from groupbox within w_214_exception_filter
integer x = 91
integer y = 456
integer width = 919
integer height = 440
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Date Range"
end type

type gb_1 from groupbox within w_214_exception_filter
integer x = 325
integer y = 4
integer width = 1074
integer height = 412
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Carrier"
end type

type dw_from_to_dates from u_base_dw_ext within w_214_exception_filter
integer x = 119
integer y = 552
integer width = 823
integer height = 312
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_from_and_to_dates"
end type

event constructor;call super::constructor;InsertRow(0)

//date current
//current = today()
//

 
dw_from_to_dates.SetItem(1, "from_date", RelativeDate(Today(), -1))
dw_from_to_dates.SetItem(1, "to_date", RelativeDate(Today(), 0))
dw_from_to_dates.SetFocus()
SelectText(1,10)
end event

event itemchanged;call super::itemchanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)

Date		ld_from, &
			ld_to


If dwo.name = "to_date" Then
	ld_from = dw_from_to_dates.GetItemDate(1,"from_date")
	ld_to = Date(data)
	If ld_from > ld_to Then
		MessageBox("Date error", "To Date can not be less than the From Date, please reenter dates")
		SelectText(1,10)
		Return 1
	End If
End If
end event

event itemerror;call super::itemerror;Return 1
end event

event getfocus;SelectText(1,10)
end event

type dw_status from u_base_dw_ext within w_214_exception_filter
integer x = 1088
integer y = 484
integer width = 521
integer height = 172
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_status_code"
boolean border = false
end type

event constructor;call super::constructor;insertRow(0)
end event

event itemerror;call super::itemerror;Return 1
end event

event getfocus;THIS.SelectText (1,99)
end event

type dw_exception from u_base_dw_ext within w_214_exception_filter
integer x = 1097
integer y = 704
integer width = 494
integer height = 176
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_exception_code"
boolean border = false
end type

event constructor;call super::constructor;insertRow(0)
ii_rc = GetChild( 'exception_code', idwc_exception )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for type code/record type of EDI214EX.")

idwc_exception.SetTrans(SQLCA)
idwc_exception.Retrieve('EDI214EX')
end event

event itemerror;call super::itemerror;Return 1
end event

event itemchanged;call super::itemchanged;long ll_FoundRow
   
If Trim(data) = "" Then Return

ll_FoundRow = idwc_exception.Find ( "exception_code='"+data+"'", 1, idwc_exception.RowCount() )
IF ll_FoundRow < 1 THEN Return 1

end event

event getfocus;THIS.SelectText (1,99)
end event

