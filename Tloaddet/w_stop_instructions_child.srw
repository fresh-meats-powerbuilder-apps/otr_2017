HA$PBExportHeader$w_stop_instructions_child.srw
forward
global type w_stop_instructions_child from w_base_response_ext
end type
type dw_load_info from u_base_dw_ext within w_stop_instructions_child
end type
type dw_order_info from u_base_dw_ext within w_stop_instructions_child
end type
end forward

global type w_stop_instructions_child from w_base_response_ext
integer x = 55
integer y = 61
integer width = 3094
integer height = 1536
string title = "Traffic Instructions"
long backcolor = 12632256
dw_load_info dw_load_info
dw_order_info dw_order_info
end type
global w_stop_instructions_child w_stop_instructions_child

type variables
//u_otr004      iu_otr004

u_ws_review_queue_load_list		iu_ws_review_queue_load_list
end variables

forward prototypes
public function integer wf_get_instructions_info (string as_load_key, string as_stop_code)
end prototypes

public function integer wf_get_instructions_info (string as_load_key, string as_stop_code);integer               li_rtn

string                ls_load_info_string, &
                      ls_order_info_string, &
                      ls_req_load_key, &
                      ls_req_stop_code

s_error               lstr_Error_Info


SetPointer( HourGlass! )

ls_req_load_key      = as_load_key 
ls_req_stop_code     = as_stop_code
 

lstr_error_info.se_event_name = "wf_get_instructions_info"
lstr_error_info.se_window_name = "w_stop_instructions_child"
lstr_error_info.se_procedure_name = "wf_get_instructions_info"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)
 
ls_load_info_string	= Space(386)
ls_order_info_string = Space(15000)  
/*
li_rtn = iu_otr004.nf_otrt53ar(ls_req_load_key, ls_req_stop_code, &
                       ls_load_info_string, ls_order_info_string, &
                       lstr_error_info, 0)
*/ 

li_rtn = iu_ws_review_queue_load_list.nf_otrt53er(ls_req_load_key, ls_req_stop_code, &
                       ls_load_info_string, ls_order_info_string, &
                       lstr_error_info)
							  
// Check the return code from the above function call
IF li_rtn < 0 THEN
 	return(-1)
ELSE
      iw_frame.SetMicroHelp( "Ready...")
END IF

li_rtn = dw_load_info.ImportString(trim(ls_load_info_string)) 
li_Rtn = dw_order_info.ImportString(trim(ls_order_info_string))
 
return(1)      

end function

on w_stop_instructions_child.create
int iCurrent
call super::create
this.dw_load_info=create dw_load_info
this.dw_order_info=create dw_order_info
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_load_info
this.Control[iCurrent+2]=this.dw_order_info
end on

on w_stop_instructions_child.destroy
call super::destroy
destroy(this.dw_load_info)
destroy(this.dw_order_info)
end on

event close;call super::close;//DESTROY iu_otr004

DESTROY iu_ws_review_queue_load_list
end event

event open;call super::open;string       ls_input_string, &
             ls_temp, &
             ls_stop_code, &
             ls_load_key 

//iu_otr004  =  CREATE u_otr004

iu_ws_review_queue_load_list = CREATE u_ws_review_queue_load_list

ls_input_string = Message.StringParm

ls_temp = Left(ls_input_string, 7)	
ls_load_key =  ls_temp

ls_temp =  Mid(ls_input_string, 8, 2)	
ls_stop_code =  ls_temp

wf_get_instructions_info(ls_load_key, ls_stop_code)

 


end event

event ue_base_ok;call super::ue_base_ok;Close (w_stop_instructions_child)

iw_frame.SetMicroHelp ("Ready...")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_stop_instructions_child
integer x = 1331
integer y = 1312
integer taborder = 20
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_stop_instructions_child
boolean visible = false
integer x = 1174
integer y = 1421
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_stop_instructions_child
integer x = 973
integer y = 1312
end type

type dw_load_info from u_base_dw_ext within w_stop_instructions_child
integer x = 18
integer y = 16
integer width = 2662
integer height = 541
integer taborder = 0
string dataobject = "d_load_info"
boolean border = false
end type

type dw_order_info from u_base_dw_ext within w_stop_instructions_child
integer x = 18
integer y = 557
integer width = 3061
integer height = 720
integer taborder = 0
string dataobject = "d_order_info"
boolean vscrollbar = true
end type

