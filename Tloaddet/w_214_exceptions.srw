HA$PBExportHeader$w_214_exceptions.srw
forward
global type w_214_exceptions from w_base_sheet_ext
end type
type dw_1 from u_base_dw_ext within w_214_exceptions
end type
end forward

global type w_214_exceptions from w_base_sheet_ext
integer x = 5
integer y = 28
integer width = 2907
integer height = 1384
string title = "Shipment Status Exceptions"
long backcolor = 79741120
dw_1 dw_1
end type
global w_214_exceptions w_214_exceptions

type variables
character   ic_control

long	il_RowCount, &
	il_Row, &
	il_error_row, &
	il_error_column

string	is_closemessage_txt 
                 
integer	ii_rc, &
                ii_rowcount, &
                ii_ind = 0, &
	ii_error_column

date         id_date

u_otr006 		iu_otr006

u_214_exception	iu_214_exception


end variables

forward prototypes
public function boolean wf_retrieve ()
public function integer wf_changed ()
public function boolean wf_214_exception_inq ()
public function boolean wf_update ()
public subroutine wf_delete ()
public function boolean wf_pre_save ()
end prototypes

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer

 
li_rc = wf_Changed()

CHOOSE CASE li_rc	//begin 1
	CASE  1	//	a valid change occurred ...
		li_answer = MessageBox("Wait",is_closemessage_txt,question!,yesnocancel!)
		CHOOSE CASE li_answer
			CASE 1	//	yes, save changes
				IF wf_Update() THEN	//	the save succeeded
				ELSE	//	the save failed
					li_answer = MessageBox("Save Failed","Retrieve Anyway?",question!,yesno!)
					IF li_answer = 1 THEN	//	retrieve w/o saving changes
					ELSE	//	cancel the close
						Return( false )
					END IF
				END IF	
			CASE 2 	//	retrieve w/o saving changes
			CASE 3	//	cancel the retrieve
				Return(false)
		END CHOOSE
	CASE  -1 		//	a validation error occurred
		li_answer = MessageBox("Data Entry Error","Retrieve w/o Save?",question!,okcancel!)
		CHOOSE CASE li_answer
			CASE 1	//	retrieve w/o saving changes
			CASE 2	//	cancel the retrieve
				Return( false )
		END CHOOSE
	CASE  -2 	//	a serious validation error, window may not close in this state
		MessageBox("A Serious Error Has Occurred","Correct Error Before Retrieving!",exclamation!,ok!)
		Return(False)
	CASE ELSE	//	nothing has changed
END CHOOSE


OpenWithParm( w_214_exception_filter, iu_214_exception, iw_frame )
iu_214_exception = Message.PowerObjectParm

IF isvalid(iu_214_exception) = False then
	Return True
End If

IF iu_214_exception.cancel = True  Then
   Return True
End If
	

// Turn the redraw off for the window while we retrieve
SetPointer(HourGlass!)
This.SetRedraw(FALSE)


// Populate the datawindows
This.wf_214_exception_inq ()
//li_rc = dw_1.InsertRow(0)
//dw_1.setrow(li_rc)

//dw_3.SetItemStatus ( 1, 0, Primary!, NotModified! )



// Turn the redraw off for the window while we retrieve
This.SetRedraw(TRUE)
SetPointer(Arrow!)

Return False
end function

public function integer wf_changed ();/*
	purpose:	this function will examine all datawindow controls on the window
				that have update capability against the database.  if anything has changed,
				and the change is valid, a 1 is returned.  if an invalid change has occurred
				then a -1 is returned.  if nothing has changed a 0 is returned.

*/

int i, totcontrols, rc, counter
datawindow dw[]

totcontrols = UpperBound(this.control)

FOR i = 1 to totcontrols
	IF TypeOf(this.control[i]) = datawindow! THEN
		counter ++
		dw[counter] = this.control[i]
			rc = dw[counter].AcceptText()
			IF rc = -1 THEN 
				RETURN -1
			ELSE
				CONTINUE
			END IF
			counter --
	END IF
NEXT

FOR i = 1 to counter
	IF dw[i].ModifiedCount() > 0 THEN
		is_closemessage_txt = "Save Changes?"
		RETURN 1
	END IF
NEXT

RETURN 0
	
end function

public function boolean wf_214_exception_inq ();boolean	lb_rtn

integer	li_RPCrtn

s_error	lstr_error_info

string 	ls_req_exception_code, &
			ls_req_from_carr, &
			ls_req_to_carr, &
			ls_req_status_code, &
			ls_req_from_date, &
			ls_req_to_date
					 
dw_1.SetRedraw(False)
dw_1.Reset( ) 

ls_req_exception_code =	iu_214_exception.exception_code
ls_req_from_carr      = iu_214_exception.from_carrier
ls_req_to_carr        = iu_214_exception.to_carrier
ls_req_status_code    = iu_214_exception.status_code
ls_req_from_date      = iu_214_exception.from_date
ls_req_to_date        = iu_214_exception.to_date

 
lstr_error_info.se_event_name = "starts in wf_retrieve "
lstr_error_info.se_window_name = "w_214_exception"
lstr_error_info.se_procedure_name = "wf_214_exception_inq"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)


//lb_rtn = iu_otr006.nf_otrt68ar(ls_req_exception_code, &				
// 										 ls_req_from_carr, &
//										 ls_req_to_carr, &
//										 ls_req_status_code, &
//										 ls_req_from_date, &
//										 ls_req_to_date, &
//										 dw_1, & 
// 										 lstr_error_info) 
										  
		  
// Check the return code from the above function call
IF lb_rtn = False THEN
 	iw_frame.SetMicroHelp( "Invalid Inquiry Attempt" )
   dw_1.InsertRow(0)
	return(False)
ELSE
      iw_frame.SetMicroHelp( "Ready...")
END IF										  
											 
This.Title = "Shipment Status Exceptions for " + ls_req_from_carr + " thru " + ls_req_to_carr
dw_1.SetFocus()
dw_1.ScrollToRow(1)
dw_1.SetColumn("status_date")
dw_1.SelectText(1,10)
dw_1.ResetUpdate()
dw_1.SetRedraw(true)
Return( True )

end function

public function boolean wf_update ();Boolean				lb_return

Long			 		ll_Row,&
						ll_RowCount, &
						ll_Count, &
						ll_del_row

Integer           li_rtn, &
						li_rc, &
						li_sequence_no

String            ls_exception_code, &
						ls_date_added, ls_time_added, &
						ls_load_key, ls_stop_code, &
						ls_tran_date, ls_tran_time, ls_tran_time_code, &
						ls_status_code, ls_status_date, ls_status_time, ls_status_time_code, &
						ls_status_city, ls_status_state, ls_status_country, &
						ls_carrier_code, ls_carrier_equipment, ls_carrier_invoice, &
						ls_reason_code, ls_process_code, ls_comment_line1, ls_comment_line2, &
						ls_space, &
						ls_temp_date, &
						ls_temp_time
						
Date					ld_date, ld_temp_date
Time					lt_time, lt_temp_time

dwItemStatus		status

s_error				lstr_Error_Info



lb_return = wf_pre_save()
If lb_return = FALSE Then
	iw_frame.SetMicroHelp("No Data Modified")
	Return False 
End If


IF dw_1.AcceptText() = -1 THEN Return(False)

SetPointer(HourGlass!)

ll_RowCount = dw_1.RowCount()
ls_space		= " "
 
lstr_error_info.se_event_name = "wf_update"
lstr_error_info.se_window_name = "w_214_exception"
lstr_error_info.se_procedure_name = "otrt69ar"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

dw_1.Setredraw(FALSE)
DO WHILE ll_Row <= ll_RowCount 
	ll_Row = dw_1.GetNextModified( ll_Row, Primary! )
	IF ll_Row > 0  THEN 
		
			ll_Count = ll_Count + 1
			ls_exception_code	   = dw_1.GetItemString( ll_Row, "exception_code" )
			ld_date              = dw_1.GetItemDate( ll_row, "date_added" )
			ls_date_added		   = String( ld_date, "yyyy-mm-dd" )
			lt_time					= dw_1.GetItemTime( ll_row, "time_added" )
			ls_time_added  	   = String( lt_time, "hh.mm.ss" )
			li_sequence_no       = dw_1.GetItemNumber( ll_Row, "sequence_no" )
			ls_load_key     	   = dw_1.GetItemString( ll_Row, "load_key" )
			ls_stop_code   	   = dw_1.GetItemString( ll_Row, "stop_code" )
//			ls_tran_date         = dw_1.GetItemString( ll_Row, "tran_date" )
//			ls_tran_time         = dw_1.GetItemString( ll_Row, "tran_time" )
			ls_tran_time_code    = dw_1.GetItemString( ll_Row, "tran_time_code" )
			ls_status_code       = dw_1.GetItemString( ll_Row, "status_code" )
			ls_temp_date         = dw_1.GetItemString( ll_Row, "status_date" )
			
			ls_status_date       = mid(ls_temp_date,5,4) + "-" + left(ls_temp_date,2) + &
					                 "-" +  mid(ls_temp_date,3,2)
										  
			ls_temp_time         = dw_1.GetItemString( ll_Row, "status_time" )
			ls_status_time       = left(ls_temp_time,2) + "." + mid(ls_temp_time,3,2) + &
			                       "." +  mid(ls_temp_time,5,2)
										  
			ls_status_time_code  = dw_1.GetItemString( ll_Row, "status_time_code" )
			ls_status_city       = dw_1.GetItemString( ll_Row, "status_city" )		
			ls_status_state      = dw_1.GetItemString( ll_Row, "status_state" )
			ls_status_country    = dw_1.GetItemString( ll_Row, "status_country" )
			ls_carrier_code      = dw_1.GetItemString( ll_Row, "carrier_code" )
			ls_carrier_equipment	= dw_1.GetItemString( ll_Row, "carrier_equipment" )
			ls_carrier_invoice   = dw_1.GetItemString( ll_Row, "carrier_invoice" )
			ls_reason_code       = dw_1.GetItemString( ll_Row, "reason_code" )
			ls_process_code      = dw_1.GetItemString( ll_Row, "processed_code" )
			ls_comment_line1     = dw_1.GetItemString( ll_Row, "comment_line1" )
			ls_comment_line2     = dw_1.GetItemString( ll_Row, "comment_line2" )
			
			
			ls_temp_date         = dw_1.GetItemString( ll_Row, "tran_date" )
			ls_tran_date         = mid(ls_temp_date,5,4) + "-" + left(ls_temp_date,2) + &
					                 "-" +  mid(ls_temp_date,3,2)
         If IsDate(ls_tran_date) = False then
     	      ld_temp_date      = dw_1.GetItemDate( ll_row, "processed_date" )
				ls_tran_date      = String( ld_temp_date, "yyyy-mm-dd" )
   		End if
			
			ls_temp_time         = dw_1.GetItemString( ll_Row, "tran_time" )
			ls_tran_time         = left(ls_temp_time,2) + "." + mid(ls_temp_time,3,2) + &
			                       "." +  mid(ls_temp_time,5,2)
         If IsTime(ls_tran_time) = False then
     	      lt_temp_time      = dw_1.GetItemTime( ll_row, "processed_time" )
				ls_tran_time      = String( lt_temp_time, "hh.mm.ss" )
   		End if
			
//			li_rtn = iu_otr006.nf_otrt69ar(  ls_exception_code, &
//														ls_date_added, &
//														ls_time_added, &
//														li_sequence_no, &
//														ls_load_key, &
//														ls_stop_code, &
//														ls_tran_date, &
//														ls_tran_time, &
//														ls_tran_time_code, &
//														ls_status_code, &
//														ls_status_date, &
//														ls_status_time, &
//														ls_status_time_code, &
//														ls_status_city, &
//														ls_status_state, &
//														ls_status_country, &
//														ls_carrier_code, &
//														ls_carrier_equipment, &
//														ls_carrier_invoice, &
//														ls_reason_code, &
//														ls_process_code, &
//														ls_comment_line1, &
//														ls_comment_line2, &
//													   lstr_Error_Info, 0 )

		CHOOSE CASE li_rtn
			CASE is < 0 
				dw_1.Setredraw(TRUE)
//				li_rc = MessageBox( "Update Error",  lstr_Error_Info.se_message + &
//										ls_load_key + " has failed due to an Error.  Do you want to Continue?", &
//										Question!, YesNo! )
//				iw_frame.SetMicroHelp(lstr_error_info.se_message)
//				ll_Count --
				li_rc = 2
				dw_1.Setredraw(FALSE)
			CASE is > 0 
				If iu_otr006.ii_messagebox_rtn = 3 then
					li_rc = 2
				End if
//				iw_frame.SetMicroHelp(lstr_error_info.se_message)						
				dw_1.Setredraw(FALSE)
				dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
			CASE 0
				iw_frame.SetMicroHelp("Processing Complete")
				dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
				li_rc = 0
			END CHOOSE

			IF li_rc = 2 THEN 			// NO  do not continue processing
				dw_1.Setredraw(TRUE)
				Return(False)
			END IF
			If (ls_process_code = "R" or ls_process_code = "A") then
				If li_rc = 0 then
			      ll_del_row = ll_row
				   dw_1.DeleteRow( ll_del_row)
				End if
			End If
			ll_Row --
			ll_RowCount --
	ELSE
		dw_1.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )
		ll_Row = ll_RowCount + 1
	END IF
LOOP
dw_1.scrolltorow(dw_1.getrow())
dw_1.ResetUpdate()
dw_1.setredraw(true)
SetPointer(Arrow!)

Return(True)


end function

public subroutine wf_delete ();
Integer    li_rtn, &
			  li_rc, &
           li_delete_count, &
           li_msg, &
			  li_sequence_no

Long		  ll_Row,&
           ll_del_row, &
			  ll_RowCount

Boolean	  ib_any_selected_rows, &
			  lb_ret

String     ls_exception_code, &
			  ls_date_added, ls_time_added, &
			  ls_load_key, ls_stop_code, &
			  ls_tran_date, ls_tran_time, ls_tran_time_code, &
			  ls_status_code, ls_status_date, ls_status_time, ls_status_time_code, &
			  ls_status_city, ls_status_state, ls_status_country, &
			  ls_carrier_code, ls_carrier_equipment, ls_carrier_invoice, &
			  ls_reason_code, ls_process_code, ls_comment_line1, ls_comment_line2, &
			  ls_space
						
Date		  ld_date
Time		  lt_time

dwItemStatus		status

s_error    lstr_Error_Info


lstr_error_info.se_event_name = "wf_delete"
lstr_error_info.se_window_name = "w_214_exception"
lstr_error_info.se_procedure_name = "otrt69ar delete "
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)


ll_RowCount = dw_1.RowCount()
ib_any_selected_rows = false
If ll_RowCount > 0 Then
  FOR ll_Row = 1  TO ll_RowCount
    If dw_1.IsSelected(ll_Row) = True Then
		ib_any_selected_rows = True
	 END IF
  NEXT
END IF

// If no rows are selected - get out
IF NOT ib_any_selected_rows Then Return

li_msg = MessageBox("Delete Verification", &
         "Do you want to delete selected rows?", &
         Question!, YesNo!, 1)

dw_1.SetRedraw(False)

//    P10-delete-prompt-end
IF li_msg = 2 then
	dw_1.SetRedraw(True)
	iw_frame.SetMicroHelp("No Rows Deleted")
	Return
End If


SetPointer(HourGlass!)
If ll_RowCount > 0 Then
  FOR ll_Row = 1  TO ll_RowCount
    If dw_1.IsSelected(ll_Row) = True Then

			ls_exception_code	   = dw_1.GetItemString( ll_Row, "exception_code" )
			If IsNull(ls_exception_code) then
				ls_exception_code = space(8)
			end if
				
			ld_date              = dw_1.GetItemDate( ll_row, "date_added" )
			ls_date_added		   = String( ld_date, "yyyy-mm-dd" )
			lt_time					= dw_1.GetItemTime( ll_row, "time_added" )
			ls_time_added  	   = String( lt_time, "hh.mm.ss" )
			li_sequence_no       = dw_1.GetItemNumber( ll_Row, "sequence_no" )
			
			ls_load_key     	   = dw_1.GetItemString( ll_Row, "load_key" )
			If IsNull(ls_load_key) then
				ls_load_key       = space(7)
			end if
				
			ls_stop_code   	   = dw_1.GetItemString( ll_Row, "stop_code" )
			If IsNull(ls_stop_code) then
				ls_stop_code      = space(2)
			end if
				
			ls_tran_date         = dw_1.GetItemString( ll_Row, "tran_date" )
			ls_tran_time         = dw_1.GetItemString( ll_Row, "tran_time" )
			
			ls_tran_time_code    = dw_1.GetItemString( ll_Row, "tran_time_code" )
			If IsNull(ls_tran_time_code) then
				ls_tran_time_code = space(2)
			end if
				
			ls_status_code       = dw_1.GetItemString( ll_Row, "status_code" )
			If IsNull(ls_status_code) then
				ls_status_code    = space(2)
			end if
				
			ls_status_date       = dw_1.GetItemString( ll_Row, "status_date" )
			ls_status_time  	   = dw_1.GetItemString( ll_Row, "status_time" )
			
			ls_status_time_code  = dw_1.GetItemString( ll_Row, "status_time_code" )
			If IsNull(ls_status_time_code) then
				ls_status_time_code = space(2)
			end if
				
			ls_status_city       = dw_1.GetItemString( ll_Row, "status_city" )		
			If IsNull(ls_status_city) then
				ls_status_city    = space(30)
			end if
				
			ls_status_state      = dw_1.GetItemString( ll_Row, "status_state" )
			If IsNull(ls_status_state) then
				ls_status_state   = space(2)
			end if
				
			ls_status_country    = dw_1.GetItemString( ll_Row, "status_country" )
			If IsNull(ls_status_country) then
				ls_status_country = space(3)
			end if
				
			ls_carrier_code      = dw_1.GetItemString( ll_Row, "carrier_code" )
			If IsNull(ls_carrier_code) then
				ls_carrier_code   = space(4)
			end if
				
			ls_carrier_equipment	= dw_1.GetItemString( ll_Row, "carrier_equipment" )
			If IsNull(ls_carrier_equipment) then
				ls_carrier_equipment = space(14)
			end if
				
			ls_carrier_invoice   = dw_1.GetItemString( ll_Row, "carrier_invoice" )
			If IsNull(ls_carrier_invoice) then
				ls_carrier_invoice = space(22)
			end if
				
			ls_reason_code       = dw_1.GetItemString( ll_Row, "reason_code" )
			If IsNull(ls_reason_code) then
				ls_reason_code    = space(3)
			end if
				
			ls_process_code      = "R"
			ls_comment_line1     = dw_1.GetItemString( ll_Row, "comment_line1" )
			If IsNull(ls_comment_line1) then
				ls_comment_line1  = space(30)
			end if
				
			ls_comment_line2     = dw_1.GetItemString( ll_Row, "comment_line2" )
			If IsNull(ls_comment_line2) then
				ls_comment_line2  = space(30)
			end if


//			li_rtn = iu_otr006.nf_otrt69ar(  ls_exception_code, &
//														ls_date_added, &
//														ls_time_added, &
//														li_sequence_no, &
//														ls_load_key, &
//														ls_stop_code, &
//														ls_tran_date, &
//														ls_tran_time, &
//														ls_tran_time_code, &
//														ls_status_code, &
//														ls_status_date, &
//														ls_status_time, &
//														ls_status_time_code, &
//														ls_status_city, &
//														ls_status_state, &
//														ls_status_country, &
//														ls_carrier_code, &
//														ls_carrier_equipment, &
//														ls_carrier_invoice, &
//														ls_reason_code, &
//														ls_process_code, &
//														ls_comment_line1, &
//														ls_comment_line2, &
//													   lstr_Error_Info, 0 )


		 CHOOSE CASE li_rtn
		   CASE 0
 			  	ll_del_row = ll_row
 				dw_1.DeleteRow( ll_del_row )
					ll_Row --
			 	   ll_RowCount --
				   li_rc = 0
         CASE ELSE
             dw_1.SetRow(ll_row)
             EXIT
		 END CHOOSE
    END IF
  Next
END IF
 
dw_1.ResetUpdate()
dw_1.setredraw(true)
SetPointer(Arrow!)
iw_frame.SetMicroHelp( "Delete Processing Completed") 
Return      


end subroutine

public function boolean wf_pre_save ();long		ll_Total_No_Rows, &
         ll_Row

string	ls_status_date, &
			ls_status_time, &
			ls_status_time_code, &
			ls_reason_code, &
			ls_status_city, &
			ls_status_state, &
			ls_status_country, &
			ls_ibp_processed_code, &
			ls_temp_status_date, &
			ls_temp_status_time

ll_Row = 0

dw_1.AcceptText()

ll_Total_No_Rows = dw_1.RowCount()

ll_Row = dw_1.GetNextModified(ll_Row, PRIMARY!)

If ll_Row > 0 then
	ls_ibp_processed_code      = dw_1.GetItemString( ll_Row, "processed_code" )
	IF ls_ibp_processed_code = "R" Then
   	Return True 
	End If
Else
	Return False
End If


Do while ll_Row <> 0
   ls_temp_status_date = dw_1.GetItemString(ll_Row, "status_date")
	ls_status_date = left(ls_temp_status_date,2) + "-" + mid(ls_temp_status_date,3,2) + &
					   "-" +  mid(ls_temp_status_date,5,4)
   If IsDate(ls_status_date) = False then
     	MessageBox("Status Date", "Please enter a valid Status Date")
      dw_1.SetColumn("status_date")
		ii_error_column = dw_1.getcolumn()
      il_error_row = ll_Row
		dw_1.postevent("ue_post_tab")
      Return FALSE     
   End if

   ls_temp_status_time = dw_1.GetItemString(ll_Row, "status_time")
	ls_status_time = left(ls_temp_status_time,2) + "." + mid(ls_temp_status_time,3,2) + &
				   "." +  mid(ls_temp_status_time,5,2)
   If IsTime(ls_status_time) = FALSE then
      MessageBox("Status Time", "Please enter a valid Status Time")
      dw_1.SetColumn("status_Time")
   	  ii_error_column = dw_1.getcolumn()
      il_error_row = ll_Row
	     dw_1.postevent("ue_post_tab")
      Return FALSE    
   End if
	
//   If String(lt_ApptTime) = '00:00:00' then
//      MessageBox("Appointment Time", "Please enter an Appointment Time")
//      dw_appt_exception.SetColumn("appt_Time")
//		ii_error_column = dw_appt_exception.getcolumn()
//      il_error_row = ll_Row
//		dw_appt_exception.postevent("ue_post_tab")
//      Return FALSE    
//   End if
//   ld_ApptDate = dw_appt_exception.GetItemDate(ll_Row, "appt_date")
//   If IsDate(String(ld_ApptDate))= FALSE then      
//     	MessageBox("Appointment Date", "Please enter an Appointment Date")
//      dw_appt_exception.SetColumn("appt_date")
//		ii_error_column = dw_appt_exception.getcolumn()
//      il_error_row = ll_Row
//		dw_appt_exception.postevent("ue_post_tab")
//      Return FALSE     
//   End if
   ll_Row = dw_1.GetNextModified(ll_Row, PRIMARY!)
Loop 


Return TRUE
end function

on w_214_exceptions.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_214_exceptions.destroy
call super::destroy
destroy(this.dw_1)
end on

event ue_postopen;call super::ue_postopen;iu_otr006  =  CREATE u_otr006


IF wf_retrieve() THEN Close( this )
end event

event close;call super::close;Destroy	u_otr006
end event

event open;call super::open;datawindowchild lddwc_edilater

dw_1.InsertRow(0)

dw_1.getchild("reason_code",lddwc_edilater)
lddwc_edilater.settrans(sqlca)
lddwc_edilater.retrieve("EDILATER")

dw_1.ResetUpdate()
This.SetRedraw(True)

end event

event activate;call super::activate;iw_frame.im_menu.mf_disable("m_insert")
iw_frame.im_menu.mf_disable("m_print")

If message.nf_get_app_id() <> 'OTR' Then 
	iw_frame.im_menu.mf_disable("m_save")
   iw_frame.im_menu.mf_disable("m_delete")
End If

end event

type dw_1 from u_base_dw_ext within w_214_exceptions
event ue_post_tab pbm_custom53
integer width = 2866
integer height = 1252
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_214_exception"
boolean vscrollbar = true
end type

event ue_post_tab;scrolltorow(il_error_row)
setrow(il_error_row)
setcolumn(ii_error_column)
setfocus()

end event

event itemchanged;call super::itemchanged;
long		ll_row

string	ls_status_date, &
			ls_temp_status_date, &
			ls_status_time, &
			ls_temp_status_time

Integer  li_CurrentCol

ll_row = 0
li_CurrentCol = This.GetColumn()
dw_1.AcceptText()
ll_Row = dw_1.GetNextModified(ll_Row, PRIMARY!)

//ls_status_date = left(ls_temp_status_date,2) + "-" + mid(ls_temp_status_date,3,2) + &
//					   "-" +  mid(ls_temp_status_date,5,4)

//tab_load_detail.tabpage_load_detail.dw_check_call.SetColumn("stop_code")
//			  tab_load_detail.tabpage_load_detail.dw_check_call.Setfocus()
//			  SelectText(1,2)


Choose Case dwo.name
	Case "status_date"
		ls_temp_status_date = dw_1.GetItemString(row, "status_date")
		ls_status_date = left(ls_temp_status_date,2) + "-" + mid(ls_temp_status_date,3,2) + &
					   "-" +  mid(ls_temp_status_date,5,4)
	   If IsDate(ls_status_date) = False then
			MessageBox("Status Date", "Please enter a valid Status Date.", StopSign!, OK!)
		  	dw_1.SetColumn("status_date")
		   ii_error_column = dw_1.getcolumn()
         il_error_row = ll_Row
		   dw_1.postevent("ue_post_tab")
//			dw_1.Setfocus()
//			SelectText(1,10)
         Return       
		End If
		
	Case "status_time"
		ls_temp_status_time = dw_1.GetItemString(row, "status_time")
		ls_status_time = left(ls_temp_status_time,2) + "." + mid(ls_temp_status_time,3,2) + &
					   "." +  mid(ls_temp_status_time,5,2)
   	If IsTime(ls_status_time) = FALSE then
     		MessageBox("Status Time", "Please enter a valid Status Time.", StopSign!, OK!)
     		dw_1.SetColumn("status_time")
			dw_1.Setfocus()
			dw_1.SelectText(1,8)
         Return      
		End If
		
End Choose

end event

event doubleclicked;call super::doubleclicked;//dw_1.SelectRow( row,true )
//dw_1.ScrollToRow(row) 
end event

event constructor;call super::constructor;This.is_selection = "3"
end event

