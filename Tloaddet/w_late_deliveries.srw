HA$PBExportHeader$w_late_deliveries.srw
forward
global type w_late_deliveries from w_netwise_sheet
end type
type dw_1 from u_netwise_dw within w_late_deliveries
end type
type dw_2 from u_netwise_dw within w_late_deliveries
end type
end forward

global type w_late_deliveries from w_netwise_sheet
integer x = 9
integer y = 28
integer width = 2825
integer height = 1404
string title = "Late Deliveries"
long backcolor = 79741120
dw_1 dw_1
dw_2 dw_2
end type
global w_late_deliveries w_late_deliveries

type variables
string is_closemessage_txt

u_late_deliveries_filter   iu_late_deliveries_filter
u_validate_customer	iu_validate_customer

u_otr006   iu_otr006
u_ws_cust_service iu_ws_cust_service


end variables

forward prototypes
public function boolean wf_retrieve ()
public function integer wf_late_delv_inq ()
public function integer wf_header_parameters ()
end prototypes

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer

OpenWithParm( w_late_deliveries_filter, iu_late_deliveries_filter, iw_frame )
iu_late_deliveries_filter = Message.PowerObjectParm

If isvalid(iu_late_deliveries_filter) = False then
	Return True
End If

IF iu_late_deliveries_filter.cancel = True  Then
     Return True
End If

// Turn the redraw off for the window while we retrieve
This.SetRedraw(FALSE)

// Populate the datawindow #2
This.wf_late_delv_inq ()

// Populate the datawindow #2
This.wf_header_parameters ()

// Turn the redraw off for the window while we retrieve
This.SetRedraw(TRUE)

Return FALSE




end function

public function integer wf_late_delv_inq ();integer			li_rtn

string         ls_carrier, &
               ls_complex,  &
               ls_sales_loc, &
               ls_billto_cust, &
               ls_shipto_cust, &
               ls_tsr, &
					ls_svc_region, &
					ls_division, &
					ls_delv_from_date, &
					ls_delv_to_date, &
					ls_trans_mode               

s_error        lstr_Error_Info

dw_1.Reset()
dw_2.Reset()
SetPointer( HourGlass! )

ls_carrier			= iu_late_deliveries_filter.carrier
ls_complex			= iu_late_deliveries_filter.complex
ls_sales_loc		= iu_late_deliveries_filter.sales_loc 
ls_billto_cust		= iu_late_deliveries_filter.billto_cust
ls_shipto_cust		= iu_late_deliveries_filter.shipto_cust
ls_tsr				= iu_late_deliveries_filter.tsr 
ls_svc_region		= iu_late_deliveries_filter.svc_region
ls_division			= iu_late_deliveries_filter.division
ls_delv_from_date	= iu_late_deliveries_filter.delv_from_date 
ls_delv_to_date	= iu_late_deliveries_filter.delv_to_date
ls_trans_mode		= iu_late_deliveries_filter.trans_mode


lstr_error_info.se_event_name = "wf_late_delv_inq"
lstr_error_info.se_window_name = "w_late_deliveries"
lstr_error_info.se_procedure_name = "wf_late_delv_inq"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)


//li_rtn = iu_otr006.nf_otrt74ar(ls_trans_mode, ls_delv_from_date, &
//         ls_delv_to_date, ls_billto_cust, ls_shipto_cust, ls_complex, &
//			ls_carrier, ls_sales_loc, ls_tsr, ls_svc_region, ls_division, &
//			dw_2, lstr_error_info)

li_rtn = iu_ws_cust_service.nf_otrt74er(ls_trans_mode, ls_delv_from_date, &
         ls_delv_to_date, ls_billto_cust, ls_shipto_cust, ls_complex, &
			ls_carrier, ls_sales_loc, ls_tsr, ls_svc_region, ls_division, &
			dw_2, lstr_Error_Info)


// Check the return code from the above function call
IF li_rtn <> 0 THEN
 	iw_frame.SetMicroHelp( "Invalid Inquiry Attempt" )
   dw_2.InsertRow(0)
	return(-1)
ELSE
      iw_frame.SetMicroHelp( "Ready...")
END IF										  

dw_2.GroupCalc()
dw_2.SetRedraw(true)		
		
This.Title = "Late Deliveries for " + ls_delv_from_date + " to " + ls_delv_to_date + " using transmode " + ls_trans_mode 
If ls_delv_from_date = ls_delv_to_date then
   This.Title = "Late Deliveries for " + ls_delv_from_date + " using transmode " + ls_trans_mode
End if


dw_1.ResetUpdate()
dw_1.SetRedraw(true)
dw_2.ResetUpdate()
dw_2.SetRedraw(true)
Return( 1 )










end function

public function integer wf_header_parameters ();integer			li_rtn

string         ls_carrier, &
               ls_complex,  &
               ls_sales_loc, &
               ls_billto_cust, &
               ls_shipto_cust, &
               ls_tsr, &
					ls_svc_region, &
					ls_division, &
					ls_param_string

s_error        lstr_Error_Info

dw_1.Reset()
SetPointer( HourGlass! )
//iu_late_deliveries_filter = Message.PowerObjectParm

ls_carrier			= iu_late_deliveries_filter.carrier
ls_complex			= iu_late_deliveries_filter.complex
ls_sales_loc		= iu_late_deliveries_filter.sales_loc 
ls_billto_cust		= iu_late_deliveries_filter.billto_cust
ls_shipto_cust		= iu_late_deliveries_filter.shipto_cust
ls_tsr				= iu_late_deliveries_filter.tsr 
ls_svc_region		= iu_late_deliveries_filter.svc_region
ls_division			= iu_late_deliveries_filter.division


If iu_late_deliveries_filter.carrier > "    " Then
     ls_carrier = ("Carrier: " + iu_late_deliveries_filter.carrier + "    ")
End IF
	
If iu_late_deliveries_filter.complex > "   " Then
     ls_complex = (" Complex: " + iu_late_deliveries_filter.complex + "    ")
End IF
	
If iu_late_deliveries_filter.sales_loc > "   " Then
     ls_sales_loc = ("Sales Location: " + iu_late_deliveries_filter.sales_loc + "    ")
End IF
	
If iu_late_deliveries_filter.svc_region > "   " Then
     ls_svc_region = (" SVC Region: " + iu_late_deliveries_filter.svc_region + "    ")
End IF
	
If iu_late_deliveries_filter.tsr > "   " Then
     ls_tsr = (" TSR: " + iu_late_deliveries_filter.tsr + "    ")
End IF
	
If iu_late_deliveries_filter.division > "  " Then
     ls_division = (" Division: " + iu_late_deliveries_filter.division + "    ")
End IF
	
If iu_late_deliveries_filter.billto_cust > "       " Then
     ls_billto_cust = ("Bill to Customer: " + iu_late_deliveries_filter.billto_cust + "    ")
End IF
	
If iu_late_deliveries_filter.shipto_cust > "       " Then
     ls_shipto_cust = ("Ship to Customer: " + iu_late_deliveries_filter.shipto_cust + "    ")
End IF
	
ls_param_string = ls_carrier + ls_complex + '~t' + ls_sales_loc + ls_svc_region + &
                  ls_tsr + ls_division + '~t'  + ls_billto_cust + ls_shipto_cust

dw_1.ImportString(ls_param_string)

dw_1.ResetUpdate()
dw_1.SetRedraw(true)
Return( 1 )










end function

on w_late_deliveries.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.dw_2=create dw_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.dw_2
end on

on w_late_deliveries.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.dw_2)
end on

event close;call super::close;DESTROY iu_otr006
DESTROY iu_ws_cust_service
end event

event open;call super::open;dw_1.insertrow(0)

dw_2.InsertRow(0) 


end event

event ue_postopen;call super::ue_postopen;iu_otr006  =  CREATE u_otr006
iu_ws_cust_service = CREATE u_ws_cust_service

IF wf_retrieve() THEN Close( this )

end event

event ue_printwithsetup;call super::ue_printwithsetup;////override anchestor
//this.triggerevent("ue_fileprint")
end event

event activate;call super::activate;iw_frame.im_menu.mf_disable("m_delete")
iw_frame.im_menu.mf_disable("m_save")
iw_frame.im_menu.mf_disable("m_insert")

end event

event ue_fileprint;call super::ue_fileprint;dw_2.Print( )
end event

type dw_1 from u_netwise_dw within w_late_deliveries
integer x = 32
integer y = 32
integer width = 2738
integer height = 236
integer taborder = 0
boolean bringtotop = true
string dataobject = "d_late_deliveries_header"
end type

type dw_2 from u_netwise_dw within w_late_deliveries
integer x = 41
integer y = 296
integer width = 2729
integer height = 988
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_late_deliveries_view"
boolean vscrollbar = true
boolean border = false
end type

event doubleclicked;call super::doubleclicked;String          ls_load_key, &
                ls_input_string

w_load_detail    lw_load_detail 

is_ObjectAtPointer = GetObjectAtPointer()
is_ObjectAtPointer = Left( is_ObjectAtPointer, Pos( is_ObjectAtPointer, "~t") - 1 )

If is_ObjectAtPointer  = "load_key" Then
   
   ls_load_key = This.GetItemString(row, "load_key")
   ls_input_string = ls_load_key
   OpenSheetWithParm(lw_load_detail, ls_input_string, iw_frame, 0 , Original!)     
End if

end event

