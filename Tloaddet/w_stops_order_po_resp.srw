HA$PBExportHeader$w_stops_order_po_resp.srw
forward
global type w_stops_order_po_resp from w_base_response_ext
end type
type dw_stops_order_po_resp from u_base_dw_ext within w_stops_order_po_resp
end type
end forward

global type w_stops_order_po_resp from w_base_response_ext
integer x = 594
integer y = 444
integer width = 1367
integer height = 912
string title = "Appointment Exception Filter "
long backcolor = 12632256
dw_stops_order_po_resp dw_stops_order_po_resp
end type
global w_stops_order_po_resp w_stops_order_po_resp

type variables
u_otr002    iu_otr002

 u_ws_cust_service iu_ws_cust_service
end variables

forward prototypes
public function boolean wf_get_stops_order_po_resp (string as_load_key, string as_stop_code)
end prototypes

public function boolean wf_get_stops_order_po_resp (string as_load_key, string as_stop_code);integer  li_rtn

string   ls_stops_detail_string

s_error  lstr_Error_Info

lstr_error_info.se_event_name = "wf_get_stops_order_po_resp"
lstr_error_info.se_window_name = "w_stops_order_po_resp"
lstr_error_info.se_procedure_name = "wf_get_stops_order_po_resp"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)


SetPointer(HourGlass!)

cb_base_ok.SetFocus()

dw_stops_order_po_resp.Reset()

ls_stops_detail_string = Space(1876)
	
//li_rtn = iu_otr002.nf_otrt33ar(as_load_key, as_stop_code,&
//	 		ls_stops_detail_string, lstr_error_info, 0)

li_rtn = iu_ws_cust_service.nf_otrt33er(as_load_key, as_stop_code,ls_stops_detail_string, lstr_error_info)


// Check the return code from the above function call
If li_rtn <> 0 Then
 		Return False 
End If

li_rtn = dw_stops_order_po_resp.ImportString(Trim(ls_stops_detail_string))

Return TRUE


end function

event close;call super::close;DESTROY iu_otr002

DESTROY u_ws_cust_service
end event

event open;call super::open;iu_otr002  =  CREATE u_otr002
iu_ws_cust_service = Create u_ws_cust_service

long          ll_newrow

string        ls_input_string, &
              ls_temp, &
              as_stop_code, &
              as_load_key

ls_input_String = Message.StringParm

ls_temp = Left(ls_input_string, 7)	
as_load_key =  ls_temp

ls_temp =  Mid(ls_input_string, 8, 2)	
as_stop_code =  ls_temp


This.Title = "Information for  Load Key "+as_load_key + " Stop " + as_stop_code  

ll_newrow = dw_stops_order_po_resp.InsertRow(0) 
dw_stops_order_po_resp.ScrollToRow(ll_newrow)

wf_get_stops_order_po_resp(as_load_key, as_stop_code)

 


end event

on w_stops_order_po_resp.create
int iCurrent
call super::create
this.dw_stops_order_po_resp=create dw_stops_order_po_resp
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_stops_order_po_resp
end on

on w_stops_order_po_resp.destroy
call super::destroy
destroy(this.dw_stops_order_po_resp)
end on

type cb_base_help from w_base_response_ext`cb_base_help within w_stops_order_po_resp
integer x = 658
integer y = 704
integer taborder = 20
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_stops_order_po_resp
boolean visible = false
integer x = 1275
integer y = 280
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_stops_order_po_resp
integer x = 306
integer y = 704
end type

event cb_base_ok::clicked;call super::clicked;Close(w_stops_order_po_resp)
end event

type dw_stops_order_po_resp from u_base_dw_ext within w_stops_order_po_resp
integer x = 32
integer y = 20
integer width = 1275
integer height = 664
integer taborder = 0
string dataobject = "d_stops_order_po_resp"
boolean vscrollbar = true
end type

