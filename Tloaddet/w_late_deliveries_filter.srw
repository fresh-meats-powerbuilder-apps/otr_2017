HA$PBExportHeader$w_late_deliveries_filter.srw
forward
global type w_late_deliveries_filter from w_netwise_response
end type
type dw_carrier from u_netwise_dw within w_late_deliveries_filter
end type
type dw_complex from u_netwise_dw within w_late_deliveries_filter
end type
type dw_sales_loc from u_netwise_dw within w_late_deliveries_filter
end type
type dw_shipto from u_netwise_dw within w_late_deliveries_filter
end type
type dw_tsr from u_netwise_dw within w_late_deliveries_filter
end type
type dw_svc from u_netwise_dw within w_late_deliveries_filter
end type
type dw_division from u_netwise_dw within w_late_deliveries_filter
end type
type dw_dates from u_netwise_dw within w_late_deliveries_filter
end type
type dw_trans_mode from u_netwise_dw within w_late_deliveries_filter
end type
type st_1 from statictext within w_late_deliveries_filter
end type
type gb_1 from groupbox within w_late_deliveries_filter
end type
type st_2 from statictext within w_late_deliveries_filter
end type
type st_3 from statictext within w_late_deliveries_filter
end type
type dw_billto from u_netwise_dw within w_late_deliveries_filter
end type
end forward

global type w_late_deliveries_filter from w_netwise_response
integer x = 430
integer y = 628
integer width = 2446
integer height = 1268
string title = "Late Deliveries Filter"
long backcolor = 79741120
dw_carrier dw_carrier
dw_complex dw_complex
dw_sales_loc dw_sales_loc
dw_shipto dw_shipto
dw_tsr dw_tsr
dw_svc dw_svc
dw_division dw_division
dw_dates dw_dates
dw_trans_mode dw_trans_mode
st_1 st_1
gb_1 gb_1
st_2 st_2
st_3 st_3
dw_billto dw_billto
end type
global w_late_deliveries_filter w_late_deliveries_filter

type variables
DataWindowChild	idwc_carrier, &
		idwc_complex, &
		idwc_sales_loc, &
		idwc_billto, &
		idwc_shipto, &
		idwc_tsr, &
		idwc_svc_region, &
		idwc_division, &
		idwc_trans_mode

integer	ii_rc

String	is_trans_mode, &
	is_carrier, &
	is_complex, &
	is_sales_loc, &
	is_svc_region, &
	is_tsr, &
	is_division, &
	is_billto_cust, &
	is_shipto_cust, &
	is_cancel

u_late_deliveries_filter	iu_late_deliveries_filter
u_validate_customer	iu_validate_customer
end variables

forward prototypes
public subroutine wf_billto_retrieve ()
public subroutine wf_shipto_retrieve ()
end prototypes

public subroutine wf_billto_retrieve ();//dw_billto.getchild("billto_customer", idwc_billto)
//idwc_billto.SetTransObject(SQLCA)
//idwc_billto.Retrieve()

//dw_billto.insertRow(0)
ii_rc = dw_billto.GetChild( 'billto_customer', idwc_billto )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for billto_customer.")
idwc_billto.Reset()
idwc_billto.SetTrans(SQLCA)
idwc_billto.Retrieve()
end subroutine

public subroutine wf_shipto_retrieve ();//dw_shipto.getchild("shipto_customer", idwc_shipto)
//idwc_shipto.SetTransObject(SQLCA)
//idwc_shipto.Retrieve()
//
//dw_shipto.insertRow(0)
ii_rc = dw_shipto.GetChild( 'shipto_customer', idwc_shipto )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for shipto_customer.")
idwc_shipto.Reset()
idwc_shipto.SetTrans(SQLCA)
idwc_shipto.Retrieve()
end subroutine

on w_late_deliveries_filter.create
int iCurrent
call super::create
this.dw_carrier=create dw_carrier
this.dw_complex=create dw_complex
this.dw_sales_loc=create dw_sales_loc
this.dw_shipto=create dw_shipto
this.dw_tsr=create dw_tsr
this.dw_svc=create dw_svc
this.dw_division=create dw_division
this.dw_dates=create dw_dates
this.dw_trans_mode=create dw_trans_mode
this.st_1=create st_1
this.gb_1=create gb_1
this.st_2=create st_2
this.st_3=create st_3
this.dw_billto=create dw_billto
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_carrier
this.Control[iCurrent+2]=this.dw_complex
this.Control[iCurrent+3]=this.dw_sales_loc
this.Control[iCurrent+4]=this.dw_shipto
this.Control[iCurrent+5]=this.dw_tsr
this.Control[iCurrent+6]=this.dw_svc
this.Control[iCurrent+7]=this.dw_division
this.Control[iCurrent+8]=this.dw_dates
this.Control[iCurrent+9]=this.dw_trans_mode
this.Control[iCurrent+10]=this.st_1
this.Control[iCurrent+11]=this.gb_1
this.Control[iCurrent+12]=this.st_2
this.Control[iCurrent+13]=this.st_3
this.Control[iCurrent+14]=this.dw_billto
end on

on w_late_deliveries_filter.destroy
call super::destroy
destroy(this.dw_carrier)
destroy(this.dw_complex)
destroy(this.dw_sales_loc)
destroy(this.dw_shipto)
destroy(this.dw_tsr)
destroy(this.dw_svc)
destroy(this.dw_division)
destroy(this.dw_dates)
destroy(this.dw_trans_mode)
destroy(this.st_1)
destroy(this.gb_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.dw_billto)
end on

event open;call super::open;iu_late_deliveries_filter = Message.PowerObjectParm
iu_validate_customer = CREATE u_validate_customer
date	ld_from_date, ld_to_date, current


current = today()
IF IsNull(iu_late_deliveries_filter.delv_from_date) THEN
	dw_dates.SetItem(1, "from_date", current)
ELSE
	IF iu_late_deliveries_filter.delv_from_date <> '' THEN 
		ld_from_date = Date(iu_late_deliveries_filter.delv_from_date)
		dw_dates.SetItem(1, "from_date", ld_from_date)
	ELSE
		dw_dates.SetItem(1, "from_date", current)
	END IF		
END IF


IF IsNull(iu_late_deliveries_filter.delv_to_date) THEN
	dw_dates.SetItem( 1, "to_date", RelativeDate(Today(),+1))
ELSE
	IF iu_late_deliveries_filter.delv_to_date <> '' THEN 
   	ld_to_date = Date(iu_late_deliveries_filter.delv_to_date)
		dw_dates.SetItem(1, "to_date", ld_to_date)
	ELSE
		dw_dates.SetItem( 1, "to_date", RelativeDate(Today(),+1))
	END IF
END IF










IF IsNull(iu_late_deliveries_filter.trans_mode) THEN
	dw_trans_mode.SetItem( 1, "trans_mode", "T")
ELSE
   IF iu_late_deliveries_filter.trans_mode <> '' THEN 
	   dw_trans_mode.SetItem( 1, "trans_mode", iu_late_deliveries_filter.trans_mode)
	ELSE
		dw_trans_mode.SetItem( 1, "trans_mode", "T")
   END IF
END IF

IF IsNull(iu_late_deliveries_filter.carrier) THEN
	dw_carrier.SetItem( 1, "carrier", "    ")
ELSE
   IF iu_late_deliveries_filter.carrier <> '    ' THEN 
	   dw_carrier.SetItem( 1, "carrier", iu_late_deliveries_filter.carrier)
		is_carrier = iu_late_deliveries_filter.carrier
   END IF
END IF

IF IsNull(iu_late_deliveries_filter.complex) THEN
	dw_complex.SetItem( 1, "complex", "   ")
ELSE
   IF iu_late_deliveries_filter.complex <> '   ' THEN 
	   dw_complex.SetItem( 1, "complex", iu_late_deliveries_filter.complex)
		is_complex = iu_late_deliveries_filter.complex
   END IF
END IF

IF IsNull(iu_late_deliveries_filter.sales_loc) THEN
	dw_sales_loc.SetItem( 1, "sales_loc", "   ")
ELSE
   IF iu_late_deliveries_filter.sales_loc <> '   ' THEN 
	   dw_sales_loc.SetItem( 1, "sales_loc", iu_late_deliveries_filter.sales_loc)
		is_sales_loc = iu_late_deliveries_filter.sales_loc
   END IF
END IF

IF IsNull(iu_late_deliveries_filter.svc_region) THEN
	dw_svc.SetItem( 1, "svc_region", "   ")
ELSE
   IF iu_late_deliveries_filter.svc_region <> '   ' THEN 
	   dw_svc.SetItem( 1, "svc_region", iu_late_deliveries_filter.svc_region)
		is_svc_region = iu_late_deliveries_filter.svc_region
   END IF
END IF

IF IsNull(iu_late_deliveries_filter.tsr) THEN
	dw_tsr.SetItem( 1, "tsr", "   ")
ELSE
   IF iu_late_deliveries_filter.tsr <> '   ' THEN 
	   dw_tsr.SetItem( 1, "tsr", iu_late_deliveries_filter.tsr)
		is_tsr = iu_late_deliveries_filter.tsr
   END IF
END IF

IF IsNull(iu_late_deliveries_filter.division) THEN
	dw_division.SetItem( 1, "division", "  ")
ELSE
   IF iu_late_deliveries_filter.division <> '  ' THEN 
	   dw_division.SetItem( 1, "division", iu_late_deliveries_filter.division)
		is_division = iu_late_deliveries_filter.division
   END IF
END IF

IF IsNull(iu_late_deliveries_filter.billto_cust) THEN
	dw_billto.SetItem( 1, "billto_customer", "       ")
ELSE
   IF iu_late_deliveries_filter.billto_cust <> '       ' THEN 
	   dw_billto.SetItem( 1, "billto_customer", iu_late_deliveries_filter.billto_cust)
		is_billto_cust = iu_late_deliveries_filter.billto_cust
   END IF
END IF

IF IsNull(iu_late_deliveries_filter.shipto_cust) THEN
	dw_shipto.SetItem( 1, "shipto_customer", "       ")
ELSE
   IF iu_late_deliveries_filter.shipto_cust <> '       ' THEN 
	   dw_shipto.SetItem( 1, "shipto_customer", iu_late_deliveries_filter.shipto_cust)
		is_shipto_cust = iu_late_deliveries_filter.shipto_cust
   END IF
END IF

dw_dates.SetFocus()
end event

event ue_base_cancel;iu_late_deliveries_filter.cancel = TRUE
CloseWithReturn( This, iu_late_deliveries_filter )

end event

event ue_base_ok;string		ls_from_date, &
				ls_to_date, &
				ls_trans_mode, &
				ls_carrier, &
				ls_complex, &
				ls_sales_loc, &
				ls_svc_region, &
				ls_tsr, &
				ls_division, &
				ls_billto_cust, &
				ls_shipto_cust	
				
Long			ll_between_days				

Date			ld_from_date, &
				ld_to_date, &
				ld_today

ld_today = today()

If dw_dates.AcceptText() = 1 then
   ld_from_date    = dw_dates.GetItemDate( 1, "from_date" )
	ld_to_date      = dw_dates.GetItemDate( 1, "to_date" )
	ls_from_date    = String(ld_from_date, "yyyy-mm-dd")
	ls_to_date      = String(ld_to_date, "yyyy-mm-dd")
Else
	MessageBox("Date Fields Required", "Please select a date range")
	Return
End If

If ld_from_date < ld_today Then
	ll_between_days = DaysAfter ( ld_from_date, ld_today )
	If ll_between_days > 30 Then
	   MessageBox("From Date Error" , "To Inquire Lates More Than 30 Days Back, Please Use CSR System")
		Return
	End If
End If




If dw_trans_mode.AcceptText() = 1 then
	ls_trans_mode = ( dw_trans_mode.GetItemString( 1, "trans_mode" ))
Else
	MessageBox("Transportation Mode field Required", "Please select a transportation mode")
	Return
End If




//If dw_carrier.AcceptText() = 1 then
//	ls_carrier = dw_carrier.GetItemString( 1, "carrier" )
//Else
//	ls_carrier = "    "
//End If
//If IsNull(ls_carrier) Then
//	ls_carrier = "    "
//End If
//
//If dw_complex.AcceptText() = 1 then
//	ls_complex = dw_complex.GetItemString( 1, "complex" )
//Else
//	ls_complex = "   "
//End If
//If IsNull(ls_complex) Then
//	ls_complex = "   "
//End If
//
//If dw_sales_loc.AcceptText() = 1 then
//	ls_sales_loc = dw_sales_loc.GetItemString( 1, "sales_loc" )
//Else
//	ls_sales_loc = "   "
//End If
//If IsNull(ls_sales_loc) Then
//	ls_sales_loc = "   "
//End If
//
//If dw_svc.AcceptText() = 1 then
//	ls_svc_region = dw_svc.GetItemString( 1, "svc_region" )
//Else
//	ls_svc_region = "   "
//End If
//If IsNull(ls_svc_region) Then
//	ls_svc_region = "   "
//End If
//
//If dw_tsr.AcceptText() = 1 then
//	ls_tsr = dw_tsr.GetItemString( 1, "tsr" )
//Else
//	ls_tsr = "   "
//End If
//If IsNull(ls_tsr) Then
//	ls_tsr = "   "
//End If
//
//If dw_division.AcceptText() = 1 then
//	ls_division = dw_division.GetItemString( 1, "division" )
//Else
//	ls_division = "  "
//End If
//If IsNull(ls_division) Then
//	ls_division = "  "
//End If
//
//If dw_billto.AcceptText() = 1 then
//	ls_billto_cust = dw_billto.GetItemString( 1, "billto_customer" )
//Else
//	ls_billto_cust = "       "
//End If
//If IsNull(ls_billto_cust) Then
//	ls_billto_cust = "       "
//End If
//
//If dw_shipto.AcceptText() = 1 then
//	ls_shipto_cust = dw_shipto.GetItemString( 1, "shipto_customer" )
//Else
//	ls_shipto_cust = "       "
//End If
//If IsNull(ls_shipto_cust) Then
//	ls_shipto_cust = "       "
//End If

dw_carrier.AcceptText()
dw_complex.AcceptText()
dw_sales_loc.AcceptText()
dw_svc.AcceptText()
dw_tsr.AcceptText()
dw_division.AcceptText()
dw_billto.AcceptText()
dw_shipto.AcceptText()

If is_carrier > "    " then
	ls_carrier = is_carrier
Else
	ls_carrier = "    "
End If
If IsNull(ls_carrier) Then
	ls_carrier = "    "
End If

If is_complex > "   " then
	ls_complex = is_complex
Else
	ls_complex = "   "
End If
If IsNull(ls_complex) Then
	ls_complex = "   "
End If

If is_sales_loc > "   " then
	ls_sales_loc = is_sales_loc
Else
	ls_sales_loc = "   "
End If
If IsNull(ls_sales_loc) Then
	ls_sales_loc = "   "
End If

If is_svc_region > "   " then
	ls_svc_region = is_svc_region
Else
	ls_svc_region = "   "
End If
If IsNull(ls_svc_region) Then
	ls_svc_region = "   "
End If

If is_tsr > "   " then
	ls_tsr = is_tsr
Else
	ls_tsr = "   "
End If
If IsNull(ls_tsr) Then
	ls_tsr = "   "
End If

If is_division > "  " then
	ls_division = is_division
Else
	ls_division = "  "
End If
If IsNull(ls_division) Then
	ls_division = "  "
End If

If is_billto_cust > "       " then
	ls_billto_cust = is_billto_cust
Else
	ls_billto_cust = "       "
End If
If IsNull(ls_billto_cust) Then
	ls_billto_cust = "       "
End If

If is_shipto_cust > "       " then
	ls_shipto_cust = is_shipto_cust
Else
	ls_shipto_cust = "       "
End If
If IsNull(ls_shipto_cust) Then
	ls_shipto_cust = "       "
End If


iu_late_deliveries_filter.delv_from_date	= ls_from_date
iu_late_deliveries_filter.delv_to_date		= ls_to_date
iu_late_deliveries_filter.trans_mode		= ls_trans_mode
iu_late_deliveries_filter.carrier			= ls_carrier
iu_late_deliveries_filter.complex			= ls_complex
iu_late_deliveries_filter.sales_loc			= ls_sales_loc
iu_late_deliveries_filter.svc_region		= ls_svc_region
iu_late_deliveries_filter.tsr					= ls_tsr
iu_late_deliveries_filter.division			= ls_division
iu_late_deliveries_filter.billto_cust		= ls_billto_cust
iu_late_deliveries_filter.shipto_cust		= ls_shipto_cust
iu_late_deliveries_filter.cancel = False

CloseWithReturn( This, iu_late_deliveries_filter )
	Return

end event

type cb_base_help from w_netwise_response`cb_base_help within w_late_deliveries_filter
integer x = 1344
integer y = 1052
integer taborder = 130
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_late_deliveries_filter
integer x = 1065
integer y = 1052
integer taborder = 120
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_late_deliveries_filter
integer x = 786
integer y = 1052
integer taborder = 110
end type

type dw_carrier from u_netwise_dw within w_late_deliveries_filter
integer x = 9
integer y = 420
integer width = 306
integer height = 184
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_dddw_carrier"
end type

event constructor;call super::constructor;
insertRow(0)
ii_rc = GetChild( 'carrier', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for carrier.")
idwc_carrier.Reset()
idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()


end event

event itemchanged;call super::itemchanged;long ll_FoundRow
iw_frame.SetMicroHelp(dwo.name)
IF Trim(data) = "" THEN
	is_carrier = space(4)
	Return
END IF

ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Carrier Error", data + " is Not a Valid Carrier Code." )
	SelectText(1,4)
	Return 1
ELSE
	is_carrier = data
END IF





//

//string ls_carrier, ls_option
//long ll_nbr, ll_foundrow
//integer rtn
//boolean lb_rtn
//
//ls_carrier = data
//
//
//
//ll_nbr = idwc_carrier.RowCount()
//
//ls_option = ("carrier_code = '"+ data +"'")
//
//ll_foundrow = idwc_carrier.Find("carrier_code = '"+ data +"'", 1, ll_nbr)
//   if ll_foundrow = 0 then
//		messagebox("Message",data + " is not a valid Carrier code")
//		this.setitem(1,"carrier","")
//		return 1
//	end if
//	return
////
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)



end event

event itemerror;call super::itemerror;Return 1
end event

event getfocus;THIS.SelectText (1,99)
end event

type dw_complex from u_netwise_dw within w_late_deliveries_filter
integer x = 357
integer y = 420
integer width = 306
integer height = 184
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_dddw_complex"
end type

event constructor;call super::constructor;
insertRow(0)
ii_rc = GetChild( 'complex', idwc_complex )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for complex.")
idwc_complex.Reset()
idwc_complex.SetTrans(SQLCA)
idwc_complex.Retrieve()

end event

event itemerror;call super::itemerror;Return 1
end event

event itemchanged;call super::itemchanged;long ll_FoundRow
iw_frame.SetMicroHelp(dwo.name)
IF Trim(data) = "" THEN
	is_complex = space(3)
	Return
END IF

ll_FoundRow = idwc_complex.Find ( "complex_code='"+data+"'", 1, idwc_complex.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Complex Error", data + " is Not a Valid Complex Code." )
	SelectText(1,3)
	Return 1
ELSE
	is_complex = data
END IF		

//
//datawindowchild dwc_customer_id,dwc_tsr_code,dwc_sales_location,dwc_sales_division
//string ls_cust_type,ls_value = "",ls_value1 = "",ls_value2 = "",ls_sales_division
//long ll_nbr, ll_foundrow
//date ld_from_date,ld_to_date
//integer rtn
//boolean lb_rtn
//
//ls_value = dw_1.getitemstring(1,"customer_id")
//ls_value1 = dw_1.getitemstring(1,"tsr_code")
//ls_value2 = dw_1.getitemstring(1,"sales_location")
//ls_sales_division = dw_1.getitemstring(1,"sales_division")
//
//if rb_corporate.checked = true  then
//	ls_cust_type = "C"
//end if
//
//if rb_shipto.checked = true then
//	ls_cust_type = "S"
//end if
//
//if rb_billto.checked = true then
//	ls_cust_type = "B"
//end if
//
////dw_1.getchild("customer_id",dwc_customer_id)
//dw_1.getchild("tsr_code",dwc_tsr_code)
//dw_1.getchild("sales_location",dwc_sales_location)
//dw_1.getchild("sales_division",dwc_sales_division)
//
//if is_columnname = "customer_id" then
//	ls_value = dw_1.gettext()
//	if ls_value1 <> "" or ls_value2 <> "" then
//		Messagebox("Message ITEM CHANGED","Only one of Customer or Sales Location or TSR can be selected")
//		this.setitem(1,"customer_id","")
//		return 1
//	end if
//	if ls_value <> "" then
//		lb_rtn = iu_validate_customer.nf_validate_customer(ls_cust_type,ls_value)
//		if lb_rtn = false then
//			messagebox("Message",ls_value + " is not a valid customer ")
//			this.settext("")
//			return 1
//		end if
//	end if
////	ll_nbr = dwc_customer_id.RowCount()	
////	ls_value = this.gettext()
////	if ls_value = "" then
////		return 
////	end if
////	choose case ls_cust_type
////		case "S"
////			ll_foundrow = dwc_customer_id.Find( &
////			"customer_id = '"+ ls_value +"'", 1, ll_nbr)
////		case "B"
////			ll_foundrow = dwc_customer_id.Find( &
////			"bill_to_id = '"+ ls_value +"'", 1, ll_nbr)
////		case "C"
////			ll_foundrow = dwc_customer_id.Find( &
////			"corp_id = '"+ ls_value +"'", 1, ll_nbr)
////	end choose
////   if ll_foundrow = 0 then
////		messagebox("Message", ls_value + " is not a valid Customer" )
////		this.setitem(1,"customer_id","")
////		return 1
////	end if
////	return
//end if
//
//if is_columnname = "tsr_code" then
//	if ((ls_value <> "" or ls_value2 <> "")) then
//		Messagebox("Message","Only one of Customer or Sales Location or TSR can be selected")
//		this.setitem(1,"tsr_code","")
//		return 1
//	end if
//	ll_nbr = dwc_tsr_code.RowCount()
//	ls_value1 = this.gettext()
//	if ls_value1 = "" then
//		return 
//	end if
//	ll_foundrow = dwc_tsr_code.Find( &
//	"smancode = '"+ ls_value1 +"'", 1, ll_nbr)
//   if ll_foundrow = 0 then
//		messagebox("Message",ls_value1 + " is not a valid TSR/FSC code")
//		this.setitem(1,"tsr_code","")
//		return 1
//	end if
//	return
//end if
//
//if is_columnname = "sales_location" then
//	if ls_value <> "" or ls_value1 <> "" then
//		Messagebox("Message","Only one of Customer or Sales Location or TSR can be selected")
//		this.setitem(1,"sales_location","")
//		return 1
//	end if
//	dw_1.getchild("sales_location",dwc_sales_location)
//	ll_nbr = dwc_sales_location.RowCount()
//	ls_value2 = this.gettext()
//	if ls_value2 = "" then
//		return 
//	end if
//	ll_foundrow = dwc_sales_location.Find( &
//	"service_center_code = '"+ ls_value2 +"'", 1, ll_nbr)
//   if ll_foundrow = 0 then
//		messagebox("Message",ls_value2 + " is not a valid Sales location")
//		this.setitem(1,"sales_location","")
//		return 1
//	end if
//	return
//end if
//
//if is_columnname = "sales_division" then
//	ll_nbr = dwc_sales_division.RowCount()	
//	ls_sales_division = this.gettext()
//	if ls_sales_division = "" then
//		return 
//	end if
//	ll_foundrow = dwc_sales_division.Find( &
//	"type_code = '"+ ls_sales_division +"'", 1, ll_nbr)
//	   
//	if ll_foundrow = 0 then
//		messagebox("Message", ls_sales_division + " is not a valid sales division" )
//		this.setitem(1,"sales_division","")
//		return 1
//	end if
//	return
//end if
//
//if is_columnname = "to_date" then
//ld_to_date = date(this.gettext())
//ld_from_date = dw_1.getitemdate(1,"from_date")
//	if (daysafter(ld_to_date,today()) <= 1) then
//		messagebox("Message"," To Date should be less than today")
//		dw_1.setitem(1,"to_date",relativedate(today(),-1))
//		return 1
//	end if
//	if (daysafter(ld_from_date, ld_to_date) < 0) then
//		Messagebox("Message","From date cannot be greater than to date")
//		dw_1.setitem(1,"to_date",relativedate(today(),-1))
//		return 1
//	end if
//end if
//
//if is_columnname = "from_date" then
//	ld_to_date = dw_1.getitemdate(1,"to_date")
//   ld_from_date = date(this.gettext())
//	rtn = daysafter(1997-12-29,ld_from_date)
//	if (rtn < 0) then
//		messagebox("Message" ," There is no inventory of data before '12/29/1997'")
//		dw_1.setitem(1,"from_date",relativedate(today(),-8))
//		return 1
//	end if
//	if (daysafter(ld_from_date, ld_to_date) < 0) then
//		Messagebox("Message","From date cannot be greater than to date")
//		dw_1.setitem(1,"from_date",relativedate(today(),-8))
//		return 1
//	end if
//end if
//
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)



end event

event getfocus;THIS.SelectText (1,99)
end event

type dw_sales_loc from u_netwise_dw within w_late_deliveries_filter
integer x = 1225
integer y = 420
integer width = 535
integer height = 184
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_dddw_sales_loc"
end type

event constructor;call super::constructor;insertRow(0)
ii_rc = GetChild( 'sales_loc', idwc_sales_loc )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for sales_loc.")
idwc_sales_loc.Reset()
idwc_sales_loc.SetTrans(SQLCA)
idwc_sales_loc.Retrieve()

end event

event itemchanged;call super::itemchanged;long ll_FoundRow
iw_frame.SetMicroHelp(dwo.name)
IF Trim(data) = "" THEN
	is_sales_loc = space(3)
	Return
END IF

ll_FoundRow = idwc_sales_loc.Find ( "service_center_code='"+data+"'", 1, idwc_sales_loc.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Sales Location Error", data + " is Not a Valid Sales Location." )
	SelectText(1,3)
	Return 1
ELSE
	is_sales_loc = data
END IF		

//
//datawindowchild dwc_customer_id,dwc_tsr_code,dwc_sales_location,dwc_sales_division
//string ls_cust_type,ls_value = "",ls_value1 = "",ls_value2 = "",ls_sales_division
//long ll_nbr, ll_foundrow
//date ld_from_date,ld_to_date
//integer rtn
//boolean lb_rtn
//
//ls_value = dw_1.getitemstring(1,"customer_id")
//ls_value1 = dw_1.getitemstring(1,"tsr_code")
//ls_value2 = dw_1.getitemstring(1,"sales_location")
//ls_sales_division = dw_1.getitemstring(1,"sales_division")
//
//if rb_corporate.checked = true  then
//	ls_cust_type = "C"
//end if
//
//if rb_shipto.checked = true then
//	ls_cust_type = "S"
//end if
//
//if rb_billto.checked = true then
//	ls_cust_type = "B"
//end if
//
////dw_1.getchild("customer_id",dwc_customer_id)
//dw_1.getchild("tsr_code",dwc_tsr_code)
//dw_1.getchild("sales_location",dwc_sales_location)
//dw_1.getchild("sales_division",dwc_sales_division)
//
//if is_columnname = "customer_id" then
//	ls_value = dw_1.gettext()
//	if ls_value1 <> "" or ls_value2 <> "" then
//		Messagebox("Message ITEM CHANGED","Only one of Customer or Sales Location or TSR can be selected")
//		this.setitem(1,"customer_id","")
//		return 1
//	end if
//	if ls_value <> "" then
//		lb_rtn = iu_validate_customer.nf_validate_customer(ls_cust_type,ls_value)
//		if lb_rtn = false then
//			messagebox("Message",ls_value + " is not a valid customer ")
//			this.settext("")
//			return 1
//		end if
//	end if
////	ll_nbr = dwc_customer_id.RowCount()	
////	ls_value = this.gettext()
////	if ls_value = "" then
////		return 
////	end if
////	choose case ls_cust_type
////		case "S"
////			ll_foundrow = dwc_customer_id.Find( &
////			"customer_id = '"+ ls_value +"'", 1, ll_nbr)
////		case "B"
////			ll_foundrow = dwc_customer_id.Find( &
////			"bill_to_id = '"+ ls_value +"'", 1, ll_nbr)
////		case "C"
////			ll_foundrow = dwc_customer_id.Find( &
////			"corp_id = '"+ ls_value +"'", 1, ll_nbr)
////	end choose
////   if ll_foundrow = 0 then
////		messagebox("Message", ls_value + " is not a valid Customer" )
////		this.setitem(1,"customer_id","")
////		return 1
////	end if
////	return
//end if
//
//if is_columnname = "tsr_code" then
//	if ((ls_value <> "" or ls_value2 <> "")) then
//		Messagebox("Message","Only one of Customer or Sales Location or TSR can be selected")
//		this.setitem(1,"tsr_code","")
//		return 1
//	end if
//	ll_nbr = dwc_tsr_code.RowCount()
//	ls_value1 = this.gettext()
//	if ls_value1 = "" then
//		return 
//	end if
//	ll_foundrow = dwc_tsr_code.Find( &
//	"smancode = '"+ ls_value1 +"'", 1, ll_nbr)
//   if ll_foundrow = 0 then
//		messagebox("Message",ls_value1 + " is not a valid TSR/FSC code")
//		this.setitem(1,"tsr_code","")
//		return 1
//	end if
//	return
//end if
//
//if is_columnname = "sales_location" then
//	if ls_value <> "" or ls_value1 <> "" then
//		Messagebox("Message","Only one of Customer or Sales Location or TSR can be selected")
//		this.setitem(1,"sales_location","")
//		return 1
//	end if
//	dw_1.getchild("sales_location",dwc_sales_location)
//	ll_nbr = dwc_sales_location.RowCount()
//	ls_value2 = this.gettext()
//	if ls_value2 = "" then
//		return 
//	end if
//	ll_foundrow = dwc_sales_location.Find( &
//	"service_center_code = '"+ ls_value2 +"'", 1, ll_nbr)
//   if ll_foundrow = 0 then
//		messagebox("Message",ls_value2 + " is not a valid Sales location")
//		this.setitem(1,"sales_location","")
//		return 1
//	end if
//	return
//end if
//
//if is_columnname = "sales_division" then
//	ll_nbr = dwc_sales_division.RowCount()	
//	ls_sales_division = this.gettext()
//	if ls_sales_division = "" then
//		return 
//	end if
//	ll_foundrow = dwc_sales_division.Find( &
//	"type_code = '"+ ls_sales_division +"'", 1, ll_nbr)
//	   
//	if ll_foundrow = 0 then
//		messagebox("Message", ls_sales_division + " is not a valid sales division" )
//		this.setitem(1,"sales_division","")
//		return 1
//	end if
//	return
//end if
//
//if is_columnname = "to_date" then
//ld_to_date = date(this.gettext())
//ld_from_date = dw_1.getitemdate(1,"from_date")
//	if (daysafter(ld_to_date,today()) <= 1) then
//		messagebox("Message"," To Date should be less than today")
//		dw_1.setitem(1,"to_date",relativedate(today(),-1))
//		return 1
//	end if
//	if (daysafter(ld_from_date, ld_to_date) < 0) then
//		Messagebox("Message","From date cannot be greater than to date")
//		dw_1.setitem(1,"to_date",relativedate(today(),-1))
//		return 1
//	end if
//end if
//
//if is_columnname = "from_date" then
//	ld_to_date = dw_1.getitemdate(1,"to_date")
//   ld_from_date = date(this.gettext())
//	rtn = daysafter(1997-12-29,ld_from_date)
//	if (rtn < 0) then
//		messagebox("Message" ," There is no inventory of data before '12/29/1997'")
//		dw_1.setitem(1,"from_date",relativedate(today(),-8))
//		return 1
//	end if
//	if (daysafter(ld_from_date, ld_to_date) < 0) then
//		Messagebox("Message","From date cannot be greater than to date")
//		dw_1.setitem(1,"from_date",relativedate(today(),-8))
//		return 1
//	end if
//end if
//
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)


end event

event itemerror;call super::itemerror;Return 1
end event

event getfocus;THIS.SelectText (1,99)
end event

type dw_shipto from u_netwise_dw within w_late_deliveries_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 1865
integer y = 836
integer width = 526
integer height = 184
integer taborder = 100
boolean bringtotop = true
string dataobject = "d_dddw_shipto_cust"
end type

event ue_dwndropdown;wf_shipto_retrieve()
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)
end event

event itemerror;call super::itemerror;Return 1
end event

event constructor;call super::constructor;
insertRow(0)
//ii_rc = GetChild( 'shipto_customer', idwc_shipto )
//IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for shipto_customer.")
//
//idwc_shipto.SetTransObject(SQLCA)
//idwc_shipto.Retrieve()
end event

event itemchanged;call super::itemchanged;string 	ls_customer_type, &
			ls_customer_id
			
long 		ll_nbr, &
			ll_foundrow
			
integer 	rtn
boolean 	lb_rtn

ls_customer_type	= 'S'
ls_customer_id		= data


//dw_1.getchild("customer_id",dwc_customer_id)
	
if ls_customer_id <> "" then
	lb_rtn = iu_validate_customer.nf_validate_customer(ls_customer_type,ls_customer_id)
	if lb_rtn = false then
		messagebox("Message",ls_customer_id + " is not a valid customer ")
		this.settext("")
		return 1
	else
		is_shipto_cust = ls_customer_id
	end if
else
	is_shipto_cust = ls_customer_id
end if


//Return 1
end event

event getfocus;THIS.SelectText (1,99)
end event

type dw_tsr from u_netwise_dw within w_late_deliveries_filter
integer x = 1225
integer y = 628
integer width = 622
integer height = 184
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_dddw_tsr"
end type

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)


end event

event itemerror;call super::itemerror;Return 1
end event

event constructor;call super::constructor;
insertRow(0)
ii_rc = GetChild( 'tsr', idwc_tsr )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for tsr.")
idwc_tsr.Reset()
idwc_tsr.SetTrans(SQLCA)
idwc_tsr.Retrieve()
end event

event itemchanged;call super::itemchanged;long ll_FoundRow
iw_frame.SetMicroHelp(dwo.name)
IF Trim(data) = "" THEN
	is_tsr = data
	Return
END IF

ll_FoundRow = idwc_tsr.Find ( "smancode='"+data+"'", 1, idwc_tsr.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "TSR Error", data + " is Not a Valid TSR Code." )
	SelectText(1,3)
	Return 1
ELSE
	is_tsr = data
END IF		

//



//if is_columnname = "tsr_code" then
//	if ((ls_value <> "" or ls_value2 <> "")) then
//		Messagebox("Message","Only one of Customer or Sales Location or TSR can be selected")
//		this.setitem(1,"tsr_code","")
//		return 1
//	end if
//	ll_nbr = dwc_tsr_code.RowCount()
//	ls_value1 = this.gettext()
//	if ls_value1 = "" then
//		return 
//	end if
//	ll_foundrow = dwc_tsr_code.Find( &
//	"smancode = '"+ ls_value1 +"'", 1, ll_nbr)
//   if ll_foundrow = 0 then
//		messagebox("Message",ls_value1 + " is not a valid TSR/FSC code")
//		this.setitem(1,"tsr_code","")
//		return 1
//	end if
//	return
//end if

end event

event getfocus;THIS.SelectText (1,99)
end event

type dw_svc from u_netwise_dw within w_late_deliveries_filter
integer x = 1865
integer y = 420
integer width = 549
integer height = 184
integer taborder = 60
boolean bringtotop = true
string dataobject = "d_dddw_svc_region"
end type

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)


end event

event itemerror;call super::itemerror;Return 1
end event

event constructor;call super::constructor;
insertRow(0)
ii_rc = GetChild( 'svc_region', idwc_svc_region )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for svc_region.")
idwc_svc_region.Reset()
idwc_svc_region.SetTrans(SQLCA)
idwc_svc_region.Retrieve('SCREGION')

end event

event itemchanged;call super::itemchanged;long ll_FoundRow
iw_frame.SetMicroHelp(dwo.name)
IF Trim(data) = "" THEN
	is_svc_region = space(3)
	Return
END IF

ll_FoundRow = idwc_svc_region.Find ( "type_code='"+data+"'", 1, idwc_division.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "SVC Region Error", data + " is Not a Valid SVC Region Code." )
	SelectText(1,3)
	Return 1
ELSE
	is_svc_region = data
END IF		


end event

event getfocus;THIS.SelectText (1,99)
end event

type dw_division from u_netwise_dw within w_late_deliveries_filter
integer x = 1865
integer y = 628
integer width = 517
integer height = 184
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_dddw_div"
end type

event constructor;call super::constructor;insertRow(0)
ii_rc = GetChild( 'division', idwc_division )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for division.")
idwc_division.Reset()
idwc_division.SetTrans(SQLCA)
idwc_division.Retrieve('DIVCODE')
end event

event itemchanged;call super::itemchanged;long ll_FoundRow
iw_frame.SetMicroHelp(dwo.name)
IF Trim(data) = "" THEN
	is_division = data
	Return
END IF

ll_FoundRow = idwc_division.Find ( "type_code='"+data+"'", 1, idwc_division.RowCount() )
IF ll_FoundRow < 1 THEN
	MessageBox( "Division Error", data + " is Not a Valid Division Code." )
	SelectText(1,2)
	Return 1
ELSE
	is_division = data
END IF		

end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)


end event

event itemerror;call super::itemerror;Return 1
end event

event getfocus;THIS.SelectText (1,99)
end event

type dw_dates from u_netwise_dw within w_late_deliveries_filter
integer x = 562
integer y = 84
integer width = 718
integer height = 184
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_dates"
end type

event constructor;call super::constructor;InsertRow(0)

date current
current = today()
SetItem( 1, "from_date", current )
dw_dates.SetItem(1, "from_date", current )

SetItem( 1, "to_date", current )
dw_dates.SetItem(1, "to_date", RelativeDate(Today(),+1))

end event

event itemchanged;call super::itemchanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)

Date		ld_from, &
			ld_to


If dwo.name = "to_date" Then
	ld_from = dw_dates.GetItemDate(1,"from_date")
	ld_to = Date(data)
	If ld_from > ld_to Then
		MessageBox("Date error", "To Date can not be less than the From Date, please reenter dates")
		SelectText(1,10)
		Return 1
	End If
End If
end event

event itemerror;call super::itemerror;Return 1
end event

event getfocus;THIS.SelectText (1,99)
end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText(1,99)
end event

type dw_trans_mode from u_netwise_dw within w_late_deliveries_filter
integer x = 1312
integer y = 84
integer width = 571
integer height = 184
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_trans_mode"
end type

event constructor;call super::constructor;insertRow(0)
end event

event getfocus;THIS.SelectText (1,99)
end event

event itemerror;call super::itemerror;Return 1
end event

type st_1 from statictext within w_late_deliveries_filter
integer x = 608
integer y = 28
integer width = 613
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Delivery Date Range"
alignment alignment = center!
boolean focusrectangle = false
end type

type gb_1 from groupbox within w_late_deliveries_filter
integer x = 485
integer y = 4
integer width = 1495
integer height = 304
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

type st_2 from statictext within w_late_deliveries_filter
integer x = 9
integer y = 320
integer width = 1189
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Optional Traffic Filter parameters"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type st_3 from statictext within w_late_deliveries_filter
integer x = 1225
integer y = 320
integer width = 1189
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Optional Sales Filter parameters"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type dw_billto from u_netwise_dw within w_late_deliveries_filter
event ue_dwndropdown pbm_dwndropdown
integer x = 1225
integer y = 836
integer width = 526
integer height = 184
integer taborder = 90
boolean bringtotop = true
string dataobject = "d_dddw_billto_cust"
end type

event ue_dwndropdown;wf_billto_retrieve()
end event

event constructor;call super::constructor;insertRow(0)
end event

event itemchanged;call super::itemchanged;string 	ls_customer_type, &
			ls_customer_id
			
long 		ll_nbr, &
			ll_foundrow
			
integer 	rtn
boolean 	lb_rtn

ls_customer_type	= 'B'
ls_customer_id		= data


//dw_1.getchild("customer_id",dwc_customer_id)
	
if ls_customer_id <> "" then
	lb_rtn = iu_validate_customer.nf_validate_customer(ls_customer_type,ls_customer_id)
	if lb_rtn = false then
		messagebox("Message",ls_customer_id + " is not a valid customer ")
		this.settext("")
		return 1
	else
		is_billto_cust = ls_customer_id
	end if
else
	is_billto_cust = ls_customer_id
end if

//this.settext(data)
//Return
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)
end event

event getfocus;THIS.SelectText (1,99)
end event

