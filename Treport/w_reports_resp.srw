HA$PBExportHeader$w_reports_resp.srw
forward
global type w_reports_resp from w_base_response_ext
end type
type dw_cust_late_fax from u_base_dw_ext within w_reports_resp
end type
end forward

global type w_reports_resp from w_base_response_ext
integer width = 1499
integer height = 1104
long backcolor = 79741120
dw_cust_late_fax dw_cust_late_fax
end type
global w_reports_resp w_reports_resp

type variables
DataWindowChild idwc_customer

integer ii_rc

u_otr005 iu_otr005
end variables

on w_reports_resp.create
int iCurrent
call super::create
this.dw_cust_late_fax=create dw_cust_late_fax
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_cust_late_fax
end on

on w_reports_resp.destroy
call super::destroy
destroy(this.dw_cust_late_fax)
end on

event open;call super::open;long    ll_newrow
string  ls_report_requested

iu_otr005  =  CREATE u_otr005

ls_report_requested = Message.StringParm

// Depending on the argument passed from the menu,
// will be used to determine which was the chosen menu item, 
// will then determine the window and the title of the window to be instanciated
//
//
If ls_report_requested = "OTR155XL" then
	dw_cust_late_fax.visible = True
	This.Title = "Initiate Customer Late Fax Report(OTR155XL)"
else
	dw_cust_late_fax.visible = False
end if





ll_newrow = dw_cust_late_fax.InsertRow(0) 
end event

event ue_base_cancel;call super::ue_base_cancel;close(this)


end event

event ue_base_ok;call super::ue_base_ok;string    ls_load_key, &
          ls_stop_code, &
			 ls_report_id, &
			 ls_output_info, &
			 ls_rpt_initiation_string
			 
integer   li_rtn, &
          li_stop_num

s_error   lstr_Error_Info

//ls_rpt_initiation_string = space(2000)

dw_cust_late_fax.AcceptText()

ls_load_key  = dw_cust_late_fax.GetItemString( 1, "load_key" )
ls_stop_code = dw_cust_late_fax.GetItemString(1, "stop_code")
ls_report_id = dw_cust_late_fax.GetItemString(1, "report_id")


// Empty String or NULL is not allowed for any  column.	
If IsNull( ls_load_key ) Then
	MessageBox( "Data entry error1", "Load Key is required." )
	dw_cust_late_fax.SetFocus()
	Return
end if
If IsNull( ls_stop_code ) Then
	MessageBox( "Data entry error2", "Stop Code is required." )
	dw_cust_late_fax.SetFocus()
	Return
end if

IF len(trim(ls_load_key)) < 1 THEN 
	MessageBox("Load Key", "Load Key is a required field, please enter a value.")
	dw_cust_late_fax.object.load_key.SetFocus()
	dw_cust_late_fax.object.load_key.SelectText()
	Return 
END IF



li_stop_num = Integer( ls_stop_code )
If ( li_stop_num > 99 ) OR ( li_stop_num < 1 ) Then
	MessageBox( "Data entry error", &
	"Stop Code must be between 01 and 99.")
	Return
End If

If Len( ls_stop_code ) = 1 then
	ls_stop_code = '0' + ls_stop_code
end if 

lstr_error_info.se_event_name = "ue_base_ok for w_reports_resp"
lstr_error_info.se_window_name = "w_reports_resp"
lstr_error_info.se_procedure_name = "wf_load_info_strings"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

ls_rpt_initiation_string = ls_report_id + '~t' + ls_load_key + '~t' + ls_stop_code + '~r~n'


//li_rtn = iu_otr005.nf_otrt62ar(ls_rpt_initiation_string, lstr_error_info, 0)

iw_frame.SetMicroHelp( "Ready")
// Check the return code from the above function call
IF li_rtn < 0 THEN
	iw_frame.SetMicroHelp( "Invalid Report Initiation" )
   dw_cust_late_fax.InsertRow(0)
	return
Else
	iw_frame.SetMicroHelp(lstr_error_info.se_message)
END IF

end event

event close;call super::close;DESTROY iu_otr005 
end event

event activate;iw_frame.im_menu.mf_disable("m_delete")
iw_frame.im_menu.mf_disable("m_save")
iw_frame.im_menu.mf_disable("m_inquire")
end event

event deactivate;iw_frame.im_menu.mf_enable("m_delete")
iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_inquire")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_reports_resp
integer x = 1147
integer y = 836
integer taborder = 20
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_reports_resp
integer x = 443
integer y = 836
integer taborder = 40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_reports_resp
integer x = 96
integer y = 836
integer taborder = 30
end type

type dw_cust_late_fax from u_base_dw_ext within w_reports_resp
integer x = 41
integer y = 28
integer width = 1074
integer height = 728
integer taborder = 10
string dataobject = "d_cust_late_fax"
boolean border = false
end type

event itemchanged;call super::itemchanged;integer li_column_data
string  ls_stop_code
If ( Len( Trim( Data ) ) = 0 ) OR ( IsNull( Data ) ) Then
	
	// Empty String or NULL is not allowed for any  column.
	Choose Case dwo.Name
		Case "load_key"
			MessageBox( "Data entry error", "Load Key is required." )
			dw_cust_late_fax.SetFocus()
		Case "stop_code"
			MessageBox( "Data entry error", "Stop Code is required." )
			dw_cust_late_fax.Setfocus()
	End Choose

	Return
End If

//ls_stop_code = dw_cust_late_fax.Gettext(1, "stop_code" )
 
Choose Case dwo.Name
    Case "stop_code"
		ls_stop_code = dw_cust_late_fax.Gettext()
		li_column_data = Integer( ls_stop_code )
		If ( li_column_data > 99 ) OR ( li_column_data < 1 ) Then
			MessageBox( "Data entry error", &
			"Stop Code must be between 01 and 99.")
			Return
		End If
End Choose

end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText( 1,99 )
end event

