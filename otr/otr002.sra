HA$PBExportHeader$otr002.sra
forward
global type otr002 from application
end type
global u_base_transaction_ext sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global u_base_message_ext message
end forward

global variables
// A pointer the MDI Frame window
w_base_frame_ext	iw_frame
w_base_frame         gw_base_frame
w_netwise_frame     gw_netwise_frame
end variables

global type otr002 from application
string appname = "otr002"
end type
global otr002 otr002

event open;Message.nf_set_app_id('OTR')

// Call a function on the Message Object that will open the frame
//Message.nf_OpenFrameWindow("w_base_frame_ext", "ibp002.ini",'')
OpenWithParm(iw_frame, "ibp002.ini", "w_base_frame_ext")
end event

on systemerror;Open(w_system_error)
end on

on otr002.create
appname="otr002"
message=create u_base_message_ext
sqlca=create u_base_transaction_ext
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on otr002.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

