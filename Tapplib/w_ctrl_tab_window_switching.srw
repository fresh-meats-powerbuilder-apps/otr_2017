HA$PBExportHeader$w_ctrl_tab_window_switching.srw
forward
global type w_ctrl_tab_window_switching from Window
end type
type sle_1 from statictext within w_ctrl_tab_window_switching
end type
type p_1 from picture within w_ctrl_tab_window_switching
end type
end forward

global type w_ctrl_tab_window_switching from Window
int X=481
int Y=641
int Width=2017
int Height=313
long BackColor=12632256
WindowType WindowType=response!
event ue_keyup pbm_keyup
sle_1 sle_1
p_1 p_1
end type
global w_ctrl_tab_window_switching w_ctrl_tab_window_switching

type variables
Window	iw_ActiveSheet,&
	iw_LastSheet
end variables

on ue_keyup;IF Not KeyDown(KeyControl!) Then
	IF IsValid (iw_ActiveSheet) Then
		iw_ActiveSheet.BringtoTop = True
		iw_ActiveSheet.SetFocus()
		Close(This)
	end if
END IF

end on

event key;Window	lw_LastSheet
IF KeyDown( KeyShift!) Then
	IF KeyDown( KeyControl!) And KeyDown( KeyTab!) Then
		iw_ActiveSheet = gw_base_frame.GetFirstSheet()
		DO 
			lw_LastSheet = iw_ActiveSheet
			iw_ActiveSheet = gw_base_Frame.GetNextSheet( iw_ActiveSheet)
		Loop While IsValid( iw_ActiveSheet) AND iw_ActiveSheet<> iw_LastSheet
		iw_LastSheet = lw_LastSheet
		iw_ActiveSheet = lw_LastSheet
	END IF
ELSE
	IF KeyDown( KeyControl!) And KeyDown( KeyTab!) Then
		IF IsValid( iw_ActiveSheet) Then
			iw_ActiveSheet = gw_base_frame.GetNextSheet( iw_ActiveSheet)
			IF Not IsValid( iw_ActiveSheet) Then
				iw_ActiveSheet = gw_base_frame.GetFirstSheet()
			END IF
		ELSE
			iw_ActiveSheet = gw_base_frame.GetFirstSheet()
			iw_ActiveSheet = gw_base_frame.GetNextSheet( iw_ActiveSheet)
		END IF
	END IF
END IF
If IsValid( iw_ActiveSheet) Then
	sle_1.Text = Trim(iw_ActiveSheet.Title)
ELSE
	Close(This)
END IF

end event

event open;Window	lw_LastSheet

String	ls_appid

ls_appid	=	Message.nf_Get_app_id()
p_1.PictureName	=	ls_appid + '.BMP'
iw_activesheet = gw_base_frame.GetFirstSheet()
IF KeyDown(KeyShift!) Then
	DO
		lw_LastSheet = iw_ActiveSheet
		iw_ActiveSheet = gw_base_frame.GetNextSheet( iw_ActiveSheet)
	Loop While IsValid( iw_ActiveSheet)
	iw_ActiveSheet = lw_LastSheet
ELSE
	IF IsValid( iw_ActiveSheet) Then
		iw_activesheet = gw_base_frame.GetNextSHeet( iw_ActiveSheet)
	ELSE
		Close(This)
	END IF
END IF
IF IsValid ( Iw_ActiveSheet) Then
	sle_1.Text = Trim(iw_ActiveSheet.Title)
ELSE
	Close( This)
END IF
end event

on w_ctrl_tab_window_switching.create
this.sle_1=create sle_1
this.p_1=create p_1
this.Control[]={ this.sle_1,&
this.p_1}
end on

on w_ctrl_tab_window_switching.destroy
destroy(this.sle_1)
destroy(this.p_1)
end on

type sle_1 from statictext within w_ctrl_tab_window_switching
int X=321
int Y=93
int Width=1459
int Height=101
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
string Text="none"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-11
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type p_1 from picture within w_ctrl_tab_window_switching
int X=151
int Y=81
int Width=147
int Height=121
boolean FocusRectangle=false
end type

