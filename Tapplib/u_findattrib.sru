HA$PBExportHeader$u_findattrib.sru
forward
global type u_findattrib from nonvisualobject
end type
end forward

global type u_findattrib from nonvisualobject autoinstantiate
end type
global u_findattrib u_findattrib

type variables
powerobject	ipo_requestor

string 		is_find=''
string 		is_replacewith=''

boolean 		ib_wholewordvisible=True
boolean 		ib_wholewordenabled=True
boolean 		ib_wholeword=False

boolean 		ib_matchcasevisible=True
boolean 		ib_matchcaseenabled=True
boolean 		ib_matchcase=False

boolean 		ib_directionvisible=True
boolean 		ib_directionenabled=True
string 		is_direction=''

boolean 		ib_lookvisible=True
boolean 		ib_lookenabled=True
integer 		ii_lookindex
string 		is_lookdata[]
string 		is_lookdisplay[]

end variables

on u_findattrib.create
TriggerEvent( this, "constructor" )
end on

on u_findattrib.destroy
TriggerEvent( this, "destructor" )
end on

