HA$PBExportHeader$u_ip_functions.sru
forward
global type u_ip_functions from nonvisualobject
end type
end forward

type wsadata from structure
	unsignedinteger		wversion
	unsignedinteger		whighversion
	character		szdescription[257]
	character		szsystemstatus[129]
	unsignedinteger		imaxsockets
	unsignedinteger		imaxudpdg
	string		lpvendorinfo
end type

global type u_ip_functions from nonvisualobject autoinstantiate
end type

type prototypes
Function String inet_ntoa(ulong addr) Library "winsock.dll" alias for "inet_ntoa;Ansi"
Function Int WSACleanup() library "winsock.dll"
Function uLong GetMyIPAddr() library "wlibsock.dll"
Function Int WSAGetLastError() Library "winsock.dll"
Function Int WSAStartup(uInt VersionRequired, ref WSAData lpWSAData) library "winsock.dll" alias for "WSAStartup;Ansi"
Function int Get_Ip_add_string( Ref Char PCNAME[100]) Library "Ip.dll" alias for "Get_Ip_add_string;Ansi"

end prototypes

type variables
Environment	ienv_Environment

end variables

forward prototypes
public function string nf_get_ip_address ()
end prototypes

public function string nf_get_ip_address ();char lc_PCIP_Address[100]

Int		li_ret,&
			li_rtn

uLong		ll_name
String	ls_ip_addr,&
			ls_PCIP_Address
			
String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message			
			
			
WSAData	lstr_WSAData

lc_PCIP_Address = Space(100)
ls_PCIP_Address = Space(100)
ls_ip_addr = space(16)

// PowerBuilder 5.0 32 bit Windows 95 or Windows NT
IF ienv_environment.OsMajorRevision = 4 OR ienv_environment.OsType = WindowsNT! Then
	lc_PCIP_Address = Space(100)
	li_rtn =  Get_Ip_add_string( lc_PCIP_Address)
	ls_ip_addr = lc_PCIP_Address
	
ELSE
	SetPointer(HourGlass!)
	li_ret = WSAStartup(257, lstr_WSAData)
	If li_ret <> 0 Then li_ret = WSAGetLastError()
	ll_name = GetMyIPAddr()
	ls_ip_addr = inet_ntoa(ll_name)
	li_ret = WSACleanup()
	if li_ret <> 0 TheN	li_ret = WSAGetLastError()
END IF

Return ls_ip_addr

end function

on u_ip_functions.create
TriggerEvent( this, "constructor" )
end on

on u_ip_functions.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;GetEnvironment( ienv_Environment)

end event

