HA$PBExportHeader$w_base_settings.srw
forward
global type w_base_settings from Window
end type
type tab_1 from tab within w_base_settings
end type
type tabpage_3 from userobject within tab_1
end type
type dw_window from datawindow within tabpage_3
end type
type tabpage_2 from userobject within tab_1
end type
type dw_toolbars from datawindow within tabpage_2
end type
type cb_reset from commandbutton within w_base_settings
end type
type cb_ok from commandbutton within w_base_settings
end type
type tabpage_3 from userobject within tab_1
dw_window dw_window
end type
type tabpage_2 from userobject within tab_1
dw_toolbars dw_toolbars
end type
type tab_1 from tab within w_base_settings
tabpage_3 tabpage_3
tabpage_2 tabpage_2
end type
end forward

type s_toolbar from structure
	string		se_alignment
	integer		se_text
	integer		se_tips
	integer		se_hide
end type

global type w_base_settings from Window
int X=353
int Y=249
int Width=2094
int Height=1009
boolean TitleBar=true
string Title="Settings"
long BackColor=12632256
boolean ControlMenu=true
WindowType WindowType=response!
tab_1 tab_1
cb_reset cb_reset
cb_ok cb_ok
end type
global w_base_settings w_base_settings

type variables
// Holds the pointer to its parent window
w_base_frame  iw_parent

// Holds pointer to application object
application ia_app

// Holds the current toolbar alignment
ToolBarAlignMent  ie_ToolBarAlignment

s_toolbar	istr_toolbar

//microhelp color values
long	il_message_color, &
	il_status_color, &
	il_userid_color, &
	il_date_color

integer	il_show_userid, &
	il_show_status, &	
	il_show_date

boolean	ib_colorwindowopen

datawindow	idw_active

integer	ii_prompt_on_exit, &
	ii_prompt_to_save_onexit, &
	ii_save_dw_layouts, &	
	ii_display_hscrollbars, &
	ii_display_vscrollbars, &
	ii_prompt_on_close
string	is_always_openas		


end variables

forward prototypes
public subroutine wf_set_toolbars ()
public subroutine wf_initialize (datawindow adw_target)
end prototypes

public subroutine wf_set_toolbars ();u_conversion_functions	lu_conv

CHOOSE CASE iw_parent.ToolbarAlignment
	CASE AlignAtLeft! 
		istr_toolbar.se_alignment	=	'alignatleft'
	CASE AlignAtRight! 
		istr_toolbar.se_alignment	=	'alignatright'
	CASE AlignAtTop!  
		istr_toolbar.se_alignment	=	'alignattop'
	CASE Floating!  
		istr_toolbar.se_alignment	=	'floating'
END CHOOSE
If iw_parent.ToolBarVisible Then
	istr_toolbar.se_hide = 1
Else
	istr_toolbar.se_hide = 0
End If

	istr_toolbar.se_text = lu_conv.nf_integer(ia_app.ToolBarText)

	istr_toolbar.se_tips = lu_conv.nf_integer(ia_app.ToolBarTips)

end subroutine

public subroutine wf_initialize (datawindow adw_target);u_base_data	lu_base_data

lu_base_data = gw_base_frame.iu_base_data

ii_prompt_on_exit				=	lu_base_data.ii_prompt_on_exit
ii_prompt_to_save_onexit	=	lu_base_data.ii_prompt_to_save_onexit
ii_save_dw_layouts			=	lu_base_data.ii_save_dw_layouts
ii_display_hscrollbars		=	lu_base_data.ii_display_hscrollbars
ii_display_vscrollbars		=	lu_base_data.ii_display_vscrollbars
ii_prompt_on_close			=	lu_base_data.ii_prompt_on_close
is_always_openas				=	lu_base_data.is_always_openas
adw_target.Object.prompt_on_exit[1]						=	ii_prompt_on_exit
adw_target.Object.prompt_to_save_onexit[1]			=	ii_prompt_to_save_onexit
adw_target.Object.save_datawindow_layouts[1]			=	ii_save_dw_layouts
adw_target.Object.display_horizontal_scrollbars[1]	=	ii_display_hscrollbars
adw_target.Object.display_vertical_scrollbars[1]	=	ii_display_vscrollbars
adw_target.Object.prompt_on_close[1]					=	ii_prompt_on_close
adw_target.Object.always_openas[1]						=	is_always_openas

end subroutine

on w_base_settings.create
this.tab_1=create tab_1
this.cb_reset=create cb_reset
this.cb_ok=create cb_ok
this.Control[]={ this.tab_1,&
this.cb_reset,&
this.cb_ok}
end on

on w_base_settings.destroy
destroy(this.tab_1)
destroy(this.cb_reset)
destroy(this.cb_ok)
end on

event open;// Get the passed pointer to the parent window
// Note: This window must be opened with Parm (OpenWithParm)
// and passed the pointer of the parent window (frame window)
iw_parent = Message.PowerObjectParm

// Get a pointer to the applicaition object
ia_app = GetApplication()


wf_initialize(tab_1.tabpage_3.dw_window)





end event

type tab_1 from tab within w_base_settings
event selectionchanging pbm_tcnselchanging
int X=5
int Width=2076
int Height=753
int TabOrder=10
boolean FixedWidth=true
boolean ShowPicture=false
boolean RaggedRight=true
boolean BoldSelectedText=true
Alignment Alignment=Center!
int SelectedTab=1
long BackColor=12632256
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
tabpage_3 tabpage_3
tabpage_2 tabpage_2
end type

event selectionchanging;DragObject	ldo_object

IF oldindex <= UpperBound(Control) AND oldindex > 0 THEN
	This.Control[oldindex].Control[1].TriggerEvent("ue_update")
END IF

IF newindex <= UpperBound(Control) AND newindex > 0 THEN
	ldo_object = This.Control[newindex].Control[1]
	ldo_object.SetFocus()
END IF
end event

on tab_1.create
this.tabpage_3=create tabpage_3
this.tabpage_2=create tabpage_2
this.Control[]={ this.tabpage_3,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_3)
destroy(this.tabpage_2)
end on

type tabpage_3 from userobject within tab_1
int X=19
int Y=113
int Width=2039
int Height=625
long BackColor=12632256
string Text="Window"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_window dw_window
end type

on tabpage_3.create
this.dw_window=create dw_window
this.Control[]={ this.dw_window}
end on

on tabpage_3.destroy
destroy(this.dw_window)
end on

type dw_window from datawindow within tabpage_3
event ue_reset ( )
int X=5
int Y=5
int Width=2035
int Height=617
int TabOrder=3
string DataObject="d_settings"
boolean Border=false
boolean LiveScroll=true
end type

event ue_reset;This.Object.prompt_on_exit[1]						=	1
This.Object.prompt_to_save_onexit[1]			=	0
This.Object.save_datawindow_layouts[1]			=	0
This.Object.display_horizontal_scrollbars[1]	=	0
This.Object.display_vertical_scrollbars[1]	=	0
This.Object.prompt_on_close[1]					=	1
This.Object.always_openas[1]						=	'original'
ii_prompt_on_exit				=	1
ii_prompt_to_save_onexit	=	0
ii_save_dw_layouts			=	0
ii_display_hscrollbars		=	0
ii_display_vscrollbars		=	0
ii_prompt_on_close			=	1
is_always_openas				=	'original'

end event

event constructor;InsertRow(0)

end event

event destructor;gw_base_frame.iu_base_data.Trigger Event ue_reset_winsettings(ii_prompt_on_exit, &
	ii_prompt_to_save_onexit, ii_save_dw_layouts, ii_display_hscrollbars, &
	ii_display_vscrollbars, ii_prompt_on_close, is_always_openas)
end event

event itemchanged;
CHOOSE CASE	dwo.Name
	CASE	'prompt_on_exit'
		ii_prompt_on_exit				=	Long(Data)
	CASE	'prompt_to_save_onexit'
		ii_prompt_to_save_onexit	=	Long(Data)
	CASE	'save_datawindow_layouts'
		ii_save_dw_layouts			=	Long(Data)
	CASE	'display_horizontal_scrollbars'
		ii_display_hscrollbars		=	Long(Data)
	CASE	'display_vertical_scrollbars'
		ii_display_vscrollbars		=	Long(Data)
	CASE	'prompt_on_close'
		ii_prompt_on_close			=	Long(Data)
	CASE	'always_openas'
		is_always_openas				=	String(Data)
END CHOOSE
end event

type tabpage_2 from userobject within tab_1
int X=19
int Y=113
int Width=2039
int Height=625
long BackColor=12632256
string Text="Toolbar"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_toolbars dw_toolbars
end type

on tabpage_2.create
this.dw_toolbars=create dw_toolbars
this.Control[]={ this.dw_toolbars}
end on

on tabpage_2.destroy
destroy(this.dw_toolbars)
end on

type dw_toolbars from datawindow within tabpage_2
event constructor pbm_constructor
event getfocus pbm_dwnsetfocus
event itemchanged pbm_dwnitemchange
event ue_update pbm_custom01
event ue_reset ( )
int Y=9
int Width=1870
int Height=617
int TabOrder=2
string DataObject="d_toolbars"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;InsertRow(0)
end event

event getfocus;wf_set_toolbars()
This.Object.Data[1]	=	istr_toolbar
end event

event itemchanged;string	ls_data

u_conversion_functions	lu_conv

ls_data	=	String(data)
CHOOSE CASE dwo.Name
	CASE	'toolbar_position'
		CHOOSE CASE ls_data
			CASE	'alignatleft'
				iw_parent.ToolBarAlignment = AlignAtLeft! 
			CASE	'alignatright'
				iw_parent.ToolBarAlignment = AlignAtRight!
			CASE	'alignattop'
				iw_parent.ToolBarAlignment = AlignAtTop!
			CASE	'Bottom'
				iw_parent.ToolBarAlignment = AlignAtBottom!
			CASE	'Floating'
				iw_parent.ToolBarAlignment = Floating!
		END CHOOSE
	CASE	'show_text'
		ia_app.ToolBarText = lu_conv.nf_boolean(ls_data)
	CASE	'show_tips'
		ia_app.ToolBarTips = lu_conv.nf_boolean(ls_data)
	CASE	'hide_orshow'
		iw_parent.ToolBarVisible = lu_conv.nf_boolean(ls_data)
END CHOOSE
		
end event

event ue_reset;iw_parent.ToolBarAlignment = AlignAtTop! 
ia_app.ToolBarText = False
ia_app.ToolBarTips = True
iw_parent.ToolBarVisible = True

This.Object.toolbar_position[1]	=	'alignattop'
This.Object.show_text[1]			=	0
This.Object.show_tips[1]			=	1
This.Object.hide_orshow[1]			=	1


		


end event

type cb_reset from commandbutton within w_base_settings
int X=993
int Y=793
int Width=247
int Height=109
int TabOrder=20
string Text="Reset"
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;Tab_1.tabpage_2.dw_toolbars.Trigger Event ue_reset()
Tab_1.tabpage_3.dw_window.Trigger Event ue_reset()
end event

type cb_ok from commandbutton within w_base_settings
int X=686
int Y=793
int Width=247
int Height=109
int TabOrder=30
string Text="&Close"
boolean Default=true
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;Close(Parent)
end event

