HA$PBExportHeader$w_system_error.srw
forward
global type w_system_error from Window
end type
type cb_email from commandbutton within w_system_error
end type
type dw_error from datawindow within w_system_error
end type
type cb_print from commandbutton within w_system_error
end type
type cb_exit from commandbutton within w_system_error
end type
type cb_continue from commandbutton within w_system_error
end type
end forward

global type w_system_error from Window
int X=321
int Y=421
int Width=2570
int Height=913
boolean TitleBar=true
string Title="Application System Error"
long BackColor=12632256
WindowType WindowType=response!
cb_email cb_email
dw_error dw_error
cb_print cb_print
cb_exit cb_exit
cb_continue cb_continue
end type
global w_system_error w_system_error

type variables
mailSession	ims_mail
end variables

forward prototypes
public function string wf_get_error_text (integer ai_error_number)
end prototypes

public function string wf_get_error_text (integer ai_error_number);string	ls_text

CHOOSE CASE ai_error_number
	CASE	1
		ls_text = "Divide by zero"
	CASE	2
		ls_text = "Null object reference"
	CASE	3
		ls_text = "Array boundary exceeded"
	CASE	4
		ls_text = "Enumerated value is out of range"
	CASE	5
		ls_text = "Negative value encountered in function"
	CASE	6
		ls_text = "Invalid DataWindow row/column specified"
	CASE	7
		ls_text = "Unresolvable external when linking references"
	CASE	8
		ls_text = "Referance of array with NULL subscript"
	CASE	9
		ls_text = "DLL function not found in current application"
	CASE	10
		ls_text = "Unsupported argument type in DLL function"
	CASE	12
		ls_text = "DataWindow column type does not match GetItem Type"
	CASE	13
		ls_text = "Unresolved attribute reference"
	CASE	14
		ls_text = "Error opening DLL library for external function"
	CASE	15
		ls_text = "Error calling external function"
	CASE	16
		ls_text = "Maximum string size exceeded"
	CASE	17
		ls_text = "DataWindow referenced in DataWindow object does not exist"
	CASE	50
		ls_text = "Application reference could not be resolved"
	CASE	51
		ls_text = "Failure loading dynamic library"
END CHOOSE

Return ls_text









end function

event open;/////////////////////////////////////////////////////////////////////////
//
// Event	 :  w_system_error.open
//
// Purpose:
// 			Displays system errors and allows the user to either continue
//				running the application, exit the application, or print the 
//				error message.  Called from the systemerror event in the
//				application object.
//
// Log:
// 
// DATE		NAME				REVISION
//------		-------------------------------------------------------------
// Powersoft Corporation	INITIAL VERSION
//
///////////////////////////////////////////////////////////////////////////
string		ls_appid, &
				ls_userid, &
				ls_message

integer		li_file


dw_error.insertrow (1)
ls_appid  = Message.nf_get_app_id()
Message.StringParm = ""
Message.TriggerEvent("ue_userid")
ls_userid = Message.StringParm
IF IsNull(error.text) or Trim(error.text) = "" THEN
	ls_message = wf_get_error_text(error.number)
ELSE
	ls_message = error.text
END IF

dw_error.setitem (1,"errornum",string(error.number))
dw_error.setitem (1,"message" ,ls_message)
dw_error.setitem (1,"where"   ,error.windowmenu)
dw_error.setitem (1,"object"  ,error.object)
dw_error.setitem (1,"event"   ,error.objectevent)
dw_error.setitem (1,"line"    ,string(error.line))
dw_error.setitem (1,"err_date",String(Today(), "mmm dd, yyyy"))
dw_error.setitem (1,"err_time",Now())
dw_error.setitem (1,"application_id",ls_appid)
dw_error.setitem (1,"userid", ls_userid)



IF FileExists("f:\software\pb\pblog101\pbsyserr.log") THEN
	li_file = FileOpen("f:\software\pb\pblog101\pbsyserr.log", &
									LineMode!, Write!, LockWrite!, Append!)
		If li_file <> -1 Then
			FileWrite(li_file, '~r~n' + String(Today(), "mm-dd-yyyy") + '~t' + &
					String(Now(), "hh:mm:ss.ff") + '~t' + ls_userid + &
					'~t' + ls_appid + '~t' + error.windowmenu + '~t' + &
					error.object + '~t' + error.objectevent + '~r~n' + &
					'~t' + "Err No: " + string(error.line) + '~t' + &
					+ "Err Line: " +  string(error.number) + '~t' + &
					"Err Message: " + ls_message )
				FileClose(li_file)
		END IF
ELSE
	li_file = FileOpen(gw_base_frame.is_workingdir + "PBSYSERR.LOG", &
										LineMode!, Write!, LockWrite!, Append!)
  		IF li_file <> -1 Then
			FileWrite(li_file, '~r~n' + String(Today(), "mm-dd-yyyy") + '~t' + &
					String(Now(), "hh:mm:ss.ff") + '~t' + ls_userid + &
					'~t' + ls_appid + '~t' + error.windowmenu + '~t' + &
					error.object + '~t' + error.objectevent + '~r~n' + &
					'~t' + "Err No: " + string(error.line) + '~t' + &
					+ "Err Line: " +  string(error.number) + '~t' + &
					"Err Message: " + ls_message )
			FileClose(li_file)
		End if
END IF
		
end event

on w_system_error.create
this.cb_email=create cb_email
this.dw_error=create dw_error
this.cb_print=create cb_print
this.cb_exit=create cb_exit
this.cb_continue=create cb_continue
this.Control[]={ this.cb_email,&
this.dw_error,&
this.cb_print,&
this.cb_exit,&
this.cb_continue}
end on

on w_system_error.destroy
destroy(this.cb_email)
destroy(this.dw_error)
destroy(this.cb_print)
destroy(this.cb_exit)
destroy(this.cb_continue)
end on

type cb_email from commandbutton within w_system_error
event clicked pbm_bnclicked
event destructor pbm_destructor
int X=2094
int Y=361
int Width=407
int Height=85
int TabOrder=31
string Text="&E-mail"
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;mailMessage		lmm_Msg

mailRecipient	lmr_to

String			ls_Body


If MessageBox("E-mail Help Desk", "This will e-mail this error to the Help Desk." + &
				"Do you want to continue?", Question!, YesNo!, 1) = 2 Then
	MessageBox("E-mail Help Desk", "E-mail was not sent to the Help Desk")
	return
End if

If Not IsValid(ims_mail) Then
	ims_mail = CREATE mailSession

	If ims_Mail.mailLogon() <> mailReturnSuccess! Then
		MessageBox("E-Mail Help Desk", "Unable to start mail.  Please call the help desk at extension 3133")
		Destroy(ims_mail)
		return
	End if
End if

ls_Body = "This message was generated by the " + &
			dw_error.GetItemString (1, "Application_id") + &
			" system.~r~n~r~n" + &
			"Please log this issue and forward it on to Applications Development.~r~n"
ls_Body += "This error was generated by user: " + SQLCA.userid + "~r~n~r~n"
			
ls_Body += "Application ID:~t" + dw_error.GetItemString (1, "Application_id") + "~r~n"
ls_Body += "Error Number:~t" + dw_error.GetItemString (1, "errornum") + "~r~n"
ls_Body += "Where:~t" + dw_error.GetItemString (1, "where") + "~r~n"
ls_Body += "Object:~t" + dw_error.GetItemString (1, "object") + "~r~n"
ls_Body += "Event:~t" + dw_error.GetItemString (1, "event") + "~r~n"
ls_Body += "Line:~t" + dw_error.GetItemString (1, "line") + "~r~n"
ls_Body += "Message:~t" + dw_error.GetItemString (1, "message") + "~r~n"
 
 
lmm_Msg.Subject = 'Message from the ' + dw_error.GetItemString (1, "Application_id") + ' system'
lmm_Msg.Recipient[1].Name = 'Help Desk'
lmm_Msg.NoteText = ls_Body


If ims_mail.MailSend(lmm_Msg) = mailReturnSuccess! Then
	MessageBox("E-mail Help Desk", "The message was successfully sent to the Help Desk")
Else
	MessageBox("E-mail Help Desk", "There was an error e-mailing the Help Desk.  Please call them at Extension 3133")
End if

end event

event destructor;If IsValid(ims_mail) Then 
	ims_mail.mailLogoff()
	Destroy(ims_Mail)
End if
end event

event constructor;If Not FileExists("f:\software\am_i_on\Yes") Then
	Hide(This)
End if
end event

type dw_error from datawindow within w_system_error
int X=23
int Y=41
int Width=2030
int Height=717
int TabOrder=10
boolean Enabled=false
string DataObject="d_system_error"
BorderStyle BorderStyle=StyleLowered!
end type

type cb_print from commandbutton within w_system_error
int X=2094
int Y=253
int Width=407
int Height=85
int TabOrder=40
string Text="&Print"
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;/////////////////////////////////////////////////////////////////////////
//
// Event	 :  w_system_error.cb_print.clicked!
//
// Purpose:
// 			Event cb_print.clicked - Print the current error message
//				and write the error message to the supplied file name.
//
// Log:
// 
//  DATE		NAME				REVISION
// ------	-------------------------------------------------------------
// Powersoft Corporation	INITIAL VERSION
//
///////////////////////////////////////////////////////////////////////////

string ls_line
Long	ll_prt

ll_prt   = printopen("System Error")

// Print each string variable

print    (ll_prt, "System error message - "+string(today())+" - "+string(now(), "HH:MM:SS"))
print    (ll_prt, " ")

ls_line = "Error Number  : " + getitemstring(dw_error,1,1)
print    (ll_prt, ls_line)

ls_line = "Error Message : " + getitemstring(dw_error,1,2)
print    (ll_prt, ls_line)

ls_line = "Window/Menu   : " + getitemstring(dw_error,1,3)
print    (ll_prt, ls_line)

ls_line = "Object        : " + getitemstring(dw_error,1,4)
print    (ll_prt, ls_line)

ls_line = "Event         : " + getitemstring(dw_error,1,5)
print    (ll_prt, ls_line)

ls_line = "Line Number   : " + getitemstring(dw_error,1,6)
print    (ll_prt, ls_line)

printclose(ll_prt)
return
end event

type cb_exit from commandbutton within w_system_error
int X=2094
int Y=145
int Width=407
int Height=85
int TabOrder=30
string Text="&Exit Program"
boolean Default=true
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;/////////////////////////////////////////////////////////////////////////
//
// Event	 :  w_system_error.cb_exit
//
// Purpose:
// 			Ends the application session
//
// Log:
// 
// DATE		NAME				REVISION
//------		-------------------------------------------------------------
// Powersoft Corporation	INITIAL VERSION
//
///////////////////////////////////////////////////////////////////////////

halt close
end on

type cb_continue from commandbutton within w_system_error
int X=2094
int Y=37
int Width=407
int Height=85
int TabOrder=20
string Text="&Continue"
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;/////////////////////////////////////////////////////////////////////////
//
// Event	 :  w_system_error.cb_continue
//
// Purpose:
// 			Closes w_system_error
//
// Log:
// 
// DATE		NAME				REVISION
//------		-------------------------------------------------------------
// Powersoft Corporation	INITIAL VERSION
//
///////////////////////////////////////////////////////////////////////////
Close(gw_base_frame.GetActiveSheet())
Close(parent)
end event

