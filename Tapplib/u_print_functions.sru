HA$PBExportHeader$u_print_functions.sru
forward
global type u_print_functions from nonvisualobject
end type
end forward

global type u_print_functions from nonvisualobject
end type
global u_print_functions u_print_functions

type prototypes
Function int showPrintDlg(ULONG hwndOwner, ref string as_printer, &
		ref boolean ab_all_pages,  &
		ref boolean ab_collate, &
		ref boolean ab_disable_printtofile,  &
		ref boolean ab_hide_printtofile, &
		ref boolean ab_nopagenums,  &
		ref boolean ab_noselection, &
		ref boolean ab_nowarning,  &
		ref boolean ab_pagenums,  &
		ref boolean ab_printsetup,  &
		ref boolean ab_printtofile, &
		ref boolean ab_returndefault,  &
		ref boolean ab_selection, &
		ref boolean ab_showhelp,  &
		ref int ai_frompage,  &
		ref int ai_topage, &
		ref int ai_minpage,  &
		ref int ai_maxpage,  &
		ref int ai_copies)  &
library "printdlg.dll" alias for "showPrintDlg;Ansi"

Function int showPageSetupDlg(long hwndOwner,  &
			ref string as_printer, &
			ref boolean ab_default_min_margins, &
			ref boolean ab_disable_margins, &
			ref boolean ab_disable_orientation, &
			ref boolean ab_disable_paper, &
			ref boolean ab_disable_printer, &
			ref boolean ab_margins, &
			ref boolean ab_min_margins, &
			ref boolean ab_nowarning, &
			ref boolean ab_return_default, &
			ref boolean ab_show_help, &
			ref long al_size_x, &
			ref long al_size_y, &
			ref long al_min_left, &
			ref long al_min_right, &
			ref long al_min_top, &
			ref long al_min_bottom, &
			ref long al_left, &
			ref long al_right, &
			ref long al_top, &
			ref long al_bottom) &
library "printdlg.dll" alias for "showPageSetupDlg;Ansi"
end prototypes

type variables
string is_default, is_driver, is_port, is_printer,is_OrigPrinter
end variables

forward prototypes
public function boolean uf_printdialog (ref string as_printer_name, ref boolean ab_all_pages, ref boolean ab_collate, ref boolean ab_disable_printtofile, ref boolean ab_hide_printtofile, ref boolean ab_nopagenums, ref boolean ab_noselection, ref boolean ab_nowarning, ref boolean ab_pagenums, ref boolean ab_printsetup, ref boolean ab_printtofile, ref boolean ab_returndefault, ref boolean ab_selection, ref boolean ab_showhelp, ref integer ai_frompage, ref integer ai_topage, ref integer ai_minpage, ref integer ai_maxpage, ref integer ai_copies)
public function boolean uf_pagesetupdialog (ref string as_printer_name, ref boolean ab_default_min_margins, ref boolean ab_disable_margins, ref boolean ab_disable_orientation, ref boolean ab_disable_paper, ref boolean ab_disable_printer, ref boolean ab_margins, ref boolean ab_min_margins, ref boolean ab_nowarning, ref boolean ab_return_default, ref boolean ab_show_help, ref long al_size_x, ref long al_size_y, ref long al_min_left, ref long al_min_right, ref long al_min_top, ref long al_min_bottom, ref long al_left, ref long al_right, ref long al_top, ref long al_bottom)
public function boolean uf_printdialog (ref boolean ab_collate, ref integer ai_from_page, ref integer ai_to_page, ref integer ai_min_page, ref integer ai_max_page, ref integer ai_copies)
public function string uf_get_default_printer ()
public function boolean nf_getcurrentprinter ()
public function boolean nf_setprinter (string as_printer)
public function boolean nf_resetprinter ()
public function integer nf_saveprinter (string as_printername)
public function string nf_getsavedprinter ()
end prototypes

public function boolean uf_printdialog (ref string as_printer_name, ref boolean ab_all_pages, ref boolean ab_collate, ref boolean ab_disable_printtofile, ref boolean ab_hide_printtofile, ref boolean ab_nopagenums, ref boolean ab_noselection, ref boolean ab_nowarning, ref boolean ab_pagenums, ref boolean ab_printsetup, ref boolean ab_printtofile, ref boolean ab_returndefault, ref boolean ab_selection, ref boolean ab_showhelp, ref integer ai_frompage, ref integer ai_topage, ref integer ai_minpage, ref integer ai_maxpage, ref integer ai_copies);Boolean	lb_return 
Int li_temp
Long ll_handle
String	ls_LastPrinter


u_string_functions	lu_string

as_printer_name = Space(63)

If IsValid(gw_base_frame.GetActiveSheet()) Then
	ll_handle = Handle(gw_base_frame.GetActiveSheet())
Else
	ll_handle = Handle(gw_base_frame)
End If


//Save The current Printer Name (Default Printer)
lb_return  = This.nf_getcurrentPrinter()
IF Not  LB_Return Then Return FALSE
is_OrigPrinter = is_Default


// Get the one we last printed to
ls_LastPrinter = nf_getSavedPrinter()
IF ls_LastPrinter <> "Not Set" Then 
	lb_Return = This.nf_SetPrinter(ls_LastPrinter)
	IF Not  lb_Return Then Return FALSE
END IF

li_temp = ShowPrintDlg(ll_handle, as_printer_name, &
		ref ab_all_pages,   &
		ref ab_collate,     &
		ref ab_disable_printtofile, &
		ref ab_hide_printtofile,    &
		ref ab_nopagenums,  &
		ref ab_noselection, &
		ref ab_nowarning,   &
		ref ab_pagenums,    &  
		ref ab_printsetup,  &
		ref ab_printtofile, & 
		ref ab_returndefault, & 
		ref ab_selection,     & 
		ref ab_showhelp,      &  
		ref ai_frompage,      & 
		ref ai_topage,        & 
		ref ai_minpage,       & 
		ref ai_maxpage,       & 
		ref ai_copies)        & 

If li_temp <> 0 Then
	// Error Occurred
	return False
End If

If lu_string.nf_isempty(as_printer_name) Then
	// Chose Cancel
	return False
End If

lb_Return = This.nf_SetPrinter(as_Printer_name)
IF Not  lb_Return Then Return FALSE

IF nf_SavePrinter(as_Printer_Name) = 1 Then
	Return True
ELSE
	Return False
ENd IF
end function

public function boolean uf_pagesetupdialog (ref string as_printer_name, ref boolean ab_default_min_margins, ref boolean ab_disable_margins, ref boolean ab_disable_orientation, ref boolean ab_disable_paper, ref boolean ab_disable_printer, ref boolean ab_margins, ref boolean ab_min_margins, ref boolean ab_nowarning, ref boolean ab_return_default, ref boolean ab_show_help, ref long al_size_x, ref long al_size_y, ref long al_min_left, ref long al_min_right, ref long al_min_top, ref long al_min_bottom, ref long al_left, ref long al_right, ref long al_top, ref long al_bottom);Int li_temp
Long ll_handle
u_string_functions	lu_string

as_printer_name = Space(63)

If IsValid(gw_base_frame.GetActiveSheet()) Then
	ll_handle = Handle(gw_base_frame.GetActiveSheet())
Else
	ll_handle = Handle(gw_base_frame)
End If

li_temp = ShowPageSetupDlg(ll_handle, as_printer_name, &
		ref ab_default_min_margins, &
		ref ab_disable_margins, &
		ref ab_disable_orientation, &
		ref ab_disable_paper, &
		ref ab_disable_printer, &
		ref ab_margins, &
		ref ab_min_margins, &
		ref ab_nowarning, &
		ref ab_return_default, &
		ref ab_show_help, &
		ref al_size_x, &
		ref al_size_y, &
		ref al_min_left, &
		ref al_min_right, &
		ref al_min_top, &
		ref al_min_bottom, &
		ref al_left, &
		ref al_right, &
		ref al_top, &
		ref al_bottom)

If li_temp <> 0 Then
	// Error Occurred
	return False
End If

If lu_string.nf_isempty(as_printer_name) Then
	// Chose Cancel
	return False
End If

return True
end function

public function boolean uf_printdialog (ref boolean ab_collate, ref integer ai_from_page, ref integer ai_to_page, ref integer ai_min_page, ref integer ai_max_page, ref integer ai_copies);// This function supplies most default arguments to PrintDialog().
// Arguments are:					
//		boolean	ab_collate			Whether collating is set on or not
//		integer	ai_from_page
//		integer	ai_to_page
//		integer	ai_min_page			Minimum page number the user can choose
//		integer	ai_max_page			Maximum page number the user can choose
//		integer	ai_copies	
// 
// On entry, these arguments populate the PrintDialog dialog box initially.
// On exit, these arguments contain the final values the user selected in the dialog box.

String ls_printer_name
Boolean	lb_true = True, &
			lb_false = False

return uf_printdialog(ls_printer_name, &
					lb_True, &
					ab_collate, &
					lb_False, &
					lb_True, &
					lb_False, &
					lb_True, &
					lb_False, &
					lb_False, &
					lb_False, &
					lb_False, &
					lb_False, &
					lb_False, &
					lb_False, &
					ai_from_page, &
					ai_to_page, &
					ai_min_page, &
					ai_max_page, &
					ai_copies)
				
end function

public function string uf_get_default_printer ();// This function returns a string containing the Windows 95 text description of the
// current default printer.

String	ls_printer_name
Boolean	lb_ret, &
			lb_true = True, &
			lb_false = False

Integer	li_from = 1, &
			li_to = 1, &
			li_min = 1, &
			li_max = 1, &
			li_copies = 1

u_string_functions	lu_string


lb_ret = uf_printdialog(ls_printer_name, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_true, /* return values for default printer */ &    
								lb_false, &
								lb_false, &
								li_from, &
								li_to, &
								li_min, &
								li_max, &
								li_copies)
								
If Not lb_ret or lu_string.nf_isempty(ls_printer_name) Then
	return ""
Else
	return ls_printer_name								
End If
					
end function

public function boolean nf_getcurrentprinter ();
//Variable declarations:
int li_rtn
Environment env

String	ls_Key
string 	ls_default, ls_driver, ls_port, ls_printer



li_rtn = GetEnvironment(env)
IF li_rtn <> 1 THEN RETURN FALSE

CHOOSE CASE env.OSType
	CASE Windows!
		//Get the current default printer name.
		RegistryGet("HKEY_LOCAL_MACHINE\Config\0001\System\CurrentControlSet\Control\Print\Printers", "default", ls_default)
		is_Default = ls_Default

		//Get the default printers driver and port.
		ls_key = "HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Print\Printers\"+ ls_Default
		RegistryGet(ls_key, "Printer Driver", ls_driver)
		is_Driver = ls_Driver
		RegistryGet(ls_key, "Port", ls_port)
		is_Port = ls_Port
		

	Case WindowsNT!
		//Get the current default printer name.
		RegistryGet("HKEY_CURRENT_USER\Software\Microsoft\Windows NT\CurrentVersion\Windows", "Device", ls_default)
		is_Default = ls_Default
	CASE ELSE
		RETURN False
END CHOOSE
Return True
















end function

public function boolean nf_setprinter (string as_printer);//Variable declarations:
int 						li_rtn
Environment				env
String					ls_printer, &
							ls_port


li_rtn = GetEnvironment(env)

IF li_rtn <> 1 THEN RETURN FALSE

ls_Printer=as_printer

CHOOSE CASE env.OSType
	CASE Windows!
		//Set a new default printer name.
		if POS(ls_printer,",") < 1 THEN
			if RegistryGet("HKEY_LOCAL_MACHINE\System\CurrentControlSet\control\Print\Printers\"+ls_Printer,&
										 "Port",RegString!,ls_Port) = 1 Then &
			ls_printer = ls_Printer+",WINSPOOL,"+ls_Port 
		End if
		li_rtn = RegistrySet("HKEY_LOCAL_MACHINE\Config\0001\System\CurrentControlSet\Control\Print\Printers", "default", ls_Printer)
		if li_rtn <> 1 Then Return FaLSE	
		
		//Set the new default printer name, driver and port in the WIN.INI file. FOR dos 16BIT Compatibilty
		li_rtn = SetProfileString("c:\windows\win.ini", "Windows", "device", ls_Printer)
		if li_rtn <> 1 Then Return FaLSE	
		
	Case WindowsNT!
		//Set a new default printer name.
		if Right(ls_Printer,15) <> ",winspool,Ne01:" Then ls_Printer += ",winspool,Ne01:"
		li_rtn = RegistrySet("HKEY_CURRENT_USER\Software\Microsoft\Windows NT\CurrentVersion\Windows", &
		"Device", ls_Printer)
		if li_rtn <> 1 Then Return FALSE
	CASE ELSE
		RETURN FALSE
END CHOOSE

Return TRUE










end function

public function boolean nf_resetprinter ();Return This.nf_SetPrinter(is_origprinter)
end function

public function integer nf_saveprinter (string as_printername);Integer	li_LoopCount,&
			li_Pos
String	ls_lastprinter,&
			ls_FilePath,&
			ls_JustPath

u_sdkcalls	lu_sdkCalls
lu_sdkcalls = Create u_sdkcalls


//*********Check to see if first time ran the application
// Get the exe fullpath and Name
ls_FilePath = lu_sdkcalls.nf_getmodulefilename()
Destroy lu_sdkcalls
li_LoopCount = 0
// Find The Last "\"
Do
	li_LoopCount = POS( ls_FilePath,"\", li_LoopCount + 1)
	IF li_LoopCount > 0 Then li_pos = li_LoopCount
Loop While li_LoopCount <> 0

//Get just The Path
ls_JustPath = Left( ls_FilePath, li_pos - 1)

Return SetProfileString(ls_JustPath+"\Ibpuser.ini", "Settings", "Printer", as_PrinterName)

end function

public function string nf_getsavedprinter ();Integer	li_LoopCount,&
			li_Pos
String	ls_lastprinter,&
			ls_FilePath,&
			ls_JustPath

u_sdkcalls	lu_sdkCalls
lu_sdkcalls = Create u_sdkcalls


//*********Check to see if first time ran the application
// Get the exe fullpath and Name
ls_FilePath = lu_sdkcalls.nf_getmodulefilename()
Destroy lu_sdkcalls
li_LoopCount = 0
// Find The Last "\"
Do
	li_LoopCount = POS( ls_FilePath,"\", li_LoopCount + 1)
	IF li_LoopCount > 0 Then li_pos = li_LoopCount
Loop While li_LoopCount <> 0

//Get just The Path
ls_JustPath = Left( ls_FilePath, li_pos - 1)

ls_LastPrinter = ProfileString(ls_JustPath+"\Ibpuser.ini", "Settings", "Printer", "Not Set")
Return ls_LastPrinter
end function

on u_print_functions.create
TriggerEvent( this, "constructor" )
end on

on u_print_functions.destroy
TriggerEvent( this, "destructor" )
end on

