HA$PBExportHeader$w_replace.srw
forward
global type w_replace from w_base_popup
end type
type st_findwhere from u_base_statictext within w_replace
end type
type st_findwhat from u_base_statictext within w_replace
end type
type ddlb_findwhere from u_base_dropdownlistbox within w_replace
end type
type st_searchdirection from u_base_statictext within w_replace
end type
type cbx_wholeword from u_base_checkbox within w_replace
end type
type cbx_matchcase from u_base_checkbox within w_replace
end type
type cb_findnext from u_base_commandbutton within w_replace
end type
type cb_cancel from u_base_commandbutton within w_replace
end type
type sle_findwhat from u_base_singlelineedit within w_replace
end type
type sle_replace from u_base_singlelineedit within w_replace
end type
type st_replace from u_base_statictext within w_replace
end type
type cb_replace from u_base_commandbutton within w_replace
end type
type cb_replaceall from u_base_commandbutton within w_replace
end type
type ddlb_searchdirection from u_base_dropdownlistbox within w_replace
end type
end forward

global type w_replace from w_base_popup
int X=426
int Y=549
int Width=1843
int Height=657
boolean TitleBar=true
string Title="Replace"
long BackColor=80263581
boolean MinBox=false
boolean MaxBox=false
boolean Resizable=false
event ue_default ( )
event ue_findnext ( )
event ue_replace ( )
event ue_replaceall ( )
st_findwhere st_findwhere
st_findwhat st_findwhat
ddlb_findwhere ddlb_findwhere
st_searchdirection st_searchdirection
cbx_wholeword cbx_wholeword
cbx_matchcase cbx_matchcase
cb_findnext cb_findnext
cb_cancel cb_cancel
sle_findwhat sle_findwhat
sle_replace sle_replace
st_replace st_replace
cb_replace cb_replace
cb_replaceall cb_replaceall
ddlb_searchdirection ddlb_searchdirection
end type
global w_replace w_replace

type variables
Public:
u_findattrib	inv_findattrib

end variables

event ue_default;//////////////////////////////////////////////////////////////////////////////
//
//	Event:  			pfc_default
//
//	Arguments: 		None
//	
//	Returns:  		None
//
//	Description:   Gather all the inforamtion entered by the user.
//
//////////////////////////////////////////////////////////////////////////////

//Set text to search for.
inv_findattrib.is_find = sle_findwhat.text

//Set searchwhere index (this index is the array number the user selected).
inv_findattrib.ii_lookindex = ddlb_findwhere.finditem(ddlb_findwhere.text,1)

//Set text which replaces the searched text.
inv_findattrib.is_replacewith = sle_replace.text

//Set the direction flag.
inv_findattrib.is_direction = ddlb_searchdirection.text

//Set wholeword flag
inv_findattrib.ib_wholeword = cbx_wholeword.checked

//St match case flag
inv_findattrib.ib_matchcase = cbx_matchcase.checked


end event

event ue_findnext;//////////////////////////////////////////////////////////////////////////////
//
//	Event:  			pfc_findnext
//
//	(Arguments:		None)
//
//	(Returns:  		None)
//
//	Description:	Populate the find attributes and perform the find next
//						processing.
//
//////////////////////////////////////////////////////////////////////////////

//Populate the find attributes.
this.Event ue_default ()

//Call event on requestor to process.
If inv_findattrib.ipo_requestor.dynamic event ue_findnext(inv_findattrib) = 0 Then
	messagebox(this.Title, this.Title+" has finished searching.")
end if

end event

event ue_replace;//////////////////////////////////////////////////////////////////////////////
//
//	Event:  			pfc_replace
//
//	(Arguments:		None)
//
//	(Returns:  		None)
//
//	Description:	Populate the find attributes and perform the replace
//						processing.
//
//////////////////////////////////////////////////////////////////////////////

//Populate the find attributes.
this.Event ue_default ()

//Call event on requestor to process.
If inv_findattrib.ipo_requestor.dynamic event ue_replace(inv_findattrib) = 0 Then
	messagebox(this.Title, this.Title+" has finished searching.")
end if

end event

event ue_replaceall;call super::ue_replaceall;//
//	Event:  			pfc_repaceall
//
//	(Arguments:		None)
//
//	(Returns:  		None)
//
//	Description:	Populate the find attributes and perform the replace all
//						processing.
//
//////////////////////////////////////////////////////////////////////////////

long ll_rc

//Populate the find attributes.
this.Event ue_default ()

//Call event on requestor to process.
ll_rc = inv_findattrib.ipo_requestor.dynamic event ue_replaceall(inv_findattrib)
If ll_rc >=0 Then
	MessageBox(this.Title, this.Title+" has finished replacing "+string(ll_rc)+" items.")
end if

end event

event open;//////////////////////////////////////////////////////////////////////////////
//
//	Event:  			Open
//
//	Arguments: 		None
//	
// Returns:  		None	
//
//	Description:  	This event is used to initialize the window using the passed in
//						nvo-structure
//
//////////////////////////////////////////////////////////////////////////////
integer	li_count
integer	li_i
integer	li_adjust

//Make a local copy of attribute nvo-structure
inv_findattrib = message.powerobjectparm

//////////////////////////////////////////////////////////////////////////////
// Set the Enabled/Visible attributes for the appropriate controls.
//////////////////////////////////////////////////////////////////////////////

//The Whole Word control.
cbx_wholeword.Visible = inv_findattrib.ib_wholewordvisible
cbx_wholeword.Enabled = inv_findattrib.ib_wholewordenabled

//The Match Case control.
cbx_matchcase.Visible = inv_findattrib.ib_matchcasevisible
cbx_matchcase.Enabled = inv_findattrib.ib_matchcaseenabled

//The lookup controls.
ddlb_findwhere.Visible = inv_findattrib.ib_lookvisible
st_findwhere.Visible = inv_findattrib.ib_lookvisible
ddlb_findwhere.Enabled = inv_findattrib.ib_lookenabled
st_findwhere.Enabled = inv_findattrib.ib_lookenabled

//The direction controls.
ddlb_searchdirection.Visible = inv_findattrib.ib_directionvisible
st_searchdirection.Visible = inv_findattrib.ib_directionvisible
ddlb_searchdirection.Enabled = inv_findattrib.ib_directionenabled
st_searchdirection.Enabled = inv_findattrib.ib_directionenabled

//////////////////////////////////////////////////////////////////////////////
// Initialize controls with the appropriate data.
//////////////////////////////////////////////////////////////////////////////

//Set the lookup values.
If ddlb_findwhere.visible Then
	li_count = upperbound(inv_findattrib.is_lookdata)
	if li_count >0  THEN 
		for li_i=1 TO li_count
			ddlb_findwhere.additem(inv_findattrib.is_lookdisplay[li_i])
		next
	end if
	If inv_findattrib.ii_lookindex > 0 Then
		ddlb_findwhere.SelectItem(inv_findattrib.ii_lookindex)	
	Else
		ddlb_findwhere.SelectItem(1)	
	End If
End If

//Set text to Find What.
sle_findwhat.text = inv_findattrib.is_find

//Set the text to replace with.
sle_replace.text = inv_findattrib.is_replacewith

//Set the WholeWord flag.
If cbx_wholeword.Visible Then
	cbx_wholeword.Checked = inv_findattrib.ib_wholeword
End If

//Set the MatchCase flag.
If cbx_matchcase.Visible Then
	cbx_matchcase.Checked = inv_findattrib.ib_matchcase
End If	

//Set the Direction attribute.
If ddlb_searchdirection.visible Then
	If Lower(inv_findattrib.is_direction)= 'up' Then
		ddlb_searchdirection.Text = 'Up'
	Else
		ddlb_searchdirection.Text = 'Down'
	End If
End If

//////////////////////////////////////////////////////////////////////////////
// Resize window and Move controls, if appropriate.
//////////////////////////////////////////////////////////////////////////////

//If the lookup controls are not visible, moving of other controls is required.
if ddlb_findwhere.visible = False then
	// calculate Y position to adjust.
	li_adjust = sle_findwhat.y - ddlb_findwhere.y

	//Move Controls up
	cbx_matchcase.y = cbx_matchcase.y - li_adjust
	cbx_wholeword.y = cbx_wholeword.y - li_adjust
	st_searchdirection.y = st_searchdirection.y - li_adjust	
	ddlb_searchdirection.y = ddlb_searchdirection.y - li_adjust
	st_replace.y = st_replace.y - li_adjust	
	sle_replace.y = sle_replace.y  - li_adjust
	st_findwhat.y = st_findwhat.y - li_adjust	
	sle_findwhat.y = sle_findwhat.y - li_adjust
	
	//Resize the window to match the adjustment.
	this.height = this.height - li_adjust
	
	//Set focus on the appropriate control.
	sle_findwhat.setfocus()
end if

//If the wholeword is not visible, move the matchase control.
If cbx_wholeword.visible = False and cbx_matchcase.Visible Then
	cbx_matchcase.Y = cbx_wholeword.Y
	//Resize the window to match the adjustment
	this.Height = this.Height - (cbx_matchcase.Height /2)
End If

//If all bottom conrols are not visible, adjust the size of the window.
If ddlb_searchdirection.Visible=False And cbx_wholeword.visible = False And &
	cbx_matchcase.Visible= False Then
	this.Height = this.Height - (cbx_wholeword.Height *2)
End If
end event

on w_replace.create
int iCurrent
call w_base_popup::create
this.st_findwhere=create st_findwhere
this.st_findwhat=create st_findwhat
this.ddlb_findwhere=create ddlb_findwhere
this.st_searchdirection=create st_searchdirection
this.cbx_wholeword=create cbx_wholeword
this.cbx_matchcase=create cbx_matchcase
this.cb_findnext=create cb_findnext
this.cb_cancel=create cb_cancel
this.sle_findwhat=create sle_findwhat
this.sle_replace=create sle_replace
this.st_replace=create st_replace
this.cb_replace=create cb_replace
this.cb_replaceall=create cb_replaceall
this.ddlb_searchdirection=create ddlb_searchdirection
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_findwhere
this.Control[iCurrent+2]=st_findwhat
this.Control[iCurrent+3]=ddlb_findwhere
this.Control[iCurrent+4]=st_searchdirection
this.Control[iCurrent+5]=cbx_wholeword
this.Control[iCurrent+6]=cbx_matchcase
this.Control[iCurrent+7]=cb_findnext
this.Control[iCurrent+8]=cb_cancel
this.Control[iCurrent+9]=sle_findwhat
this.Control[iCurrent+10]=sle_replace
this.Control[iCurrent+11]=st_replace
this.Control[iCurrent+12]=cb_replace
this.Control[iCurrent+13]=cb_replaceall
this.Control[iCurrent+14]=ddlb_searchdirection
end on

on w_replace.destroy
call w_base_popup::destroy
destroy(this.st_findwhere)
destroy(this.st_findwhat)
destroy(this.ddlb_findwhere)
destroy(this.st_searchdirection)
destroy(this.cbx_wholeword)
destroy(this.cbx_matchcase)
destroy(this.cb_findnext)
destroy(this.cb_cancel)
destroy(this.sle_findwhat)
destroy(this.sle_replace)
destroy(this.st_replace)
destroy(this.cb_replace)
destroy(this.cb_replaceall)
destroy(this.ddlb_searchdirection)
end on

type st_findwhere from u_base_statictext within w_replace
int X=33
int Y=53
int Width=298
string Text="Find wher&e"
long BackColor=79416533
end type

type st_findwhat from u_base_statictext within w_replace
int X=33
int Y=181
int Width=298
string Text="Fi&nd what"
long BackColor=79416533
end type

type ddlb_findwhere from u_base_dropdownlistbox within w_replace
int X=366
int Y=45
int Width=1034
int Height=309
int TabOrder=10
boolean Sorted=false
int Accelerator=101
end type

type st_searchdirection from u_base_statictext within w_replace
int X=33
int Y=429
int Width=298
string Text="&Search"
long BackColor=79416533
end type

type cbx_wholeword from u_base_checkbox within w_replace
int X=897
int Y=413
int Width=444
int TabOrder=50
string Text="Whole &Word"
long BackColor=79416533
int TextSize=-8
end type

type cbx_matchcase from u_base_checkbox within w_replace
int X=897
int Y=485
int Width=449
int TabOrder=60
string Text="Match &Case  "
long BackColor=79416533
int TextSize=-8
end type

type cb_findnext from u_base_commandbutton within w_replace
int X=1463
int Y=37
int Width=334
int Height=89
int TabOrder=70
string Text="&Find Next"
int TextSize=-8
end type

event clicked;//////////////////////////////////////////////////////////////////////////////
//
//	Event: 		 	Clicked
//
//	Arguments: 		None
//
//	Returns:  		None
//
//	Description:  Perform the find next processing.
//
//////////////////////////////////////////////////////////////////////////////

Parent.Event ue_findnext ()
end event

type cb_cancel from u_base_commandbutton within w_replace
int X=1463
int Y=333
int Width=334
int Height=89
int TabOrder=100
string Text="Cancel"
int TextSize=-8
end type

event clicked;//////////////////////////////////////////////////////////////////////////////
//
//	Event:  			Clicked
//
//	(Arguments:		None)
//
//	(Returns:  		None)
//
//	Description:	Close this window.
//
//////////////////////////////////////////////////////////////////////////////
Close(Parent)
end event

type sle_findwhat from u_base_singlelineedit within w_replace
int X=366
int Y=165
int Width=1034
int Height=81
int TabOrder=20
int Accelerator=110
end type

type sle_replace from u_base_singlelineedit within w_replace
int X=366
int Y=289
int Width=1034
int Height=81
int TabOrder=30
int Accelerator=112
end type

type st_replace from u_base_statictext within w_replace
int X=33
int Y=301
int Width=298
string Text="Re&place with"
long BackColor=79416533
end type

type cb_replace from u_base_commandbutton within w_replace
int X=1463
int Y=137
int Width=334
int Height=89
int TabOrder=80
string Text="&Replace"
int TextSize=-8
end type

event clicked;//////////////////////////////////////////////////////////////////////////////
//
//	Event: 		 	Clicked
//
//	Arguments: 		None
//
//	Returns:  		None
//
//	Description:  Perform the replace processing.
//
//////////////////////////////////////////////////////////////////////////////

Parent.Event ue_replace ()

end event

type cb_replaceall from u_base_commandbutton within w_replace
int X=1463
int Y=233
int Width=334
int Height=89
int TabOrder=90
string Text="Replace &All"
int TextSize=-8
end type

event clicked;//////////////////////////////////////////////////////////////////////////////
//
//	Event: 		 	Clicked
//
//	Arguments: 		None
//
//	Returns:  		None
//
//	Description:  Perform the replace processing.
//
//////////////////////////////////////////////////////////////////////////////

Parent.Event ue_replaceall ()

end event

type ddlb_searchdirection from u_base_dropdownlistbox within w_replace
int X=366
int Y=425
int Height=209
int TabOrder=40
boolean Sorted=false
int Accelerator=115
long BackColor=1090519039
string Item[]={"Down",&
"Up"}
end type

