HA$PBExportHeader$u_spin_big.sru
forward
global type u_spin_big from u_spin
end type
end forward

global type u_spin_big from u_spin
int Width=147
int Height=225
end type
global u_spin_big u_spin_big

on u_spin_big.create
call u_spin::create
end on

on u_spin_big.destroy
call u_spin::destroy
end on

type p_down from u_spin`p_down within u_spin_big
int Y=117
int Width=138
int Height=101
end type

type p_up from u_spin`p_up within u_spin_big
int Width=138
int Height=101
end type

