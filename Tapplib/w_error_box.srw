HA$PBExportHeader$w_error_box.srw
forward
global type w_error_box from Window
end type
type cb_print from commandbutton within w_error_box
end type
type mle_msg from multilineedit within w_error_box
end type
type cb_ok from commandbutton within w_error_box
end type
end forward

global type w_error_box from Window
int X=677
int Y=269
int Width=1825
int Height=1201
boolean TitleBar=true
string Title="Untitled"
long BackColor=12632256
boolean ControlMenu=true
WindowType WindowType=popup!
cb_print cb_print
mle_msg mle_msg
cb_ok cb_ok
end type
global w_error_box w_error_box

on w_error_box.create
this.cb_print=create cb_print
this.mle_msg=create mle_msg
this.cb_ok=create cb_ok
this.Control[]={ this.cb_print,&
this.mle_msg,&
this.cb_ok}
end on

on w_error_box.destroy
destroy(this.cb_print)
destroy(this.mle_msg)
destroy(this.cb_ok)
end on

type cb_print from commandbutton within w_error_box
int X=887
int Y=965
int Width=247
int Height=89
int TabOrder=30
string Text="&Print"
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;/////////////////////////////////////////////////////////////////////////
//
// Event	 :  w_error_box.cb_print
//
// Purpose:  Print error message displayed in the multi line edit.
// 		
// Log:
// 
// DATE		NAME			REVISION
//------		-------------------------------------------------------------
// Powersoft Corporation INITIAL VERSION
//
/////////////////////////////////////////////////////////////////////////

int li_job

li_job = printopen()

setpointer ( hourglass! )

print(li_job, mle_msg.text)
printclose(li_job)
end on

type mle_msg from multilineedit within w_error_box
int X=33
int Y=25
int Width=1747
int Height=889
int TabOrder=10
boolean Enabled=false
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean DisplayOnly=true
long BackColor=16777215
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_ok from commandbutton within w_error_box
int X=595
int Y=965
int Width=247
int Height=89
int TabOrder=20
string Text="OK"
boolean Default=true
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;/////////////////////////////////////////////////////////////////////////
//
// Event	 :  w_error_box.cb_ok
//
// Purpose:  
// 		
// Log:
// 
// DATE		NAME			REVISION
//------		-------------------------------------------------------------
// 09/06/92	ALEX WHITNEY	INITIAL VERSION
//
/////////////////////////////////////////////////////////////////////////

close(parent)
end on

