HA$PBExportHeader$w_parameters.srw
forward
global type w_parameters from Window
end type
type dw_1 from datawindow within w_parameters
end type
type cb_1 from commandbutton within w_parameters
end type
type st_1 from statictext within w_parameters
end type
type sle_1 from singlelineedit within w_parameters
end type
end forward

global type w_parameters from Window
int X=833
int Y=361
int Width=1998
int Height=1153
boolean TitleBar=true
string Title="Parameters"
long BackColor=79741120
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
boolean Resizable=true
dw_1 dw_1
cb_1 cb_1
st_1 st_1
sle_1 sle_1
end type
global w_parameters w_parameters

on w_parameters.create
this.dw_1=create dw_1
this.cb_1=create cb_1
this.st_1=create st_1
this.sle_1=create sle_1
this.Control[]={ this.dw_1,&
this.cb_1,&
this.st_1,&
this.sle_1}
end on

on w_parameters.destroy
destroy(this.dw_1)
destroy(this.cb_1)
destroy(this.st_1)
destroy(this.sle_1)
end on

type dw_1 from datawindow within w_parameters
int X=10
int Y=285
int Width=1761
int Height=757
int TabOrder=30
string DataObject="d_parameters"
BorderStyle BorderStyle=StyleRaised!
boolean HScrollBar=true
boolean LiveScroll=true
end type

type cb_1 from commandbutton within w_parameters
int X=1294
int Y=157
int Width=476
int Height=109
int TabOrder=20
string Text="List Parameters"
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;Window	lw_temp	


dw_1.Reset()

lw_temp = Create Using sle_1.Text

If Not IsValid(lw_temp) Then 
	MessageBox("Fool", "That's not a window in your application.")
	return
End if
Message.StringParm = ''
lw_temp.Event Dynamic ue_get_data("query")
dw_1.ImportString(Message.StringParm)
Destroy(lw_temp)



end event

type st_1 from statictext within w_parameters
int X=33
int Y=49
int Width=398
int Height=77
boolean Enabled=false
string Text="Window Name"
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_1 from singlelineedit within w_parameters
int X=435
int Y=41
int Width=1335
int Height=93
int TabOrder=10
boolean AutoHScroll=false
long BackColor=16777215
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

