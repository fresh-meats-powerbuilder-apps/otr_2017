HA$PBExportHeader$u_findreplace.sru
forward
global type u_findreplace from nonvisualobject
end type
end forward

global type u_findreplace from nonvisualobject
event type long ue_findnext ( u_findattrib anv_findattr )
event type long ue_replace ( u_findattrib anv_findattr )
event type integer ue_replaceall ( u_findattrib anv_findattr )
event ue_finddlg ( )
event ue_replacedlg ( )
end type
global u_findreplace u_findreplace

type variables
Protected:
string		is_lastfindcolumn
long        		il_lastfindrow

string       		is_findexpression

boolean  		ib_replaced=False
boolean		ib_ongoingfind=False
u_findattrib 	inv_findattrib
u_base_dw	idw_requestor
end variables

forward prototypes
protected function string nf_buildfindexpression (string as_find, string as_column)
protected function integer nf_buildcolumnnames (boolean ab_replacelist)
protected function string nf_getcolumneditstatus (string as_column, long al_row)
protected function long nf_find (string as_column, string as_find, long al_startrow, long al_endrow)
protected function long nf_find ()
protected function boolean nf_compareattrib (u_findattrib anv_findattrib)
protected function integer nf_selecttext (string as_colname, long al_row, boolean ab_matchcase, string as_find)
public function integer nf_initialize (u_findattrib anv_findattrib)
public function boolean nf_isongoingfind (u_findattrib anv_findattrib)
public function string nf_getheadername (string as_column)
public function string nf_getheadername (string as_column, string as_suffix)
public function string nf_getitem (long al_row, string as_column)
public function string nf_getitem (long al_row, string as_column, dwbuffer adw_buffer, boolean ab_orig_value)
public function integer nf_replace (long al_row, string as_colname, string as_replacewith)
public subroutine nf_setrequestor (u_base_dw adw_requestor)
public function integer nf_getobjects (ref string as_objlist[], string as_objtype, string as_band, boolean ab_visibleonly)
protected function integer nf_findstartandendrows (ref long al_startrow, ref long al_endrow)
end prototypes

event ue_findnext;//////////////////////////////////////////////////////////////////////////////
//
//	Event: 			ue_findnext
//
//	Arguments:
//	anv_findattrib Reference of search attributes
//
//	Returns:  		long
//						search results
//
//	Description:	This event is called to start the search
//						using the current attribute settings.
//
//////////////////////////////////////////////////////////////////////////////

long		ll_row

If ib_ongoingfind Then
	//Determine if this continues to be an Ongoing Find. 
	ib_ongoingfind = nf_IsOnGoingFind(anv_findattr)
End If

//Stored in the service the passed arguments.
inv_findattrib = anv_findattr

//Build the find expression.
is_findexpression = nf_BuildFindExpression(inv_findattrib.is_find, &
	inv_findattrib.is_lookdata[inv_findattrib.ii_lookindex])
If is_findexpression = '!' Then Return 0

//Perform the find operation.
ll_row = nf_Find ()

//Return find row.
return ll_row
end event

event ue_replace;//////////////////////////////////////////////////////////////////////////////
//
//	Event:  				ue_replace
//
//	Arguments:
//	anv_findattrib		search attributes.
//
//	Returns:  			integer
//							 The find results..
//							 0 = No row was found.
//							-1 = Error;
//									No FindNext was performed.
//							-2 = The replace operation was not valid because the
//									ReplaceWith value was not of the correct type;
//									or the field is protected;
//									No FindNext was performed.
//
//	Description:  		Search and replace using the passed in settings.
//
//////////////////////////////////////////////////////////////////////////////
long  	ll_row
long		ll_currrow
string	ls_colname
integer	li_rc
string	ls_coleditstatus

If ib_ongoingfind Then
	//Determine if this continues to be an Ongoing Find. 
	ib_ongoingfind = nf_IsOnGoingFind(anv_findattr)
End If

//Make a copy of passed argument.
inv_findattrib = anv_findattr

//Get the current row.
ll_currrow = idw_requestor.GetRow()

//Get the current column.
ls_colname = inv_findattrib.is_lookdata[inv_findattrib.ii_lookindex]

//Get the column edit status.
ls_coleditstatus = nf_GetColumnEditStatus(ls_colname, ll_currrow)

//Determine if the current row/column can be replaced.
If ls_coleditstatus = 'editable' Then

	//Build find expression.
	is_findexpression = nf_BuildFindExpression(inv_findattrib.is_find, ls_colname)
	If is_findexpression = '!' Then Return 0

	If idw_requestor.GetColumnName() = ls_colname Then
		//Attempt to replace the selected text.
		li_rc = nf_replace(ll_currrow, ls_colname, inv_findattrib.is_replacewith)
		If li_rc = 1 Then
			/* Replace was successful. */
			ib_replaced = True
		ElseIf li_rc = 0 Then
			/* Replacing of text was not appropriate. Not an error*/
		ElseIf li_rc <= -1 Then
			/* An unappropriate action was selected */
			Return li_rc
		End If
	End If
	
End If

//Perform the find operation.
ll_row = nf_Find()

return ll_row

end event

event ue_replaceall;//////////////////////////////////////////////////////////////////////////////
//
//	Event:  				ue_replaceall
//
//	Arguments:
//	anv_findattrib		nvo-structure - reference of search attributes
//
//	Returns:  			none
//	
//
//	Description:  		This event is called dynimaclly to search and replace all
//							instances of the text using the current settings.
//
//////////////////////////////////////////////////////////////////////////////
integer	li_counter=0
integer	li_rc

SetPointer(HourGlass!)
			
idw_requestor.SetRedraw(False)			
DO
	ib_replaced = False
	li_rc = this.Event ue_Replace(anv_findattr)
	If ib_replaced Then li_counter ++
LOOP UNTIL li_rc <= 0
idw_requestor.SetRedraw(True)

return li_counter

end event

event ue_finddlg;//////////////////////////////////////////////////////////////////////////////
//
//	Event:  			ue_finddlg
//
//	(Arguments:		None)
//
//	(Returns:  		None)
//
//	Description:  	Begin a search based on current settings
//						of the nvo-structure inv_findattrib with the find dialog. 
//
//////////////////////////////////////////////////////////////////////////////
integer li_rc

//Set this object as the requestor.
inv_findattrib.ipo_requestor = this

//Look Options are required for Column Searches.
inv_findattrib.ib_lookvisible = True
inv_findattrib.ib_lookenabled = True

//Whole word capabilities are not supported on column searches.
inv_findattrib.ib_wholewordvisible =False
inv_findattrib.ib_wholewordenabled =False
inv_findattrib.ib_wholeword = False

//Build the Column List for the Find operation.
//The Find list is build everytime to catch for possible manipulation of the
//columns.  i.e., columns changing between visible and not visible.
li_rc = nf_BuildColumnNames (False)

//Open find window.
OpenWithParm(w_find, inv_findattrib)

end event

event ue_replacedlg;//////////////////////////////////////////////////////////////////////////////
//
//	Event:  			ue_replacedlg
//
//	(Arguments:		None)
//
//	(Returns:  		None)
//
//	Description:	Call the replace dialog window.
//
//////////////////////////////////////////////////////////////////////////////

integer li_rc

//Set this object as the requestor.
inv_findattrib.ipo_requestor = this

//Look Options are required for Column Searches.
inv_findattrib.ib_lookvisible = True
inv_findattrib.ib_lookenabled = True

//Whole word capabilities are not supported on column searches.
inv_findattrib.ib_wholewordvisible =False
inv_findattrib.ib_wholewordenabled =False
inv_findattrib.ib_wholeword = False

//Build the Column List for the Replace operation.
//The Replace list is build everytime to catch for possible manipulation of the
//columns between visible and not visible.
li_rc = nf_BuildColumnNames (True)

//Open Replace window.
OpenWithParm(w_replace, inv_findattrib)
end event

protected function string nf_buildfindexpression (string as_find, string as_column);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_BuildFindExpression
//
//	Access:  		protected
//
//	Arguments: 		
//	as_find			String being searched for.
//	as_column		The column to search in.
//
//	Returns:  		string
//						String expression to use in search.
//						'!' if an error is encountered.
//
//	Description:  	This function build the string that is passed to the 
//						find function.
//
//////////////////////////////////////////////////////////////////////////////
//
//	Revision History
//
//	Version
//	5.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
string	ls_findexp
string	ls_coltype
string	ls_editstyle

//Check arguments.
If IsNull(as_find) or Len(as_find)=0 or &
	IsNull(as_column) or Len(Trim(as_column))=0 Then
	Return '!'
End If

//Check required.
If Not IsValid(idw_requestor) Then
	Return '!'
end If

//Get the column type and edit style.
ls_coltype = idw_requestor.describe(as_column+".coltype")
ls_editstyle = idw_requestor.Describe(as_column+".Edit.Style")

//Create the string according to the column type and edit style.
If ls_editstyle='dddw' or ls_editstyle='ddlb' Then
	//Add the MatchCase attributes.
	if inv_findattrib.ib_matchcase THEN
		ls_findexp = "Pos(LookUpDisplay(" +as_column+ "),'" + as_find + "') > 0"
	else
		ls_findexp = "Pos(Lower(LookUpDisplay(" +as_column+ ")),'" +Lower(as_find)+ "') > 0"
	end if
Else
	//Process according to the column type.
	Choose Case Left(Lower(ls_coltype),4)
		Case 'numb', 'long', 'inte', 'deci'
			If IsNumber(as_find) Then
				ls_findexp = as_column+ " = " + as_find
			Else
				ls_findexp = "!"
			End If
		Case 'date'
			If IsDate(as_find) Then
				ls_findexp = as_column+ " = Date ('" + as_find + "')"
			Else
				ls_findexp = "!"
			End If			
		Case 'time'	
			If IsTime(as_find) Then
				ls_findexp = as_column+ " = Time ('" + as_find + "')"
			Else
				ls_findexp = "!"
			End If				
		Case Else
			//Add the MatchCase attributes.
			if inv_findattrib.ib_matchcase THEN
				ls_findexp = "Pos(" +as_column+ ",'" + as_find + "') > 0"
			else
				ls_findexp = "Pos(Lower(" +as_column+ "),'" +Lower(as_find)+ "') > 0"
			end if
	End Choose
End If

Return ls_findexp

end function

protected function integer nf_buildcolumnnames (boolean ab_replacelist);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_BuildColumnNames
//
//	Access:  		protected
//
//	Arguments:		
//	ab_replacelist	Replace list of column names flag.
//
//	Returns:  		Integer
//						Then number of columns
//						-1 if an error is encountered.
//
//	Description: 	Populate the array with the column names to use for the
//						find or find/replace dialog window.  The list is different
//						depending if it will be used with the find dialog, or if
//						it will be used with the find/replace dialog.
//						This will be used with an index that is set 
//						when the user selects the header label name from the
//						dialog window.
//
//////////////////////////////////////////////////////////////////////////////

integer	li_objects
integer	li_count=0
integer	li_i
string	ls_headernm
string	ls_obj_column[]
string	ls_oldlookdata
string	ls_oldfind
string	ls_oldreplacewith
string	ls_editstyle

//Check required.
If Not IsValid(idw_requestor) Then
	Return -1
end If

//Store the previous current look data (if any)
If inv_findattrib.ii_lookindex > 0 And &
	UpperBound(inv_findattrib.is_lookdata) >= inv_findattrib.ii_lookindex Then
	ls_oldlookdata = inv_findattrib.is_lookdata[inv_findattrib.ii_lookindex]
	ls_oldfind = inv_findattrib.is_find
	ls_oldreplacewith = inv_findattrib.is_replacewith
End If

//Reset the current index, find, and replace information.
inv_findattrib.ii_lookindex = 0
inv_findattrib.is_find = ''
inv_findattrib.is_replacewith = ''
inv_findattrib.is_lookdata = ls_obj_column[]
inv_findattrib.is_lookdisplay = ls_obj_column[]

//////////////////////////////////////////////////////////////////////////////
// populate string array for the user to select column from.
//////////////////////////////////////////////////////////////////////////////

//First get the Visible column names to use in search.
li_objects = nf_GetObjects ( ls_obj_column, "column", "*", True )
	
//Get a list of all text objects associated with the column labels
FOR li_i=1 TO li_objects
	//Determine if the column is of unwanted type.
	ls_editstyle = idw_requestor.Describe(ls_obj_column[li_i]+".Edit.Style")
	If ls_editstyle='checkbox' or ls_editstyle='radiobuttons' Then
		Continue
	End If
	
	//If the list is being costructed for Replace then do not show 
	//DropDownDataWindow, DropDownListBoxes, Tab=0 Columns, or Display 
	//only columns.   Protected columns will still be 
	//shown since it could be a row dependendent.
	If ab_replacelist Then
		If ls_editstyle='dddw' or &
			ls_editstyle='ddlb' or &
			idw_requestor.Describe(ls_obj_column[li_i]+".TabSequence") = "0" or &
			idw_requestor.Describe(ls_obj_column[li_i]+".Edit.DisplayOnly") = "yes" Then
			Continue
		End If
	End If
	
	//Add the column name and column label to the array	
	li_count ++
	ls_headernm= nf_GetHeaderName ( ls_obj_column[li_i] )
	inv_findattrib.is_lookdata[li_count] = ls_obj_column[li_i]
	inv_findattrib.is_lookdisplay[li_count] = ls_headernm

	//Reset the Index value (if any)
	If ls_oldlookdata = inv_findattrib.is_lookdata[li_count] Then
		inv_findattrib.ii_lookindex = li_count
		inv_findattrib.is_find = ls_oldfind
		inv_findattrib.is_replacewith = ls_oldreplacewith	
	End If
NEXT

Return li_count
end function

protected function string nf_getcolumneditstatus (string as_column, long al_row);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_GetColumnEditStatus
//
//	Access:  		protected
//
//	Arguments:		
//	as_column		The column to test.
//	al_row			The row number to test.
//
//	Returns:  		String
//						"editable" The column/row is editable.
//						"displayonly" The column/row is display only
//						"protected" The column/row is not editable.
//						"!" if an error is encountered.
//
//	Description: 	Determine the Edit Status of the passed column/row 
//						combination.
//
//////////////////////////////////////////////////////////////////////////////

constant string lcs_editable = 'editable'
constant string lcs_readonly = 'readonly'
constant string lcs_protected= 'protected'
string	ls_columnprotect
string	ls_evaluate, ls_evaluateresult

//Check arguments
If IsNull(as_column) or Len(Trim(as_column))=0 or &
	IsNull(al_row) or al_row <=0 Then
	Return '!'
End If

//////////////////////////////////////////////////////////////////////////////
// If the TabSequence for the column is not Greater than Zero then 
//	the field is protected.
//////////////////////////////////////////////////////////////////////////////
If idw_requestor.Describe(as_column+".TabSequence") = '0' Then
	Return lcs_protected
End If

//////////////////////////////////////////////////////////////////////////////
// If the Protected attribute for the column/row evaluates to One then
// the field is protected.
//////////////////////////////////////////////////////////////////////////////
ls_columnprotect = idw_requestor.Describe(as_column+".Protect")

//Get rid of extra quotes.
If Left(ls_columnprotect, 1) = "'" or Left(ls_columnprotect, 1) = '"' Then
	ls_columnprotect = Mid(ls_columnprotect, 2)
	If Right(ls_columnprotect, 1) = "'" or Right(ls_columnprotect, 1) = '"' Then
		ls_columnprotect = Mid(ls_columnprotect, 1, Len(ls_columnprotect)-1 )
	End If
End If

If IsNumber (ls_columnprotect) Then
	If Long(ls_columnprotect) <> 0 Then
		Return lcs_protected
	End If
Else
	//Get the expression and evaluate it.
	ls_evaluate = 'Evaluate ("'+Mid(ls_columnprotect, 3)+'",'+string(al_row)+')'
	ls_evaluateresult = idw_requestor.Describe(ls_evaluate)
	If IsNumber(ls_evaluateresult) Then
		If Long(ls_evaluateresult) <> 0 Then
			Return lcs_protected
		End If
	Else
		Return lcs_protected
	End If
End If

//////////////////////////////////////////////////////////////////////////////
// If the Edit.DisplayOnly for the column is Yes then the field is ReadOnly.
//////////////////////////////////////////////////////////////////////////////
If idw_requestor.Describe(as_column+".Edit.DisplayOnly") = "yes" Then
	Return lcs_readonly
End If

//////////////////////////////////////////////////////////////////////////////
// If None of the above then the field is editable.
//////////////////////////////////////////////////////////////////////////////
Return lcs_editable
end function

protected function long nf_find (string as_column, string as_find, long al_startrow, long al_endrow);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_Find
//
//	Access:  		public
//
//	Arguments: 	
//	as_column		Column name to search.
//	as_find			Text to search for.
//	al_startrow		Row number to start search.
//	al_endrow		Row number to end search.
//
//	Returns:  		long
//						Row number where text was found, or
//						0 if not found
//						-1 if an error is encountered.
//
//	Description: 	Search on a specific column, for a specific value, with a
//						starting row, and an ending row. 
//
//////////////////////////////////////////////////////////////////////////////
string   ls_findexp
long		ll_rowfound

//Check arguments
If IsNull(as_column) or Len(Trim(as_column))=0 or &
	IsNull(as_find) or Len(Trim(as_find))=0 or &
	IsNull(al_startrow) or al_startrow < 0 or &
	IsNull(al_endrow)	or al_endrow < 0 Then
	Return -1
End If

//Check required.
If Not IsValid(idw_requestor) Then
	Return -1
end If

//Build the find expression to search the dw.
ls_findexp = nf_BuildFindExpression(as_find, as_column)
If ls_findexp = "!" Then Return -1

//Get the starting row for which the search text was found.
ll_rowfound = idw_requestor.Find (ls_findexp, al_startrow, al_endrow)

//Return search results.
return ll_rowfound


end function

protected function long nf_find ();//////////////////////////////////////////////////////////////////////////////
//
//	Function: 		nf_Find
//
//	Access:  		protected
//
//	Arguments: 		none
//
//	Returns:  		long
//						row number found
//						 0 - not found (end of search)
//
//	Description:  	Searches datawindow using current settings and returns the
//						row number.
//
//////////////////////////////////////////////////////////////////////////////

string	ls_colname
string	ls_coleditstatus
long		ll_startrow, ll_endrow
integer	li_rowfound
integer	li_rc

//Check required.
If Not IsValid(idw_requestor) Then
	Return -1
end If

//Get the current column.
ls_colname = inv_findattrib.is_lookdata[inv_findattrib.ii_lookindex]

//Determine the start and end rows for the find.
li_rc = nf_FindStartAndEndRows (ll_startrow, ll_endrow)
If li_rc <= 0 Then
	ib_ongoingfind	= False
	return 0
End If

//Find the row that meets the expression and the Starting and Ending rows.
li_rowfound = idw_requestor.Find (is_findexpression, ll_startrow, ll_endrow)
If IsNull(li_rowfound) or li_rowfound <= 0 Then
	ib_ongoingfind	= False
	return 0
End If

//The find was successful.
ib_ongoingfind = True

//Set focus on the row/column just found.
idw_requestor.ScrollToRow (li_rowfound)
idw_requestor.SetColumn(ls_colname)
idw_requestor.SetFocus()

//Only select the text if the column can receive focus.
ls_coleditstatus = nf_GetColumnEditStatus(ls_colname, li_rowfound)
If ls_coleditstatus = 'editable' or ls_coleditstatus='readonly' Then
	nf_SelectText(ls_colname, li_rowfound, inv_findattrib.ib_matchcase, inv_findattrib.is_find)
End If

//Set last found row and column.
il_lastfindrow = li_rowfound
is_lastfindcolumn = idw_requestor.GetColumnName()

//Return the found row.
return li_rowfound

end function

protected function boolean nf_compareattrib (u_findattrib anv_findattrib);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  			nf_CompareAttrib
//
//	Access:  			protected
//
//	Arguments: 
//	anv_findattrib		nvo structure to compare
//
//
//	Returns: 			boolean 
//							true - the structures are the same
//							false - the structures are not the same
//
//	Description: 		This function is called to replace text found in search.
//
//////////////////////////////////////////////////////////////////////////////

//If any of the attributes do not match, return false
if anv_findattrib.is_find <> inv_findattrib.is_find then return false
if anv_findattrib.is_replacewith <> inv_findattrib.is_replacewith then return false
if anv_findattrib.ib_wholeword <> inv_findattrib.ib_wholeword then return false
if anv_findattrib.ib_matchcase <> inv_findattrib.ib_matchcase then return false
if anv_findattrib.is_direction <> inv_findattrib.is_direction then return false

//The structures match.
return true


end function

protected function integer nf_selecttext (string as_colname, long al_row, boolean ab_matchcase, string as_find);//////////////////////////////////////////////////////////////////////////////
//
//	Function: 		nf_SelectText
//
//	Access:  		protected
//
//	Arguments: 		
//	as_colname		The column name.
//	al_row			The row.
//	ab_matchcase	Match case flag.
//	as_find			The find string.
//
//	Returns:  		Integer
//						1 if it succeeds.
//						0 Selection of text is not appropriate.
//
//	Description:  	Selects the appropriate text on the column/row.  Function 
//						takes into consideration the column may be a dropdowndatawindow,
//						dropdownlistbox, or a string with an editmask.
//
//////////////////////////////////////////////////////////////////////////////

string	ls_find, ls_maskedfind
integer	li_find_startpos, li_find_selectedtextlen
integer	li_maskedfind_textlen, li_maskedfind_startpos, li_maskedfind_selectedtextlen
integer	li_i, li_count
string	ls_coltype, ls_editstyle

//Check required.
If Not IsValid(idw_requestor) Then
	Return -1
end If

//////////////////////////////////////////////////////////////////////////////
//Get the current column, columntype, and editstyle
//////////////////////////////////////////////////////////////////////////////
ls_coltype = idw_requestor.describe(as_colname+".coltype")
ls_editstyle = idw_requestor.Describe(as_colname+".Edit.Style")

//////////////////////////////////////////////////////////////////////////////
// If the editstyle of the column is of dddw or ddlb use the LookUpDisplay 
// value.
//////////////////////////////////////////////////////////////////////////////
If ls_editstyle='dddw' or ls_editstyle='ddlb' Then
	If idw_requestor.Describe(as_colname+".ddlb.AllowEdit") = 'yes' or &
		idw_requestor.Describe(as_colname+".dddw.AllowEdit") = 'yes' Then
		//Get the LookUpDisplay value.
		ls_find = idw_requestor.Describe( &
			"Evaluate('LookUpDisplay("+as_colname+")',"+string(al_row)+")")
		//Determine the "LookUpDisplay" starting position and its length.
		If ab_matchcase Then
			li_find_startpos = Pos (ls_find, as_find)
		Else			
			li_find_startpos = Pos (Lower(ls_find), Lower(as_find))
		End If
		li_find_selectedtextlen = Len(as_find)			
		//Select the appropriate portion of the LookUpDisplay value.
		idw_requestor.SelectText(li_find_startpos, li_find_selectedtextlen)		
		Return 1
	End If
End If

//////////////////////////////////////////////////////////////////////////////
// If the column is of character then only select the matching characters 
// within the string.
// Determine the simple find case for a string case.. 
//////////////////////////////////////////////////////////////////////////////
If Left(Lower(ls_coltype),4) = 'char' Then
	//Get complete "Find" string.
	ls_find = idw_requestor.GetItemString(al_row, as_colname)

	//Determine the "Find" starting position and its length.
	If ab_matchcase Then
		li_find_startpos = Pos (ls_find, as_find)
	Else			
		li_find_startpos = Pos (Lower(ls_find), Lower(as_find))
	End If
	li_find_selectedtextlen = Len(as_find)
End If

//////////////////////////////////////////////////////////////////////////////
//	Process strings that have NO editmask
//////////////////////////////////////////////////////////////////////////////
//If a string find, then only select the matching text.
If Left(Lower(ls_coltype),4) = 'char' Then
	If ls_editstyle<>"editmask" Then
		//Select the appropriate text
		idw_requestor.SelectText(li_find_startpos, li_find_selectedtextlen)
		Return 1
	End If
End If

//////////////////////////////////////////////////////////////////////////////
//	Process strings that have an editmask.
//////////////////////////////////////////////////////////////////////////////
//If a string find, then only select the matching text.
If Left(Lower(ls_coltype),4) = 'char' Then
	If ls_editstyle="editmask" Then
		//Get "Masked Find"  and its appropriate length.
		ls_maskedfind = nf_GetItem(al_row, as_colname)
		li_maskedfind_textlen = Len(ls_maskedfind)

		//Determine the "Masked Find" starting position and its length.
		li_count = 1
		For li_i = 1 to li_maskedfind_textlen
			If Mid(ls_find, li_count, 1) =  Mid(ls_maskedfind, li_i, 1) Then
				If li_count = li_find_startpos Then
					li_maskedfind_startpos = li_i
				End If
				If li_count = li_find_startpos+li_find_selectedtextlen -1 Then
					li_maskedfind_selectedtextlen = li_i - (li_maskedfind_startpos -1)
					Exit
				End If
				li_count ++							
			End If
		Next

		//Select the appropriate editmasked text.
		idw_requestor.SelectText(li_maskedfind_startpos, li_maskedfind_selectedtextlen)
		Return 1
	End If
End If	

Return 0
end function

public function integer nf_initialize (u_findattrib anv_findattrib);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_initialize
//
//	Access:  		public
//
//	Arguments:		
//	anv_findattrib	NonVisual holding the desired initial values.
//
//	Returns:  		Integer
//
//	Description:  	Initialize the options for this service.
//
//////////////////////////////////////////////////////////////////////////////

//Check arguments.
If Not IsValid(anv_findattrib) Then
	Return -1
End If

inv_findattrib.is_find = anv_findattrib.is_find
inv_findattrib.is_replacewith  = anv_findattrib.is_replacewith

//Whole word capabilities are not supported on column searches.
inv_findattrib.ib_wholewordvisible =False
inv_findattrib.ib_wholewordenabled =False
inv_findattrib.ib_wholeword = False

inv_findattrib.ib_matchcasevisible = anv_findattrib.ib_matchcasevisible
inv_findattrib.ib_matchcaseenabled = anv_findattrib.ib_matchcaseenabled
inv_findattrib.ib_matchcase = anv_findattrib.ib_matchcase

inv_findattrib.ib_directionvisible = anv_findattrib.ib_directionvisible
inv_findattrib.ib_directionenabled = anv_findattrib.ib_directionenabled
inv_findattrib.is_direction = anv_findattrib.is_direction

//Look Options are required for Column Searches.
inv_findattrib.ib_lookvisible = True
inv_findattrib.ib_lookenabled = True

Return 1

end function

public function boolean nf_isongoingfind (u_findattrib anv_findattrib);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_IsOnGoingFind
//
//	Access:  		protected
//
//	Arguments:		
//	anv_findattrib	Copy of structure holding all find/replace flags.
//
//	Returns:  		Boolean
//						True - This is an ongoing find.
//						False - This is not an ongoing find.
//						Null - if an error occurrs.
//
//	Description:  	Determines if the current find operation is a brand new
//						Find or a FindNext operation.
//
//////////////////////////////////////////////////////////////////////////////

//Check required.
If Not IsValid(idw_requestor) Then
	boolean lb_null
	SetNull (lb_null)
	Return lb_null
end If

//Check if find/replace criteria was changed on visual object.
If IsValid(anv_findattrib) Then
	if NOT nf_CompareAttrib(anv_findattrib) THEN
		Return False
	end if
End If

//Check that the current row is the same as the last found row.
If il_lastfindrow <> idw_requestor.GetRow() Then
	Return False
End If

//Check that the current column is the same as the last found column.
If idw_requestor.GetColumnName() <> is_lastfindcolumn Then
	Return False
End If

Return True
end function

public function string nf_getheadername (string as_column);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_GetHeaderName (FORMAT 1) 
//
//	Access:    		Public
//
//	Arguments:
//   as_column   	A datawindow columnname
//
//	Returns:  		String
//   					The formatted column header for the column specified
//
//	Description:  	Extracts a formatted (underscores, carraige return/line
//					  	feeds and quotes removed) column header.
//					  	If no column header found, then the column name is
//					  	formatted (no underscores and Word Capped).
//
//  	*NOTE: 	Use this format when column header text has the default 
//	  				suffix of "_t"
//
//////////////////////////////////////////////////////////////////////////////

Return nf_GetHeaderName ( as_column, "_t" ) 
end function

public function string nf_getheadername (string as_column, string as_suffix);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  nf_GetHeaderName 
//	Access:    Public
//
//	Arguments:
//   as_column   A datawindow columnname
//	  as_suffix   The suffix used on column header text
//
//	Returns:  String
//	  The formatted column header for the column specified
//
//	Description:  Extracts a formatted (underscores, carriage return/line
//					  feeds and quotes removed) column header.
//					  If no column header found, then the column name is
//					  formatted (no underscores and Word Capped).
//
//  *NOTE: Use this format when column header text does NOT
//	  use the default suffix of "_t".
//
//////////////////////////////////////////////////////////////////////////////
string ls_colhead
u_string_functions	lu_string

//Try using the column header.
ls_colhead = idw_Requestor.Describe ( as_column + as_suffix + ".Text" )
If ls_colhead = "!" Then
	//No valid column header, use column name.
	ls_colhead = as_column
End If	

//Remove undesired characters.
ls_colhead = lu_string.nf_GlobalReplace ( ls_colhead, "~r~n", " " ) 
ls_colhead = lu_string.nf_GlobalReplace ( ls_colhead, "~t", " " ) 
ls_colhead = lu_string.nf_GlobalReplace ( ls_colhead, "~r", " " ) 
ls_colhead = lu_string.nf_GlobalReplace ( ls_colhead, "~n", " " ) 
ls_colhead = lu_string.nf_GlobalReplace ( ls_colhead, "_", " " ) 
ls_colhead = lu_string.nf_GlobalReplace ( ls_colhead, "~"", "" ) 
ls_colhead = lu_string.nf_GlobalReplace ( ls_colhead, "~'", "" ) 
ls_colhead = lu_string.nf_GlobalReplace ( ls_colhead, "~~", "" )

//WordCap string.
ls_colhead = idw_Requestor.Describe ( "Evaluate('WordCap(~"" + ls_colhead + "~")',0)" )

Return ls_colhead
end function

public function string nf_getitem (long al_row, string as_column);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  nf_GetItem 
//
//	Access:    Public
//
//	Arguments:
//   al_row			   : The row reference
//   as_column    	: The column name reference
//
//	Returns:  String
//	  The formatted string value of the item
//
//	Description:  Returns the formatted (including formats, editmasks and display
//					  values) text of any column on a datawindow, regardless of the 
//					  column's datatype.  
//
//////////////////////////////////////////////////////////////////////////////
Return nf_GetItem ( al_row, as_column, Primary!, FALSE )
end function

public function string nf_getitem (long al_row, string as_column, dwbuffer adw_buffer, boolean ab_orig_value);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  nf_GetItem
//
//	Access:    Public
//
//	Arguments:
//   al_row			   : The row reference
//   as_column    	: The column name reference
//   adw_buffer   	: The dw buffer from which to get the column's data value.
//   ab_orig_value	: When True, returns the original values that were 
//							  retrieved from the database.
//
//	Returns:  String
//	  The formatted string value of the item
//
//	Description:  Returns the formatted (including formats, editmasks and display
//					  values) text of any column on a datawindow, regardless of the 
//					  column's datatype.  
//
//////////////////////////////////////////////////////////////////////////////
string ls_col_format, ls_col_mask, ls_string_format, ls_string, ls_compute_exp
boolean lb_editmask_used=False
u_string_functions	lu_string


ls_col_format = idw_Requestor.Describe ( as_column + ".format" )
ls_col_mask   = idw_Requestor.Describe ( as_column + ".editmask.mask") 

IF ls_col_mask = "!" or ls_col_mask = "?" THEN
	ls_string_format = ls_col_format
ELSE 
	ls_string_format = ls_col_mask
	lb_editmask_used = TRUE
END IF 
 
IF ls_string_format = "!" or ls_string_format = "?" THEN 
	ls_string_format = ""
END IF  

/*  Determine the datatype of the column and then call the appropriate 
	 GetItemxxx function and format the returned value */
CHOOSE CASE Lower ( Left ( idw_Requestor.Describe ( as_column + ".ColType" ) , 5 ) )

		CASE "char("				//  CHARACTER DATATYPE
			IF lb_editmask_used = TRUE THEN 
				/*  Need to replace 'EditMask' characters with 'Format' characters */
				ls_string_format = lu_string.nf_GlobalReplace ( ls_string_format, "^", "@" ) //Lowercase
				ls_string_format = lu_string.nf_GlobalReplace ( ls_string_format, "!", "@")	//Uppercase
				ls_string_format = lu_string.nf_GlobalReplace ( ls_string_format, "#", "@" ) //Number
				ls_string_format = lu_string.nf_GlobalReplace ( ls_string_format, "a", "@" ) //Aplhanumeric
				ls_string_format = lu_string.nf_GlobalReplace ( ls_string_format, "x", "@" ) //Any Character
			END IF 
			ls_string = idw_Requestor.GetItemString ( al_row, as_column, adw_buffer, ab_orig_value ) 
			ls_string = String ( ls_string, ls_string_format ) 
	
		CASE "date"					//  DATE DATATYPE
			date ld_date
			ld_date = idw_Requestor.GetItemDate ( al_row, as_column, adw_buffer, ab_orig_value ) 
			if Len (ls_string_format) > 0 then
				ls_string = String ( ld_date, ls_string_format ) 
			else
				ls_string = String (ld_date)
			end if

		CASE "datet"				//  DATETIME DATATYPE
			datetime ldtm_datetime
			ldtm_datetime = idw_Requestor.GetItemDateTime ( al_row, as_column, adw_buffer, ab_orig_value ) 
			if Len (ls_string_format) > 0 then
				ls_string = String ( ldtm_datetime, ls_string_format ) 
			else
				ls_string = String (ldtm_datetime)
			end if

		CASE "decim"				//  DECIMAL DATATYPE
			decimal ldec_decimal
			ldec_decimal = idw_Requestor.GetItemDecimal ( al_row, as_column, adw_buffer, ab_orig_value ) 
			if Len (ls_string_format) > 0 then
				ls_string = String ( ldec_decimal, ls_string_format ) 
			else
				ls_string = String (ldec_decimal)
			end if	
	
		CASE "numbe", "long", "ulong", "real"				//  NUMBER DATATYPE	
			long ll_long
			ll_long = idw_Requestor.GetItemNumber ( al_row, as_column, adw_buffer, ab_orig_value ) 
			if Len (ls_string_format) > 0 then
				ls_string = String ( ll_long, ls_string_format ) 
			else
				ls_string = String (ll_long)
			end if
	
		CASE "time", "times"		//  TIME DATATYPE
			time ltm_time
			ltm_time = idw_Requestor.GetItemTime ( al_row, as_column, adw_buffer, ab_orig_value ) 
			if Len (ls_string_format) > 0 then
				ls_string = String ( ltm_time, ls_string_format ) 
			else
				ls_string = String (ltm_time)
			end if

		CASE ELSE 					//  MUST BE A COMPUTED COLUMN
			IF idw_Requestor.Describe ( as_column + ".Type" ) = "compute" THEN 
				ls_compute_exp = idw_Requestor.Describe ( as_column + ".Expression" )
				ls_string = idw_Requestor.Describe( "Evaluate('" + ls_compute_exp + "', " + string (al_row) + ")" )
				ls_string = String (ls_string, ls_string_format ) 
				Return ls_string
			ELSE
				Return ""
			END IF 

END CHOOSE

IF adw_buffer = Primary! THEN 
	IF Lower ( idw_Requestor.Describe ( as_column + ".Edit.CodeTable" ) ) = "yes"	& 
	OR Lower ( idw_Requestor.Describe ( as_column + ".EditMask.CodeTable" ) ) = "yes"	& 
	OR	idw_Requestor.Describe ( as_column + ".RadioButtons.Columns" ) <> "0"		&
	OR	idw_Requestor.Describe ( as_column + ".DDDW.DataColumn" ) > "?" THEN
		ls_string = idw_Requestor.Describe ( &
			"Evaluate('LookUpDisplay(" + as_column + ")', " + String(al_row) + ")" ) 
	ELSEIF idw_Requestor.Describe ( as_column + ".CheckBox.On" ) <> "?"	THEN
		ls_string = ls_string + "~t" + idw_Requestor.Describe ( &
			"Evaluate('LookUpDisplay(" + as_column + ")', " + String(al_row) + ")" ) 
	END IF
END IF

Return ls_string
end function

public function integer nf_replace (long al_row, string as_colname, string as_replacewith);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_Replace
//
//	Access:  		public
//
//	Arguments: 	
//	al_row			The row number for the value that may be replaced.
//	as_colname		The column name for the value that may be replaced.
//	as_replacewith	The value to replace with.
//
//	Returns:  		integer
//						1 if it succeeds and$$HEX1$$a000$$ENDHEX$$-1 if an error occurs. 
//
//	Description: 	Replace on a specific row and column, with a specific value.
//
//////////////////////////////////////////////////////////////////////////////
string 	ls_selectedtext
string	ls_coltype
string	ls_find
integer	li_rc=0
integer	li_count, li_i
boolean 	lb_replacechars=False
integer	li_find_startpos
string	ls_editstyle
string	ls_maskedfind, ls_maskedfind_selectedtext
integer	li_maskedfind_textlen, li_maskedfind_startpos, li_maskedfind_selectedtextlen

//Get the current column, columntype, and editstyle
ls_coltype = idw_requestor.describe(as_colname+".ColType")
ls_editstyle = idw_requestor.Describe(as_colname+".Edit.Style")

//Get the currently selected text.
ls_selectedtext = idw_requestor.SelectedText()

//Process according to the column type.
//Check that the ReplaceWith value is appropriate for the column type.
Choose Case Left(Lower(ls_coltype),4)
	Case 'numb', 'long', 'inte'
		If IsNumber(ls_selectedtext) and IsNumber(inv_findattrib.is_find) Then
			If Long(ls_selectedtext) = Long(inv_findattrib.is_find) Then
				If IsNumber(inv_findattrib.is_replacewith) Then
					li_rc = idw_requestor.SetItem(al_row, as_colname, &
					 			Long(inv_findattrib.is_replacewith))
				Else
					MessageBox ('Replace', 'The Replace With value is not a valid number.')
					li_rc = -2
				End If										
			End If
		End If
		Return li_rc /* numb, long, inte */
		
	Case 'ulon'
		If IsNumber(ls_selectedtext) and IsNumber(inv_findattrib.is_find) Then
			If Long(ls_selectedtext) = Long(inv_findattrib.is_find) Then
				If IsNumber(inv_findattrib.is_replacewith) Then
					If Long(inv_findattrib.is_replacewith) > 0 Then
 						li_rc = idw_requestor.SetItem(al_row, as_colname, &
						 			Long(inv_findattrib.is_replacewith))
					Else
						MessageBox ('Replace', 'The Replace With value is not a valid number.')
						li_rc = -2
					End If														
				Else
					MessageBox ('Replace', 'The Replace With value is not a valid number.')
					li_rc = -2
				End If										
			End If
		End If	
		Return li_rc /* ulon */
		
	Case 'deci'
		If IsNumber(ls_selectedtext) and IsNumber(inv_findattrib.is_find) Then
			If Dec(ls_selectedtext) = Dec(inv_findattrib.is_find) Then
				If IsNumber(inv_findattrib.is_replacewith) Then
					li_rc = idw_requestor.SetItem(al_row, as_colname, &
								Dec(inv_findattrib.is_replacewith))
				Else
					MessageBox ('Replace', 'The Replace With value is not a valid number.')
					li_rc = -2
				End If									
			End If
		End If	
		Return li_rc /* Deci */
		
	Case 'date'
		If IsDate(ls_selectedtext) and IsDate(inv_findattrib.is_find) Then
			If Date(ls_selectedtext) = Date(inv_findattrib.is_find) Then
				If IsDate(inv_findattrib.is_replacewith) Then
					li_rc = idw_requestor.SetItem(al_row, as_colname, &
								Date(inv_findattrib.is_replacewith))
				Else
					MessageBox ('Replace', 'The Replace With value is not a valid date.')
					li_rc = -2
				End If								
			End If
		End If
		Return li_rc /* Date */
		
	Case 'time'	
		If IsTime(ls_selectedtext) and IsTime(inv_findattrib.is_find) Then
			If Time(ls_selectedtext) = Time(inv_findattrib.is_find) Then
				If IsTime(inv_findattrib.is_replacewith) Then
					li_rc = idw_requestor.SetItem(al_row, as_colname, &
								Time(inv_findattrib.is_replacewith))
				Else
					MessageBox ('Replace', 'The Replace With value is not a valid time.')
					li_rc = -2
				End If								
			End If
		End If	
		Return li_rc /* Time */		
		
	Case 'char' 
		If nf_Find(as_colname, inv_findattrib.is_find, al_row, al_row) = al_row Then
			//Get the entire string.
			ls_find = idw_requestor.GetItemString(al_row, as_colname)
			li_find_startpos = Pos(Lower(ls_find),Lower(inv_findattrib.is_find))

			//Handle edimask fields.
			If ls_editstyle='editmask' Then
				//Check if the selected text matches the the "Find string".
				//Get "Masked Find"  and its appropriate length.
//				ls_maskedfind = nf_GetItem(al_row, as_colname)
				li_maskedfind_textlen = Len(ls_maskedfind)
				li_maskedfind_startpos = Pos (ls_maskedfind, ls_selectedtext)
				li_maskedfind_selectedtextlen = Len(ls_selectedtext)
				
				li_count = 1
				For li_i = 1 to li_maskedfind_textlen
					If Mid(ls_find, li_count, 1) =  Mid(ls_maskedfind, li_i, 1) Then
						If li_i >= li_maskedfind_startpos And &
							li_i <= li_maskedfind_startpos + (li_maskedfind_selectedtextlen -1) Then
							ls_maskedfind_selectedtext = ls_maskedfind_selectedtext + Mid(ls_find, li_count, 1)
						ElseIf li_i > li_maskedfind_startpos + (li_maskedfind_selectedtextlen -1) Then
								Exit
						End If
						li_count ++							
					End If
				Next 
				
				//Check if the selected text matches the the "Find string".					
				If (inv_findattrib.ib_matchcase And &
						ls_maskedfind_selectedtext = inv_findattrib.is_find) Or &
					(Not inv_findattrib.ib_matchcase And &
						Lower(ls_maskedfind_selectedtext) = Lower(inv_findattrib.is_find)) Then
					lb_replacechars = True
				End If
			Else
				//Check if the selected text matches the the "Find string".
				If (inv_findattrib.ib_matchcase And &
						ls_selectedtext = inv_findattrib.is_find) Or &
					(Not inv_findattrib.ib_matchcase And &
						Lower(ls_selectedtext) = Lower(inv_findattrib.is_find)) Then
					lb_replacechars = True
				End If
			End If
				
			If lb_replacechars Then
				//Replace the "Find" characters with the "ReplaceWith" characters.
				ls_find = Replace(ls_find, li_find_startpos, Len(inv_findattrib.is_find), &
							inv_findattrib.is_replacewith)
				li_rc = idw_requestor.SetItem(al_row, as_colname, ls_find)
			End If
		End If
		Return li_rc /* Char */
		
	Case Else
		//Code should never reach this line.		
		Return -1
End Choose

//Code should never reach this line.
Return -1
end function

public subroutine nf_setrequestor (u_base_dw adw_requestor);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_SetRequestor
//
//	Access:    Public
//
//	Arguments:
//   adw_Requestor   The datawindow requesting the service
//
//	Returns:  None
//
//	Description:  Associates a datawindow control with a datawindow service NVO
//			        by setting the idw_Requestor instance variable.
//
//////////////////////////////////////////////////////////////////////////////
idw_Requestor = adw_Requestor
end subroutine

public function integer nf_getobjects (ref string as_objlist[], string as_objtype, string as_band, boolean ab_visibleonly);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_GetObjects 
//
//	Access:    		Public
//
//	Arguments:
//   as_objlist[]:	A string array to hold objects (passed by reference)
//   as_objtype:  	The type of objects to get (* for all, others defined
//							by the object .TYPE attribute)
//   as_band:  		The dw band to get objects from (* for all) 
//							Valid bands: header, detail, footer, summary
//							header.#, trailer.#
//   ab_visibleonly: TRUE  - get only the visible objects,
//							 FALSE - get visible and non-visible objects
//
//	Returns:  		Integer
//   					The number of objects in the array
//
//	Description:	The following function will parse the list of objects 
//						contained in the datawindow control associated with this service,
//						returning their names into a string array passed by reference, 
//						and returning the number of names in the array as the return value 
//						of the function.
//
//////////////////////////////////////////////////////////////////////////////
string	ls_ObjString, ls_ObjHolder
integer	li_Start, li_Tab, li_Count

li_Start=1
li_Count=0

/* Get the Object String */
ls_ObjString = idw_Requestor.Describe("Datawindow.Objects")

/* Get the first tab position. */
li_Tab =  Pos(ls_ObjString, "~t", li_Start)
Do While li_Tab > 0
	ls_ObjHolder = Mid(ls_ObjString, li_Start, (li_Tab - li_Start))

	// Determine if object is the right type and in the right band
	If (idw_Requestor.Describe(ls_ObjHolder + ".type") = as_ObjType Or as_ObjType = "*") And &
		(idw_Requestor.Describe(ls_ObjHolder + ".band") = as_Band Or as_Band = "*") And &
		(idw_Requestor.Describe(ls_ObjHolder + ".visible") = "1" Or Not ab_VisibleOnly) Then
			li_Count ++
			as_ObjList[li_Count] = ls_ObjHolder
	End if

	/* Get the next tab position. */
	li_Start = li_Tab + 1
	li_Tab =  Pos(ls_ObjString, "~t", li_Start)
Loop 

// Check the last object
ls_ObjHolder = Mid(ls_ObjString, li_Start, Len(ls_ObjString))

// Determine if object is the right type and in the right band
If (idw_Requestor.Describe(ls_ObjHolder + ".type") = as_ObjType or as_ObjType = "*") And &
	(idw_Requestor.Describe(ls_ObjHolder + ".band") = as_Band or as_Band = "*") And &
	(idw_Requestor.Describe(ls_ObjHolder + ".visible") = "1" Or Not ab_VisibleOnly) Then
		li_Count ++
		as_ObjList[li_Count] = ls_ObjHolder
End if

Return li_Count
end function

protected function integer nf_findstartandendrows (ref long al_startrow, ref long al_endrow);//////////////////////////////////////////////////////////////////////////////
//
//	Function: 		nf_FindStartAndEndRows
//
//	Access:  		protected
//
//	Arguments: 		
//	al_startrow		The row the find should start looking in. (by Reference)
//	al_endrow		The row the find will stop looking in.	(by Reference)
//
//	Returns:  		Integer
//						1 if it succeeds and$$HEX1$$a000$$ENDHEX$$-1 if an error occurs.
//						0 if the point of no more searches has been reached.
//
//	Description:  	Determine what the Starting row and the Ending row should
//						be for the Find statement.
//
//////////////////////////////////////////////////////////////////////////////

//Validate the direction attribute.
If Lower(inv_findattrib.is_direction) <> "up" And &
	Lower(inv_findattrib.is_direction) <> "down" Then
	inv_findattrib.is_direction = "Down"
End If

//Check required.
If Not IsValid(idw_requestor) Then
	Return -1
end If

//Get the current row.
al_startrow = idw_requestor.GetRow()

//Determine the start and end rows for the find.
CHOOSE CASE Lower(inv_findattrib.is_direction)
	CASE "up"
		al_endrow = 1
		If ib_ongoingfind Then 
			//For ongoing finds do not search on the current row.
			If al_startrow - 1 >= al_endrow Then
				al_startrow --
			Else
				Return 0
			End If
		End If
	CASE Else 
		// "down"
		al_endrow = idw_requestor.RowCount()		
		If ib_ongoingfind Then		
			//For ongoing finds do not search on the current row.			
			If al_startrow + 1 <= al_endrow Then
				al_startrow ++
			Else
				Return 0
			End If
		End If
END CHOOSE

Return 1
end function

on u_findreplace.create
TriggerEvent( this, "constructor" )
end on

on u_findreplace.destroy
TriggerEvent( this, "destructor" )
end on

