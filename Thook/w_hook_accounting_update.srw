HA$PBExportHeader$w_hook_accounting_update.srw
forward
global type w_hook_accounting_update from w_base_sheet_ext
end type
type dw_hook_update from u_base_dw_ext within w_hook_accounting_update
end type
end forward

global type w_hook_accounting_update from w_base_sheet_ext
integer x = 567
integer y = 268
integer width = 1970
integer height = 1516
string title = "Hook Accounting"
long backcolor = 12632256
dw_hook_update dw_hook_update
end type
global w_hook_accounting_update w_hook_accounting_update

type variables
boolean  ib_plant_valid = true

string is_closemessage_txt, &
         is_carrier, &
         is_plant

integer ii_error_column
long     il_error_row  

u_otr004 iu_otr004

integer i, cnt = 0



end variables

forward prototypes
public function boolean wf_update ()
public function boolean wf_retrieve ()
public function integer wf_changed ()
public function integer wf_hook_update ()
end prototypes

public function boolean wf_update ();Boolean    lb_return

datawindowchild lddwc_plant

integer    li_rtn, &
			  li_rc
		
long		  ll_Row 

Double	  ld_adj_in, &
			  ld_adj_out
//           ld_short_in, &
//			  ld_short_out
  
String     ls_plant_code, &
			  ls_hook_type

s_error	  lstr_Error_Info

dwItemStatus		status



lstr_error_info.se_event_name = "wf_update"
lstr_error_info.se_window_name = "w_hook_accounting_update"
lstr_error_info.se_procedure_name = "wf_update"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)


IF dw_hook_update.AcceptText() = -1 THEN Return(False)

IF not ib_plant_valid Then
	MessageBox("Plant Code Error", &
	  "All entered plant codes must be valid, please verify that there are no duplicates or mistyped codes.", &
	  Stopsign!, OK!)
	Return(False)
End IF


ll_Row = dw_hook_update.GetNextModified( ll_Row, Primary! )
DO WHILE ll_Row > 0 
		ls_plant_code	= dw_hook_update.GetItemString( ll_Row, "plant_code" )
      If ls_plant_code > "   " Then 
        	ld_adj_in        = dw_hook_update.GetItemNumber(ll_Row, "adjust1" )
   		ld_adj_out       = dw_hook_update.GetItemNumber(ll_Row, "adjust2")
			ls_hook_type  	  = dw_hook_update.getitemstring(ll_row, "hook_type")
//   		ld_short_in       = dw_hook_update.GetItemNumber(ll_Row, "adjust3")
//	   	ld_short_out   = dw_hook_update.GetItemNumber(ll_Row, "adjust4")
     
//         li_rtn = iu_otr004.nf_hamh02ar(is_carrier, ls_plant_code, ls_hook_type, &
//              ld_adj_out, ld_adj_in, lstr_Error_Info, 0)

	      dw_hook_update.SetItemStatus ( ll_Row, 0, Primary!, NotModified! )

		CHOOSE CASE li_rtn
			CASE is < 0 
              dw_hook_update.SetRow(ll_row)
              EXIT
         CASE is > 0
            li_rc = SQLCA.ii_messagebox_rtn
            CHOOSE CASE li_rc
               CASE 1
                  li_rc = 0
               CASE 2
                  li_rc = 0
               CASE 3
                  dw_hook_update.SetRow(ll_row)
                  EXIT
               CASE 4
                  dw_hook_update.SetRow(ll_row)
                  EXIT
               CASE 5
                  dw_hook_update.SetRow(ll_row)
                  dw_hook_update.SetRedraw(True)
                  Return False
               CASE 6
                  li_rc = 6
               CASE ELSE
                  dw_hook_update.SetRow(ll_row)
            END CHOOSE
        	CASE ELSE
				li_rc = 0
		END CHOOSE   
      End if  
  ll_Row = dw_hook_update.GetNextModified( ll_Row, Primary! )
LOOP

This.wf_hook_update()

Return TRUE



end function

public function boolean wf_retrieve ();integer	li_rc, & 
			li_answer

string   ls_hook_info

li_rc = wf_Changed()

CHOOSE CASE li_rc	//begin 1
	CASE  1	//	a valid change occurred ...
		li_answer = MessageBox("Wait",is_closemessage_txt,question!,yesnocancel!)
		CHOOSE CASE li_answer
			CASE 1	//	yes, save changes
				IF wf_Update() THEN	//	the save succeeded
				ELSE	//	the save failed
					li_answer = MessageBox("Save Failed","Retrieve Anyway?",question!,yesno!)
					IF li_answer = 1 THEN	//	retrieve w/o saving changes
					ELSE	//	cancel the close
						Return( false )
					END IF
				END IF	
			CASE 2 	//	retrieve w/o saving changes
			CASE 3	//	cancel the retrieve
				Return(false)
		END CHOOSE
	CASE  -1 		//	a validation error occurred
		li_answer = MessageBox("Data Entry Error","Retrieve w/o Save?",question!,okcancel!)
		CHOOSE CASE li_answer
			CASE 1	//	retrieve w/o saving changes
			CASE 2	//	cancel the retrieve
				Return( false )
		END CHOOSE
	CASE  -2 	//	a serious validation error, window may not close in this state
		MessageBox("A Serious Error Has Occurred","Correct Error Before Retrieving!",exclamation!,ok!)
		Return(False)
	CASE ELSE	//	nothing has changed
END CHOOSE

ls_hook_info = is_carrier + '~t' + is_plant
OpenWithParm( w_hook_accounting_filter, ls_hook_info, iw_frame ) 
ls_hook_info = Message.StringParm
IF len(trim(ls_hook_info)) = 0  Then
   Return True
End If
is_carrier = iw_frame.iu_string.nf_GetToken(ls_hook_info, '~t')
is_plant   = iw_frame.iu_string.nf_GetToken(ls_hook_info, '~t')


// Populate the datawindows
This.wf_hook_update()

If is_carrier <> " " Then
	this.Title = "Hook Summary For Carrier " + is_carrier
End If

Return FALSE

end function

public function integer wf_changed ();/*
	purpose:	this function will examine all datawindow controls on the window
				that have update capability against the database.  if anything has changed,
				and the change is valid, a 1 is returned.  if an invalid change has occurred
				then a -1 is returned.  if nothing has changed a 0 is returned.

*/


IF dw_hook_update.ModifiedCount() + &
	dw_hook_update.DeletedCount() > 0 THEN
	is_closemessage_txt = "Save Changes?"
	RETURN 1
END IF


// nothing has been changed
RETURN 0
	
end function

public function integer wf_hook_update ();datawindowchild lddwc_plant,lddwc_type

integer  li_rtn,li_row

string   ls_hook_update_string, &
      	ls_temp, &
         ls_refetch_plant_code, &
			ls_refetch_hook_type

dwitemstatus   ls_status

s_error                 lstr_Error_Info

dw_hook_update.Reset()
SetPointer( HourGlass! )
This.SetRedraw(False)

ls_refetch_plant_code      = Space(3)
ls_refetch_hook_type			= Space(2)

lstr_error_info.se_event_name = "wf_retrieve"
lstr_error_info.se_window_name = "w_hook_update_Accounting"
lstr_error_info.se_procedure_name = "wf_retrieve"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)
		
Do 
	ls_hook_update_string = Space(28100)
 	
//   li_rtn = iu_otr004.nf_hamh01ar(is_carrier, is_plant, &
//			ls_hook_update_string, ls_refetch_plant_code, ls_refetch_hook_type, &
//			lstr_error_info, 0)

// Check the return code from the above function call
		If li_rtn = 2 Then
         dw_hook_update.InsertRow(0)
         dw_hook_update.GetChild ("plant_code", lddwc_plant)
         lddwc_plant.SetTrans(SQLCA)
			lddwc_plant.Retrieve()
         dw_hook_update.SetColumn("plant_code")
         dw_hook_update.SetFocus()
			dw_hook_update.getchild("hook_type",lddwc_type)
			lddwc_type.SetTrans(sqlca)
			lddwc_type.Retrieve("EQIPTYPE")
         dw_hook_update.ResetUpdate()
         This.SetRedraw(True)
 			Return 1
		ELSE
         IF li_rtn <> 0 Then
            Return 1
         ELSE
      	   iw_frame.SetMicroHelp( "Ready...")
         END IF
		END IF

	//messagebox("Message",trim(ls_hook_update_string))
   li_rtn = dw_hook_update.ImportString(Trim(ls_hook_update_string))

Loop While len(trim(ls_Refetch_plant_code)) > 0

IF Trim(is_plant) = "" Then
   dw_hook_update.InsertRow(0)
   dw_hook_update.GetChild ("plant_code", lddwc_plant)
   lddwc_plant.SetTrans(SQLCA)
	lddwc_plant.Retrieve()
else
	li_row = dw_hook_update.rowcount()
	dw_hook_update.setcolumn("plant_code")
	dw_hook_update.setitem( li_row ,"plant_code",is_plant)
End If

dw_hook_update.getchild("hook_type",lddwc_type)
lddwc_type.SetTrans(sqlca)
lddwc_type.Retrieve("EQIPTYPE")

dw_hook_update.SetColumn("adjust1")
dw_hook_update.SetFocus()
dw_hook_update.ResetUpdate()
This.SetRedraw(True)

return 1
end function

event ue_postopen;call super::ue_postopen;string        ls_input_string


ls_input_string  =  Message.StringParm

is_carrier = Left(ls_input_string, 4)	
is_plant = Mid(ls_input_string, 5, 3)

IF wf_retrieve() Then 
	Close(This) 
	Return
END IF

If is_carrier <> " " Then
	this.Title = "Hook Summary For Carrier " + is_carrier
End If

end event

event open;call super::open;iu_otr004  =  CREATE u_otr004

dw_hook_update.InsertRow(0)









end event

on close;call w_base_sheet_ext::close;Destroy iu_otr004
end on

on w_hook_accounting_update.create
int iCurrent
call super::create
this.dw_hook_update=create dw_hook_update
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_hook_update
end on

on w_hook_accounting_update.destroy
call super::destroy
destroy(this.dw_hook_update)
end on

type dw_hook_update from u_base_dw_ext within w_hook_accounting_update
event ue_posttab pbm_custom24
integer x = 14
integer y = 4
integer width = 1911
integer height = 1400
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_hook_summary"
boolean vscrollbar = true
end type

event ue_posttab;scrolltorow(il_error_row)
setrow(il_error_row)
setcolumn(ii_error_column)
setfocus()

end event

event itemchanged;call super::itemchanged;datawindowchild lddwc_plant

long ll_RowCount, &
     ll_FoundRow, & 
	  ll_row

String ls_selected, &
       ls_plant


ll_RowCount =  dw_hook_update.RowCount()

IF dwo.name = "plant_code" THEN	
	IF KeyDown(KeyUpArrow!) = False and KeyDown(KeyDownArrow!) = False THEN
		ib_plant_valid = true
		IF len(trim(data)) = 0 Then return
   	ll_FoundRow = dw_hook_update.Find ( "plant_code='"+data+"'", 1, ll_RowCount)
   	IF ll_FoundRow > 0 THEN
		  	ib_plant_valid = false
        	Return 1
		Else
			Connect Using SQLCA;
      	SELECT locations.location_code  
       	INTO :ls_selected
     		FROM locations  
        	WHERE locations.location_code = :data;
      	IF sqlca.sqlcode = 0 THEN
        		dw_hook_update.SetItem(Row, "carrier_code", is_carrier)
				IF Row = ll_rowcount THEN 
	        		dw_hook_update.InsertRow(0) 
				END IF
     		Else
         	ib_plant_valid = false
				Disconnect Using SQLCA;
         	Return 1
      	END IF 
			Disconnect Using SQLCA;
   	END IF
  	END IF
ELSE
	ls_plant = dw_hook_update.object.data [ll_rowcount, 2]
	If row = ll_rowcount and len(trim(ls_plant)) = 0 then
	  	ib_plant_valid = false
	  	MessageBox("Warning", "Please make sure to enter a plant", Exclamation!, OK!)
  	End If
END if

//IF dwo.name = "hook_type" THEN	
//	if is_plant <> "" then
//		ll_row = dw_hook_update.insertrow(0)
//		dw_hook_update.setitem(ll_row,"carrier_code",is_carrier)
//		dw_hook_update.setitem(ll_row,"plant_code",is_plant)
//		//dw_hook_update
//	end if
//end if
end event

event itemerror;call super::itemerror;This.SelectText(1, 9999)
return 0

end event

event itemfocuschanged;call super::itemfocuschanged;IF not ib_plant_valid then dw_hook_update.setColumn("plant_code")

IF len(trim(dw_hook_update.object.data[rowcount(), 2]))>0 then
   IF len(trim(is_plant)) = 0 then
	   dw_hook_update.Insertrow(0)
	End IF
End if

selecttext(1,10)
end event

event ue_postconstructor;call super::ue_postconstructor;ib_updateable = TRUE

end event

