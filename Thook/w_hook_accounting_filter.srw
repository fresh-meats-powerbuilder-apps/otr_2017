HA$PBExportHeader$w_hook_accounting_filter.srw
forward
global type w_hook_accounting_filter from w_base_response_ext
end type
type dw_carriers from u_base_dw_ext within w_hook_accounting_filter
end type
type dw_plant from u_base_dw_ext within w_hook_accounting_filter
end type
end forward

global type w_hook_accounting_filter from w_base_response_ext
integer x = 859
integer y = 368
integer width = 1207
integer height = 528
string title = "Hook Accounting Filter "
long backcolor = 12632256
dw_carriers dw_carriers
dw_plant dw_plant
end type
global w_hook_accounting_filter w_hook_accounting_filter

type variables
datawindowchild  	idwc_carrier, idwc_plant

integer  ii_rc



end variables

event open;call super::open;
string        		ls_input_string, &
              		ls_carrier, &
						ls_plant


ls_input_string = Message.StringParm

ls_carrier = iw_frame.iu_string.nf_GetToken(ls_input_string, '~t')
ls_plant   = iw_frame.iu_string.nf_GetToken(ls_input_string, '~t')

IF ls_plant <> '' THEN 
	dw_plant.SetItem( 1, "plant_code", ls_plant)
END IF


IF ls_carrier <> '' THEN 
	dw_carriers.SetItem( 1, "carrier_code", ls_carrier)
END IF

dw_carriers.SelectText(1, 4)


end event

on w_hook_accounting_filter.create
int iCurrent
call super::create
this.dw_carriers=create dw_carriers
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_carriers
this.Control[iCurrent+2]=this.dw_plant
end on

on w_hook_accounting_filter.destroy
call super::destroy
destroy(this.dw_carriers)
destroy(this.dw_plant)
end on

event ue_base_cancel;call super::ue_base_cancel;string  ls_cancel

ls_cancel = ' '

CloseWithReturn( This, ls_cancel)


end event

event ue_base_ok;call super::ue_base_ok;char      lc_carrier_code[4], &
          lc_plant_code[3]

string    ls_output_info

lc_carrier_code[]	= " "
lc_plant_code[]	= " "


CHOOSE CASE 2
  case dw_carriers.AcceptText() + dw_plant.AcceptText()
  case else 
       return
END CHOOSE

lc_carrier_code = dw_carriers.GetItemString( 1, "carrier_code" )
lc_plant_code   = dw_plant.GetItemString(1, "plant_code")

IF len(trim(lc_carrier_code)) < 1 THEN 
	MessageBox("Carrier Code", "Carrier is a required field, please enter a value.")
	dw_carriers.SetFocus()
	dw_carriers.SelectText(1,4)
	Return 
END IF

ls_output_info = lc_carrier_code + '~t' + lc_plant_code
CloseWithReturn( This, ls_output_info )

 

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_hook_accounting_filter
integer x = 814
integer y = 284
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_hook_accounting_filter
integer x = 466
integer y = 284
integer taborder = 40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_hook_accounting_filter
integer x = 119
integer y = 280
integer taborder = 30
end type

type dw_carriers from u_base_dw_ext within w_hook_accounting_filter
integer x = 123
integer y = 32
integer width = 407
integer height = 176
integer taborder = 10
string dataobject = "d_dddw_carriers"
boolean border = false
end type

event itemchanged;call super::itemchanged;long ll_FoundRow

ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
IF ll_FoundRow < 1 THEN Return 1
IF len(trim(data)) < 1 THEN Return 1


end event

event itemerror;call super::itemerror;dw_carriers.SetFocus()  
Return 0
end event

event constructor;call super::constructor;InsertRow(0)

ii_rc = GetChild( 'carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTrans(SQLCA)
idwc_carrier.Retrieve()

end event

event getfocus;call super::getfocus;SelectText(1,4)
end event

type dw_plant from u_base_dw_ext within w_hook_accounting_filter
integer x = 654
integer y = 40
integer width = 439
integer height = 176
integer taborder = 20
string dataobject = "d_dddw_plants"
boolean border = false
end type

event itemerror;call super::itemerror;SetFocus()
Return 0
end event

on getfocus;call u_base_dw_ext::getfocus;SelectText(1,4)
end on

event constructor;call super::constructor;InsertRow(0)

ii_rc = GetChild( 'plant_code', idwc_plant )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for plant_code.")

idwc_plant.SetTrans(SQLCA)
idwc_plant.Retrieve()


end event

event itemchanged;call super::itemchanged;long ll_FoundRow

IF Trim(data) = "" THEN Return
ll_FoundRow = idwc_plant.Find ( "location_code='"+data+"'", 1, idwc_plant.RowCount() )
IF ll_FoundRow < 1 THEN Return 1
end event

